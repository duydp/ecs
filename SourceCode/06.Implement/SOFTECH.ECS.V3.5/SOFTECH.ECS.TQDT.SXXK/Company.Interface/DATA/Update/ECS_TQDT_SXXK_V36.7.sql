-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Insert]
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Berth]
(
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
)
VALUES 
(
	@ReferenceDB,
	@CodeHQ,
	@Code,
	@MoTa_HeThong,
	@TenCang,
	@DiaChi,
	@Note
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Update]
	@ID bigint,
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255)
AS

UPDATE
	[dbo].[t_VNACC_Category_Berth]
SET
	[ReferenceDB] = @ReferenceDB,
	[CodeHQ] = @CodeHQ,
	[Code] = @Code,
	[MoTa_HeThong] = @MoTa_HeThong,
	[TenCang] = @TenCang,
	[DiaChi] = @DiaChi,
	[Note] = @Note
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_InsertUpdate]
	@ID bigint,
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Berth] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Berth] 
		SET
			[ReferenceDB] = @ReferenceDB,
			[CodeHQ] = @CodeHQ,
			[Code] = @Code,
			[MoTa_HeThong] = @MoTa_HeThong,
			[TenCang] = @TenCang,
			[DiaChi] = @DiaChi,
			[Note] = @Note
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Berth]
		(
			[ReferenceDB],
			[CodeHQ],
			[Code],
			[MoTa_HeThong],
			[TenCang],
			[DiaChi],
			[Note]
		)
		VALUES 
		(
			@ReferenceDB,
			@CodeHQ,
			@Code,
			@MoTa_HeThong,
			@TenCang,
			@DiaChi,
			@Note
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Berth]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Berth] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM
	[dbo].[t_VNACC_Category_Berth]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM [dbo].[t_VNACC_Category_Berth] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM
	[dbo].[t_VNACC_Category_Berth]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_BorderGate_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_BorderGate]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@BorderGateCode,
	@BorderGateName,
	@CustomsOfficeCode,
	@ImmigrationBureauUserCode,
	@ArrivalDepartureForImmigration,
	@PassengerForImmigration,
	@QuarantineOfficeUserCode,
	@ArrivalDepartureForQuarantine,
	@PassengerForQuarantine,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_BorderGate]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[BorderGateName] = @BorderGateName,
	[CustomsOfficeCode] = @CustomsOfficeCode,
	[ImmigrationBureauUserCode] = @ImmigrationBureauUserCode,
	[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
	[PassengerForImmigration] = @PassengerForImmigration,
	[QuarantineOfficeUserCode] = @QuarantineOfficeUserCode,
	[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
	[PassengerForQuarantine] = @PassengerForQuarantine,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[BorderGateCode] = @BorderGateCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BorderGateCode varchar(10),
	@BorderGateName nvarchar(250),
	@CustomsOfficeCode varchar(5),
	@ImmigrationBureauUserCode varchar(5),
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@QuarantineOfficeUserCode varchar(5),
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [BorderGateCode] FROM [dbo].[t_VNACC_Category_BorderGate] WHERE [BorderGateCode] = @BorderGateCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_BorderGate] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[BorderGateName] = @BorderGateName,
			[CustomsOfficeCode] = @CustomsOfficeCode,
			[ImmigrationBureauUserCode] = @ImmigrationBureauUserCode,
			[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
			[PassengerForImmigration] = @PassengerForImmigration,
			[QuarantineOfficeUserCode] = @QuarantineOfficeUserCode,
			[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
			[PassengerForQuarantine] = @PassengerForQuarantine,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[BorderGateCode] = @BorderGateCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_BorderGate]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[BorderGateCode],
			[BorderGateName],
			[CustomsOfficeCode],
			[ImmigrationBureauUserCode],
			[ArrivalDepartureForImmigration],
			[PassengerForImmigration],
			[QuarantineOfficeUserCode],
			[ArrivalDepartureForQuarantine],
			[PassengerForQuarantine],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@BorderGateCode,
			@BorderGateName,
			@CustomsOfficeCode,
			@ImmigrationBureauUserCode,
			@ArrivalDepartureForImmigration,
			@PassengerForImmigration,
			@QuarantineOfficeUserCode,
			@ArrivalDepartureForQuarantine,
			@PassengerForQuarantine,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Delete]
	@BorderGateCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_BorderGate]
WHERE
	[BorderGateCode] = @BorderGateCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_BorderGate] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_Load]
	@BorderGateCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_BorderGate]
WHERE
	[BorderGateCode] = @BorderGateCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_BorderGate] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_BorderGate_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_BorderGate_SelectAll]



















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BorderGateCode],
	[BorderGateName],
	[CustomsOfficeCode],
	[ImmigrationBureauUserCode],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[QuarantineOfficeUserCode],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_BorderGate]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Cargo_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Cargo]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@BondedAreaCode,
	@UserCode,
	@BondedAreaName,
	@BondedAreaDemarcation,
	@NecessityIndicationOfBondedAreaName,
	@CustomsOfficeCodeForImportDeclaration,
	@CustomsOfficeCodeForExportDeclaration,
	@CustomsOfficeCodeForDeclarationOnTransportation,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Cargo]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[UserCode] = @UserCode,
	[BondedAreaName] = @BondedAreaName,
	[BondedAreaDemarcation] = @BondedAreaDemarcation,
	[NecessityIndicationOfBondedAreaName] = @NecessityIndicationOfBondedAreaName,
	[CustomsOfficeCodeForImportDeclaration] = @CustomsOfficeCodeForImportDeclaration,
	[CustomsOfficeCodeForExportDeclaration] = @CustomsOfficeCodeForExportDeclaration,
	[CustomsOfficeCodeForDeclarationOnTransportation] = @CustomsOfficeCodeForDeclarationOnTransportation,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[BondedAreaCode] = @BondedAreaCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@BondedAreaCode varchar(10),
	@UserCode varchar(10),
	@BondedAreaName nvarchar(250),
	@BondedAreaDemarcation char(1),
	@NecessityIndicationOfBondedAreaName int,
	@CustomsOfficeCodeForImportDeclaration varchar(5),
	@CustomsOfficeCodeForExportDeclaration varchar(5),
	@CustomsOfficeCodeForDeclarationOnTransportation varchar(5),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [BondedAreaCode] FROM [dbo].[t_VNACC_Category_Cargo] WHERE [BondedAreaCode] = @BondedAreaCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Cargo] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[UserCode] = @UserCode,
			[BondedAreaName] = @BondedAreaName,
			[BondedAreaDemarcation] = @BondedAreaDemarcation,
			[NecessityIndicationOfBondedAreaName] = @NecessityIndicationOfBondedAreaName,
			[CustomsOfficeCodeForImportDeclaration] = @CustomsOfficeCodeForImportDeclaration,
			[CustomsOfficeCodeForExportDeclaration] = @CustomsOfficeCodeForExportDeclaration,
			[CustomsOfficeCodeForDeclarationOnTransportation] = @CustomsOfficeCodeForDeclarationOnTransportation,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[BondedAreaCode] = @BondedAreaCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Cargo]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[BondedAreaCode],
			[UserCode],
			[BondedAreaName],
			[BondedAreaDemarcation],
			[NecessityIndicationOfBondedAreaName],
			[CustomsOfficeCodeForImportDeclaration],
			[CustomsOfficeCodeForExportDeclaration],
			[CustomsOfficeCodeForDeclarationOnTransportation],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@BondedAreaCode,
			@UserCode,
			@BondedAreaName,
			@BondedAreaDemarcation,
			@NecessityIndicationOfBondedAreaName,
			@CustomsOfficeCodeForImportDeclaration,
			@CustomsOfficeCodeForExportDeclaration,
			@CustomsOfficeCodeForDeclarationOnTransportation,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Delete]
	@BondedAreaCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Cargo]
WHERE
	[BondedAreaCode] = @BondedAreaCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Cargo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_Load]
	@BondedAreaCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Cargo]
WHERE
	[BondedAreaCode] = @BondedAreaCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Cargo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Cargo_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Cargo_SelectAll]


















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[BondedAreaCode],
	[UserCode],
	[BondedAreaName],
	[BondedAreaDemarcation],
	[NecessityIndicationOfBondedAreaName],
	[CustomsOfficeCodeForImportDeclaration],
	[CustomsOfficeCodeForExportDeclaration],
	[CustomsOfficeCodeForDeclarationOnTransportation],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Cargo]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_CityUNLOCODE]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@LOCODE,
	@CityCode,
	@CountryCode,
	@NecessityIndicationOfInputName,
	@AirportPortClassification,
	@CityNameOrStateName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CityUNLOCODE]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[LOCODE] = @LOCODE,
	[CityCode] = @CityCode,
	[CountryCode] = @CountryCode,
	[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
	[AirportPortClassification] = @AirportPortClassification,
	[CityNameOrStateName] = @CityNameOrStateName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@LOCODE varchar(10),
	@CityCode varchar(5),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@AirportPortClassification int,
	@CityNameOrStateName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_CityUNLOCODE] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CityUNLOCODE] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[LOCODE] = @LOCODE,
			[CityCode] = @CityCode,
			[CountryCode] = @CountryCode,
			[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
			[AirportPortClassification] = @AirportPortClassification,
			[CityNameOrStateName] = @CityNameOrStateName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_CityUNLOCODE]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[LOCODE],
			[CityCode],
			[CountryCode],
			[NecessityIndicationOfInputName],
			[AirportPortClassification],
			[CityNameOrStateName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@LOCODE,
			@CityCode,
			@CountryCode,
			@NecessityIndicationOfInputName,
			@AirportPortClassification,
			@CityNameOrStateName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CityUNLOCODE]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CityUNLOCODE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CityUNLOCODE]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CityUNLOCODE] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CityUNLOCODE_SelectAll]

















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[LOCODE],
	[CityCode],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[AirportPortClassification],
	[CityNameOrStateName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CityUNLOCODE]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Common_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Common_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Insert]
	@ReferenceDB varchar(5),
	@Code varchar(10),
	@Name_VN nvarchar(max),
	@Name_EN nvarchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Common]
(
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ReferenceDB,
	@Code,
	@Name_VN,
	@Name_EN,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Update]
	@ID int,
	@ReferenceDB varchar(5),
	@Code varchar(10),
	@Name_VN nvarchar(max),
	@Name_EN nvarchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Common]
SET
	[ReferenceDB] = @ReferenceDB,
	[Code] = @Code,
	[Name_VN] = @Name_VN,
	[Name_EN] = @Name_EN,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_InsertUpdate]
	@ID int,
	@ReferenceDB varchar(5),
	@Code varchar(10),
	@Name_VN nvarchar(max),
	@Name_EN nvarchar(max),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Common] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Common] 
		SET
			[ReferenceDB] = @ReferenceDB,
			[Code] = @Code,
			[Name_VN] = @Name_VN,
			[Name_EN] = @Name_EN,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Common]
		(
			[ReferenceDB],
			[Code],
			[Name_VN],
			[Name_EN],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ReferenceDB,
			@Code,
			@Name_VN,
			@Name_EN,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Common]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Common] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Common]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Common] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Common_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Common_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[Code],
	[Name_VN],
	[Name_EN],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Common]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_ContainerSize_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ContainerSizeCode varchar(5),
	@ContainerSizeName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_ContainerSize]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@ContainerSizeCode,
	@ContainerSizeName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ContainerSizeCode varchar(5),
	@ContainerSizeName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_ContainerSize]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[ContainerSizeCode] = @ContainerSizeCode,
	[ContainerSizeName] = @ContainerSizeName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@ContainerSizeCode varchar(5),
	@ContainerSizeName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_ContainerSize] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_ContainerSize] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[ContainerSizeCode] = @ContainerSizeCode,
			[ContainerSizeName] = @ContainerSizeName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_ContainerSize]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[ContainerSizeCode],
			[ContainerSizeName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@ContainerSizeCode,
			@ContainerSizeName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_ContainerSize]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_ContainerSize] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ContainerSize]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_ContainerSize] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_ContainerSize_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_ContainerSize_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[ContainerSizeCode],
	[ContainerSizeName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_ContainerSize]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Insert]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_CurrencyExchange]
(
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ErrorCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@MakerClassification,
	@NumberOfKeyItems,
	@CurrencyCode,
	@CurrencyName,
	@GenerationManagementIndication,
	@DateOfMaintenanceUpdated,
	@EndDate,
	@StartDate_1,
	@ExchangeRate_1,
	@StartDate_2,
	@ExchangeRate_2,
	@StartDate_3,
	@ExchangeRate_3,
	@StartDate_4,
	@ExchangeRate_4,
	@StartDate_5,
	@ExchangeRate_5,
	@StartDate_6,
	@ExchangeRate_6,
	@StartDate_7,
	@ExchangeRate_7,
	@StartDate_8,
	@ExchangeRate_8,
	@StartDate_9,
	@ExchangeRate_9,
	@StartDate_10,
	@ExchangeRate_10,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Update]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CurrencyExchange]
SET
	[ErrorCode] = @ErrorCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[MakerClassification] = @MakerClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CurrencyName] = @CurrencyName,
	[GenerationManagementIndication] = @GenerationManagementIndication,
	[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
	[EndDate] = @EndDate,
	[StartDate_1] = @StartDate_1,
	[ExchangeRate_1] = @ExchangeRate_1,
	[StartDate_2] = @StartDate_2,
	[ExchangeRate_2] = @ExchangeRate_2,
	[StartDate_3] = @StartDate_3,
	[ExchangeRate_3] = @ExchangeRate_3,
	[StartDate_4] = @StartDate_4,
	[ExchangeRate_4] = @ExchangeRate_4,
	[StartDate_5] = @StartDate_5,
	[ExchangeRate_5] = @ExchangeRate_5,
	[StartDate_6] = @StartDate_6,
	[ExchangeRate_6] = @ExchangeRate_6,
	[StartDate_7] = @StartDate_7,
	[ExchangeRate_7] = @ExchangeRate_7,
	[StartDate_8] = @StartDate_8,
	[ExchangeRate_8] = @ExchangeRate_8,
	[StartDate_9] = @StartDate_9,
	[ExchangeRate_9] = @ExchangeRate_9,
	[StartDate_10] = @StartDate_10,
	[ExchangeRate_10] = @ExchangeRate_10,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[CurrencyCode] = @CurrencyCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_InsertUpdate]
	@ErrorCode char(25),
	@PGNumber char(2),
	@TableID char(5),
	@ProcessClassification char(1),
	@MakerClassification char(1),
	@NumberOfKeyItems char(2),
	@CurrencyCode char(3),
	@CurrencyName nvarchar(250),
	@GenerationManagementIndication char(1),
	@DateOfMaintenanceUpdated datetime,
	@EndDate datetime,
	@StartDate_1 datetime,
	@ExchangeRate_1 decimal(9, 2),
	@StartDate_2 datetime,
	@ExchangeRate_2 decimal(9, 2),
	@StartDate_3 datetime,
	@ExchangeRate_3 decimal(9, 2),
	@StartDate_4 datetime,
	@ExchangeRate_4 decimal(9, 2),
	@StartDate_5 datetime,
	@ExchangeRate_5 decimal(9, 2),
	@StartDate_6 datetime,
	@ExchangeRate_6 decimal(9, 2),
	@StartDate_7 datetime,
	@ExchangeRate_7 decimal(9, 2),
	@StartDate_8 datetime,
	@ExchangeRate_8 decimal(9, 2),
	@StartDate_9 datetime,
	@ExchangeRate_9 decimal(9, 2),
	@StartDate_10 datetime,
	@ExchangeRate_10 decimal(9, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [CurrencyCode] FROM [dbo].[t_VNACC_Category_CurrencyExchange] WHERE [CurrencyCode] = @CurrencyCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CurrencyExchange] 
		SET
			[ErrorCode] = @ErrorCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[MakerClassification] = @MakerClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CurrencyName] = @CurrencyName,
			[GenerationManagementIndication] = @GenerationManagementIndication,
			[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
			[EndDate] = @EndDate,
			[StartDate_1] = @StartDate_1,
			[ExchangeRate_1] = @ExchangeRate_1,
			[StartDate_2] = @StartDate_2,
			[ExchangeRate_2] = @ExchangeRate_2,
			[StartDate_3] = @StartDate_3,
			[ExchangeRate_3] = @ExchangeRate_3,
			[StartDate_4] = @StartDate_4,
			[ExchangeRate_4] = @ExchangeRate_4,
			[StartDate_5] = @StartDate_5,
			[ExchangeRate_5] = @ExchangeRate_5,
			[StartDate_6] = @StartDate_6,
			[ExchangeRate_6] = @ExchangeRate_6,
			[StartDate_7] = @StartDate_7,
			[ExchangeRate_7] = @ExchangeRate_7,
			[StartDate_8] = @StartDate_8,
			[ExchangeRate_8] = @ExchangeRate_8,
			[StartDate_9] = @StartDate_9,
			[ExchangeRate_9] = @ExchangeRate_9,
			[StartDate_10] = @StartDate_10,
			[ExchangeRate_10] = @ExchangeRate_10,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[CurrencyCode] = @CurrencyCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_CurrencyExchange]
	(
			[ErrorCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[MakerClassification],
			[NumberOfKeyItems],
			[CurrencyCode],
			[CurrencyName],
			[GenerationManagementIndication],
			[DateOfMaintenanceUpdated],
			[EndDate],
			[StartDate_1],
			[ExchangeRate_1],
			[StartDate_2],
			[ExchangeRate_2],
			[StartDate_3],
			[ExchangeRate_3],
			[StartDate_4],
			[ExchangeRate_4],
			[StartDate_5],
			[ExchangeRate_5],
			[StartDate_6],
			[ExchangeRate_6],
			[StartDate_7],
			[ExchangeRate_7],
			[StartDate_8],
			[ExchangeRate_8],
			[StartDate_9],
			[ExchangeRate_9],
			[StartDate_10],
			[ExchangeRate_10],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ErrorCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@MakerClassification,
			@NumberOfKeyItems,
			@CurrencyCode,
			@CurrencyName,
			@GenerationManagementIndication,
			@DateOfMaintenanceUpdated,
			@EndDate,
			@StartDate_1,
			@ExchangeRate_1,
			@StartDate_2,
			@ExchangeRate_2,
			@StartDate_3,
			@ExchangeRate_3,
			@StartDate_4,
			@ExchangeRate_4,
			@StartDate_5,
			@ExchangeRate_5,
			@StartDate_6,
			@ExchangeRate_6,
			@StartDate_7,
			@ExchangeRate_7,
			@StartDate_8,
			@ExchangeRate_8,
			@StartDate_9,
			@ExchangeRate_9,
			@StartDate_10,
			@ExchangeRate_10,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Delete]
	@CurrencyCode char(3)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CurrencyExchange]
WHERE
	[CurrencyCode] = @CurrencyCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CurrencyExchange] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_Load]
	@CurrencyCode char(3)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CurrencyExchange]
WHERE
	[CurrencyCode] = @CurrencyCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CurrencyExchange] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CurrencyExchange_SelectAll]



































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ErrorCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[MakerClassification],
	[NumberOfKeyItems],
	[CurrencyCode],
	[CurrencyName],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[EndDate],
	[StartDate_1],
	[ExchangeRate_1],
	[StartDate_2],
	[ExchangeRate_2],
	[StartDate_3],
	[ExchangeRate_3],
	[StartDate_4],
	[ExchangeRate_4],
	[StartDate_5],
	[ExchangeRate_5],
	[StartDate_6],
	[ExchangeRate_6],
	[StartDate_7],
	[ExchangeRate_7],
	[StartDate_8],
	[ExchangeRate_8],
	[StartDate_9],
	[ExchangeRate_9],
	[StartDate_10],
	[ExchangeRate_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CurrencyExchange]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsOffice_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_CustomsOffice]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@CustomsCode,
	@CustomsProvinceCode,
	@OfficeIndicationOfExport,
	@OfficeIndicationOfImport,
	@OfficeIndicationOfBondedRelated,
	@OfficeIndicationOfSupervision,
	@CustomsOfficeName,
	@CustomsOfficeNameInVietnamese,
	@NameOfHeadOfCustomsOfficeInVietnamese,
	@Postcode,
	@AddressInVietnamese,
	@NameOfHeadOfCustomsOfficeInRomanAlphabet,
	@CustomsProvinceName,
	@CustomsBranchName,
	@DestinationCode_1,
	@DestinationCode_2,
	@DestinationCode_3,
	@DestinationCode_4,
	@DestinationCode_5,
	@DestinationCode_6,
	@DestinationCode_7,
	@DestinationCode_8,
	@DestinationCode_9,
	@DestinationCode_10,
	@InspectAtInspectionSite,
	@InspectUsingLargeXrayScanner,
	@TransportationKindCode,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CustomsOffice]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CustomsProvinceCode] = @CustomsProvinceCode,
	[OfficeIndicationOfExport] = @OfficeIndicationOfExport,
	[OfficeIndicationOfImport] = @OfficeIndicationOfImport,
	[OfficeIndicationOfBondedRelated] = @OfficeIndicationOfBondedRelated,
	[OfficeIndicationOfSupervision] = @OfficeIndicationOfSupervision,
	[CustomsOfficeName] = @CustomsOfficeName,
	[CustomsOfficeNameInVietnamese] = @CustomsOfficeNameInVietnamese,
	[NameOfHeadOfCustomsOfficeInVietnamese] = @NameOfHeadOfCustomsOfficeInVietnamese,
	[Postcode] = @Postcode,
	[AddressInVietnamese] = @AddressInVietnamese,
	[NameOfHeadOfCustomsOfficeInRomanAlphabet] = @NameOfHeadOfCustomsOfficeInRomanAlphabet,
	[CustomsProvinceName] = @CustomsProvinceName,
	[CustomsBranchName] = @CustomsBranchName,
	[DestinationCode_1] = @DestinationCode_1,
	[DestinationCode_2] = @DestinationCode_2,
	[DestinationCode_3] = @DestinationCode_3,
	[DestinationCode_4] = @DestinationCode_4,
	[DestinationCode_5] = @DestinationCode_5,
	[DestinationCode_6] = @DestinationCode_6,
	[DestinationCode_7] = @DestinationCode_7,
	[DestinationCode_8] = @DestinationCode_8,
	[DestinationCode_9] = @DestinationCode_9,
	[DestinationCode_10] = @DestinationCode_10,
	[InspectAtInspectionSite] = @InspectAtInspectionSite,
	[InspectUsingLargeXrayScanner] = @InspectUsingLargeXrayScanner,
	[TransportationKindCode] = @TransportationKindCode,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[CustomsCode] = @CustomsCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsProvinceCode varchar(5),
	@OfficeIndicationOfExport int,
	@OfficeIndicationOfImport int,
	@OfficeIndicationOfBondedRelated int,
	@OfficeIndicationOfSupervision int,
	@CustomsOfficeName varchar(100),
	@CustomsOfficeNameInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInVietnamese nvarchar(250),
	@Postcode varchar(10),
	@AddressInVietnamese nvarchar(250),
	@NameOfHeadOfCustomsOfficeInRomanAlphabet varchar(100),
	@CustomsProvinceName nvarchar(250),
	@CustomsBranchName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@InspectAtInspectionSite nvarchar(250),
	@InspectUsingLargeXrayScanner nvarchar(250),
	@TransportationKindCode int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [CustomsCode] FROM [dbo].[t_VNACC_Category_CustomsOffice] WHERE [CustomsCode] = @CustomsCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CustomsOffice] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CustomsProvinceCode] = @CustomsProvinceCode,
			[OfficeIndicationOfExport] = @OfficeIndicationOfExport,
			[OfficeIndicationOfImport] = @OfficeIndicationOfImport,
			[OfficeIndicationOfBondedRelated] = @OfficeIndicationOfBondedRelated,
			[OfficeIndicationOfSupervision] = @OfficeIndicationOfSupervision,
			[CustomsOfficeName] = @CustomsOfficeName,
			[CustomsOfficeNameInVietnamese] = @CustomsOfficeNameInVietnamese,
			[NameOfHeadOfCustomsOfficeInVietnamese] = @NameOfHeadOfCustomsOfficeInVietnamese,
			[Postcode] = @Postcode,
			[AddressInVietnamese] = @AddressInVietnamese,
			[NameOfHeadOfCustomsOfficeInRomanAlphabet] = @NameOfHeadOfCustomsOfficeInRomanAlphabet,
			[CustomsProvinceName] = @CustomsProvinceName,
			[CustomsBranchName] = @CustomsBranchName,
			[DestinationCode_1] = @DestinationCode_1,
			[DestinationCode_2] = @DestinationCode_2,
			[DestinationCode_3] = @DestinationCode_3,
			[DestinationCode_4] = @DestinationCode_4,
			[DestinationCode_5] = @DestinationCode_5,
			[DestinationCode_6] = @DestinationCode_6,
			[DestinationCode_7] = @DestinationCode_7,
			[DestinationCode_8] = @DestinationCode_8,
			[DestinationCode_9] = @DestinationCode_9,
			[DestinationCode_10] = @DestinationCode_10,
			[InspectAtInspectionSite] = @InspectAtInspectionSite,
			[InspectUsingLargeXrayScanner] = @InspectUsingLargeXrayScanner,
			[TransportationKindCode] = @TransportationKindCode,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[CustomsCode] = @CustomsCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_CustomsOffice]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[CustomsCode],
			[CustomsProvinceCode],
			[OfficeIndicationOfExport],
			[OfficeIndicationOfImport],
			[OfficeIndicationOfBondedRelated],
			[OfficeIndicationOfSupervision],
			[CustomsOfficeName],
			[CustomsOfficeNameInVietnamese],
			[NameOfHeadOfCustomsOfficeInVietnamese],
			[Postcode],
			[AddressInVietnamese],
			[NameOfHeadOfCustomsOfficeInRomanAlphabet],
			[CustomsProvinceName],
			[CustomsBranchName],
			[DestinationCode_1],
			[DestinationCode_2],
			[DestinationCode_3],
			[DestinationCode_4],
			[DestinationCode_5],
			[DestinationCode_6],
			[DestinationCode_7],
			[DestinationCode_8],
			[DestinationCode_9],
			[DestinationCode_10],
			[InspectAtInspectionSite],
			[InspectUsingLargeXrayScanner],
			[TransportationKindCode],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@CustomsCode,
			@CustomsProvinceCode,
			@OfficeIndicationOfExport,
			@OfficeIndicationOfImport,
			@OfficeIndicationOfBondedRelated,
			@OfficeIndicationOfSupervision,
			@CustomsOfficeName,
			@CustomsOfficeNameInVietnamese,
			@NameOfHeadOfCustomsOfficeInVietnamese,
			@Postcode,
			@AddressInVietnamese,
			@NameOfHeadOfCustomsOfficeInRomanAlphabet,
			@CustomsProvinceName,
			@CustomsBranchName,
			@DestinationCode_1,
			@DestinationCode_2,
			@DestinationCode_3,
			@DestinationCode_4,
			@DestinationCode_5,
			@DestinationCode_6,
			@DestinationCode_7,
			@DestinationCode_8,
			@DestinationCode_9,
			@DestinationCode_10,
			@InspectAtInspectionSite,
			@InspectUsingLargeXrayScanner,
			@TransportationKindCode,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Delete]
	@CustomsCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CustomsOffice]
WHERE
	[CustomsCode] = @CustomsCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CustomsOffice] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_Load]
	@CustomsCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsOffice]
WHERE
	[CustomsCode] = @CustomsCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CustomsOffice] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsOffice_SelectAll]





































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsProvinceCode],
	[OfficeIndicationOfExport],
	[OfficeIndicationOfImport],
	[OfficeIndicationOfBondedRelated],
	[OfficeIndicationOfSupervision],
	[CustomsOfficeName],
	[CustomsOfficeNameInVietnamese],
	[NameOfHeadOfCustomsOfficeInVietnamese],
	[Postcode],
	[AddressInVietnamese],
	[NameOfHeadOfCustomsOfficeInRomanAlphabet],
	[CustomsProvinceName],
	[CustomsBranchName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[InspectAtInspectionSite],
	[InspectUsingLargeXrayScanner],
	[TransportationKindCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsOffice]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_CustomsSubSection]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@CustomsCode,
	@CustomsSubSectionCode,
	@ImportExportClassification,
	@Name,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Update]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_CustomsSubSection]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[CustomsCode] = @CustomsCode,
	[CustomsSubSectionCode] = @CustomsSubSectionCode,
	[ImportExportClassification] = @ImportExportClassification,
	[Name] = @Name,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_InsertUpdate]
	@ID int,
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@CustomsCode varchar(10),
	@CustomsSubSectionCode varchar(5),
	@ImportExportClassification varchar(5),
	@Name nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_CustomsSubSection] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_CustomsSubSection] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[CustomsCode] = @CustomsCode,
			[CustomsSubSectionCode] = @CustomsSubSectionCode,
			[ImportExportClassification] = @ImportExportClassification,
			[Name] = @Name,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_CustomsSubSection]
		(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[CustomsCode],
			[CustomsSubSectionCode],
			[ImportExportClassification],
			[Name],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@CustomsCode,
			@CustomsSubSectionCode,
			@ImportExportClassification,
			@Name,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_CustomsSubSection]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_CustomsSubSection] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsSubSection]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_CustomsSubSection] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_CustomsSubSection_SelectAll]















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[CustomsCode],
	[CustomsSubSectionCode],
	[ImportExportClassification],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_CustomsSubSection]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(12),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_HSCode]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@HSCode,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(12),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_HSCode]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[HSCode] = @HSCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(12),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [HSCode] FROM [dbo].[t_VNACC_Category_HSCode] WHERE [HSCode] = @HSCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_HSCode] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[HSCode] = @HSCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_HSCode]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[HSCode],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@HSCode,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Delete]
	@HSCode varchar(12)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_HSCode]
WHERE
	[HSCode] = @HSCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_HSCode] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Load]
	@HSCode varchar(12)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_HSCode]
WHERE
	[HSCode] = @HSCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_HSCode] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_HSCode]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Nation_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Nation]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@NationCode,
	@GenerationManagementIndication,
	@DateOfMaintenanceUpdated,
	@CountryShortName,
	@ApplicationStartDate,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Nation]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[GenerationManagementIndication] = @GenerationManagementIndication,
	[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
	[CountryShortName] = @CountryShortName,
	[ApplicationStartDate] = @ApplicationStartDate,
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry] = @PlaceOfOriginIsMFNTaxRatesApplicationCountry,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[NationCode] = @NationCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NationCode varchar(10),
	@GenerationManagementIndication varchar(5),
	@DateOfMaintenanceUpdated datetime,
	@CountryShortName varchar(20),
	@ApplicationStartDate datetime,
	@PlaceOfOriginIsMFNTaxRatesApplicationCountry int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [NationCode] FROM [dbo].[t_VNACC_Category_Nation] WHERE [NationCode] = @NationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Nation] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[GenerationManagementIndication] = @GenerationManagementIndication,
			[DateOfMaintenanceUpdated] = @DateOfMaintenanceUpdated,
			[CountryShortName] = @CountryShortName,
			[ApplicationStartDate] = @ApplicationStartDate,
			[PlaceOfOriginIsMFNTaxRatesApplicationCountry] = @PlaceOfOriginIsMFNTaxRatesApplicationCountry,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[NationCode] = @NationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Nation]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[NationCode],
			[GenerationManagementIndication],
			[DateOfMaintenanceUpdated],
			[CountryShortName],
			[ApplicationStartDate],
			[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@NationCode,
			@GenerationManagementIndication,
			@DateOfMaintenanceUpdated,
			@CountryShortName,
			@ApplicationStartDate,
			@PlaceOfOriginIsMFNTaxRatesApplicationCountry,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Delete]
	@NationCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Nation]
WHERE
	[NationCode] = @NationCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Nation] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_Load]
	@NationCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Nation]
WHERE
	[NationCode] = @NationCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Nation] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Nation_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Nation_SelectAll]
















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NationCode],
	[GenerationManagementIndication],
	[DateOfMaintenanceUpdated],
	[CountryShortName],
	[ApplicationStartDate],
	[PlaceOfOriginIsMFNTaxRatesApplicationCountry],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Nation]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_OGAUser_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_OGAUser]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@OfficeOfApplicationCode,
	@OfficeOfApplicationName,
	@DestinationCode_1,
	@DestinationCode_2,
	@DestinationCode_3,
	@DestinationCode_4,
	@DestinationCode_5,
	@DestinationCode_6,
	@DestinationCode_7,
	@DestinationCode_8,
	@DestinationCode_9,
	@DestinationCode_10,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_OGAUser]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[OfficeOfApplicationName] = @OfficeOfApplicationName,
	[DestinationCode_1] = @DestinationCode_1,
	[DestinationCode_2] = @DestinationCode_2,
	[DestinationCode_3] = @DestinationCode_3,
	[DestinationCode_4] = @DestinationCode_4,
	[DestinationCode_5] = @DestinationCode_5,
	[DestinationCode_6] = @DestinationCode_6,
	[DestinationCode_7] = @DestinationCode_7,
	[DestinationCode_8] = @DestinationCode_8,
	[DestinationCode_9] = @DestinationCode_9,
	[DestinationCode_10] = @DestinationCode_10,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@OfficeOfApplicationCode varchar(10),
	@OfficeOfApplicationName nvarchar(250),
	@DestinationCode_1 varchar(10),
	@DestinationCode_2 varchar(10),
	@DestinationCode_3 varchar(10),
	@DestinationCode_4 varchar(10),
	@DestinationCode_5 varchar(10),
	@DestinationCode_6 varchar(10),
	@DestinationCode_7 varchar(10),
	@DestinationCode_8 varchar(10),
	@DestinationCode_9 varchar(10),
	@DestinationCode_10 varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [OfficeOfApplicationCode] FROM [dbo].[t_VNACC_Category_OGAUser] WHERE [OfficeOfApplicationCode] = @OfficeOfApplicationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_OGAUser] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[OfficeOfApplicationName] = @OfficeOfApplicationName,
			[DestinationCode_1] = @DestinationCode_1,
			[DestinationCode_2] = @DestinationCode_2,
			[DestinationCode_3] = @DestinationCode_3,
			[DestinationCode_4] = @DestinationCode_4,
			[DestinationCode_5] = @DestinationCode_5,
			[DestinationCode_6] = @DestinationCode_6,
			[DestinationCode_7] = @DestinationCode_7,
			[DestinationCode_8] = @DestinationCode_8,
			[DestinationCode_9] = @DestinationCode_9,
			[DestinationCode_10] = @DestinationCode_10,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[OfficeOfApplicationCode] = @OfficeOfApplicationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_OGAUser]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[OfficeOfApplicationCode],
			[OfficeOfApplicationName],
			[DestinationCode_1],
			[DestinationCode_2],
			[DestinationCode_3],
			[DestinationCode_4],
			[DestinationCode_5],
			[DestinationCode_6],
			[DestinationCode_7],
			[DestinationCode_8],
			[DestinationCode_9],
			[DestinationCode_10],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@OfficeOfApplicationCode,
			@OfficeOfApplicationName,
			@DestinationCode_1,
			@DestinationCode_2,
			@DestinationCode_3,
			@DestinationCode_4,
			@DestinationCode_5,
			@DestinationCode_6,
			@DestinationCode_7,
			@DestinationCode_8,
			@DestinationCode_9,
			@DestinationCode_10,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Delete]
	@OfficeOfApplicationCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_OGAUser]
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_OGAUser] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_Load]
	@OfficeOfApplicationCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_OGAUser]
WHERE
	[OfficeOfApplicationCode] = @OfficeOfApplicationCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_OGAUser] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_OGAUser_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_OGAUser_SelectAll]






















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[OfficeOfApplicationCode],
	[OfficeOfApplicationName],
	[DestinationCode_1],
	[DestinationCode_2],
	[DestinationCode_3],
	[DestinationCode_4],
	[DestinationCode_5],
	[DestinationCode_6],
	[DestinationCode_7],
	[DestinationCode_8],
	[DestinationCode_9],
	[DestinationCode_10],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_OGAUser]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_PackagesUnit_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_PackagesUnit]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@NumberOfPackagesUnitCode,
	@NumberOfPackagesUnitName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_PackagesUnit]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[NumberOfPackagesUnitName] = @NumberOfPackagesUnitName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@NumberOfPackagesUnitCode varchar(5),
	@NumberOfPackagesUnitName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [NumberOfPackagesUnitCode] FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE [NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_PackagesUnit] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[NumberOfPackagesUnitName] = @NumberOfPackagesUnitName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_PackagesUnit]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[NumberOfPackagesUnitCode],
			[NumberOfPackagesUnitName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@NumberOfPackagesUnitCode,
			@NumberOfPackagesUnitName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Delete]
	@NumberOfPackagesUnitCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_PackagesUnit]
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_PackagesUnit] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_Load]
	@NumberOfPackagesUnitCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_PackagesUnit]
WHERE
	[NumberOfPackagesUnitCode] = @NumberOfPackagesUnitCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_PackagesUnit] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_PackagesUnit_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[NumberOfPackagesUnitCode],
	[NumberOfPackagesUnitName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_PackagesUnit]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_QuantityUnit_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Insert]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_QuantityUnit]
(
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@TableID,
	@Code,
	@Name,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Update]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_QuantityUnit]
SET
	[TableID] = @TableID,
	[Name] = @Name,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[Code] = @Code

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_InsertUpdate]
	@TableID varchar(10),
	@Code varchar(10),
	@Name nvarchar(250),
	@Notes nvarchar(150),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [Code] FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE [Code] = @Code)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_QuantityUnit] 
		SET
			[TableID] = @TableID,
			[Name] = @Name,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[Code] = @Code
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_QuantityUnit]
	(
			[TableID],
			[Code],
			[Name],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@TableID,
			@Code,
			@Name,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Delete]
	@Code varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_QuantityUnit]
WHERE
	[Code] = @Code

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_QuantityUnit] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_Load]
	@Code varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_QuantityUnit]
WHERE
	[Code] = @Code
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_QuantityUnit] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_QuantityUnit_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[TableID],
	[Code],
	[Name],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_QuantityUnit]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Station_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Station_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(6),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_Stations]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@StationCode,
	@StationName,
	@CountryCode,
	@NecessityIndicationOfInputName,
	@CustomsOfficeCode,
	@CustomsOfficeCodeRM,
	@UserCodeOfImmigrationBureau,
	@ManifestForImmigration,
	@ArrivalDepartureForImmigration,
	@PassengerForImmigration,
	@CrewForImmigration,
	@UserCodeOfQuarantineStation,
	@ManifestForQuarantine,
	@ArrivalDepartureForQuarantine,
	@PassengerForQuarantine,
	@CrewForQuarantine,
	@UserCodeOfRailwayStationAuthority,
	@ManifestForRailwayStationAuthority,
	@ArrivalDepartureForRailwayStationAuthority,
	@PassengerForRailwayStationAuthority,
	@CrewForRailwayStationAuthority,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(6),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_Stations]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[StationName] = @StationName,
	[CountryCode] = @CountryCode,
	[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
	[CustomsOfficeCode] = @CustomsOfficeCode,
	[CustomsOfficeCodeRM] = @CustomsOfficeCodeRM,
	[UserCodeOfImmigrationBureau] = @UserCodeOfImmigrationBureau,
	[ManifestForImmigration] = @ManifestForImmigration,
	[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
	[PassengerForImmigration] = @PassengerForImmigration,
	[CrewForImmigration] = @CrewForImmigration,
	[UserCodeOfQuarantineStation] = @UserCodeOfQuarantineStation,
	[ManifestForQuarantine] = @ManifestForQuarantine,
	[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
	[PassengerForQuarantine] = @PassengerForQuarantine,
	[CrewForQuarantine] = @CrewForQuarantine,
	[UserCodeOfRailwayStationAuthority] = @UserCodeOfRailwayStationAuthority,
	[ManifestForRailwayStationAuthority] = @ManifestForRailwayStationAuthority,
	[ArrivalDepartureForRailwayStationAuthority] = @ArrivalDepartureForRailwayStationAuthority,
	[PassengerForRailwayStationAuthority] = @PassengerForRailwayStationAuthority,
	[CrewForRailwayStationAuthority] = @CrewForRailwayStationAuthority,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[StationCode] = @StationCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@StationCode varchar(6),
	@StationName nvarchar(250),
	@CountryCode varchar(5),
	@NecessityIndicationOfInputName int,
	@CustomsOfficeCode varchar(5),
	@CustomsOfficeCodeRM varchar(5),
	@UserCodeOfImmigrationBureau varchar(5),
	@ManifestForImmigration int,
	@ArrivalDepartureForImmigration int,
	@PassengerForImmigration int,
	@CrewForImmigration int,
	@UserCodeOfQuarantineStation varchar(5),
	@ManifestForQuarantine int,
	@ArrivalDepartureForQuarantine int,
	@PassengerForQuarantine int,
	@CrewForQuarantine int,
	@UserCodeOfRailwayStationAuthority varchar(5),
	@ManifestForRailwayStationAuthority int,
	@ArrivalDepartureForRailwayStationAuthority int,
	@PassengerForRailwayStationAuthority int,
	@CrewForRailwayStationAuthority int,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [StationCode] FROM [dbo].[t_VNACC_Category_Stations] WHERE [StationCode] = @StationCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Stations] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[StationName] = @StationName,
			[CountryCode] = @CountryCode,
			[NecessityIndicationOfInputName] = @NecessityIndicationOfInputName,
			[CustomsOfficeCode] = @CustomsOfficeCode,
			[CustomsOfficeCodeRM] = @CustomsOfficeCodeRM,
			[UserCodeOfImmigrationBureau] = @UserCodeOfImmigrationBureau,
			[ManifestForImmigration] = @ManifestForImmigration,
			[ArrivalDepartureForImmigration] = @ArrivalDepartureForImmigration,
			[PassengerForImmigration] = @PassengerForImmigration,
			[CrewForImmigration] = @CrewForImmigration,
			[UserCodeOfQuarantineStation] = @UserCodeOfQuarantineStation,
			[ManifestForQuarantine] = @ManifestForQuarantine,
			[ArrivalDepartureForQuarantine] = @ArrivalDepartureForQuarantine,
			[PassengerForQuarantine] = @PassengerForQuarantine,
			[CrewForQuarantine] = @CrewForQuarantine,
			[UserCodeOfRailwayStationAuthority] = @UserCodeOfRailwayStationAuthority,
			[ManifestForRailwayStationAuthority] = @ManifestForRailwayStationAuthority,
			[ArrivalDepartureForRailwayStationAuthority] = @ArrivalDepartureForRailwayStationAuthority,
			[PassengerForRailwayStationAuthority] = @PassengerForRailwayStationAuthority,
			[CrewForRailwayStationAuthority] = @CrewForRailwayStationAuthority,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[StationCode] = @StationCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_Stations]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[StationCode],
			[StationName],
			[CountryCode],
			[NecessityIndicationOfInputName],
			[CustomsOfficeCode],
			[CustomsOfficeCodeRM],
			[UserCodeOfImmigrationBureau],
			[ManifestForImmigration],
			[ArrivalDepartureForImmigration],
			[PassengerForImmigration],
			[CrewForImmigration],
			[UserCodeOfQuarantineStation],
			[ManifestForQuarantine],
			[ArrivalDepartureForQuarantine],
			[PassengerForQuarantine],
			[CrewForQuarantine],
			[UserCodeOfRailwayStationAuthority],
			[ManifestForRailwayStationAuthority],
			[ArrivalDepartureForRailwayStationAuthority],
			[PassengerForRailwayStationAuthority],
			[CrewForRailwayStationAuthority],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@StationCode,
			@StationName,
			@CountryCode,
			@NecessityIndicationOfInputName,
			@CustomsOfficeCode,
			@CustomsOfficeCodeRM,
			@UserCodeOfImmigrationBureau,
			@ManifestForImmigration,
			@ArrivalDepartureForImmigration,
			@PassengerForImmigration,
			@CrewForImmigration,
			@UserCodeOfQuarantineStation,
			@ManifestForQuarantine,
			@ArrivalDepartureForQuarantine,
			@PassengerForQuarantine,
			@CrewForQuarantine,
			@UserCodeOfRailwayStationAuthority,
			@ManifestForRailwayStationAuthority,
			@ArrivalDepartureForRailwayStationAuthority,
			@PassengerForRailwayStationAuthority,
			@CrewForRailwayStationAuthority,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Delete]
	@StationCode varchar(6)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Stations]
WHERE
	[StationCode] = @StationCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Stations] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_Load]
	@StationCode varchar(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Stations]
WHERE
	[StationCode] = @StationCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_Stations] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Station_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Station_SelectAll]































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[StationCode],
	[StationName],
	[CountryCode],
	[NecessityIndicationOfInputName],
	[CustomsOfficeCode],
	[CustomsOfficeCodeRM],
	[UserCodeOfImmigrationBureau],
	[ManifestForImmigration],
	[ArrivalDepartureForImmigration],
	[PassengerForImmigration],
	[CrewForImmigration],
	[UserCodeOfQuarantineStation],
	[ManifestForQuarantine],
	[ArrivalDepartureForQuarantine],
	[PassengerForQuarantine],
	[CrewForQuarantine],
	[UserCodeOfRailwayStationAuthority],
	[ManifestForRailwayStationAuthority],
	[ArrivalDepartureForRailwayStationAuthority],
	[PassengerForRailwayStationAuthority],
	[CrewForRailwayStationAuthority],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_Stations]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_TaxClassificationCode]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@TaxCode,
	@TaxName,
	@IndicationOfCommonTax,
	@IndicationOfPreferantialTax,
	@IndicationOfMFNTax,
	@IndicationOfFTATax,
	@IndicationOfOutOfQuota,
	@IndicationOfSpecificDuty,
	@IndicationOfSpecificDutyAndAdValoremDuty,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_TaxClassificationCode]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[TaxName] = @TaxName,
	[IndicationOfCommonTax] = @IndicationOfCommonTax,
	[IndicationOfPreferantialTax] = @IndicationOfPreferantialTax,
	[IndicationOfMFNTax] = @IndicationOfMFNTax,
	[IndicationOfFTATax] = @IndicationOfFTATax,
	[IndicationOfOutOfQuota] = @IndicationOfOutOfQuota,
	[IndicationOfSpecificDuty] = @IndicationOfSpecificDuty,
	[IndicationOfSpecificDutyAndAdValoremDuty] = @IndicationOfSpecificDutyAndAdValoremDuty,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[TaxCode] = @TaxCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TaxCode varchar(5),
	@TaxName nvarchar(250),
	@IndicationOfCommonTax decimal(5, 2),
	@IndicationOfPreferantialTax decimal(5, 2),
	@IndicationOfMFNTax decimal(5, 2),
	@IndicationOfFTATax decimal(5, 2),
	@IndicationOfOutOfQuota decimal(5, 2),
	@IndicationOfSpecificDuty decimal(5, 2),
	@IndicationOfSpecificDutyAndAdValoremDuty decimal(5, 2),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [TaxCode] FROM [dbo].[t_VNACC_Category_TaxClassificationCode] WHERE [TaxCode] = @TaxCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_TaxClassificationCode] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[TaxName] = @TaxName,
			[IndicationOfCommonTax] = @IndicationOfCommonTax,
			[IndicationOfPreferantialTax] = @IndicationOfPreferantialTax,
			[IndicationOfMFNTax] = @IndicationOfMFNTax,
			[IndicationOfFTATax] = @IndicationOfFTATax,
			[IndicationOfOutOfQuota] = @IndicationOfOutOfQuota,
			[IndicationOfSpecificDuty] = @IndicationOfSpecificDuty,
			[IndicationOfSpecificDutyAndAdValoremDuty] = @IndicationOfSpecificDutyAndAdValoremDuty,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[TaxCode] = @TaxCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_TaxClassificationCode]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[TaxCode],
			[TaxName],
			[IndicationOfCommonTax],
			[IndicationOfPreferantialTax],
			[IndicationOfMFNTax],
			[IndicationOfFTATax],
			[IndicationOfOutOfQuota],
			[IndicationOfSpecificDuty],
			[IndicationOfSpecificDutyAndAdValoremDuty],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@TaxCode,
			@TaxName,
			@IndicationOfCommonTax,
			@IndicationOfPreferantialTax,
			@IndicationOfMFNTax,
			@IndicationOfFTATax,
			@IndicationOfOutOfQuota,
			@IndicationOfSpecificDuty,
			@IndicationOfSpecificDutyAndAdValoremDuty,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Delete]
	@TaxCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_TaxClassificationCode]
WHERE
	[TaxCode] = @TaxCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_TaxClassificationCode] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_Load]
	@TaxCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TaxClassificationCode]
WHERE
	[TaxCode] = @TaxCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_TaxClassificationCode] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TaxClassificationCode_SelectAll]



















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TaxCode],
	[TaxName],
	[IndicationOfCommonTax],
	[IndicationOfPreferantialTax],
	[IndicationOfMFNTax],
	[IndicationOfFTATax],
	[IndicationOfOutOfQuota],
	[IndicationOfSpecificDuty],
	[IndicationOfSpecificDutyAndAdValoremDuty],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TaxClassificationCode]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_TransportMean_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TransportMeansCode varchar(5),
	@TransportMeansName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_TransportMeans]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@TransportMeansCode,
	@TransportMeansName,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TransportMeansCode varchar(5),
	@TransportMeansName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_TransportMeans]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[TransportMeansName] = @TransportMeansName,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[TransportMeansCode] = @TransportMeansCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@TransportMeansCode varchar(5),
	@TransportMeansName nvarchar(250),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [TransportMeansCode] FROM [dbo].[t_VNACC_Category_TransportMeans] WHERE [TransportMeansCode] = @TransportMeansCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_TransportMeans] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[TransportMeansName] = @TransportMeansName,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[TransportMeansCode] = @TransportMeansCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_TransportMeans]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[TransportMeansCode],
			[TransportMeansName],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@TransportMeansCode,
			@TransportMeansName,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Delete]
	@TransportMeansCode varchar(5)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_TransportMeans]
WHERE
	[TransportMeansCode] = @TransportMeansCode

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_TransportMeans] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_Load]
	@TransportMeansCode varchar(5)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TransportMeans]
WHERE
	[TransportMeansCode] = @TransportMeansCode
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_TransportMeans] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_TransportMean_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_TransportMean_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[TransportMeansCode],
	[TransportMeansName],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_TransportMeans]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Type_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Type_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Insert]
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Type]
(
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
)
VALUES 
(
	@English,
	@Vietnamese,
	@ReferenceDB,
	@Notes
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Update]
	@ID int,
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150)
AS

UPDATE
	[dbo].[t_VNACC_Category_Type]
SET
	[English] = @English,
	[Vietnamese] = @Vietnamese,
	[ReferenceDB] = @ReferenceDB,
	[Notes] = @Notes
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_InsertUpdate]
	@ID int,
	@English varchar(250),
	@Vietnamese nvarchar(250),
	@ReferenceDB varchar(4),
	@Notes nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Type] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Type] 
		SET
			[English] = @English,
			[Vietnamese] = @Vietnamese,
			[ReferenceDB] = @ReferenceDB,
			[Notes] = @Notes
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Type]
		(
			[English],
			[Vietnamese],
			[ReferenceDB],
			[Notes]
		)
		VALUES 
		(
			@English,
			@Vietnamese,
			@ReferenceDB,
			@Notes
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Type]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Type] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM
	[dbo].[t_VNACC_Category_Type]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM [dbo].[t_VNACC_Category_Type] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Type_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Type_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[English],
	[Vietnamese],
	[ReferenceDB],
	[Notes]
FROM
	[dbo].[t_VNACC_Category_Type]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Update]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_Load]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACCS_Mapper_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACCS_Mapper_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Insert]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Insert]
	@CodeV4 varchar(100),
	@NameV4 nvarchar(2000),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(2000),
	@LoaiMapper varchar(50),
	@Notes nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_VNACCS_Mapper]
(
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
)
VALUES 
(
	@CodeV4,
	@NameV4,
	@CodeV5,
	@NameV5,
	@LoaiMapper,
	@Notes
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Update]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Update]
	@ID bigint,
	@CodeV4 varchar(100),
	@NameV4 nvarchar(2000),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(2000),
	@LoaiMapper varchar(50),
	@Notes nvarchar(2000)
AS

UPDATE
	[dbo].[t_VNACCS_Mapper]
SET
	[CodeV4] = @CodeV4,
	[NameV4] = @NameV4,
	[CodeV5] = @CodeV5,
	[NameV5] = @NameV5,
	[LoaiMapper] = @LoaiMapper,
	[Notes] = @Notes
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_InsertUpdate]
	@ID bigint,
	@CodeV4 varchar(100),
	@NameV4 nvarchar(2000),
	@CodeV5 varchar(50),
	@NameV5 nvarchar(2000),
	@LoaiMapper varchar(50),
	@Notes nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACCS_Mapper] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACCS_Mapper] 
		SET
			[CodeV4] = @CodeV4,
			[NameV4] = @NameV4,
			[CodeV5] = @CodeV5,
			[NameV5] = @NameV5,
			[LoaiMapper] = @LoaiMapper,
			[Notes] = @Notes
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACCS_Mapper]
		(
			[CodeV4],
			[NameV4],
			[CodeV5],
			[NameV5],
			[LoaiMapper],
			[Notes]
		)
		VALUES 
		(
			@CodeV4,
			@NameV4,
			@CodeV5,
			@NameV5,
			@LoaiMapper,
			@Notes
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Delete]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_VNACCS_Mapper]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACCS_Mapper] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_Load]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM
	[dbo].[t_VNACCS_Mapper]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM [dbo].[t_VNACCS_Mapper] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_VNACCS_Mapper_SelectAll]
-- Database: ECS_TQDT_GC_V4_01_04_2016_SCAVI
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACCS_Mapper_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[CodeV4],
	[NameV4],
	[CodeV5],
	[NameV5],
	[LoaiMapper],
	[Notes]
FROM
	[dbo].[t_VNACCS_Mapper]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '36.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('36.7',GETDATE(), N'CẬP NHẬT MÃ ĐỊNH DANH HÀNG HÓA TT MỚI')
END