﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseSPMaperManagementForm : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public T_KHOKETOAN_SANPHAM_MAP SPMap = new T_KHOKETOAN_SANPHAM_MAP();

        public WareHouseSPMaperManagementForm()
        {
            InitializeComponent();
        }

        private void WareHouseSPMaperManagementForm_Load(object sender, EventArgs e)
        {

        }
        private void BinDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionAll();
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSPMap()
        {
            try
            {
                grListSPMap.Refetch();
                grListSPMap.DataSource = SP.SPCollection;
                grListSPMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BinDataSP();
        }

        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SP = (T_KHOKETOAN_SANPHAM)i.GetRow().DataRow;
                        SP.SPCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionBy_KHOSP_ID(SP.ID);
                    }
                }
                BindDataSPMap();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSPMap_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                SPMap = (T_KHOKETOAN_SANPHAM_MAP)e.Row.DataRow;
                SP = T_KHOKETOAN_SANPHAM.Load(SPMap.KHOSP_ID);
                SP.SPCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionBy_KHOSP_ID(SP.ID);
                WareHouseSPMaperForm f = new WareHouseSPMaperForm();
                f.SP = SP;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            try
            {
                WareHouseNPLAutoMaperForm f = new WareHouseNPLAutoMaperForm();
                f.Type = "SP";
                f.ShowDialog(this);
                BinDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                WareHouseSPMaperForm f = new WareHouseSPMaperForm();
                f.ShowDialog(this);
                BinDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearchSP_Click(object sender, EventArgs e)
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("MASP LIKE '%" + txtMaSP.Text.ToString() + "%'", "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchSPMap_Click(object sender, EventArgs e)
        {
            try
            {
                grListSPMap.Refetch();
                grListSPMap.DataSource = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP LIKE '%" + txtSPMap.Text.ToString() + "%'", "");
                grListSPMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
