using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class ThongkeTK : DevExpress.XtraReports.UI.XtraReport
    {
       
        public DateTime tungay;
        public DateTime denngay;
        public string NguoiLapBieu;
        public string NguoiDuyetBieu;
        public string DonViBaoCao;
        public ThongkeTK()
        {
            InitializeComponent();
        }

        public void BindData() {
            lblSoToKhai.DataBindings.Add("Text", this.DataSource,"ToKhai");            
            lblMaHaiQuan.DataBindings.Add("Text", this.DataSource, "MaHaiQuan");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, "NgayDangKy","{0:dd/MM/yyyy}");
            lblTriGiaKB.DataBindings.Add("Text", this.DataSource, "TongTriGiaKhaiBao", "{0:N2}");
            lblTriGiaTT.DataBindings.Add("Text", this.DataSource, "TongTriGiaTinhThue", "{0:N2}");
            lblNguyenTe.DataBindings.Add("Text", this.DataSource, "NguyenTe_ID");
            lblTrangthai.DataBindings.Add("Text", this.DataSource, "stringTrangThai");
            DateTime today = new DateTime();
            today = DateTime.Now;
            lbNgay.Text = "Từ ngày   " + tungay.ToString("dd/MM/yyyy") + "   Đến ngày   " + denngay.ToString("dd/MM/yyyy");
            lblNgayKy.Text = "..........., ngày........tháng...... năm........ "; ;
            
            lblNguoiLapBieu.Text = this.NguoiLapBieu.ToUpper();
            lblNguoiDuyetBieu.Text = this.NguoiDuyetBieu.ToUpper();
            lblTenNganDN.Text = "ĐVBC : " + this.DonViBaoCao.ToUpper();

            lblTenDN.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblMaDN.Text = "Mã số : " + GlobalSettings.MA_DON_VI;   
        
        }

    }
}
