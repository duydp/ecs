﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseManager : BaseForm
    {
        public T_KHOKETOAN_PHIEUXNKHO PhieuXNKho;
        public bool IsShowChonPXNK = false;
        public string whereCondition;
        public string LoaiChungTu;
        public List<T_KHOKETOAN_PHIEUXNKHO> PhieuXNKhoCollectionSelected = null;
        public WareHouseManager()
        {
            InitializeComponent();
            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";
        }
        private string GetSearchWhere()
        {
            try
            {
                String where = " 1 = 1 ";
                if (!String.IsNullOrEmpty(txtSoCT.Text.ToString()))
                    where += " AND SOCT LIKE '%" + txtSoCT.Text.ToString() + "%'";
                string TuNgay = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:000");
                string DenNgay = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                where += " AND NGAYCT BETWEEN '" + TuNgay +"' AND '" + DenNgay + "'";
                if (!String.IsNullOrEmpty(txtSoCTGoc.Text.ToString()))
                    where += " AND SOCTGOC LIKE '%" + txtSoCT.Text.ToString() + "%'";
                if (!String.IsNullOrEmpty(txtSoTK.Text.ToString()))
                {
                    where += " AND SOTK LIKE '%" + txtSoTK.Text.ToString() + "%'";
                    string DateFrom = clcDateFrom.Value.ToString("yyyy-MM-dd 00:00:000");
                    string DateTo = clcDateTo.Value.ToString("yyyy-MM-dd 23:59:59");
                    where += " AND NGAYTK BETWEEN '" + DateFrom + "' AND '" + DateTo + "'";
                }
                if (!String.IsNullOrEmpty(txtSoHoaDon.Text.ToString()))
                    where += " AND SOHOADON LIKE '%" + txtSoHoaDon.Text.ToString() + "%'";
                if (!String.IsNullOrEmpty(cbbMaKho.Value.ToString()))
                    where += " AND MAKHO ='" + cbbMaKho.Value.ToString() + "'";
                if (!String.IsNullOrEmpty(cbbLoaiPhieu.SelectedValue.ToString()))
                    where += " AND LOAICHUNGTU ='" + cbbLoaiPhieu.SelectedValue.ToString() + "'";
                if (!String.IsNullOrEmpty(cbbLoaiHang.SelectedValue.ToString()))
                    where += " AND LOAIHANGHOA ='" + cbbLoaiHang.SelectedValue.ToString() + "'";
                return where;
            }
            catch (Exception ex)
            {
                return "";
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                grList.Refetch();
                grList.DataSource = T_KHOKETOAN_PHIEUXNKHO.SelectCollectionDynamic(GetSearchWhere(), "ID");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void WareHouseManager_Load(object sender, EventArgs e)
        {
            cbbMaKho.SelectedIndex = 0;
            cbbLoaiPhieu.SelectedIndex = 0;
            cbbLoaiHang.SelectedIndex = 0;
            btnSearch_Click(null,null);
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                PhieuXNKho = new T_KHOKETOAN_PHIEUXNKHO();
                PhieuXNKho = (T_KHOKETOAN_PHIEUXNKHO)e.Row.DataRow;
                PhieuXNKho.HangCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionBy_PHIEUXNKHO_ID(PhieuXNKho.ID);
                WareHouseExportForm f = new WareHouseExportForm();
                f.PhieuXNKho = PhieuXNKho;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.Cells["LOAICHUNGTU"].Value != null)
                {
                    switch (e.Row.Cells["LOAICHUNGTU"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu nhập kho";
                            break;
                        case "X":
                            e.Row.Cells["LOAICHUNGTU"].Text = "Phiếu xuất kho";
                            break;
                    }
                }
                if (e.Row.Cells["LOAIHANGHOA"].Value != null)
                {
                    switch (e.Row.Cells["LOAIHANGHOA"].Value.ToString())
                    {
                        case "N":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Nguyên phụ liệu";
                            break;
                        case "S":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Sản phẩm";
                            break;
                        case "T":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Thiết bị";
                            break;
                        case "H":
                            e.Row.Cells["LOAIHANGHOA"].Text = "Hàng mẫu";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_PHIEUXNKHO> ItemColl = new List<T_KHOKETOAN_PHIEUXNKHO>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_PHIEUXNKHO)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_PHIEUXNKHO item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> HangHoaCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionBy_PHIEUXNKHO_ID(item.ID);
                            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA hang in HangHoaCollection)
                            {
                                hang.Delete();
                            }
                            item.Delete();
                        }
                    }
                    btnSearch_Click(null,null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {

                WareHouseXNKAutoForm frm = new WareHouseXNKAutoForm();
                frm.ShowDialog(this);
        }

        private void mnuPrint_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_PHIEUXNKHO> ItemColl = new List<T_KHOKETOAN_PHIEUXNKHO>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ItemColl.Add((T_KHOKETOAN_PHIEUXNKHO)i.GetRow().DataRow);
                    }

                }
                WareHousePrintReportAllForm f = new WareHousePrintReportAllForm();
                f.PhieuXNKCollection = ItemColl;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
