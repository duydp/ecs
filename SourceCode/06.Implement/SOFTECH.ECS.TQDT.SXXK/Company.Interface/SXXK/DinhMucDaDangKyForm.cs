﻿

using System;
using System.Windows.Forms;
using Company.BLL.SXXK ;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;


using System.Xml.Serialization;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.SXXK
{
    public partial class DinhMucDaDangKyForm : BaseForm
    {
        private DinhMucCollection dmCollection = new DinhMucCollection();
        private DinhMucCollection tmpCollection = new DinhMucCollection();
        private string xmlCurrent = "";
        public string nhomLoaiHinh = "";
        public DinhMucDaDangKyForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

      

        

        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor; 
                this.search();               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
        //private ToKhaiMauDich getTKMDByID(long id)
        //{
        //    foreach (ToKhaiMauDich tk in this.tkmdCollection)
        //    {
        //        if (tk.ID == id) return tk;
        //    }
        //    return null;
        //}

        

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
          
            this.dmCollection = DinhMuc.GetDinhMucXML(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            dgList.DataSource = this.dmCollection ;
          
           

           
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }


        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                ShowMessage("Chưa chọn danh sách tờ khai", false);
                return;
            }
            try
            {
                DinhMucCollection col = new DinhMucCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(DinhMucCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;                           
                            col.Add(dmSelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
       
       
      

        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
     

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
   

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXuat_Click(object sender, EventArgs e)
        {
          this.ExportData();
        }


       

       
    }
}