﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.DuLieuChuan;
using Company.Interface.SXXK;
using NguyenPhuLieu=Company.BLL.SXXK.NguyenPhuLieu;
using SanPham=Company.BLL.SXXK.SanPham;
using Janus.Windows.GridEX;
using System.Data;
using System.Collections.Generic;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucEditForm : BaseForm
    {
        public DinhMuc DMDetail = new DinhMuc();        
        public DinhMucDangKy dmDangKy;        
        public string maSP = "";
        SelectSanPhamDMForm fsp = new SelectSanPhamDMForm();
        SelectNguyenPhuLieuDMForm fnpl = new SelectNguyenPhuLieuDMForm();
        public DinhMucEditForm()
        {
            InitializeComponent();
            fsp.status = true;
            fnpl.status = true;
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
            nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.Ma = txtMaNPL.Text;
            if (nplSXXK.Load())
            {                
                txtMaNPL.Text = nplSXXK.Ma.Trim();
                txtTenNPL.Text = nplSXXK.Ten.Trim();
                txtMaHSNPL.Text = nplSXXK.MaHS.Trim();
                txtDonViTinhNPL.Text = DonViTinh.GetName(nplSXXK.DVT_ID);
                error.SetError(txtMaNPL, string.Empty);
            }
            else
            {                
                error.SetIconPadding(txtMaNPL, -8);
               // error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                }
                else
                {
                    error.SetError(txtMaNPL, "This material does not exist.");
                }
                
                txtTenNPL.Text = txtMaHSNPL.Text = txtDonViTinhNPL.Text = string.Empty;
            }
        }

        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            if (fsp == null)
            {
                fsp = new SelectSanPhamDMForm();
                fsp.status = true;
            }
            fsp.CalledForm = this.Name;
            fsp.ShowDialog();            
            
            if (fsp.SanPhamSelected.Ma.Length > 0)
            {
                
                txtMaSP.Text = fsp.SanPhamSelected.Ma.Trim();
                txtTenSP.Text = fsp.SanPhamSelected.Ten.Trim();
                txtMaHSSP.Text = fsp.SanPhamSelected.MaHS.Trim();
                txtDonViTinhSP.Text = DonViTinh.GetName(fsp.SanPhamSelected.DVT_ID);
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            if (fnpl == null)
            {
                fnpl = new SelectNguyenPhuLieuDMForm();
                fnpl.status = true;
            }
            fnpl.CalledForm = this.Name;
            fnpl.ShowDialog(this);
            if (fnpl.NguyenPhuLieuSelected.Ma.Length > 0)
            {
                
                txtMaNPL.Text = fnpl.NguyenPhuLieuSelected.Ma.Trim();
                txtTenNPL.Text = fnpl.NguyenPhuLieuSelected.Ten.Trim();
                txtMaHSNPL.Text = fnpl.NguyenPhuLieuSelected.MaHS.Trim();
                txtDonViTinhNPL.Text = DonViTinh.GetName(fnpl.NguyenPhuLieuSelected.DVT_ID);
            }
        }
      
        private void DinhMucEditForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtDinhMuc.DecimalDigits = GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            
            txtTyLeHH.DecimalDigits = GlobalSettings.SoThapPhan.TLHH;
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            
            SanPham spSXXK = new SanPham();
            spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.Ma = DMDetail.MaSanPham;
            if (spSXXK.Load())
            {                
                txtMaSP.Text = spSXXK.Ma.Trim();
                txtTenSP.Text = spSXXK.Ten.Trim();
                txtMaHSSP.Text = spSXXK.MaHS;
                txtDonViTinhSP.Text = DonViTinh.GetName(spSXXK.DVT_ID);
            }
          
            NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
            nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.Ma = DMDetail.MaNguyenPhuLieu;
            if (nplSXXK.Load())
            {                
                txtMaNPL.Text = nplSXXK.Ma.Trim();
                txtTenNPL.Text = nplSXXK.Ten.Trim();
                txtMaHSNPL.Text = nplSXXK.MaHS;
                txtDonViTinhNPL.Text = DonViTinh.GetName(nplSXXK.DVT_ID);
            }
           
            txtDinhMuc.Value = DMDetail.DinhMucSuDung;
            txtTyLeHH.Value = DMDetail.TyLeHaoHut;     
            dgList.DataSource = dmDangKy.DMCollection;
            dgList.Refetch();
            if (dmDangKy.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                btnAdd.Visible = false;
                btnCopy.Visible = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            SanPham spSXXK = new SanPham();
            spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.Ma = txtMaSP.Text;
            if (spSXXK.Load())
            {                
                error.SetError(txtMaNPL, string.Empty);

                txtMaSP.Text = spSXXK.Ma.Trim();
                txtTenSP.Text = spSXXK.Ten.Trim();
                txtMaHSSP.Text = spSXXK.MaHS;
                txtDonViTinhSP.Text = DonViTinh.GetName(spSXXK.DVT_ID);
                error.SetError(txtMaSP, string.Empty);
            }
            else
            {                
                error.SetIconPadding(txtMaSP, -8);
                //error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                }
                else
                {
                    error.SetError(txtMaSP, "This product does not exist.");
                }

                txtTenSP.Text = txtMaHSSP.Text = txtDonViTinhSP.Text = string.Empty;
            }
        }
 
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private bool checkDM(string masp, string manpl)
        {          
            foreach (DinhMuc dm in dmDangKy.DMCollection)
            {
                if (dm.MaSanPham.Trim() == txtMaSP.Text.Trim() && dm.MaNguyenPhuLieu.Trim() == txtMaNPL.Text.Trim())
                {
                    
                       // ShowMessage("Định mức này đã được khai báo trên lưới.", false);

                     MLMessages("Định mức này đã được khai báo trên lưới.","MSG_ADD01", "", false);                   
                   
                    
                    if (DMDetail.MaSanPham.Trim() != "")
                        this.dmDangKy.DMCollection.Add(DMDetail);
                    return true;
                }
            }
            return false;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            txtMaNPL.Focus();
            SanPham spSXXK = new SanPham();
            spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.Ma = txtMaSP.Text;
            if (!spSXXK.Load())
            {
                error.SetIconPadding(txtMaSP, -8);
               // error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaSP, "Không tồn tại sản phẩm này.");
                }
                else
                {
                    error.SetError(txtMaSP, "This product haven't exist.");
                }

                txtTenSP.Text = txtMaHSSP.Text = txtDonViTinhSP.Text = string.Empty;
                return;
            }
            NguyenPhuLieu nplSXXK = new NguyenPhuLieu();
            nplSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            nplSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            nplSXXK.Ma = txtMaNPL.Text;
            if (!nplSXXK.Load())
            {
                error.SetIconPadding(txtMaNPL, -8);
                //error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaNPL, "Không tồn tại nguyên phụ liệu này.");
                }
                else
                {
                    error.SetError(txtMaNPL, "This material haven't exist.");
                }
                txtTenNPL.Text = txtMaHSNPL.Text = txtDonViTinhNPL.Text = string.Empty;
            }

            if (Convert.ToDecimal(txtDinhMuc.Text) <= 0)
            {
                error.SetIconPadding(txtDinhMuc, -8);
               // error.SetError(txtDinhMuc, "Định mức phải lớn hơn 0.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtDinhMuc, "Định mức phải lớn hơn 0.");
                }
                else
                {
                    error.SetError(txtDinhMuc, "The norm must be greater than zero.");
                }
                return;
            }
            long id=DinhMuc.GetIDDinhMucExit(txtMaSP.Text.Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            if (id>0)
            {
                MLMessages("Định mức này đã được khai báo trong danh sách định mức khác có ID = " + id, "MSG_ADD01", "", false);                   
                
                return ;
            }
            
            //kiem tra xem da dang ky dinh muc chua
            Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
            ttdm.MaHaiQuan = dmDangKy.MaHaiQuan;
            ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            ttdm.MaSanPham = txtMaSP.Text.Trim();
            if (ttdm.Load())
            {
                //ShowMessage("Sản phẩm này đã được đăng ký.", false);
                MLMessages("Sản phẩm này đã được đăng ký.", "MSG_ADD01", "", false);
                //Message("MSG_ADD01","", false);
                return;
            }
            this.dmDangKy.DMCollection.Remove(DMDetail);
            foreach (DinhMuc dm in dmDangKy.DMCollection)
            {
                if (dm.MaSanPham == txtMaSP.Text.Trim() && dm.MaNguyenPhuLieu == txtMaNPL.Text.Trim())
                {
                   // ShowMessage("Định mức này đã được khai báo trên lưới.", false);
                    MLMessages("Định mức này đã được khai báo trên lưới.", "MSG_ADD01", "", false);
                   // Message("MSG_ADD01","", false);
                    if(DMDetail.MaSanPham.Trim()!="")
                        this.dmDangKy.DMCollection.Add(DMDetail);
                    return ;
                }
            }

           if(DMDetail==null || DMDetail.ID==0)
                this.DMDetail = new DinhMuc();
            this.DMDetail.MaSanPham = txtMaSP.Text.Trim();
            this.DMDetail.MaNguyenPhuLieu = txtMaNPL.Text.Trim();
            this.DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Value);
            this.DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Value);
            this.DMDetail.DVT_ID = this.DonViTinh_GetID(txtDonViTinhNPL.Text.Trim());
            this.DMDetail.TenNPL = txtTenNPL.Text;
            this.DMDetail.IsFromVietNam = chIsVietNam.Checked;
            this.dmDangKy.DMCollection.Add(DMDetail);            
            //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
            try
            {
                string where = "1 = 1";
                where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", DMDetail.Master_ID, Company.KDT.SHARE.Components.LoaiKhaiBao.DinhMuc);
                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                if (listLog.Count > 0)
                {
                    long idLog = listLog[0].IDLog;
                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                    long idDK = listLog[0].ID_DK;
                    string guidstr = listLog[0].GUIDSTR_DK;
                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                    string userSuaDoi = GlobalSettings.UserLog;
                    DateTime ngaySuaDoi = DateTime.Now;
                    string ghiChu = listLog[0].GhiChu;
                    bool isDelete = listLog[0].IsDelete;
                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }

            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }

            txtMaNPL.Text = "";
            txtMaHSNPL.Text = "";
            txtTenNPL.Text = "";
            txtDinhMuc.Value = 0;
            txtTyLeHH.Value = 0;
          
            this.DMDetail = new DinhMuc();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
           // if (ShowMessage("Bạn có muốn xóa nguyên phụ liệu này không?", true) == "Yes")
            //if (Message("MSG_DEL01", "", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa định mức này không?", "", "MSG_DEL01", true) == "Yes")   
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                        // Thực hiện xóa trong CSDL.
                        if (dmSelected.ID > 0)
                        {
                            dmSelected.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {                
                DMDetail = (DinhMuc)e.Row.DataRow;
                txtMaSP.Text = DMDetail.MaSanPham.Trim();
                txtMaNPL.Text = DMDetail.MaNguyenPhuLieu.Trim();
                txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                chIsVietNam.Checked = DMDetail.IsFromVietNam;
                txtMaSP_Leave(null, null);
                txtMaNPL_Leave(null, null);
            }            
        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            //Company.Interface.Controls.ScreenShoot.CaptureImage("Test Bug");
            if(txtMaSP.Text.Trim()=="")
            {
                //ShowMessage("Bạn phải chọn mã sản phẩm cần khai báo định mức trước.",false);
                MLMessages("Bạn phải chọn mã sản phẩm cần khai báo định mức trước.", "MSG_SP01", "", false);
                return;
            }
            if (DinhMuc.checkDinhMucExit(txtMaSP.Text.Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
            {
               // ShowMessage("Định mức này đã được khai báo trong danh sách định mức khác.", false);
                //thoilv
                MLMessages("Định mức này đã được khai báo trong danh sách định mức khác.", "MSG_DMC03", "", false); 
                return ;
            }
            Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
            ttdm.MaHaiQuan = dmDangKy.MaHaiQuan;
            ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            ttdm.MaSanPham = txtMaSP.Text.Trim();
            if (ttdm.Load())
            {
               // ShowMessage("Sản phẩm này đã được đăng ký định mức.", false);
                MLMessages("Sản phẩm này đã được đăng ký định mức.", "MSG_DMC04", "", false);
               // Message("MSG_ADD01","", false); 
                return;
            }
            SaoChepDinhMucForm f = new SaoChepDinhMucForm();
            f.dmdk = dmDangKy;
            f.MaSP = txtMaSP.Text.Trim();
            f.ShowDialog();
            {
                foreach (BLL.SXXK.DinhMuc dmSaoChep in f.collectionCopy)
                {
                    DinhMuc dm = new DinhMuc();
                    dm.MaSanPham = txtMaSP.Text.Trim();
                    dm.MaNguyenPhuLieu = dmSaoChep.MaNguyenPhuLieu;
                    dm.DinhMucSuDung = dmSaoChep.DinhMucSuDung;
                    dm.TyLeHaoHut = dmSaoChep.TyLeHaoHut;
                    NguyenPhuLieu NPL = new NguyenPhuLieu();
                    NPL.Ma = dm.MaNguyenPhuLieu;
                    NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    NPL.Load();
                    dm.TenNPL = NPL.Ten;
                    dmDangKy.DMCollection.Add(dm);
                }
              
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void btnCopyMenu_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow().RowType == RowType.GroupHeader)
            {
                SaoChepDinhMucMenuForm f = new SaoChepDinhMucMenuForm();
                f.DMDangKy = dmDangKy;
                DinhMuc dmSelect = (DinhMuc)dgList.GetRow().GetChildRows()[0].DataRow;
                f.MaSanPhamCopy = dmSelect.MaSanPham;
                f.ShowDialog();
                foreach (DinhMuc dm in f.collectionCopy)
                {
                    DinhMuc dmCopy = new DinhMuc();
                    dmCopy.MaSanPham = dm.MaSanPham;
                    dmCopy.MaNguyenPhuLieu = dm.MaNguyenPhuLieu;
                    dmCopy.DinhMucSuDung = dm.DinhMucSuDung;
                    dmCopy.TyLeHaoHut = dm.TyLeHaoHut;
                    NguyenPhuLieu NPL = new NguyenPhuLieu();
                    NPL.Ma = dmCopy.MaNguyenPhuLieu;
                    NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    NPL.Load();
                    dmCopy.TenNPL = NPL.Ten;
                    dmDangKy.DMCollection.Add(dmCopy);
                }
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DinhMucCollection DMCollectionTMP = new DinhMucCollection();
            if (MLMessages("Bạn có muốn xóa định mức này không?", "", "MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMuc dmSelected = (DinhMuc)i.GetRow().DataRow;
                        // Thực hiện xóa trong CSDL.
                        if (dmSelected.ID > 0)
                        {
                            dmSelected.Delete();
                            DMCollectionTMP.Add(dmSelected);
                        }
                    }
                }
            }
            else
            {
                return;
            }
            foreach (DinhMuc dm in DMCollectionTMP)
            {
                dmDangKy.DMCollection.Remove(dm);
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (txtMaSP.Text.Trim().Length == 0)
            {
                ShowMessage("Chưa chọn mã sản phẩm.", false);
                return;
            }
            foreach (DinhMuc dmtmp in dmDangKy.DMCollection)
            {
                if (dmtmp.MaSanPham.Trim().ToUpper() == txtMaSP.Text.Trim().ToUpper())
                {
                    ShowMessage("Đã nhập định mức của  mã sản phẩm này trên lưới.", false);
                    return;
                }
            }

            if (DinhMuc.checkDinhMucExit(txtMaSP.Text.Trim(), dmDangKy.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI))
            {                
                MLMessages("Định mức này đã được khai báo trong danh sách định mức khác.", "MSG_DMC03", "", false);
                return;
            }
            Company.BLL.SXXK.ThongTinDinhMuc ttdm = new Company.BLL.SXXK.ThongTinDinhMuc();
            ttdm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            ttdm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            ttdm.MaSanPham = txtMaSP.Text.Trim();
            if (ttdm.Load())
            {
                ShowMessage("Sản phẩm này đã được đăng ký định mức.", false);
                return;
            }
            DataSet ds = Company.BLL.SXXK.ThanhKhoan.NPLNhapTon.SelectTongNPLbyTenChuHang(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, txtMaSP.Text.Trim());
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                DinhMuc dm = new DinhMuc();
                dm.DinhMucSuDung = Convert.ToDecimal(row["Ton"]);
                dm.MaSanPham = txtMaSP.Text.Trim();
                dm.MaNguyenPhuLieu = row["Ma"].ToString();
                dm.DVT_ID = row["DVT_ID"].ToString();
                dm.TenNPL = row["Ten"].ToString();
                dmDangKy.DMCollection.Add(dm);
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            string dmsd = e.Row.Cells["DinhMucSuDung"].Text;
            string tlhh = e.Row.Cells["TyLeHaoHut"].Text;
            dmsd = dmsd == "" ? "0" : dmsd;
            tlhh = tlhh == "" ? "0" : tlhh;

            decimal dmChung = Convert.ToDecimal(dmsd) * (Convert.ToDecimal(tlhh) / 100 + 1);
            try
            {
                e.Row.Cells["DinhMucChung"].Text = dmChung.ToString();
            }
            catch (Exception ex) { }
        }

    }
}