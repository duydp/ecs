using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class GiayKiemTra : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public string SoGiayKiemTra { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string MaNguoiDuocCap { set; get; }
		public string TenNguoiDuocCap { set; get; }
		public string DiaDiemKiemTra { set; get; }
		public string MaCoQuanKT { set; get; }
		public string TenCoQuanKT { set; get; }
		public string LoaiGiay { set; get; }
		public string KetQuaKiemTra { set; get; }
		public int LoaiKB { set; get; }
		public string GuidStr { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public long SoTiepNhan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<GiayKiemTra> ConvertToCollection(IDataReader reader)
		{
			IList<GiayKiemTra> collection = new List<GiayKiemTra>();
			while (reader.Read())
			{
				GiayKiemTra entity = new GiayKiemTra();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayKiemTra"))) entity.SoGiayKiemTra = reader.GetString(reader.GetOrdinal("SoGiayKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiDuocCap"))) entity.MaNguoiDuocCap = reader.GetString(reader.GetOrdinal("MaNguoiDuocCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiDuocCap"))) entity.TenNguoiDuocCap = reader.GetString(reader.GetOrdinal("TenNguoiDuocCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemKiemTra"))) entity.DiaDiemKiemTra = reader.GetString(reader.GetOrdinal("DiaDiemKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanKT"))) entity.MaCoQuanKT = reader.GetString(reader.GetOrdinal("MaCoQuanKT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoQuanKT"))) entity.TenCoQuanKT = reader.GetString(reader.GetOrdinal("TenCoQuanKT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiGiay"))) entity.LoaiGiay = reader.GetString(reader.GetOrdinal("LoaiGiay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaKiemTra"))) entity.KetQuaKiemTra = reader.GetString(reader.GetOrdinal("KetQuaKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<GiayKiemTra> collection, long id)
        {
            foreach (GiayKiemTra item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GiayKiemTra VALUES(@TKMD_ID, @SoGiayKiemTra, @NgayDangKy, @MaNguoiDuocCap, @TenNguoiDuocCap, @DiaDiemKiemTra, @MaCoQuanKT, @TenCoQuanKT, @LoaiGiay, @KetQuaKiemTra, @LoaiKB, @GuidStr, @NgayTiepNhan, @TrangThaiXuLy, @SoTiepNhan)";
            string update = "UPDATE t_KDT_GiayKiemTra SET TKMD_ID = @TKMD_ID, SoGiayKiemTra = @SoGiayKiemTra, NgayDangKy = @NgayDangKy, MaNguoiDuocCap = @MaNguoiDuocCap, TenNguoiDuocCap = @TenNguoiDuocCap, DiaDiemKiemTra = @DiaDiemKiemTra, MaCoQuanKT = @MaCoQuanKT, TenCoQuanKT = @TenCoQuanKT, LoaiGiay = @LoaiGiay, KetQuaKiemTra = @KetQuaKiemTra, LoaiKB = @LoaiKB, GuidStr = @GuidStr, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, SoTiepNhan = @SoTiepNhan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GiayKiemTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, "SoGiayKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoQuanKT", SqlDbType.VarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiay", SqlDbType.VarChar, "LoaiGiay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, "SoGiayKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoQuanKT", SqlDbType.VarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiay", SqlDbType.VarChar, "LoaiGiay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GiayKiemTra VALUES(@TKMD_ID, @SoGiayKiemTra, @NgayDangKy, @MaNguoiDuocCap, @TenNguoiDuocCap, @DiaDiemKiemTra, @MaCoQuanKT, @TenCoQuanKT, @LoaiGiay, @KetQuaKiemTra, @LoaiKB, @GuidStr, @NgayTiepNhan, @TrangThaiXuLy, @SoTiepNhan)";
            string update = "UPDATE t_KDT_GiayKiemTra SET TKMD_ID = @TKMD_ID, SoGiayKiemTra = @SoGiayKiemTra, NgayDangKy = @NgayDangKy, MaNguoiDuocCap = @MaNguoiDuocCap, TenNguoiDuocCap = @TenNguoiDuocCap, DiaDiemKiemTra = @DiaDiemKiemTra, MaCoQuanKT = @MaCoQuanKT, TenCoQuanKT = @TenCoQuanKT, LoaiGiay = @LoaiGiay, KetQuaKiemTra = @KetQuaKiemTra, LoaiKB = @LoaiKB, GuidStr = @GuidStr, NgayTiepNhan = @NgayTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, SoTiepNhan = @SoTiepNhan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GiayKiemTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, "SoGiayKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoQuanKT", SqlDbType.VarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiay", SqlDbType.VarChar, "LoaiGiay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, "SoGiayKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, "MaNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, "TenNguoiDuocCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoQuanKT", SqlDbType.VarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiay", SqlDbType.VarChar, "LoaiGiay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static GiayKiemTra Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<GiayKiemTra> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<GiayKiemTra> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<GiayKiemTra> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<GiayKiemTra> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GiayKiemTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayKiemTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GiayKiemTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayKiemTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_GiayKiemTra_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertGiayKiemTra(long tKMD_ID, string soGiayKiemTra, DateTime ngayDangKy, string maNguoiDuocCap, string tenNguoiDuocCap, string diaDiemKiemTra, string maCoQuanKT, string tenCoQuanKT, string loaiGiay, string ketQuaKiemTra, int loaiKB, string guidStr, DateTime ngayTiepNhan, int trangThaiXuLy, long soTiepNhan)
		{
			GiayKiemTra entity = new GiayKiemTra();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoGiayKiemTra = soGiayKiemTra;
			entity.NgayDangKy = ngayDangKy;
			entity.MaNguoiDuocCap = maNguoiDuocCap;
			entity.TenNguoiDuocCap = tenNguoiDuocCap;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.MaCoQuanKT = maCoQuanKT;
			entity.TenCoQuanKT = tenCoQuanKT;
			entity.LoaiGiay = loaiGiay;
			entity.KetQuaKiemTra = ketQuaKiemTra;
			entity.LoaiKB = loaiKB;
			entity.GuidStr = guidStr;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTiepNhan = soTiepNhan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GiayKiemTra_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, SoGiayKiemTra);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, MaNguoiDuocCap);
			db.AddInParameter(dbCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, TenNguoiDuocCap);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@MaCoQuanKT", SqlDbType.VarChar, MaCoQuanKT);
			db.AddInParameter(dbCommand, "@TenCoQuanKT", SqlDbType.NVarChar, TenCoQuanKT);
			db.AddInParameter(dbCommand, "@LoaiGiay", SqlDbType.VarChar, LoaiGiay);
			db.AddInParameter(dbCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, KetQuaKiemTra);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<GiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayKiemTra item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateGiayKiemTra(long id, long tKMD_ID, string soGiayKiemTra, DateTime ngayDangKy, string maNguoiDuocCap, string tenNguoiDuocCap, string diaDiemKiemTra, string maCoQuanKT, string tenCoQuanKT, string loaiGiay, string ketQuaKiemTra, int loaiKB, string guidStr, DateTime ngayTiepNhan, int trangThaiXuLy, long soTiepNhan)
		{
			GiayKiemTra entity = new GiayKiemTra();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoGiayKiemTra = soGiayKiemTra;
			entity.NgayDangKy = ngayDangKy;
			entity.MaNguoiDuocCap = maNguoiDuocCap;
			entity.TenNguoiDuocCap = tenNguoiDuocCap;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.MaCoQuanKT = maCoQuanKT;
			entity.TenCoQuanKT = tenCoQuanKT;
			entity.LoaiGiay = loaiGiay;
			entity.KetQuaKiemTra = ketQuaKiemTra;
			entity.LoaiKB = loaiKB;
			entity.GuidStr = guidStr;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTiepNhan = soTiepNhan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GiayKiemTra_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, SoGiayKiemTra);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, MaNguoiDuocCap);
			db.AddInParameter(dbCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, TenNguoiDuocCap);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@MaCoQuanKT", SqlDbType.VarChar, MaCoQuanKT);
			db.AddInParameter(dbCommand, "@TenCoQuanKT", SqlDbType.NVarChar, TenCoQuanKT);
			db.AddInParameter(dbCommand, "@LoaiGiay", SqlDbType.VarChar, LoaiGiay);
			db.AddInParameter(dbCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, KetQuaKiemTra);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<GiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayKiemTra item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateGiayKiemTra(long id, long tKMD_ID, string soGiayKiemTra, DateTime ngayDangKy, string maNguoiDuocCap, string tenNguoiDuocCap, string diaDiemKiemTra, string maCoQuanKT, string tenCoQuanKT, string loaiGiay, string ketQuaKiemTra, int loaiKB, string guidStr, DateTime ngayTiepNhan, int trangThaiXuLy, long soTiepNhan)
		{
			GiayKiemTra entity = new GiayKiemTra();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoGiayKiemTra = soGiayKiemTra;
			entity.NgayDangKy = ngayDangKy;
			entity.MaNguoiDuocCap = maNguoiDuocCap;
			entity.TenNguoiDuocCap = tenNguoiDuocCap;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.MaCoQuanKT = maCoQuanKT;
			entity.TenCoQuanKT = tenCoQuanKT;
			entity.LoaiGiay = loaiGiay;
			entity.KetQuaKiemTra = ketQuaKiemTra;
			entity.LoaiKB = loaiKB;
			entity.GuidStr = guidStr;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTiepNhan = soTiepNhan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoGiayKiemTra", SqlDbType.NVarChar, SoGiayKiemTra);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaNguoiDuocCap", SqlDbType.VarChar, MaNguoiDuocCap);
			db.AddInParameter(dbCommand, "@TenNguoiDuocCap", SqlDbType.NVarChar, TenNguoiDuocCap);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@MaCoQuanKT", SqlDbType.VarChar, MaCoQuanKT);
			db.AddInParameter(dbCommand, "@TenCoQuanKT", SqlDbType.NVarChar, TenCoQuanKT);
			db.AddInParameter(dbCommand, "@LoaiGiay", SqlDbType.VarChar, LoaiGiay);
			db.AddInParameter(dbCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, KetQuaKiemTra);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<GiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayKiemTra item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteGiayKiemTra(long id)
		{
			GiayKiemTra entity = new GiayKiemTra();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<GiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayKiemTra item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}