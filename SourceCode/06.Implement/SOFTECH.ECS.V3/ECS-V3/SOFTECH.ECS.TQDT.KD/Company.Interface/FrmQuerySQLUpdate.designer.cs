﻿namespace Company.Interface
{
    partial class FrmQuerySQLUpdate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQuerySQLUpdate));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtQuery = new Janus.Windows.GridEX.EditControls.EditBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnQuery = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.grtop = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNgayDangKy = new System.Windows.Forms.DateTimePicker();
            this.txtNgayTiepNhan = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtReferenceid = new System.Windows.Forms.TextBox();
            this.txtTT_XuLy = new System.Windows.Forms.TextBox();
            this.txtSTN = new System.Windows.Forms.TextBox();
            this.btnUpdate = new Janus.Windows.EditControls.UIButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnSoTK = new System.Windows.Forms.RadioButton();
            this.rbtnSoTN = new System.Windows.Forms.RadioButton();
            this.rbtnID = new System.Windows.Forms.RadioButton();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.btnSelect = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTab = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.grdResult = new System.Windows.Forms.DataGridView();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtMessage = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grtop.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab)).BeginInit();
            this.uiTab.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.splitter1);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(801, 465);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Controls.Add(this.groupBox3);
            this.uiGroupBox1.Controls.Add(this.grtop);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(801, 258);
            this.uiGroupBox1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtQuery);
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 126);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(795, 129);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            // 
            // txtQuery
            // 
            this.txtQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQuery.Location = new System.Drawing.Point(3, 46);
            this.txtQuery.Multiline = true;
            this.txtQuery.Name = "txtQuery";
            this.txtQuery.Size = new System.Drawing.Size(789, 80);
            this.txtQuery.TabIndex = 11;
            this.txtQuery.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtQuery_KeyUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(789, 29);
            this.panel1.TabIndex = 10;
            // 
            // btnQuery
            // 
            this.btnQuery.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.btnQuery.ImageIndex = 1;
            this.btnQuery.ImageList = this.imageList1;
            this.btnQuery.Location = new System.Drawing.Point(4, 2);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(75, 23);
            this.btnQuery.TabIndex = 9;
            this.btnQuery.Text = "Thực hiện";
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "exit.png");
            this.imageList1.Images.SetKeyName(1, "bt_play.png");
            this.imageList1.Images.SetKeyName(2, "erase.png");
            // 
            // grtop
            // 
            this.grtop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grtop.Controls.Add(this.groupBox2);
            this.grtop.Controls.Add(this.groupBox1);
            this.grtop.Dock = System.Windows.Forms.DockStyle.Top;
            this.grtop.Location = new System.Drawing.Point(3, 8);
            this.grtop.Name = "grtop";
            this.grtop.Size = new System.Drawing.Size(795, 118);
            this.grtop.TabIndex = 13;
            this.grtop.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNgayDangKy);
            this.groupBox2.Controls.Add(this.txtNgayTiepNhan);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtReferenceid);
            this.groupBox2.Controls.Add(this.txtTT_XuLy);
            this.groupBox2.Controls.Add(this.txtSTN);
            this.groupBox2.Controls.Add(this.btnUpdate);
            this.groupBox2.Location = new System.Drawing.Point(201, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(592, 104);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cập nhật";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(353, 21);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Ngày đăng ký:";
            // 
            // txtNgayDangKy
            // 
            this.txtNgayDangKy.CustomFormat = "yyyy/MM/dd";
            this.txtNgayDangKy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtNgayDangKy.Location = new System.Drawing.Point(431, 18);
            this.txtNgayDangKy.Name = "txtNgayDangKy";
            this.txtNgayDangKy.Size = new System.Drawing.Size(113, 21);
            this.txtNgayDangKy.TabIndex = 24;
            this.txtNgayDangKy.Value = new System.DateTime(2012, 7, 31, 12, 53, 7, 0);
            // 
            // txtNgayTiepNhan
            // 
            this.txtNgayTiepNhan.CustomFormat = "yyyy/MM/dd";
            this.txtNgayTiepNhan.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtNgayTiepNhan.Location = new System.Drawing.Point(238, 18);
            this.txtNgayTiepNhan.Name = "txtNgayTiepNhan";
            this.txtNgayTiepNhan.Size = new System.Drawing.Size(113, 21);
            this.txtNgayTiepNhan.TabIndex = 23;
            this.txtNgayTiepNhan.Value = new System.DateTime(2012, 7, 31, 12, 53, 7, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(156, 47);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Referenceid:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(87, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Trạng thái xữ lý:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Số tiếp nhận:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(156, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(84, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Ngày tiếp nhận:";
            // 
            // txtReferenceid
            // 
            this.txtReferenceid.Location = new System.Drawing.Point(237, 44);
            this.txtReferenceid.Name = "txtReferenceid";
            this.txtReferenceid.Size = new System.Drawing.Size(310, 21);
            this.txtReferenceid.TabIndex = 6;
            this.txtReferenceid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTT_XuLy
            // 
            this.txtTT_XuLy.Location = new System.Drawing.Point(90, 44);
            this.txtTT_XuLy.Name = "txtTT_XuLy";
            this.txtTT_XuLy.Size = new System.Drawing.Size(60, 21);
            this.txtTT_XuLy.TabIndex = 5;
            this.txtTT_XuLy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSTN
            // 
            this.txtSTN.Location = new System.Drawing.Point(90, 19);
            this.txtSTN.Name = "txtSTN";
            this.txtSTN.Size = new System.Drawing.Size(60, 21);
            this.txtSTN.TabIndex = 3;
            this.txtSTN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUpdate.ImageIndex = 1;
            this.btnUpdate.ImageList = this.imageList1;
            this.btnUpdate.Location = new System.Drawing.Point(6, 74);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbtnSoTK);
            this.groupBox1.Controls.Add(this.rbtnSoTN);
            this.groupBox1.Controls.Add(this.rbtnID);
            this.groupBox1.Controls.Add(this.comboBoxTable);
            this.groupBox1.Controls.Add(this.btnSelect);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Location = new System.Drawing.Point(6, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(194, 103);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tìm";
            // 
            // rbtnSoTK
            // 
            this.rbtnSoTK.AutoSize = true;
            this.rbtnSoTK.Location = new System.Drawing.Point(129, 51);
            this.rbtnSoTK.Name = "rbtnSoTK";
            this.rbtnSoTK.Size = new System.Drawing.Size(52, 17);
            this.rbtnSoTK.TabIndex = 6;
            this.rbtnSoTK.Text = "Số TK";
            this.rbtnSoTK.UseVisualStyleBackColor = true;
            // 
            // rbtnSoTN
            // 
            this.rbtnSoTN.AutoSize = true;
            this.rbtnSoTN.Location = new System.Drawing.Point(67, 51);
            this.rbtnSoTN.Name = "rbtnSoTN";
            this.rbtnSoTN.Size = new System.Drawing.Size(53, 17);
            this.rbtnSoTN.TabIndex = 5;
            this.rbtnSoTN.Text = "Số TN";
            this.rbtnSoTN.UseVisualStyleBackColor = true;
            // 
            // rbtnID
            // 
            this.rbtnID.AutoSize = true;
            this.rbtnID.Checked = true;
            this.rbtnID.Location = new System.Drawing.Point(17, 51);
            this.rbtnID.Name = "rbtnID";
            this.rbtnID.Size = new System.Drawing.Size(36, 17);
            this.rbtnID.TabIndex = 4;
            this.rbtnID.TabStop = true;
            this.rbtnID.Text = "ID";
            this.rbtnID.UseVisualStyleBackColor = true;
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.Location = new System.Drawing.Point(44, 19);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(146, 21);
            this.comboBoxTable.TabIndex = 0;
            // 
            // btnSelect
            // 
            this.btnSelect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelect.Enabled = false;
            this.btnSelect.ImageIndex = 1;
            this.btnSelect.ImageList = this.imageList1;
            this.btnSelect.Location = new System.Drawing.Point(129, 74);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(61, 23);
            this.btnSelect.TabIndex = 2;
            this.btnSelect.Text = "Tìm";
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Table";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(11, 74);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(112, 21);
            this.txtID.TabIndex = 1;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            this.txtID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyUp);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.Location = new System.Drawing.Point(720, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 10;
            this.btnClose.Text = "Đóng";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 258);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(801, 5);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 434);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(801, 31);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // editBox1
            // 
            this.editBox1.Location = new System.Drawing.Point(6, 40);
            this.editBox1.Multiline = true;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(561, 109);
            this.editBox1.TabIndex = 0;
            // 
            // uiTab
            // 
            this.uiTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab.Location = new System.Drawing.Point(0, 263);
            this.uiTab.Name = "uiTab";
            this.uiTab.Size = new System.Drawing.Size(801, 171);
            this.uiTab.TabIndex = 5;
            this.uiTab.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.AutoScroll = true;
            this.uiTabPage1.Controls.Add(this.grdResult);
            this.uiTabPage1.Key = "tabPageGrid";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(799, 149);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Kết quả";
            // 
            // grdResult
            // 
            this.grdResult.AllowUserToAddRows = false;
            this.grdResult.AllowUserToDeleteRows = false;
            this.grdResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdResult.Location = new System.Drawing.Point(0, 0);
            this.grdResult.Name = "grdResult";
            this.grdResult.Size = new System.Drawing.Size(799, 149);
            this.grdResult.TabIndex = 0;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.AutoScroll = true;
            this.uiTabPage2.Controls.Add(this.txtMessage);
            this.uiTabPage2.Key = "tabPageMessage";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(635, 164);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông báo";
            // 
            // txtMessage
            // 
            this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessage.Location = new System.Drawing.Point(0, 0);
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMessage.Size = new System.Drawing.Size(635, 164);
            this.txtMessage.TabIndex = 0;
            this.txtMessage.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.AutoScroll = true;
            this.uiTabPage4.Key = "tabPageGrid";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(571, 215);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Kết quả";
            // 
            // FrmQuerySQLUpdate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(801, 465);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmQuerySQLUpdate";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thực hiện truy vấn SQL";
            this.Load += new System.EventHandler(this.FrmQuerySQL_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.grtop.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab)).EndInit();
            this.uiTab.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter splitter1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.UI.Tab.UITab uiTab;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private System.Windows.Forms.DataGridView grdResult;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMessage;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private System.Windows.Forms.ComboBox comboBoxTable;
        private System.Windows.Forms.TextBox txtSTN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtID;
        private Janus.Windows.EditControls.UIButton btnSelect;
        private Janus.Windows.EditControls.UIButton btnUpdate;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtReferenceid;
        private System.Windows.Forms.TextBox txtTT_XuLy;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton btnQuery;
        private System.Windows.Forms.DateTimePicker txtNgayTiepNhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker txtNgayDangKy;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rbtnSoTK;
        private System.Windows.Forms.RadioButton rbtnSoTN;
        private System.Windows.Forms.RadioButton rbtnID;
        private System.Windows.Forms.GroupBox grtop;
        private System.Windows.Forms.GroupBox groupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuery;
        private System.Windows.Forms.Panel panel1;
    }
}