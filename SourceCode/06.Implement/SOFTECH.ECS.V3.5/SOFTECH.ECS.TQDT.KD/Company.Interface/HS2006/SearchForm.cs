﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Windows.Forms;
using Customs.Component.Utility;
using Janus.Windows.GridEX;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Text;

namespace HaiQuan.HS
{
    public class SearchForm : CommonForm
	{
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
		private Janus.Windows.GridEX.EditControls.EditBox txtID;
		private Janus.Windows.GridEX.EditControls.EditBox txtDescription;
		private Janus.Windows.EditControls.UIButton btnSearch;
		private Janus.Windows.GridEX.EditControls.EditBox txtDescription02;
		private Janus.Windows.GridEX.GridEX gridEX1;
		private Janus.Windows.EditControls.UIComboBox cbOperator;
		private Janus.Windows.EditControls.UIComboBox cbSearchWhat;
		private System.Windows.Forms.Label lblID;
		private System.Windows.Forms.Label lblSearchWhat;
		private System.Windows.Forms.Label lblDescription;
		private System.Windows.Forms.Label lblTitle;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem mnuGroupFavourite;
		private System.Windows.Forms.MenuItem mnuItemFavourite;
		private IContainer components = null;

		public SearchForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
			Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
			Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
			Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
			Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
			Janus.Windows.GridEX.GridEXLayout gridEXLayout1 = new Janus.Windows.GridEX.GridEXLayout();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SearchForm));
			this.txtID = new Janus.Windows.GridEX.EditControls.EditBox();
			this.lblID = new System.Windows.Forms.Label();
			this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
			this.lblTitle = new System.Windows.Forms.Label();
			this.txtDescription = new Janus.Windows.GridEX.EditControls.EditBox();
			this.cbSearchWhat = new Janus.Windows.EditControls.UIComboBox();
			this.lblSearchWhat = new System.Windows.Forms.Label();
			this.cbOperator = new Janus.Windows.EditControls.UIComboBox();
			this.gridEX1 = new Janus.Windows.GridEX.GridEX();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.mnuGroupFavourite = new System.Windows.Forms.MenuItem();
			this.mnuItemFavourite = new System.Windows.Forms.MenuItem();
			this.txtDescription02 = new Janus.Windows.GridEX.EditControls.EditBox();
			this.btnSearch = new Janus.Windows.EditControls.UIButton();
			this.lblDescription = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
			this.uiGroupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
			this.SuspendLayout();
			// 
			// txtID
			// 
			this.txtID.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.txtID.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.txtID.Location = new System.Drawing.Point(8, 24);
			this.txtID.Name = "txtID";
			this.txtID.Size = new System.Drawing.Size(88, 21);
			this.txtID.TabIndex = 2;
			this.txtID.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
			this.txtID.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			// 
			// lblID
			// 
			this.lblID.AutoSize = true;
			this.lblID.BackColor = System.Drawing.Color.Transparent;
			this.lblID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblID.Location = new System.Drawing.Point(8, 8);
			this.lblID.Name = "lblID";
			this.lblID.Size = new System.Drawing.Size(93, 17);
			this.lblID.TabIndex = 3;
			this.lblID.Text = "Mã số hàng hóa";
			// 
			// uiGroupBox1
			// 
			this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
			this.uiGroupBox1.Controls.Add(this.lblTitle);
			this.uiGroupBox1.Controls.Add(this.txtDescription);
			this.uiGroupBox1.Controls.Add(this.cbSearchWhat);
			this.uiGroupBox1.Controls.Add(this.lblSearchWhat);
			this.uiGroupBox1.Controls.Add(this.cbOperator);
			this.uiGroupBox1.Controls.Add(this.gridEX1);
			this.uiGroupBox1.Controls.Add(this.txtDescription02);
			this.uiGroupBox1.Controls.Add(this.btnSearch);
			this.uiGroupBox1.Controls.Add(this.lblDescription);
			this.uiGroupBox1.Controls.Add(this.txtID);
			this.uiGroupBox1.Controls.Add(this.lblID);
			this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
			this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
			this.uiGroupBox1.Name = "uiGroupBox1";
			this.uiGroupBox1.Size = new System.Drawing.Size(536, 318);
			this.uiGroupBox1.TabIndex = 4;
			this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
			// 
			// lblTitle
			// 
			this.lblTitle.AutoSize = true;
			this.lblTitle.BackColor = System.Drawing.Color.Transparent;
			this.lblTitle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblTitle.Location = new System.Drawing.Point(8, 16);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(50, 26);
			this.lblTitle.TabIndex = 15;
			this.lblTitle.Text = "Title";
			this.lblTitle.Visible = false;
			// 
			// txtDescription
			// 
			this.txtDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtDescription.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.txtDescription.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.txtDescription.Location = new System.Drawing.Point(168, 24);
			this.txtDescription.Name = "txtDescription";
			this.txtDescription.Size = new System.Drawing.Size(96, 21);
			this.txtDescription.TabIndex = 8;
			this.txtDescription.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
			this.txtDescription.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			// 
			// cbSearchWhat
			// 
			this.cbSearchWhat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cbSearchWhat.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
			uiComboBoxItem1.FormatStyle.Alpha = 0;
			uiComboBoxItem1.Text = "Chính xác từng từ";
			uiComboBoxItem1.Value = 0;
			uiComboBoxItem2.FormatStyle.Alpha = 0;
			uiComboBoxItem2.Text = "Chính xác một trong các từ";
			uiComboBoxItem3.FormatStyle.Alpha = 0;
			uiComboBoxItem3.Text = "Chính xác cả cụm từ";
			this.cbSearchWhat.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
																								 uiComboBoxItem1,
																								 uiComboBoxItem2,
																								 uiComboBoxItem3});
			this.cbSearchWhat.Location = new System.Drawing.Point(272, 24);
			this.cbSearchWhat.Name = "cbSearchWhat";
			this.cbSearchWhat.Size = new System.Drawing.Size(168, 21);
			this.cbSearchWhat.TabIndex = 9;
			this.cbSearchWhat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
			// 
			// lblSearchWhat
			// 
			this.lblSearchWhat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.lblSearchWhat.AutoSize = true;
			this.lblSearchWhat.BackColor = System.Drawing.Color.Transparent;
			this.lblSearchWhat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblSearchWhat.Location = new System.Drawing.Point(272, 8);
			this.lblSearchWhat.Name = "lblSearchWhat";
			this.lblSearchWhat.Size = new System.Drawing.Size(133, 17);
			this.lblSearchWhat.TabIndex = 14;
			this.lblSearchWhat.Text = "Phương pháp tìm kiếm";
			// 
			// cbOperator
			// 
			this.cbOperator.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
			uiComboBoxItem4.FormatStyle.Alpha = 0;
			uiComboBoxItem4.Text = "VÀ";
			uiComboBoxItem4.Value = "AND";
			uiComboBoxItem5.FormatStyle.Alpha = 0;
			uiComboBoxItem5.Text = "HOẶC";
			uiComboBoxItem5.Value = "OR";
			this.cbOperator.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
																							   uiComboBoxItem4,
																							   uiComboBoxItem5});
			this.cbOperator.Location = new System.Drawing.Point(104, 24);
			this.cbOperator.Name = "cbOperator";
			this.cbOperator.Size = new System.Drawing.Size(56, 21);
			this.cbOperator.TabIndex = 13;
			this.cbOperator.TextAlignment = Janus.Windows.EditControls.TextAlignment.Center;
			this.cbOperator.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
			// 
			// gridEX1
			// 
			this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
			this.gridEX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.gridEX1.ColumnAutoResize = true;
			this.gridEX1.ContextMenu = this.contextMenu1;
			gridEXLayout1.LayoutString = "<GridEXLayoutData><RootTable><Columns Collection=\"true\"><Column0 ID=\"ID\"><Caption" +
				">ID</Caption><DataMember>ID</DataMember><Key>ID</Key><Position>0</Position><Visi" +
				"ble>False</Visible><Width>51</Width></Column0><Column1 ID=\"ID01\"><DataMember>ID0" +
				"1</DataMember><Key>ID01</Key><Position>1</Position><Width>56</Width></Column1><C" +
				"olumn2 ID=\"ID02\"><DataMember>ID02</DataMember><Key>ID02</Key><Position>2</Positi" +
				"on><Width>28</Width></Column2><Column3 ID=\"ID03\"><DataMember>ID03</DataMember><K" +
				"ey>ID03</Key><Position>3</Position><Width>25</Width></Column3><Column4 ID=\"ID04\"" +
				"><DataMember>ID04</DataMember><Key>ID04</Key><Position>4</Position><Width>25</Wi" +
				"dth></Column4><Column5 ID=\"Description\"><Caption>Mô tả hàng hóa</Caption><DataMe" +
				"mber>Description</DataMember><Key>Description</Key><Position>5</Position><Width>" +
				"308</Width></Column5><Column6 ID=\"Percent\"><Caption>TS (%)</Caption><DataMember>" +
				"Percent</DataMember><Key>Percent</Key><Position>6</Position><TextAlignment>Cente" +
				"r</TextAlignment><Width>58</Width></Column6></Columns><FormatConditions Collecti" +
				"on=\"true\"><Condition0 ID=\"FormatCondition1\"><ColIndex>1</ColIndex><ConditionOper" +
				"ator>IsEmpty</ConditionOperator><FormatStyleIndex>0</FormatStyleIndex><PreviewRo" +
				"wFormatStyleIndex>0</PreviewRowFormatStyleIndex><TargetColIndex>1</TargetColInde" +
				"x><Value1 /><Key>FormatCondition1</Key><FilterCondition><ColumnIndex>4</ColumnIn" +
				"dex><ConditionOperator>IsEmpty</ConditionOperator><Conditions Collection=\"true\">" +
				"<FilterCondition0><ColumnIndex>3</ColumnIndex><ConditionOperator>IsEmpty</Condit" +
				"ionOperator><LogicalOperator>And</LogicalOperator></FilterCondition0><FilterCond" +
				"ition1><ColumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator" +
				"><LogicalOperator>And</LogicalOperator></FilterCondition1><FilterCondition2><Col" +
				"umnIndex>1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOperator><Logica" +
				"lOperator>And</LogicalOperator></FilterCondition2></Conditions></FilterCondition" +
				"></Condition0><Condition1 ID=\"FormatCondition2\"><ColIndex>2</ColIndex><FormatSty" +
				"leIndex>0</FormatStyleIndex><PreviewRowFormatStyleIndex>0</PreviewRowFormatStyle" +
				"Index><TargetColIndex>2</TargetColIndex><Key>FormatCondition2</Key><FilterCondit" +
				"ion><ColumnIndex>1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOperator" +
				"><Conditions Collection=\"true\"><FilterCondition0><ColumnIndex>2</ColumnIndex><Co" +
				"nditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator" +
				"></FilterCondition0><FilterCondition1><ColumnIndex>3</ColumnIndex><ConditionOper" +
				"ator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCo" +
				"ndition1><FilterCondition2><ColumnIndex>4</ColumnIndex><ConditionOperator>IsEmpt" +
				"y</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition2></" +
				"Conditions></FilterCondition></Condition1><Condition2 ID=\"FormatCondition3\"><Col" +
				"Index>3</ColIndex><FormatStyleIndex>0</FormatStyleIndex><PreviewRowFormatStyleIn" +
				"dex>0</PreviewRowFormatStyleIndex><TargetColIndex>3</TargetColIndex><Key>FormatC" +
				"ondition3</Key><FilterCondition><ColumnIndex>1</ColumnIndex><ConditionOperator>N" +
				"otIsEmpty</ConditionOperator><Conditions Collection=\"true\"><FilterCondition0><Co" +
				"lumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalO" +
				"perator>And</LogicalOperator></FilterCondition0><FilterCondition1><ColumnIndex>3" +
				"</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And" +
				"</LogicalOperator></FilterCondition1><FilterCondition2><ColumnIndex>4</ColumnInd" +
				"ex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOp" +
				"erator></FilterCondition2></Conditions></FilterCondition></Condition2><Condition" +
				"3 ID=\"FormatCondition4\"><ColIndex>4</ColIndex><FormatStyleIndex>0</FormatStyleIn" +
				"dex><PreviewRowFormatStyleIndex>0</PreviewRowFormatStyleIndex><TargetColIndex>4<" +
				"/TargetColIndex><Key>FormatCondition4</Key><FilterCondition><ColumnIndex>1</Colu" +
				"mnIndex><ConditionOperator>NotIsEmpty</ConditionOperator><Conditions Collection=" +
				"\"true\"><FilterCondition0><ColumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty<" +
				"/ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition0><Fil" +
				"terCondition1><ColumnIndex>3</ColumnIndex><ConditionOperator>IsEmpty</ConditionO" +
				"perator><LogicalOperator>And</LogicalOperator></FilterCondition1><FilterConditio" +
				"n2><ColumnIndex>4</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><Lo" +
				"gicalOperator>And</LogicalOperator></FilterCondition2></Conditions></FilterCondi" +
				"tion></Condition3><Condition4 ID=\"FormatCondition5\"><ColIndex>5</ColIndex><Forma" +
				"tStyleIndex>0</FormatStyleIndex><PreviewRowFormatStyleIndex>0</PreviewRowFormatS" +
				"tyleIndex><TargetColIndex>5</TargetColIndex><Key>FormatCondition5</Key><FilterCo" +
				"ndition><ColumnIndex>1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOper" +
				"ator><Conditions Collection=\"true\"><FilterCondition0><ColumnIndex>2</ColumnIndex" +
				"><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOper" +
				"ator></FilterCondition0><FilterCondition1><ColumnIndex>3</ColumnIndex><Condition" +
				"Operator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator></Filt" +
				"erCondition1><FilterCondition2><ColumnIndex>4</ColumnIndex><ConditionOperator>Is" +
				"Empty</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition" +
				"2></Conditions></FilterCondition></Condition4><Condition5 ID=\"FormatCondition6\">" +
				"<ColIndex>6</ColIndex><FormatStyleIndex>0</FormatStyleIndex><PreviewRowFormatSty" +
				"leIndex>0</PreviewRowFormatStyleIndex><TargetColIndex>6</TargetColIndex><Key>For" +
				"matCondition6</Key><FilterCondition><ColumnIndex>1</ColumnIndex><ConditionOperat" +
				"or>NotIsEmpty</ConditionOperator><Conditions Collection=\"true\"><FilterCondition0" +
				"><ColumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><Logi" +
				"calOperator>And</LogicalOperator></FilterCondition0><FilterCondition1><ColumnInd" +
				"ex>3</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator" +
				">And</LogicalOperator></FilterCondition1><FilterCondition2><ColumnIndex>4</Colum" +
				"nIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</Logic" +
				"alOperator></FilterCondition2></Conditions></FilterCondition></Condition5></Form" +
				"atConditions><GroupCondition ID=\"\" /></RootTable><FormatStyles Collection=\"true\"" +
				"><Style0 ID=\"FormatStyle1\"><BackColor>255, 255, 192</BackColor><FontBold>True</F" +
				"ontBold><Key>FormatStyle1</Key></Style0></FormatStyles></GridEXLayoutData>";
			this.gridEX1.DesignTimeLayout = gridEXLayout1;
			this.gridEX1.GroupByBoxVisible = false;
			this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
			this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
			this.gridEX1.Location = new System.Drawing.Point(8, 56);
			this.gridEX1.Name = "gridEX1";
			this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
			this.gridEX1.Size = new System.Drawing.Size(520, 192);
			this.gridEX1.TabIndex = 12;
			this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			this.gridEX1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridEX1_RowDoubleClick);
			this.gridEX1.SelectionChanged += new System.EventHandler(this.gridEX1_SelectionChanged);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.mnuGroupFavourite,
																						 this.mnuItemFavourite});
			// 
			// mnuGroupFavourite
			// 
			this.mnuGroupFavourite.Index = 0;
			this.mnuGroupFavourite.Text = ">> Đưa vào danh sách thường dùng (Cả nhóm)";
			this.mnuGroupFavourite.Click += new System.EventHandler(this.mnuGroupFavourite_Click);
			// 
			// mnuItemFavourite
			// 
			this.mnuItemFavourite.Index = 1;
			this.mnuItemFavourite.Text = ">> Đưa vào danh sách thường dùng (Dòng này)";
			this.mnuItemFavourite.Click += new System.EventHandler(this.mnuItemFavourite_Click);
			// 
			// txtDescription02
			// 
			this.txtDescription02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtDescription02.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.txtDescription02.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.txtDescription02.Location = new System.Drawing.Point(8, 256);
			this.txtDescription02.Multiline = true;
			this.txtDescription02.Name = "txtDescription02";
			this.txtDescription02.ReadOnly = true;
			this.txtDescription02.Size = new System.Drawing.Size(520, 56);
			this.txtDescription02.TabIndex = 11;
			this.txtDescription02.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
			this.txtDescription02.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			// 
			// btnSearch
			// 
			this.btnSearch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnSearch.Location = new System.Drawing.Point(448, 24);
			this.btnSearch.Name = "btnSearch";
			this.btnSearch.Size = new System.Drawing.Size(75, 23);
			this.btnSearch.TabIndex = 10;
			this.btnSearch.Text = "Tìm kiếm";
			this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
			this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
			// 
			// lblDescription
			// 
			this.lblDescription.AutoSize = true;
			this.lblDescription.BackColor = System.Drawing.Color.Transparent;
			this.lblDescription.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.lblDescription.Location = new System.Drawing.Point(168, 8);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(92, 17);
			this.lblDescription.TabIndex = 7;
			this.lblDescription.Text = "Mô tả hàng hóa";
			// 
			// SearchForm
			// 
			this.AcceptButton = this.btnSearch;
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(536, 318);
			this.Controls.Add(this.uiGroupBox1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SearchForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Tag = "SearchForm";
			this.Text = "Tra cứu biểu thuế";
			this.Load += new System.EventHandler(this.SearchForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
			this.uiGroupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		public void SearchByChapter(string chapter)
		{
			Item item = new Item();
			gridEX1.DataSource = item.SearchByChapter(chapter).Tables[0];
		}

		public void SearchByFavourite()
		{
			Item item = new Item();
			gridEX1.DataSource = item.SearchByFavourite().Tables[0];			
		}

		public void Search(string id, string description, string searchOperator, HSSearchWhat searchWhat)
		{
			Item item = new Item();			
			DataSet ds = item.Search(id, description, searchOperator, searchWhat);
			gridEX1.DataSource = ds.Tables[0];
		}
		
		public void SetSearchObjectVisible(bool val, string title)
		{
			this.lblID.Visible = val;
			this.lblDescription.Visible = val;
			this.lblSearchWhat.Visible = val;
			this.txtID.Visible = val;
			this.txtDescription.Visible = val;
			this.cbOperator.Visible = val;
			this.cbSearchWhat.Visible = val;
			this.btnSearch.Visible = val;
			this.lblTitle.Visible = !val;
			this.lblTitle.Text = title;
		}

		private void SearchForm_Load(object sender, EventArgs e)
		{
			cbOperator.SelectedIndex = 0;
			cbSearchWhat.SelectedIndex = 0;
		}

		private void btnSearch_Click(object sender, System.EventArgs e)
		{
			HSSearchWhat searchWhat = HSSearchWhat.AllWord;
			if (cbSearchWhat.SelectedIndex == 1)
			{
				searchWhat = HSSearchWhat.AnyWord;
			}
			else if (cbSearchWhat.SelectedIndex == 2)
			{
				searchWhat = HSSearchWhat.Exact;
			}
			this.Search(txtID.Text, txtDescription.Text, cbOperator.SelectedValue.ToString(), searchWhat);
		}

		private void gridEX1_SelectionChanged(object sender, System.EventArgs e)
		{
			try
			{
				txtDescription02.Text = gridEX1.GetRow().Cells["Description"].Text;
			}
			catch
			{
			}
		}

		private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
		{
			string ID = e.Row.Cells["ID"].Text.ToString().Substring(0,4);
			SearchForm f = new SearchForm();
			f.SetSearchObjectVisible(false, "Chương " + ID.Substring(0, 2));
			f.Search(ID, "", "AND", HSSearchWhat.AllWord);			
			f.FormBorderStyle = FormBorderStyle.FixedDialog;
			f.MinimizeBox = false;
			f.MaximizeBox = false;
			f.ShowDialog(this);
		}

		private void mnuGroupFavourite_Click(object sender, System.EventArgs e)
		{
			try
			{
				Item item = new Item();
				item.AddFavourite(gridEX1.GetRow().Cells["ID"].Text, true);
				MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void mnuItemFavourite_Click(object sender, System.EventArgs e)
		{
			try
			{
				Item item = new Item();
				item.AddFavourite(gridEX1.GetRow().Cells["ID"].Text, false);
				MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		
		}

		private void mnuClearGroupFavourite_Click(object sender, System.EventArgs e)
		{
			try
			{
				DialogResult ret = MessageBox.Show("Bạn có muốn xóa nhóm này khỏi danh sách thường dùng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ret == DialogResult.Yes)
				{
					Item item = new Item();
					item.ClearFavourite(gridEX1.GetRow().Cells["ID"].Text, true);
					MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.SearchByFavourite();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		
		}

		private void mnuClearItemFavourite_Click(object sender, System.EventArgs e)
		{
			try
			{
				DialogResult ret = MessageBox.Show("Bạn có muốn xóa dòng này khỏi danh sách thường dùng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ret == DialogResult.Yes)
				{
					Item item = new Item();
					item.ClearFavourite(gridEX1.GetRow().Cells["ID"].Text, false);
					MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.SearchByFavourite();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}		
		}

		private void mnuClearAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				DialogResult ret = MessageBox.Show("Bạn có muốn xóa tất cả khỏi danh sách thường dùng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ret == DialogResult.Yes)
				{
					Item item = new Item();
					item.ClearAllFavourite();
					MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.SearchByFavourite();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

		}
	}
}