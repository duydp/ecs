using System;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

namespace HaiQuan.HS
{
	public class Item
	{
		
		public void AddFavourite(string id, bool isGroup)
		{
			string query = string.Empty;
			if (isGroup)
			{
				string idGroup = id.Substring(0, 4);
				query = string.Format("UPDATE t_Items SET IsFavourite = Yes WHERE LEFT(ID, 4) = '{0}' ", idGroup);
			}
			else
			{
				query = string.Format("UPDATE t_Items SET IsFavourite = Yes WHERE ID = '{0}' ", id);
			}
			
			OleDbHelper.ExecuteNonQuery(Global.ConnectionString, CommandType.Text, query);
		
		}
		
		public void ClearFavourite(string id, bool isGroup)
		{
			string query = string.Empty;
			if (isGroup)
			{
				string idGroup = id.Substring(0, 4);
				query = string.Format("UPDATE t_Items SET IsFavourite = No WHERE LEFT(ID, 4) = '{0}' ", idGroup);
			}
			else
			{
				query = string.Format("UPDATE t_Items SET IsFavourite = No WHERE ID = '{0}' ", id);
			}
			
			OleDbHelper.ExecuteNonQuery(Global.ConnectionString, CommandType.Text, query);
		}

		public void ClearAllFavourite()
		{
			string query = string.Empty;
			query = "UPDATE t_Items SET IsFavourite = No WHERE IsFavourite = Yes";
			OleDbHelper.ExecuteNonQuery(Global.ConnectionString, CommandType.Text, query);		
		}

		public DataSet SearchByChapter(string chapter)
		{
			string query = string.Empty;
			query = string.Format("SELECT * FROM t_Items WHERE LEFT(ID, 2) = '{0}' ORDER BY ID", chapter);
			return OleDbHelper.ExecuteDataset(Global.ConnectionString, CommandType.Text, query);
		}

		public DataSet SelectExportItems()
		{
			string query = string.Empty; 
			query = "SELECT * FROM t_Items_Export ORDER BY ID";
			return OleDbHelper.ExecuteDataset(Global.ConnectionString, CommandType.Text, query);
		}


		public DataSet SearchByFavourite()
		{
			string query = string.Empty;
			query = "SELECT * FROM t_Items WHERE IsFavourite = Yes ORDER BY ID";
			return OleDbHelper.ExecuteDataset(Global.ConnectionString, CommandType.Text, query);			
		}
		
		//-----------------------------------------------------------------------------------------
		public DataSet Search(string id, string description, string searchOperator, HSSearchWhat searchWhat)
		{
			StringBuilder query = new StringBuilder(string.Empty);			
			query.Append("SELECT ");
			query.Append("* ");
			query.Append("FROM t_Items ");
			query.Append("WHERE ");
			if (id.Length > 0)
			{
				query.Append(string.Format("(LEFT(ID, 4)+ID02+ID03+ID04 LIKE '{0}%') ", id));				
			}
			else
			{
				query.Append("(1 = 1)");
			}

			query.Append(" " + searchOperator + " ");

			if (description.Length > 0)
			{
				
				string[] words;	
				switch (searchWhat)
				{
					case HSSearchWhat.AllWord:
						words = description.Split(' ');
						foreach (string word in words)
						{
							query.Append(string.Format("(Description LIKE '%{0}%') AND ", word));
						}
						// Remove last AND in sql query.
						query.Remove(query.Length - 5, 5);
						break;
					case HSSearchWhat.AnyWord:
						words = description.Split(' ');
						query.Append("(");
						foreach (string word in words)
						{
							query.Append(string.Format("(Description LIKE '%{0}%') OR ", word));
						}
						// Remove last OR in sql query.
						query.Remove(query.Length - 4, 4);
						query.Append(")");
						break;
					case HSSearchWhat.Exact:
						query.Append(string.Format("(Description LIKE '%{0}%')", description));
						break;
				}
			}
			else
			{
				query.Append("(2 = 2)");
			}
			query.Append(" ORDER BY ID");
			try
			{
				return OleDbHelper.ExecuteDataset(Global.ConnectionString, CommandType.Text, query.ToString());
			}
			catch
			{
				throw new Exception(query.ToString());
			}
		}
	}
}
