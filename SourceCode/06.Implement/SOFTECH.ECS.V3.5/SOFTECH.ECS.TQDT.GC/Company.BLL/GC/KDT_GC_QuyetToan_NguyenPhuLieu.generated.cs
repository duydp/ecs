﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_QuyetToan_NguyenPhuLieu : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HopDong_ID { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal LuongNhapDK { set; get; }
		public decimal TriGiaNhapDK { set; get; }
		public decimal LuongCungUngDK { set; get; }
		public decimal TriGiaCungUngDK { set; get; }
		public decimal LuongSuDungDK { set; get; }
		public decimal TriGiaSuDungDK { set; get; }
		public decimal LuongNhapTK { set; get; }
		public decimal TriGiaNhapTK { set; get; }
		public decimal LuongCungUngTK { set; get; }
		public decimal TriGiaCungUngTK { set; get; }
		public decimal LuongSuDungTK { set; get; }
		public decimal TriGiaSuDungTK { set; get; }
		public decimal LuongNhapNK { set; get; }
		public decimal TriGiaNhapNK { set; get; }
		public decimal LuongCungUngNK { set; get; }
		public decimal TriGiaCungUngNK { set; get; }
		public decimal LuongSuDungNK { set; get; }
		public decimal TriGiaSuDungNK { set; get; }
		public decimal LuongTonCK { set; get; }
		public decimal TriGiaTonCK { set; get; }
		public string NamQT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_QuyetToan_NguyenPhuLieu> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_QuyetToan_NguyenPhuLieu> collection = new List<KDT_GC_QuyetToan_NguyenPhuLieu>();
			while (reader.Read())
			{
				KDT_GC_QuyetToan_NguyenPhuLieu entity = new KDT_GC_QuyetToan_NguyenPhuLieu();
				//if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapDK"))) entity.LuongNhapDK = reader.GetDecimal(reader.GetOrdinal("LuongNhapDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNhapDK"))) entity.TriGiaNhapDK = reader.GetDecimal(reader.GetOrdinal("TriGiaNhapDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUngDK"))) entity.LuongCungUngDK = reader.GetDecimal(reader.GetOrdinal("LuongCungUngDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaCungUngDK"))) entity.TriGiaCungUngDK = reader.GetDecimal(reader.GetOrdinal("TriGiaCungUngDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSuDungDK"))) entity.LuongSuDungDK = reader.GetDecimal(reader.GetOrdinal("LuongSuDungDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDungDK"))) entity.TriGiaSuDungDK = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDungDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapTK"))) entity.LuongNhapTK = reader.GetDecimal(reader.GetOrdinal("LuongNhapTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNhapTK"))) entity.TriGiaNhapTK = reader.GetDecimal(reader.GetOrdinal("TriGiaNhapTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUngTK"))) entity.LuongCungUngTK = reader.GetDecimal(reader.GetOrdinal("LuongCungUngTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaCungUngTK"))) entity.TriGiaCungUngTK = reader.GetDecimal(reader.GetOrdinal("TriGiaCungUngTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSuDungTK"))) entity.LuongSuDungTK = reader.GetDecimal(reader.GetOrdinal("LuongSuDungTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDungTK"))) entity.TriGiaSuDungTK = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDungTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapNK"))) entity.LuongNhapNK = reader.GetDecimal(reader.GetOrdinal("LuongNhapNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaNhapNK"))) entity.TriGiaNhapNK = reader.GetDecimal(reader.GetOrdinal("TriGiaNhapNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUngNK"))) entity.LuongCungUngNK = reader.GetDecimal(reader.GetOrdinal("LuongCungUngNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaCungUngNK"))) entity.TriGiaCungUngNK = reader.GetDecimal(reader.GetOrdinal("TriGiaCungUngNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongSuDungNK"))) entity.LuongSuDungNK = reader.GetDecimal(reader.GetOrdinal("LuongSuDungNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaSuDungNK"))) entity.TriGiaSuDungNK = reader.GetDecimal(reader.GetOrdinal("TriGiaSuDungNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCK"))) entity.LuongTonCK = reader.GetDecimal(reader.GetOrdinal("LuongTonCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTonCK"))) entity.TriGiaTonCK = reader.GetDecimal(reader.GetOrdinal("TriGiaTonCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamQT"))) entity.NamQT = reader.GetString(reader.GetOrdinal("NamQT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_QuyetToan_NguyenPhuLieu> collection, long id)
        {
            foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_QuyetToan_NguyenPhuLieu VALUES(@HopDong_ID, @MaNPL, @TenNPL, @MaHS, @DVT_ID, @LuongNhapDK, @TriGiaNhapDK, @LuongCungUngDK, @TriGiaCungUngDK, @LuongSuDungDK, @TriGiaSuDungDK, @LuongNhapTK, @TriGiaNhapTK, @LuongCungUngTK, @TriGiaCungUngTK, @LuongSuDungTK, @TriGiaSuDungTK, @LuongNhapNK, @TriGiaNhapNK, @LuongCungUngNK, @TriGiaCungUngNK, @LuongSuDungNK, @TriGiaSuDungNK, @LuongTonCK, @TriGiaTonCK, @NamQT)";
            string update = "UPDATE t_KDT_GC_QuyetToan_NguyenPhuLieu SET HopDong_ID = @HopDong_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongNhapDK = @LuongNhapDK, TriGiaNhapDK = @TriGiaNhapDK, LuongCungUngDK = @LuongCungUngDK, TriGiaCungUngDK = @TriGiaCungUngDK, LuongSuDungDK = @LuongSuDungDK, TriGiaSuDungDK = @TriGiaSuDungDK, LuongNhapTK = @LuongNhapTK, TriGiaNhapTK = @TriGiaNhapTK, LuongCungUngTK = @LuongCungUngTK, TriGiaCungUngTK = @TriGiaCungUngTK, LuongSuDungTK = @LuongSuDungTK, TriGiaSuDungTK = @TriGiaSuDungTK, LuongNhapNK = @LuongNhapNK, TriGiaNhapNK = @TriGiaNhapNK, LuongCungUngNK = @LuongCungUngNK, TriGiaCungUngNK = @TriGiaCungUngNK, LuongSuDungNK = @LuongSuDungNK, TriGiaSuDungNK = @TriGiaSuDungNK, LuongTonCK = @LuongTonCK, TriGiaTonCK = @TriGiaTonCK, NamQT = @NamQT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_QuyetToan_NguyenPhuLieu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUngDK", SqlDbType.Decimal, "LuongCungUngDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, "TriGiaCungUngDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUngTK", SqlDbType.Decimal, "LuongCungUngTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, "TriGiaCungUngTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUngNK", SqlDbType.Decimal, "LuongCungUngNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, "TriGiaCungUngNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUngDK", SqlDbType.Decimal, "LuongCungUngDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, "TriGiaCungUngDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUngTK", SqlDbType.Decimal, "LuongCungUngTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, "TriGiaCungUngTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUngNK", SqlDbType.Decimal, "LuongCungUngNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, "TriGiaCungUngNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_QuyetToan_NguyenPhuLieu VALUES(@HopDong_ID, @MaNPL, @TenNPL, @MaHS, @DVT_ID, @LuongNhapDK, @TriGiaNhapDK, @LuongCungUngDK, @TriGiaCungUngDK, @LuongSuDungDK, @TriGiaSuDungDK, @LuongNhapTK, @TriGiaNhapTK, @LuongCungUngTK, @TriGiaCungUngTK, @LuongSuDungTK, @TriGiaSuDungTK, @LuongNhapNK, @TriGiaNhapNK, @LuongCungUngNK, @TriGiaCungUngNK, @LuongSuDungNK, @TriGiaSuDungNK, @LuongTonCK, @TriGiaTonCK, @NamQT)";
            string update = "UPDATE t_KDT_GC_QuyetToan_NguyenPhuLieu SET HopDong_ID = @HopDong_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongNhapDK = @LuongNhapDK, TriGiaNhapDK = @TriGiaNhapDK, LuongCungUngDK = @LuongCungUngDK, TriGiaCungUngDK = @TriGiaCungUngDK, LuongSuDungDK = @LuongSuDungDK, TriGiaSuDungDK = @TriGiaSuDungDK, LuongNhapTK = @LuongNhapTK, TriGiaNhapTK = @TriGiaNhapTK, LuongCungUngTK = @LuongCungUngTK, TriGiaCungUngTK = @TriGiaCungUngTK, LuongSuDungTK = @LuongSuDungTK, TriGiaSuDungTK = @TriGiaSuDungTK, LuongNhapNK = @LuongNhapNK, TriGiaNhapNK = @TriGiaNhapNK, LuongCungUngNK = @LuongCungUngNK, TriGiaCungUngNK = @TriGiaCungUngNK, LuongSuDungNK = @LuongSuDungNK, TriGiaSuDungNK = @TriGiaSuDungNK, LuongTonCK = @LuongTonCK, TriGiaTonCK = @TriGiaTonCK, NamQT = @NamQT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_QuyetToan_NguyenPhuLieu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUngDK", SqlDbType.Decimal, "LuongCungUngDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, "TriGiaCungUngDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUngTK", SqlDbType.Decimal, "LuongCungUngTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, "TriGiaCungUngTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUngNK", SqlDbType.Decimal, "LuongCungUngNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, "TriGiaCungUngNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.Char, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapDK", SqlDbType.Decimal, "LuongNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapDK", SqlDbType.Decimal, "TriGiaNhapDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUngDK", SqlDbType.Decimal, "LuongCungUngDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, "TriGiaCungUngDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungDK", SqlDbType.Decimal, "LuongSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, "TriGiaSuDungDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTK", SqlDbType.Decimal, "LuongNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapTK", SqlDbType.Decimal, "TriGiaNhapTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUngTK", SqlDbType.Decimal, "LuongCungUngTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, "TriGiaCungUngTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungTK", SqlDbType.Decimal, "LuongSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, "TriGiaSuDungTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapNK", SqlDbType.Decimal, "LuongNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaNhapNK", SqlDbType.Decimal, "TriGiaNhapNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUngNK", SqlDbType.Decimal, "LuongCungUngNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, "TriGiaCungUngNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongSuDungNK", SqlDbType.Decimal, "LuongSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, "TriGiaSuDungNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCK", SqlDbType.Decimal, "LuongTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTonCK", SqlDbType.Decimal, "TriGiaTonCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.NVarChar, "NamQT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_QuyetToan_NguyenPhuLieu Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_QuyetToan_NguyenPhuLieu> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_QuyetToan_NguyenPhuLieu> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_QuyetToan_NguyenPhuLieu> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicBy(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamicBy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        public static DataSet SelectDynamicGroupBy(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamicGroupBy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
        public List<KDT_GC_QuyetToan_NguyenPhuLieu> SelectCollectionDynamicByHD_ID(string whereCondition, string orderByExpression)
        {
            IDataReader reader = SelectReaderDynamicByHD_ID(whereCondition, orderByExpression);
            return ConvertToCollection(reader);
        }
        public static IDataReader SelectReaderDynamicByHD_ID(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_SelectDynamicByHD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_QuyetToan_NguyenPhuLieu(long hopDong_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongNhapDK, decimal triGiaNhapDK, decimal luongCungUngDK, decimal triGiaCungUngDK, decimal luongSuDungDK, decimal triGiaSuDungDK, decimal luongNhapTK, decimal triGiaNhapTK, decimal luongCungUngTK, decimal triGiaCungUngTK, decimal luongSuDungTK, decimal triGiaSuDungTK, decimal luongNhapNK, decimal triGiaNhapNK, decimal luongCungUngNK, decimal triGiaCungUngNK, decimal luongSuDungNK, decimal triGiaSuDungNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_NguyenPhuLieu entity = new KDT_GC_QuyetToan_NguyenPhuLieu();	
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNhapDK = luongNhapDK;
			entity.TriGiaNhapDK = triGiaNhapDK;
			entity.LuongCungUngDK = luongCungUngDK;
			entity.TriGiaCungUngDK = triGiaCungUngDK;
			entity.LuongSuDungDK = luongSuDungDK;
			entity.TriGiaSuDungDK = triGiaSuDungDK;
			entity.LuongNhapTK = luongNhapTK;
			entity.TriGiaNhapTK = triGiaNhapTK;
			entity.LuongCungUngTK = luongCungUngTK;
			entity.TriGiaCungUngTK = triGiaCungUngTK;
			entity.LuongSuDungTK = luongSuDungTK;
			entity.TriGiaSuDungTK = triGiaSuDungTK;
			entity.LuongNhapNK = luongNhapNK;
			entity.TriGiaNhapNK = triGiaNhapNK;
			entity.LuongCungUngNK = luongCungUngNK;
			entity.TriGiaCungUngNK = triGiaCungUngNK;
			entity.LuongSuDungNK = luongSuDungNK;
			entity.TriGiaSuDungNK = triGiaSuDungNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNhapDK", SqlDbType.Decimal, LuongNhapDK);
			db.AddInParameter(dbCommand, "@TriGiaNhapDK", SqlDbType.Decimal, TriGiaNhapDK);
			db.AddInParameter(dbCommand, "@LuongCungUngDK", SqlDbType.Decimal, LuongCungUngDK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, TriGiaCungUngDK);
			db.AddInParameter(dbCommand, "@LuongSuDungDK", SqlDbType.Decimal, LuongSuDungDK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, TriGiaSuDungDK);
			db.AddInParameter(dbCommand, "@LuongNhapTK", SqlDbType.Decimal, LuongNhapTK);
			db.AddInParameter(dbCommand, "@TriGiaNhapTK", SqlDbType.Decimal, TriGiaNhapTK);
			db.AddInParameter(dbCommand, "@LuongCungUngTK", SqlDbType.Decimal, LuongCungUngTK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, TriGiaCungUngTK);
			db.AddInParameter(dbCommand, "@LuongSuDungTK", SqlDbType.Decimal, LuongSuDungTK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, TriGiaSuDungTK);
			db.AddInParameter(dbCommand, "@LuongNhapNK", SqlDbType.Decimal, LuongNhapNK);
			db.AddInParameter(dbCommand, "@TriGiaNhapNK", SqlDbType.Decimal, TriGiaNhapNK);
			db.AddInParameter(dbCommand, "@LuongCungUngNK", SqlDbType.Decimal, LuongCungUngNK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, TriGiaCungUngNK);
			db.AddInParameter(dbCommand, "@LuongSuDungNK", SqlDbType.Decimal, LuongSuDungNK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, TriGiaSuDungNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_QuyetToan_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_QuyetToan_NguyenPhuLieu(long id, long hopDong_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongNhapDK, decimal triGiaNhapDK, decimal luongCungUngDK, decimal triGiaCungUngDK, decimal luongSuDungDK, decimal triGiaSuDungDK, decimal luongNhapTK, decimal triGiaNhapTK, decimal luongCungUngTK, decimal triGiaCungUngTK, decimal luongSuDungTK, decimal triGiaSuDungTK, decimal luongNhapNK, decimal triGiaNhapNK, decimal luongCungUngNK, decimal triGiaCungUngNK, decimal luongSuDungNK, decimal triGiaSuDungNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_NguyenPhuLieu entity = new KDT_GC_QuyetToan_NguyenPhuLieu();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNhapDK = luongNhapDK;
			entity.TriGiaNhapDK = triGiaNhapDK;
			entity.LuongCungUngDK = luongCungUngDK;
			entity.TriGiaCungUngDK = triGiaCungUngDK;
			entity.LuongSuDungDK = luongSuDungDK;
			entity.TriGiaSuDungDK = triGiaSuDungDK;
			entity.LuongNhapTK = luongNhapTK;
			entity.TriGiaNhapTK = triGiaNhapTK;
			entity.LuongCungUngTK = luongCungUngTK;
			entity.TriGiaCungUngTK = triGiaCungUngTK;
			entity.LuongSuDungTK = luongSuDungTK;
			entity.TriGiaSuDungTK = triGiaSuDungTK;
			entity.LuongNhapNK = luongNhapNK;
			entity.TriGiaNhapNK = triGiaNhapNK;
			entity.LuongCungUngNK = luongCungUngNK;
			entity.TriGiaCungUngNK = triGiaCungUngNK;
			entity.LuongSuDungNK = luongSuDungNK;
			entity.TriGiaSuDungNK = triGiaSuDungNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_QuyetToan_NguyenPhuLieu_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNhapDK", SqlDbType.Decimal, LuongNhapDK);
			db.AddInParameter(dbCommand, "@TriGiaNhapDK", SqlDbType.Decimal, TriGiaNhapDK);
			db.AddInParameter(dbCommand, "@LuongCungUngDK", SqlDbType.Decimal, LuongCungUngDK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, TriGiaCungUngDK);
			db.AddInParameter(dbCommand, "@LuongSuDungDK", SqlDbType.Decimal, LuongSuDungDK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, TriGiaSuDungDK);
			db.AddInParameter(dbCommand, "@LuongNhapTK", SqlDbType.Decimal, LuongNhapTK);
			db.AddInParameter(dbCommand, "@TriGiaNhapTK", SqlDbType.Decimal, TriGiaNhapTK);
			db.AddInParameter(dbCommand, "@LuongCungUngTK", SqlDbType.Decimal, LuongCungUngTK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, TriGiaCungUngTK);
			db.AddInParameter(dbCommand, "@LuongSuDungTK", SqlDbType.Decimal, LuongSuDungTK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, TriGiaSuDungTK);
			db.AddInParameter(dbCommand, "@LuongNhapNK", SqlDbType.Decimal, LuongNhapNK);
			db.AddInParameter(dbCommand, "@TriGiaNhapNK", SqlDbType.Decimal, TriGiaNhapNK);
			db.AddInParameter(dbCommand, "@LuongCungUngNK", SqlDbType.Decimal, LuongCungUngNK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, TriGiaCungUngNK);
			db.AddInParameter(dbCommand, "@LuongSuDungNK", SqlDbType.Decimal, LuongSuDungNK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, TriGiaSuDungNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_QuyetToan_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_QuyetToan_NguyenPhuLieu(long id, long hopDong_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongNhapDK, decimal triGiaNhapDK, decimal luongCungUngDK, decimal triGiaCungUngDK, decimal luongSuDungDK, decimal triGiaSuDungDK, decimal luongNhapTK, decimal triGiaNhapTK, decimal luongCungUngTK, decimal triGiaCungUngTK, decimal luongSuDungTK, decimal triGiaSuDungTK, decimal luongNhapNK, decimal triGiaNhapNK, decimal luongCungUngNK, decimal triGiaCungUngNK, decimal luongSuDungNK, decimal triGiaSuDungNK, decimal luongTonCK, decimal triGiaTonCK, string namQT)
		{
			KDT_GC_QuyetToan_NguyenPhuLieu entity = new KDT_GC_QuyetToan_NguyenPhuLieu();			
			entity.ID = id;
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongNhapDK = luongNhapDK;
			entity.TriGiaNhapDK = triGiaNhapDK;
			entity.LuongCungUngDK = luongCungUngDK;
			entity.TriGiaCungUngDK = triGiaCungUngDK;
			entity.LuongSuDungDK = luongSuDungDK;
			entity.TriGiaSuDungDK = triGiaSuDungDK;
			entity.LuongNhapTK = luongNhapTK;
			entity.TriGiaNhapTK = triGiaNhapTK;
			entity.LuongCungUngTK = luongCungUngTK;
			entity.TriGiaCungUngTK = triGiaCungUngTK;
			entity.LuongSuDungTK = luongSuDungTK;
			entity.TriGiaSuDungTK = triGiaSuDungTK;
			entity.LuongNhapNK = luongNhapNK;
			entity.TriGiaNhapNK = triGiaNhapNK;
			entity.LuongCungUngNK = luongCungUngNK;
			entity.TriGiaCungUngNK = triGiaCungUngNK;
			entity.LuongSuDungNK = luongSuDungNK;
			entity.TriGiaSuDungNK = triGiaSuDungNK;
			entity.LuongTonCK = luongTonCK;
			entity.TriGiaTonCK = triGiaTonCK;
			entity.NamQT = namQT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongNhapDK", SqlDbType.Decimal, LuongNhapDK);
			db.AddInParameter(dbCommand, "@TriGiaNhapDK", SqlDbType.Decimal, TriGiaNhapDK);
			db.AddInParameter(dbCommand, "@LuongCungUngDK", SqlDbType.Decimal, LuongCungUngDK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngDK", SqlDbType.Decimal, TriGiaCungUngDK);
			db.AddInParameter(dbCommand, "@LuongSuDungDK", SqlDbType.Decimal, LuongSuDungDK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungDK", SqlDbType.Decimal, TriGiaSuDungDK);
			db.AddInParameter(dbCommand, "@LuongNhapTK", SqlDbType.Decimal, LuongNhapTK);
			db.AddInParameter(dbCommand, "@TriGiaNhapTK", SqlDbType.Decimal, TriGiaNhapTK);
			db.AddInParameter(dbCommand, "@LuongCungUngTK", SqlDbType.Decimal, LuongCungUngTK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngTK", SqlDbType.Decimal, TriGiaCungUngTK);
			db.AddInParameter(dbCommand, "@LuongSuDungTK", SqlDbType.Decimal, LuongSuDungTK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungTK", SqlDbType.Decimal, TriGiaSuDungTK);
			db.AddInParameter(dbCommand, "@LuongNhapNK", SqlDbType.Decimal, LuongNhapNK);
			db.AddInParameter(dbCommand, "@TriGiaNhapNK", SqlDbType.Decimal, TriGiaNhapNK);
			db.AddInParameter(dbCommand, "@LuongCungUngNK", SqlDbType.Decimal, LuongCungUngNK);
			db.AddInParameter(dbCommand, "@TriGiaCungUngNK", SqlDbType.Decimal, TriGiaCungUngNK);
			db.AddInParameter(dbCommand, "@LuongSuDungNK", SqlDbType.Decimal, LuongSuDungNK);
			db.AddInParameter(dbCommand, "@TriGiaSuDungNK", SqlDbType.Decimal, TriGiaSuDungNK);
			db.AddInParameter(dbCommand, "@LuongTonCK", SqlDbType.Decimal, LuongTonCK);
			db.AddInParameter(dbCommand, "@TriGiaTonCK", SqlDbType.Decimal, TriGiaTonCK);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.NVarChar, NamQT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_QuyetToan_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_QuyetToan_NguyenPhuLieu(long id)
		{
			KDT_GC_QuyetToan_NguyenPhuLieu entity = new KDT_GC_QuyetToan_NguyenPhuLieu();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public  int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_QuyetToan_NguyenPhuLieu_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_QuyetToan_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_QuyetToan_NguyenPhuLieu item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}