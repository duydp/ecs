﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SignRemote;
using Company.KDT.SHARE.Components;
using Microsoft.Win32;
using ServerRemote.BLL;
using ServerRemote.DriverDetect;
using Microsoft.Win32.SafeHandles;
using System.Runtime.InteropServices;
using Message = System.Windows.Forms.Message;
using System.Threading;
//using FMS.Drawing;
namespace ServerRemote
{
    public partial class frmMain : Form
    {
        private DriveDetector _detector = null;
        string _args = string.Empty;
        private bool isData = true;
        private List<OUTBOX> ListTrack = new List<OUTBOX>();
        private List<MessageSend> _queueMessages = new List<MessageSend>();
        public int TimeSleep = 1000;
        public int TimeSleepTotal = 1000;
        public int Count = 1;
        private ServerRemote.InboxData m_Data = null;
        private ServerRemote.InboxFileData m_FileData = null;
        public frmMain(string args, bool isUseDatabase)
        {
            InitializeComponent();
            _args = args;
            isData = isUseDatabase;
        }
        protected override void WndProc(ref Message m)
        {
            try
            {
                base.WndProc(ref m);

                if (_detector != null)
                {
                    _detector.WndProc(ref m);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
          
        }
        public frmMain()
        {
            InitializeComponent();
            clcDenNgay.Value = DateTime.Now;
        }
        List<SignLogger> dataSigns = new List<SignLogger>();
        private void frmMain_Load(object sender, EventArgs e)
        {
            //OUTBOX.DeleteDynamic("MSG_SIGN_DATA IS NULL");

            MSG_OUTBOX.DeleteDynamic("MSG_SIGN_DATA IS NULL");
            btnSearchInbox_Click(null,null);
            grList.CheckAllRecords();
            this.Activate();
            if (_args == "autohide")
            {
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
                rkApp.SetValue("SignRemote", string.Format("\"{0}\" \"autohide\"", Application.ExecutablePath));
            }
            try
            {
                if (Globals.IsSignOnLan || isData)
                {
                    dataSigns = SignLogger.SelectTop50();
                    dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                    {
                        return y.DATECREATE.CompareTo(x.DATECREATE);
                    });
                    bindingSource1.DataSource = dataSigns;
                    gridList.DataSource = bindingSource1.DataSource;
                    gridList.Refresh();
                }
                else
                {
                    dataSigns = SignLogger.SelectCollectionAccess(Application.StartupPath, 50, "", "");
                    dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                    {
                        return y.DATECREATE.CompareTo(x.DATECREATE);
                    });
                    bindingSource1.DataSource = dataSigns;
                    gridList.DataSource = bindingSource1.DataSource;
                    gridList.Refresh();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            //_detector = new DriveDetector(this.Handle);
            //_detector.DeviceArrived += new DriveDetectorEventHandler(OnDriveArrived);
            //_detector.DeviceRemoved += new DriveDetectorEventHandler(OnDriveRemoved);
            //_detector.QueryRemove += new DriveDetectorEventHandler(OnQueryRemove);
            Excute();
            #region New
            // Create a new ChatData object
            if (m_Data==null)
            {
                m_Data = new ServerRemote.InboxData();   
            }
            // Hook up event
            m_Data.OnNewMessage += new ServerRemote.InboxData.NewMessage(OnNewMessage);

            // Load existing message
            LoadMessages();
            //Excute();

             //Create a new ChatData object
            if (m_FileData==null)
            {
                m_FileData = new ServerRemote.InboxFileData();   
            }

            // Hook up event
            m_FileData.OnNewMessage += new ServerRemote.InboxFileData.NewMessage(OnNewFileMessage);

            LoadFileMessages();
            //ExcuteFile();
            #endregion
        }
        /// <summary>
        /// Handler for NewMessage
        /// </summary>
        void OnNewFileMessage()
        {
            ISynchronizeInvoke i = (ISynchronizeInvoke)this;

            // Check if the event was generated from another
            // thread and needs invoke instead
            if (i.InvokeRequired)
            {
                ServerRemote.InboxFileData.NewMessage tempDelegate = new ServerRemote.InboxFileData.NewMessage(OnNewFileMessage);
                i.BeginInvoke(tempDelegate, null);
                return;
            }

            // If not coming from a seperate thread
            // we can access the Windows form controls
            LoadFileMessages();
        }
        public void LoadFileMessages()
        {

            // Get the messages
            DataTable dt = m_FileData.GetInboxMessages();
            if (dt.Rows.Count >= 1)
            {
                Company.KDT.SHARE.Components.Globals.IsSignOnLan = true;
                ExcuteFile();
            }
        }
        /// <summary>
        /// Handler for NewMessage
        /// </summary>
        void OnNewMessage()
        {
            ISynchronizeInvoke i = (ISynchronizeInvoke)this;

            // Check if the event was generated from another
            // thread and needs invoke instead
            if (i.InvokeRequired)
            {
                ServerRemote.InboxData.NewMessage tempDelegate = new ServerRemote.InboxData.NewMessage(OnNewMessage);
                i.BeginInvoke(tempDelegate, null);
                return;
            }

            // If not coming from a seperate thread
            // we can access the Windows form controls
            LoadMessages();
        }
        public void LoadMessages()
        {

            // Get the messages
            DataTable dt = m_Data.GetInboxMessages();
            if (dt.Rows.Count >=1)
            {
                Company.KDT.SHARE.Components.Globals.IsSignOnLan = true;
                Excute();
            }
        }
        #region Detect usb connect
        // Called by DriveDetector when removable device in inserted 
        /// <summary>
        /// Connect usb
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDriveArrived(object sender, DriveDetectorEventArgs e)
        {
            try
            {
                Helpers.GetSignature("Test");
                if (!Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    SignRemote.MessageSend msg = new SignRemote.MessageSend()
                    {
                        From = Globals.UserNameLogin,
                        Password = Globals.PasswordLogin,
                        SendStatus = SendStatusInfo.ServerLogin,
                        Warrning = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss")
                    };

                    ISignMessage service = WebService.SignMessage();
                    service.ServerSend(Helpers.Serializer(msg));
                }
                setNotifyIcon("Hệ thống đã kết nối thiết bị ký số");
                //ProcessAPI.IsAbort = false;
                Excute();

            }
            catch { }
            // Report the event in the listbox.
            // e.Drive is the drive letter for the device which just arrived, e.g. "E:\\"
            //string s = "Drive arrived " + e.Drive;
            //listBox1.Items.Add(s);

            // If you want to be notified when drive is being removed (and be able to cancel it), 
            // set HookQueryRemove to true 
            //if (checkBoxAskMe.Checked)
            //    e.HookQueryRemove = true;
        }
        /// <summary>
        /// Remove driver
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDriveRemoved(object sender, DriveDetectorEventArgs e)
        {
            // TODO: do clean up here, etc. Letter of the removed drive is in e.Drive;           
            try
            {
                Helpers.GetSignature("test");
            }
            catch (Exception ex)
            {
                //ProcessAPI.IsAbort = true;
                setNotifyIcon(ex.Message);
            }
        }
        private void OnQueryRemove(object sender, DriveDetectorEventArgs e)
        {
            // Should we allow the drive to be unplugged?
            //if (checkBoxAskMe.Checked)
            //{
            //if (MessageBox.Show("Allow remove?", "Query remove",
            //    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //    e.Cancel = false;       // Allow removal
            //else
            //    e.Cancel = true;        // Cancel the removal of the device  
            //}
        }
        #endregion
        protected override void OnClosing(CancelEventArgs e)
        {
            if (MessageBoxControl.ShowMessageConfirm("Bạn có muốn đóng ứng dụng không?") == "Yes")
            //if (MessageBox.Show("Bạn có muốn đóng ứng dụng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (!Globals.IsSignOnLan)
                {
                    MessageSend msg = new MessageSend()
                    {
                        From = Globals.UserNameLogin,
                        Password = Globals.PasswordLogin,
                        SendStatus = SendStatusInfo.ServerLogout
                    };
                    ISignMessage service = WebService.SignMessage();
                    service.ServerSend(Helpers.Serializer(msg));
                    //ProcessAPI.IsAbort = true;
                }
                base.OnClosing(e);
            }
            else e.Cancel = true;
        }
        public void Excute()
        {
            ProcessAPI proc = new ProcessAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
            proc.Sign += new EventHandler<SignEventArgs>(proc_Sign);
            proc.DoProcess();
        }
        public void ExcuteFile()
        {
            ProcessFileAPI proc = new ProcessFileAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
            proc.FileSign += new EventHandler<SignFileEventArgs>(proc_SignFile);
            proc.DoProcessFile();
        }
        private void SendHandler(object sender, SignEventArgs e)
        {

            if (e.Error == null && e.Content != null)
            {
                SignLogger sign = new SignLogger()
                {
                    MSG_FROM = e.Content.From,
                    MESSAGE = e.Content.Message.Content,
                    MSG_TO = e.Content.To,
                    TIME_PROCESS = new DateTime(e.TotalTime.Ticks),
                    DATECREATE = DateTime.Now
                };
                try
                {
                    if (this.isData)
                    {
                        sign.Insert();
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                        bindingSource1.DataSource = dataSigns;
                        gridList.DataSource = null;
                        gridList.DataSource = bindingSource1.DataSource;
                    }
                    else
                    {
                        sign.InsertAccess(Application.StartupPath);
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                        bindingSource1.DataSource = dataSigns;
                        gridList.DataSource = null;
                        gridList.DataSource = bindingSource1.DataSource;
                    }
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                    setNotifyIcon(string.Format("Đã ký thông tin yêu cầu từ '{0}'", e.Content.From));
                }
                catch
                {
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                    setNotifyIcon(string.Format("Lỗi ký yêu cầu từ '{0}'", e.Content.From));
                }
            }
            else
            {
                if (e.Content.From == null) e.Content.From = "Anonymous";
                setNotifyIcon(string.Format("Lỗi ký yêu cầu từ '{0}'", e.Content.From));
            }
        }
        private void SenFiledHandler(object sender, SignFileEventArgs e)
        {

            if (e.Error == null && e.Content != null)
            {
                SignLogger sign = new SignLogger()
                {
                    MSG_FROM = e.Content.From,
                    MESSAGE = e.Content.Message.FileName,
                    MSG_TO = e.Content.To,
                    TIME_PROCESS = new DateTime(e.TotalTime.Ticks),
                    DATECREATE = DateTime.Now
                };
                try
                {
                    if (this.isData)
                    {
                        sign.Insert();
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                        bindingSource2.DataSource = dataSigns;
                        gridFile.DataSource = null;
                        gridFile.DataSource = bindingSource2.DataSource;
                    }
                    else
                    {
                        sign.InsertAccess(Application.StartupPath);
                        if (dataSigns.Count > 50)
                        {
                            dataSigns.RemoveAt(0);
                            dataSigns.Add(sign);
                        }
                        else
                            dataSigns.Add(sign);
                        dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                        {
                            return y.DATECREATE.CompareTo(x.DATECREATE);
                        });
                        bindingSource2.DataSource = dataSigns;
                        gridFile.DataSource = null;
                        gridFile.DataSource = bindingSource2.DataSource;
                    }
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                    setNotifyIcon(string.Format("Đã ký thông tin yêu cầu từ '{0}'", e.Content.From));
                }
                catch
                {
                    if (e.Content.From == null) e.Content.From = "Anonymous";
                    setNotifyIcon(string.Format("Lỗi ký yêu cầu từ '{0}'", e.Content.From));
                }
            }
            else
            {
                if (e.Content.From == null) e.Content.From = "Anonymous";
                setNotifyIcon(string.Format("Lỗi ký yêu cầu từ '{0}'", e.Content.From));
            }
        }
        void proc_Sign(object sender, SignEventArgs e)
        {
            try
            {
                this.Invoke(
                     new EventHandler<SignEventArgs>(SendHandler),
                         sender, e);
            }
            catch { }
        }
        void proc_SignFile(object sender, SignFileEventArgs e)
        {
            try
            {
                this.Invoke(
                     new EventHandler<SignFileEventArgs>(SenFiledHandler),
                         sender, e);
            }
            catch { }
        }
        private void setNotifyIcon(string text)
        {
            notifyIcon1.Visible = true;
            string sfmtCurr = notifyIcon1.BalloonTipText;
            notifyIcon1.BalloonTipText = text;
            notifyIcon1.ShowBalloonTip(500);
            notifyIcon1.BalloonTipText = sfmtCurr;
        }
        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                notifyIcon1.ShowBalloonTip(500);
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            Show();
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String dateFrom = clcDateFrom.Value.ToString("yyyy-MM-dd 00:00:00");
                String dateTo = clcDateTo.Value.ToString("yyyy-MM-dd 23:59:59");
                string where = string.Format("MSG_FROM like '%{0}%' AND  DATECREATE BETWEEN '{1}' AND '{2}'", txtMsgFrom.Text,dateFrom,dateTo);
                if (isData)
                    dataSigns = SignLogger.SelectCollectionDynamic(where, "");
                else
                    dataSigns = SignLogger.SelectCollectionAccess(Application.StartupPath, 0, where, "DATECREATE");
                dataSigns.Sort(delegate(SignLogger x, SignLogger y)
                {
                    return y.DATECREATE.CompareTo(x.DATECREATE);
                });
                bindingSource1.DataSource = dataSigns;
                gridList.DataSource = null;
                gridList.DataSource = bindingSource1.DataSource;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (new frmLogin().ShowDialog() == DialogResult.OK)
                Application.Restart();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            new frmLoginUSB().ShowDialog();
        }

        private void btnList_Click(object sender, EventArgs e)
        {
            frmDanhSachAccount f = new frmDanhSachAccount();
            f.ShowDialog(this);
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            frmChangePassDL f = new frmChangePassDL();
            f.openType = ServerRemote.frmChangePassDL.OpenType.DoanhNghiep;
            f.User = Globals.UserNameLogin;
            f.ShowDialog(this);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://softech.vn/");
        }

        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {                
                lblPhanTram.Text = "0%";
                prbTienTrinh.Value = 0;
                txtError.Text = "";
                ListTrack.Clear();
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                TimeSleepTotal = 3000;
                if (listChecked.Length > 0)
                {

                        for (int i = 0; i < listChecked.Length; i++)
                        {
                            OUTBOX outbox = new OUTBOX();
                            Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                            outbox.MSG_ORIGIN = item.Cells["MSG_ORIGIN"].Value.ToString();
                            outbox.STATUS = Convert.ToInt32(item.Cells["STATUS"].Value.ToString());
                            outbox.GUIDSTR = item.Cells["GUIDSTR"].Value.ToString();
                            outbox.CREATE_TIME = Convert.ToDateTime(item.Cells["CREATE_TIME"].Value);
                            ListTrack.Add(outbox);

                        }
                        //ExcuteNew();
                        Count = ListTrack.Count;
                        TimeSleepTotal = (TimeSleep * Count);
                        System.Threading.ThreadPool.QueueUserWorkItem(DoProcess);
                        System.Threading.Thread.Sleep(TimeSleepTotal/3);
                 }            
                else
                {
                    System.Threading.Thread.Sleep(1000);
                }
                btnSearchInbox_Click(null,null);
                grList.CheckAllRecords();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void ExcuteNew()
        {
            ProcessAPI proc = new ProcessAPI(Company.KDT.SHARE.Components.Globals.IsSignOnLan);
            proc.Sign += new EventHandler<SignEventArgs>(proc_Sign);
            proc.OutBoxCollection = ListTrack;
            proc.DoProcessNew();
            btnSearchInbox_Click(null, null);
            grList.CheckAllRecords();
        }
        private void DoProcess(object obj)
        {
            try
            {
                ThreadPool.QueueUserWorkItem(DoGetContentLocalNew);
                System.Threading.Thread.Sleep(TimeSleepTotal / 3);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public void DoGetContentLocalNew(object obj)
        {
                try
                {
                    foreach (OUTBOX outBoxRoot in ListTrack)
                    {
                        _queueMessages.Clear();
                        if (outBoxRoot != null)
                        {
                            MessageSend msgSend = new MessageSend()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                SendStatus = SendStatusInfo.Request,
                                Message = new SignRemote.Message()
                                {
                                    Content = outBoxRoot.MSG_ORIGIN,
                                    GuidStr = outBoxRoot.GUIDSTR
                                },
                                TypeMessages = outBoxRoot.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                            };
                            _queueMessages.Add(msgSend);    
                            
                            ThreadPool.QueueUserWorkItem(DoSignContentNew);
                            System.Threading.Thread.Sleep(TimeSleepTotal / 3);
                        }
                        else
                            Thread.Sleep(new TimeSpan(0, 0, 5));
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

        }
        public void DoSignContentNew(object obj)
        {
                try
                {
                    bool isSignVNACCS = false;
                    int index = 0;
                    MessageSend msgSend = _queueMessages[index];

                    #region SignData
                    isSignVNACCS = msgSend.TypeMessages == "VNACCS";

                    string content;
                    MessageSignature signInfo;
                    if (!isSignVNACCS)
                    {
                        content = Helpers.ConvertFromBase64(msgSend.Message.Content);
                        signInfo = Helpers.GetSignature(content);
                    }
                    else
                    {
                        content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                        Helpers.GetSignature("Test");
                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2New(Globals.GetX509CertificatedName);
                        signInfo = new MessageSignature()
                        {
                            Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                        };
                    }

                    OUTBOX.UpdateDataSignLocal(msgSend.Message.GuidStr, signInfo.Data, signInfo.FileCert);
                    INBOX.DeleteDynamic("GUIDSTR ='" + msgSend.Message.GuidStr + "'");
                    #endregion
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
        }
        //void receive_SyncEventArgs(object sender, SyncVNACCSEventArgs e)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        this.Invoke(new MethodInvoker(delegate { SetLable(e); }));
        //    }
        //    else
        //    {
        //        SetLable(e);
        //    }
        //}

        //void send_SyncEventArgs(object sender, SyncVNACCSEventArgs e)
        //{
        //    if (this.InvokeRequired)
        //    {
        //        this.Invoke(new MethodInvoker(delegate { SetLable(e); }));
        //    }
        //    else
        //    {
        //        SetLable(e);
        //    }
        //}
        //private void SetLable(SyncVNACCSEventArgs e)
        //{
        //    if (e.Error != null)
        //    {
        //        Exception ex = new Exception(e.Error.ToString());

        //        txtError.Text = txtError.Text + "\r\n" + e.Error.ToString();//Set lable error;
        //        Logger.LocalLogger.Instance().WriteMessage("Lỗi Yêu cầu ký chữ ký số \r\n", ex);
        //    }
        //    else if (!string.IsNullOrEmpty(e.Message))
        //    {
        //        txtError.Text = "";
        //        lblPhanTram.Text = e.Percent.ToString() + "%";// Set lable trang thai
        //    }
        //    if (e.Percent > 0)
        //        prbTienTrinh.Value = e.Percent;// set  Process = percent
        //}

        private void btnSearchInbox_Click(object sender, EventArgs e)
        {
            try
            {
                string whereCondition = " MSG_SIGN_DATA IS NULL AND MSG_FROM IS NULL ";
                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    whereCondition += " AND MSG_FROM LIKE '%"+txtFrom.Text+"%' ";
                }
                String dateFrom = clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:00");
                String dateTo = clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59");
                whereCondition += " AND CREATE_TIME BETWEEN '" + dateFrom + "' AND '" + dateTo + "'";
                grList.Refresh();
                grList.DataSource = MSG_OUTBOX.SelectCollectionDynamic(whereCondition, "CREATE_TIME DESC");
                grList.Refetch();
                grList.CheckAllRecords();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                btnTransfer.PerformClick();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAutoSign_Click(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.Timer tmr = new System.Windows.Forms.Timer();
                tmr.Interval = 3000;
                tmr.Tick += new EventHandler(timer1_Tick);
                tmr.Start();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
    }
}
