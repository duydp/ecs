﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SOFTECH - ECS 2.0 - TQDT - KD")]
[assembly: AssemblyDescription("SOFTECH - ECS 2.0 - TQDT - KD")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SOFTECH")]
[assembly: AssemblyProduct("SOFTECH - ECS 2.0 - TQDT - KD")]
[assembly: AssemblyCopyright("Copyright © SOFTECH")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("de0da7d8-1d33-40c1-b539-fdb859dbe5b7")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("2.0.2012.2421")]
[assembly: AssemblyFileVersion("2.0.2012.2421")]
[assembly: NeutralResourcesLanguageAttribute("")]
