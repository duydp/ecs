-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Insert]
	@HoaDon_ID bigint,
	@MaHang varchar(40),
	@MaSoHang varchar(12),
	@TenHang nvarchar(200),
	@MaNuocXX varchar(2),
	@TenNuocXX varchar(30),
	@SoKienHang varchar(35),
	@SoLuong1 numeric(12, 0),
	@MaDVT_SoLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@MaDVT_SoLuong2 varchar(4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTT_DonGia varchar(3),
	@MaDVT_DonGia varchar(3),
	@TriGiaHoaDon numeric(24, 4),
	@MaTT_GiaTien varchar(3),
	@LoaiKhauTru varchar(20),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(50),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangHoaDon]
(
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@HoaDon_ID,
	@MaHang,
	@MaSoHang,
	@TenHang,
	@MaNuocXX,
	@TenNuocXX,
	@SoKienHang,
	@SoLuong1,
	@MaDVT_SoLuong1,
	@SoLuong2,
	@MaDVT_SoLuong2,
	@DonGiaHoaDon,
	@MaTT_DonGia,
	@MaDVT_DonGia,
	@TriGiaHoaDon,
	@MaTT_GiaTien,
	@LoaiKhauTru,
	@SoTienKhauTru,
	@MaTT_TienKhauTru,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Update]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaHang varchar(40),
	@MaSoHang varchar(12),
	@TenHang nvarchar(200),
	@MaNuocXX varchar(2),
	@TenNuocXX varchar(30),
	@SoKienHang varchar(35),
	@SoLuong1 numeric(12, 0),
	@MaDVT_SoLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@MaDVT_SoLuong2 varchar(4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTT_DonGia varchar(3),
	@MaDVT_DonGia varchar(3),
	@TriGiaHoaDon numeric(24, 4),
	@MaTT_GiaTien varchar(3),
	@LoaiKhauTru varchar(20),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(50),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangHoaDon]
SET
	[HoaDon_ID] = @HoaDon_ID,
	[MaHang] = @MaHang,
	[MaSoHang] = @MaSoHang,
	[TenHang] = @TenHang,
	[MaNuocXX] = @MaNuocXX,
	[TenNuocXX] = @TenNuocXX,
	[SoKienHang] = @SoKienHang,
	[SoLuong1] = @SoLuong1,
	[MaDVT_SoLuong1] = @MaDVT_SoLuong1,
	[SoLuong2] = @SoLuong2,
	[MaDVT_SoLuong2] = @MaDVT_SoLuong2,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[MaTT_DonGia] = @MaTT_DonGia,
	[MaDVT_DonGia] = @MaDVT_DonGia,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[MaTT_GiaTien] = @MaTT_GiaTien,
	[LoaiKhauTru] = @LoaiKhauTru,
	[SoTienKhauTru] = @SoTienKhauTru,
	[MaTT_TienKhauTru] = @MaTT_TienKhauTru,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaHang varchar(40),
	@MaSoHang varchar(12),
	@TenHang nvarchar(200),
	@MaNuocXX varchar(2),
	@TenNuocXX varchar(30),
	@SoKienHang varchar(35),
	@SoLuong1 numeric(12, 0),
	@MaDVT_SoLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@MaDVT_SoLuong2 varchar(4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTT_DonGia varchar(3),
	@MaDVT_DonGia varchar(3),
	@TriGiaHoaDon numeric(24, 4),
	@MaTT_GiaTien varchar(3),
	@LoaiKhauTru varchar(20),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(50),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangHoaDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangHoaDon] 
		SET
			[HoaDon_ID] = @HoaDon_ID,
			[MaHang] = @MaHang,
			[MaSoHang] = @MaSoHang,
			[TenHang] = @TenHang,
			[MaNuocXX] = @MaNuocXX,
			[TenNuocXX] = @TenNuocXX,
			[SoKienHang] = @SoKienHang,
			[SoLuong1] = @SoLuong1,
			[MaDVT_SoLuong1] = @MaDVT_SoLuong1,
			[SoLuong2] = @SoLuong2,
			[MaDVT_SoLuong2] = @MaDVT_SoLuong2,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[MaTT_DonGia] = @MaTT_DonGia,
			[MaDVT_DonGia] = @MaDVT_DonGia,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[MaTT_GiaTien] = @MaTT_GiaTien,
			[LoaiKhauTru] = @LoaiKhauTru,
			[SoTienKhauTru] = @SoTienKhauTru,
			[MaTT_TienKhauTru] = @MaTT_TienKhauTru,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangHoaDon]
		(
			[HoaDon_ID],
			[MaHang],
			[MaSoHang],
			[TenHang],
			[MaNuocXX],
			[TenNuocXX],
			[SoKienHang],
			[SoLuong1],
			[MaDVT_SoLuong1],
			[SoLuong2],
			[MaDVT_SoLuong2],
			[DonGiaHoaDon],
			[MaTT_DonGia],
			[MaDVT_DonGia],
			[TriGiaHoaDon],
			[MaTT_GiaTien],
			[LoaiKhauTru],
			[SoTienKhauTru],
			[MaTT_TienKhauTru],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@HoaDon_ID,
			@MaHang,
			@MaSoHang,
			@TenHang,
			@MaNuocXX,
			@TenNuocXX,
			@SoKienHang,
			@SoLuong1,
			@MaDVT_SoLuong1,
			@SoLuong2,
			@MaDVT_SoLuong2,
			@DonGiaHoaDon,
			@MaTT_DonGia,
			@MaDVT_DonGia,
			@TriGiaHoaDon,
			@MaTT_GiaTien,
			@LoaiKhauTru,
			@SoTienKhauTru,
			@MaTT_TienKhauTru,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangHoaDon]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangHoaDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangHoaDon]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangHoaDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangHoaDon]	

GO

