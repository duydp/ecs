using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich_KhaiBoSung_Thue
	{
        List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> _ListHangBoSungThuKhac = new List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac>();

        public List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> HangThuKhacCollection
        {
            set { this._ListHangBoSungThuKhac = value; }
            get { return this._ListHangBoSungThuKhac; }
        }

        public void LoadHangThuKhacCollection()
        {
            HangThuKhacCollection = KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac.SelectCollectionDynamic("HMDBoSung_ID = " + this.ID, "ID");
        }
	}	
}