﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.Interface.KDT.SXXK
{
    public partial class NguyenPhuLieuEditForm_CX : BaseForm
    {        
        public HangDuaVao NPLDetail;
        public List<HangDuaVao> NPLCollection;
        public HangDuaVaoDangKy nplDangKy;
        private string MoTa = "";
        public NguyenPhuLieuEditForm_CX()
        {
            InitializeComponent();
        }
        
        private void btnClose_Click(object sender, EventArgs e)
        {
           if(this.OpenType==OpenFormType.Insert)
                this.NPLDetail = null;
           this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                // Kiểm tra tính hợp lệ của mã HS.
                if (!MaHS.Validate(txtMaHS.Text, 8))
                {
                    error.SetIconPadding(txtMaHS, -8);
                    //error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    }
                    else
                    {
                        error.SetError(txtMaHS, "HS code is invalid");
                    }
                    return;
                }

                if (!cvError.IsValid) return;

                if (checkNPLExit(txtMa.Text.Trim()))
                {
                    //  ShowMessage("Nguyên phụ liệu này đã được khai báo trên lưới.", false);
                    MLMessages("Nguyên phụ liệu này đã được khai báo trên lưới.", "MSG_PUB10", "", false);
                    return;
                }
                //if (NguyenPhuLieu.checkNPLExit(txtMa.Text.Trim(),nplDangKy.ID))
                //{
                //    //ShowMessage("Nguyên phụ liệu này đã được khai báo trong danh sách khác.", false);
                //    MLMessages("Nguyên phụ liệu này đã được khai báo trong danh sách khác.", "MSG_PUB10", "", false);
                //    return;
                //}
                if (this.OpenType == OpenFormType.Insert)
                {
                    // Kiểm tra xem NPL(SXXK) đã tồn tại hay chưa?
                    //                 BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
                    //                 nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    //                 nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                    //                 nplSXXK.Ma = txtMa.Text.Trim();
                    //                 if (nplSXXK.Load())
                    //                 {
                    //                     //ShowMessage("Nguyên phụ liệu này đã được đăng ký.", false);
                    //                     MLMessages("Nguyên phụ liệu này đã được đăng ký.","MSG_PUB06", "", false);
                    //                     return;
                    //                 }

                    this.NPLDetail = new HangDuaVao();
                    this.NPLDetail.Ma = txtMa.Text.Trim();
                    this.NPLDetail.Ten = txtTen.Text.Trim();
                    this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                    this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.NPLDetail.MucDich = cmbMucDich.SelectedItem == null ? "01" : cmbMucDich.SelectedValue.ToString();
                    this.NPLDetail.LoaiNPL = cmbLoaiHang.SelectedItem == null ? "1" : cmbLoaiHang.SelectedValue.ToString();
                }
                else if (this.OpenType == OpenFormType.Edit)
                {
                    this.NPLDetail.Ten = txtTen.Text.Trim();
                    this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                    this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.NPLDetail.MucDich = cmbMucDich.SelectedItem == null ? "01" : cmbMucDich.SelectedValue.ToString();
                    this.NPLDetail.LoaiNPL = cmbLoaiHang.SelectedItem == null ? "1" : cmbLoaiHang.SelectedValue.ToString();
                    //                 if (txtMa.Text.Trim().ToUpper() != NPLDetail.Ma.Trim().ToUpper())
                    //                 {
                    //                     BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
                    //                     nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    //                     nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                    //                     nplSXXK.Ma = txtMa.Text.Trim();
                    //                     if (nplSXXK.Load())
                    //                     {
                    //                         //ShowMessage("Nguyên phụ liệu này đã được đăng ký.", false);
                    //                         MLMessages("Nguyên phụ liệu này đã được đăng ký.", "MSG_PUB06", "", false);
                    //                         return;
                    //                     }           
                    //                 }
                    //                 string maSP = txtMa.Text.Trim();
                    //                 string tenSP = txtTen.Text.Trim();
                    //                 string mahs = txtMaHS.Text.Trim();
                    //                 string dvt = cbDonViTinh.SelectedValue.ToString();
                    //                 if (maSP != NPLDetail.Ma || tenSP != NPLDetail.Ten || NPLDetail.MaHS != mahs || NPLDetail.DVT_ID != dvt)
                    //                 {
                    //                     string maCu = NPLDetail.Ma;
                    //                     this.NPLDetail.Ma = txtMa.Text.Trim();
                    //                     this.NPLDetail.Ten = txtTen.Text.Trim();
                    //                     this.NPLDetail.MaHS = txtMaHS.Text.Trim();
                    //                     this.NPLDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    //                     if (NPLDetail.ID > 0)
                    //                     {
                    //                         this.NPLDetail.Update();
                    //                         BLL.SXXK.NguyenPhuLieu nplSXXK = new BLL.SXXK.NguyenPhuLieu();
                    //                         nplSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    //                         nplSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                    //                         nplSXXK.Ma = maCu;
                    //                         nplSXXK.Delete();
                    // 
                    //                         nplSXXK.Ma = NPLDetail.Ma;
                    //                         nplSXXK.MaHS = NPLDetail.MaHS;
                    //                         nplSXXK.Ten = NPLDetail.Ten;
                    //                         nplSXXK.DVT_ID = NPLDetail.DVT_ID;
                    //                         nplSXXK.Insert();
                    // 
                    //                         //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                    //                         try
                    //                         {
                    //                             string where = "1 = 1";
                    //                             where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", NPLDetail.Master_ID, Company.KDT.SHARE.Components.LoaiKhaiBao.NguyenPhuLieu);
                    //                             List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    //                             if (listLog.Count > 0)
                    //                             {
                    //                                 long idLog = listLog[0].IDLog;
                    //                                 string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                    //                                 long idDK = listLog[0].ID_DK;
                    //                                 string guidstr = listLog[0].GUIDSTR_DK;
                    //                                 string userKhaiBao = listLog[0].UserNameKhaiBao;
                    //                                 DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                    //                                 string userSuaDoi = GlobalSettings.UserLog;
                    //                                 DateTime ngaySuaDoi = DateTime.Now;
                    //                                 string ghiChu = listLog[0].GhiChu;
                    //                                 bool isDelete = listLog[0].IsDelete;
                    //                                 Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                    //                                                                                             userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    //                             }
                    //                         }
                    //                         catch (Exception ex)
                    //                         { 
                    //                             ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                    //                             return;
                    //                         }
                    //                     }        
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }         
        }
        private bool checkNPLExit(string maNPL) 
        {
            if (NPLCollection != null)
            {
                foreach (HangDuaVao npl in NPLCollection)
                {
                    if (npl.Ma == maNPL) return true;
                }
                
            }
            return false;
        }
        private void NguyenPhuLieuEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                txtMa.Focus();
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
                cmbMucDich.DataSource = MucDichSuDung.SelectAll().Tables[0];
                cmbMucDich.DisplayMember = "TenLoaiMucDich";
                cmbMucDich.ValueMember = "ID";
                if (this.OpenType == OpenFormType.Insert)
                {
                    // cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
                }
                else if (this.OpenType == OpenFormType.Edit)
                {
                    if (this.NPLDetail != null)
                    {

                        txtMa.Enabled = false;
                        txtMa.Text = this.NPLDetail.Ma;
                        txtTen.Text = this.NPLDetail.Ten;
                        txtMaHS.Text = this.NPLDetail.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLDetail.DVT_ID;
                        cmbMucDich.SelectedValue = this.NPLDetail.MucDich;
                        cmbLoaiHang.SelectedValue = this.NPLDetail.LoaiNPL;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            try
            {
                if (!MaHS.Validate(txtMaHS.Text, 10))
                {
                    error.SetIconPadding(txtMaHS, -8);
                    //error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                    }
                    else
                    {
                        error.SetError(txtMaHS, "HS code is invalid");
                    }
                }
                else
                {
                    error.SetError(txtMaHS, string.Empty);
                }
                this.MoTa = MaHS.CheckExist(txtMaHS.Text);
                if (this.MoTa == "")
                {
                    error.SetIconPadding(txtMaHS, -8);
                    //error.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        error.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                    }
                    else
                    {
                        error.SetError(txtMaHS, "HS code haven't exist in HS list.");
                    }
                }
                else
                {
                    //Janus.Windows.Common.SuperTipSettings s = new Janus.Windows.Common.SuperTipSettings();
                    //s.HeaderText = "Mô tả";
                    //s.Text = this.MoTa;
                    //janusSuperTip1.Show(s);
                    error.SetError(txtMaHS, string.Empty);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }
    }
}