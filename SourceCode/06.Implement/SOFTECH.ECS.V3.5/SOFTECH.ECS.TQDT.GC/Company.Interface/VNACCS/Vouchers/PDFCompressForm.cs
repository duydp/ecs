﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using iTextSharp.text.pdf;
using System.Diagnostics;
using iTextSharp.text;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class PDFCompressForm : Company.Interface.BaseForm
    {
        public String FileName = "";
        public String srcFilename = "";
        public String dstFilename = "";
        public String dstFilenameConvert = "";
        public PDFCompressForm()
        {
            InitializeComponent();
        }

        private void txtSelectFile_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.FileName = "";
                openFileDialog1.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.bmp;*.dib;*.rle;*.tiff;*.png ;*.jpe, *.jfif,)|*.jpg;*.jpeg;*.gif;*.bmp;*.dib;*.rle;*.tiff;*.tif;*.png;*.jpe;*.jfif;|"
                        + "Text files (*.txt;*.xml;*xsl)|*.txt;*.xml;*xsl|"
                        + "Application File (*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;)|*.csv;*.doc;*.mdb;*.pdf;*.ppt;*.xls;";
                openFileDialog1.ShowDialog(this);
                txtSelectFile.Text = openFileDialog1.FileName;
                FileName = openFileDialog1.SafeFileName;
                srcFilename = openFileDialog1.FileName;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void txtFolderCompress_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                FolderBrowserDialog targetDirectory = new FolderBrowserDialog();
                if (targetDirectory.ShowDialog() != DialogResult.OK)
                    return;
                txtFolderCompress.Text = targetDirectory.SelectedPath;
                dstFilename = targetDirectory.SelectedPath +"\\"+ FileName +"_Compress.pdf";
                dstFilenameConvert = targetDirectory.SelectedPath + "\\" + FileName + "_Convert_Compress.pdf";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static void ConvertImageToPdf(string srcFilename, string dstFilename)
        {
            iTextSharp.text.Rectangle pageSize = null;

            using (var srcImage = new Bitmap(srcFilename))
            {
                pageSize = new iTextSharp.text.Rectangle(0, 0, srcImage.Width, srcImage.Height);
            }
            using (var ms = new MemoryStream())
            {
                var document = new iTextSharp.text.Document(pageSize, 0, 0, 0, 0);
                iTextSharp.text.pdf.PdfWriter.GetInstance(document, ms).SetFullCompression();
                document.Open();
                var image = iTextSharp.text.Image.GetInstance(srcFilename);
                document.Add(image);
                document.Close();

                File.WriteAllBytes(dstFilename, ms.ToArray());
            }
        }
        public static void ConvertImageToPdfNew(string source, string output)
        {
            iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(source);
            using (FileStream fs = new FileStream(output, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                using (Document doc = new Document(image))
                {
                    using (PdfWriter writer = PdfWriter.GetInstance(doc, fs))
                    {
                        doc.Open();
                        image.SetAbsolutePosition(0, 0);
                        writer.DirectContent.AddImage(image);
                        doc.Close();
                    }
                }
            }
        }
        public static bool CheckExitsSignature (string source)
        {
            PdfReader reader = new PdfReader(source);
            List<string> blanks = reader.AcroFields.GetSignatureNames();
            if (blanks.Count == 0)
            {
                return true;
            }
            return false;
        }
        private void btnCompression_Click(object sender, EventArgs e)
        {
            try
            {
                string ex = Path.GetExtension(txtSelectFile.Text.ToString());       
                if (ex == ".pdf")
                {
                    if (CheckExitsSignature(srcFilename))
                    {
                        ShowMessage("File PDF đã được ký chữ ký số trước đó . ", false);
                        return;
                    }
                    PdfReader reader = new PdfReader(txtSelectFile.Text.ToString());
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(dstFilename, FileMode.Create), PdfWriter.VERSION_1_5);
                    PdfWriter writer = stamper.Writer;
                    writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_7);
                    writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                    int pageNum = reader.NumberOfPages;
                    for (int i = 1; i <= pageNum; i++)
                    {
                        reader.SetPageContent(i, reader.GetPageContent(i));
                    }
                    stamper.SetFullCompression();
                    stamper.Close();
                    ShowMessage("Nén thành công ", false);        
                }
                else if (ex == ".jpg" || ex == ".jpeg" || ex == ".jpe" || ex == ".gif" || ex == ".jfif" || ex == ".bmp" || ex == ".dib" || ex == ".rle" || ex == ".png" || ex == ".tiff")
                {
                    ConvertImageToPdf(srcFilename, dstFilename);

                    PdfReader reader = new PdfReader(dstFilename);
                    PdfStamper stamper = new PdfStamper(reader, new FileStream(dstFilenameConvert, FileMode.Create), PdfWriter.VERSION_1_5);
                    PdfWriter writer = stamper.Writer;
                    writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_7);
                    writer.CompressionLevel = PdfStream.BEST_COMPRESSION;

                    int pageNum = reader.NumberOfPages;
                    for (int i = 1; i <= pageNum; i++)
                    {
                        reader.SetPageContent(i, reader.GetPageContent(i));
                    }
                    stamper.SetFullCompression();
                    stamper.Close();
                    ShowMessage("Chuyển đổi File ảnh thành PDF và Nén thành công .", false);        
                }
                else
                {
                    ShowMessage("Định dạng File đã chọn không hỗ trợ Nén File .",false);
                } 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnOpenFolder_Click(object sender, EventArgs e)
        {
            if (String.Empty != txtFolderCompress.Text)
            {
                Process.Start(txtFolderCompress.Text);
            }
        }
    }
}
