
/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/12/2012 4:40:12 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[p_SXXK_Convert_MaHS8SoAuto]'
GO

CREATE PROC [dbo].[p_SXXK_Convert_MaHS8SoAuto]  
(  
 @MaHaiQuan VARCHAR(10),  
 @MaDoanhNghiep VARCHAR(20)
)  
AS  
begin  
  
/*Updated by HungTQ, 12/09/2012*/  
  
--Update HS 8 so NPL
UPDATE dbo.t_KDT_SXXK_NguyenPhuLieu SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_SXXK_NguyenPhuLieu SET MaHS = LEFT(MaHS, 8) WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep
--Update HS 8 so SP
UPDATE dbo.t_KDT_SXXK_SanPham SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_SXXK_SanPham SET MaHS = LEFT(MaHS, 8) WHERE MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep
--Update HS 8 so HangMauDich
UPDATE dbo.t_KDT_HangMauDich SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_SXXK_HangMauDich SET MaHS = LEFT(MaHS, 8) WHERE MaHaiQuan = @MaHaiQuan
--Update HS 8 so hang trong Chung tu kem
UPDATE dbo.t_KDT_HangGiayPhepDetail SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HangVanDonDetail SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HangCoDetail SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = LEFT(MaHS, 8)
UPDATE dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = LEFT(MaHS, 8)
--2 bang duoi day co ma HS nhung chua tam thoi chua cap nhat.
--dbo.t_SXXK_HangMauDichDieuChinh
--dbo.t_KDT_SXXK_DMHH
  
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_Convert_MaHS8So]'
GO

CREATE PROC [dbo].[p_SXXK_Convert_MaHS8So]
(
	@MaHaiQuan	VARCHAR(10),
	@MaDoanhNghiep	VARCHAR(20),
	@Ma	NVARCHAR(250),
	@DVT_ID	NVARCHAR(50),
	@MaHSCu	VARCHAR(10),
	@MaHSMoi	VARCHAR(10)
)
AS
begin

/*Updated by HungTQ, 12/09/2012*/

--Update HS 8 so NPL
UPDATE dbo.t_KDT_SXXK_NguyenPhuLieu SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_SXXK_NguyenPhuLieu SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep
--Update HS 8 so SP
UPDATE dbo.t_KDT_SXXK_SanPham SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_SXXK_SanPham SET MaHS = @MaHSMoi WHERE Ma = @Ma AND MaHaiQuan = @MaHaiQuan AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep
--Update HS 8 so HangMauDich
UPDATE dbo.t_KDT_HangMauDich SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_SXXK_HangMauDich SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND MaHaiQuan = @MaHaiQuan AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND MaHaiQuan = @MaHaiQuan
--Update HS 8 so hang trong Chung tu kem
UPDATE dbo.t_KDT_HangGiayPhepDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_KDT_HangVanDonDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_KDT_HangCoDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
UPDATE dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu
--2 bang duoi day co ma HS nhung chua tam thoi chua cap nhat.
--dbo.t_SXXK_HangMauDichDieuChinh
--dbo.t_KDT_SXXK_DMHH

end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SXXK_SelectHang]'
GO

CREATE PROC [dbo].[p_SXXK_SelectHang]      
(      
 @MaHaiQuan VARCHAR(10),      
 @MaDoanhNghiep VARCHAR(20),      
 @MaLoaiHinh VARCHAR(5)      
)      
AS      
      
/*Updateed by Hungtq, 08/09/2012*/      
      
DECLARE @sql VARCHAR(max), @TableName VARCHAR(50)     
      
SET @sql =      
' SELECT t.MaHaiQuan, t.MaDoanhNghiep, h.MaPhu AS Ma, h.TenHang AS Ten, h.MaHS, h.DVT_ID '      
+ ' FROM dbo.t_KDT_ToKhaiMauDich t INNER JOIN dbo.t_KDT_HangMauDich h '      
+ ' ON t.ID = h.TKMD_ID '      
+ ' WHERE t.MaHaiQuan = ''' + @MaHaiQuan + ''' AND t.MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''      
+ ' AND t.TrangThaiXuLy = 1 AND t.MaLoaiHinh LIKE ''' + @MaLoaiHinh + '%'''      
    
SET @sql = @sql + ' UNION '      
    
IF(@MaLoaiHinh = 'N')        
 SET @TableName = 't_SXXK_NguyenPhuLieu'    
ELSE    
 SET @TableName = 't_SXXK_SanPham'    
    
SET @sql = @sql     
+ ' SELECT MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID '    
+ ' FROM dbo.' + @TableName    
+ ' WHERE MaHaiQuan = ''' + @MaHaiQuan + ''' AND MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''    

SET @sql = 'SELECT distinct MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID from (' + @sql + ') as v'
    
--PRINT @sql      
EXEC (@sql)      
      
--exec p_SXXK_SelectHang 'C34C', '0100101308', 'N'      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.6') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.6', getdate(), N'')
	end 