-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Insert]
	@TKMD_ID bigint,
	@SoToKhaiBoSung numeric(12, 0),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@NhomXuLyHoSoID int,
	@PhanLoaiXuatNhapKhau varchar(1),
	@SoToKhai numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@NgayKhaiBao datetime,
	@NgayCapPhep datetime,
	@ThoiHanTaiNhapTaiXuat datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@SoDienThoaiNguoiKhai varchar(20),
	@MaLyDoKhaiBoSung varchar(1),
	@MaTienTeTienThue varchar(3),
	@MaNganHangTraThueThay varchar(110),
	@NamPhatHanhHanMuc numeric(4, 0),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoChungTuPhatHanhHanMuc varchar(10),
	@MaXacDinhThoiHanNopThue varchar(1),
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh numeric(4, 0),
	@KyHieuPhatHanhChungTuBaoLanh varchar(10),
	@SoHieuPhatHanhChungTuBaoLanh varchar(10),
	@MaTienTeTruocKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiTruocKhiKhaiBoSung numeric(9, 0),
	@MaTienTeSauKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiSauKhiKhaiBoSung numeric(9, 0),
	@SoQuanLyTrongNoiBoDoanhNghiep nvarchar(20),
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung nvarchar(300),
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung nvarchar(300),
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoThongBao numeric(12, 0),
	@NgayHoanThanhKiemTra datetime,
	@GioHoanThanhKiemTra datetime,
	@NgayDangKyKhaiBoSung datetime,
	@GioDangKyKhaiBoSung datetime,
	@DauHieuBaoQua60Ngay varchar(1),
	@MaHetThoiHan varchar(1),
	@MaDaiLyHaiQuan varchar(5),
	@TenDaiLyHaiQuan nvarchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@PhanLoaiNopThue varchar(1),
	@NgayHieuLucChungTu datetime,
	@SoNgayNopThue numeric(3, 0),
	@SoNgayNopThueDanhChoVATHangHoaDacBiet numeric(3, 0),
	@HienThiTongSoTienTangGiamThueXuatNhapKhau varchar(1),
	@TongSoTienTangGiamThueXuatNhapKhau numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau varchar(3),
	@TongSoTrangToKhaiBoSung numeric(2, 0),
	@TongSoDongHangToKhaiBoSung numeric(2, 0),
	@LyDo nvarchar(600),
	@TenNguoiPhuTrach nvarchar(108),
	@TenTruongDonViHaiQuan nvarchar(108),
	@NgayDangKyDuLieu datetime,
	@GioDangKyDuLieu datetime,
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
(
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
)
VALUES 
(
	@TKMD_ID,
	@SoToKhaiBoSung,
	@CoQuanHaiQuan,
	@NhomXuLyHoSo,
	@NhomXuLyHoSoID,
	@PhanLoaiXuatNhapKhau,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayKhaiBao,
	@NgayCapPhep,
	@ThoiHanTaiNhapTaiXuat,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@MaBuuChinh,
	@DiaChiNguoiKhai,
	@SoDienThoaiNguoiKhai,
	@MaLyDoKhaiBoSung,
	@MaTienTeTienThue,
	@MaNganHangTraThueThay,
	@NamPhatHanhHanMuc,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoChungTuPhatHanhHanMuc,
	@MaXacDinhThoiHanNopThue,
	@MaNganHangBaoLanh,
	@NamPhatHanhBaoLanh,
	@KyHieuPhatHanhChungTuBaoLanh,
	@SoHieuPhatHanhChungTuBaoLanh,
	@MaTienTeTruocKhiKhaiBoSung,
	@TyGiaHoiDoaiTruocKhiKhaiBoSung,
	@MaTienTeSauKhiKhaiBoSung,
	@TyGiaHoiDoaiSauKhiKhaiBoSung,
	@SoQuanLyTrongNoiBoDoanhNghiep,
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
	@SoTiepNhan,
	@NgayTiepNhan,
	@PhanLuong,
	@HuongDan,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@SoThongBao,
	@NgayHoanThanhKiemTra,
	@GioHoanThanhKiemTra,
	@NgayDangKyKhaiBoSung,
	@GioDangKyKhaiBoSung,
	@DauHieuBaoQua60Ngay,
	@MaHetThoiHan,
	@MaDaiLyHaiQuan,
	@TenDaiLyHaiQuan,
	@MaNhanVienHaiQuan,
	@PhanLoaiNopThue,
	@NgayHieuLucChungTu,
	@SoNgayNopThue,
	@SoNgayNopThueDanhChoVATHangHoaDacBiet,
	@HienThiTongSoTienTangGiamThueXuatNhapKhau,
	@TongSoTienTangGiamThueXuatNhapKhau,
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
	@TongSoTrangToKhaiBoSung,
	@TongSoDongHangToKhaiBoSung,
	@LyDo,
	@TenNguoiPhuTrach,
	@TenTruongDonViHaiQuan,
	@NgayDangKyDuLieu,
	@GioDangKyDuLieu,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoToKhaiBoSung numeric(12, 0),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@NhomXuLyHoSoID int,
	@PhanLoaiXuatNhapKhau varchar(1),
	@SoToKhai numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@NgayKhaiBao datetime,
	@NgayCapPhep datetime,
	@ThoiHanTaiNhapTaiXuat datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@SoDienThoaiNguoiKhai varchar(20),
	@MaLyDoKhaiBoSung varchar(1),
	@MaTienTeTienThue varchar(3),
	@MaNganHangTraThueThay varchar(110),
	@NamPhatHanhHanMuc numeric(4, 0),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoChungTuPhatHanhHanMuc varchar(10),
	@MaXacDinhThoiHanNopThue varchar(1),
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh numeric(4, 0),
	@KyHieuPhatHanhChungTuBaoLanh varchar(10),
	@SoHieuPhatHanhChungTuBaoLanh varchar(10),
	@MaTienTeTruocKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiTruocKhiKhaiBoSung numeric(9, 0),
	@MaTienTeSauKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiSauKhiKhaiBoSung numeric(9, 0),
	@SoQuanLyTrongNoiBoDoanhNghiep nvarchar(20),
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung nvarchar(300),
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung nvarchar(300),
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoThongBao numeric(12, 0),
	@NgayHoanThanhKiemTra datetime,
	@GioHoanThanhKiemTra datetime,
	@NgayDangKyKhaiBoSung datetime,
	@GioDangKyKhaiBoSung datetime,
	@DauHieuBaoQua60Ngay varchar(1),
	@MaHetThoiHan varchar(1),
	@MaDaiLyHaiQuan varchar(5),
	@TenDaiLyHaiQuan nvarchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@PhanLoaiNopThue varchar(1),
	@NgayHieuLucChungTu datetime,
	@SoNgayNopThue numeric(3, 0),
	@SoNgayNopThueDanhChoVATHangHoaDacBiet numeric(3, 0),
	@HienThiTongSoTienTangGiamThueXuatNhapKhau varchar(1),
	@TongSoTienTangGiamThueXuatNhapKhau numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau varchar(3),
	@TongSoTrangToKhaiBoSung numeric(2, 0),
	@TongSoDongHangToKhaiBoSung numeric(2, 0),
	@LyDo nvarchar(600),
	@TenNguoiPhuTrach nvarchar(108),
	@TenTruongDonViHaiQuan nvarchar(108),
	@NgayDangKyDuLieu datetime,
	@GioDangKyDuLieu datetime,
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoToKhaiBoSung] = @SoToKhaiBoSung,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[NhomXuLyHoSo] = @NhomXuLyHoSo,
	[NhomXuLyHoSoID] = @NhomXuLyHoSoID,
	[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayKhaiBao] = @NgayKhaiBao,
	[NgayCapPhep] = @NgayCapPhep,
	[ThoiHanTaiNhapTaiXuat] = @ThoiHanTaiNhapTaiXuat,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
	[MaLyDoKhaiBoSung] = @MaLyDoKhaiBoSung,
	[MaTienTeTienThue] = @MaTienTeTienThue,
	[MaNganHangTraThueThay] = @MaNganHangTraThueThay,
	[NamPhatHanhHanMuc] = @NamPhatHanhHanMuc,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoChungTuPhatHanhHanMuc] = @SoChungTuPhatHanhHanMuc,
	[MaXacDinhThoiHanNopThue] = @MaXacDinhThoiHanNopThue,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
	[KyHieuPhatHanhChungTuBaoLanh] = @KyHieuPhatHanhChungTuBaoLanh,
	[SoHieuPhatHanhChungTuBaoLanh] = @SoHieuPhatHanhChungTuBaoLanh,
	[MaTienTeTruocKhiKhaiBoSung] = @MaTienTeTruocKhiKhaiBoSung,
	[TyGiaHoiDoaiTruocKhiKhaiBoSung] = @TyGiaHoiDoaiTruocKhiKhaiBoSung,
	[MaTienTeSauKhiKhaiBoSung] = @MaTienTeSauKhiKhaiBoSung,
	[TyGiaHoiDoaiSauKhiKhaiBoSung] = @TyGiaHoiDoaiSauKhiKhaiBoSung,
	[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung] = @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung] = @GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[SoThongBao] = @SoThongBao,
	[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra,
	[GioHoanThanhKiemTra] = @GioHoanThanhKiemTra,
	[NgayDangKyKhaiBoSung] = @NgayDangKyKhaiBoSung,
	[GioDangKyKhaiBoSung] = @GioDangKyKhaiBoSung,
	[DauHieuBaoQua60Ngay] = @DauHieuBaoQua60Ngay,
	[MaHetThoiHan] = @MaHetThoiHan,
	[MaDaiLyHaiQuan] = @MaDaiLyHaiQuan,
	[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
	[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
	[PhanLoaiNopThue] = @PhanLoaiNopThue,
	[NgayHieuLucChungTu] = @NgayHieuLucChungTu,
	[SoNgayNopThue] = @SoNgayNopThue,
	[SoNgayNopThueDanhChoVATHangHoaDacBiet] = @SoNgayNopThueDanhChoVATHangHoaDacBiet,
	[HienThiTongSoTienTangGiamThueXuatNhapKhau] = @HienThiTongSoTienTangGiamThueXuatNhapKhau,
	[TongSoTienTangGiamThueXuatNhapKhau] = @TongSoTienTangGiamThueXuatNhapKhau,
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau] = @MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
	[TongSoTrangToKhaiBoSung] = @TongSoTrangToKhaiBoSung,
	[TongSoDongHangToKhaiBoSung] = @TongSoDongHangToKhaiBoSung,
	[LyDo] = @LyDo,
	[TenNguoiPhuTrach] = @TenNguoiPhuTrach,
	[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
	[NgayDangKyDuLieu] = @NgayDangKyDuLieu,
	[GioDangKyDuLieu] = @GioDangKyDuLieu,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoToKhaiBoSung numeric(12, 0),
	@CoQuanHaiQuan varchar(6),
	@NhomXuLyHoSo varchar(2),
	@NhomXuLyHoSoID int,
	@PhanLoaiXuatNhapKhau varchar(1),
	@SoToKhai numeric(12, 0),
	@MaLoaiHinh varchar(3),
	@NgayKhaiBao datetime,
	@NgayCapPhep datetime,
	@ThoiHanTaiNhapTaiXuat datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@SoDienThoaiNguoiKhai varchar(20),
	@MaLyDoKhaiBoSung varchar(1),
	@MaTienTeTienThue varchar(3),
	@MaNganHangTraThueThay varchar(110),
	@NamPhatHanhHanMuc numeric(4, 0),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoChungTuPhatHanhHanMuc varchar(10),
	@MaXacDinhThoiHanNopThue varchar(1),
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh numeric(4, 0),
	@KyHieuPhatHanhChungTuBaoLanh varchar(10),
	@SoHieuPhatHanhChungTuBaoLanh varchar(10),
	@MaTienTeTruocKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiTruocKhiKhaiBoSung numeric(9, 0),
	@MaTienTeSauKhiKhaiBoSung varchar(3),
	@TyGiaHoiDoaiSauKhiKhaiBoSung numeric(9, 0),
	@SoQuanLyTrongNoiBoDoanhNghiep nvarchar(20),
	@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung nvarchar(300),
	@GhiChuNoiDungLienQuanSauKhiKhaiBoSung nvarchar(300),
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@SoThongBao numeric(12, 0),
	@NgayHoanThanhKiemTra datetime,
	@GioHoanThanhKiemTra datetime,
	@NgayDangKyKhaiBoSung datetime,
	@GioDangKyKhaiBoSung datetime,
	@DauHieuBaoQua60Ngay varchar(1),
	@MaHetThoiHan varchar(1),
	@MaDaiLyHaiQuan varchar(5),
	@TenDaiLyHaiQuan nvarchar(50),
	@MaNhanVienHaiQuan varchar(5),
	@PhanLoaiNopThue varchar(1),
	@NgayHieuLucChungTu datetime,
	@SoNgayNopThue numeric(3, 0),
	@SoNgayNopThueDanhChoVATHangHoaDacBiet numeric(3, 0),
	@HienThiTongSoTienTangGiamThueXuatNhapKhau varchar(1),
	@TongSoTienTangGiamThueXuatNhapKhau numeric(11, 0),
	@MaTienTeTongSoTienTangGiamThueXuatNhapKhau varchar(3),
	@TongSoTrangToKhaiBoSung numeric(2, 0),
	@TongSoDongHangToKhaiBoSung numeric(2, 0),
	@LyDo nvarchar(600),
	@TenNguoiPhuTrach nvarchar(108),
	@TenTruongDonViHaiQuan nvarchar(108),
	@NgayDangKyDuLieu datetime,
	@GioDangKyDuLieu datetime,
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoToKhaiBoSung] = @SoToKhaiBoSung,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[NhomXuLyHoSo] = @NhomXuLyHoSo,
			[NhomXuLyHoSoID] = @NhomXuLyHoSoID,
			[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayKhaiBao] = @NgayKhaiBao,
			[NgayCapPhep] = @NgayCapPhep,
			[ThoiHanTaiNhapTaiXuat] = @ThoiHanTaiNhapTaiXuat,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
			[MaLyDoKhaiBoSung] = @MaLyDoKhaiBoSung,
			[MaTienTeTienThue] = @MaTienTeTienThue,
			[MaNganHangTraThueThay] = @MaNganHangTraThueThay,
			[NamPhatHanhHanMuc] = @NamPhatHanhHanMuc,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoChungTuPhatHanhHanMuc] = @SoChungTuPhatHanhHanMuc,
			[MaXacDinhThoiHanNopThue] = @MaXacDinhThoiHanNopThue,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
			[KyHieuPhatHanhChungTuBaoLanh] = @KyHieuPhatHanhChungTuBaoLanh,
			[SoHieuPhatHanhChungTuBaoLanh] = @SoHieuPhatHanhChungTuBaoLanh,
			[MaTienTeTruocKhiKhaiBoSung] = @MaTienTeTruocKhiKhaiBoSung,
			[TyGiaHoiDoaiTruocKhiKhaiBoSung] = @TyGiaHoiDoaiTruocKhiKhaiBoSung,
			[MaTienTeSauKhiKhaiBoSung] = @MaTienTeSauKhiKhaiBoSung,
			[TyGiaHoiDoaiSauKhiKhaiBoSung] = @TyGiaHoiDoaiSauKhiKhaiBoSung,
			[SoQuanLyTrongNoiBoDoanhNghiep] = @SoQuanLyTrongNoiBoDoanhNghiep,
			[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung] = @GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
			[GhiChuNoiDungLienQuanSauKhiKhaiBoSung] = @GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[SoThongBao] = @SoThongBao,
			[NgayHoanThanhKiemTra] = @NgayHoanThanhKiemTra,
			[GioHoanThanhKiemTra] = @GioHoanThanhKiemTra,
			[NgayDangKyKhaiBoSung] = @NgayDangKyKhaiBoSung,
			[GioDangKyKhaiBoSung] = @GioDangKyKhaiBoSung,
			[DauHieuBaoQua60Ngay] = @DauHieuBaoQua60Ngay,
			[MaHetThoiHan] = @MaHetThoiHan,
			[MaDaiLyHaiQuan] = @MaDaiLyHaiQuan,
			[TenDaiLyHaiQuan] = @TenDaiLyHaiQuan,
			[MaNhanVienHaiQuan] = @MaNhanVienHaiQuan,
			[PhanLoaiNopThue] = @PhanLoaiNopThue,
			[NgayHieuLucChungTu] = @NgayHieuLucChungTu,
			[SoNgayNopThue] = @SoNgayNopThue,
			[SoNgayNopThueDanhChoVATHangHoaDacBiet] = @SoNgayNopThueDanhChoVATHangHoaDacBiet,
			[HienThiTongSoTienTangGiamThueXuatNhapKhau] = @HienThiTongSoTienTangGiamThueXuatNhapKhau,
			[TongSoTienTangGiamThueXuatNhapKhau] = @TongSoTienTangGiamThueXuatNhapKhau,
			[MaTienTeTongSoTienTangGiamThueXuatNhapKhau] = @MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
			[TongSoTrangToKhaiBoSung] = @TongSoTrangToKhaiBoSung,
			[TongSoDongHangToKhaiBoSung] = @TongSoDongHangToKhaiBoSung,
			[LyDo] = @LyDo,
			[TenNguoiPhuTrach] = @TenNguoiPhuTrach,
			[TenTruongDonViHaiQuan] = @TenTruongDonViHaiQuan,
			[NgayDangKyDuLieu] = @NgayDangKyDuLieu,
			[GioDangKyDuLieu] = @GioDangKyDuLieu,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
		(
			[TKMD_ID],
			[SoToKhaiBoSung],
			[CoQuanHaiQuan],
			[NhomXuLyHoSo],
			[NhomXuLyHoSoID],
			[PhanLoaiXuatNhapKhau],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayKhaiBao],
			[NgayCapPhep],
			[ThoiHanTaiNhapTaiXuat],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[MaBuuChinh],
			[DiaChiNguoiKhai],
			[SoDienThoaiNguoiKhai],
			[MaLyDoKhaiBoSung],
			[MaTienTeTienThue],
			[MaNganHangTraThueThay],
			[NamPhatHanhHanMuc],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoChungTuPhatHanhHanMuc],
			[MaXacDinhThoiHanNopThue],
			[MaNganHangBaoLanh],
			[NamPhatHanhBaoLanh],
			[KyHieuPhatHanhChungTuBaoLanh],
			[SoHieuPhatHanhChungTuBaoLanh],
			[MaTienTeTruocKhiKhaiBoSung],
			[TyGiaHoiDoaiTruocKhiKhaiBoSung],
			[MaTienTeSauKhiKhaiBoSung],
			[TyGiaHoiDoaiSauKhiKhaiBoSung],
			[SoQuanLyTrongNoiBoDoanhNghiep],
			[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
			[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
			[SoTiepNhan],
			[NgayTiepNhan],
			[PhanLuong],
			[HuongDan],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[SoThongBao],
			[NgayHoanThanhKiemTra],
			[GioHoanThanhKiemTra],
			[NgayDangKyKhaiBoSung],
			[GioDangKyKhaiBoSung],
			[DauHieuBaoQua60Ngay],
			[MaHetThoiHan],
			[MaDaiLyHaiQuan],
			[TenDaiLyHaiQuan],
			[MaNhanVienHaiQuan],
			[PhanLoaiNopThue],
			[NgayHieuLucChungTu],
			[SoNgayNopThue],
			[SoNgayNopThueDanhChoVATHangHoaDacBiet],
			[HienThiTongSoTienTangGiamThueXuatNhapKhau],
			[TongSoTienTangGiamThueXuatNhapKhau],
			[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
			[TongSoTrangToKhaiBoSung],
			[TongSoDongHangToKhaiBoSung],
			[LyDo],
			[TenNguoiPhuTrach],
			[TenTruongDonViHaiQuan],
			[NgayDangKyDuLieu],
			[GioDangKyDuLieu],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoToKhaiBoSung,
			@CoQuanHaiQuan,
			@NhomXuLyHoSo,
			@NhomXuLyHoSoID,
			@PhanLoaiXuatNhapKhau,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayKhaiBao,
			@NgayCapPhep,
			@ThoiHanTaiNhapTaiXuat,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@MaBuuChinh,
			@DiaChiNguoiKhai,
			@SoDienThoaiNguoiKhai,
			@MaLyDoKhaiBoSung,
			@MaTienTeTienThue,
			@MaNganHangTraThueThay,
			@NamPhatHanhHanMuc,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoChungTuPhatHanhHanMuc,
			@MaXacDinhThoiHanNopThue,
			@MaNganHangBaoLanh,
			@NamPhatHanhBaoLanh,
			@KyHieuPhatHanhChungTuBaoLanh,
			@SoHieuPhatHanhChungTuBaoLanh,
			@MaTienTeTruocKhiKhaiBoSung,
			@TyGiaHoiDoaiTruocKhiKhaiBoSung,
			@MaTienTeSauKhiKhaiBoSung,
			@TyGiaHoiDoaiSauKhiKhaiBoSung,
			@SoQuanLyTrongNoiBoDoanhNghiep,
			@GhiChuNoiDungLienQuanTruocKhiKhaiBoSung,
			@GhiChuNoiDungLienQuanSauKhiKhaiBoSung,
			@SoTiepNhan,
			@NgayTiepNhan,
			@PhanLuong,
			@HuongDan,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@SoThongBao,
			@NgayHoanThanhKiemTra,
			@GioHoanThanhKiemTra,
			@NgayDangKyKhaiBoSung,
			@GioDangKyKhaiBoSung,
			@DauHieuBaoQua60Ngay,
			@MaHetThoiHan,
			@MaDaiLyHaiQuan,
			@TenDaiLyHaiQuan,
			@MaNhanVienHaiQuan,
			@PhanLoaiNopThue,
			@NgayHieuLucChungTu,
			@SoNgayNopThue,
			@SoNgayNopThueDanhChoVATHangHoaDacBiet,
			@HienThiTongSoTienTangGiamThueXuatNhapKhau,
			@TongSoTienTangGiamThueXuatNhapKhau,
			@MaTienTeTongSoTienTangGiamThueXuatNhapKhau,
			@TongSoTrangToKhaiBoSung,
			@TongSoDongHangToKhaiBoSung,
			@LyDo,
			@TenNguoiPhuTrach,
			@TenTruongDonViHaiQuan,
			@NgayDangKyDuLieu,
			@GioDangKyDuLieu,
			@TrangThaiXuLy
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoToKhaiBoSung],
	[CoQuanHaiQuan],
	[NhomXuLyHoSo],
	[NhomXuLyHoSoID],
	[PhanLoaiXuatNhapKhau],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayKhaiBao],
	[NgayCapPhep],
	[ThoiHanTaiNhapTaiXuat],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinh],
	[DiaChiNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[MaLyDoKhaiBoSung],
	[MaTienTeTienThue],
	[MaNganHangTraThueThay],
	[NamPhatHanhHanMuc],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoChungTuPhatHanhHanMuc],
	[MaXacDinhThoiHanNopThue],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuPhatHanhChungTuBaoLanh],
	[SoHieuPhatHanhChungTuBaoLanh],
	[MaTienTeTruocKhiKhaiBoSung],
	[TyGiaHoiDoaiTruocKhiKhaiBoSung],
	[MaTienTeSauKhiKhaiBoSung],
	[TyGiaHoiDoaiSauKhiKhaiBoSung],
	[SoQuanLyTrongNoiBoDoanhNghiep],
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung],
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[SoThongBao],
	[NgayHoanThanhKiemTra],
	[GioHoanThanhKiemTra],
	[NgayDangKyKhaiBoSung],
	[GioDangKyKhaiBoSung],
	[DauHieuBaoQua60Ngay],
	[MaHetThoiHan],
	[MaDaiLyHaiQuan],
	[TenDaiLyHaiQuan],
	[MaNhanVienHaiQuan],
	[PhanLoaiNopThue],
	[NgayHieuLucChungTu],
	[SoNgayNopThue],
	[SoNgayNopThueDanhChoVATHangHoaDacBiet],
	[HienThiTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTienTangGiamThueXuatNhapKhau],
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau],
	[TongSoTrangToKhaiBoSung],
	[TongSoDongHangToKhaiBoSung],
	[LyDo],
	[TenNguoiPhuTrach],
	[TenTruongDonViHaiQuan],
	[NgayDangKyDuLieu],
	[GioDangKyDuLieu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]	

GO

