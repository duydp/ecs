using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAF8080 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute TYS { get; set; }
public PropertiesAttribute A01 { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute A03 { get; set; }
public PropertiesAttribute A04 { get; set; }
public PropertiesAttribute A05 { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute A10 { get; set; }
public PropertiesAttribute A11 { get; set; }

public VAF8080()
        {
TYS = new PropertiesAttribute(1, typeof(string));
A01 = new PropertiesAttribute(11, typeof(string));
A02 = new PropertiesAttribute(210, typeof(string));
A03 = new PropertiesAttribute(4, typeof(int));
A04 = new PropertiesAttribute(10, typeof(string));
A05 = new PropertiesAttribute(10, typeof(string));
A06 = new PropertiesAttribute(1, typeof(int));
A07 = new PropertiesAttribute(13, typeof(string));
A08 = new PropertiesAttribute(300, typeof(string));
A09 = new PropertiesAttribute(8, typeof(DateTime));
A10 = new PropertiesAttribute(8, typeof(DateTime));
A11 = new PropertiesAttribute(13, typeof(int));
TongSoByte = 613;
}

}public partial class EnumGroupID
    {
}

}
