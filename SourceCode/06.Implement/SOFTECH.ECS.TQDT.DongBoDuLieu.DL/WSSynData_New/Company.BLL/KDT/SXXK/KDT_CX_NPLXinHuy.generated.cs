using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class KDT_CX_NPLXinHuy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_id { set; get; }
		public decimal SoToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaHang { set; get; }
		public string TenHang { set; get; }
		public decimal LuongTieuHuy { set; get; }
		public int LoaiHang { set; get; }
		public string DVT { set; get; }
		public string Temp { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_CX_NPLXinHuy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_CX_NPLXinHuy> collection = new List<KDT_CX_NPLXinHuy>();
			while (reader.Read())
			{
				KDT_CX_NPLXinHuy entity = new KDT_CX_NPLXinHuy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_id"))) entity.Master_id = reader.GetInt64(reader.GetOrdinal("Master_id"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTieuHuy"))) entity.LuongTieuHuy = reader.GetDecimal(reader.GetOrdinal("LuongTieuHuy"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHang"))) entity.LoaiHang = reader.GetInt32(reader.GetOrdinal("LoaiHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("Temp"))) entity.Temp = reader.GetString(reader.GetOrdinal("Temp"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_CX_NPLXinHuy> collection, long id)
        {
            foreach (KDT_CX_NPLXinHuy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_CX_NPLXinHuy VALUES(@Master_id, @SoToKhai, @MaLoaiHinh, @NgayDangKy, @MaHaiQuan, @MaHang, @TenHang, @LuongTieuHuy, @LoaiHang, @DVT, @Temp)";
            string update = "UPDATE t_KDT_CX_NPLXinHuy SET Master_id = @Master_id, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayDangKy = @NgayDangKy, MaHaiQuan = @MaHaiQuan, MaHang = @MaHang, TenHang = @TenHang, LuongTieuHuy = @LuongTieuHuy, LoaiHang = @LoaiHang, DVT = @DVT, Temp = @Temp WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_CX_NPLXinHuy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.NVarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHang", SqlDbType.Int, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp", SqlDbType.NVarChar, "Temp", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.NVarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHang", SqlDbType.Int, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp", SqlDbType.NVarChar, "Temp", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_CX_NPLXinHuy VALUES(@Master_id, @SoToKhai, @MaLoaiHinh, @NgayDangKy, @MaHaiQuan, @MaHang, @TenHang, @LuongTieuHuy, @LoaiHang, @DVT, @Temp)";
            string update = "UPDATE t_KDT_CX_NPLXinHuy SET Master_id = @Master_id, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayDangKy = @NgayDangKy, MaHaiQuan = @MaHaiQuan, MaHang = @MaHang, TenHang = @TenHang, LuongTieuHuy = @LuongTieuHuy, LoaiHang = @LoaiHang, DVT = @DVT, Temp = @Temp WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_CX_NPLXinHuy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.NVarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHang", SqlDbType.Int, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Temp", SqlDbType.NVarChar, "Temp", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_id", SqlDbType.BigInt, "Master_id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.VarChar, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.NVarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTieuHuy", SqlDbType.Decimal, "LuongTieuHuy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHang", SqlDbType.Int, "LoaiHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Temp", SqlDbType.NVarChar, "Temp", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_CX_NPLXinHuy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_CX_NPLXinHuy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_CX_NPLXinHuy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_CX_NPLXinHuy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_CX_NPLXinHuy(long master_id, decimal soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maHaiQuan, string maHang, string tenHang, decimal luongTieuHuy, int loaiHang, string dVT, string temp)
		{
			KDT_CX_NPLXinHuy entity = new KDT_CX_NPLXinHuy();	
			entity.Master_id = master_id;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.LuongTieuHuy = luongTieuHuy;
			entity.LoaiHang = loaiHang;
			entity.DVT = dVT;
			entity.Temp = temp;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.NVarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@LuongTieuHuy", SqlDbType.Decimal, LuongTieuHuy);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Int, LoaiHang);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@Temp", SqlDbType.NVarChar, Temp);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_CX_NPLXinHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_CX_NPLXinHuy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_CX_NPLXinHuy(long id, long master_id, decimal soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maHaiQuan, string maHang, string tenHang, decimal luongTieuHuy, int loaiHang, string dVT, string temp)
		{
			KDT_CX_NPLXinHuy entity = new KDT_CX_NPLXinHuy();			
			entity.ID = id;
			entity.Master_id = master_id;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.LuongTieuHuy = luongTieuHuy;
			entity.LoaiHang = loaiHang;
			entity.DVT = dVT;
			entity.Temp = temp;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_CX_NPLXinHuy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.NVarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@LuongTieuHuy", SqlDbType.Decimal, LuongTieuHuy);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Int, LoaiHang);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@Temp", SqlDbType.NVarChar, Temp);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_CX_NPLXinHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_CX_NPLXinHuy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_CX_NPLXinHuy(long id, long master_id, decimal soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maHaiQuan, string maHang, string tenHang, decimal luongTieuHuy, int loaiHang, string dVT, string temp)
		{
			KDT_CX_NPLXinHuy entity = new KDT_CX_NPLXinHuy();			
			entity.ID = id;
			entity.Master_id = master_id;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.LuongTieuHuy = luongTieuHuy;
			entity.LoaiHang = loaiHang;
			entity.DVT = dVT;
			entity.Temp = temp;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_id", SqlDbType.BigInt, Master_id);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.NVarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@LuongTieuHuy", SqlDbType.Decimal, LuongTieuHuy);
			db.AddInParameter(dbCommand, "@LoaiHang", SqlDbType.Int, LoaiHang);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@Temp", SqlDbType.NVarChar, Temp);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_CX_NPLXinHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_CX_NPLXinHuy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_CX_NPLXinHuy(long id)
		{
			KDT_CX_NPLXinHuy entity = new KDT_CX_NPLXinHuy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_CX_NPLXinHuy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_CX_NPLXinHuy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_CX_NPLXinHuy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}