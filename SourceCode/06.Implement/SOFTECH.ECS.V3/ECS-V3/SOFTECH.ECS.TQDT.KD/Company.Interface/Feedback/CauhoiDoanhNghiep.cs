﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class CauhoiDoanhNghiep : UserControl
    {
        private DataTable dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["CauHoiDoanhNghiep"];
        public CauhoiDoanhNghiep()
        {
            InitializeComponent();
        }

        public void DataBinding()
        {
            try
            {
                if (dt == null || dt.Rows.Count == 0)
                {
                    dt = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["CauHoiDoanhNghiep"];
                }
                //DataView dv = dt.Copy().DefaultView;
                //dv.RowFilter = "HienThi = 1 and LinhVuc_ID = " + LinhVucID + " ";
                //dv.Sort = "NgayTao asc";

                dglist.DataSource = dt;
               
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); throw ex; }
        }

        public delegate void CauHoiDNSelectionChangedEventHandler(object sender, EventArgs e);
        public event CauHoiDNSelectionChangedEventHandler CauHoiDNSelectionChanged;
        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            if (CauHoiDNSelectionChanged != null)
            {
                CauHoiDNSelectionChanged(sender, e);
            }
        }

        private void CauhoiDoanhNghiep_Load(object sender, EventArgs e)
        {
            DataBinding();
        }
        public DataRow CurrentRow()
        {
            Janus.Windows.GridEX.GridEXRow[] rows = dglist.GetRows();

            foreach (Janus.Windows.GridEX.GridEXRow item in rows)
            {
                if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    if (item.Selected)
                    {
                        return item.DataRow.GetType() == typeof(System.Data.DataRowView) ? ((System.Data.DataRowView)item.DataRow).Row : (System.Data.DataRow)item.DataRow;
                    }
                }
            }

            return null;
        }
        
    }
}
