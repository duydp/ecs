﻿/*
 * HUNGTQ, Created 16/09/2010.
 * THỰC HIỆN ĐỒNG BỘ DỮ LIỆU TỪ THÔNG QUAN ĐIỆN TỬ SANG KHAI TỪ XA ĐỂ CHẠY THANH KHOẢN.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DongBoDuLieu.Controls
{
    public partial class TimePanel : UserControl
    {
        public enum TimePanelType
        {
            Year,
            Month,
            Date,
            Custom
        }

        private bool _isValidate = true;

        /// <summary>
        /// Lấy thông tin kiểm tra hợp lệ trên Control.
        /// </summary>
        public bool IsValidate
        {
            get { return _isValidate; }
        }

        public DateTime ToDate
        {
            get { return dtToDate.Value; }
            set { dtToDate.Value = value; }
        }

        public DateTime FromDate
        {
            get { return dtFromDate.Value; }
            set { dtFromDate.Value = value; }
        }

        public string TieuDe
        {
            get { return lblTieuDe.Text; }
            set { lblTieuDe.Text = value; }
        }

        public TimePanel()
        {
            InitializeComponent();
        }

        private void cboTime_SelectedValueChanged(object sender, EventArgs e)
        {
            //string code = cboTime.SelectedValue.ToString();

            //lblLink1.Enabled = lblYear.Enabled = numYear.Enabled = (code == "Year");
            //lblLink2.Enabled = lblMonth.Enabled = numMonth.Enabled = (code == "Month");
            //lblLink3.Enabled = lblFromDate.Enabled = dtFromDate.Enabled
            //    = lblToDate.Enabled = dtToDate.Enabled = (code == "Date");
            //lblLink4.Enabled = lblMaSP.Enabled = txtMaSP.Enabled = (code == "Custom");

            //if (code == "Year")
            //{
            //    numYear.Value = DateTime.Now.Year;
            //    numYear.Focus();
            //}
            //else if (code == "Month")
            //{
            //    numMonth.Value = DateTime.Now.Month;
            //    numMonth.Focus();
            //}
            //else if (code == "Date")
            //{
            //    //Thiet lap gia tri trong khoang thoi gian 1 tuan
            //    dtFromDate.Value = DateTime.Today.AddDays(-7);
            //    dtToDate.Value = DateTime.Today;

            //    dtFromDate.Focus();
            //}
            //else if (code == "Custom")
            //{
            //    txtMaSP.Text = "";

            //    txtMaSP.Focus();

            //}
        }

        private void TimePanel_Load(object sender, EventArgs e)
        {
            //dtToDate.Value = DateTime.Today;
            //dtFromDate.Value = DateTime.Today.AddDays(-7);

            //Mac dinh chon ngay
            //if (cboTime.SelectedValue == null)
            //    cboTime.SelectedValue = "Date";

            //cboTime_SelectedValueChanged(null, EventArgs.Empty);
        }

        private void dtFromDate_ValueChanged(object sender, EventArgs e)
        {
            ValidateFromDate();
        }

        private void ValidateFromDate()
        {
            errorProvider.SetError(dtFromDate, "");
            _isValidate = true;

            if (dtFromDate.Value > dtToDate.Value)
            {
                errorProvider.SetError(dtFromDate, "Giá trị không hợp lệ, từ ngày phải <= đến ngày.");
                _isValidate = false;
                dtFromDate.Focus();
                return;
            }
        }

        private void dtToDate_ValueChanged(object sender, EventArgs e)
        {
            ValidateToDate();
        }

        private void ValidateToDate()
        {
            errorProvider.SetError(dtToDate, "");
            _isValidate = true;

            if (dtToDate.Value < dtFromDate.Value)
            {
                errorProvider.SetError(dtToDate, "Giá trị không hợp lệ, đến ngày phải >= từ ngày.");
                _isValidate = false;
                dtToDate.Focus();
                return;
            }
        }
    }
}
