-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@KetQuaXuLy numeric(1, 0),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@SoCuaGiayPhepLuuHanh varchar(17),
	@SoGiayPhepThucHanh varchar(17),
	@MaCuaKhauNhapDuKien varchar(6),
	@TenCuaKhauNhapDuKien nvarchar(35),
	@TonKhoDenNgay datetime,
	@TenGiamDocDoanhNghiep nvarchar(300),
	@DaiDienDonViCapPhep nvarchar(300),
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@NgayKhaiBao datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SMA]
(
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
)
VALUES 
(
	@MaNguoiKhai,
	@TenNguoiKhai,
	@MaBuuChinhNguoiKhai,
	@DiaChiNguoiKhai,
	@MaNuocNguoiKhai,
	@SoDienThoaiNguoiKhai,
	@SoFaxNguoiKhai,
	@EmailNguoiKhai,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@MaDonViCapPhep,
	@TenDonViCapPhep,
	@MaThuongNhanNhapKhau,
	@TenThuongNhanNhapKhau,
	@MaBuuChinhNhapKhau,
	@DiaChiThuongNhanNhapKhau,
	@MaQuocGiaNhapKhau,
	@SoDienThoaiNhapKhau,
	@SoFaxNhapKhau,
	@EmailNhapKhau,
	@KetQuaXuLy,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@SoCuaGiayPhepLuuHanh,
	@SoGiayPhepThucHanh,
	@MaCuaKhauNhapDuKien,
	@TenCuaKhauNhapDuKien,
	@TonKhoDenNgay,
	@TenGiamDocDoanhNghiep,
	@DaiDienDonViCapPhep,
	@GhiChuDanhChoCoQuanCapPhep,
	@NgayKhaiBao,
	@SoTiepNhan,
	@NgayTiepNhan,
	@PhanLuong,
	@HuongDan,
	@HoSoLienQuan,
	@GhiChu,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@KetQuaXuLy numeric(1, 0),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@SoCuaGiayPhepLuuHanh varchar(17),
	@SoGiayPhepThucHanh varchar(17),
	@MaCuaKhauNhapDuKien varchar(6),
	@TenCuaKhauNhapDuKien nvarchar(35),
	@TonKhoDenNgay datetime,
	@TenGiamDocDoanhNghiep nvarchar(300),
	@DaiDienDonViCapPhep nvarchar(300),
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@NgayKhaiBao datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[MaBuuChinhNguoiKhai] = @MaBuuChinhNguoiKhai,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[MaNuocNguoiKhai] = @MaNuocNguoiKhai,
	[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
	[SoFaxNguoiKhai] = @SoFaxNguoiKhai,
	[EmailNguoiKhai] = @EmailNguoiKhai,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[MaDonViCapPhep] = @MaDonViCapPhep,
	[TenDonViCapPhep] = @TenDonViCapPhep,
	[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
	[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
	[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
	[DiaChiThuongNhanNhapKhau] = @DiaChiThuongNhanNhapKhau,
	[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
	[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
	[SoFaxNhapKhau] = @SoFaxNhapKhau,
	[EmailNhapKhau] = @EmailNhapKhau,
	[KetQuaXuLy] = @KetQuaXuLy,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[SoCuaGiayPhepLuuHanh] = @SoCuaGiayPhepLuuHanh,
	[SoGiayPhepThucHanh] = @SoGiayPhepThucHanh,
	[MaCuaKhauNhapDuKien] = @MaCuaKhauNhapDuKien,
	[TenCuaKhauNhapDuKien] = @TenCuaKhauNhapDuKien,
	[TonKhoDenNgay] = @TonKhoDenNgay,
	[TenGiamDocDoanhNghiep] = @TenGiamDocDoanhNghiep,
	[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
	[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
	[NgayKhaiBao] = @NgayKhaiBao,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[HoSoLienQuan] = @HoSoLienQuan,
	[GhiChu] = @GhiChu,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@KetQuaXuLy numeric(1, 0),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@SoCuaGiayPhepLuuHanh varchar(17),
	@SoGiayPhepThucHanh varchar(17),
	@MaCuaKhauNhapDuKien varchar(6),
	@TenCuaKhauNhapDuKien nvarchar(35),
	@TonKhoDenNgay datetime,
	@TenGiamDocDoanhNghiep nvarchar(300),
	@DaiDienDonViCapPhep nvarchar(300),
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@NgayKhaiBao datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SMA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SMA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[MaBuuChinhNguoiKhai] = @MaBuuChinhNguoiKhai,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[MaNuocNguoiKhai] = @MaNuocNguoiKhai,
			[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
			[SoFaxNguoiKhai] = @SoFaxNguoiKhai,
			[EmailNguoiKhai] = @EmailNguoiKhai,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[MaDonViCapPhep] = @MaDonViCapPhep,
			[TenDonViCapPhep] = @TenDonViCapPhep,
			[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
			[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
			[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
			[DiaChiThuongNhanNhapKhau] = @DiaChiThuongNhanNhapKhau,
			[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
			[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
			[SoFaxNhapKhau] = @SoFaxNhapKhau,
			[EmailNhapKhau] = @EmailNhapKhau,
			[KetQuaXuLy] = @KetQuaXuLy,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[SoCuaGiayPhepLuuHanh] = @SoCuaGiayPhepLuuHanh,
			[SoGiayPhepThucHanh] = @SoGiayPhepThucHanh,
			[MaCuaKhauNhapDuKien] = @MaCuaKhauNhapDuKien,
			[TenCuaKhauNhapDuKien] = @TenCuaKhauNhapDuKien,
			[TonKhoDenNgay] = @TonKhoDenNgay,
			[TenGiamDocDoanhNghiep] = @TenGiamDocDoanhNghiep,
			[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
			[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
			[NgayKhaiBao] = @NgayKhaiBao,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[HoSoLienQuan] = @HoSoLienQuan,
			[GhiChu] = @GhiChu,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SMA]
		(
			[MaNguoiKhai],
			[TenNguoiKhai],
			[MaBuuChinhNguoiKhai],
			[DiaChiNguoiKhai],
			[MaNuocNguoiKhai],
			[SoDienThoaiNguoiKhai],
			[SoFaxNguoiKhai],
			[EmailNguoiKhai],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[MaDonViCapPhep],
			[TenDonViCapPhep],
			[MaThuongNhanNhapKhau],
			[TenThuongNhanNhapKhau],
			[MaBuuChinhNhapKhau],
			[DiaChiThuongNhanNhapKhau],
			[MaQuocGiaNhapKhau],
			[SoDienThoaiNhapKhau],
			[SoFaxNhapKhau],
			[EmailNhapKhau],
			[KetQuaXuLy],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[SoCuaGiayPhepLuuHanh],
			[SoGiayPhepThucHanh],
			[MaCuaKhauNhapDuKien],
			[TenCuaKhauNhapDuKien],
			[TonKhoDenNgay],
			[TenGiamDocDoanhNghiep],
			[DaiDienDonViCapPhep],
			[GhiChuDanhChoCoQuanCapPhep],
			[NgayKhaiBao],
			[SoTiepNhan],
			[NgayTiepNhan],
			[PhanLuong],
			[HuongDan],
			[HoSoLienQuan],
			[GhiChu],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@TenNguoiKhai,
			@MaBuuChinhNguoiKhai,
			@DiaChiNguoiKhai,
			@MaNuocNguoiKhai,
			@SoDienThoaiNguoiKhai,
			@SoFaxNguoiKhai,
			@EmailNguoiKhai,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@MaDonViCapPhep,
			@TenDonViCapPhep,
			@MaThuongNhanNhapKhau,
			@TenThuongNhanNhapKhau,
			@MaBuuChinhNhapKhau,
			@DiaChiThuongNhanNhapKhau,
			@MaQuocGiaNhapKhau,
			@SoDienThoaiNhapKhau,
			@SoFaxNhapKhau,
			@EmailNhapKhau,
			@KetQuaXuLy,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@SoCuaGiayPhepLuuHanh,
			@SoGiayPhepThucHanh,
			@MaCuaKhauNhapDuKien,
			@TenCuaKhauNhapDuKien,
			@TonKhoDenNgay,
			@TenGiamDocDoanhNghiep,
			@DaiDienDonViCapPhep,
			@GhiChuDanhChoCoQuanCapPhep,
			@NgayKhaiBao,
			@SoTiepNhan,
			@NgayTiepNhan,
			@PhanLuong,
			@HuongDan,
			@HoSoLienQuan,
			@GhiChu,
			@TrangThaiXuLy
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SMA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SMA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]	

GO

