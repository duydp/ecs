﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface
{
    public partial class CauHinhToKhaiForm : Company.Interface.BaseForm
    {
        public CauHinhToKhaiForm()
        {
            InitializeComponent();
        }
        public List<Label> Listlable = new List<Label>();
        public Label lblPhuongThucTT { get { return _lblPhuongThucTT; } }
        private void khoitao_DuLieuChuan()
        {
            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            txtTenDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
//            txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
            cbMaHTS.SelectedIndex = GlobalSettings.MaHTS;

            ctrCuaKhauXuatHang.Ma = GlobalSettings.CUA_KHAU;
            ctrDiaDiemDoHang.Ma = GlobalSettings.DIA_DIEM_DO_HANG;

            txtMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            txtMienThue2.Text = GlobalSettings.DonViBaoCao;
            cbTinhThue.SelectedValue = GlobalSettings.TuDongTinhThue;
            //KhanhHN 22/06/2012
            if (Company.KDT.SHARE.Components.Globals.IsTinhThue)
                cbPTTinhThue.SelectedValue = GlobalSettings.TinhThueTGNT;
            else
                cbPTTinhThue.SelectedValue = "-1";

        }
        private void CauHinhToKhaiForm_Load(object sender, EventArgs e)
        {
            khoitao_DuLieuChuan();
            
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            Properties.Settings.Default.TEN_DOI_TAC = txtTenDoiTac.Text;
            Properties.Settings.Default.PTTT_MAC_DINH = cbPTTT.SelectedValue.ToString();
            Properties.Settings.Default.PTVT_MAC_DINH = cbPTVT.SelectedValue.ToString();
            Properties.Settings.Default.DKGH_MAC_DINH = cbDKGH.SelectedValue.ToString();
            //Properties.Settings.Default.TY_GIA_USD = Convert.ToDecimal(txtTyGiaUSD.Value);
            Properties.Settings.Default.CUA_KHAU = ctrCuaKhauXuatHang.Ma;
            Properties.Settings.Default.DIA_DIEM_DO_HANG = ctrDiaDiemDoHang.Ma;
            Properties.Settings.Default.NGUYEN_TE_MAC_DINH = ctrNguyenTe.Ma;
            Properties.Settings.Default.NUOC = ctrNuocXK.Ma;
            Properties.Settings.Default.MA_HTS = Convert.ToInt32(cbMaHTS.SelectedValue);
            Properties.Settings.Default.DonViBaoCao = txtMienThue2.Text.Trim();
            Properties.Settings.Default.TuDongTinhThue = cbTinhThue.SelectedValue.ToString();
            //Khanh 22/06/2012
            Properties.Settings.Default.TinhThueTGNT = cbPTTinhThue.SelectedValue.ToString();
            if (cbPTTinhThue.SelectedValue != "-1")
            {
                Company.KDT.SHARE.Components.Globals.IsTinhThue = true;
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsTinhThue", "True");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TinhThueTGNT", cbPTTinhThue.SelectedValue.ToString());
                GlobalSettings.TinhThueTGNT = cbPTTinhThue.SelectedValue.ToString();
            }
            else
            {
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsTinhThue", "False");
                Company.KDT.SHARE.Components.Globals.IsTinhThue = false;
                
            }

            Properties.Settings.Default.TieuDeInDinhMuc = txtMienThue1.Text.Trim();
            Properties.Settings.Default.MienThueGTGT = txtMienThue2.Text.Trim();
            Properties.Settings.Default.Save();
            GlobalSettings.RefreshKey();
            ShowMessage("Lưu cấu hình thành công.", false);
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Label lable in Listlable)
            {
                lable.ForeColor = lable.ForeColor == Color.Black ? Color.Red : Color.Black;
            }
        }


    }
}

