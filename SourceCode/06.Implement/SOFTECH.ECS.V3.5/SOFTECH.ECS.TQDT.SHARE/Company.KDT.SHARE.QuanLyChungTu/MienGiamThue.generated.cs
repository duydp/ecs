using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class MienGiamThue : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HMD_ID { set; get; }
		public string SoVanBanMienGiam { set; get; }
		public double ThueSuatTruocGiam { set; get; }
		public double TyLeMienGiam { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<MienGiamThue> ConvertToCollection(IDataReader reader)
		{
			IList<MienGiamThue> collection = new List<MienGiamThue>();
			while (reader.Read())
			{

				MienGiamThue entity = new MienGiamThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanBanMienGiam"))) entity.SoVanBanMienGiam = reader.GetString(reader.GetOrdinal("SoVanBanMienGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTruocGiam"))) entity.ThueSuatTruocGiam = reader.GetDouble(reader.GetOrdinal("ThueSuatTruocGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeMienGiam"))) entity.TyLeMienGiam = reader.GetDouble(reader.GetOrdinal("TyLeMienGiam"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<MienGiamThue> collection, long id)
        {
            foreach (MienGiamThue item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_MienGiamThue VALUES(@HMD_ID, @SoVanBanMienGiam, @ThueSuatTruocGiam, @TyLeMienGiam)";
            string update = "UPDATE t_KDT_MienGiamThue SET HMD_ID = @HMD_ID, SoVanBanMienGiam = @SoVanBanMienGiam, ThueSuatTruocGiam = @ThueSuatTruocGiam, TyLeMienGiam = @TyLeMienGiam WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_MienGiamThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, "SoVanBanMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTruocGiam", SqlDbType.Float, "ThueSuatTruocGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeMienGiam", SqlDbType.Float, "TyLeMienGiam", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, "SoVanBanMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTruocGiam", SqlDbType.Float, "ThueSuatTruocGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeMienGiam", SqlDbType.Float, "TyLeMienGiam", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_MienGiamThue VALUES(@HMD_ID, @SoVanBanMienGiam, @ThueSuatTruocGiam, @TyLeMienGiam)";
            string update = "UPDATE t_KDT_MienGiamThue SET HMD_ID = @HMD_ID, SoVanBanMienGiam = @SoVanBanMienGiam, ThueSuatTruocGiam = @ThueSuatTruocGiam, TyLeMienGiam = @TyLeMienGiam WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_MienGiamThue WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, "SoVanBanMienGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTruocGiam", SqlDbType.Float, "ThueSuatTruocGiam", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TyLeMienGiam", SqlDbType.Float, "TyLeMienGiam", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, "SoVanBanMienGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTruocGiam", SqlDbType.Float, "ThueSuatTruocGiam", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TyLeMienGiam", SqlDbType.Float, "TyLeMienGiam", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static MienGiamThue Load(long id)
		{
			const string spName = "[dbo].[p_KDT_MienGiamThue_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<MienGiamThue> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<MienGiamThue> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<MienGiamThue> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<MienGiamThue> SelectCollectionBy_HMD_ID(long hMD_ID)
		{
            IDataReader reader = SelectReaderBy_HMD_ID(hMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HMD_ID(long hMD_ID)
		{
			const string spName = "[dbo].[p_KDT_MienGiamThue_SelectBy_HMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_MienGiamThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_MienGiamThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_MienGiamThue_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_MienGiamThue_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HMD_ID(long hMD_ID)
		{
			const string spName = "p_KDT_MienGiamThue_SelectBy_HMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertMienGiamThue(long hMD_ID, string soVanBanMienGiam, double thueSuatTruocGiam, double tyLeMienGiam)
		{
			MienGiamThue entity = new MienGiamThue();	
			entity.HMD_ID = hMD_ID;
			entity.SoVanBanMienGiam = soVanBanMienGiam;
			entity.ThueSuatTruocGiam = thueSuatTruocGiam;
			entity.TyLeMienGiam = tyLeMienGiam;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_MienGiamThue_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, SoVanBanMienGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTruocGiam", SqlDbType.Float, ThueSuatTruocGiam);
			db.AddInParameter(dbCommand, "@TyLeMienGiam", SqlDbType.Float, TyLeMienGiam);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<MienGiamThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MienGiamThue item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateMienGiamThue(long id, long hMD_ID, string soVanBanMienGiam, double thueSuatTruocGiam, double tyLeMienGiam)
		{
			MienGiamThue entity = new MienGiamThue();			
			entity.ID = id;
			entity.HMD_ID = hMD_ID;
			entity.SoVanBanMienGiam = soVanBanMienGiam;
			entity.ThueSuatTruocGiam = thueSuatTruocGiam;
			entity.TyLeMienGiam = tyLeMienGiam;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_MienGiamThue_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, SoVanBanMienGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTruocGiam", SqlDbType.Float, ThueSuatTruocGiam);
			db.AddInParameter(dbCommand, "@TyLeMienGiam", SqlDbType.Float, TyLeMienGiam);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<MienGiamThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MienGiamThue item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateMienGiamThue(long id, long hMD_ID, string soVanBanMienGiam, double thueSuatTruocGiam, double tyLeMienGiam)
		{
			MienGiamThue entity = new MienGiamThue();			
			entity.ID = id;
			entity.HMD_ID = hMD_ID;
			entity.SoVanBanMienGiam = soVanBanMienGiam;
			entity.ThueSuatTruocGiam = thueSuatTruocGiam;
			entity.TyLeMienGiam = tyLeMienGiam;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_MienGiamThue_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@SoVanBanMienGiam", SqlDbType.NVarChar, SoVanBanMienGiam);
			db.AddInParameter(dbCommand, "@ThueSuatTruocGiam", SqlDbType.Float, ThueSuatTruocGiam);
			db.AddInParameter(dbCommand, "@TyLeMienGiam", SqlDbType.Float, TyLeMienGiam);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<MienGiamThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MienGiamThue item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteMienGiamThue(long id)
		{
			MienGiamThue entity = new MienGiamThue();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_MienGiamThue_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HMD_ID(long hMD_ID)
		{
			const string spName = "[dbo].[p_KDT_MienGiamThue_DeleteBy_HMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, hMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_MienGiamThue_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<MienGiamThue> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MienGiamThue item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}