﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAS501 : BasicVNACC
    {
   
        public List<VAS501_HANG> HangHoa{ get; set; }

        //public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        //{
        //    StringBuilder str = StrBuild;
        //    str = BuildEdiMessages<VAS5050>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050");
        //    foreach (VAS5050_HANG item in HangHoa)
        //    {
        //        item.BuildEdiMessages<VAS5050_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050_HANG");

        //    }
        //    return str;
        //}
        public void LoadVAS501(string strResult)
        {
            try
            {
                this.GetObject<VAS501>(strResult, true, GlobalVNACC.PathConfig, "VAS501", false, VAS501.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAS501.TongSoByte);
                while (true)
                {
                    VAS501_HANG vad1agHang = new VAS501_HANG();
                    vad1agHang.GetObject<VAS501_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAS501_HANG", false, VAS501_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAS501_HANG>();
                    this.HangHoa.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAS501_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAS501_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAS501_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }
}
