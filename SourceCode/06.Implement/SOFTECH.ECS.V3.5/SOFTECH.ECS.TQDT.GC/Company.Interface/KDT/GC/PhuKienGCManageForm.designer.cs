﻿using System.ComponentModel;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using DonViHaiQuanControl=Company.Interface.Controls.DonViHaiQuanControl;

namespace Company.Interface.KDT.GC
{
    partial class PhuKienGCManageForm
    {
        private ImageList ImageList1;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuKienGCManageForm));
            Janus.Windows.GridEX.GridEXLayout dgPhuKien_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgPhuKien_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            Janus.Windows.GridEX.GridEXLayout cbHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdGet = new Janus.Windows.UI.CommandBars.UICommand("cmdGet");
            this.KhaiBao = new Janus.Windows.UI.CommandBars.UICommand("KhaiBao");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.XacNhanThongTin = new Janus.Windows.UI.CommandBars.UICommand("XacNhanThongTin");
            this.cmdXuatPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatPhuKien");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdUpdateGUIDSTR = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGUIDSTR");
            this.cmdExportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdUpdateStatus = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateStatus");
            this.uiContextMenu1 = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdCSDaDuyet2 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdExportExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdUpdateStatus1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateStatus");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.dgPhuKien = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckbTimKiem = new Janus.Windows.EditControls.UICheckBox();
            this.grbtimkiem = new Janus.Windows.EditControls.UIGroupBox();
            this.clcDateTo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcDateFrom = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cbHopDong = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label7 = new System.Windows.Forms.Label();
            this.cbStatus = new Janus.Windows.EditControls.UIComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNamTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label5 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAuto = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoPhuKien = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiContextMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).BeginInit();
            this.grbtimkiem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgPhuKien);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(1002, 555);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "printer.png");
            this.ImageList1.Images.SetKeyName(5, "report.png");
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGet,
            this.KhaiBao,
            this.Huy,
            this.XacNhanThongTin,
            this.cmdXuatPhuKien,
            this.cmdCSDaDuyet,
            this.InPhieuTN,
            this.cmdUpdateGUIDSTR,
            this.cmdExportExcel,
            this.cmdUpdateStatus});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.uiContextMenu1});
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.ShowShortcutInToolTips = true;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCSDaDuyet1,
            this.InPhieuTN1,
            this.cmdExportExcel1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(403, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.Visible = false;
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            this.cmdCSDaDuyet1.Text = "&Chuyển Trạng Thái";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            this.InPhieuTN1.Text = "&In phiếu tiếp nhận";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExcel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdGet
            // 
            this.cmdGet.Image = ((System.Drawing.Image)(resources.GetObject("cmdGet.Image")));
            this.cmdGet.Key = "cmdGet";
            this.cmdGet.Name = "cmdGet";
            this.cmdGet.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.cmdGet.Text = "Nhận thông tin";
            // 
            // KhaiBao
            // 
            this.KhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("KhaiBao.Image")));
            this.KhaiBao.Key = "KhaiBao";
            this.KhaiBao.Name = "KhaiBao";
            this.KhaiBao.Text = "Khai báo";
            // 
            // Huy
            // 
            this.Huy.Image = ((System.Drawing.Image)(resources.GetObject("Huy.Image")));
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy khai báo";
            // 
            // XacNhanThongTin
            // 
            this.XacNhanThongTin.Image = ((System.Drawing.Image)(resources.GetObject("XacNhanThongTin.Image")));
            this.XacNhanThongTin.Key = "XacNhanThongTin";
            this.XacNhanThongTin.Name = "XacNhanThongTin";
            this.XacNhanThongTin.Text = "Xác nhận";
            // 
            // cmdXuatPhuKien
            // 
            this.cmdXuatPhuKien.Image = ((System.Drawing.Image)(resources.GetObject("cmdXuatPhuKien.Image")));
            this.cmdXuatPhuKien.Key = "cmdXuatPhuKien";
            this.cmdXuatPhuKien.Name = "cmdXuatPhuKien";
            this.cmdXuatPhuKien.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Image = ((System.Drawing.Image)(resources.GetObject("InPhieuTN.Image")));
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "In phiếu tiếp nhận";
            // 
            // cmdUpdateGUIDSTR
            // 
            this.cmdUpdateGUIDSTR.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGUIDSTR.Image")));
            this.cmdUpdateGUIDSTR.Key = "cmdUpdateGUIDSTR";
            this.cmdUpdateGUIDSTR.Name = "cmdUpdateGUIDSTR";
            this.cmdUpdateGUIDSTR.Text = "Cập nhập chuỗi phản hồi";
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportExcel.Image")));
            this.cmdExportExcel.Key = "cmdExportExcel";
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Text = "Xuất Excel";
            // 
            // cmdUpdateStatus
            // 
            this.cmdUpdateStatus.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateStatus.Image")));
            this.cmdUpdateStatus.Key = "cmdUpdateStatus";
            this.cmdUpdateStatus.Name = "cmdUpdateStatus";
            this.cmdUpdateStatus.Text = "Cập nhật trạng thái";
            // 
            // uiContextMenu1
            // 
            this.uiContextMenu1.CommandManager = this.cmMain;
            this.uiContextMenu1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCSDaDuyet2,
            this.InPhieuTN2,
            this.cmdExportExcel2,
            this.cmdUpdateStatus1});
            this.uiContextMenu1.Key = "ContextMenu1";
            // 
            // cmdCSDaDuyet2
            // 
            this.cmdCSDaDuyet2.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet2.Name = "cmdCSDaDuyet2";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            // 
            // cmdExportExcel2
            // 
            this.cmdExportExcel2.Key = "cmdExportExcel";
            this.cmdExportExcel2.Name = "cmdExportExcel2";
            // 
            // cmdUpdateStatus1
            // 
            this.cmdUpdateStatus1.Key = "cmdUpdateStatus";
            this.cmdUpdateStatus1.Name = "cmdUpdateStatus1";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1002, 32);
            // 
            // dgPhuKien
            // 
            this.dgPhuKien.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgPhuKien.AlternatingColors = true;
            this.dgPhuKien.BorderStyle = Janus.Windows.GridEX.BorderStyle.RaisedLight3D;
            this.dgPhuKien.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgPhuKien.ColumnAutoResize = true;
            this.cmMain.SetContextMenu(this.dgPhuKien, this.uiContextMenu1);
            dgPhuKien_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgPhuKien_DesignTimeLayout_Reference_0.Instance")));
            dgPhuKien_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgPhuKien_DesignTimeLayout_Reference_0});
            dgPhuKien_DesignTimeLayout.LayoutString = resources.GetString("dgPhuKien_DesignTimeLayout.LayoutString");
            this.dgPhuKien.DesignTimeLayout = dgPhuKien_DesignTimeLayout;
            this.dgPhuKien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgPhuKien.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgPhuKien.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgPhuKien.GroupByBoxVisible = false;
            this.dgPhuKien.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgPhuKien.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgPhuKien.ImageList = this.ImageList1;
            this.dgPhuKien.Location = new System.Drawing.Point(0, 128);
            this.dgPhuKien.Name = "dgPhuKien";
            this.dgPhuKien.RecordNavigator = true;
            this.dgPhuKien.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgPhuKien.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgPhuKien.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgPhuKien.Size = new System.Drawing.Size(1002, 386);
            this.dgPhuKien.TabIndex = 340;
            this.dgPhuKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgPhuKien.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgPhuKien.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgPhuKien_DeletingRecords);
            this.dgPhuKien.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtSoPhuKien);
            this.uiGroupBox4.Controls.Add(this.ckbTimKiem);
            this.uiGroupBox4.Controls.Add(this.grbtimkiem);
            this.uiGroupBox4.Controls.Add(this.cbHopDong);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.cbStatus);
            this.uiGroupBox4.Controls.Add(this.label1);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.txtNamTiepNhan);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1002, 128);
            this.uiGroupBox4.TabIndex = 7;
            this.uiGroupBox4.Text = "Thông tin tìm kiếm";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ckbTimKiem
            // 
            this.ckbTimKiem.Location = new System.Drawing.Point(615, 12);
            this.ckbTimKiem.Margin = new System.Windows.Forms.Padding(0);
            this.ckbTimKiem.Name = "ckbTimKiem";
            this.ckbTimKiem.Size = new System.Drawing.Size(15, 15);
            this.ckbTimKiem.TabIndex = 282;
            this.ckbTimKiem.VisualStyleManager = this.vsmMain;
            this.ckbTimKiem.CheckedChanged += new System.EventHandler(this.ckbTimKiem_CheckedChanged);
            // 
            // grbtimkiem
            // 
            this.grbtimkiem.Controls.Add(this.clcDateTo);
            this.grbtimkiem.Controls.Add(this.clcDateFrom);
            this.grbtimkiem.Controls.Add(this.label9);
            this.grbtimkiem.Controls.Add(this.label8);
            this.grbtimkiem.Enabled = false;
            this.grbtimkiem.Location = new System.Drawing.Point(622, 15);
            this.grbtimkiem.Name = "grbtimkiem";
            this.grbtimkiem.Size = new System.Drawing.Size(201, 68);
            this.grbtimkiem.TabIndex = 283;
            this.grbtimkiem.VisualStyleManager = this.vsmMain;
            // 
            // clcDateTo
            // 
            this.clcDateTo.CustomFormat = "dd/MM/yyyy";
            this.clcDateTo.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDateTo.DropDownCalendar.Name = "";
            this.clcDateTo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDateTo.Location = new System.Drawing.Point(81, 41);
            this.clcDateTo.Name = "clcDateTo";
            this.clcDateTo.Size = new System.Drawing.Size(105, 22);
            this.clcDateTo.TabIndex = 7;
            this.clcDateTo.Value = new System.DateTime(2018, 8, 20, 0, 0, 0, 0);
            this.clcDateTo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcDateFrom
            // 
            this.clcDateFrom.CustomFormat = "dd/MM/yyyy";
            this.clcDateFrom.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDateFrom.DropDownCalendar.Name = "";
            this.clcDateFrom.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDateFrom.Location = new System.Drawing.Point(81, 11);
            this.clcDateFrom.Name = "clcDateFrom";
            this.clcDateFrom.Size = new System.Drawing.Size(105, 22);
            this.clcDateFrom.TabIndex = 6;
            this.clcDateFrom.Value = new System.DateTime(2018, 8, 20, 0, 0, 0, 0);
            this.clcDateFrom.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 14);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(53, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Từ ngày";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 44);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 14);
            this.label8.TabIndex = 0;
            this.label8.Text = "Đến ngày";
            // 
            // cbHopDong
            // 
            cbHopDong_DesignTimeLayout.LayoutString = resources.GetString("cbHopDong_DesignTimeLayout.LayoutString");
            this.cbHopDong.DesignTimeLayout = cbHopDong_DesignTimeLayout;
            this.cbHopDong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHopDong.Location = new System.Drawing.Point(111, 89);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.SelectedIndex = -1;
            this.cbHopDong.SelectedItem = null;
            this.cbHopDong.Size = new System.Drawing.Size(486, 22);
            this.cbHopDong.TabIndex = 7;
            this.cbHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbHopDong.ValueChanged += new System.EventHandler(this.cbHopDong_ValueChanged);
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnSearch.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSearch.Location = new System.Drawing.Point(615, 89);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(82, 23);
            this.btnSearch.TabIndex = 8;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(315, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Trạng thái";
            // 
            // cbStatus
            // 
            this.cbStatus.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbStatus.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Chưa khai báo";
            uiComboBoxItem1.Value = -1;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Đã khai báo";
            uiComboBoxItem2.Value = "-3";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Chờ duyệt";
            uiComboBoxItem3.Value = 0;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Đã duyệt";
            uiComboBoxItem4.Value = 1;
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.FormatStyle.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Không phê duyệt";
            uiComboBoxItem5.Value = ((short)(2));
            this.cbStatus.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5});
            this.cbStatus.Location = new System.Drawing.Point(400, 22);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(197, 22);
            this.cbStatus.TabIndex = 2;
            this.cbStatus.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbStatus.SelectedIndexChanged += new System.EventHandler(this.cbStatus_SelectedIndexChanged);
            this.cbStatus.SelectedValueChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(87, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Năm tiếp nhận";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtNamTiepNhan
            // 
            this.txtNamTiepNhan.DecimalDigits = 0;
            this.txtNamTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamTiepNhan.FormatString = "####";
            this.txtNamTiepNhan.Location = new System.Drawing.Point(112, 55);
            this.txtNamTiepNhan.MaxLength = 4;
            this.txtNamTiepNhan.Name = "txtNamTiepNhan";
            this.txtNamTiepNhan.Size = new System.Drawing.Size(197, 22);
            this.txtNamTiepNhan.TabIndex = 3;
            this.txtNamTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamTiepNhan.Value = ((short)(0));
            this.txtNamTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNamTiepNhan.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Hợp đồng";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(112, 22);
            this.txtSoTiepNhan.MaxLength = 12;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(197, 22);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số tiếp nhận";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(830, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(82, 23);
            this.btnXoa.TabIndex = 281;
            this.btnXoa.Text = "Xóa ";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(918, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 23;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Blue;
            this.label5.Location = new System.Drawing.Point(5, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(205, 14);
            this.label5.TabIndex = 22;
            this.label5.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.btnAuto);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 514);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1002, 41);
            this.uiGroupBox1.TabIndex = 339;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnAuto
            // 
            this.btnAuto.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuto.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAuto.Image = ((System.Drawing.Image)(resources.GetObject("btnAuto.Image")));
            this.btnAuto.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnAuto.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAuto.Location = new System.Drawing.Point(674, 11);
            this.btnAuto.Name = "btnAuto";
            this.btnAuto.Size = new System.Drawing.Size(149, 23);
            this.btnAuto.TabIndex = 281;
            this.btnAuto.Text = "Khai báo tự động";
            this.btnAuto.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAuto.Click += new System.EventHandler(this.btnAuto_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(315, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Số phụ kiện";
            this.label1.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtSoPhuKien
            // 
            this.txtSoPhuKien.BackColor = System.Drawing.Color.White;
            this.txtSoPhuKien.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoPhuKien.Location = new System.Drawing.Point(400, 53);
            this.txtSoPhuKien.MaxLength = 50;
            this.txtSoPhuKien.Name = "txtSoPhuKien";
            this.txtSoPhuKien.Size = new System.Drawing.Size(197, 22);
            this.txtSoPhuKien.TabIndex = 4;
            this.txtSoPhuKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoPhuKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoPhuKien.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // PhuKienGCManageForm
            // 
            this.AcceptButton = this.btnSearch;
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 15);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(1002, 587);
            this.Controls.Add(this.TopRebar1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PhuKienGCManageForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Theo dõi phụ kiện";
            this.Load += new System.EventHandler(this.PhuKienGCManageForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiContextMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPhuKien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbtimkiem)).EndInit();
            this.grbtimkiem.ResumeLayout(false);
            this.grbtimkiem.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdGet;
        private UIGroupBox uiGroupBox4;
        public MultiColumnCombo cbHopDong;
        private UIButton btnSearch;
        private Label label7;
        private UIComboBox cbStatus;
        private Label label4;
        private NumericEditBox txtNamTiepNhan;
        private Label label3;
        private NumericEditBox txtSoTiepNhan;
        private Label label2;
        private Janus.Windows.UI.CommandBars.UICommand KhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand Huy;
        private Janus.Windows.UI.CommandBars.UICommand XacNhanThongTin;
        private Janus.Windows.UI.CommandBars.UIContextMenu uiContextMenu1;
        private UIButton btnClose;
        private Label label5;
        private UIButton btnXoa;
        private SaveFileDialog saveFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatPhuKien;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet2;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN2;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGUIDSTR;
        private UIGroupBox uiGroupBox1;
        private GridEX dgPhuKien;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel;
        private UICheckBox ckbTimKiem;
        private UIGroupBox grbtimkiem;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcTuNgay;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar ucCalendar1;
        private Label label9;
        private Label label8;
        private Janus.Windows.UI.CommandBars.UICommand cmdExportExcel2;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDateTo;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDateFrom;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateStatus;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateStatus1;
        private UIButton btnAuto;
        private Label label1;
        private EditBox txtSoPhuKien;

    }
}
