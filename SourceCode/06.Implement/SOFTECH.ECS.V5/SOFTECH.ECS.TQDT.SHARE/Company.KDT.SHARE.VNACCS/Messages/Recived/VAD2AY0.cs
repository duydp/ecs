﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAD2AY0 : BasicVNACC
    {
        public List<VAD2AY0_HANG> HangMD { get; set; }
        public void LoadVAD2AY0(string strResult)
        {
            try
            {

                this.GetObject<VAD2AY0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD2AY0, false, VAD2AY0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AY0.TongSoByte);
                while (true)
                {
                    VAD2AY0_HANG vad1agHang = new VAD2AY0_HANG();
                    vad1agHang.GetObject<VAD2AY0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD2AY0_HANG", false, VAD2AY0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD2AY0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD2AY0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD2AY0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD2AY0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }
}
