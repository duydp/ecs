namespace Licenses_v2
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.pictureEdit1 = new DevExpress.XtraEditors.PictureEdit();
            this.skinLicense = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.navBarLicenses = new DevExpress.XtraNavBar.NavBarControl();
            this.navCustomers = new DevExpress.XtraNavBar.NavBarGroup();
            this.navCustNew = new DevExpress.XtraNavBar.NavBarItem();
            this.navCustManager = new DevExpress.XtraNavBar.NavBarItem();
            this.navCustReports = new DevExpress.XtraNavBar.NavBarItem();
            this.navProducts = new DevExpress.XtraNavBar.NavBarGroup();
            this.navProdNew = new DevExpress.XtraNavBar.NavBarItem();
            this.navProdManager = new DevExpress.XtraNavBar.NavBarItem();
            this.navProdReport = new DevExpress.XtraNavBar.NavBarItem();
            this.navLicenses = new DevExpress.XtraNavBar.NavBarGroup();
            this.navLicNew = new DevExpress.XtraNavBar.NavBarItem();
            this.navLicManager = new DevExpress.XtraNavBar.NavBarItem();
            this.navLicNewSerial = new DevExpress.XtraNavBar.NavBarItem();
            this.navLicSerManager = new DevExpress.XtraNavBar.NavBarItem();
            this.navLicReport = new DevExpress.XtraNavBar.NavBarItem();
            this.navInvoices = new DevExpress.XtraNavBar.NavBarGroup();
            this.navInvNew = new DevExpress.XtraNavBar.NavBarItem();
            this.navInvManager = new DevExpress.XtraNavBar.NavBarItem();
            this.navInvReport = new DevExpress.XtraNavBar.NavBarItem();
            this.navSystem = new DevExpress.XtraNavBar.NavBarGroup();
            this.navSysEmpl = new DevExpress.XtraNavBar.NavBarItem();
            this.navSysInfoEmpl = new DevExpress.XtraNavBar.NavBarItem();
            this.navSysGroupEmpl = new DevExpress.XtraNavBar.NavBarItem();
            this.navSysGroupManager = new DevExpress.XtraNavBar.NavBarItem();
            this.navSysConfig = new DevExpress.XtraNavBar.NavBarItem();
            this.xtraMdi = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.barLicense = new DevExpress.XtraBars.BarManager(this.components);
            this.bar3 = new DevExpress.XtraBars.Bar();
            this.barStaticCompany = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticTimer = new DevExpress.XtraBars.BarStaticItem();
            this.barStaticAction = new DevExpress.XtraBars.BarStaticItem();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarLicenses)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraMdi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barLicense)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.pictureEdit1);
            this.panelControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelControl1.Location = new System.Drawing.Point(0, 0);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(864, 60);
            this.panelControl1.TabIndex = 1;
            // 
            // pictureEdit1
            // 
            this.pictureEdit1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureEdit1.EditValue = ((object)(resources.GetObject("pictureEdit1.EditValue")));
            this.pictureEdit1.Location = new System.Drawing.Point(0, 0);
            this.pictureEdit1.Name = "pictureEdit1";
            this.pictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.pictureEdit1.Properties.Appearance.Options.UseBackColor = true;
            this.pictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pictureEdit1.Size = new System.Drawing.Size(230, 60);
            this.pictureEdit1.TabIndex = 0;
            // 
            // skinLicense
            // 
            this.skinLicense.LookAndFeel.SkinName = "Blue";
            // 
            // navBarLicenses
            // 
            this.navBarLicenses.ActiveGroup = this.navCustomers;
            this.navBarLicenses.AllowDrop = false;
            this.navBarLicenses.ContentButtonHint = null;
            this.navBarLicenses.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarLicenses.Groups.AddRange(new DevExpress.XtraNavBar.NavBarGroup[] {
            this.navCustomers,
            this.navProducts,
            this.navLicenses,
            this.navInvoices,
            this.navSystem});
            this.navBarLicenses.Items.AddRange(new DevExpress.XtraNavBar.NavBarItem[] {
            this.navCustNew,
            this.navCustManager,
            this.navCustReports,
            this.navProdNew,
            this.navProdManager,
            this.navProdReport,
            this.navLicNew,
            this.navLicManager,
            this.navLicReport,
            this.navInvNew,
            this.navInvManager,
            this.navInvReport,
            this.navSysEmpl,
            this.navSysInfoEmpl,
            this.navSysConfig,
            this.navLicNewSerial,
            this.navLicSerManager,
            this.navSysGroupEmpl,
            this.navSysGroupManager});
            this.navBarLicenses.Location = new System.Drawing.Point(0, 60);
            this.navBarLicenses.Name = "navBarLicenses";
            this.navBarLicenses.OptionsNavPane.ExpandedWidth = 158;
            this.navBarLicenses.Size = new System.Drawing.Size(169, 349);
            this.navBarLicenses.StoreDefaultPaintStyleName = true;
            this.navBarLicenses.TabIndex = 7;
            this.navBarLicenses.Text = "Menu";
            // 
            // navCustomers
            // 
            this.navCustomers.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.navCustomers.Appearance.Options.UseFont = true;
            this.navCustomers.Caption = "Quản Lý Khách Hàng";
            this.navCustomers.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navCustomers.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navCustNew),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navCustManager),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navCustReports)});
            this.navCustomers.LargeImage = ((System.Drawing.Image)(resources.GetObject("navCustomers.LargeImage")));
            this.navCustomers.Name = "navCustomers";
            // 
            // navCustNew
            // 
            this.navCustNew.Caption = "Thêm mới";
            this.navCustNew.LargeImage = ((System.Drawing.Image)(resources.GetObject("navCustNew.LargeImage")));
            this.navCustNew.Name = "navCustNew";
            this.navCustNew.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navCustNew_LinkClicked);
            // 
            // navCustManager
            // 
            this.navCustManager.Caption = "Quản lý";
            this.navCustManager.LargeImage = ((System.Drawing.Image)(resources.GetObject("navCustManager.LargeImage")));
            this.navCustManager.Name = "navCustManager";
            this.navCustManager.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navCustManager_LinkClicked);
            // 
            // navCustReports
            // 
            this.navCustReports.Caption = "Báo cáo";
            this.navCustReports.LargeImage = ((System.Drawing.Image)(resources.GetObject("navCustReports.LargeImage")));
            this.navCustReports.Name = "navCustReports";
            // 
            // navProducts
            // 
            this.navProducts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.navProducts.Appearance.Options.UseFont = true;
            this.navProducts.Caption = "Quản Lý Sản Phẩm";
            this.navProducts.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navProducts.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navProdNew),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navProdManager),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navProdReport)});
            this.navProducts.LargeImage = ((System.Drawing.Image)(resources.GetObject("navProducts.LargeImage")));
            this.navProducts.Name = "navProducts";
            // 
            // navProdNew
            // 
            this.navProdNew.Caption = "Thêm mới";
            this.navProdNew.LargeImage = ((System.Drawing.Image)(resources.GetObject("navProdNew.LargeImage")));
            this.navProdNew.Name = "navProdNew";
            this.navProdNew.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navProdNew_LinkClicked);
            // 
            // navProdManager
            // 
            this.navProdManager.Caption = "Quản lý";
            this.navProdManager.LargeImage = ((System.Drawing.Image)(resources.GetObject("navProdManager.LargeImage")));
            this.navProdManager.Name = "navProdManager";
            this.navProdManager.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navProdManager_LinkClicked);
            // 
            // navProdReport
            // 
            this.navProdReport.Caption = "Báo cáo";
            this.navProdReport.LargeImage = ((System.Drawing.Image)(resources.GetObject("navProdReport.LargeImage")));
            this.navProdReport.Name = "navProdReport";
            // 
            // navLicenses
            // 
            this.navLicenses.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.navLicenses.Appearance.Options.UseFont = true;
            this.navLicenses.Caption = "Quản Lý Bản Quyền";
            this.navLicenses.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navLicenses.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navLicNew),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navLicManager),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navLicNewSerial),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navLicSerManager),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navLicReport)});
            this.navLicenses.LargeImage = ((System.Drawing.Image)(resources.GetObject("navLicenses.LargeImage")));
            this.navLicenses.Name = "navLicenses";
            // 
            // navLicNew
            // 
            this.navLicNew.Caption = "Thêm mới";
            this.navLicNew.LargeImage = ((System.Drawing.Image)(resources.GetObject("navLicNew.LargeImage")));
            this.navLicNew.Name = "navLicNew";
            this.navLicNew.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navLicNew_LinkClicked);
            // 
            // navLicManager
            // 
            this.navLicManager.Caption = "Quản lý";
            this.navLicManager.LargeImage = ((System.Drawing.Image)(resources.GetObject("navLicManager.LargeImage")));
            this.navLicManager.Name = "navLicManager";
            this.navLicManager.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navLicManager_LinkClicked);
            // 
            // navLicNewSerial
            // 
            this.navLicNewSerial.Caption = "Thêm mới Serial";
            this.navLicNewSerial.LargeImage = ((System.Drawing.Image)(resources.GetObject("navLicNewSerial.LargeImage")));
            this.navLicNewSerial.Name = "navLicNewSerial";
            this.navLicNewSerial.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navLicNewSerial_LinkClicked);
            // 
            // navLicSerManager
            // 
            this.navLicSerManager.Caption = "Quản lý Serial";
            this.navLicSerManager.LargeImage = ((System.Drawing.Image)(resources.GetObject("navLicSerManager.LargeImage")));
            this.navLicSerManager.Name = "navLicSerManager";
            this.navLicSerManager.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navLicSerManager_LinkClicked);
            // 
            // navLicReport
            // 
            this.navLicReport.Caption = "Báo cáo";
            this.navLicReport.LargeImage = ((System.Drawing.Image)(resources.GetObject("navLicReport.LargeImage")));
            this.navLicReport.Name = "navLicReport";
            // 
            // navInvoices
            // 
            this.navInvoices.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.navInvoices.Appearance.Options.UseFont = true;
            this.navInvoices.Caption = "Quản Lý Hợp Đồng";
            this.navInvoices.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navInvoices.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navInvNew),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navInvManager),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navInvReport)});
            this.navInvoices.LargeImage = ((System.Drawing.Image)(resources.GetObject("navInvoices.LargeImage")));
            this.navInvoices.Name = "navInvoices";
            // 
            // navInvNew
            // 
            this.navInvNew.Caption = "Thêm mới";
            this.navInvNew.LargeImage = ((System.Drawing.Image)(resources.GetObject("navInvNew.LargeImage")));
            this.navInvNew.Name = "navInvNew";
            this.navInvNew.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navInvNew_LinkClicked);
            // 
            // navInvManager
            // 
            this.navInvManager.Caption = "Quản lý";
            this.navInvManager.LargeImage = ((System.Drawing.Image)(resources.GetObject("navInvManager.LargeImage")));
            this.navInvManager.Name = "navInvManager";
            this.navInvManager.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navInvManager_LinkClicked);
            // 
            // navInvReport
            // 
            this.navInvReport.Caption = "Báo cáo";
            this.navInvReport.LargeImage = ((System.Drawing.Image)(resources.GetObject("navInvReport.LargeImage")));
            this.navInvReport.Name = "navInvReport";
            // 
            // navSystem
            // 
            this.navSystem.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.navSystem.Appearance.Options.UseFont = true;
            this.navSystem.Caption = "Quản Lý Hệ Thống";
            this.navSystem.Expanded = true;
            this.navSystem.GroupStyle = DevExpress.XtraNavBar.NavBarGroupStyle.LargeIconsText;
            this.navSystem.ItemLinks.AddRange(new DevExpress.XtraNavBar.NavBarItemLink[] {
            new DevExpress.XtraNavBar.NavBarItemLink(this.navSysEmpl),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navSysInfoEmpl),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navSysGroupEmpl),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navSysGroupManager),
            new DevExpress.XtraNavBar.NavBarItemLink(this.navSysConfig)});
            this.navSystem.LargeImage = ((System.Drawing.Image)(resources.GetObject("navSystem.LargeImage")));
            this.navSystem.Name = "navSystem";
            // 
            // navSysEmpl
            // 
            this.navSysEmpl.Caption = "Thêm mới nhân viên";
            this.navSysEmpl.LargeImage = ((System.Drawing.Image)(resources.GetObject("navSysEmpl.LargeImage")));
            this.navSysEmpl.Name = "navSysEmpl";
            this.navSysEmpl.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navSysEmpl_LinkClicked);
            // 
            // navSysInfoEmpl
            // 
            this.navSysInfoEmpl.Caption = "Thông tin nhân viên";
            this.navSysInfoEmpl.LargeImage = ((System.Drawing.Image)(resources.GetObject("navSysInfoEmpl.LargeImage")));
            this.navSysInfoEmpl.Name = "navSysInfoEmpl";
            this.navSysInfoEmpl.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navSysInfoEmpl_LinkClicked);
            // 
            // navSysGroupEmpl
            // 
            this.navSysGroupEmpl.Caption = "Thêm mới nhóm nhân viên";
            this.navSysGroupEmpl.Name = "navSysGroupEmpl";
            this.navSysGroupEmpl.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navSysGroupEmpl_LinkClicked);
            // 
            // navSysGroupManager
            // 
            this.navSysGroupManager.Caption = "Thông tin nhóm nhân viên";
            this.navSysGroupManager.Name = "navSysGroupManager";
            this.navSysGroupManager.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navSysGroupManager_LinkClicked);
            // 
            // navSysConfig
            // 
            this.navSysConfig.Caption = "Thiết lập cấu hình";
            this.navSysConfig.LargeImage = ((System.Drawing.Image)(resources.GetObject("navSysConfig.LargeImage")));
            this.navSysConfig.Name = "navSysConfig";
            this.navSysConfig.LinkClicked += new DevExpress.XtraNavBar.NavBarLinkEventHandler(this.navSysConfig_LinkClicked);
            // 
            // xtraMdi
            // 
            this.xtraMdi.MdiParent = this;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // barLicense
            // 
            this.barLicense.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.bar3});
            this.barLicense.DockControls.Add(this.barDockControlTop);
            this.barLicense.DockControls.Add(this.barDockControlBottom);
            this.barLicense.DockControls.Add(this.barDockControlLeft);
            this.barLicense.DockControls.Add(this.barDockControlRight);
            this.barLicense.Form = this;
            this.barLicense.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barStaticCompany,
            this.barStaticTimer,
            this.barStaticAction});
            this.barLicense.MaxItemId = 3;
            this.barLicense.StatusBar = this.bar3;
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockRow = 0;
            this.bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticCompany),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticTimer),
            new DevExpress.XtraBars.LinkPersistInfo(this.barStaticAction)});
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.DrawSizeGrip = true;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // barStaticCompany
            // 
            this.barStaticCompany.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.barStaticCompany.Caption = "Phần mềm quản lý bản quyền phần mềm - Công ty cổ phẩn Công Nghệ Phần Mềm Đà Nẵng." +
                "";
            this.barStaticCompany.Id = 0;
            this.barStaticCompany.Name = "barStaticCompany";
            this.barStaticCompany.RightIndent = 1;
            this.barStaticCompany.TextAlignment = System.Drawing.StringAlignment.Near;
            this.barStaticCompany.Width = 32;
            // 
            // barStaticTimer
            // 
            this.barStaticTimer.Caption = "Timer";
            this.barStaticTimer.Id = 1;
            this.barStaticTimer.Name = "barStaticTimer";
            this.barStaticTimer.RightIndent = 1;
            this.barStaticTimer.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // barStaticAction
            // 
            this.barStaticAction.Id = 2;
            this.barStaticAction.Name = "barStaticAction";
            this.barStaticAction.RightIndent = 1;
            this.barStaticAction.TextAlignment = System.Drawing.StringAlignment.Near;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 435);
            this.Controls.Add(this.navBarLicenses);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "FrmMain";
            this.Text = "Phần mềm quản lý bản quyền phần mềm";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navBarLicenses)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraMdi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barLicense)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        public DevExpress.LookAndFeel.DefaultLookAndFeel skinLicense;
        public DevExpress.XtraNavBar.NavBarControl navBarLicenses;
        private DevExpress.XtraNavBar.NavBarGroup navCustomers;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraMdi;
        private DevExpress.XtraNavBar.NavBarItem navCustNew;
        private DevExpress.XtraNavBar.NavBarGroup navProducts;
        private DevExpress.XtraNavBar.NavBarGroup navLicenses;
        private DevExpress.XtraNavBar.NavBarGroup navInvoices;
        private DevExpress.XtraNavBar.NavBarGroup navSystem;
        private DevExpress.XtraNavBar.NavBarItem navSysEmpl;
        private DevExpress.XtraNavBar.NavBarItem navSysInfoEmpl;
        private DevExpress.XtraNavBar.NavBarItem navSysConfig;
        private DevExpress.XtraNavBar.NavBarItem navCustManager;
        private DevExpress.XtraNavBar.NavBarItem navCustReports;
        private DevExpress.XtraNavBar.NavBarItem navProdNew;
        private DevExpress.XtraNavBar.NavBarItem navProdManager;
        private DevExpress.XtraNavBar.NavBarItem navProdReport;
        private DevExpress.XtraNavBar.NavBarItem navLicNew;
        private DevExpress.XtraNavBar.NavBarItem navLicManager;
        private DevExpress.XtraNavBar.NavBarItem navLicReport;
        private DevExpress.XtraNavBar.NavBarItem navInvNew;
        private DevExpress.XtraNavBar.NavBarItem navInvManager;
        private DevExpress.XtraNavBar.NavBarItem navInvReport;
        private DevExpress.XtraEditors.PictureEdit pictureEdit1;
        private System.Windows.Forms.Timer timer1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarManager barLicense;
        private DevExpress.XtraBars.Bar bar3;
        private DevExpress.XtraBars.BarStaticItem barStaticCompany;
        private DevExpress.XtraBars.BarStaticItem barStaticTimer;
        private DevExpress.XtraBars.BarStaticItem barStaticAction;
        private DevExpress.XtraNavBar.NavBarItem navLicNewSerial;
        private DevExpress.XtraNavBar.NavBarItem navLicSerManager;
        private DevExpress.XtraNavBar.NavBarItem navSysGroupEmpl;
        private DevExpress.XtraNavBar.NavBarItem navSysGroupManager;
    }
}