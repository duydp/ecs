using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DongBoDuLieu.BLL.GC
{
    public class HCT
    {
        public static DataSet GetDataSetALLHangChuyenTiep(Company.GC.BLL.KDT.GC.HopDong HD, string MatKhau)
        {
            
            string queryHang =
                           "SELECT H.* " +
                           " from DCTGC T  inner join DHangCTGC H" +
                           " on H.So_CT=T.So_CT and H.Ma_CT=T.Ma_CT and H.MA_HQ=T.MA_HQ " +
                           " and H.Nam_CT=T.Nam_CT where  T.MA_HQ = '" + HD.MaHaiQuan + "' AND T.DVGC = '" + HD.MaDoanhNghiep + "' and So_HD='" + HD.SoHopDong + "'";
            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, MatKhau);        
            return dsHang;
            
            //TODO: HangChuyenTiep
            /*
            StringBuilder queryTK = new StringBuilder();
            queryTK.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryTK.Append(" SELECT H.* ");
            queryTK.Append(" from DCTGC T  inner join DHangCTGC H ");
            queryTK.Append(" on H.So_CT=T.So_CT and H.Ma_CT=T.Ma_CT and H.MA_HQ=T.MA_HQ and H.Nam_CT=T.Nam_CT ");
            queryTK.Append("WHERE T.Ma_HQ = ''" + HD.MaHaiQuan + "'' AND t.DVGC = ''" + HD.MaDoanhNghiep + "'' and So_HD=''" + HD.SoHopDong + "''");
            queryTK.Append(" ') AS a;");

            DataSet dsHang = WebServiceDB.getDataBySQL(queryTK.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, MatKhau);
            return dsHang;
            */
        }

        public static DataSet GetDataSetHangChuyenTiepByNgay(Company.GC.BLL.KDT.GC.HopDong HD, DateTime NgayBatDau, DateTime NgayKetThuc, string MatKhau)
        {
            string queryHang =
                           "SELECT H.* " +
                           " from DCTGC T  inner join DHangCTGC H" +
                           " on H.So_CT=T.So_CT and H.Ma_CT=T.Ma_CT and H.MA_HQ=T.MA_HQ " +
                           " and H.Nam_CT=T.Nam_CT where  T.MA_HQ = '" + HD.MaHaiQuan + "' AND T.DVGC = '" + HD.MaDoanhNghiep + "' and So_HD='" + HD.SoHopDong + "'" +
                           " AND t.Ngay_CT >= '" + NgayBatDau.ToString("MM/dd/yyyy") + "'" +
                           " AND t.Ngay_CT <= '" + NgayKetThuc.ToString("MM/dd/yyyy") + "'";
            DataSet dsHang = WebServiceDB.getDataBySQL(queryHang.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, MatKhau);
            return dsHang;
        }
    }
}
