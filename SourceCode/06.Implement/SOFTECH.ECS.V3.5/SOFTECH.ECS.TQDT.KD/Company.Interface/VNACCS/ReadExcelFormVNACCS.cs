﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.KDT.SHARE.VNACCS;
using System.Data.OleDb;
using Janus.Windows.GridEX;
using Company.Interface.CauHinh;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
#endif
#if GC_V4
using Company.GC.BLL.GC;
using Company.Interface.VNACCS;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
#endif

namespace Company.Interface
{
    public partial class ReadExcelFormVNACCS : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public string LoaiHinh = "";
        public string TenHang;
        public string MaTienTen_HMD = "";
        private string ConnectionStringExcel = "Driver={Microsoft Excel Driver(*.xls)};DriverId=790;Dbq=";

        public int FormatSoLuong = 2;
        public int FormatSoLuong1 = 2;

        public int FormatDonGia = 6;
        public int FormatTriGia = 6;
        public bool IsSuccess = false;
        public ReadExcelFormVNACCS()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            try
            {
                openFileDialog1.ShowDialog(this);
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.TKMD.HangCollection.Count; i++)
            {
                if (this.TKMD.HangCollection[i].MaQuanLy.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
               // Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                    return null;
                }
                List<String> Collection = new List<string>();
                //Dictionary<string, Worksheet> dict = new Dictionary<string, Worksheet>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                    //dict.Add(worksheet.Name, worksheet);
                }
                return Collection;
            }
            catch (Exception ex)
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                error.SetIconPadding(txtRow, 8);
                return;

            }

            Workbook wb = new Workbook();

            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true);

            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                MLMessages("Không tồn tại sheet \"" + cbbSheetName.Text + "\"MSG_EXC01", "", cbbSheetName.Text, false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int maHangCol = ConvertCharToInt(maHangColumn);
            //NguyenPhuLieu npl = NguyenPhuLieu.Select().Find(delegate(VNACC_Category_CustomsOffice c)
            //{
            //    return c.CustomsCode == temp;
            //});
            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int tenHangCol = ConvertCharToInt(tenHangColumn);

            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text.Trim());
            int maHSCol = ConvertCharToInt(maHSColumn);

            char dVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int dVTCol = ConvertCharToInt(dVTColumn);

            char soLuongColumn = Convert.ToChar(txtSoLuongColumn.Text.Trim());
            int soLuongCol = ConvertCharToInt(soLuongColumn);

            //minhnd 12/04/2015 thêm sl2 vs dvt2
            char dVTColumn2 = Convert.ToChar(txtDVTColumn2.Text.Trim());
            int dVTCol2 = ConvertCharToInt(dVTColumn2);

            char soLuongColumn2 = Convert.ToChar(txtSoLuongColumn2.Text.Trim());
            int soLuongCol2 = ConvertCharToInt(soLuongColumn2);
            //minhnd 12/04/2015

            char donGiaColumn = Convert.ToChar(txtDonGiaColumn.Text.Trim());
            int donGiaCol = ConvertCharToInt(donGiaColumn);

            char xuatXuColumn = Convert.ToChar(txtXuatXuColumn.Text.Trim());
            int xuatXuCol = ConvertCharToInt(xuatXuColumn);

            char TriGiaTTColumn = Convert.ToChar(txtTriGiaTT.Text);
            int TriGiaTTCol = ConvertCharToInt(TriGiaTTColumn);

            char tsXNKColumn = Convert.ToChar(txtBieuThueXNK.Text.Trim());
            int tsXNKCol = ConvertCharToInt(tsXNKColumn);
            char thueSuatColumn = Convert.ToChar(txtThueSuat.Text.Trim());
            int thueSuatCol = ConvertCharToInt(thueSuatColumn);
            #region Thuế thu khác
            int maThueThuKhacCol1 = 0;
            int maThueThuKhacCol2 = 0;
            int maThueThuKhacCol3 = 0;
            int maThueThuKhacCol4 = 0;

            int maMienGiamCol1 = 0;
            int maMienGiamCol2 = 0;
            int maMienGiamCol3 = 0;
            int maMienGiamCol4 = 0;

            int soTienMGCol1 = 0;
            int soTienMGCol2 = 0;
            int soTienMGCol3 = 0;
            int soTienMGCol4 = 0;

            if (!string.IsNullOrEmpty(txtThueKhac1.Text.Trim()))
            {
                maThueThuKhacCol1 = ConvertCharToInt(Convert.ToChar(txtThueKhac1.Text.Trim()));

                maMienGiamCol1 = ConvertCharToInt(Convert.ToChar(txtMaMienGiamThueKhac1.Text.Trim()));

                soTienMGCol1 = ConvertCharToInt(Convert.ToChar(txtSoTienMienGiamThueKhac1.Text.Trim()));
            }
            if (!string.IsNullOrEmpty(txtThueKhac2.Text.Trim()))
            {
                maThueThuKhacCol2 = ConvertCharToInt(Convert.ToChar(txtThueKhac2.Text.Trim()));

                maMienGiamCol2 = ConvertCharToInt(Convert.ToChar(txtMaMienGiamThueKhac2.Text.Trim()));

                soTienMGCol2 = ConvertCharToInt(Convert.ToChar(txtSoTienMienGiamThueKhac2.Text.Trim()));
            }
            if (!string.IsNullOrEmpty(txtThueKhac3.Text.Trim()))
            {
                maThueThuKhacCol3 = ConvertCharToInt(Convert.ToChar(txtThueKhac3.Text.Trim()));

                maMienGiamCol3 = ConvertCharToInt(Convert.ToChar(txtMaMienGiamThueKhac3.Text.Trim()));

                soTienMGCol3 = ConvertCharToInt(Convert.ToChar(txtSoTienMienGiamThueKhac3.Text.Trim()));
            }
            if (!string.IsNullOrEmpty(txtThueKhac4.Text.Trim()))
            {
                maThueThuKhacCol4 = ConvertCharToInt(Convert.ToChar(txtThueKhac4.Text.Trim()));

                maMienGiamCol4 = ConvertCharToInt(Convert.ToChar(txtMaMienGiamThueKhac4.Text.Trim()));

                soTienMGCol4 = ConvertCharToInt(Convert.ToChar(txtSoTienMienGiamThueKhac4.Text.Trim()));
            }

            #endregion


            bool tcvn3 = ckTCVN3.Checked;
            bool checkReadExcel = checkRead.Checked;

            string errorTotal = "";
            string errorMaHangHoa = "";
            string errorMaHangSpecialChar = "";
            string errorTenHangHoa = "";
            string errorMaHS = "";
            string errorMaHSExits = "";
            string errorDVT = "";
            string errorDVTExits = "";
            string errorDVT2 = "";
            string errorDVTExits2 = "";
            string errorXuatXu = "";
            string errorXuatXuValid = "";
            string errorSoLuong = "";
            string errorSoLuongValid = "";
            string errorSoLuong2 = "";
            string errorSoLuongValid2 = "";
            string errorDonGia = "";
            string errorDonGiaValid = "";
            string errorTriGia = "";
            string errorTriGiaValid = "";
            string errorTongTriGiaValid = "";

            string errorNPL = "";
            string errorSP = "";
            string errorTB = "";
            string errorHM = "";
#if GC_V4
            List<GC_NguyenPhuLieu> NPLCollection = GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(TKMD.HopDong_ID);
            List<GC_SanPham> SPCollection = GC_SanPham.SelectCollectionBy_HopDong_ID(TKMD.HopDong_ID);
            List<GC_ThietBi> TBCollection = GC_ThietBi.SelectCollectionBy_HopDong_ID(TKMD.HopDong_ID);
            List<GC_HangMau> HMCollection = GC_HangMau.SelectCollectionDynamic("HopDong_ID = " + TKMD.HopDong_ID, "");
#elif SXXK_V4
            List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionAll();
            List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionAll();
#endif
            List<VNACC_Category_Nation> XuatXuCollection = VNACC_Category_Nation.SelectCollectionAll();
            List<VNACC_Category_HSCode> HSCollection = VNACC_Category_HSCode.SelectCollectionAll();
            List<VNACC_Category_QuantityUnit> DVTCollection = VNACC_Category_QuantityUnit.SelectCollectionAll();
            List<KDT_VNACC_HangMauDich> HangCollection = new List<KDT_VNACC_HangMauDich>();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        bool isAdd = true;
                        bool isExits = false;
                        Company.KDT.SHARE.VNACCS.KDT_VNACC_HangMauDich hmd = new Company.KDT.SHARE.VNACCS.KDT_VNACC_HangMauDich();
                        try
                        {
                            hmd.MaSoHang = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                            if (hmd.MaSoHang.Trim().Length == 0)
                            {
                                errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                foreach (VNACC_Category_HSCode item in HSCollection)
                                {
                                    if (item.HSCode == hmd.MaSoHang)
                                    {
                                        isExits = true;
                                        break;
                                    }
                                }
                            }
                            if (!isExits)
                            {
                                errorMaHSExits += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                                isAdd = false;
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHS += "[" + (wsr.Index + 1) + "]-[" + hmd.MaSoHang + "]\n";
                            isAdd = false;
                        }
                        isExits = false;

#if GC_V4 || SXXK_V4
                        try
                        {
                            hmd.MaHangHoa = Company.KDT.SHARE.Components.Utils.FontConverter.UTF8Literal2Unicode(Convert.ToString(wsr.Cells[maHangCol].Value).Trim());
                            if (hmd.MaHangHoa.Length > 49)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            else if (hmd.MaHangHoa.Length == 0)
                            {
                                errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                isAdd = false;
                            }
                            //else if (Helpers.ValidateSpecialChar(hmd.MaHangHoa))
                            //{
                            //    errorMaHangSpecialChar += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                            //    isAdd = false;
                            //}
                            else
                            {
                                isExits = false;
#if GC_V4
                                switch (TKMD.LoaiHang)
                                {
                                    case "N":
                                        foreach (GC_NguyenPhuLieu item in NPLCollection)
                                        {
                                            if (item.Ma == hmd.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorNPL += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        break;
                                    case "S":
                                        foreach (GC_SanPham item in SPCollection)
                                        {
                                            if (item.Ma == hmd.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorSP += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        break;
                                    case "T":
                                        foreach (GC_ThietBi item in TBCollection)
                                        {
                                            if (item.Ma == hmd.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorTB += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        break;
                                    case "H":
                                        foreach (GC_HangMau item in HMCollection)
                                        {
                                            if (item.Ma == hmd.MaHangHoa)
                                            {
                                                isExits = true;
                                                break;
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorHM += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                            isAdd = false;
                                        }
                                        break;
                                }
#endif
#if SXXK_V4
                            switch (TKMD.LoaiHang)
                            {
                                case "N":
                                    foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                                    {
                                        if (item.Ma == hmd.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorNPL += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                    break;
                                case "S":
                                    foreach (Company.BLL.KDT.SXXK.SXXK_SanPham item in SPCollection)
                                    {
                                        if (item.Ma == hmd.MaHangHoa)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        errorSP += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                    break;
                            }
#endif
                            }
                        }
                        catch (Exception)
                        {
                            errorMaHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                            isAdd = false;
                        }
#elif KD_V4
                            hmd.MaQuanLy = Convert.ToString(wsr.Cells[maHangCol].Value).ToUpper().Trim();
                            if (hmd.MaQuanLy.Length > 7)
                            {
                                //MLMessages("Mã quản lý  riêng dòng " + (wsr.Index + 1) + " không hợp lệ. Nhập tối đa 7 ký tự ", "MSG_EXC09", Convert.ToString(wsr.Index + 1), false);
                                return;
                            }
#endif
                            try
                            {
                                if (!tcvn3)
                                    hmd.TenHang = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                                else
                                    hmd.TenHang = TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.UTF8Literal2Unicode(wsr.Cells[tenHangCol].Value.ToString());
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.TenHang + "]\n";
                                isAdd = false;
                            }
                            #region
#if GC_V4
                        if (checkReadExcel)
                        {
                            isExits = false;
                            switch (TKMD.LoaiHang)
                            {
                                case "N":
                                    foreach (GC_NguyenPhuLieu item in NPLCollection)
                                    {
                                        if (item.Ma == hmd.MaHangHoa)
                                        {
                                            hmd.TenHang = item.Ten;
                                            hmd.MaSoHang = item.MaHS;
                                            hmd.DVTLuong1 = this.DVT_VNACC(item.DVT_ID);
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong1.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong1)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong2.Trim().Length == 0)
                                            {
                                                errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong2)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                        errorNPL += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                    break;
                                case "S":
                                    foreach (GC_SanPham item in SPCollection)
                                    {
                                        if (item.Ma == hmd.MaHangHoa)
                                        {
                                            hmd.TenHang = item.Ten;
                                            hmd.MaSoHang = item.MaHS;
                                            hmd.DVTLuong1 = this.DVT_VNACC(item.DVT_ID);
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong1.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong1)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong2.Trim().Length == 0)
                                            {
                                                errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong2)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                        errorSP += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                    break;
                                case "T":
                                    foreach (GC_ThietBi item in TBCollection)
                                    {
                                        if (item.Ma == hmd.MaHangHoa)
                                        {
                                            hmd.TenHang = item.Ten;
                                            hmd.MaSoHang = item.MaHS;
                                            hmd.DVTLuong1 = this.DVT_VNACC(item.DVT_ID);
                                            isExits = true;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong1.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong1)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong2.Trim().Length == 0)
                                            {
                                                errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong2)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                        errorTB += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                    break;
                                case "H":
                                    foreach (GC_HangMau item in HMCollection)
                                    {
                                        if (item.Ma == hmd.MaHangHoa)
                                        {
                                            hmd.TenHang = item.Ten;
                                            hmd.MaSoHang = item.MaHS;
                                            hmd.DVTLuong1 = this.DVT_VNACC(item.DVT_ID);
                                            isExits = true;
                                            break;
                                        }
                                    }
                                    if (!isExits)
                                    {
                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                                            if (hmd.DVTLuong1.Trim().Length == 0)
                                            {
                                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong1)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }

                                        try
                                        {
                                            isExits = false;
                                            hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim();
                                            if (hmd.DVTLuong2.Trim().Length == 0)
                                            {
                                                errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                            else
                                            {
                                                foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                                {
                                                    if (item.Code == hmd.DVTLuong2)
                                                    {
                                                        isExits = true;
                                                        break;
                                                    }
                                                }
                                            }
                                            if (!isExits)
                                            {
                                                errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                        catch (Exception)
                                        {
                                            errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                        errorHM += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                        isAdd = false;
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            try
                            {
                                hmd.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.UTF8Literal2Unicode(wsr.Cells[tenHangCol].Value.ToString());
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.TenHang + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                                if (hmd.DVTLuong1.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == hmd.DVTLuong1)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                isAdd = false;
                            }
                            isExits = false;
                        }
#endif
                            #endregion
                            #region
#if SXXK_V4
                        if (checkReadExcel)
	                    {
                            if (TKMD.LoaiHang == "N")
                            {
                                isExits = false;
                                foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                                {
                                    if (item.Ma == hmd.MaHangHoa)
                                    {
                                        hmd.TenHang = item.Ten;
                                        hmd.MaSoHang = item.MaHS;
                                        hmd.DVTLuong1 = this.DVT_VNACC(item.DVT_ID);
                                        isExits = true;
                                    }
                                }
                                if (!isExits)
                                {
                                    try
                                    {
                                        isExits = false;
                                        hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim();
                                        if (hmd.DVTLuong1.Trim().Length == 0)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                            {
                                                if (item.Code == hmd.DVTLuong1)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                        isAdd = false;
                                    }
                                    try
                                    {
                                        isExits = false;
                                        hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim();
                                        if (hmd.DVTLuong2.Trim().Length == 0)
                                        {
                                            errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                            {
                                                if (item.Code == hmd.DVTLuong2)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                        isAdd = false;
                                    }
                                    errorNPL += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                }
                            }
                            else
                            {
                                foreach (Company.BLL.KDT.SXXK.SXXK_SanPham item in SPCollection)
                                {
                                    if (item.Ma == hmd.MaHangHoa)
                                    {
                                        hmd.TenHang = item.Ten;
                                        hmd.MaSoHang = item.MaHS;
                                        hmd.DVTLuong1 = this.DVT_VNACC(item.DVT_ID);
                                        isExits = true;
                                        break;
                                    }
                                }
                                if (!isExits)
                                {
                                    try
                                    {
                                        isExits = false;
                                        hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim();
                                        if (hmd.DVTLuong1.Trim().Length == 0)
                                        {
                                            errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                            {
                                                if (item.Code == hmd.DVTLuong1)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                        isAdd = false;
                                    }
                                    try
                                    {
                                        isExits = false;
                                        hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim();
                                        if (hmd.DVTLuong2.Trim().Length == 0)
                                        {
                                            errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                            {
                                                if (item.Code == hmd.DVTLuong2)
                                                {
                                                    isExits = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!isExits)
                                        {
                                            errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                            isAdd = false;
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                        isAdd = false;
                                    }
                                    errorSP += "[" + (wsr.Index + 1) + "]-[" + hmd.MaHangHoa + "]\n";
                                }
                            }
	                    }
                        else
	                    {
                            try
                            {
                                hmd.TenHang = Company.KDT.SHARE.Components.Utils.FontConverter.UTF8Literal2Unicode(wsr.Cells[tenHangCol].Value.ToString());
                            }
                            catch (Exception)
                            {
                                errorTenHangHoa += "[" + (wsr.Index + 1) + "]-[" + hmd.TenHang + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim();
                                if (hmd.DVTLuong1.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == hmd.DVTLuong1)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                isAdd = false;
                            }
	                    }
                        
#endif
                            #endregion
#if KD_V4
                            try
                            {
                                isExits = false;
                                hmd.DVTLuong1 = Convert.ToString(wsr.Cells[dVTCol].Value).Trim().ToUpper();
                                if (hmd.DVTLuong1.Trim().Length == 0)
                                {
                                    errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == hmd.DVTLuong1)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong1 + "]\n";
                                isAdd = false;
                            }
                            isExits = false;
#endif
                            try
                            {
                                isExits = false;
                                hmd.NuocXuatXu = Convert.ToString(wsr.Cells[xuatXuCol].Value).Trim().ToUpper();
                                if (hmd.NuocXuatXu.Length == 0)
                                {
                                    errorXuatXu += "[" + (wsr.Index + 1) + "]-[" + hmd.NuocXuatXu + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_Nation item in XuatXuCollection)
                                    {
                                        if (item.NationCode == hmd.NuocXuatXu)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorXuatXuValid += "[" + (wsr.Index + 1) + "]-[" + hmd.NuocXuatXu + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorXuatXu += "[" + (wsr.Index + 1) + "]-[" + hmd.NuocXuatXu + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                isExits = false;
                                hmd.DVTLuong2 = Convert.ToString(wsr.Cells[dVTCol2].Value).Trim().ToUpper();
                                if (hmd.DVTLuong2.Trim().Length == 0)
                                {
                                    errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    foreach (VNACC_Category_QuantityUnit item in DVTCollection)
                                    {
                                        if (item.Code == hmd.DVTLuong2)
                                        {
                                            isExits = true;
                                            break;
                                        }
                                    }
                                }
                                if (!isExits)
                                {
                                    errorDVTExits2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                    isAdd = false;
                                }
                            }
                            catch (Exception)
                            {
                                errorDVT2 += "[" + (wsr.Index + 1) + "]-[" + hmd.DVTLuong2 + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                hmd.SoLuong1 = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                                if (hmd.SoLuong1.ToString().Length == 0)
                                {
                                    errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong1 + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(hmd.SoLuong1, FormatSoLuong) != hmd.SoLuong1)
                                    {
                                        errorSoLuongValid += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong1 + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuong += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong1 + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                hmd.SoLuong2 = Convert.ToDecimal(wsr.Cells[soLuongCol2].Value);
                                if (hmd.SoLuong2.ToString().Length == 0)
                                {
                                    errorSoLuong2 += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong2 + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(hmd.SoLuong2, FormatSoLuong1) != hmd.SoLuong2)
                                    {
                                        errorSoLuongValid2 += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong2 + "]\n";
                                        isAdd = false;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorSoLuong2 += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong2 + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                hmd.DonGiaHoaDon = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[donGiaCol].Value));
                                //if (hmd.DonGiaHoaDon.ToString().Length == 0)
                                //{
                                //    errorDonGia += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                                //    isAdd = false;
                                //}
                                //else
                                //{
                                //    if (Decimal.Round(hmd.DonGiaHoaDon, FormatDonGia) != hmd.DonGiaHoaDon)
                                //    {
                                //        errorDonGiaValid += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                                //        isAdd = false;
                                //    }
                                //}
                            }
                            catch (Exception)
                            {
                                errorDonGia += "[" + (wsr.Index + 1) + "]-[" + hmd.DonGiaHoaDon + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                hmd.TriGiaHoaDon = Convert.ToDecimal(Convert.ToDecimal(wsr.Cells[TriGiaTTCol].Value));
                                if (hmd.TriGiaHoaDon.ToString().Length == 0)
                                {
                                    errorTriGia += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (Decimal.Round(hmd.TriGiaHoaDon, FormatTriGia) != hmd.TriGiaHoaDon)
                                    {
                                        errorTriGiaValid += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                                        isAdd = false;
                                        isExits = true;
                                    }
                                    //if (!isExits)
                                    //{
                                    //    if (hmd.SoLuong1 * hmd.DonGiaHoaDon != hmd.TriGiaHoaDon)
                                    //    {
                                    //        errorTongTriGiaValid += "[" + (wsr.Index + 1) + "]-[" + hmd.SoLuong1 + "]-[" + hmd.DonGiaHoaDon + "]-[" + hmd.TriGiaHoaDon + "]\n";
                                    //        isAdd = false;
                                    //    }
                                    //}
                                }
                            }
                            catch (Exception ex)
                            {
                                errorTriGia += "[" + (wsr.Index + 1) + "]-[" + hmd.TriGiaHoaDon + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                hmd.MaBieuThueNK = Convert.ToString(wsr.Cells[tsXNKCol].Value);
                            }
                            catch (Exception ex)
                            {

                            }
                            try
                            {
                                hmd.ThueSuat = Convert.ToDecimal(wsr.Cells[thueSuatCol].Value);
                            }
                            catch (Exception)
                            {

                            }
#if GC_V4 || SXXK_V4
                            try
                            {
                                hmd.MaMienGiamThue = ctrMaMienGiam.Code;
                            }
                            catch (Exception)
                            {

                            }
#endif
                            hmd.MaTTDonGia = MaTienTen_HMD.Trim();
                            hmd.DVTDonGia = hmd.DVTLuong1;


                            #region Thuế khác

                            if (maThueThuKhacCol1 > 0)
                            {

                                string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol1].Value);
                                string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol1].Value);
                                string SoTien = Convert.ToString(wsr.Cells[soTienMGCol1].Value);
                                if (LoaiHinh != "X")
                                {
                                    if (MaMucThue != "")
                                    {
                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol1].Value);
                                        thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol1].Value);
                                        thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol1].Value);
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }
                            if (maThueThuKhacCol2 > 0)
                            {
                                string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol2].Value);
                                string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol2].Value);
                                string SoTien = Convert.ToString(wsr.Cells[soTienMGCol2].Value);
                                if (LoaiHinh != "X")
                                {

                                    if (MaMucThue != "")
                                    {
                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol2].Value);
                                        thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol2].Value);
                                        thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol2].Value);
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }
                            if (maThueThuKhacCol3 > 0)
                            {
                                string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol3].Value);
                                string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol3].Value);
                                string SoTien = Convert.ToString(wsr.Cells[soTienMGCol3].Value);
                                if (LoaiHinh != "X")
                                {
                                    if (MaMucThue != "")
                                    {
                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol3].Value);
                                        thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol3].Value);
                                        thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol3].Value);
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }
                            if (maThueThuKhacCol4 > 0)
                            {
                                string MaMucThue = Convert.ToString(wsr.Cells[maThueThuKhacCol4].Value);
                                string MaMienGiam = Convert.ToString(wsr.Cells[maMienGiamCol4].Value);
                                string SoTien = Convert.ToString(wsr.Cells[soTienMGCol4].Value);
                                if (LoaiHinh != "X")
                                {
                                    if (MaMucThue != "")
                                    {
                                        KDT_VNACC_HangMauDich_ThueThuKhac thue = new KDT_VNACC_HangMauDich_ThueThuKhac();
                                        thue.MaTSThueThuKhac = Convert.ToString(wsr.Cells[maThueThuKhacCol4].Value);
                                        thue.MaMGThueThuKhac = Convert.ToString(wsr.Cells[maMienGiamCol4].Value);
                                        thue.SoTienGiamThueThuKhac = string.IsNullOrEmpty(SoTien) ? 0 : Convert.ToDecimal(wsr.Cells[soTienMGCol4].Value);
                                        hmd.ThueThuKhacCollection.Add(thue);
                                    }
                                }
                            }

                            #endregion

                            if (isAdd)
                                HangCollection.Add(hmd);

                        }
                        catch (Exception ex)
                        {
                            this.SaveDefault();
                            return;
                        }
                    }
            }
                if (!String.IsNullOrEmpty(errorMaHangHoa))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangHoa + " không được để trống";
                if (!String.IsNullOrEmpty(errorMaHangSpecialChar))
                    errorTotal += "\n - [STT] -[MÃ HÀNG HÓA] : \n" + errorMaHangSpecialChar + " chứa các ký tự đặc biệt hoặc Unicode hoặc xuống dòng \n";
                if (!String.IsNullOrEmpty(errorTenHangHoa))
                    errorTotal += "\n - [STT] -[TÊN HÀNG HÓA] : \n" + errorTenHangHoa + " không được để trống";
                if (!String.IsNullOrEmpty(errorDVT))
                    errorTotal += "\n - [STT] -[ĐVT 1] : \n" + errorDVT + " không được để trống";
                if (!String.IsNullOrEmpty(errorDVTExits))
                    errorTotal += "\n - [STT] -[ĐVT 1] : \n" + errorDVTExits + " không hợp lệ. ĐVT phải là ĐVT VNACCS (Ví dụ : Cái/Chiếc là : PCE)";
                if (!String.IsNullOrEmpty(errorDVT2))
                    errorTotal += "\n - [STT] -[ĐVT 2] : \n" + errorDVT2 + " không được để trống";
                if (!String.IsNullOrEmpty(errorDVTExits2))
                    errorTotal += "\n - [STT] -[ĐVT 2] : \n" + errorDVTExits2 + " không hợp lệ. ĐVT phải là ĐVT VNACCS (Ví dụ : Cái/Chiếc là : PCE)";
                if (!String.IsNullOrEmpty(errorXuatXu))
                    errorTotal += "\n - [STT] -[NƯỚC XUẤT XỨ] : \n" + errorXuatXu + " không được để trống";
                if (!String.IsNullOrEmpty(errorXuatXuValid))
                    errorTotal += "\n - [STT] -[NƯỚC XUẤT XỨ] : \n" + errorXuatXuValid + " không hợp lệ .Ví dụ : Việt Nam : VN";
                if (!String.IsNullOrEmpty(errorMaHS))
                    errorTotal += "\n - [STT] -[MÃ HS] : " + errorMaHS + " không được để trống";
                if (!String.IsNullOrEmpty(errorMaHSExits))
                    errorTotal += "\n - [STT] -[MÃ HS] : \n" + errorMaHSExits + "không hợp lê. Mã HS có độ dài là 8 số .";
                if (!String.IsNullOrEmpty(errorNPL))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorNPL + " chưa được đăng ký";
                if (!String.IsNullOrEmpty(errorSP))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " chưa được đăng ký";
                if (!String.IsNullOrEmpty(errorTB))
                    errorTotal += "\n - [STT] -[MÃ THIẾT BỊ] : \n" + errorTB + "chưa được đăng ký";
                if (!String.IsNullOrEmpty(errorHM))
                    errorTotal += "\n - [STT] -[MÃ HÀNG MẪU] : \n" + errorHM + "chưa được đăng ký";
                if (!String.IsNullOrEmpty(errorSoLuong))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG 1] : " + errorSoLuong + " không được để trống";
                if (!String.IsNullOrEmpty(errorSoLuongValid))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG 1] : \n" + errorSoLuongValid + " đã cấu hình chỉ được nhập tối đa " + FormatSoLuong + " số thập phân . Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel \n";
                if (!String.IsNullOrEmpty(errorSoLuong2))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG 2] : " + errorSoLuong2 + " không được để trống";
                if (!String.IsNullOrEmpty(errorSoLuongValid2))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG 2] : \n" + errorSoLuongValid2 + " đã cấu hình chỉ được nhập tối đa " + FormatSoLuong1 + " số thập phân .Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
                if (!String.IsNullOrEmpty(errorDonGia))
                    errorTotal += "\n - [STT] -[ĐƠN GIÁ] : " + errorDonGia + " không được để trống";
                if (!String.IsNullOrEmpty(errorDonGiaValid))
                    errorTotal += "\n - [STT] -[ĐƠN GIÁ] : \n" + errorDonGiaValid + " đã cấu hình chỉ được nhập tối đa " + FormatDonGia + " số thập phân . Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
                if (!String.IsNullOrEmpty(errorTriGia))
                    errorTotal += "\n - [STT] -[TRỊ GIÁ] : " + errorTriGia + " không được để trống";
                if (!String.IsNullOrEmpty(errorTriGiaValid))
                    errorTotal += "\n - [STT] -[TRỊ GIÁ] : \n" + errorTriGiaValid + " đã cấu hình chỉ được nhập tối đa " + FormatTriGia + " số thập phân  .Để cấu hình : Meunu Hệ thống -  Cấu hình hệ thống - Cấu hình Thông số đọc Excel\n";
                if (!String.IsNullOrEmpty(errorTongTriGiaValid))
                    errorTotal += "\n - [STT] -[SỐ LƯỢNG]- [ĐƠN GIÁ] - [TRỊ GIÁ] : " + errorTongTriGiaValid + " Tổng trị giá không đúng bằng : Số lượng * Đơn giá";

                if (String.IsNullOrEmpty(errorTotal))
                {
                    foreach (KDT_VNACC_HangMauDich item in HangCollection)
                    {
                        TKMD.HangCollection.Add(item);
                    }
                    IsSuccess = true;
                    ShowMessageTQDT("Thông báo từ hệ thống", "Nhập hàng từ Excel thành công ", false);
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Nhập hàng từ Excel không thành công. \n" + errorTotal, false);
                }
                this.SaveDefault();
                this.Close();
        }
        private List<KDT_VNACC_ToKhaiMauDich> CopyToKhai(KDT_VNACC_ToKhaiMauDich tkmd, bool isCopyHangHoa, int number)
        {
            List<KDT_VNACC_ToKhaiMauDich> ToKhaiMauDichCollection = new List<KDT_VNACC_ToKhaiMauDich>();
            int index = 0;
            int count = 49;
            for (int i = 0; i < number; i++)
            {
                KDT_VNACC_ToKhaiMauDich tkmdcopy = new KDT_VNACC_ToKhaiMauDich();
                HelperVNACCS.UpdateObject<KDT_VNACC_ToKhaiMauDich>(tkmdcopy, tkmd, false);
                tkmdcopy.NgayDangKy = new DateTime(1900, 1, 1);
                tkmdcopy.SoToKhai = 0;
                tkmdcopy.SoNhanhToKhai = 0;
                tkmdcopy.SoToKhaiDauTien = "";
                if (i==0)
                    tkmdcopy.SoToKhaiDauTien = "F";
                tkmdcopy.SoNhanhToKhai = i + 1;
                tkmdcopy.TongSoTKChiaNho = number;
                tkmdcopy.MaPhanLoaiKiemTra = string.Empty;
                tkmdcopy.AnHangCollection = new List<KDT_VNACC_TK_PhanHoi_AnHan>();
                tkmdcopy.ChiThiHQCollection = new List<KDT_VNACC_ChiThiHaiQuan>();
                tkmdcopy.SacThueCollection = new List<KDT_VNACC_TK_PhanHoi_SacThue>();
                tkmdcopy.TrangThaiXuLy = "0";
                tkmdcopy.TyGiaCollection = new List<KDT_VNACC_TK_PhanHoi_TyGia>();
                tkmdcopy.InputMessageID = string.Empty;
                tkmdcopy.MessageTag = string.Empty;
                tkmdcopy.IndexTag = string.Empty;
                tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
                tkmdcopy.HangCollection = tkmd.HangCollection.GetRange(index, count);
                index += 50;
                count += 49;
                ToKhaiMauDichCollection.Add(tkmdcopy);
            }
            return ToKhaiMauDichCollection;
            //if (!isCopyHangHoa)
            //{
            //    tkmdcopy.HangCollection = new List<KDT_VNACC_HangMauDich>();
            //}
            //tkmdcopy.HangCollection = tkmd.HangCollection;

            //DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + tkmdcopy.MaLoaiHinh + "'", "ID");
            //DataRow dr = ds.Tables[0].Rows[0];
            //string loaiHinh = dr["ReferenceDB"].ToString();
            //if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
            //{
            //    VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
            //    //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
            //    f.isCopy = true;
            //    f.TKMD = tkmdcopy;
            //    f.ShowDialog();
            //}
            //if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
            //{
            //    VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
            //    //f.TKMD = new KDT_VNACC_ToKhaiMauDich();
            //    f.IsCopy = true;
            //    f.TKMD = tkmdcopy;
            //    f.ShowDialog();
            //}
        }
        private void SaveDefault()
        {
            try
            {
                DocExcel_VNACCS docExcel = new DocExcel_VNACCS();
                docExcel.insertThongSoHangTK(cbbSheetName.Text.Trim(),
                                                                      txtRow.Text.Trim(),
                                                                      txtMaHangColumn.Text.Trim(),
                                                                      txtTenHangColumn.Text.Trim(),
                                                                      txtMaHSColumn.Text.Trim(),
                                                                      txtXuatXuColumn.Text.Trim(),
                                                                      txtSoLuongColumn.Text.Trim(),
                                                                      txtSoLuongColumn2.Text.Trim(),
                                                                      txtDVTColumn.Text.Trim(),
                                                                      txtDVTColumn2.Text.Trim(),
                    ////minhnd12/04/2015
                    //txtSoLuongColumn2.Text.Trim(),
                    //txtDVTColumn2.Text.Trim(),
                    ////minhnd12/04/2015
                                                                      txtDonGiaColumn.Text.Trim(),
                                                                      txtTriGiaTT.Text.Trim(),
                                                                      txtBieuThueXNK.Text.Trim(),
                                                                      txtThueSuat.Text.Trim(),
                                                                      txtMaMienGiamThueKhac1.Text.Trim(),
                                                                      txtThueKhac1.Text.Trim(),
                                                                      txtSoTienMienGiamThueKhac1.Text.Trim()

                                                                      );
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void ReadDefault()
        {
            try
            {
                //DocExcel_VNACCS docExcel = new DocExcel_VNACCS();
                FrmCauHinhThongSoReadExcel docExcel = new FrmCauHinhThongSoReadExcel();
                docExcel.ReadDefault(cbbSheetName,
                                      txtRow,
                                      txtMaHangColumn,
                                      txtTenHangColumn,
                                      txtMaHSColumn,
                                      txtXuatXuColumn,
                                      txtSoLuongColumn,
                                      txtSoLuongColumn2,
                                      txtDVTColumn,
                                      txtDVTColumn2,
                                      txtDonGiaColumn,
                                      txtTriGiaTT,
                                      txtBieuThueXNK,
                                      txtThueSuat,
                                      txtThueKhac1,
                                      txtMaMienGiamThueKhac1,
                                      txtSoTienMienGiamThueKhac1
                                      );
                docExcel.FormatSoLuong = FormatSoLuong;
                docExcel.FormatSoLuong1 = FormatSoLuong1;
                docExcel.FormatDonGia = FormatDonGia;
                docExcel.FormatTriGia = FormatTriGia;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            try
            {
                ReadDefault();
                if (LoaiHinh == "X")
                {
                    lblXuatXu.Visible = true;
                    txtXuatXuColumn.Visible = true;
                    lblThue.Visible = false;
                    txtBieuThueXNK.Visible = false;
                    lblMucThue.Visible = false;
                    txtThueKhac1.Visible = false;
                    lblMaMienGiam.Visible = false;
                    txtMaMienGiamThueKhac1.Visible = false;
                    lblTienMG.Visible = false;
                    txtSoTienMienGiamThueKhac1.Visible = false;
                    grbThuKhac.Visible = false;
                    uiGroupBox1.Height = uiGroupBox1.Height - grbThuKhac.Height;
                    this.Height = this.Height - grbThuKhac.Height;
                }
#if GC_V4
                if (LoaiHinh == "N")
                {
                    ctrMaMienGiam.ImportType = EImportExport.I;
                    ctrMaMienGiam.ReLoadData();
                    ctrMaMienGiam.Code = "XNG81";
                }
                else
                {
                    ctrMaMienGiam.ImportType = EImportExport.E;
                    ctrMaMienGiam.Code = "XNG82";
                    ctrMaMienGiam.ReLoadData();
                }
#elif SXXK_V4
                if (LoaiHinh == "N")
                {
                    ctrMaMienGiam.ImportType = EImportExport.I;
                    ctrMaMienGiam.ReLoadData();
                    ctrMaMienGiam.Code = "XN210";
                }
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }


        private void lblLinkExcelTemplate_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_HangHoa_VANCCS();
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtXuatXuColumn_TextChanged(object sender, EventArgs e)
        {

        }


    }
}