using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAGP110_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute MDC { get; set; }
public PropertiesAttribute HSC { get; set; }
public PropertiesAttribute ACE { get; set; }
public PropertiesAttribute QSD { get; set; }
public PropertiesAttribute RNO { get; set; }
public PropertiesAttribute EOM { get; set; }
public PropertiesAttribute QTT { get; set; }
public PropertiesAttribute QTU { get; set; }
public PropertiesAttribute ACA { get; set; }
public PropertiesAttribute EFF { get; set; }
public PropertiesAttribute TWE { get; set; }
public PropertiesAttribute TWU { get; set; }
public PropertiesAttribute IMP { get; set; }
public PropertiesAttribute SWP { get; set; }
public PropertiesAttribute ESP { get; set; }
public PropertiesAttribute EFN { get; set; }
public PropertiesAttribute EXC { get; set; }
public PropertiesAttribute EFA { get; set; }
public PropertiesAttribute EFS { get; set; }
public PropertiesAttribute EFD { get; set; }
public PropertiesAttribute EFC { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute MFN { get; set; }
public PropertiesAttribute MPC { get; set; }
public PropertiesAttribute MAB { get; set; }
public PropertiesAttribute MSN { get; set; }
public PropertiesAttribute MDN { get; set; }
public PropertiesAttribute MCN { get; set; }
public PropertiesAttribute MCC { get; set; }
public PropertiesAttribute DTN { get; set; }
public PropertiesAttribute DPC { get; set; }
public PropertiesAttribute DAB { get; set; }
public PropertiesAttribute DSN { get; set; }
public PropertiesAttribute DDN { get; set; }
public PropertiesAttribute DCN { get; set; }
public PropertiesAttribute DCC { get; set; }
public PropertiesAttribute TTN { get; set; }
public PropertiesAttribute TPC { get; set; }
public PropertiesAttribute TAB { get; set; }
public PropertiesAttribute TSN { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute TCN { get; set; }
public PropertiesAttribute TCC { get; set; }
public PropertiesAttribute QIS { get; set; }
public PropertiesAttribute QSU { get; set; }
public PropertiesAttribute QIM { get; set; }
public PropertiesAttribute TQA { get; set; }
public PropertiesAttribute TEX { get; set; }
public PropertiesAttribute ISU { get; set; }
public PropertiesAttribute DMD { get; set; }
public PropertiesAttribute RMK { get; set; }

public VAGP110_HANG()
        {
MDC = new PropertiesAttribute(768, typeof(string));
HSC = new PropertiesAttribute(12, typeof(string));
ACE = new PropertiesAttribute(35, typeof(string));
QSD = new PropertiesAttribute(35, typeof(string));
RNO = new PropertiesAttribute(17, typeof(string));
EOM = new PropertiesAttribute(8, typeof(DateTime));
QTT = new PropertiesAttribute(8, typeof(int));
QTU = new PropertiesAttribute(3, typeof(string));
ACA = new PropertiesAttribute(70, typeof(string));
EFF = new PropertiesAttribute(50, typeof(string));
TWE = new PropertiesAttribute(10, typeof(int));
TWU = new PropertiesAttribute(3, typeof(string));
IMP = new PropertiesAttribute(19, typeof(int));
SWP = new PropertiesAttribute(19, typeof(int));
ESP = new PropertiesAttribute(19, typeof(int));
EFN = new PropertiesAttribute(70, typeof(string));
EXC = new PropertiesAttribute(9, typeof(string));
EFA = new PropertiesAttribute(35, typeof(string));
EFS = new PropertiesAttribute(35, typeof(string));
EFD = new PropertiesAttribute(35, typeof(string));
EFC = new PropertiesAttribute(35, typeof(string));
ECC = new PropertiesAttribute(2, typeof(string));
MFN = new PropertiesAttribute(70, typeof(string));
MPC = new PropertiesAttribute(9, typeof(string));
MAB = new PropertiesAttribute(35, typeof(string));
MSN = new PropertiesAttribute(35, typeof(string));
MDN = new PropertiesAttribute(35, typeof(string));
MCN = new PropertiesAttribute(35, typeof(string));
MCC = new PropertiesAttribute(2, typeof(string));
DTN = new PropertiesAttribute(70, typeof(string));
DPC = new PropertiesAttribute(9, typeof(string));
DAB = new PropertiesAttribute(35, typeof(string));
DSN = new PropertiesAttribute(35, typeof(string));
DDN = new PropertiesAttribute(35, typeof(string));
DCN = new PropertiesAttribute(35, typeof(string));
DCC = new PropertiesAttribute(2, typeof(string));
TTN = new PropertiesAttribute(70, typeof(string));
TPC = new PropertiesAttribute(9, typeof(string));
TAB = new PropertiesAttribute(35, typeof(string));
TSN = new PropertiesAttribute(35, typeof(string));
TDN = new PropertiesAttribute(35, typeof(string));
TCN = new PropertiesAttribute(35, typeof(string));
TCC = new PropertiesAttribute(2, typeof(string));
QIS = new PropertiesAttribute(8, typeof(int));
QSU = new PropertiesAttribute(3, typeof(string));
QIM = new PropertiesAttribute(8, typeof(int));
TQA = new PropertiesAttribute(9, typeof(int));
TEX = new PropertiesAttribute(9, typeof(int));
ISU = new PropertiesAttribute(8, typeof(int));
DMD = new PropertiesAttribute(8, typeof(int));
RMK = new PropertiesAttribute(105, typeof(string));
TongSoByte = 2220;
}

}public partial class EnumGroupID
    {
}

}
