﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.Interface.DongBoDuLieu
{
    public class SyncEventArgs : EventArgs
    {
        public Exception Error { get; private set; }
        public string Message { get; private set; }
        public int Percent { get; private set; }
        public object Data { get; private set; }
        public SyncEventArgs(string message, int percent, Exception ex, object data)
        {
            Message = message;
            Error = ex;
            Percent = percent;
            Data = data;
        }
        public SyncEventArgs(string message, int percent)
            : this(message, percent, null, null)
        {
        }
        public SyncEventArgs(Exception ex)
            : this(string.Empty, 0, ex, null)
        {

        }
    }
}
