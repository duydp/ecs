
/****** Object:  StoredProcedure [dbo].[p_GiayPhep_Delete]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_DeleteDynamic]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_Insert]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_InsertUpdate]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_Load]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_SelectAll]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_SelectDynamic]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_GiayPhep_Update]    Script Date: 11/12/2013 08:29:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GiayPhep_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GiayPhep_Update]
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich]    Script Date: 11/11/2013 16:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[PhanLoaiBaoCaoSuaDoi] [varchar](1) NULL,
	[SoToKhaiDauTien] [numeric](12, 0) NULL,
	[SoNhanhToKhai] [numeric](2, 0) NULL,
	[TongSoTKChiaNho] [numeric](2, 0) NULL,
	[SoToKhaiTNTX] [numeric](12, 0) NULL,
	[MaLoaiHinh] [varchar](3) NULL,
	[MaPhanLoaiHH] [varchar](1) NULL,
	[MaPhuongThucVT] [varchar](1) NULL,
	[PhanLoaiToChuc] [varchar](1) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[NhomXuLyHS] [varchar](2) NULL,
	[ThoiHanTaiNhapTaiXuat] [datetime] NULL,
	[NgayDangKy] [datetime] NULL,
	[MaDonVi] [varchar](13) NULL,
	[TenDonVi] [nvarchar](100) NULL,
	[MaBuuChinhDonVi] [varchar](7) NULL,
	[DiaChiDonVi] [nvarchar](100) NULL,
	[SoDienThoaiDonVi] [varchar](20) NULL,
	[MaUyThac] [varchar](13) NULL,
	[TenUyThac] [nvarchar](100) NULL,
	[MaDoiTac] [varchar](13) NULL,
	[TenDoiTac] [nvarchar](100) NULL,
	[MaBuuChinhDoiTac] [varchar](7) NULL,
	[DiaChiDoiTac1] [nvarchar](35) NULL,
	[DiaChiDoiTac2] [nvarchar](35) NULL,
	[DiaChiDoiTac3] [nvarchar](35) NULL,
	[DiaChiDoiTac4] [nvarchar](35) NULL,
	[MaNuocDoiTac] [varchar](2) NULL,
	[NguoiUyThacXK] [nvarchar](70) NULL,
	[MaDaiLyHQ] [varchar](5) NULL,
	[SoLuong] [numeric](8, 0) NULL,
	[MaDVTSoLuong] [varchar](3) NULL,
	[TrongLuong] [numeric](14, 4) NULL,
	[MaDVTTrongLuong] [varchar](3) NULL,
	[MaDDLuuKho] [varchar](7) NULL,
	[SoHieuKyHieu] [varchar](140) NULL,
	[MaPTVC] [varchar](9) NULL,
	[TenPTVC] [nvarchar](35) NULL,
	[NgayHangDen] [datetime] NULL,
	[MaDiaDiemDoHang] [varchar](6) NULL,
	[TenDiaDiemDohang] [nvarchar](35) NULL,
	[MaDiaDiemXepHang] [varchar](6) NULL,
	[TenDiaDiemXepHang] [nvarchar](35) NULL,
	[SoLuongCont] [numeric](3, 0) NULL,
	[MaKetQuaKiemTra] [varchar](1) NULL,
	[MaVanbanPhapQuy1] [varchar](2) NULL,
	[MaVanbanPhapQuy2] [varchar](2) NULL,
	[MaVanbanPhapQuy3] [varchar](2) NULL,
	[MaVanbanPhapQuy4] [varchar](2) NULL,
	[MaVanbanPhapQuy5] [varchar](2) NULL,
	[PhanLoaiHD] [varchar](1) NULL,
	[SoTiepNhanHD] [numeric](12, 0) NULL,
	[SoHoaDon] [varchar](35) NULL,
	[NgayPhatHanhHD] [datetime] NULL,
	[PhuongThucTT] [varchar](7) NULL,
	[PhanLoaiGiaHD] [varchar](1) NULL,
	[MaDieuKienGiaHD] [varchar](3) NULL,
	[MaTTHoaDon] [varchar](3) NULL,
	[TongTriGiaHD] [numeric](24, 4) NULL,
	[MaPhanLoaiTriGia] [varchar](1) NULL,
	[SoTiepNhanTKTriGia] [numeric](9, 0) NULL,
	[MaTTHieuChinhTriGia] [varchar](3) NULL,
	[GiaHieuChinhTriGia] [numeric](24, 4) NULL,
	[MaPhanLoaiPhiVC] [varchar](1) NULL,
	[MaTTPhiVC] [varchar](3) NULL,
	[PhiVanChuyen] [numeric](22, 4) NULL,
	[MaPhanLoaiPhiBH] [varchar](1) NULL,
	[MaTTPhiBH] [varchar](3) NULL,
	[PhiBaoHiem] [numeric](22, 4) NULL,
	[SoDangKyBH] [varchar](6) NULL,
	[ChiTietKhaiTriGia] [nvarchar](280) NULL,
	[TriGiaTinhThue] [numeric](24, 4) NULL,
	[PhanLoaiKhongQDVND] [varchar](1) NULL,
	[MaTTTriGiaTinhThue] [varchar](3) NULL,
	[TongHeSoPhanBoTG] [numeric](24, 4) NULL,
	[MaLyDoDeNghiBP] [varchar](1) NULL,
	[NguoiNopThue] [varchar](1) NULL,
	[MaNHTraThueThay] [varchar](11) NULL,
	[NamPhatHanhHM] [numeric](4, 0) NULL,
	[KyHieuCTHanMuc] [varchar](10) NULL,
	[SoCTHanMuc] [varchar](10) NULL,
	[MaXDThoiHanNopThue] [varchar](1) NULL,
	[MaNHBaoLanh] [varchar](11) NULL,
	[NamPhatHanhBL] [numeric](4, 0) NULL,
	[KyHieuCTBaoLanh] [varchar](10) NULL,
	[SoCTBaoLanh] [varchar](10) NULL,
	[NgayNhapKhoDau] [datetime] NULL,
	[NgayKhoiHanhVC] [datetime] NULL,
	[DiaDiemDichVC] [varchar](7) NULL,
	[NgayDen] [datetime] NULL,
	[GhiChu] [nvarchar](100) NULL,
	[SoQuanLyNoiBoDN] [varchar](20) NULL,
	[MaPhanLoaiKiemTra] [varchar](3) NULL,
	[MaSoThueDaiDien] [varchar](4) NULL,
	[TenCoQuanHaiQuan] [varchar](10) NULL,
	[NgayThayDoiDangKy] [datetime] NULL,
	[BieuThiTruongHopHetHan] [varchar](1) NULL,
	[TenDaiLyHaiQuan] [varchar](50) NULL,
	[MaNhanVienHaiQuan] [varchar](5) NULL,
	[TenDDLuuKho] [varchar](20) NULL,
	[MaPhanLoaiTongGiaCoBan] [varchar](1) NULL,
	[PhanLoaiCongThucChuan] [varchar](1) NULL,
	[MaPhanLoaiDieuChinhTriGia] [varchar](2) NULL,
	[PhuongPhapDieuChinhTriGia] [varchar](22) NULL,
	[TongTienThuePhaiNop] [numeric](15, 4) NULL,
	[SoTienBaoLanh] [numeric](15, 4) NULL,
	[TenTruongDonViHaiQuan] [nvarchar](36) NULL,
	[NgayCapPhep] [datetime] NULL,
	[PhanLoaiThamTraSauThongQuan] [varchar](2) NULL,
	[NgayPheDuyetBP] [datetime] NULL,
	[NgayHoanThanhKiemTraBP] [datetime] NULL,
	[SoNgayDoiCapPhepNhapKhau] [numeric](2, 0) NULL,
	[TieuDe] [varchar](27) NULL,
	[MaSacThueAnHan_VAT] [varchar](1) NULL,
	[TenSacThueAnHan_VAT] [nvarchar](9) NULL,
	[HanNopThueSauKhiAnHan_VAT] [datetime] NULL,
	[PhanLoaiNopThue] [varchar](1) NULL,
	[TongSoTienThueXuatKhau] [numeric](15, 4) NULL,
	[MaTTTongTienThueXuatKhau] [varchar](3) NULL,
	[TongSoTienLePhi] [numeric](15, 4) NULL,
	[MaTTCuaSoTienBaoLanh] [varchar](3) NULL,
	[SoQuanLyNguoiSuDung] [varchar](5) NULL,
	[NgayHoanThanhKiemTra] [datetime] NULL,
	[TongSoTrangCuaToKhai] [numeric](2, 0) NULL,
	[TongSoDongHangCuaToKhai] [numeric](2, 0) NULL,
	[NgayKhaiBaoNopThue] [datetime] NULL,
	[TrangThaiXuLy] [varchar](2) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]    Script Date: 11/11/2013 16:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TenCoQuanHaiQuan] [nvarchar](210) NULL,
	[TenChiCucHaiQuanNoiMoToKhai] [nvarchar](210) NULL,
	[SoChungTu] [numeric](12, 0) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[NgayDangKyToKhai] [datetime] NULL,
	[TenDonViXuatNhapKhau] [nvarchar](300) NULL,
	[MaDonViXuatNhapKhau] [varchar](13) NULL,
	[MaBuuChinh] [varchar](7) NULL,
	[DiaChiNguoiXuatNhapKhau] [nvarchar](300) NULL,
	[SoDienThoaiNguoiXuatNhapKhau] [varchar](20) NULL,
	[TenNganHangBaoLanh] [nvarchar](210) NULL,
	[MaNganHangBaoLanh] [varchar](11) NULL,
	[KiHieuChungTuBaoLanh] [varchar](10) NULL,
	[SoChungTuBaoLanh] [varchar](10) NULL,
	[LoaiBaoLanh] [nvarchar](60) NULL,
	[TenNganHangTraThay] [nvarchar](210) NULL,
	[MaNganHangTraThay] [varchar](11) NULL,
	[KiHieuChungTuPhatHanhHanMuc] [varchar](10) NULL,
	[SoHieuPhatHanhHanMuc] [varchar](10) NULL,
	[TongSoThueKhaiBao] [numeric](11, 0) NULL,
	[TongSoThueAnDinh] [numeric](11, 0) NULL,
	[TongSoThueChenhLech] [numeric](12, 0) NULL,
	[MaTienTe] [varchar](3) NULL,
	[TyGiaQuyDoi] [numeric](9, 0) NULL,
	[SoNgayDuocAnHan] [numeric](3, 0) NULL,
	[NgayHetHieuLucTamNhapTaiXuat] [datetime] NULL,
	[SoTaiKhoanKhoBac] [varchar](15) NULL,
	[TenKhoBac] [nvarchar](210) NULL,
	[LaiSuatPhatChamNop] [nvarchar](765) NULL,
	[NgayPhatHanhChungTu] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_ThongBaoAnDinhThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]    Script Date: 11/11/2013 16:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[SoToKhaiBoSung] [numeric](12, 0) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[NhomXuLyHoSo] [varchar](2) NULL,
	[NhomXuLyHoSoID] [int] NULL,
	[PhanLoaiXuatNhapKhau] [varchar](1) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[MaLoaiHinh] [varchar](3) NULL,
	[NgayKhaiBao] [datetime] NULL,
	[NgayCapPhep] [datetime] NULL,
	[ThoiHanTaiNhapTaiXuat] [datetime] NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[TenNguoiKhai] [nvarchar](300) NULL,
	[MaBuuChinh] [varchar](7) NULL,
	[DiaChiNguoiKhai] [nvarchar](300) NOT NULL,
	[SoDienThoaiNguoiKhai] [varchar](20) NULL,
	[MaLyDoKhaiBoSung] [varchar](1) NULL,
	[MaTienTeTienThue] [varchar](3) NULL,
	[MaNganHangTraThueThay] [varchar](110) NULL,
	[NamPhatHanhHanMuc] [numeric](4, 0) NULL,
	[KiHieuChungTuPhatHanhHanMuc] [varchar](10) NULL,
	[SoChungTuPhatHanhHanMuc] [varchar](10) NULL,
	[MaXacDinhThoiHanNopThue] [varchar](1) NULL,
	[MaNganHangBaoLanh] [varchar](11) NULL,
	[NamPhatHanhBaoLanh] [numeric](4, 0) NULL,
	[KyHieuPhatHanhChungTuBaoLanh] [varchar](10) NULL,
	[SoHieuPhatHanhChungTuBaoLanh] [varchar](10) NULL,
	[MaTienTeTruocKhiKhaiBoSung] [varchar](3) NULL,
	[TyGiaHoiDoaiTruocKhiKhaiBoSung] [numeric](9, 0) NULL,
	[MaTienTeSauKhiKhaiBoSung] [varchar](3) NULL,
	[TyGiaHoiDoaiSauKhiKhaiBoSung] [numeric](9, 0) NULL,
	[SoQuanLyTrongNoiBoDoanhNghiep] [nvarchar](20) NULL,
	[GhiChuNoiDungLienQuanTruocKhiKhaiBoSung] [nvarchar](300) NULL,
	[GhiChuNoiDungLienQuanSauKhiKhaiBoSung] [nvarchar](300) NULL,
	[SoTiepNhan] [numeric](12, 0) NULL,
	[NgayTiepNhan] [datetime] NULL,
	[PhanLuong] [nvarchar](300) NULL,
	[HuongDan] [nvarchar](1000) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[SoThongBao] [numeric](12, 0) NULL,
	[NgayHoanThanhKiemTra] [datetime] NULL,
	[GioHoanThanhKiemTra] [datetime] NULL,
	[NgayDangKyKhaiBoSung] [datetime] NULL,
	[GioDangKyKhaiBoSung] [datetime] NULL,
	[DauHieuBaoQua60Ngay] [varchar](1) NULL,
	[MaHetThoiHan] [varchar](1) NULL,
	[MaDaiLyHaiQuan] [varchar](5) NULL,
	[TenDaiLyHaiQuan] [nvarchar](50) NULL,
	[MaNhanVienHaiQuan] [varchar](5) NULL,
	[PhanLoaiNopThue] [varchar](1) NULL,
	[NgayHieuLucChungTu] [datetime] NULL,
	[SoNgayNopThue] [numeric](3, 0) NULL,
	[SoNgayNopThueDanhChoVATHangHoaDacBiet] [numeric](3, 0) NULL,
	[HienThiTongSoTienTangGiamThueXuatNhapKhau] [varchar](1) NULL,
	[TongSoTienTangGiamThueXuatNhapKhau] [numeric](11, 0) NULL,
	[MaTienTeTongSoTienTangGiamThueXuatNhapKhau] [varchar](3) NULL,
	[TongSoTrangToKhaiBoSung] [numeric](2, 0) NULL,
	[TongSoDongHangToKhaiBoSung] [numeric](2, 0) NULL,
	[LyDo] [nvarchar](600) NULL,
	[TenNguoiPhuTrach] [nvarchar](108) NULL,
	[TenTruongDonViHaiQuan] [nvarchar](108) NULL,
	[NgayDangKyDuLieu] [datetime] NULL,
	[GioDangKyDuLieu] [datetime] NULL,
	[TrangThaiXuLy] [int] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_BoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]    Script Date: 11/11/2013 16:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TenCoQuanHaiQuan] [nvarchar](210) NULL,
	[TenChiCucHaiQuan] [nvarchar](210) NULL,
	[SoChungTu] [numeric](12, 0) NULL,
	[TenDonViXuatNhapKhau] [nvarchar](300) NULL,
	[MaDonViXuatNhapKhau] [varchar](13) NULL,
	[MaBuuChinh] [varchar](7) NULL,
	[DiaChiNguoiXuatNhapKhau] [nvarchar](300) NULL,
	[SoDienThoaiNguoiXuatNhapKhau] [varchar](20) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[NgayDangKyToKhai] [datetime] NULL,
	[MaPhanLoaiToKhai] [varchar](3) NULL,
	[TenNganHangTraThay] [nvarchar](210) NULL,
	[MaNganHangTraThay] [varchar](11) NULL,
	[KiHieuChungTuPhatHanhHanMuc] [varchar](10) NULL,
	[SoHieuPhatHanhHanMuc] [varchar](10) NULL,
	[TongSoPhiPhaiNop] [numeric](12, 0) NULL,
	[SoTaiKhoanKhoBac] [varchar](15) NULL,
	[TenKhoBac] [nvarchar](210) NULL,
	[NgayPhatHanhChungTu] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_LePhi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]    Script Date: 11/11/2013 16:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_PhanHoi](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[PhanLoaiBaoCaoSuaDoi] [varchar](1) NULL,
	[MaPhanLoaiKiemTra] [varchar](3) NULL,
	[MaSoThueDaiDien] [varchar](4) NULL,
	[TenCoQuanHaiQuan] [varchar](10) NULL,
	[NgayDangKy] [datetime] NULL,
	[NgayThayDoiDangKy] [datetime] NULL,
	[BieuThiTruongHopHetHan] [varchar](1) NULL,
	[TenDaiLyHaiQuan] [varchar](50) NULL,
	[MaNhanVienHaiQuan] [varchar](5) NULL,
	[TenDDLuuKho] [varchar](20) NULL,
	[MaPhanLoaiTongGiaCoBan] [varchar](1) NULL,
	[PhanLoaiCongThucChuan] [varchar](1) NULL,
	[MaPhanLoaiDieuChinhTriGia] [varchar](2) NULL,
	[PhuongPhapDieuChinhTriGia] [varchar](22) NULL,
	[TongTienThuePhaiNop] [numeric](11, 0) NULL,
	[SoTienBaoLanh] [numeric](11, 0) NULL,
	[TenTruongDonViHaiQuan] [nvarchar](36) NULL,
	[NgayCapPhep] [datetime] NULL,
	[PhanLoaiThamTraSauThongQuan] [varchar](2) NULL,
	[NgayPheDuyetBP] [datetime] NULL,
	[NgayHoanThanhKiemTraBP] [datetime] NULL,
	[SoNgayDoiCapPhepNhapKhau] [numeric](2, 0) NULL,
	[TieuDe] [varchar](27) NULL,
	[MaSacThueAnHan_VAT] [varchar](1) NULL,
	[TenSacThueAnHan_VAT] [nvarchar](9) NULL,
	[HanNopThueSauKhiAnHan_VAT] [datetime] NULL,
	[PhanLoaiNopThue] [varchar](1) NULL,
	[TongSoTienThueXuatKhau] [numeric](11, 0) NULL,
	[MaTTTongTienThueXuatKhau] [varchar](3) NULL,
	[TongSoTienLePhi] [numeric](11, 0) NULL,
	[MaTTCuaSoTienBaoLanh] [varchar](3) NULL,
	[SoQuanLyNguoiSuDung] [varchar](5) NULL,
	[NgayHoanThanhKiemTra] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_PhanHoi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]    Script Date: 11/11/2013 16:32:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TenCoQuanHaiQuan] [nvarchar](210) NULL,
	[TenChiCucHaiQuanNoiMoToKhai] [nvarchar](210) NULL,
	[SoChungTu] [numeric](12, 0) NULL,
	[TenDonViXuatNhapKhau] [nvarchar](300) NULL,
	[MaDonViXuatNhapKhau] [varchar](13) NULL,
	[MaBuuChinh] [varchar](7) NULL,
	[DiaChiNguoiXuatNhapKhau] [nvarchar](300) NULL,
	[SoDienThoaiNguoiXuatNhapKhau] [varchar](20) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[NgayDangKyToKhai] [datetime] NULL,
	[MaPhanLoaiToKhai] [varchar](3) NULL,
	[TenNganHangBaoLanh] [nvarchar](210) NULL,
	[MaNganHangBaoLanh] [varchar](11) NULL,
	[KiHieuChungTuBaoLanh] [varchar](10) NULL,
	[SoChungTuBaoLanh] [varchar](10) NULL,
	[LoaiBaoLanh] [nvarchar](60) NULL,
	[TenNganHangTraThay] [nvarchar](210) NULL,
	[MaNganHangTraThay] [varchar](11) NULL,
	[KiHieuChungTuPhatHanhHanMuc] [varchar](10) NULL,
	[SoHieuPhatHanhHanMuc] [varchar](10) NULL,
	[TongSoTienThue] [numeric](12, 0) NULL,
	[TongSoTienMien] [numeric](11, 0) NULL,
	[TongSoTienGiam] [numeric](11, 0) NULL,
	[TongSoThuePhaiNop] [numeric](12, 0) NULL,
	[MaTienTe] [varchar](3) NULL,
	[TyGiaQuyDoi] [numeric](9, 0) NULL,
	[SoNgayDuocAnHan] [numeric](3, 0) NULL,
	[NgayHetHieuLucTamNhapTaiXuat] [datetime] NULL,
	[SoTaiKhoanKhoBac] [varchar](15) NULL,
	[TenKhoBac] [nvarchar](210) NULL,
	[LaiSuatPhatChamNop] [nvarchar](765) NULL,
	[NgayPhatHanhChungTu] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_ThongBaoThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO




/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]    Script Date: 11/11/2013 16:37:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung_TangGiamThueThuKhac](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMDBoSung_ID] [bigint] NULL,
	[SoDong] [varchar](2) NULL,
	[MaKhoanMucTiepNhanTongSoTienTangGiamThuKhac] [varchar](1) NULL,
	[TenKhoanMucTiepNhanTongSoTienTangGiamThuKhac] [nvarchar](9) NULL,
	[HienThiTongSoTienTangGiamThuKhac] [varchar](1) NULL,
	[TongSoTienTangGiamThuKhac] [numeric](11, 0) NULL,
	[MaTienTeTongSoTienTangGiamThuKhac] [varchar](3) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_ThongBaoThue_SacThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO




IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]'))
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] DROP CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]'))
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] DROP CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]'))
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] DROP CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]    Script Date: 11/11/2013 16:39:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AnDinhThue_ID] [bigint] NULL,
	[TenSacThue] [nvarchar](27) NULL,
	[TieuMuc] [numeric](4, 0) NULL,
	[SoThueKhaiBao] [numeric](11, 0) NULL,
	[SoThueAnDinh] [numeric](11, 0) NULL,
	[SoThueChenhLech] [numeric](12, 0) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_ThongBaoAnDinhThueSacThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]    Script Date: 11/11/2013 16:39:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LePhi_ID] [bigint] NULL,
	[TenSacPhi] [nvarchar](210) NULL,
	[SoPhiPhaiNop] [numeric](12, 0) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]    Script Date: 11/11/2013 16:39:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ThuePhaiThu_ID] [bigint] NULL,
	[TenSacThue] [nvarchar](27) NULL,
	[TieuMuc] [numeric](4, 0) NULL,
	[TienThue] [numeric](12, 0) NULL,
	[SoTienMienThue] [numeric](11, 0) NULL,
	[SoTienGiamThue] [numeric](11, 0) NULL,
	[SoThuePhaiNop] [numeric](12, 0) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] FOREIGN KEY([AnDinhThue_ID])
REFERENCES [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThue] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] CHECK CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_t_KDT_VNACC_ToKhaiMauDich_AnDinhThue]
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] FOREIGN KEY([LePhi_ID])
REFERENCES [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet] CHECK CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] FOREIGN KEY([ThuePhaiThu_ID])
REFERENCES [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue] CHECK CONSTRAINT [FK_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThuSacThue_t_KDT_VNACC_ToKhaiMauDich_ThuePhaiThu]
GO




/****** Object:  Table [dbo].[t_KDT_VNACC_ChiThiHaiQuan]    Script Date: 11/11/2013 16:44:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChiThiHaiQuan]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ChiThiHaiQuan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoaiThongTin] [varchar](10) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[PhanLoai] [varchar](1) NULL,
	[Ngay] [datetime] NULL,
	[Ten] [varchar](500) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ChiThiHaiQuan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('t_KDT_VNACC_ChiThiHaiQuan') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'LoaiThongTin' AND [object_id] = OBJECT_ID('t_KDT_VNACC_ChiThiHaiQuan')))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GP: Giay phep, TK: To khai, HDon: Hoa don, HDong: Hop dong, v.v...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_KDT_VNACC_ChiThiHaiQuan', @level2type=N'COLUMN',@level2name=N'LoaiThongTin'
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ChungTuDinhKem]    Script Date: 11/11/2013 16:44:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChungTuDinhKem]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ChungTuDinhKem](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoaiChungTu] [varchar](3) NOT NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[TrangThaiXuLy] [varchar](50) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[NhomXuLyHoSo] [varchar](2) NULL,
	[TieuDe] [nvarchar](210) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[GhiChu] [nvarchar](996) NULL,
	[PhanLoaiThuTucKhaiBao] [varchar](3) NULL,
	[SoDienThoaiNguoiKhaiBao] [varchar](20) NULL,
	[SoQuanLyTrongNoiBoDoanhNghiep] [varchar](20) NULL,
	[MaKetQuaXuLy] [varchar](75) NULL,
	[TenThuTucKhaiBao] [nvarchar](210) NULL,
	[NgayKhaiBao] [datetime] NULL,
	[TrangThaiKhaiBao] [varchar](30) NULL,
	[NgaySuaCuoiCung] [datetime] NULL,
	[TenNguoiKhaiBao] [nvarchar](300) NULL,
	[DiaChiNguoiKhaiBao] [nvarchar](300) NULL,
	[SoDeLayTepDinhKem] [numeric](16, 0) NULL,
	[NgayHoanThanhKiemTraHoSo] [datetime] NULL,
	[SoTiepNhan] [numeric](11, 0) NULL,
	[NgayTiepNhan] [datetime] NULL,
	[TongDungLuong] [numeric](18, 0) NULL,
	[PhanLuong] [nvarchar](50) NULL,
	[HuongDan] [varchar](4000) NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayCapNhatCuoiCung] [datetime] NULL,
	[CanCuPhapLenh] [nvarchar](996) NULL,
	[CoBaoXoa] [varchar](1) NULL,
	[GhiChuHaiQuan] [nvarchar](996) NULL,
	[HinhThucTimKiem] [varchar](1) NULL,
	[LyDoChinhSua] [nvarchar](300) NULL,
	[MaDiaDiemDen] [varchar](6) NULL,
	[MaNhaVanChuyen] [varchar](6) NULL,
	[MaPhanLoaiDangKy] [varchar](1) NULL,
	[MaPhanLoaiXuLy] [varchar](1) NULL,
	[MaThongTinXuat] [varchar](7) NULL,
	[NgayCapPhep] [datetime] NULL,
	[NgayThongBao] [datetime] NULL,
	[NguoiGui] [varchar](5) NULL,
	[NoiDungChinhSua] [nvarchar](300) NULL,
	[PhuongTienVanChuyen] [varchar](12) NULL,
	[SoChuyenDiBien] [varchar](10) NULL,
	[SoSeri] [varchar](3) NULL,
	[SoTiepNhanToKhaiPhoThong] [numeric](11, 0) NULL,
	[TrangThaiXuLyHaiQuan] [varchar](1) NULL,
	[NguoiCapNhatCuoiCung] [varchar](8) NULL,
	[NhomXuLyHoSoID] [int] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ChungTuDinhKem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('t_KDT_VNACC_ChungTuDinhKem') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'LoaiChungTu' AND [object_id] = OBJECT_ID('t_KDT_VNACC_ChungTuDinhKem')))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'MSB, HYS, HYE, IHY' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_KDT_VNACC_ChungTuDinhKem', @level2type=N'COLUMN',@level2name=N'LoaiChungTu'
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_ChungTuDinhKem_ChiTiet_t_KDT_VNACC_ChungTuDinhKem]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]'))
ALTER TABLE [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet] DROP CONSTRAINT [FK_t_KDT_VNACC_ChungTuDinhKem_ChiTiet_t_KDT_VNACC_ChungTuDinhKem]
GO

/****** Object:  Table [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]    Script Date: 11/11/2013 16:59:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChungTuKemID] [bigint] NULL,
	[FileName] [nvarchar](255) NULL,
	[FileSize] [numeric](18, 0) NULL,
	[NoiDung] [image] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ChungTuDinhKem_ChiTiet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
end
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_ChungTuDinhKem_ChiTiet_t_KDT_VNACC_ChungTuDinhKem] FOREIGN KEY([ChungTuKemID])
REFERENCES [dbo].[t_KDT_VNACC_ChungTuDinhKem] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ChungTuDinhKem_ChiTiet] CHECK CONSTRAINT [FK_t_KDT_VNACC_ChungTuDinhKem_ChiTiet_t_KDT_VNACC_ChungTuDinhKem]
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_ChungTuThanhToan_ChiTiet_t_KDT_VNACC_ChungTuThanhToan]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]'))
ALTER TABLE [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] DROP CONSTRAINT [FK_t_KDT_VNACC_ChungTuThanhToan_ChiTiet_t_KDT_VNACC_ChungTuThanhToan]
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ChungTuThanhToan]    Script Date: 11/11/2013 17:00:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChungTuThanhToan]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ChungTuThanhToan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoaiChungTu] [varchar](3) NOT NULL,
	[LoaiHinhBaoLanh] [varchar](1) NULL,
	[MaNganHangCungCapBaoLanh] [varchar](11) NULL,
	[NamPhatHanh] [numeric](4, 0) NULL,
	[KiHieuChungTuPhatHanhBaoLanh] [varchar](10) NULL,
	[SoChungTuPhatHanhBaoLanh] [varchar](10) NULL,
	[MaTienTe] [varchar](3) NULL,
	[MaNganHangTraThay] [varchar](11) NULL,
	[TenNganHangTraThay] [nvarchar](300) NULL,
	[MaDonViSuDungHanMuc] [varchar](13) NULL,
	[TenDonViSuDungHanMuc] [nvarchar](300) NULL,
	[KiHieuChungTuPhatHanhHanMuc] [varchar](10) NULL,
	[SoHieuPhatHanhHanMuc] [varchar](10) NULL,
	[MaKetQuaXuLy] [varchar](75) NULL,
	[TenNganHangCungCapBaoLanh] [nvarchar](210) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[NgayDuDinhKhaiBao] [datetime] NULL,
	[MaLoaiHinh] [varchar](3) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[TenCucHaiQuan] [nvarchar](210) NULL,
	[SoVanDon_1] [varchar](35) NULL,
	[SoVanDon_2] [varchar](35) NULL,
	[SoVanDon_3] [varchar](35) NULL,
	[SoVanDon_4] [varchar](35) NULL,
	[SoVanDon_5] [varchar](35) NULL,
	[SoHoaDon] [varchar](35) NULL,
	[MaDonViSuDungBaoLanh] [varchar](13) NULL,
	[TenDonViSuDungBaoLanh] [nvarchar](300) NULL,
	[MaDonViDaiDienSuDungBaoLanh] [varchar](13) NULL,
	[TenDonViDaiDienSuDungBaoLanh] [nvarchar](300) NULL,
	[NgayBatDauHieuLuc] [datetime] NULL,
	[NgayHetHieuLuc] [datetime] NULL,
	[SoTienDangKyBaoLanh] [numeric](13, 0) NULL,
	[CoBaoVoHieu] [varchar](1) NULL,
	[SoDuBaoLanh] [numeric](13, 0) NULL,
	[SoTienHanMucDangKi] [numeric](13, 0) NULL,
	[SoDuHanMuc] [numeric](13, 0) NULL,
	[SoThuTuHanMuc] [varchar](10) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[TrangThaiXuLy] [varchar](50) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ChungTuThanhToan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('t_KDT_VNACC_ChungTuThanhToan') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'LoaiChungTu' AND [object_id] = OBJECT_ID('t_KDT_VNACC_ChungTuThanhToan')))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'IAS, IBA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_KDT_VNACC_ChungTuThanhToan', @level2type=N'COLUMN',@level2name=N'LoaiChungTu'
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]    Script Date: 11/11/2013 17:00:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ChungTuThanhToan_ID] [bigint] NULL,
	[SoThuTuGiaoDich] [numeric](4, 0) NULL,
	[NgayTaoDuLieu] [datetime] NULL,
	[ThoiGianTaoDuLieu] [datetime] NULL,
	[SoTienTruLui] [numeric](13, 0) NULL,
	[SoTienTangLen] [numeric](13, 0) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[NgayDangKyToKhai] [datetime] NULL,
	[MaLoaiHinh] [varchar](3) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[TenCucHaiQuan] [nvarchar](210) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ChungTuThanhToan_ChiTiet] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_ChungTuThanhToan_ChiTiet_t_KDT_VNACC_ChungTuThanhToan] FOREIGN KEY([ChungTuThanhToan_ID])
REFERENCES [dbo].[t_KDT_VNACC_ChungTuThanhToan] ([ID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_ChungTuThanhToan_ChiTiet] CHECK CONSTRAINT [FK_t_KDT_VNACC_ChungTuThanhToan_ChiTiet_t_KDT_VNACC_ChungTuThanhToan]
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_TK_Container]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_Container]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_Container](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[MaDiaDiem1] [varchar](7) NULL,
	[MaDiaDiem2] [varchar](7) NULL,
	[MaDiaDiem3] [varchar](7) NULL,
	[MaDiaDiem4] [varchar](7) NULL,
	[MaDiaDiem5] [varchar](7) NULL,
	[TenDiaDiem] [nvarchar](70) NULL,
	[DiaChiDiaDiem] [nvarchar](100) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_Container] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_TK_DinhKemDienTu]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_DinhKemDienTu]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_DinhKemDienTu](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[SoTT] [int] NULL,
	[PhanLoai] [varchar](3) NULL,
	[SoDinhKemKhaiBaoDT] [numeric](12, 0) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_DinhKemDienTu] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_TK_GiayPhep]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_GiayPhep]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_GiayPhep](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[SoTT] [int] NULL,
	[PhanLoai] [varchar](4) NULL,
	[SoGiayPhep] [varchar](20) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_GiayPhep] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[SoTT] [int] NULL,
	[MaTenDieuChinh] [varchar](1) NULL,
	[MaPhanLoaiDieuChinh] [varchar](3) NULL,
	[MaTTDieuChinhTriGia] [varchar](3) NULL,
	[TriGiaKhoanDieuChinh] [numeric](24, 4) NULL,
	[TongHeSoPhanBo] [numeric](24, 4) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_KhoanDieuChinh] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_PhanHoi_AnHan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[MaSacThueAnHan] [varchar](1) NULL,
	[TenSacThueAnHan] [nvarchar](9) NULL,
	[HanNopThueSauKhiAnHan] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_AnHan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_PhanHoi_SacThue](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[MaSacThue] [varchar](1) NULL,
	[TenSacThue] [nvarchar](50) NULL,
	[TongTienThue] [numeric](15, 4) NULL,
	[SoDongTongTienThue] [numeric](2, 0) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_SacThue] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[MaTTTyGiaTinhThue] [varchar](3) NULL,
	[TyGiaTinhThue] [numeric](13, 4) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiMauDich_TyGia] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_TK_SoContainer]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_SoContainer]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_SoContainer](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NOT NULL,
	[SoTT] [int] NULL,
	[SoContainer] [varchar](12) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_SoContainer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_TK_SoVanDon]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_SoVanDon]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_SoVanDon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NULL,
	[SoTT] [int] NULL,
	[SoVanDon] [varchar](35) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_SoVanDon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_TK_TrungChuyen]    Script Date: 11/11/2013 17:02:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_TK_TrungChuyen]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_TK_TrungChuyen](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[SoTT] [int] NULL,
	[MaDiaDiem] [varchar](7) NULL,
	[NgayDen] [datetime] NULL,
	[NgayKhoiHanh] [datetime] NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TK_TrungChuyen] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangMauDich]    Script Date: 11/11/2013 17:15:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NOT NULL,
	[MaSoHang] [varchar](12) NOT NULL,
	[MaQuanLy] [varchar](7) NULL,
	[TenHang] [nvarchar](200) NOT NULL,
	[ThueSuat] [numeric](10, 0) NULL,
	[ThueSuatTuyetDoi] [numeric](14, 4) NULL,
	[MaDVTTuyetDoi] [varchar](4) NULL,
	[MaTTTuyetDoi] [varchar](4) NULL,
	[NuocXuatXu] [varchar](2) NULL,
	[SoLuong1] [numeric](12, 0) NOT NULL,
	[DVTLuong1] [varchar](4) NOT NULL,
	[SoLuong2] [numeric](12, 0) NULL,
	[DVTLuong2] [varchar](4) NULL,
	[TriGiaHoaDon] [numeric](24, 4) NULL,
	[DonGiaHoaDon] [numeric](13, 4) NULL,
	[MaTTDonGia] [varchar](4) NULL,
	[DVTDonGia] [varchar](3) NULL,
	[MaBieuThueNK] [varchar](3) NULL,
	[MaHanNgach] [varchar](1) NULL,
	[MaThueNKTheoLuong] [varchar](10) NULL,
	[MaMienGiamThue] [varchar](5) NULL,
	[SoTienGiamThue] [numeric](20, 4) NULL,
	[TriGiaTinhThue] [numeric](24, 4) NULL,
	[MaTTTriGiaTinhThue] [varchar](3) NULL,
	[SoMucKhaiKhoanDC] [varchar](5) NULL,
	[SoTTDongHangTKTNTX] [varchar](2) NULL,
	[SoDMMienThue] [numeric](12, 0) NULL,
	[SoDongDMMienThue] [varchar](3) NULL,
	[MaMienGiam] [varchar](5) NULL,
	[SoTienMienGiam] [numeric](20, 4) NULL,
	[MaTTSoTienMienGiam] [varchar](3) NULL,
	[MaVanBanPhapQuyKhac1] [varchar](2) NULL,
	[MaVanBanPhapQuyKhac2] [varchar](2) NULL,
	[MaVanBanPhapQuyKhac3] [varchar](2) NULL,
	[MaVanBanPhapQuyKhac4] [varchar](2) NULL,
	[MaVanBanPhapQuyKhac5] [varchar](2) NULL,
	[SoDong] [varchar](2) NULL,
	[MaPhanLoaiTaiXacNhanGia] [varchar](1) NULL,
	[TenNoiXuatXu] [varchar](7) NULL,
	[SoLuongTinhThue] [numeric](16, 4) NULL,
	[MaDVTDanhThue] [varchar](4) NULL,
	[DonGiaTinhThue] [numeric](22, 4) NULL,
	[DV_SL_TrongDonGiaTinhThue] [varchar](4) NULL,
	[MaTTDonGiaTinhThue] [varchar](3) NULL,
	[TriGiaTinhThueS] [numeric](21, 4) NULL,
	[MaTTTriGiaTinhThueS] [varchar](3) NULL,
	[MaTTSoTienMienGiam1] [varchar](3) NULL,
	[MaPhanLoaiThueSuatThue] [varchar](1) NULL,
	[ThueSuatThue] [varchar](30) NULL,
	[PhanLoaiThueSuatThue] [varchar](1) NULL,
	[SoTienThue] [numeric](20, 4) NULL,
	[MaTTSoTienThueXuatKhau] [varchar](3) NULL,
	[TienLePhi_DonGia] [varchar](21) NULL,
	[TienBaoHiem_DonGia] [varchar](21) NULL,
	[TienLePhi_SoLuong] [numeric](16, 4) NULL,
	[TienLePhi_MaDVSoLuong] [varchar](4) NULL,
	[TienBaoHiem_SoLuong] [numeric](16, 4) NULL,
	[TienBaoHiem_MaDVSoLuong] [varchar](4) NULL,
	[TienLePhi_KhoanTien] [numeric](20, 4) NULL,
	[TienBaoHiem_KhoanTien] [numeric](20, 4) NULL,
	[DieuKhoanMienGiam] [varchar](60) NULL,
 CONSTRAINT [PK_T_KDT_VNACC_HangMauDich] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_HangMauDich_BoSung_t_KDT_VNACC_ToKhaiMauDich_BoSung]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]'))
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] DROP CONSTRAINT [FK_t_KDT_VNACC_HangMauDich_BoSung_t_KDT_VNACC_ToKhaiMauDich_BoSung]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_HangMauDich_BoSung_ThueThuKhac_t_KDT_VNACC_HangMauDich_BoSung_Thue]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]'))
ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] DROP CONSTRAINT [FK_t_KDT_VNACC_HangMauDich_BoSung_ThueThuKhac_t_KDT_VNACC_HangMauDich_BoSung_Thue]
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]    Script Date: 11/11/2013 17:16:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMDBoSung_ID] [bigint] NULL,
	[SoDong] [varchar](2) NULL,
	[SoThuTuDongHangTrenToKhaiGoc] [varchar](2) NULL,
	[MoTaHangHoaTruocKhiKhaiBoSung] [nvarchar](600) NULL,
	[MoTaHangHoaSauKhiKhaiBoSung] [nvarchar](600) NULL,
	[MaSoHangHoaTruocKhiKhaiBoSung] [varchar](12) NULL,
	[MaSoHangHoaSauKhiKhaiBoSung] [varchar](12) NULL,
	[MaNuocXuatXuTruocKhiKhaiBoSung] [varchar](2) NULL,
	[MaNuocXuatXuSauKhiKhaiBoSung] [varchar](2) NULL,
	[TriGiaTinhThueTruocKhiKhaiBoSung] [numeric](17, 0) NULL,
	[TriGiaTinhThueSauKhiKhaiBoSung] [numeric](17, 0) NULL,
	[SoLuongTinhThueTruocKhiKhaiBoSung] [numeric](12, 0) NULL,
	[SoLuongTinhThueSauKhiKhaiBoSung] [numeric](12, 0) NULL,
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] [varchar](4) NULL,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] [varchar](50) NULL,
	[ThueSuatTruocKhiKhaiBoSung] [varchar](13) NULL,
	[ThueSuatSauKhiKhaiBoSung] [varchar](30) NULL,
	[SoTienThueTruocKhiKhaiBoSung] [numeric](16, 0) NULL,
	[SoTienThueSauKhiKhaiBoSung] [numeric](16, 0) NULL,
	[HienThiMienThueTruocKhiKhaiBoSung] [varchar](1) NULL,
	[HienThiMienThueSauKhiKhaiBoSung] [varchar](1) NULL,
	[HienThiSoTienTangGiamThueXuatNhapKhau] [varchar](1) NULL,
	[SoTienTangGiamThue] [numeric](16, 0) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HangMauDich_BoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]    Script Date: 11/11/2013 17:16:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HMDBoSung_ID] [bigint] NULL,
	[SoDong] [varchar](2) NULL,
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] [numeric](17, 0) NULL,
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] [numeric](17, 0) NULL,
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] [numeric](12, 0) NULL,
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] [numeric](12, 0) NULL,
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] [varchar](4) NULL,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] [varchar](4) NULL,
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] [varchar](10) NULL,
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] [varchar](10) NULL,
	[ThueSuatTruocKhiKhaiBoSungThuKhac] [varchar](25) NULL,
	[ThueSuatSauKhiKhaiBoSungThuKhac] [varchar](25) NULL,
	[SoTienThueTruocKhiKhaiBoSungThuKhac] [numeric](16, 0) NULL,
	[SoTienThueSauKhiKhaiBoSungThuKhac] [numeric](16, 0) NULL,
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] [varchar](1) NULL,
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] [varchar](1) NULL,
	[HienThiSoTienTangGiamThueVaThuKhac] [varchar](1) NULL,
	[SoTienTangGiamThuKhac] [numeric](16, 0) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HangMauDich_BoSung_ThueThuKhac] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]    Script Date: 11/11/2013 17:16:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[SoDong] [varchar](2) NULL,
	[MaPhanLoaiTaiXacNhanGia] [varchar](1) NULL,
	[TenNoiXuatXu] [varchar](7) NULL,
	[SoLuongTinhThue] [numeric](12, 0) NULL,
	[MaDVTDanhThue] [varchar](4) NULL,
	[DonGiaTinhThue] [numeric](18, 4) NULL,
	[DV_SL_TrongDonGiaTinhThue] [varchar](4) NULL,
	[MaTTDonGiaTinhThue] [varchar](3) NULL,
	[TriGiaTinhThueS] [numeric](17, 4) NULL,
	[MaTTTriGiaTinhThueS] [varchar](3) NULL,
	[MaTTSoTienMienGiam] [varchar](3) NULL,
	[MaPhanLoaiThueSuatThue] [varchar](1) NULL,
	[ThueSuatThue] [varchar](30) NULL,
	[PhanLoaiThueSuatThue] [varchar](1) NULL,
	[SoTienThue] [numeric](16, 0) NULL,
	[MaTTSoTienThueXuatKhau] [varchar](3) NULL,
	[TienLePhi_DonGia] [varchar](21) NULL,
	[TienBaoHiem_DonGia] [varchar](21) NULL,
	[TienLePhi_SoLuong] [numeric](12, 0) NULL,
	[TienLePhi_MaDVSoLuong] [varchar](4) NULL,
	[TienBaoHiem_SoLuong] [numeric](12, 0) NULL,
	[TienBaoHiem_MaDVSoLuong] [varchar](4) NULL,
	[TienLePhi_KhoanTien] [numeric](16, 4) NULL,
	[TienBaoHiem_KhoanTien] [numeric](16, 4) NULL,
	[DieuKhoanMienGiam] [varchar](60) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HangMauDich_PhanHoi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]    Script Date: 11/11/2013 17:16:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangMauDich_ThueThuKhac](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NOT NULL,
	[MaTSThueThuKhac] [varchar](10) NULL,
	[MaMGThueThuKhac] [varchar](5) NULL,
	[SoTienGiamThueThuKhac] [numeric](16, 4) NULL,
	[TenKhoanMucThueVaThuKhac] [nvarchar](9) NULL,
	[TriGiaTinhThueVaThuKhac] [numeric](17, 4) NULL,
	[SoLuongTinhThueVaThuKhac] [numeric](14, 0) NULL,
	[MaDVTDanhThueVaThuKhac] [varchar](4) NULL,
	[ThueSuatThueVaThuKhac] [varchar](24) NULL,
	[SoTienThueVaThuKhac] [numeric](16, 4) NULL,
	[DieuKhoanMienGiamThueVaThuKhac] [varchar](60) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HMD_ThueThuKhac] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_HangMauDich_BoSung_t_KDT_VNACC_ToKhaiMauDich_BoSung] FOREIGN KEY([TKMDBoSung_ID])
REFERENCES [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] CHECK CONSTRAINT [FK_t_KDT_VNACC_HangMauDich_BoSung_t_KDT_VNACC_ToKhaiMauDich_BoSung]
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_HangMauDich_BoSung_ThueThuKhac_t_KDT_VNACC_HangMauDich_BoSung_Thue] FOREIGN KEY([HMDBoSung_ID])
REFERENCES [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] CHECK CONSTRAINT [FK_t_KDT_VNACC_HangMauDich_BoSung_ThueThuKhac_t_KDT_VNACC_HangMauDich_BoSung_Thue]
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_HangHoaDon]    Script Date: 11/11/2013 17:18:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangHoaDon]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangHoaDon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HoaDon_ID] [bigint] NOT NULL,
	[MaHang] [varchar](40) NULL,
	[MaSoHang] [varchar](12) NULL,
	[TenHang] [nvarchar](200) NULL,
	[MaNuocXX] [varchar](2) NULL,
	[TenNuocXX] [varchar](30) NULL,
	[SoKienHang] [varchar](35) NULL,
	[SoLuong1] [numeric](12, 0) NULL,
	[MaDVT_SoLuong1] [varchar](4) NULL,
	[SoLuong2] [numeric](12, 0) NULL,
	[MaDVT_SoLuong2] [varchar](4) NULL,
	[DonGiaHoaDon] [numeric](13, 4) NULL,
	[MaTT_DonGia] [varchar](3) NULL,
	[MaDVT_DonGia] [varchar](3) NULL,
	[TriGiaHoaDon] [numeric](24, 4) NULL,
	[MaTT_GiaTien] [varchar](3) NULL,
	[LoaiKhauTru] [varchar](20) NULL,
	[SoTienKhauTru] [numeric](13, 4) NULL,
	[MaTT_TienKhauTru] [varchar](50) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HoaDon_Hang] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HoaDon]    Script Date: 11/11/2013 17:18:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HoaDon]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HoaDon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [numeric](12, 0) NULL,
	[NgayTiepNhan] [datetime] NULL,
	[PhanLoaiXuatNhap] [varchar](1) NULL,
	[MaDaiLyHQ] [varchar](5) NULL,
	[SoHoaDon] [varchar](35) NULL,
	[NgayLapHoaDon] [datetime] NULL,
	[DiaDiemLapHoaDon] [nvarchar](35) NULL,
	[PhuongThucThanhToan] [nvarchar](30) NULL,
	[PhanLoaiLienQuan] [varchar](6) NULL,
	[MaNguoiXNK] [varchar](13) NULL,
	[TenNguoiXNK] [nvarchar](100) NULL,
	[MaBuuChinh_NguoiXNK] [varchar](7) NULL,
	[DiaChi_NguoiXNK] [nvarchar](100) NULL,
	[SoDienThoai_NguoiXNK] [varchar](20) NULL,
	[NguoiLapHoaDon] [nvarchar](60) NULL,
	[MaNguoiGuiNhan] [varchar](13) NULL,
	[TenNguoiGuiNhan] [nvarchar](70) NULL,
	[MaBuuChinhNguoiGuiNhan] [varchar](9) NULL,
	[DiaChiNguoiNhanGui1] [nvarchar](35) NULL,
	[DiaChiNguoiNhanGui2] [nvarchar](35) NULL,
	[DiaChiNguoiNhanGui3] [nvarchar](35) NULL,
	[DiaChiNguoiNhanGui4] [nvarchar](35) NULL,
	[SoDienThoaiNhanGui] [varchar](12) NULL,
	[NuocSoTaiNhanGui] [varchar](2) NULL,
	[MaKyHieu] [nvarchar](140) NULL,
	[PhanLoaiVanChuyen] [varchar](4) NULL,
	[TenPTVC] [nvarchar](35) NULL,
	[SoHieuChuyenDi] [varchar](10) NULL,
	[MaDiaDiemXepHang] [varchar](5) NULL,
	[TenDiaDiemXepHang] [nvarchar](20) NULL,
	[ThoiKyXephang] [datetime] NULL,
	[MaDiaDiemDoHang] [varchar](5) NULL,
	[TenDiaDiemDoHang] [nvarchar](20) NULL,
	[MaDiaDiemTrungChuyen] [nvarchar](5) NULL,
	[TenDiaDiemTrungChuyen] [nvarchar](30) NULL,
	[TrongLuongGross] [numeric](14, 4) NULL,
	[MaDVT_TrongLuongGross] [varchar](3) NULL,
	[TrongLuongThuan] [numeric](14, 4) NULL,
	[MaDVT_TrongLuongThuan] [nvarchar](3) NULL,
	[TongTheTich] [numeric](14, 4) NULL,
	[MaDVT_TheTich] [nvarchar](3) NULL,
	[TongSoKienHang] [numeric](8, 0) NULL,
	[MaDVT_KienHang] [varchar](3) NULL,
	[GhiChuChuHang] [nvarchar](70) NULL,
	[SoPL] [varchar](10) NULL,
	[NganHangLC] [nvarchar](80) NULL,
	[TriGiaFOB] [numeric](24, 4) NULL,
	[MaTT_FOB] [varchar](3) NULL,
	[SoTienFOB] [numeric](17, 4) NULL,
	[MaTT_TienFOB] [varchar](3) NULL,
	[PhiVanChuyen] [numeric](22, 4) NULL,
	[MaTT_PhiVC] [varchar](3) NULL,
	[NoiThanhToanPhiVC] [nvarchar](20) NULL,
	[ChiPhiXepHang1] [numeric](13, 4) NULL,
	[MaTT_ChiPhiXepHang1] [nvarchar](3) NULL,
	[LoaiChiPhiXepHang1] [nvarchar](20) NULL,
	[ChiPhiXepHang2] [numeric](13, 4) NULL,
	[MaTT_ChiPhiXepHang2] [nvarchar](3) NULL,
	[LoaiChiPhiXepHang2] [nvarchar](20) NULL,
	[PhiVC_DuongBo] [numeric](13, 4) NULL,
	[MaTT_PhiVC_DuongBo] [varchar](3) NULL,
	[PhiBaoHiem] [numeric](22, 4) NULL,
	[MaTT_PhiBaoHiem] [varchar](3) NULL,
	[SoTienPhiBaoHiem] [numeric](13, 4) NULL,
	[MaTT_TienPhiBaoHiem] [varchar](3) NULL,
	[SoTienKhauTru] [numeric](13, 4) NULL,
	[MaTT_TienKhauTru] [varchar](3) NULL,
	[LoaiKhauTru] [nvarchar](20) NULL,
	[SoTienKhac] [numeric](13, 0) NULL,
	[MaTT_TienKhac] [varchar](3) NULL,
	[LoaiSoTienKhac] [nvarchar](20) NULL,
	[TongTriGiaHoaDon] [numeric](24, 4) NULL,
	[MaTT_TongTriGia] [varchar](3) NULL,
	[DieuKienGiaHoaDon] [varchar](20) NULL,
	[DiaDiemGiaoHang] [nvarchar](30) NULL,
	[GhiChuDacBiet] [nvarchar](100) NULL,
	[TongSoDongHang] [numeric](4, 0) NULL,
	[TrangThaiXuLy] [varchar](2) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HoaDon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_PhanLoaiHoaDon]    Script Date: 11/11/2013 17:19:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_PhanLoaiHoaDon]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_PhanLoaiHoaDon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HoaDon_ID] [bigint] NOT NULL,
	[MaPhaLoai] [varchar](3) NULL,
	[So] [varchar](35) NULL,
	[NgayThangNam] [datetime] NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HoaDon_PhanLoai] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_GiayPhep_SAA]    Script Date: 11/11/2013 17:20:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_GiayPhep_SAA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[MaBuuChinhNguoiKhai] [varchar](7) NULL,
	[DiaChiNguoiKhai] [nvarchar](300) NULL,
	[MaNuocNguoiKhai] [varchar](2) NULL,
	[SoDienThoaiNguoiKhai] [varchar](20) NULL,
	[SoFaxNguoiKhai] [varchar](20) NULL,
	[EmailNguoiKhai] [varchar](210) NULL,
	[SoDonXinCapPhep] [numeric](12, 0) NULL,
	[ChucNangChungTu] [int] NULL,
	[LoaiGiayPhep] [varchar](4) NULL,
	[MaDonViCapPhep] [varchar](6) NULL,
	[SoHopDong] [nvarchar](35) NULL,
	[MaThuongNhanXuatKhau] [varchar](13) NULL,
	[TenThuongNhanXuatKhau] [nvarchar](300) NULL,
	[MaBuuChinhXuatKhau] [varchar](9) NULL,
	[SoNhaTenDuongXuatKhau] [varchar](35) NULL,
	[PhuongXaXuatKhau] [nvarchar](35) NULL,
	[QuanHuyenXuatKhau] [nvarchar](35) NULL,
	[TinhThanhPhoXuatKhau] [nvarchar](35) NULL,
	[MaQuocGiaXuatKhau] [varchar](2) NULL,
	[SoDienThoaiXuatKhau] [varchar](20) NULL,
	[SoFaxXuatKhau] [varchar](20) NULL,
	[EmailXuatKhau] [varchar](210) NULL,
	[NuocXuatKhau] [varchar](2) NULL,
	[MaThuongNhanNhapKhau] [varchar](13) NULL,
	[TenThuongNhanNhapKhau] [nvarchar](300) NULL,
	[SoDangKyKinhDoanh] [varchar](35) NULL,
	[MaBuuChinhNhapKhau] [varchar](7) NULL,
	[DiaChiThuongNhanNhapKhau] [nvarchar](300) NULL,
	[MaQuocGiaNhapKhau] [varchar](2) NULL,
	[SoDienThoaiNhapKhau] [varchar](20) NULL,
	[SoFaxNhapKhau] [varchar](20) NULL,
	[EmailNhapKhau] [varchar](210) NULL,
	[NuocQuaCanh] [varchar](2) NULL,
	[PhuongTienVanChuyen] [varchar](2) NULL,
	[MaCuaKhauNhap] [varchar](6) NULL,
	[TenCuaKhauNhap] [nvarchar](35) NULL,
	[SoGiayChungNhan] [nvarchar](35) NULL,
	[DiaDiemKiemDich] [nvarchar](150) NULL,
	[BenhMienDich] [nvarchar](300) NULL,
	[NgayTiemPhong] [datetime] NULL,
	[KhuTrung] [nvarchar](300) NULL,
	[NongDo] [nvarchar](300) NULL,
	[DiaDiemNuoiTrong] [nvarchar](70) NULL,
	[NgayKiemDich] [datetime] NULL,
	[ThoiGianKiemDich] [datetime] NULL,
	[DiaDiemGiamSat] [nvarchar](150) NULL,
	[NgayGiamSat] [datetime] NULL,
	[ThoiGianGiamSat] [datetime] NULL,
	[SoBanCanCap] [numeric](2, 0) NULL,
	[VatDungKhac] [nvarchar](150) NULL,
	[HoSoLienQuan] [nvarchar](750) NULL,
	[NoiChuyenDen] [nvarchar](250) NULL,
	[NguoiKiemTra] [nvarchar](50) NULL,
	[KetQuaKiemTra] [nvarchar](996) NULL,
	[TenTau] [nvarchar](35) NULL,
	[QuocTich] [varchar](2) NULL,
	[TenThuyenTruong] [nvarchar](50) NULL,
	[TenBacSi] [nvarchar](50) NULL,
	[SoThuyenVien] [numeric](4, 0) NULL,
	[SoHanhKhach] [numeric](4, 0) NULL,
	[CangRoiCuoiCung] [nvarchar](300) NULL,
	[CangDenTiepTheo] [nvarchar](300) NULL,
	[CangBocHangDauTien] [nvarchar](300) NULL,
	[NgayRoiCang] [datetime] NULL,
	[TenHangCangDauTien] [nvarchar](300) NULL,
	[SoLuongHangCangDauTien] [numeric](8, 0) NULL,
	[DonViTinhSoLuongHangCangDauTien] [varchar](30) NULL,
	[KhoiLuongHangCangDauTien] [numeric](10, 3) NULL,
	[DonViTinhKhoiLuongHangCangDauTien] [varchar](3) NULL,
	[TenHangCanBoc] [nvarchar](300) NULL,
	[SoLuongHangCanBoc] [numeric](8, 0) NULL,
	[DonViTinhSoLuongHangCanBoc] [varchar](30) NULL,
	[KhoiLuongHangCanBoc] [numeric](10, 3) NULL,
	[DonViTinhKhoiLuongHangCanBoc] [varchar](3) NULL,
	[MaKetQuaXuLy] [varchar](75) NULL,
	[KetQuaXuLySoGiayPhep] [varchar](70) NULL,
	[KetQuaXuLyNgayCap] [datetime] NULL,
	[KetQuaXuLyHieuLucTuNgay] [datetime] NULL,
	[KetQuaXuLyHieuLucDenNgay] [datetime] NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[LyDoKhongDat] [nvarchar](996) NULL,
	[DaiDienDonViCapPhep] [nvarchar](300) NULL,
	[HoSoKemTheo_1] [varchar](10) NULL,
	[HoSoKemTheo_2] [varchar](10) NULL,
	[HoSoKemTheo_3] [varchar](10) NULL,
	[HoSoKemTheo_4] [varchar](10) NULL,
	[HoSoKemTheo_5] [varchar](10) NULL,
	[HoSoKemTheo_6] [varchar](10) NULL,
	[HoSoKemTheo_7] [varchar](10) NULL,
	[HoSoKemTheo_8] [varchar](10) NULL,
	[HoSoKemTheo_9] [varchar](10) NULL,
	[HoSoKemTheo_10] [varchar](10) NULL,
	[TenLoaiPhuongTienVanChuyen] [nvarchar](75) NULL,
	[TrangThaiXuLy] [int] NULL,
	[NgayKhaiBao] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_GiayPhep_SAA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_GiayPhep_SEA]    Script Date: 11/11/2013 17:20:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_GiayPhep_SEA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_GiayPhep_SEA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[SoDonXinCapPhep] [numeric](12, 0) NULL,
	[ChucNangChungTu] [int] NOT NULL,
	[LoaiGiayPhep] [varchar](4) NULL,
	[LoaiHinhXNK] [varchar](1) NULL,
	[MaDV_CapPhep] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](13) NULL,
	[TenDoanhNghiep] [nvarchar](300) NULL,
	[QuyetDinhSo] [varchar](17) NULL,
	[SoGiayChungNhan] [varchar](17) NULL,
	[NoiCap] [nvarchar](35) NOT NULL,
	[NgayCap] [datetime] NULL,
	[MaBuuChinh] [varchar](7) NULL,
	[DiaChi] [varchar](300) NULL,
	[MaQuocGia] [varchar](2) NULL,
	[SoDienThoai] [varchar](20) NULL,
	[SoFax] [varchar](20) NULL,
	[Email] [varchar](210) NULL,
	[SoHopDongMua] [varchar](17) NULL,
	[NgayHopDongMua] [datetime] NULL,
	[SoHopDongBan] [varchar](17) NULL,
	[NgayHopDongBan] [datetime] NULL,
	[PhuongTienVanChuyen] [varchar](2) NULL,
	[MaCuaKhauXuatNhap] [varchar](6) NULL,
	[TenCuaKhauXuatNhap] [varchar](35) NULL,
	[NgayBatDau] [datetime] NULL,
	[NgayKetThuc] [datetime] NULL,
	[HoSoLienQuan] [nvarchar](750) NULL,
	[GhiChu] [nvarchar](996) NULL,
	[TenGiamDoc] [nvarchar](300) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[HoSoKemTheo_1] [varchar](10) NULL,
	[HoSoKemTheo_2] [varchar](10) NULL,
	[HoSoKemTheo_3] [varchar](10) NULL,
	[HoSoKemTheo_4] [varchar](10) NULL,
	[HoSoKemTheo_5] [varchar](10) NULL,
	[HoSoKemTheo_6] [varchar](10) NULL,
	[HoSoKemTheo_7] [varchar](10) NULL,
	[HoSoKemTheo_8] [varchar](10) NULL,
	[HoSoKemTheo_9] [varchar](10) NULL,
	[HoSoKemTheo_10] [varchar](10) NULL,
	[MaKetQuaXuLy] [varchar](75) NULL,
	[KetQuaXuLySoGiayPhep] [varchar](50) NULL,
	[KetQuaXuLyNgayCap] [datetime] NULL,
	[KetQuaXuLyHieuLucTuNgay] [datetime] NULL,
	[KetQuaXuLyHieuLucDenNgay] [datetime] NULL,
	[GhiChuDanhChoCoQuanCapPhep] [nvarchar](996) NULL,
	[DaiDienDonViCapPhep] [nvarchar](996) NULL,
	[NgayKhaiBao] [datetime] NULL,
	[TenNguoiKhai] [nvarchar](300) NULL,
	[TenDonViCapPhep] [nvarchar](300) NULL,
	[TenPhuongTienVanChuyen] [nvarchar](75) NULL,
	[TrangThaiXuLy] [int] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_GiayPhep] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_GiayPhep_SFA]    Script Date: 11/11/2013 17:20:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_GiayPhep_SFA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_GiayPhep_SFA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[TenNguoiKhai] [nvarchar](300) NULL,
	[MaDonViCapPhep] [varchar](6) NULL,
	[TenDonViCapPhep] [nvarchar](300) NULL,
	[SoDonXinCapPhep] [numeric](12, 0) NULL,
	[ChucNangChungTu] [int] NULL,
	[LoaiGiayPhep] [varchar](4) NULL,
	[MaThuongNhanXuatKhau] [varchar](13) NULL,
	[TenThuongNhanXuatKhau] [nvarchar](210) NULL,
	[MaBuuChinhXuatKhau] [varchar](9) NULL,
	[SoNhaTenDuongXuatKhau] [varchar](35) NULL,
	[PhuongXaXuatKhau] [nvarchar](35) NULL,
	[QuanHuyenXuatKhau] [nvarchar](35) NULL,
	[TinhThanhPhoXuatKhau] [nvarchar](35) NULL,
	[MaQuocGiaXuatKhau] [varchar](2) NULL,
	[SoDienThoaiXuatKhau] [varchar](20) NULL,
	[SoFaxXuatKhau] [varchar](20) NULL,
	[EmailXuatKhau] [varchar](210) NULL,
	[SoHopDong] [nvarchar](35) NULL,
	[SoVanDon] [varchar](35) NULL,
	[MaBenDi] [varchar](6) NULL,
	[TenBenDi] [varchar](35) NULL,
	[MaThuongNhanNhapKhau] [varchar](13) NULL,
	[TenThuongNhanNhapKhau] [nvarchar](300) NULL,
	[MaBuuChinhNhapKhau] [varchar](7) NULL,
	[SoNhaTenDuongNhapKhau] [nvarchar](300) NULL,
	[MaQuocGiaNhapKhau] [varchar](2) NULL,
	[SoDienThoaiNhapKhau] [varchar](20) NULL,
	[SoFaxNhapKhau] [varchar](20) NULL,
	[EmailNhapKhau] [varchar](70) NULL,
	[MaBenDen] [varchar](6) NULL,
	[TenBenDen] [varchar](35) NULL,
	[ThoiGianNhapKhauDuKien] [datetime] NULL,
	[GiaTriHangHoa] [numeric](19, 2) NULL,
	[DonViTienTe] [varchar](3) NULL,
	[DiaDiemTapKetHangHoa] [nvarchar](70) NULL,
	[ThoiGianKiemTra] [datetime] NULL,
	[DiaDiemKiemTra] [nvarchar](70) NULL,
	[HoSoLienQuan] [nvarchar](750) NULL,
	[GhiChu] [nvarchar](996) NULL,
	[DaiDienThuongNhanNhapKhau] [nvarchar](300) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[MaKetQuaXuLy] [varchar](75) NULL,
	[KetQuaXuLySoGiayPhep] [varchar](70) NULL,
	[KetQuaXuLyNgayCap] [datetime] NULL,
	[KetQuaXuLyHieuLucTuNgay] [datetime] NULL,
	[KetQuaXuLyHieuLucDenNgay] [datetime] NULL,
	[GhiChuDanhChoCoQuanCapPhep] [nvarchar](500) NULL,
	[DaiDienCoQuanKiemTra] [nvarchar](500) NULL,
	[NgayKhaiBao] [datetime] NULL,
	[HoSoKemTheo_1] [varchar](10) NULL,
	[HoSoKemTheo_2] [varchar](10) NULL,
	[HoSoKemTheo_3] [varchar](10) NULL,
	[HoSoKemTheo_4] [varchar](10) NULL,
	[HoSoKemTheo_5] [varchar](10) NULL,
	[HoSoKemTheo_6] [varchar](10) NULL,
	[HoSoKemTheo_7] [varchar](10) NULL,
	[HoSoKemTheo_8] [varchar](10) NULL,
	[HoSoKemTheo_9] [varchar](10) NULL,
	[HoSoKemTheo_10] [varchar](10) NULL,
	[TrangThaiXuLy] [int] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_GiayPhep_SFA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_GiayPhep_SMA]    Script Date: 11/11/2013 17:20:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_GiayPhep_SMA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_GiayPhep_SMA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[TenNguoiKhai] [nvarchar](300) NULL,
	[MaBuuChinhNguoiKhai] [varchar](7) NULL,
	[DiaChiNguoiKhai] [nvarchar](300) NULL,
	[MaNuocNguoiKhai] [varchar](2) NULL,
	[SoDienThoaiNguoiKhai] [varchar](20) NULL,
	[SoFaxNguoiKhai] [varchar](20) NULL,
	[EmailNguoiKhai] [varchar](210) NULL,
	[SoDonXinCapPhep] [numeric](12, 0) NULL,
	[ChucNangChungTu] [int] NULL,
	[LoaiGiayPhep] [varchar](4) NULL,
	[MaDonViCapPhep] [varchar](6) NULL,
	[TenDonViCapPhep] [nvarchar](300) NULL,
	[MaThuongNhanNhapKhau] [varchar](13) NULL,
	[TenThuongNhanNhapKhau] [nvarchar](300) NULL,
	[MaBuuChinhNhapKhau] [varchar](7) NULL,
	[DiaChiThuongNhanNhapKhau] [nvarchar](300) NULL,
	[MaQuocGiaNhapKhau] [varchar](2) NULL,
	[SoDienThoaiNhapKhau] [varchar](20) NULL,
	[SoFaxNhapKhau] [varchar](20) NULL,
	[EmailNhapKhau] [varchar](210) NULL,
	[KetQuaXuLy] [numeric](1, 0) NULL,
	[MaKetQuaXuLy] [varchar](75) NULL,
	[KetQuaXuLySoGiayPhep] [varchar](70) NULL,
	[KetQuaXuLyNgayCap] [datetime] NULL,
	[KetQuaXuLyHieuLucTuNgay] [datetime] NULL,
	[KetQuaXuLyHieuLucDenNgay] [datetime] NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
	[HoSoKemTheo_1] [varchar](10) NULL,
	[HoSoKemTheo_2] [varchar](10) NULL,
	[HoSoKemTheo_3] [varchar](10) NULL,
	[HoSoKemTheo_4] [varchar](10) NULL,
	[HoSoKemTheo_5] [varchar](10) NULL,
	[HoSoKemTheo_6] [varchar](10) NULL,
	[HoSoKemTheo_7] [varchar](10) NULL,
	[HoSoKemTheo_8] [varchar](10) NULL,
	[HoSoKemTheo_9] [varchar](10) NULL,
	[HoSoKemTheo_10] [varchar](10) NULL,
	[SoCuaGiayPhepLuuHanh] [varchar](17) NULL,
	[SoGiayPhepThucHanh] [varchar](17) NULL,
	[MaCuaKhauNhapDuKien] [varchar](6) NULL,
	[TenCuaKhauNhapDuKien] [nvarchar](35) NULL,
	[TonKhoDenNgay] [datetime] NULL,
	[TenGiamDocDoanhNghiep] [nvarchar](300) NULL,
	[DaiDienDonViCapPhep] [nvarchar](300) NULL,
	[GhiChuDanhChoCoQuanCapPhep] [nvarchar](500) NULL,
	[NgayKhaiBao] [datetime] NULL,
	[SoTiepNhan] [numeric](12, 0) NULL,
	[NgayTiepNhan] [datetime] NULL,
	[PhanLuong] [nvarchar](300) NULL,
	[HuongDan] [nvarchar](1000) NULL,
	[HoSoLienQuan] [nvarchar](750) NULL,
	[GhiChu] [nvarchar](996) NULL,
	[TrangThaiXuLy] [int] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_GiayPhep_SMA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_t_KDT_VNACC_GiayPhep_SAA]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]'))
ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] DROP CONSTRAINT [FK_t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_t_KDT_VNACC_GiayPhep_SAA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_HangGiayPhep_CangTrungGian_t_KDT_VNACC_GiayPhep_SAA]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]'))
ALTER TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] DROP CONSTRAINT [FK_t_KDT_VNACC_HangGiayPhep_CangTrungGian_t_KDT_VNACC_GiayPhep_SAA]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_VNACC_HangGiayPhep_SMA_t_KDT_VNACC_GiayPhep_SMA]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]'))
ALTER TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] DROP CONSTRAINT [FK_t_KDT_VNACC_HangGiayPhep_SMA_t_KDT_VNACC_GiayPhep_SMA]
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]    Script Date: 11/11/2013 17:22:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GiayPhep_ID] [bigint] NOT NULL,
	[TongSoToKhai] [numeric](6, 0) NULL,
	[TinhTrangThamTra] [varchar](2) NULL,
	[PhuongThucXuLyCuoiCung] [numeric](1, 0) NULL,
	[SoDonXinCapPhep] [numeric](12, 0) NULL,
	[MaNhanVienKiemTra] [varchar](8) NULL,
	[NgayKhaiBao] [datetime] NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[TenNguoiKhai] [nvarchar](300) NULL,
	[NgayGiaoViec] [datetime] NULL,
	[GioGiaoViec] [datetime] NULL,
	[NguoiGiaoViec] [varchar](8) NULL,
	[NoiDungGiaoViec] [nvarchar](768) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangGiayPhep]    Script Date: 11/11/2013 17:22:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangGiayPhep]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangGiayPhep](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GiayPhepType] [varchar](3) NOT NULL,
	[GiayPhep_ID] [bigint] NOT NULL,
	[TenHangHoa] [nvarchar](768) NULL,
	[MaSoHangHoa] [varchar](12) NULL,
	[SoLuong] [numeric](10, 3) NULL,
	[DonVitinhSoLuong] [varchar](3) NULL,
	[KhoiLuong] [numeric](10, 3) NULL,
	[DonVitinhKhoiLuong] [varchar](3) NULL,
	[XuatXu] [varchar](2) NULL,
	[TinhBiet] [varchar](1) NULL,
	[Tuoi] [numeric](3, 0) NULL,
	[NoiSanXuat] [nvarchar](150) NULL,
	[QuyCachDongGoi] [nvarchar](50) NULL,
	[TongSoLuongNhapKhau] [numeric](8, 0) NULL,
	[DonViTinhTongSoLuongNhapKhau] [varchar](3) NULL,
	[KichCoCaThe] [nvarchar](10) NULL,
	[TrongLuongTinh] [numeric](10, 3) NULL,
	[DonViTinhTrongLuong] [varchar](3) NULL,
	[TrongLuongCaBi] [numeric](10, 3) NULL,
	[DonViTinhTrongLuongCaBi] [varchar](3) NULL,
	[SoLuongKiemDich] [numeric](8, 0) NULL,
	[DonViTinhSoLuongKiemDich] [varchar](3) NULL,
	[LoaiBaoBi] [nvarchar](100) NULL,
	[MucDichSuDung] [nvarchar](100) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_GiayPhep_HangHoa] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('t_KDT_VNACC_HangGiayPhep') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'GiayPhepType' AND [object_id] = OBJECT_ID('t_KDT_VNACC_HangGiayPhep')))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'SEA, SFA, SAA' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_KDT_VNACC_HangGiayPhep', @level2type=N'COLUMN',@level2name=N'GiayPhepType'
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]    Script Date: 11/11/2013 17:22:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GiayPhep_ID] [bigint] NOT NULL,
	[TenHangCangTrungGian] [nvarchar](300) NULL,
	[TenCangTrungGian] [nvarchar](300) NULL,
	[SoLuongHangCangTrungGian] [numeric](8, 0) NULL,
	[DonViTinhSoLuongHangCangTrungGian] [varchar](3) NULL,
	[KhoiLuongHangCangTrungGian] [numeric](10, 3) NULL,
	[DonViTinhKhoiLuongHangCangTrungGian] [varchar](3) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HangGiayPhep_CangTrungGian] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]    Script Date: 11/11/2013 17:22:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_SMA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[GiayPhep_ID] [bigint] NOT NULL,
	[TenThuocQuyCachDongGoi] [nvarchar](768) NULL,
	[MaSoHangHoa] [varchar](12) NULL,
	[TenHoatChatGayNghien] [nvarchar](70) NULL,
	[HoatChat] [varchar](35) NULL,
	[TieuChuanChatLuong] [varchar](35) NULL,
	[SoDangKy] [varchar](17) NULL,
	[HanDung] [datetime] NULL,
	[SoLuong] [numeric](10, 3) NULL,
	[DonVitinhSoLuong] [varchar](3) NULL,
	[CongDung] [varchar](50) NULL,
	[TongSoKhoiLuongHoatChatGayNghien] [numeric](10, 3) NULL,
	[DonVitinhKhoiLuong] [varchar](3) NULL,
	[GiaNhapKhauVND] [numeric](19, 3) NULL,
	[GiaBanBuonVND] [numeric](19, 3) NULL,
	[GiaBanLeVND] [numeric](19, 3) NULL,
	[MaThuongNhanXuatKhau] [varchar](13) NULL,
	[TenThuongNhanXuatKhau] [nvarchar](300) NULL,
	[MaBuuChinhXuatKhau] [varchar](9) NULL,
	[SoNhaTenDuongXuatKhau] [varchar](35) NULL,
	[PhuongXaXuatKhau] [nvarchar](35) NULL,
	[QuanHuyenXuatKhau] [nvarchar](35) NULL,
	[TinhThanhPhoXuatKhau] [nvarchar](35) NULL,
	[MaQuocGiaXuatKhau] [varchar](2) NULL,
	[MaCongTySanXuat] [varchar](13) NULL,
	[TenCongTySanXuat] [nvarchar](300) NULL,
	[MaBuuChinhCongTySanXuat] [varchar](9) NULL,
	[SoNhaTenDuongCongTySanXuat] [varchar](35) NULL,
	[PhuongXaCongTySanXuat] [nvarchar](35) NULL,
	[QuanHuyenCongTySanXuat] [nvarchar](35) NULL,
	[TinhThanhPhoCongTySanXuat] [nvarchar](35) NULL,
	[MaQuocGiaCongTySanXuat] [varchar](2) NULL,
	[MaCongTyCungCap] [varchar](13) NULL,
	[TenCongTyCungCap] [nvarchar](300) NULL,
	[MaBuuChinhCongTyCungCap] [varchar](9) NULL,
	[SoNhaTenDuongCongTyCungCap] [varchar](35) NULL,
	[PhuongXaCongTyCungCap] [nvarchar](35) NULL,
	[QuanHuyenCongTyCungCap] [nvarchar](35) NULL,
	[TinhThanhPhoCongTyCungCap] [nvarchar](35) NULL,
	[MaQuocGiaCongTyCungCap] [varchar](2) NULL,
	[MaDonViUyThac] [varchar](13) NULL,
	[TenDonViUyThac] [nvarchar](300) NULL,
	[MaBuuChinhDonViUyThac] [varchar](9) NULL,
	[SoNhaTenDuongDonViUyThac] [varchar](35) NULL,
	[PhuongXaDonViUyThac] [nvarchar](35) NULL,
	[QuanHuyenDonViUyThac] [nvarchar](35) NULL,
	[TinhThanhPhoDonViUyThac] [nvarchar](35) NULL,
	[MaQuocGiaDonViUyThac] [varchar](2) NULL,
	[LuongTonKhoKyTruoc] [numeric](8, 0) NULL,
	[DonViTinhLuongTonKyTruoc] [varchar](3) NULL,
	[LuongNhapTrongKy] [numeric](8, 0) NULL,
	[TongSo] [numeric](9, 0) NULL,
	[TongSoXuatTrongKy] [numeric](9, 0) NULL,
	[SoLuongTonKhoDenNgay] [numeric](8, 0) NULL,
	[HuHao] [numeric](8, 0) NULL,
	[GhiChuChiTiet] [nvarchar](105) NULL,
	[Notes] [nvarchar](250) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_HangGiayPhep_SMA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_t_KDT_VNACC_GiayPhep_SAA] FOREIGN KEY([GiayPhep_ID])
REFERENCES [dbo].[t_KDT_VNACC_GiayPhep_SAA] ([ID])
ON UPDATE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] CHECK CONSTRAINT [FK_t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_t_KDT_VNACC_GiayPhep_SAA]
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_HangGiayPhep_CangTrungGian_t_KDT_VNACC_GiayPhep_SAA] FOREIGN KEY([GiayPhep_ID])
REFERENCES [dbo].[t_KDT_VNACC_GiayPhep_SAA] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] CHECK CONSTRAINT [FK_t_KDT_VNACC_HangGiayPhep_CangTrungGian_t_KDT_VNACC_GiayPhep_SAA]
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]  WITH CHECK ADD  CONSTRAINT [FK_t_KDT_VNACC_HangGiayPhep_SMA_t_KDT_VNACC_GiayPhep_SMA] FOREIGN KEY([GiayPhep_ID])
REFERENCES [dbo].[t_KDT_VNACC_GiayPhep_SMA] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] CHECK CONSTRAINT [FK_t_KDT_VNACC_HangGiayPhep_SMA_t_KDT_VNACC_GiayPhep_SMA]
GO



/****** Object:  Table [dbo].[t_KDT_VNACC_ChiThiHaiQuan]    Script Date: 11/11/2013 17:26:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACC_ChiThiHaiQuan]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACC_ChiThiHaiQuan](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoaiThongTin] [varchar](10) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[PhanLoai] [varchar](1) NULL,
	[Ngay] [datetime] NULL,
	[Ten] [varchar](500) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ChiThiHaiQuan] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

IF NOT EXISTS (SELECT null FROM SYS.EXTENDED_PROPERTIES WHERE [major_id] = OBJECT_ID('t_KDT_VNACC_ChiThiHaiQuan') AND [name] = N'MS_Description' AND [minor_id] = (SELECT [column_id] FROM SYS.COLUMNS WHERE [name] = 'LoaiThongTin' AND [object_id] = OBJECT_ID('t_KDT_VNACC_ChiThiHaiQuan')))
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'GP: Giay phep, TK: To khai, HDon: Hoa don, HDong: Hop dong, v.v...' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_KDT_VNACC_ChiThiHaiQuan', @level2type=N'COLUMN',@level2name=N'LoaiThongTin'
GO



/****** Object:  Table [dbo].[t_KDT_VNACCS_TEA]    Script Date: 11/11/2013 17:27:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TEA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TEA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoDanhMucMienThue] [numeric](12, 0) NULL,
	[PhanLoaiXuatNhapKhau] [varchar](1) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[DiaChiCuaNguoiKhai] [nvarchar](100) NULL,
	[SDTCuaNguoiKhai] [varchar](20) NULL,
	[ThoiHanMienThue] [datetime] NULL,
	[TenDuAnDauTu] [nvarchar](70) NULL,
	[DiaDiemXayDungDuAn] [nvarchar](100) NULL,
	[MucTieuDuAn] [nvarchar](100) NULL,
	[MaMienGiam] [varchar](5) NULL,
	[PhamViDangKyDMMT] [nvarchar](100) NULL,
	[NgayDuKienXNK] [datetime] NULL,
	[GP_GCNDauTuSo] [varchar](20) NULL,
	[NgayChungNhan] [datetime] NULL,
	[CapBoi] [nvarchar](100) NULL,
	[GhiChu] [nvarchar](255) NULL,
	[CamKetSuDung] [nvarchar](140) NULL,
	[MaNguoiKhai] [varchar](13) NULL,
	[KhaiBaoTuNgay] [datetime] NULL,
	[KhaiBaoDenNgay] [datetime] NULL,
	[MaSoQuanLyDSMT] [varchar](14) NULL,
	[PhanLoaiCapPhep] [varchar](1) NULL,
	[TrangThaiXuLy] [varchar](2) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_TEA] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACCS_TEA_HangHoa]    Script Date: 11/11/2013 17:27:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TEA_HangHoa]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TEA_HangHoa](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[MoTaHangHoa] [nvarchar](200) NULL,
	[SoLuongDangKyMT] [numeric](19, 4) NULL,
	[DVTSoLuongDangKyMT] [varchar](4) NULL,
	[SoLuongDaSuDung] [numeric](19, 4) NULL,
	[DVTSoLuongDaSuDung] [varchar](4) NULL,
	[TriGia] [numeric](24, 4) NULL,
	[TriGiaDuKien] [numeric](24, 4) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_TEA_HangHoa] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACCS_TEADieuChinh]    Script Date: 11/11/2013 17:27:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TEADieuChinh]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TEADieuChinh](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[LanDieuChinhGP_GCN] [int] NULL,
	[ChungNhanDieuChinhSo] [varchar](20) NULL,
	[NgayChungNhanDieuChinh] [datetime] NULL,
	[DieuChinhBoi] [nvarchar](100) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_TEADieuChinh] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACCS_TEANguoiXNK]    Script Date: 11/11/2013 17:27:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TEANguoiXNK]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TEANguoiXNK](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[MaNguoiXNK] [varchar](13) NULL,
	[TenNguoiXNK] [nvarchar](100) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACCS_TIA]    Script Date: 11/11/2013 17:29:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TIA]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TIA](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoToKhai] [numeric](18, 0) NULL,
	[CoBaoNhapKhauXuatKhau] [varchar](1) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[MaNguoiKhaiDauTien] [varchar](5) NULL,
	[TenNguoiKhaiDauTien] [nvarchar](50) NULL,
	[MaNguoiXuatNhapKhau] [varchar](13) NULL,
	[TenNguoiXuatNhapKhau] [nvarchar](100) NULL,
	[ThoiHanTaiXuatNhap] [datetime] NULL,
	[TrangThaiXuLy] [varchar](2) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_TIA_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACCS_TIA_HangHoa]    Script Date: 11/11/2013 17:29:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TIA_HangHoa]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TIA_HangHoa](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[MaSoHangHoa] [varchar](12) NULL,
	[SoLuongBanDau] [numeric](19, 4) NULL,
	[DVTSoLuongBanDau] [varchar](4) NULL,
	[SoLuongDaTaiNhapTaiXuat] [numeric](19, 4) NULL,
	[DVTSoLuongDaTaiNhapTaiXuat] [varchar](4) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_TIA_HangHoa] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[t_KDT_VNACCS_TKTriGiaThap]    Script Date: 11/11/2013 17:29:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_TKTriGiaThap]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_TKTriGiaThap](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaLoaiHinh] [varchar](1) NULL,
	[SoToKhai] [numeric](12, 0) NULL,
	[PhanLoaiBaoCaoSua] [varchar](1) NULL,
	[PhanLoaiToChuc] [varchar](1) NULL,
	[CoQuanHaiQuan] [varchar](6) NULL,
	[NhomXuLyHS] [varchar](2) NULL,
	[MaDonVi] [varchar](13) NULL,
	[TenDonVi] [nvarchar](100) NULL,
	[MaBuuChinhDonVi] [varchar](7) NULL,
	[DiaChiDonVi] [nvarchar](100) NULL,
	[SoDienThoaiDonVi] [varchar](20) NULL,
	[MaDoiTac] [varchar](13) NULL,
	[TenDoiTac] [nvarchar](100) NULL,
	[MaBuuChinhDoiTac] [varchar](7) NULL,
	[DiaChiDoiTac1] [nvarchar](35) NULL,
	[DiaChiDoiTac2] [nvarchar](35) NULL,
	[DiaChiDoiTac3] [nvarchar](35) NULL,
	[DiaChiDoiTac4] [nvarchar](35) NULL,
	[MaNuocDoiTac] [varchar](2) NULL,
	[SoHouseAWB] [varchar](20) NULL,
	[SoMasterAWB] [varchar](20) NULL,
	[SoLuong] [numeric](12, 4) NULL,
	[TrongLuong] [numeric](14, 4) NULL,
	[MaDDLuuKho] [varchar](7) NULL,
	[TenMayBayChoHang] [nvarchar](12) NULL,
	[NgayHangDen] [datetime] NULL,
	[MaCangDoHang] [varchar](3) NULL,
	[MaDDNhanHangCuoiCung] [varchar](5) NULL,
	[MaDiaDiemXepHang] [varchar](6) NULL,
	[TongTriGiaTinhThue] [numeric](24, 4) NULL,
	[MaTTTongTriGiaTinhThue] [varchar](3) NULL,
	[GiaKhaiBao] [numeric](12, 4) NULL,
	[PhanLoaiGiaHD] [varchar](1) NULL,
	[MaDieuKienGiaHD] [varchar](3) NULL,
	[MaTTHoaDon] [varchar](3) NULL,
	[TongTriGiaHD] [numeric](24, 4) NULL,
	[MaPhanLoaiPhiVC] [varchar](1) NULL,
	[MaTTPhiVC] [varchar](3) NULL,
	[PhiVanChuyen] [numeric](22, 4) NULL,
	[MaPhanLoaiPhiBH] [varchar](1) NULL,
	[MaTTPhiBH] [varchar](3) NULL,
	[PhiBaoHiem] [numeric](22, 4) NULL,
	[MoTaHangHoa] [nvarchar](200) NULL,
	[MaNuocXuatXu] [varchar](2) NULL,
	[TriGiaTinhThue] [numeric](24, 4) NULL,
	[GhiChu] [nvarchar](100) NULL,
	[SoQuanLyNoiBoDN] [varchar](20) NULL,
	[TrangThaiXuLy] [nchar](10) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_TKTriGiaThap] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO



/****** Object:  Table [dbo].[t_KDT_VNACCS_MsgLog]    Script Date: 11/11/2013 17:31:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_MsgLog]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_MsgLog](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[MaNghiepVu] [varchar](10) NULL,
	[TieuDeThongBao] [nvarchar](255) NULL,
	[NoiDungThongBao] [nvarchar](max) NULL,
	[Log_Messages] [nvarchar](max) NULL,
	[Item_id] [bigint] NULL,
	[CreatedTime] [datetime] NULL,
	[MessagesTag] [varchar](26) NULL,
	[InputMessagesID] [varchar](10) NULL,
	[IndexTag] [nchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_Messages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_VNACCS_MsgPhanBo]    Script Date: 11/11/2013 17:31:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_MsgPhanBo]') AND type in (N'U'))
begin
CREATE TABLE [dbo].[t_KDT_VNACCS_MsgPhanBo](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NOT NULL,
	[TrangThai] [varchar](10) NULL,
	[RTPTag] [varchar](64) NULL,
	[messagesTag] [varchar](50) NULL,
	[MessagesInputID] [varchar](50) NULL,
	[IndexTag] [varchar](50) NULL,
	[MaNghiepVu] [varchar](10) NULL,
	[GhiChu] [nvarchar](255) NULL,
	[TerminalID] [varchar](6) NULL,
	[CreatedTime] [datetime] NULL,
	[SoTiepNhan] [varchar](50) NULL,
 CONSTRAINT [PK_t_KDT_VNACCS_MsgPhanBo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end
GO

SET ANSI_PADDING OFF
GO

--SAA
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SAA' AND column_name = 'TrangThaiXuLy') = 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA] ADD [TrangThaiXuLy] [int] NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SAA' AND column_name = 'NgayKhaiBao') = 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA] ADD [NgayKhaiBao] [datetime] NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SAA' AND column_name = 'SoDonXinCapPhep') > 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SAA] ALTER COLUMN [SoDonXinCapPhep] [numeric] (12, 0) NULL
END	
GO

--SEA
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SEA' AND column_name = 'SoDonXinCapPhep') > 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SEA] ALTER COLUMN [SoDonXinCapPhep] [numeric] (12, 0) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SEA' AND column_name = 'TrangThaiXuLy') = 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SEA] ADD [TrangThaiXuLy] [int] NULL
END	
GO

--SFA
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SFA' AND column_name = 'SoDonXinCapPhep') > 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SFA] ALTER COLUMN [SoDonXinCapPhep] [numeric] (12, 0) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SFA' AND column_name = 'TrangThaiXuLy') = 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SFA] ADD [TrangThaiXuLy] [int] NULL
END	
GO

--SMA
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SMA' AND column_name = 'SoDonXinCapPhep') > 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SMA] ALTER COLUMN [SoDonXinCapPhep] [numeric] (12, 0) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_GiayPhep_SMA' AND column_name = 'TrangThaiXuLy') = 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_GiayPhep_SMA] ADD [TrangThaiXuLy] [int] NULL
END	
GO

--t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_VNACC_ToKhaiMauDich_KhaiBoSung' AND column_name = 'TrangThaiXuLy') = 0
BEGIN
	ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich_KhaiBoSung] ADD [TrangThaiXuLy] [int] NULL
END	
GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '10.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('10.8', GETDATE(), N'Bo sung cac bang lien quan den to khai mau dich VNACCS')
END	

