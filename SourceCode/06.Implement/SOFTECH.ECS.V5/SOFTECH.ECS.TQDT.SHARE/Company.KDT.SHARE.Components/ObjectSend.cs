﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class ObjectSend
    {
        public NameBase From { get; private set; }
        public NameBase To { get; private set; }
        public SubjectBase Subject { get; private set; }
        public Object Content { get; private set; }
        public NameBase DNThueDL { get; private set; }
        public DeclarationBase ChiTietDongBo { get; private set;  }
        public ObjectSend(NameBase from, NameBase to, SubjectBase subject, Object content)
        {
            if (from == null)
                throw new ArgumentNullException("Nơi đi chưa được xác định");
            if (to == null)
                throw new ArgumentNullException("Nơi gửi đến hải quan chưa được xác định");
            if (subject == null)
                throw new ArgumentNullException("Chức năng khai báo chưa được xác định");
            //if (subject.Function == DeclarationFunction && content == null)
            //    throw new ArgumentNullException("Nội dung gửi không được rỗng");
            this.From = from;
            this.To = to;
            this.Subject = subject;
            this.Content = content;
        }
        //public ObjectSend(NameBase from, NameBase to, NameBase dnThueDL, DeclarationBase ChiTietDongBo, Object content)
        //{
        //    if (from == null)
        //        throw new ArgumentNullException("Nơi đi chưa được xác định");
        //    if (to == null)
        //        throw new ArgumentNullException("Nơi gửi đến hải quan chưa được xác định");
        //    if (ChiTietDongBo == null)
        //        throw new ArgumentNullException("Tờ khai chưa đủ thông tin để gửi");
        //    if (dnThueDL == null)
        //        throw new ArgumentNullException("Doanh Nghiệp thuê chưa được xác định");
        //    //if (subject.Function == DeclarationFunction && content == null)
        //    //    throw new ArgumentNullException("Nội dung gửi không được rỗng");
        //    this.From = from;
        //    this.To = to;
        //    this.ChiTietDongBo = ChiTietDongBo;
        //    this.Content = content;
        //    this.DNThueDL = dnThueDL;
        //}
    }
}
