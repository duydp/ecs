﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class NoiDungDieuChinhTKDetail
    {
        public List<NoiDungDieuChinhTKDetail> ListNoiDungDieuChinhTKDetail = new List<NoiDungDieuChinhTKDetail>();

        public void LoadListNoiDungDieuChinhTKDetail()
        {
            ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(this.Id_DieuChinh);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
            db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
            db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
            db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_Id_DieuChinh(SqlTransaction transaction, long TKMDID, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            string whereCondition = "Id_DieuChinh IN (SELECT ID FROM dbo.t_KDT_NoiDungDieuChinhTK WHERE TKMD_ID = " + TKMDID + ")";

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
    }
}
