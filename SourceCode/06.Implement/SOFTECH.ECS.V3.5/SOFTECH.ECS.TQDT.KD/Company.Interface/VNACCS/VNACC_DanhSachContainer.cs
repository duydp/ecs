﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.VNACCS;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_DanhSachContainer : BaseForm
    {
        public KDT_VNACC_ToKhaiMauDich TKMD = null;
        DataTable dt = new DataTable();
        private KDT_VNACCS_Container Cont = null;
        
        public VNACC_DanhSachContainer()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            txtSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            cbNgayKB.Value = TKMD.NgayDangKy;
            dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.ContainerCollection);
            grList.DataSource = dt;
        }

        private void VNACC_DanhSachContainer_Load(object sender, EventArgs e)
        {
            try
            {
                //SetContainer();
                Cursor = Cursors.WaitCursor;
                LoadData();
                //if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                //{
                    //btnSave.Enabled = false;
                    //btnXoa.Enabled = false;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetContainer()
        {
            Cont.GhiChuKhac = txtGhiChu.Text;
            //Cont.so
            //GetSoContainer();
        }
        private void SetContainer()
        {
            txtGhiChu.Text = Cont.GhiChuKhac;
            
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                int sott = 1;
                //TKMD.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow i in dt.Rows)
                {
                    KDT_VNACCS_Container cont = new KDT_VNACCS_Container();
                    if (i["ID"].ToString().Length != 0)
                    {
                        cont.ID = Convert.ToInt32(i["ID"].ToString());
                        cont.ID_TKMD = Convert.ToInt32(i["ID_TKMD"].ToString());
                    }
                    //gp.SoTT = sott;
                    cont.SoVanDon = i["SoVanDon"].ToString();
                    cont.SoContainer = i["SoContainer"].ToString();
                    cont.SoSeal = i["SoSeal"].ToString();                   
                    sott++;
                    TKMD.ContainerCollection.Add(cont);
                    //GetContainer();
                    //cont.InsertUpdateFul();
                }
                ShowMessage("Lưu thành công", false);


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ID > 0)
                {
                    if (ShowMessage("Bạn có muốn xóa container này không?", true) == "Yes")
                    {
                        KDT_VNACCS_Container.DeleteDynamic("ID=" + ID);
                        grList.CurrentRow.Delete();
                    }
                }
                else
                    grList.CurrentRow.Delete();
                grList.Refetch();

                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        //public void sendvnaccs(bool KhaiBaoSua)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception ex)
        //    {
        //        this.ShowMessage(ex.Message, false);
        //    }
        //}
        //private void btnKhaiBao_Click(object sender, EventArgs e)
        //{
        //    sendvnaccs(false);
        //}

        private void btnNhanPhanHoi_Click(object sender, EventArgs e)
        {

        }
        


    }
}
