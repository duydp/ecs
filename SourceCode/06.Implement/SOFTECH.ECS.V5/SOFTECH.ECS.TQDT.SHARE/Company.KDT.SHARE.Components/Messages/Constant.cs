﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public struct DeclarationFunction
    {
        public const string HUY = "1";
        public const string SUA = "5";
        public const string KHAI_BAO = "8";
        public const string CHUA_XU_LY = "12";
        public const string HOI_TRANG_THAI = "13";
        public const string DE_NGHI = "16";
        public const string KHONG_CHAP_NHAN = "27";
        public const string CAP_SO_TIEP_NHAN = "29";
        public const string CAP_SO_TO_KHAI = "30";
        public const string DUYET_LUONG_CHUNG_TU = "31";
        public const string THONG_QUAN = "32";
        public const string THUC_XUAT = "33";
        //public const string PHAN_LUONG = "207";
        public const string XAC_NHAN_RA_KHOI_KV_GIAM_SAT = "34";

    }
    public struct DeclarationIssuer
    {

        public const string KD_TOKHAI_NHAP = "929";
        public const string KD_TOKHAI_XUAT = "930";
        public const string SXXK_TOKHAI_NHAP = "931";
        public const string SXXK_TOKHAI_XUAT = "932";
        //public const string SXXK_TOKHAI_KCX_DONGIAN = "939";
        //public const string SXXK_TOKHAI_KCX_THANG = "940";
        public const string GC_TOKHAI_NHAP = "935";
        public const string GC_TOKHAI_XUAT = "936";
        public const string GC_GIA_HAN_THANH_KHOAN = "6021";
        public const string GC_TOKHAI_CHUYENTIEP_XUAT = "986";
        public const string GC_TOKHAI_CHUYENTIEP_NHAP= "968";

        public const string TOKHAI_DIENTU_DON_GIAN = "939";
        public const string TOKHAI_DIENTU_THANG = "940";
        public const string DNCX_TOKHAI_VAO = "931";//"976";
        public const string DNCX_TOKHAI_RA = "932";//"975";


        public const string SXXK_NPL = "100";
        public const string SXXK_SP = "101";
        public const string SXXK_DINH_MUC = "102";
        public const string HO_SO_THANH_KHOAN = "103";
        public const string HO_SO_THANH_KHOAN_FULL = "104";
        public const string HOP_DONG_GIA_CONG = "601";
        public const string GC_PHU_KIEN_HOP_DONG = "602";
        public const string GC_DINH_MUC = "603";
        public const string GC_YEU_CAU_THANH_KHOAN_HD = "605";
        public const string GC_DENGHI_GIAMSAT_TIEU_HUY = "609";
        //Doanh nghiệp chế xuất dnh mục hàng hóa đưa vào
        public const string DNCX_HANG_VAO = "501";
        public const string DNCX_HANG_RA = "502";
        public const string DNCX_DINHMUC_SANPHAM = "508";
        public const string DNCX_BAOCAO_HANG_TKHO = "510";
        public const string QDINH_KTRA_THUC_TE_HANG_TON_KHO = "511";
        public const string DNCX_HUY = "512";
        public const string THANHLY_TSCD = "513";
        //511	Quyết định kiểm tra thực tế hàng tồn kho
        //512	Thông tin huỷ nguyên liệu, sản phẩm, phế liệu, phế phẩm
        //513	Thông tin thanh lý tài sản cố định


    }
    public struct AdditionalDocumentType
    {
        public const string HOP_DONG = "315";
        public const string HOP_DONG_NHAN = "316"; //Giành cho GC chuyển tiếp
        public const string HOP_DONG_GIAO = "315"; // Giành cho GC chuyển tiếp
        public const string CHUNG_TU_NO = "999";//FPT 937 ThaiSon 999
        public const string HOA_DON_THUONG_MAI = "380";
        public const string MASTER_BILL_OF_LADING = "704";
        public const string BILL_OF_LADING_ORIGIN = "706";
        public const string BILL_OF_LADING_COPPY = "707";
        public const string EMPTY_CONTAINER_BILL = "708";
        public const string TANKER_BILL_OF_LADING = "709";
        public const string SEA_WAYBILL = "710";
        public const string HOUSE_BILL_OF_LADING = "714";
        public const string AIR_WAYBILL = "740";
        public const string MASTER_AIR_WAYBILL = "741";
        public const string SUBSTITUTE_AIR_WAYBILL = "743";
        //
        public const string PACKING_LIST = "750";
        public const string EXPORT_LICENCE = "811";
        public const string EXPORT_CO_FORM_D = "861";
        public const string EXPORT_CO_FORM_A = "865";
        public const string IMPORT_LICENCE = "911";
        public const string IMPORT_CO_FORM_D = "007";
        public const string IMPORT_CO_FORM_A = "008";
        public const string GIAY_DKKTCL_KTVSATTP_KDDTV = "400";
        public const string GIAY_TBKQKTCL_KTVSATTP_KDDTV = "401";
        public const string GIAY_TBAO_KQKT = "402";
        public const string CHUNG_THU_GIAM_DINH = "403";
        public const string GIAY_NOP_TIEN = "404";
        public const string DE_NGHI_CHUYEN_CUA_KHAU = "405";
        public const string CHUNG_TU_DANG_ANH = "200";

        public const string GIAY_DANG_KY_KT = "200";
        public const string GIAY_KET_QUA_KT = "201";

    }
    public struct CustomsValuationMethod
    {
        public const string TKTG_PP1 = "1";
        public const string TKTG_PP2 = "2";
        public const string TKTG_PP3 = "3";
        public const string TKTG_PP4 = "4";
        public const string TKTG_PP5 = "5";
        public const string TKTG_PP6 = "6";
    }
    public struct AgentsStatus
    {
        public const string DAILY_LAM_THUTUC_HAIQUAN = "1";
        public const string UYTHAC = "2";
        public const string NGUOIKHAI_HAIQUAN = "3";
        public const string NGUOICHIU_TRACHNHIEM_NOPTHUE = "4";
        public const string MA_MID = "5";
    }
    public struct DutyTaxFeeType
    {
        public const string THUE_XNK = "1";
        public const string THUE_VAT = "2";
        public const string THUE_TIEU_THU_DAT_BIET = "3";
        public const string THUE_KHAC = "5";
        public const string THUE_CHENH_LECH_GIA = "4";
        public const string THUE_BAO_VE_MOI_TRUONG = "6";
        /// <summary>
        /// Tu ve chong ban pha gia
        /// </summary>
        public const string THUE_CHONG_BAN_PHA_GIA = "7";
    }
    public struct AdditionalInformationStatement
    {
        #region tokhai tri gia 1
        public const string NGAY_XK = "101";
        public const string QUYEN_SD = "102";
        public const string KHONG_XD = "103";
        public const string TRA_THEM = "104";
        public const string TIEN_TRA_16 = "105";
        public const string CO_QHDB = "106";
        public const string KIEU_QHDB = "107";
        public const string ANH_HUONG_QH = "108";
        public const string TO_SO = "109";
        #endregion

        #region to khai tri gia 2,3
        public const string LYDO_KAD_PP1 = "01";
        public const string NGAY_XK23 = "02";
        public const string STTHANG_TT = "03";
        public const string SOTK_TT = "04";
        public const string NGAY_NK_TT = "05";
        public const string MA_HQ_TT = "06";
        public const string NGAY_XK_TT = "07";
        public const string GIAI_TRINH = "08";
        public const string Ma_LH = "09";
        public const string HANG_TUONG_TU = "10";
        #endregion
        public const string LUONG_XANH = "206";
        public const string LUONG_VANG = "207";
        public const string LUONG_DO = "208";
        public const string TAI_KHOAN_KHO_BAC = "209";
        public const string TEN_KHO_BAC = "210";



    }
    public struct ValuationAdjustmentAddition
    {
        //Dành cho tờ khai trị giá 1
        public const string Gia_hoa_don = "1001";
        public const string Thanh_toan_gian_tiep = "1002";
        public const string Tien_tra_truoc = "1003";
        public const string Phi_hoa_hong = "1004";
        public const string Phi_bao_bi = "1005";
        public const string Phi_dong_goi = "1006";
        public const string Khoan_tro_giup = "1007";
        public const string Tro_giup_NVL = "1008";
        public const string Tro_giup_NL = "1009";
        public const string Tro_giup_cong_cu = "1010";
        public const string Tro_giup_thiet_ke = "1011";
        public const string Tien_ban_quyen = "1012";
        public const string Tien_phai_tra = "1013";
        public const string Phi_van_tai = "1014";
        public const string Phi_bao_hiem = "1015";
        public const string Phi_VT_BH_noi_dia = "1101";
        public const string Phi_phat_sinh = "1102";
        public const string Tien_lai = "1103";
        public const string Thue_phi_le_phi = "1104";
        public const string Khoan_Giam_Gia = "1105";

        //Dành cho tờ khai trị giá 2,3        
        public const string Tri_gia_hang_TT = "001";
        public const string DCC_cap_do_TM = "002";
        public const string DCC_so_luong = "003";
        public const string DCC_khoan_khac = "004";
        public const string DCC_phi_van_tai = "005";
        public const string DCC_phi_bao_hiem = "006";

        //Dành cho tờ khai trị giá 3        
        //public const string Tri_gia_hang_TT = "3001";
        //public const string DCC_cap_do_TM = "3002";
        //public const string DCC_so_luong = "3003";
        //public const string DCC_khoan_khac = "3004";
        //public const string DCC_phi_van_tai = "3005";
        //public const string DCC_phi_bao_hiem = "3006";

        //Dành cho tờ khai trị giá 4
        public const string Gia_ban_VN = "4001";
        public const string Tien_hoa_hong = "4101";
        public const string Loi_nhuan = "4102";
        public const string Phi_VT_boc_xep = "4103";
        public const string Phi_BH_noi_dia = "4104";
        public const string LePhi_PhiThue_KhauTru = "4105";

        //Dành cho tờ khai trị giá 6        
        public const string Tri_Gia_TT_Da_Xac_Dinh = "6001";

    }


}
