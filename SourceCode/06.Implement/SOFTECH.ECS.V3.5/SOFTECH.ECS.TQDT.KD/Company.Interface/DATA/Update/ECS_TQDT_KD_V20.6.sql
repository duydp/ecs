GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO

DELETE  FROM t_VNACC_Category_Common WHERE ReferenceDB='A520'

-------------- BẢNG MÃ ĐỐI TƯỢNG KHÔNG CHỊU THUẾ XUẤT KHẨU, THUẾ NHẬP KHẨU DÙNG TRONG VNACCS-------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK10')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK10' ,N'Hàng vận chuyển quá cảnh, chuyển khẩu')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK10' ,Name_VN = N'Hàng vận chuyển quá cảnh, chuyển khẩu' WHERE ReferenceDB='A520' AND Code='XNK10'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK20')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK20' ,N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK20' ,Name_VN = N'Hàng viện trợ nhân đạo, viện trợ không hoàn lại' WHERE ReferenceDB='A520' AND Code='XNK20'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK31')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK31' ,N'Hàng từ khu PTQ XK ra nước ngoài')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK31' ,Name_VN = N'Hàng từ khu PTQ XK ra nước ngoài' WHERE ReferenceDB='A520' AND Code='XNK31'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK32')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK32' ,N'Hàng NK từ nước ngoài vào khu PTQ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK32' ,Name_VN = N'Hàng NK từ nước ngoài vào khu PTQ' WHERE ReferenceDB='A520' AND Code='XNK32'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK33')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK33' ,N'Hàng từ khu PTQ này sang khu PTQ khác')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK33' ,Name_VN = N'Hàng từ khu PTQ này sang khu PTQ khác' WHERE ReferenceDB='A520' AND Code='XNK33'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK40')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK40' ,N'Hàng XK là phần dầu khí thuộc thuế tài nguyên của NN')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK40' ,Name_VN = N'Hàng XK là phần dầu khí thuộc thuế tài nguyên của NN' WHERE ReferenceDB='A520' AND Code='XNK40'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNG81')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNG81' ,N'Hàng NK để gia công cho nước ngoài (đối tượng miễn thuế NK)')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNG81' ,Name_VN = N'Hàng NK để gia công cho nước ngoài (đối tượng miễn thuế NK)' WHERE ReferenceDB='A520' AND Code='XNG81'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNG82')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNG82' ,N'Sản phẩm gia công xuất trả cho phía nước ngoài (đối tượng miễn thuế XK)')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNG82' ,Name_VN = N'Sản phẩm gia công xuất trả cho phía nước ngoài (đối tượng miễn thuế XK)' WHERE ReferenceDB='A520' AND Code='XNG82'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNG83')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNG83' ,N'Hàng XK để gia công cho Việt Nam (đối tượng miễn thuế XK)')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNG83' ,Name_VN = N'Hàng XK để gia công cho Việt Nam (đối tượng miễn thuế XK)' WHERE ReferenceDB='A520' AND Code='XNG83'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNG84')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNG84' ,N'Sản phẩm gia công nhập trả Việt Nam (đối tượng miễn thuế NK)')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNG84' ,Name_VN = N'Sản phẩm gia công nhập trả Việt Nam (đối tượng miễn thuế NK)' WHERE ReferenceDB='A520' AND Code='XNG84'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XNK90')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XNK90' ,N'Hàng hóa khác')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XNK90' ,Name_VN = N'Hàng hóa khác' WHERE ReferenceDB='A520' AND Code='XNK90'
END
    
END

-------------- BẢNG MÃ ĐỐI TƯỢNG GIẢM THUẾ DÙNG TRONG VNACCS -------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XG' ,N'Hàng hóa được giảm thuế xuất khẩu ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XG' ,Name_VN = N'Hàng hóa được giảm thuế xuất khẩu ' WHERE ReferenceDB='A520' AND Code='XG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='NG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'NG' ,N'Hàng hóa được giảm thuế nhập khẩu ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='NG' ,Name_VN = N'Hàng hóa được giảm thuế nhập khẩu ' WHERE ReferenceDB='A520' AND Code='NG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='TG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'TG' ,N'Hàng hóa được giảm thuế tiêu thụ đặc biệt')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='TG' ,Name_VN = N'Hàng hóa được giảm thuế tiêu thụ đặc biệt' WHERE ReferenceDB='A520' AND Code='TG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='MG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'MG' ,N'Hàng hóa được giảm thuế bảo vệ môi trường ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='MG' ,Name_VN = N'Hàng hóa được giảm thuế bảo vệ môi trường ' WHERE ReferenceDB='A520' AND Code='MG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='VG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'VG' ,N'Hàng hóa được giảm thuế giá trị gia tăng')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='VG' ,Name_VN = N'Hàng hóa được giảm thuế giá trị gia tăng' WHERE ReferenceDB='A520' AND Code='VG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='BG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'BG' ,N'Hàng hóa được giảm thuế tự vệ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='BG' ,Name_VN = N'Hàng hóa được giảm thuế tự vệ' WHERE ReferenceDB='A520' AND Code='BG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='GG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'GG' ,N'Hàng hóa được giảm thuế chống bán phá giá')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='GG' ,Name_VN = N'Hàng hóa được giảm thuế chống bán phá giá' WHERE ReferenceDB='A520' AND Code='GG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='CG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'CG' ,N'Hàng hóa được giảm thuế chống trợ cấp')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='CG' ,Name_VN = N'Hàng hóa được giảm thuế chống trợ cấp' WHERE ReferenceDB='A520' AND Code='CG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='PG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'PG' ,N'Hàng hóa được giảm thuế chống phân biệt đối xử')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='PG' ,Name_VN = N'Hàng hóa được giảm thuế chống phân biệt đối xử' WHERE ReferenceDB='A520' AND Code='PG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='DG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'DG' ,N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='DG' ,Name_VN = N'Mã dự trữ' WHERE ReferenceDB='A520' AND Code='DG'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='EG')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'EG' ,N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='EG' ,Name_VN = N'Mã dự trữ' WHERE ReferenceDB='A520' AND Code='EG'
END    
END

-------------- BẢNG MÃ MIỄN THUẾ XUẤT KHẨU, THUẾ NHẬP KHẨU DÙNG TRONG VNACCS -------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN011')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN011' ,N'Hàng TNTX, TXTN tham dự hội chợ triển lãm')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN011' ,Name_VN = N'Hàng TNTX, TXTN tham dự hội chợ triển lãm' WHERE ReferenceDB='A520' AND Code='XN011'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN012')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN012' ,N'Hàng TNTX, TXTN phục vụ công việc trong thời hạn nhất định')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN012' ,Name_VN = N'Hàng TNTX, TXTN phục vụ công việc trong thời hạn nhất định' WHERE ReferenceDB='A520' AND Code='XN012'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN013')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN013' ,N'Hàng TNTX, TXTN phục vụ GC cho thương nhân nước ngoài')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN013' ,Name_VN = N'Hàng TNTX, TXTN phục vụ GC cho thương nhân nước ngoài' WHERE ReferenceDB='A520' AND Code='XN013'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN014')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN014' ,N'Hàng TNTX, TXTN cho thể thao, văn hóa, nghệ thuật, khác ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN014' ,Name_VN = N'Hàng TNTX, TXTN cho thể thao, văn hóa, nghệ thuật, khác ' WHERE ReferenceDB='A520' AND Code='XN014'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN015')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN015' ,N'Hàng TNTX, TXTN để thử nghiệm, nghiên cứu phát triển sản phẩm')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN015' ,Name_VN = N'Hàng TNTX, TXTN để thử nghiệm, nghiên cứu phát triển sản phẩm' WHERE ReferenceDB='A520' AND Code='XN015'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN016')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN016' ,N'Hàng TNTX, TXTN thay thế sửa chữa, cung ứng tàu biển tàu bay')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN016' ,Name_VN = N'Hàng TNTX, TXTN thay thế sửa chữa, cung ứng tàu biển tàu bay' WHERE ReferenceDB='A520' AND Code='XN016'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN017')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN017' ,N'Hàng TNTX, TXTN để bảo hành, sửa chữa, thay thế')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN017' ,Name_VN = N'Hàng TNTX, TXTN để bảo hành, sửa chữa, thay thế' WHERE ReferenceDB='A520' AND Code='XN017'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN018')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN018' ,N'Hàng TNTX, TXTN quay vòng')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN018' ,Name_VN = N'Hàng TNTX, TXTN quay vòng' WHERE ReferenceDB='A520' AND Code='XN018'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN019')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN019' ,N'Hàng kinh doanh TNTX, TXTN trong thời hạn')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN019' ,Name_VN = N'Hàng kinh doanh TNTX, TXTN trong thời hạn' WHERE ReferenceDB='A520' AND Code='XN019'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN020')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN020' ,N'Tài sản di chuyển')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN020' ,Name_VN = N'Tài sản di chuyển' WHERE ReferenceDB='A520' AND Code='XN020'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN021')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN021' ,N'Quà biếu, quà tặng')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN021' ,Name_VN = N'Quà biếu, quà tặng' WHERE ReferenceDB='A520' AND Code='XN021'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN030')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN030' ,N'Hàng của đối tượng được ưu đãi miễn trừ ngoại giao')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN030' ,Name_VN = N'Hàng của đối tượng được ưu đãi miễn trừ ngoại giao' WHERE ReferenceDB='A520' AND Code='XN030'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN050')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN050' ,N'Hàng gửi qua dịch vụ chuyển phát nhanh')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN050' ,Name_VN = N'Hàng gửi qua dịch vụ chuyển phát nhanh' WHERE ReferenceDB='A520' AND Code='XN050'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN061')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN061' ,N'Hàng NK tạo TSCĐ của dự án đầu tư vào lĩnh vực được ưu đãi')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN061' ,Name_VN = N'Hàng NK tạo TSCĐ của dự án đầu tư vào lĩnh vực được ưu đãi' WHERE ReferenceDB='A520' AND Code='XN061'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN062')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN062' ,N'Hàng NK tạo TSCĐ của dự án đầu tư vào địa bàn được ưu đãi')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN062' ,Name_VN = N'Hàng NK tạo TSCĐ của dự án đầu tư vào địa bàn được ưu đãi' WHERE ReferenceDB='A520' AND Code='XN062'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN063')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN063' ,N'Hàng NK tạo TSCĐ của dự án đầu tư bằng nguồn vốn ODA')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN063' ,Name_VN = N'Hàng NK tạo TSCĐ của dự án đầu tư bằng nguồn vốn ODA' WHERE ReferenceDB='A520' AND Code='XN063'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN064')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN064' ,N'Hàng NK tạo TSCĐ của dự án quy mô từ 6.000 tỷ đồng trở lên')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN064' ,Name_VN = N'Hàng NK tạo TSCĐ của dự án quy mô từ 6.000 tỷ đồng trở lên' WHERE ReferenceDB='A520' AND Code='XN064'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN065')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN065' ,N'Hàng NK tạo TSCĐ DA tại nông thôn SD từ 500 lao động trở lên')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN065' ,Name_VN = N'Hàng NK tạo TSCĐ DA tại nông thôn SD từ 500 lao động trở lên' WHERE ReferenceDB='A520' AND Code='XN065'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN066')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN066' ,N'Hàng NK của DN CNC, DN KH&CN, TC KH&CN')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN066' ,Name_VN = N'Hàng NK của DN CNC, DN KH&CN, TC KH&CN' WHERE ReferenceDB='A520' AND Code='XN066'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN070')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN070' ,N'Giống cây trồng, vật nuôi cho dự án nông, lâm, ngư nghiệp.')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN070' ,Name_VN = N'Giống cây trồng, vật nuôi cho dự án nông, lâm, ngư nghiệp.' WHERE ReferenceDB='A520' AND Code='XN070'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN071')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN071' ,N'Giống cây trồng, giống vật nuôi; phân bón, thuốc BVTV')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN071' ,Name_VN = N'Giống cây trồng, giống vật nuôi; phân bón, thuốc BVTV' WHERE ReferenceDB='A520' AND Code='XN071'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN081')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN081' ,N'Hàng NK cho dự án đầu tư mở rộng thuộc lĩnh vực ưu đãi')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN081' ,Name_VN = N'Hàng NK cho dự án đầu tư mở rộng thuộc lĩnh vực ưu đãi' WHERE ReferenceDB='A520' AND Code='XN081'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN082')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN082' ,N'Hàng NK cho dự án đầu tư mở rộng thuộc địa bàn ưu đãi')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN082' ,Name_VN = N'Hàng NK cho dự án đầu tư mở rộng thuộc địa bàn ưu đãi' WHERE ReferenceDB='A520' AND Code='XN082'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN083')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN083' ,N'Hàng NK tạo TSCĐ của dự án ODA mở rộng')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN083' ,Name_VN = N'Hàng NK tạo TSCĐ của dự án ODA mở rộng' WHERE ReferenceDB='A520' AND Code='XN083'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN084')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN084' ,N'Hàng NK cho dự án nông, lâm, ngư nghiệp mở rộng')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN084' ,Name_VN = N'Hàng NK cho dự án nông, lâm, ngư nghiệp mở rộng' WHERE ReferenceDB='A520' AND Code='XN084'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN085')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN085' ,N'Hàng NK tạo TSCĐ của dự án mở rộng quy mô từ 6.000 tỷ đồng trở lên ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN085' ,Name_VN = N'Hàng NK tạo TSCĐ của dự án mở rộng quy mô từ 6.000 tỷ đồng trở lên ' WHERE ReferenceDB='A520' AND Code='XN085'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN086')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN086' ,N'Hàng NK DA mở rộng tại nông thôn SD từ 500 lao động trở lên ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN086' ,Name_VN = N'Hàng NK DA mở rộng tại nông thôn SD từ 500 lao động trở lên ' WHERE ReferenceDB='A520' AND Code='XN086'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN090')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN090' ,N'Hàng NK được miễn thuế lần đầu')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN090' ,Name_VN = N'Hàng NK được miễn thuế lần đầu' WHERE ReferenceDB='A520' AND Code='XN090'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN100')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN100' ,N'Hàng NK phục vụ hoạt động dầu khí')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN100' ,Name_VN = N'Hàng NK phục vụ hoạt động dầu khí' WHERE ReferenceDB='A520' AND Code='XN100'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN111')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN111' ,N'Hàng NK của cơ sở đóng tàu')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN111' ,Name_VN = N'Hàng NK của cơ sở đóng tàu' WHERE ReferenceDB='A520' AND Code='XN111'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN112')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN112' ,N'Sản phẩm tàu biển XK của cơ sở đóng tàu')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN112' ,Name_VN = N'Sản phẩm tàu biển XK của cơ sở đóng tàu' WHERE ReferenceDB='A520' AND Code='XN112'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN120')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN120' ,N'Hàng NK cho SX sản phẩm CNTT, nội dung số, phần mềm')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN120' ,Name_VN = N'Hàng NK cho SX sản phẩm CNTT, nội dung số, phần mềm' WHERE ReferenceDB='A520' AND Code='XN120'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN130')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN130' ,N'Hàng NK cho hoạt động NCKH và phát triển công nghệ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN130' ,Name_VN = N'Hàng NK cho hoạt động NCKH và phát triển công nghệ' WHERE ReferenceDB='A520' AND Code='XN130'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN131')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN131' ,N'Hàng NK cho ươm tạo CN, ươm tạo DN KH&CN, đổi mới CN')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN131' ,Name_VN = N'Hàng NK cho ươm tạo CN, ươm tạo DN KH&CN, đổi mới CN' WHERE ReferenceDB='A520' AND Code='XN131'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN140')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN140' ,N'Hàng NK để sản xuất của dự án đầu tư được miễn thuế 05 năm')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN140' ,Name_VN = N'Hàng NK để sản xuất của dự án đầu tư được miễn thuế 05 năm' WHERE ReferenceDB='A520' AND Code='XN140'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN141')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN141' ,N'Hàng NK của DN CNC, DN KH&CN, TC KH&CN được miễn thuế 05 năm')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN141' ,Name_VN = N'Hàng NK của DN CNC, DN KH&CN, TC KH&CN được miễn thuế 05 năm' WHERE ReferenceDB='A520' AND Code='XN141'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN150')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN150' ,N'Hàng NK được sản xuất, gia công, tái chế, lắp ráp tại khu PTQ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN150' ,Name_VN = N'Hàng NK được sản xuất, gia công, tái chế, lắp ráp tại khu PTQ' WHERE ReferenceDB='A520' AND Code='XN150'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN160')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN160' ,N'Hàng TNTX để thực hiện dự án ODA')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN160' ,Name_VN = N'Hàng TNTX để thực hiện dự án ODA' WHERE ReferenceDB='A520' AND Code='XN160'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN170')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN170' ,N'Hàng NK để sản xuất của dự án đầu tư theo QĐ 33/2009/QĐ-TTg')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN170' ,Name_VN = N'Hàng NK để sản xuất của dự án đầu tư theo QĐ 33/2009/QĐ-TTg' WHERE ReferenceDB='A520' AND Code='XN170'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN180')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN180' ,N'Hàng NK để bán tại cửa hàng miễn thuế')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN180' ,Name_VN = N'Hàng NK để bán tại cửa hàng miễn thuế' WHERE ReferenceDB='A520' AND Code='XN180'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN190')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN190' ,N'Hàng hóa theo Điều ước quốc tế')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN190' ,Name_VN = N'Hàng hóa theo Điều ước quốc tế' WHERE ReferenceDB='A520' AND Code='XN190'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN200')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN200' ,N'Hàng hóa có trị giá hoặc số tiền thuế dưới mức tối thiểu.')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN200' ,Name_VN = N'Hàng hóa có trị giá hoặc số tiền thuế dưới mức tối thiểu.' WHERE ReferenceDB='A520' AND Code='XN200'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN210')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN210' ,N'Hàng hóa nhập khẩu để sản xuất hàng hóa xuất khẩu')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN210' ,Name_VN = N'Hàng hóa nhập khẩu để sản xuất hàng hóa xuất khẩu' WHERE ReferenceDB='A520' AND Code='XN210'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN220')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN220' ,N'Hàng mẫu, ảnh phim, mô hình thay thế hàng mẫu, ấn phẩm QC')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN220' ,Name_VN = N'Hàng mẫu, ảnh phim, mô hình thay thế hàng mẫu, ấn phẩm QC' WHERE ReferenceDB='A520' AND Code='XN220'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN230')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN230' ,N'Hàng hóa NK để SX, lắp ráp trang thiết bị y tế')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN230' ,Name_VN = N'Hàng hóa NK để SX, lắp ráp trang thiết bị y tế' WHERE ReferenceDB='A520' AND Code='XN230'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN240')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN240' ,N'Hàng hóa NK phục vụ in, đúc tiền')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN240' ,Name_VN = N'Hàng hóa NK phục vụ in, đúc tiền' WHERE ReferenceDB='A520' AND Code='XN240'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN251')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN251' ,N' Hàng hóa XK để bảo vệ môi trường')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN251' ,Name_VN = N' Hàng hóa XK để bảo vệ môi trường' WHERE ReferenceDB='A520' AND Code='XN251'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN252')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN252' ,N' Hàng hóa NK để bảo vệ môi trường')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN252' ,Name_VN = N' Hàng hóa NK để bảo vệ môi trường' WHERE ReferenceDB='A520' AND Code='XN252'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN260')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN260' ,N'Hàng hóa NK phục vụ trực tiếp cho giáo dục')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN260' ,Name_VN = N'Hàng hóa NK phục vụ trực tiếp cho giáo dục' WHERE ReferenceDB='A520' AND Code='XN260'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN500')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN500' ,N'Hàng miễn thuế theo K20, Đ12, NĐ 87/2010/NĐ-CP')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN500' ,Name_VN = N'Hàng miễn thuế theo K20, Đ12, NĐ 87/2010/NĐ-CP' WHERE ReferenceDB='A520' AND Code='XN500'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN501')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN501' ,N'Hàng miễn thuế theo K23, Đ16, Luật 107/2016/QH13')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN501' ,Name_VN = N'Hàng miễn thuế theo K23, Đ16, Luật 107/2016/QH13' WHERE ReferenceDB='A520' AND Code='XN501'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN600')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN600' ,N'Vật liệu xây dựng đưa vào khu PTQ theo TT 11/2012/TT-BTC ')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN600' ,Name_VN = N'Vật liệu xây dựng đưa vào khu PTQ theo TT 11/2012/TT-BTC ' WHERE ReferenceDB='A520' AND Code='XN600'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN710')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN710' ,N'Hàng nông sản NK do VN hỗ trợ đầu tư, trồng tại Campuchia')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN710' ,Name_VN = N'Hàng nông sản NK do VN hỗ trợ đầu tư, trồng tại Campuchia' WHERE ReferenceDB='A520' AND Code='XN710'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN900')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN900' ,N'Hàng NK, XK khác')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN900' ,Name_VN = N'Hàng NK, XK khác' WHERE ReferenceDB='A520' AND Code='XN900'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN910')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN910' ,N'Hàng NK, XK khác')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN910' ,Name_VN = N'Hàng NK, XK khác' WHERE ReferenceDB='A520' AND Code='XN910'
END
IF NOT EXISTS (SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB='A520' AND Code='XN920')
BEGIN
    INSERT INTO t_VNACC_Category_Common( ReferenceDB,Code,Name_VN) VALUES  ( 'A520' ,'XN920' ,N'Hàng hóa NK theo tổ hợp, dây chuyền')
END
ELSE
BEGIN
    UPDATE t_VNACC_Category_Common SET Code='XN920' ,Name_VN = N'Hàng hóa NK theo tổ hợp, dây chuyền' WHERE ReferenceDB='A520' AND Code='XN920'
END    
END

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.6') = 0
BEGIN
	INSERT INTO t_HaiQuan_Version VALUES('20.6',GETDATE(), N'BẢNG MÃ ĐỐI TƯỢNG KHÔNG CHỊU THUẾ XUẤT KHẨU, THUẾ NHẬP KHẨU DÙNG TRONG VNACCS')
END