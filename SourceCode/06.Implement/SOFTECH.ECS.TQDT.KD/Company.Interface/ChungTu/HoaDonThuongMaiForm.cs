﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Janus.Windows.GridEX;
using DevExpress.XtraCharts;

using Company.Interface.Report;
using Company.Interface.Report.SXXK;

using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;

using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class HoaDonThuongMaiForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public HoaDonThuongMai HDonTM = new HoaDonThuongMai();
        public List<HoaDonThuongMaiDetail> listHDTMDetail = new List<HoaDonThuongMaiDetail>();
        public bool isKhaiBoSung = false;
        public HoaDonThuongMaiForm()
        {
            InitializeComponent();
        }



        private void HoaDonThuongMaiForm_Load(object sender, EventArgs e)
        {
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            //cbPTTT.SelectedValue = TKMD.PTTT_ID;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            //cbDKGH.SelectedValue = TKMD.DKGH_ID;

            if (HDonTM != null && HDonTM.ID <= 0)
            {
                //DATLMQ bổ sung lấy HĐơn từ TKMD 13/12/2010
                HDonTM.NgayHoaDon = TKMD.NgayHoaDonThuongMai;
                ccNgayVanDon.Value = HDonTM.NgayHoaDon;
                HDonTM.SoHoaDon = TKMD.SoHoaDonThuongMai;
                txtSoVanDon.Text = HDonTM.SoHoaDon;
                if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    txtMaDVMua.Text = TKMD.MaDoanhNghiep;
                    txtTenDVMua.Text = TKMD.TenDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDonViDoiTac;
                }
                else
                {
                    txtTenDVMua.Text = TKMD.TenDonViDoiTac;
                    txtMaDVBan.Text = TKMD.MaDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDoanhNghiep;
                }
            }
            else if (HDonTM != null && HDonTM.ID > 0)
            {
                cbDKGH.SelectedValue = HDonTM.DKGH_ID;
                txtMaDVBan.Text = HDonTM.MaDonViBan;
                txtMaDVMua.Text = HDonTM.MaDonViMua;
                ccNgayVanDon.Value = HDonTM.NgayHoaDon;
                nguyenTeControl2.Ma = HDonTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HDonTM.PTTT_ID;
                txtSoVanDon.Text = HDonTM.SoHoaDon;
                txtTenDVBan.Text = HDonTM.TenDonViBan;
                txtTenDVMua.Text = HDonTM.TenDonViMua;
                txtThongTinKhac.Text = HDonTM.ThongTinKhac;
                BindData();
            }
            if (HDonTM.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = HDonTM.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = HDonTM.SoTiepNhan + "";
            }
            SetButtonStateHOADON(TKMD, isKhaiBoSung, HDonTM);
        }
        private void BindData()
        {
            Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            hmd.TKMD_ID = TKMD.ID;
            DataTable dt = hmd.SelectBy_TKMD_ID().Tables[0];
            dgList.DataSource = HDonTM.ConvertListToDataSet(dt);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private bool checkSoHoaDon(string soHoaDon)
        {
            foreach (HoaDonThuongMai HDTM in TKMD.HoaDonThuongMaiCollection)
            {
                if (HDTM.SoHoaDon.Trim().ToUpper() == soHoaDon.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HDonTM.ListHangMDOfHoaDon.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }


            TKMD.HoaDonThuongMaiCollection.Remove(HDonTM);

            if (checkSoHoaDon(txtSoVanDon.Text.Trim()))
            {
                if (HDonTM.ID > 0)
                    TKMD.HoaDonThuongMaiCollection.Add(HDonTM);
                ShowMessage("Số hóa đơn này đã tồn tại.", false);
                return;
            }
            HDonTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HDonTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HDonTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HDonTM.NgayHoaDon = ccNgayVanDon.Value;
            HDonTM.NguyenTe_ID = nguyenTeControl2.Ma;
            HDonTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HDonTM.SoHoaDon = txtSoVanDon.Text.Trim();
            HDonTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HDonTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HDonTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HDonTM.TKMD_ID = TKMD.ID;
            HDonTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (isKhaiBoSung)
                HDonTM.LoaiKB = 1;
            else
                HDonTM.LoaiKB = 0;

            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HoaDonThuongMaiDetail item in HDonTM.ListHangMDOfHoaDon)
                {
                    if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        item.GhiChu = row.Cells["GhiChu"].Text;
                        item.GiaiTriDieuChinhGiam = Convert.ToDouble(row.Cells["GiaiTriDieuChinhGiam"].Value);
                        item.GiaTriDieuChinhTang = Convert.ToDouble(row.Cells["GiaTriDieuChinhTang"].Text);

                        break;
                    }
                }
            }
            try
            {
                if (string.IsNullOrEmpty(HDonTM.GuidStr)) HDonTM.GuidStr = Guid.NewGuid().ToString();
                HDonTM.InsertUpdateFull();
                TKMD.HoaDonThuongMaiCollection.Add(HDonTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public HangMauDichCollection ConvertListToHangMauDichCollection(List<HoaDonThuongMaiDetail> listHangHoaDon, HangMauDichCollection hangCollection)
        {

            HangMauDichCollection tmp = new HangMauDichCollection();

            foreach (HoaDonThuongMaiDetail item in listHangHoaDon)
            {
                foreach (Company.KD.BLL.KDT.HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            /*
            if (HDonTM.ListHangMDOfHoaDon.Count > 0)
            {
                f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HDonTM.ListHangMDOfHoaDon, TKMD.HMDCollection);
            }*/
            f.ShowDialog();
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (Company.KD.BLL.KDT.HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HoaDonThuongMaiDetail hangHoaDonDetail in HDonTM.ListHangMDOfHoaDon)
                    {
                        if (hangHoaDonDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HoaDonThuongMaiDetail hoaDonDetail = new HoaDonThuongMaiDetail();
                        hoaDonDetail.HMD_ID = HMD.ID;
                        hoaDonDetail.HoaDonTM_ID = HDonTM.ID;
                        hoaDonDetail.HMD_ID = HMD.ID;
                        hoaDonDetail.HoaDonTM_ID = HDonTM.ID;
                        hoaDonDetail.MaPhu = HMD.MaPhu;
                        hoaDonDetail.MaHS = HMD.MaHS;
                        hoaDonDetail.TenHang = HMD.TenHang;
                        hoaDonDetail.DVT_ID = HMD.DVT_ID;
                        hoaDonDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hoaDonDetail.SoLuong = HMD.SoLuong;
                        hoaDonDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hoaDonDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hoaDonDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);

                        HDonTM.ListHangMDOfHoaDon.Add(hoaDonDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HDonTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = GlobalSettings.PassWordDT;
            WSForm wsForm = new WSForm();
            try
            {
                if (password == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                    password = wsForm.txtMatKhau.Text;
                }

                bool thanhcong = HDonTM.WSKhaiBaoHoaDonTM(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, listHDTMDetail, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, TKMD.ConvertHMDKDToHangMauDich());

                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.HDonTM.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.HDonTM.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HDonTM.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.HDonTM.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.HDonTM.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.HDonTM.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HDonTM.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHoaDonTMDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOA DON.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOADON(Company.KD.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon)
        {
            if (hoadon == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY);

                btnXoa.Enabled = status;
                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnChonHang.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = true;
                    //Neu hop dong chua co so tiep nhan -> phai khai bao
                    if (hoadon.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        btnLayPhanHoi.Enabled = true;
                    }


                }
            }

            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HDonTM.GuidStr != null && HDonTM.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(HDonTM.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
    }
}