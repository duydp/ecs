
IF OBJECT_ID('v_KDT_SXXK_NPLXuatTonOver9') IS NOT NULL
DROP VIEW v_KDT_SXXK_NPLXuatTonOver9
GO
IF OBJECT_ID('p_KDT_SXXK_DanhSachNPLXuatTonOver9') IS NOT NULL
DROP PROCEDURE p_KDT_SXXK_DanhSachNPLXuatTonOver9
GO
CREATE VIEW [dbo].[v_KDT_SXXK_NPLXuatTonOver9]
AS
		 SELECT TOP ( 100 ) PERCENT
        CASE WHEN a.SoToKhai <> 0
             THEN ( SELECT TOP 1
                            tkmd.SoTKVNACCS
                    FROM    dbo.t_VNACCS_CapSoToKhai tkmd
                    WHERE   tkmd.SoTK = a.SoToKhai
                            AND tkmd.NamDangKy = a.NamDangKy
                  )
             ELSE a.SoToKhai
        END AS SoToKhaiVNACCS ,
			a.SoToKhai,
            a.MaLoaiHinh ,
            a.NamDangKy ,
            a.MaHaiQuan ,
            c.MaSanPham AS MaSP ,
            b.TenHang AS TenSP ,
            e.Ten AS TenDVT_SP ,
            b.SoLuong AS LuongSP ,
            c.MaNguyenPhuLieu AS MaNPL ,
            c.DinhMucChung AS DinhMuc ,
            b.SoLuong * c.DinhMucChung AS LuongNPL ,
            b.SoLuong * c.DinhMucChung AS TonNPL ,
            a.BangKeHoSoThanhLy_ID ,
            CASE WHEN YEAR(d.NGAY_THN_THX) > 1900 THEN d.NGAY_THN_THX
                 ELSE d.NgayDangKy
            END AS NgayThucXuat ,
            CASE WHEN YEAR(d.NgayHoanThanh) > 1900 THEN d.NgayHoanThanh
                 ELSE d.NgayDangKy
            END AS NgayHoanThanhXuat ,
            d.NgayHoanThanh ,
            a.NgayDangKy ,
            b.MaHaiQuan AS Expr1 ,
			c.LenhSanXuat_ID
    FROM    dbo.t_KDT_SXXK_BKToKhaiXuat AS a
            INNER JOIN dbo.t_SXXK_ToKhaiMauDich AS d ON a.SoToKhai = d.SoToKhai
                                                        AND a.MaLoaiHinh = d.MaLoaiHinh
                                                        AND a.NamDangKy = d.NamDangKy
                                                        AND a.MaHaiQuan = d.MaHaiQuan
            INNER JOIN dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai
                                                      AND a.MaLoaiHinh = b.MaLoaiHinh
                                                      AND a.NamDangKy = b.NamDangKy
                                                      AND a.MaHaiQuan = b.MaHaiQuan
			INNER JOIN (SELECT e.ID,e.TuNgay,e.DenNgay,f.MaSanPham ,e.MaDoanhNghiep,e.MaHaiQuan FROM dbo.t_KDT_LenhSanXuat e INNER JOIN dbo.t_KDT_LenhSanXuat_SP f ON f.LenhSanXuat_ID = e.ID ) t
													 ON t.MaSanPham = b.MaPhu	
													 AND t.MaDoanhNghiep = d.MaDoanhNghiep
													 AND t.MaHaiQuan = d.MaHaiQuan												  															  										  
            INNER JOIN dbo.t_SXXK_DinhMuc AS c ON b.MaPhu = c.MaSanPham
                                                  AND b.MaHaiQuan = c.MaHaiQuan
                                                  AND d.MaDoanhNghiep = c.MaDoanhNghiep	
												  AND c.LenhSanXuat_ID = t.ID					  									  
            INNER JOIN dbo.t_HaiQuan_DonViTinh AS e ON b.DVT_ID = e.ID
    WHERE   ( a.MaLoaiHinh LIKE 'XSX%'
              OR a.MaLoaiHinh LIKE 'XV%'
              OR a.MaLoaiHinh LIKE 'XCX%'
            )
            AND ( d.Xuat_NPL_SP = 'S' ) AND (a.NgayDangKy BETWEEN t.TuNgay AND t.DenNgay)
    ORDER BY d.NgayHoanThanh ,
            a.SoToKhai
			GO
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_DanhSachNPLXuatTonOver9]    
-- Database: Haiquan    
-- Author: Ngo Thanh Tung`    
-- Time created: Monday, March 31, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DanhSachNPLXuatTonOver9]    
 @BangKeHoSoThanhLy_ID bigint,    
 @SoThapPhanNPL int    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
SELECT    
    [SoToKhai]    
      ,[MaLoaiHinh]    
      ,[NamDangKy]    
      ,[MaHaiQuan]    
      ,[MaSP]    
      ,[TenSP]    
      ,[TenDVT_SP]    
      ,[LuongSP]    
      ,[MaNPL]    
      ,[DinhMuc]    
      ,round(LuongNPL,@SoThapPhanNPL) as LuongNPL    
      ,round(TonNPL,@SoThapPhanNPL) as TonNPL    
      ,[BangKeHoSoThanhLy_ID]    
      ,[NgayThucXuat]    
      ,[NgayHoanThanhXuat]    
      ,[NgayDangKy]    
FROM    
 dbo.v_KDT_SXXK_NPLXuatTonOver9    
WHERE    
 BangKeHoSoThanhLy_ID = @BangKeHoSoThanhLy_ID    
    
ORDER BY     
--HungTQ Update 18/10/2010. Bo sung them sap xep theo tieu chi 'NgayHoanThanhXuat' truoc. Ly do vi chay thanh khoan nhung TK nao co ngay hoan thanh som nhat truoc.    
 NgayHoanThanhXuat,     
 NgayDangKy,   
 --HungTQ Update 28/02/2011. Tam thoi comment NgayThucXuat, Ly do co truong 2 TK co ngay hoan thanh giong nhau nhung ngay thuc xuat cua TK 1 > TK2, do do TK1 thanh khoan sau TK2 trong BC16/TT194. Voi cau hinh Thanh khoan TKX sap xep theo ngay hoan thanh. 
 
 --NgayThucXuat,   
 SoToKhai,MaSP    
    
    
set ANSI_NULLS ON    
set QUOTED_IDENTIFIER ON    



SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.4',GETDATE(), N'CẬP NHẬT PROCEDURE XỬ LÝ THANH KHOẢN THEO LỆNH SẢN XUẤT ĐỊNH MỨC')
END