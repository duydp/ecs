﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;

namespace Company.Interface.GC
{
    public partial class PhieuXuatKho_ThongKeTon_ChiTiet : BaseForm
    {
        public long IDPhieuXuat = 0;
        public string MaPO = "";
        public long IDHopDong = 0;
        int totalRows = 0;
        int index = 0;
        public HopDong hd = new HopDong();
        DataTable tb_XNT = new DataTable();
        DataTable tb_XNT_Details = new DataTable();

        public PhieuXuatKho_ThongKeTon_ChiTiet()
        {
            InitializeComponent();
            this.Load += new EventHandler(PhieuXuatKho_ThongKeTon_ChiTiet_Load);
            cbHopDong.ValueChanged += new EventHandler(cbHopDong_ValueChanged);
        }

        void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BindXuatNhapTon();
        }

        
        private void BindHopDong()
        {
            if (string.IsNullOrEmpty(hd.SoHopDong))
            {
                hd.SoHopDong = "";
                hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                if (IDHopDong == 0)
                    cbHopDong.SelectedIndex = collection.Count - 1;
                else
                {
                    cbHopDong.SelectedIndex = collection.FindIndex(HD => HD.ID == IDHopDong);
                }
            }
            else
            {
                //hd.SoHopDong = "";
                //hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}' AND SoHopDong = '{1}'", GlobalSettings.MA_DON_VI,hd.SoHopDong);
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                if (IDHopDong == 0)
                    cbHopDong.SelectedIndex = collection.Count - 1;
                else
                {
                    cbHopDong.SelectedIndex = collection.FindIndex(HD => HD.ID == IDHopDong);
                }
            }
        }
        private void BindXuatNhapTon()
        {

            try
            {
                long idHD = System.Convert.ToInt64(cbHopDong.Value);
                tb_XNT = KDT_GC_PhieuXuatKho.LoadXuatNhapTon(idHD, IDPhieuXuat, MaPO);
                dgList.DataSource = tb_XNT;
                tb_XNT_Details = KDT_GC_PhieuXuatKho.LoadXuatNhapTon_Details(idHD, IDPhieuXuat, MaPO);
                dgList_Details.DataSource = tb_XNT_Details;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void PhieuXuatKho_ThongKeTon_ChiTiet_Load(object sender, EventArgs e)
        {
            BindHopDong();
        }
        private void ExportExcel(GridEX grid) 
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "XuatNhapTon_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grid;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }
        private void btnExport2_Click(object sender, EventArgs e)
        {
            if (lblTrangThaiXuLy.Text == "Đã xử lý")
            {
                ExportExcel(dgList_Details);
            }
            else
                MessageBox.Show("Bạn chưa xử lý dữ liệu bảng 2","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Warning);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            ExportExcel(dgList);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (lblTrangThaiXuLy.Text == "Đã xử lý")
            {
            }
            else
            {

                #region Tính Lương tồn
                DataTable temp = tb_XNT;

                totalRows = temp.Rows.Count;

                backgroundWorker1.WorkerReportsProgress = true;
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                }
                else
                {
                    //decimal tongNhap = 0;
                    decimal tongSuDung = 0;
                    //decimal tongTon = 0;
                    foreach (DataRow dr in temp.Select())
                    {
                        //string ma1 = dr["Ma"].ToString();
                        //tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                        tongSuDung = Convert.ToDecimal(dr["LuongXuat"].ToString());
                        //tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());

                        decimal luongTK = 0;
                        //decimal luongSD = 0;
                        //decimal luongTon = 0;
                        decimal triGiaTK = 0;
                        //decimal triGiaSD = 0;
                        //decimal triGiaTon = 0;
                        decimal donGia = 0;
                        //decimal tyGia = 0;

                        foreach (DataRow dr1 in tb_XNT_Details.Select("SoTaiKhoan='" + dr["SoTaiKhoan"].ToString() + "' and Ma='" + dr["Ma"].ToString() + "' and SoHopDong = '" + dr["SoHopDong"].ToString() + "'", "NgayDangKy"))
                        {


                            luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());
                            triGiaTK = Convert.ToDecimal(dr1["TriGiaTT"].ToString());
                            donGia = Convert.ToDecimal(dr1["DonGiaTT"].ToString());
                            if (dr1["SoTaiKhoan"].ToString() == "152")
                            {

                                #region Tài khoản 152
                                try
                                {
                                    #region Tính lượng vs trị giá

                                    if (tongSuDung > luongTK)
                                    {
                                        dr1["LuongSD_TK"] = luongTK;
                                        dr1["LuongTon_TK"] = 0;
                                        try
                                        {
                                            dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                            dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                        dr1["TonTriGia"] = 0;
                                        tongSuDung = tongSuDung - luongTK;
                                    }
                                    else if (tongSuDung > 0 && tongSuDung < luongTK)
                                    {
                                        dr1["LuongSD_TK"] = tongSuDung;
                                        dr1["LuongTon_TK"] = luongTK - tongSuDung;

                                        try
                                        {
                                            dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                            dr1["TriGiaSuDung"] = Math.Round(tongSuDung * donGia, 0).ToString();
                                            dr1["TonTriGia"] = Math.Round(triGiaTK - (tongSuDung * donGia), 0).ToString();
                                        }
                                        catch (Exception ex)
                                        {
                                            Logger.LocalLogger.Instance().WriteMessage(ex);
                                        }
                                        tongSuDung = 0;
                                    }
                                    else
                                    {
                                        dr1["LuongSD_TK"] = 0;
                                        dr1["TriGiaTT"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                        dr1["TriGiaSuDung"] = 0;
                                        dr1["TonTriGia"] = Convert.ToInt64(Math.Round(triGiaTK, 0).ToString());
                                    }

                                    #endregion

                                }
                                catch (Exception ex)
                                {
                                    Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                                #endregion Tài khoản 152

                            }
                        }

                        index++;
                        try
                        {
                            int i = (index * 100) / totalRows;
                            backgroundWorker1.ReportProgress(i);
                        }
                        catch (Exception)
                        {

                        }

                    }
                }

                #endregion

            }

        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {

                progressBar1.Value = e.ProgressPercentage;
                lblPercen.Text = e.ProgressPercentage.ToString() + " %";
                lblTrangThaiXuLy.Text = "Đang xử lý...(" + index + "/" + totalRows + ")";
            }
            catch (Exception)
            {

                //    throw;
            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblTrangThaiXuLy.Text = "Đã xử lý";
            btnXuatBC_XNT.Enabled = true;
        }

        private void btnXuLyToKhai_Click(object sender, EventArgs e)
        {
            lblTrangThaiXuLy.Text = "Đang xử lý";
            btnXuLyToKhai.Enabled = false;
            if (backgroundWorker1.IsBusy)
                backgroundWorker1.CancelAsync();
            backgroundWorker1.RunWorkerAsync();
            dgList.DataSource = tb_XNT_Details;
        }

        private void btnXuatBC_XNT_Click(object sender, EventArgs e)
        {

        }
    }
}
