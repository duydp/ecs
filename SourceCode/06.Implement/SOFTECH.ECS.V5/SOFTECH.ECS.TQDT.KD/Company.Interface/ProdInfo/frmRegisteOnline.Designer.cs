﻿namespace Company.Interface.ProdInfo
{
    partial class frmRegisteOnline
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRegisteOnline));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ctrDonViHQ = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label11 = new System.Windows.Forms.Label();
            this.txtNguoiLH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtDienThoai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtEmail = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChiDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.btnSend = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvDiaChi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvNguoiLienHe = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtNguoiLienHeDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.rfvEmailDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtMaCuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTKDBDL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.btnCapNhatDBDL = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl2 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSoFaxDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDienThoaiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMailDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMailHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenNganHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkCKS = new System.Windows.Forms.CheckBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnOpenWS = new System.Windows.Forms.Button();
            this.lblWS = new System.Windows.Forms.Label();
            this.txtTenDichVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPort = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHost = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDiaChiHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiTabPageDN = new Janus.Windows.UI.Tab.UITabPage();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.uiTabPageDN.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(469, 379);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.pictureBox1);
            this.uiGroupBox1.Controls.Add(this.ctrDonViHQ);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.txtNguoiLH);
            this.uiGroupBox1.Controls.Add(this.label21);
            this.uiGroupBox1.Controls.Add(this.txtDienThoai);
            this.uiGroupBox1.Controls.Add(this.label24);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.txtEmail);
            this.uiGroupBox1.Controls.Add(this.txtDiaChiDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.txtTenDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.label26);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.txtMaDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.btnSend);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(469, 379);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 89);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 34;
            this.pictureBox1.TabStop = false;
            // 
            // ctrDonViHQ
            // 
            this.ctrDonViHQ.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHQ.Location = new System.Drawing.Point(140, 241);
            this.ctrDonViHQ.Ma = "";
            this.ctrDonViHQ.MaCuc = "";
            this.ctrDonViHQ.Name = "ctrDonViHQ";
            this.ctrDonViHQ.ReadOnly = false;
            this.ctrDonViHQ.Size = new System.Drawing.Size(345, 22);
            this.ctrDonViHQ.TabIndex = 33;
            this.ctrDonViHQ.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(1, 250);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "Chọn hải quan khai báo:";
            // 
            // txtNguoiLH
            // 
            this.txtNguoiLH.Location = new System.Drawing.Point(140, 214);
            this.txtNguoiLH.Name = "txtNguoiLH";
            this.txtNguoiLH.Size = new System.Drawing.Size(345, 21);
            this.txtNguoiLH.TabIndex = 26;
            this.txtNguoiLH.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(51, 219);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 31;
            this.label21.Text = "Người liên hệ:";
            // 
            // txtDienThoai
            // 
            this.txtDienThoai.Location = new System.Drawing.Point(140, 187);
            this.txtDienThoai.Name = "txtDienThoai";
            this.txtDienThoai.Size = new System.Drawing.Size(345, 21);
            this.txtDienThoai.TabIndex = 23;
            this.txtDienThoai.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(64, 192);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(60, 13);
            this.label24.TabIndex = 29;
            this.label24.Text = "Điện thoại:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Blue;
            this.label23.Location = new System.Drawing.Point(137, 305);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(348, 13);
            this.label23.TabIndex = 28;
            this.label23.Text = "(Vui lòng nhập chính xác Email để chúng tôi có thể  gửi CD key cho bạn)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(21, 286);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(103, 13);
            this.label25.TabIndex = 28;
            this.label25.Text = "Email doanh nghiệp:";
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(140, 281);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(345, 21);
            this.txtEmail.TabIndex = 27;
            this.txtEmail.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiDoanhNghiep
            // 
            this.txtDiaChiDoanhNghiep.Location = new System.Drawing.Point(140, 160);
            this.txtDiaChiDoanhNghiep.Name = "txtDiaChiDoanhNghiep";
            this.txtDiaChiDoanhNghiep.Size = new System.Drawing.Size(345, 21);
            this.txtDiaChiDoanhNghiep.TabIndex = 21;
            this.txtDiaChiDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDoanhNghiep
            // 
            this.txtTenDoanhNghiep.Location = new System.Drawing.Point(140, 133);
            this.txtTenDoanhNghiep.Name = "txtTenDoanhNghiep";
            this.txtTenDoanhNghiep.Size = new System.Drawing.Size(345, 21);
            this.txtTenDoanhNghiep.TabIndex = 20;
            this.txtTenDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(81, 165);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 13);
            this.label26.TabIndex = 24;
            this.label26.Text = "Địa chỉ:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(27, 138);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(97, 13);
            this.label27.TabIndex = 22;
            this.label27.Text = "Tên doanh nghiệp:";
            // 
            // txtMaDoanhNghiep
            // 
            this.txtMaDoanhNghiep.Location = new System.Drawing.Point(140, 106);
            this.txtMaDoanhNghiep.Name = "txtMaDoanhNghiep";
            this.txtMaDoanhNghiep.Size = new System.Drawing.Size(345, 21);
            this.txtMaDoanhNghiep.TabIndex = 18;
            this.txtMaDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(31, 113);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(93, 13);
            this.label28.TabIndex = 19;
            this.label28.Text = "Mã doanh nghiệp:";
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSend.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSend.Icon")));
            this.btnSend.Location = new System.Drawing.Point(322, 353);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(85, 23);
            this.btnSend.TabIndex = 2;
            this.btnSend.Text = "Đăng ký";
            this.btnSend.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(415, 353);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 2;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvDiaChi
            // 
            this.rfvDiaChi.ControlToValidate = this.txtDiaChiDoanhNghiep;
            this.rfvDiaChi.ErrorMessage = "\"Địa chỉ\" không được để trống";
            this.rfvDiaChi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChi.Icon")));
            this.rfvDiaChi.Tag = "rfvDiaChi";
            // 
            // rfvMaDN
            // 
            this.rfvMaDN.ControlToValidate = this.txtMaDoanhNghiep;
            this.rfvMaDN.ErrorMessage = "\"Mã doanh nghiệp\" không được trống";
            this.rfvMaDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDN.Icon")));
            this.rfvMaDN.Tag = "rfvMaDN";
            // 
            // rfvTenDN
            // 
            this.rfvTenDN.ControlToValidate = this.txtTenDoanhNghiep;
            this.rfvTenDN.ErrorMessage = "\"Tên doanh nghiệp \" không được trống";
            this.rfvTenDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDN.Icon")));
            this.rfvTenDN.Tag = "rfvTenDN";
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // rfvNguoiLienHe
            // 
            this.rfvNguoiLienHe.ControlToValidate = this.txtNguoiLienHeDN;
            this.rfvNguoiLienHe.ErrorMessage = "\"Người liên hệ\" không được trống";
            this.rfvNguoiLienHe.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiLienHe.Icon")));
            this.rfvNguoiLienHe.Tag = "rfvNguoiLienHe";
            // 
            // txtNguoiLienHeDN
            // 
            this.txtNguoiLienHeDN.Location = new System.Drawing.Point(141, 129);
            this.txtNguoiLienHeDN.Name = "txtNguoiLienHeDN";
            this.txtNguoiLienHeDN.Size = new System.Drawing.Size(227, 20);
            this.txtNguoiLienHeDN.TabIndex = 5;
            this.txtNguoiLienHeDN.VisualStyleManager = this.vsmMain;
            // 
            // rfvEmailDN
            // 
            this.rfvEmailDN.ControlToValidate = this.txtEmail;
            this.rfvEmailDN.ErrorMessage = "\"Email doanh nghiệp\" không được trống";
            this.rfvEmailDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvEmailDN.Icon")));
            this.rfvEmailDN.Tag = "rfvEmailDN";
            // 
            // rfvMaCuc
            // 
            this.rfvMaCuc.ControlToValidate = this.txtMaCuc;
            this.rfvMaCuc.ErrorMessage = "\"Mã cục Hải quan\" không được trống";
            this.rfvMaCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaCuc.Icon")));
            this.rfvMaCuc.Tag = "rfvMaCuc";
            // 
            // txtMaCuc
            // 
            this.txtMaCuc.Location = new System.Drawing.Point(141, 18);
            this.txtMaCuc.Name = "txtMaCuc";
            this.txtMaCuc.Size = new System.Drawing.Size(227, 20);
            this.txtMaCuc.TabIndex = 0;
            this.txtMaCuc.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTKDBDL
            // 
            this.txtSoTKDBDL.Location = new System.Drawing.Point(205, 11);
            this.txtSoTKDBDL.Name = "txtSoTKDBDL";
            this.txtSoTKDBDL.Size = new System.Drawing.Size(77, 20);
            this.txtSoTKDBDL.TabIndex = 5;
            this.txtSoTKDBDL.Text = "0";
            this.txtSoTKDBDL.Value = 0;
            this.txtSoTKDBDL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            // 
            // label31
            // 
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(11, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(377, 26);
            this.label31.TabIndex = 6;
            this.label31.Text = "Lưu ý: Nếu Đại lý không cập nhật đúng số lượng tờ khai đã quy định, Đại lý sẽ khô" +
                "ng thể tiếp tục khai báo.";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(11, 19);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(180, 13);
            this.label29.TabIndex = 7;
            this.label29.Text = "Số tờ khai Đại lý cập nhật lên Server";
            // 
            // btnCapNhatDBDL
            // 
            this.btnCapNhatDBDL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCapNhatDBDL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCapNhatDBDL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCapNhatDBDL.Icon")));
            this.btnCapNhatDBDL.Location = new System.Drawing.Point(298, 1);
            this.btnCapNhatDBDL.Name = "btnCapNhatDBDL";
            this.btnCapNhatDBDL.Size = new System.Drawing.Size(86, 23);
            this.btnCapNhatDBDL.TabIndex = 8;
            this.btnCapNhatDBDL.Text = "Cập nhật";
            this.btnCapNhatDBDL.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.donViHaiQuanControl2);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtNguoiLienHeDN);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxDN);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.txtDienThoaiDN);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtMailDoanhNghiep);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox4.Location = new System.Drawing.Point(4, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(386, 349);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl2
            // 
            this.donViHaiQuanControl2.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl2.Location = new System.Drawing.Point(141, 206);
            this.donViHaiQuanControl2.Ma = "";
            this.donViHaiQuanControl2.MaCuc = "";
            this.donViHaiQuanControl2.Name = "donViHaiQuanControl2";
            this.donViHaiQuanControl2.ReadOnly = false;
            this.donViHaiQuanControl2.Size = new System.Drawing.Size(227, 22);
            this.donViHaiQuanControl2.TabIndex = 17;
            this.donViHaiQuanControl2.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(16, 211);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(123, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Chọn hải quan khai báo:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(32, 136);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Người liên hệ:";
            // 
            // txtSoFaxDN
            // 
            this.txtSoFaxDN.Location = new System.Drawing.Point(265, 99);
            this.txtSoFaxDN.Name = "txtSoFaxDN";
            this.txtSoFaxDN.Size = new System.Drawing.Size(103, 20);
            this.txtSoFaxDN.TabIndex = 4;
            this.txtSoFaxDN.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(237, 106);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Fax:";
            // 
            // txtDienThoaiDN
            // 
            this.txtDienThoaiDN.Location = new System.Drawing.Point(141, 99);
            this.txtDienThoaiDN.Name = "txtDienThoaiDN";
            this.txtDienThoaiDN.Size = new System.Drawing.Size(90, 20);
            this.txtDienThoaiDN.TabIndex = 3;
            this.txtDienThoaiDN.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(32, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Điện thoại:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(32, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email doanh nghiệp:";
            // 
            // txtMailDoanhNghiep
            // 
            this.txtMailDoanhNghiep.Location = new System.Drawing.Point(141, 157);
            this.txtMailDoanhNghiep.Name = "txtMailDoanhNghiep";
            this.txtMailDoanhNghiep.Size = new System.Drawing.Size(227, 20);
            this.txtMailDoanhNghiep.TabIndex = 6;
            this.txtMailDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(141, 72);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(227, 20);
            this.txtDiaChi.TabIndex = 2;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(141, 45);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(227, 20);
            this.txtTenDN.TabIndex = 1;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(32, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Địa chỉ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(32, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên doanh nghiệp:";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(141, 18);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(227, 20);
            this.txtMaDN.TabIndex = 0;
            this.txtMaDN.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(32, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã doanh nghiệp:";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.txtMaCuc);
            this.uiGroupBox3.Controls.Add(this.txtMailHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.txtTenCucHQ);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtTenNganHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox3.Location = new System.Drawing.Point(4, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(386, 158);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Chi cục Hải quan khai báo";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(16, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(90, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Mã cục hải quan:";
            // 
            // txtMailHaiQuan
            // 
            this.txtMailHaiQuan.Location = new System.Drawing.Point(141, 128);
            this.txtMailHaiQuan.Name = "txtMailHaiQuan";
            this.txtMailHaiQuan.Size = new System.Drawing.Size(227, 20);
            this.txtMailHaiQuan.TabIndex = 4;
            this.txtMailHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(16, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Email Hải quan:";
            // 
            // txtTenCucHQ
            // 
            this.txtTenCucHQ.Location = new System.Drawing.Point(141, 73);
            this.txtTenCucHQ.Name = "txtTenCucHQ";
            this.txtTenCucHQ.Size = new System.Drawing.Size(227, 20);
            this.txtTenCucHQ.TabIndex = 2;
            this.txtTenCucHQ.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(16, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Tên cục hải quan:";
            // 
            // txtTenNganHQ
            // 
            this.txtTenNganHQ.Location = new System.Drawing.Point(141, 100);
            this.txtTenNganHQ.Name = "txtTenNganHQ";
            this.txtTenNganHQ.Size = new System.Drawing.Size(227, 20);
            this.txtTenNganHQ.TabIndex = 3;
            this.txtTenNganHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tên ngắn hải quan:";
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(141, 45);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(227, 22);
            this.donViHaiQuanControl1.TabIndex = 1;
            this.donViHaiQuanControl1.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(16, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chọn hải quan khai báo:";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.chkCKS);
            this.uiGroupBox5.Controls.Add(this.label22);
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Controls.Add(this.label16);
            this.uiGroupBox5.Controls.Add(this.btnOpenWS);
            this.uiGroupBox5.Controls.Add(this.lblWS);
            this.uiGroupBox5.Controls.Add(this.txtTenDichVu);
            this.uiGroupBox5.Controls.Add(this.label17);
            this.uiGroupBox5.Controls.Add(this.label18);
            this.uiGroupBox5.Controls.Add(this.txtPort);
            this.uiGroupBox5.Controls.Add(this.txtHost);
            this.uiGroupBox5.Controls.Add(this.label19);
            this.uiGroupBox5.Controls.Add(this.txtDiaChiHQ);
            this.uiGroupBox5.Controls.Add(this.label20);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(4, 163);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(386, 186);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Thông số khai báo Hải quan";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // chkCKS
            // 
            this.chkCKS.AutoSize = true;
            this.chkCKS.ForeColor = System.Drawing.Color.Blue;
            this.chkCKS.Location = new System.Drawing.Point(141, 125);
            this.chkCKS.Name = "chkCKS";
            this.chkCKS.Size = new System.Drawing.Size(200, 17);
            this.chkCKS.TabIndex = 5;
            this.chkCKS.Text = "Dùng địa chỉ tổng cục Hải quan ";
            this.chkCKS.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(368, 51);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 38;
            this.label22.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(371, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(368, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "*";
            // 
            // btnOpenWS
            // 
            this.btnOpenWS.AutoSize = true;
            this.btnOpenWS.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnOpenWS.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenWS.Image")));
            this.btnOpenWS.Location = new System.Drawing.Point(361, 152);
            this.btnOpenWS.Name = "btnOpenWS";
            this.btnOpenWS.Size = new System.Drawing.Size(22, 22);
            this.btnOpenWS.TabIndex = 7;
            this.btnOpenWS.UseVisualStyleBackColor = true;
            // 
            // lblWS
            // 
            this.lblWS.BackColor = System.Drawing.Color.Transparent;
            this.lblWS.ForeColor = System.Drawing.Color.Red;
            this.lblWS.Location = new System.Drawing.Point(16, 154);
            this.lblWS.Name = "lblWS";
            this.lblWS.Size = new System.Drawing.Size(346, 20);
            this.lblWS.TabIndex = 6;
            this.lblWS.Text = "http://www.dngcustoms.gov.vn/kdtservice/kdtservice.asmx";
            this.lblWS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenDichVu
            // 
            this.txtTenDichVu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDichVu.Location = new System.Drawing.Point(141, 47);
            this.txtTenDichVu.MaxLength = 200;
            this.txtTenDichVu.Name = "txtTenDichVu";
            this.txtTenDichVu.Size = new System.Drawing.Size(224, 21);
            this.txtTenDichVu.TabIndex = 1;
            this.txtTenDichVu.Text = "kdtservice/kdtservice.asmx";
            this.txtTenDichVu.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(16, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Tên dịch vụ khai báo";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(16, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Port proxy";
            // 
            // txtPort
            // 
            this.txtPort.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(141, 98);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(224, 21);
            this.txtPort.TabIndex = 4;
            this.txtPort.VisualStyleManager = this.vsmMain;
            // 
            // txtHost
            // 
            this.txtHost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHost.Location = new System.Drawing.Point(141, 74);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(224, 21);
            this.txtHost.TabIndex = 3;
            this.txtHost.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Host proxy";
            // 
            // txtDiaChiHQ
            // 
            this.txtDiaChiHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiHQ.Location = new System.Drawing.Point(141, 20);
            this.txtDiaChiHQ.Name = "txtDiaChiHQ";
            this.txtDiaChiHQ.Size = new System.Drawing.Size(224, 21);
            this.txtDiaChiHQ.TabIndex = 0;
            this.txtDiaChiHQ.Text = "http://www.dngcustoms.gov.vn";
            this.txtDiaChiHQ.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(16, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Địa chỉ hải quan";
            // 
            // uiTabPageDN
            // 
            this.uiTabPageDN.Controls.Add(this.uiGroupBox4);
            this.uiTabPageDN.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageDN.Name = "uiTabPageDN";
            this.uiTabPageDN.Size = new System.Drawing.Size(393, 334);
            this.uiTabPageDN.TabStop = true;
            this.uiTabPageDN.Text = "Thông tin Doanh nghiệp";
            // 
            // frmRegisteOnline
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(469, 379);
            this.Controls.Add(this.uiGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(403, 413);
            this.Name = "frmRegisteOnline";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin doanh nghiệp và hải quan";
            this.Load += new System.EventHandler(this.frmRegisteOnline_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            this.uiTabPageDN.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDN;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiLienHe;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvEmailDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaCuc;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.CheckBox chkCKS;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnOpenWS;
        private System.Windows.Forms.Label lblWS;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDichVu;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtPort;
        private Janus.Windows.GridEX.EditControls.EditBox txtHost;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiHQ;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailHaiQuan;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHQ;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHQ;
        private System.Windows.Forms.Label label1;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl2;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLienHeDN;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxDN;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoaiDN;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTKDBDL;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.EditControls.UIButton btnCapNhatDBDL;
        private Company.Interface.Controls.DonViHaiQuanControl ctrDonViHQ;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLH;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoai;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmail;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoanhNghiep;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDoanhNghiep;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageDN;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Janus.Windows.EditControls.UIButton btnSend;

    }
}