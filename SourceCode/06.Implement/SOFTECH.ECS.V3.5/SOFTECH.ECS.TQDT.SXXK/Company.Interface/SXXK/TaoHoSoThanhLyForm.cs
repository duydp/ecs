using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.Interface.SXXK.BangKe;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components;

namespace Company.Interface.SXXK
{
    public partial class TaoHoSoThanhLyForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public TaoHoSoThanhLyForm()
        {
            InitializeComponent();
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (this.HSTL.checkHSTLHasCreated(MainForm.EcsQuanTri.Identity.Name))
            {
               // if (ShowMessage("Hồ sơ thanh khoản lần trước đã tạo.\nBạn có muốn cập nhật hồ sơ trước không?", true) == "Yes")
                if (MLMessages("Hồ sơ thanh khoản lần trước đã tạo.\nBạn có muốn cập nhật hồ sơ trước không?", "MSG_THK63", "", true) == "Yes")
                {
                    Form[] forms = this.ParentForm.MdiChildren;
                    for (int i = 0; i < forms.Length; i++)
                    {
                        if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                        {
                            forms[i].Activate();
                            this.Close();
                            return;
                        }
                    }
                    HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
                    int id = HSTL.getHSTLMoiNhat(MainForm.EcsQuanTri.Identity.Name, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (id == 0)
                    {

                    }
                    else
                    {
                        HSTL.ID = id;
                        HSTL= HoSoThanhLyDangKy.Load(id);
                        HSTL.LoadBKCollection();

                    }
                    CapNhatHoSoThanhLyForm f = new CapNhatHoSoThanhLyForm();
                    f.HSTL = HSTL;
                    f.MdiParent = this.ParentForm;
                    f.Show();
                    this.Close();
                }

            }
            else
            {
                //TODO: Hungtq updated 29/03/2013: Kiểm tra và Cập nhật năm đăng ký của các tờ khai đã duyệt có NamDK = 0 trong bảng t_KDT_ToKhaiMauDich
                Company.BLL.KDT.ToKhaiMauDich.UpdateNamDK();

                //TODO: Hungtq updated 01/04/2013: Kiểm tra và Cập nhật ngày hoàn thành của các tờ khai đã duyệt (Nhập tay vào) có NgayHoanThanh = 01/01/1900 trong bảng t_SXXK_ToKhaiMauDich
                Company.BLL.KDT.ToKhaiMauDich.UpdateNgayHoanThanh();

                this.HSTL.MaHaiQuanTiepNhan = GlobalSettings.MA_HAI_QUAN;
                this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.HSTL.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                this.HSTL.NgayBatDau = DateTime.Today;
                this.HSTL.LanThanhLy = this.HSTL.GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                this.HSTL.UserName = MainForm.EcsQuanTri.Identity.Name;
                BK02WizardForm bk02 = new BK02WizardForm();
                bk02.MdiParent = this.ParentForm;
                bk02.HSTL = this.HSTL;
                bk02.Show();
                this.Close();
            }
        }

        private void TaoHoSoThanhLyForm_Load(object sender, EventArgs e)
        {
            //if (GlobalSettings.MA_DON_VI != "4000395355") btnCreateHSTKGC.Visible = false;

            //minhnd thanh khoản theo Định mức mới dành cho HOATHO
            if (GlobalSettings.MA_DON_VI != "0400101556")
                btnCreateHSTheoMaHang.Enabled = false;
            //minhnd thanh khoản theo Định mức mới dành cho HOATHO
        }

        private void btnCreateHSTKGC_Click(object sender, EventArgs e)
        {
            this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (this.HSTL.checkHSTLHasCreated(MainForm.EcsQuanTri.Identity.Name))
            {
               // if (ShowMessage("Hồ sơ thanh khoản lần trước đã tạo.\nBạn có muốn cập nhật hồ sơ trước không?", true) == "Yes")
                if (MLMessages("Hồ sơ thanh khoản lần trước đã tạo.\nBạn có muốn cập nhật hồ sơ trước không?", "MSG_THK63", "", true) == "Yes")
                {
                    Form[] forms = this.ParentForm.MdiChildren;
                    for (int i = 0; i < forms.Length; i++)
                    {
                        if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                        {
                            forms[i].Activate();
                            this.Close();
                            return;
                        }
                    }
                    HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
                    int id = HSTL.getHSTLMoiNhat(MainForm.EcsQuanTri.Identity.Name, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (id == 0)
                    {

                    }
                    else
                    {
                        HSTL.ID = id;
                        HSTL= HoSoThanhLyDangKy.Load(id);
                        HSTL.LoadBKCollection();

                    }
                    CapNhatHoSoThanhLyForm f = new CapNhatHoSoThanhLyForm();
                    f.HSTL = HSTL;
                    f.MdiParent = this.ParentForm;
                    f.Show();
                    this.Close();
                }

            }
            else
            {
                this.HSTL.MaHaiQuanTiepNhan = GlobalSettings.MA_HAI_QUAN;
                this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.HSTL.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                this.HSTL.NgayBatDau = DateTime.Today;
                this.HSTL.SoTiepNhan = 1;//Hồ sơ thanh khoản dùng tờ khai XGC
                this.HSTL.LanThanhLy = this.HSTL.GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                this.HSTL.UserName = MainForm.EcsQuanTri.Identity.Name;
                BK02XGCForm bk02 = new BK02XGCForm();
                bk02.MdiParent = this.ParentForm;
                bk02.HSTL = this.HSTL;
                bk02.Show();
                this.Close();
            }
        }

        private void btnCreateHSTheoMaHang_Click(object sender, EventArgs e)
        {
            this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (this.HSTL.checkHSTLHasCreated(MainForm.EcsQuanTri.Identity.Name))
            {
                // if (ShowMessage("Hồ sơ thanh khoản lần trước đã tạo.\nBạn có muốn cập nhật hồ sơ trước không?", true) == "Yes")
                if (MLMessages("Hồ sơ thanh khoản lần trước đã tạo.\nBạn có muốn cập nhật hồ sơ trước không?", "MSG_THK63", "", true) == "Yes")
                {
                    Form[] forms = this.ParentForm.MdiChildren;
                    for (int i = 0; i < forms.Length; i++)
                    {
                        if (forms[i].Name.ToString().Equals("CapNhatHoSoThanhLyForm"))
                        {
                            forms[i].Activate();
                            this.Close();
                            return;
                        }
                    }
                    HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
                    int id = HSTL.getHSTLMoiNhat(MainForm.EcsQuanTri.Identity.Name, GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                    if (id == 0)
                    {

                    }
                    else
                    {
                        HSTL.ID = id;
                        HSTL = HoSoThanhLyDangKy.Load(id);
                        HSTL.LoadBKCollection();

                    }
                    CapNhatHoSoThanhLyForm f = new CapNhatHoSoThanhLyForm();
                    f.HSTL = HSTL;
                    f.HSTL.HSTL_TheoMaHang = true;
                    f.MdiParent = this.ParentForm;
                    f.Show();
                    this.Close();
                }

            }
            else
            {
                //TODO: Hungtq updated 29/03/2013: Kiểm tra và Cập nhật năm đăng ký của các tờ khai đã duyệt có NamDK = 0 trong bảng t_KDT_ToKhaiMauDich
                Company.BLL.KDT.ToKhaiMauDich.UpdateNamDK();

                //TODO: Hungtq updated 01/04/2013: Kiểm tra và Cập nhật ngày hoàn thành của các tờ khai đã duyệt (Nhập tay vào) có NgayHoanThanh = 01/01/1900 trong bảng t_SXXK_ToKhaiMauDich
                Company.BLL.KDT.ToKhaiMauDich.UpdateNgayHoanThanh();

                this.HSTL.MaHaiQuanTiepNhan = GlobalSettings.MA_HAI_QUAN;
                this.HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.HSTL.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                this.HSTL.NgayBatDau = DateTime.Today;
                this.HSTL.LanThanhLy = this.HSTL.GetLanThanhLyMoiNhat(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
                this.HSTL.UserName = MainForm.EcsQuanTri.Identity.Name;
                BK02WizardForm bk02 = new BK02WizardForm();
                bk02.MdiParent = this.ParentForm;
                bk02.HSTL = this.HSTL;
                bk02.HSTL.HSTL_TheoMaHang = true;
                bk02.Show();
                this.Close();
            }
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }
    }
}