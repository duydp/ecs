﻿#define DEBUG
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml.Serialization;
using System.IO;
using System.Xml;
using System.Text;
using System.ServiceModel;
using System.Linq;
using System.Threading;
using System.Globalization;
namespace Company.KDT.SHARE.Components
{

    public class Helpers
    {

        /// <summary>
        /// Message phong bì đóng gói thông tin khai báo của doanh nghiệp gửi lên Hải quan, tất cả các message gửi đến Hải quan đều được đóng gói bên ngoài bởi message này
        /// Chú ý các tính năng format kiểu dữ liệu xem tại
        /// http://msdn.microsoft.com/en-us/library/system.string.format.aspx
        /// http://msdn.microsoft.com/en-us/library/26etazsy.aspx
        /// </summary>
        /// <param name="sendFrom">Nơi gửi</param>
        /// <param name="sendTo">Nơi đến</param>
        /// <param name="subject">Tiêu đề</param>
        /// <param name="content">Nội dung</param>
        /// <param name="signature">Chữ ký số</param>
        /// <returns></returns>
        public static string BuildSend(NameBase sendFrom, NameBase sendTo, SubjectBase subject, object objContent, MessageSignature signature)
        {

            return BuildSend(sendFrom, sendTo, subject, objContent, signature, false);

        }
        public static string BuildSend(NameBase sendFrom, NameBase sendTo, SubjectBase subject, object objContent, MessageSignature signature, bool removeNameSpace)
        {
            MessageSend<string> msgSend = Envelope<string>(sendFrom, sendTo, subject);
           // Company.KDT.SHARE.Components.KD_ToKhaiMauDich cc = (Company.KDT.SHARE.Components.KD_ToKhaiMauDich)objContent;

            string content = Serializer(objContent, true, removeNameSpace);
            content = ConvertToBase64(content);
            content = MultipleLines(content);
            msgSend.MessageBody.MessageContent.Text = content;

            if (signature != null)
            {
                msgSend.MessageBody.MessageSignature = signature;
            }
            return Serializer(msgSend);
        }
        /// <summary>
        /// Phong bì message
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sendFrom"></param>
        /// <param name="sendTo"></param>
        /// <param name="subject"></param>
        /// <returns></returns>
        private static MessageSend<T> Envelope<T>(NameBase sendFrom, NameBase sendTo, SubjectBase subject)
        {
            if (subject == null) throw new ArgumentNullException("Tiêu chí gửi không được null");
            if (sendFrom == null) throw new ArgumentNullException("Nơi gửi không được rỗng");
            if (sendTo == null) throw new ArgumentNullException("Nơi đến không được null");

            MessageSend<T> msgSend = new MessageSend<T>()
            {
                MessageHeader = new MessageHeader()
                {
                    Reference = new Reference()
                    {
                        Version = Globals.VersionSend,
                        ReplyTo = string.Empty,
                        MessageID = Guid.NewGuid().ToString()

                    },
                    SendApplication = new SendApplication()
                    {
                        Name = "ECS.SOFTECH",
                        Version = Globals.VersionSend,
                        CompanyName = "SOFTECH-DANANG",
                        CompanyIdentity = "0400392263",
                        CreateMessageIssue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    },
                    From = sendFrom,
                    To = sendTo,
                    Subject = new Subject()

                }
                ,
                MessageBody = new MessageBody<T>()

            };

            msgSend.MessageHeader.ProcedureType = "2";//TQDT , KTX is 1
            if (!string.IsNullOrEmpty(subject.Type))
                msgSend.MessageHeader.Subject.Type = subject.Type;

            msgSend.MessageHeader.Subject.Function = subject.Function;
            msgSend.MessageHeader.Subject.Reference = subject.Reference;//.Replace("-",string.Empty);
            msgSend.MessageBody = new MessageBody<T>();
            msgSend.MessageBody.MessageContent = new MessageContent<T>();
#if DEBUG
            msgSend.MessageHeader.SendApplication.Name = "ECUS3";
            msgSend.MessageHeader.SendApplication.CompanyName = "ThaiSon";
            msgSend.MessageHeader.SendApplication.CompanyIdentity = "0101300842";
            msgSend.MessageHeader.Subject.SendApplication = "ECUS_K3";
#endif
            return msgSend;
        }
        /// <summary>
        /// Lấy phản hồi từ phía hải quan
        /// </summary>
        /// <param name="sendFrom">Nơi gửi</param>
        /// <param name="sendTo">Nơi đến</param>
        /// <param name="subject">Tiêu đề gửi</param>
        /// <param name="signature">Chữ ký số</param>
        /// <returns></returns>
        public static string BuildFeedBack(NameBase sendFrom, NameBase sendTo, SubjectBase subject, MessageSignature signature)
        {
            string ret = GetResourceString("Company.KDT.SHARE.Components.PhongBi.PhongBiV3.xml");
            string subType = subject.Type;
            subject.Type = null;
            string content = Serializer(subject, Encoding.UTF8, true, true);
            subject.Type = subType;
            content = ConvertToBase64(content);
            content = MultipleLines(content);
            MessageSend<string> msgSend = Envelope<string>(sendFrom, sendTo, subject);
            ret = string.Format(ret, new object[]{
                msgSend.MessageHeader.ProcedureType,
                msgSend.MessageHeader.Reference.Version,
                msgSend.MessageHeader.Reference.MessageID,
                msgSend.MessageHeader.Reference.ReplyTo,
                msgSend.MessageHeader.SendApplication.Name,
                msgSend.MessageHeader.SendApplication.Version,
                msgSend.MessageHeader.SendApplication.CompanyName,
                msgSend.MessageHeader.SendApplication.CompanyIdentity,
                msgSend.MessageHeader.SendApplication.CreateMessageIssue,
                msgSend.MessageHeader.From.Name,
                msgSend.MessageHeader.From.Identity,
                msgSend.MessageHeader.To.Name,
                msgSend.MessageHeader.To.Identity,
                msgSend.MessageHeader.Subject.Type,
                msgSend.MessageHeader.Subject.Function,
                msgSend.MessageHeader.Subject.Reference,
                msgSend.MessageHeader.Subject.SendApplication,
                msgSend.MessageHeader.Subject.ReceiveApplication,
                content

            });
            return ret;
        }

        /// <summary>
        /// Chia ra nhiều dòng mỗi dòng đạt 80 ký tự
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private static string MultipleLines(string source)
        {
            int pos = 0;
            string ret = source;
            while (pos <= source.Length)
            {
                ret = ret.Insert(pos, "\r\n");
                pos += 84;
            }
            return ret;
        }

        /// <summary>
        /// Chuyển chuổi format xml sang object T
        /// </summary>
        /// <typeparam name="T">object</typeparam>
        /// <param name="sfmtObjectXml">format xml</param>
        /// <returns>T</returns>
        public static T Deserialize<T>(string sfmtObjectXml)
        {
            if (string.IsNullOrEmpty(sfmtObjectXml)) throw new ArgumentNullException("Deserialize parameter ->sfmtObjectXml không được rỗng");
            using (StringReader reader = new StringReader(sfmtObjectXml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }
        /// <summary>
        /// Chuyển format xml sang object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding"></param>
        /// <param name="removeDeclartion">Gở bỏ format <?xml version="1.0" encoding="utf-8"?></param>
        /// <param name="removeNameSpace">Gở bỏ namespace</param>
        /// <returns></returns>
        public static string Serializer(object obj, Encoding encoding, bool removeDeclartion, bool removeNameSpace)
        {

            XmlWriterSettings xmlWS = new XmlWriterSettings();
            if (removeDeclartion)
                xmlWS.OmitXmlDeclaration = true;
            xmlWS.Encoding = encoding;
            StringWriter sWriter = new StringWriter();
            using (XmlWriter xmlWriter = XmlWriter.Create(sWriter, xmlWS))
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                if (removeNameSpace)
                    namespaces.Add("", "");//Remove NameSpace
                else
                    namespaces.Add("dt", "urn:schemas-microsoft-com:datatypes");// Add Namespace
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(xmlWriter, obj, namespaces);
                return sWriter.ToString();
            }
        }
        public static string Serializer(object obj)
        {
            return Serializer(obj, Encoding.UTF8, false, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="removeDeclartion">Gở bỏ format <?xml version="1.0" encoding="utf-8"?></param>
        /// <param name="removeNameSpace">Gở bỏ namespace</param>
        /// <returns></returns>
        public static string Serializer(object obj, bool removeDeclartion, bool removeNameSpace)
        {
            return Serializer(obj, Encoding.UTF8, removeDeclartion, removeNameSpace);
        }
        /// <summary>
        /// Thực hiện hành động và đóng kết nối
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="action"></param>
        public static void DoActionAndClose<T>(T client, Action<T> action) where T : ICommunicationObject
        {
            try
            {
                action(client);
                client.Close();
            }
            catch
            {
                client.Abort();
                throw;
            }
        }
        public static void DoAction<T>(T client, Action<T> action) where T : ICommunicationObject
        {
            try
            {
                action(client);
            }
            catch
            {
                client.Abort();
                throw;
            }
        }
        public static string FormatNumeric(object obj)
        {
            return FormatNumeric(obj, 0);
        }
        public static string FormatNumeric(object obj, int decimalFormat)
        {
            string ret = string.Empty;
            if (obj == null) return ret;

            string sFormat = "{0:0" + Globals.GetDot(decimalFormat) + "}";

            if (obj is bool)
            {
                bool b = Convert.ToBoolean(obj, CultureInfo.CreateSpecificCulture("en-US"));
                ret = b ? "1" : "0";
            }
            else
            {
                ret = string.Format(CultureInfo.CreateSpecificCulture("en-US"), sFormat, new object[] { obj });
            }
            return ret;
        }


        public static string ConvertFromBase64(string strSource)
        {
            return Encoding.Unicode.GetString(Convert.FromBase64String(strSource));
        }
        static public string GetResourceString(string resourceName)
        {
            Stream stream = System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream(resourceName);
            if (stream == null)
                throw new FileNotFoundException("Resource not found.");
            using (StreamReader reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
        public static string ConvertToBase64(string strSource)
        {
            return Convert.ToBase64String(Encoding.Unicode.GetBytes(strSource));
        }
        public static void Demo()
        {
            ActivationInfo.ClientDetailInfo clientinf = new ActivationInfo.ClientDetailInfo()
            {
                DBName = "192.4985745",
                DiaChiHaiQuan = "NONE",
                NgayOnline = DateTime.Now
            };
            string sfmt = Serializer(clientinf);
            ActivationInfo.ClientDetailInfo clientinf1 = Deserialize<ActivationInfo.ClientDetailInfo>(sfmt);

        }
        public static void DemoSend()
        {

            string sfmtResponse = string.Empty;
            string content = string.Empty;
            string msgSend = string.Empty;
            CisWS.CISServiceSoapClient Service;
            Service = WebService.GetCisService();
            string reference = Guid.NewGuid().ToString();
            FeedBackContent feedback;
#if !DEBUG
            #region Demo Send
            KD_ToKhaiXuat KD_tkx = Company.KDT.SHARE.Components.Demo.KD_ToKhaiXuat();

            msgSend = BuildSend(
                                      new NameBase()
                                        {
                                            Name = "May 10",
                                            Identity = "0100101308"
                                        }
                                        , new NameBase()
                                        {
                                            Name = "Chi cục HQ CK Cảng Đà Nẵng KV I",
                                            Identity = "C34C"
                                        }
                                     ,
                                       new SubjectBase()
                                       {
                                           Type = "932",
                                           Function = "8",
                                           Reference = reference
                                       }
                                       ,
                                       KD_tkx, null);
            Helpers.DoAction<CisWS.CISServiceSoapClient>(Service, client =>
            {
                sfmtResponse = client.Send(msgSend, "0100101308", getPass("0100101308"));
            });
            feedback = GetFeedBackContent(sfmtResponse);
            #endregion Demo Send
#endif
            #region Request
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = "930",
                Reference = reference,
                Function = "13",
                Type = "930"

            };

            msgSend = BuildFeedBack(
                                      new NameBase()
                                        {
                                            Name = "May 10",
                                            Identity = "0100101308"
                                        },
                                        new NameBase()
                                        {
                                            Name = "Chi cục HQ CK Cảng Đà Nẵng KV I",
                                            Identity = "C34C"
                                        }, subjectBase, null);
            //ThreadPool.QueueUserWorkItem(Send);
            Helpers.DoAction<CisWS.CISServiceSoapClient>(Service, client =>
            {
                sfmtResponse = client.Send(msgSend, "0100101308", getPass("0100101308"));
            });
            #endregion Request

            #region Process Response
            feedback = GetFeedBackContent(sfmtResponse);
            #endregion Process Response

        }
        static void Send(object obj)
        {

        }
        public static FeedBackContent GetFeedBackContent(string sfmtFeedBack)
        {
            MessageSend<FeedBackContent> msg = Helpers.Deserialize<MessageSend<FeedBackContent>>(sfmtFeedBack);
            FeedBackContent fback = null;
            if (!string.IsNullOrEmpty(msg.MessageBody.MessageContent.Text))
            {
                string contentError = ConvertFromBase64(msg.MessageBody.MessageContent.Text);
                fback = Helpers.Deserialize<FeedBackContent>(contentError);
            }
            return fback;
        }
        static string getPass(string code)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes("0100101308");
            return BitConverter.ToString(((System.Security.Cryptography.HashAlgorithm)System.Security.Cryptography.CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToLower();

        }
    }
}
