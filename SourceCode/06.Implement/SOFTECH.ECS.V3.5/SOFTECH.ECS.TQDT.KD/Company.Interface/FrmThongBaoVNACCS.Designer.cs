﻿namespace Company.Interface
{
    partial class FrmThongBaoVNACCS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmThongBaoVNACCS));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ckbKhongHienThi = new Janus.Windows.EditControls.UICheckBox();
            this.value = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.tlHelp = new DevExpress.XtraTreeList.TreeList();
            this.name = new DevExpress.XtraTreeList.Columns.TreeListColumn();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tlHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(987, 564);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Open_48px.png");
            this.imageList1.Images.SetKeyName(1, "Folder_48px.png");
            this.imageList1.Images.SetKeyName(2, "statistic-document.png");
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.ckbKhongHienThi);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox3.Location = new System.Drawing.Point(8, 529);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(227, 27);
            this.uiGroupBox3.TabIndex = 1;
            // 
            // ckbKhongHienThi
            // 
            this.ckbKhongHienThi.Location = new System.Drawing.Point(22, 6);
            this.ckbKhongHienThi.Name = "ckbKhongHienThi";
            this.ckbKhongHienThi.Size = new System.Drawing.Size(174, 18);
            this.ckbKhongHienThi.TabIndex = 0;
            this.ckbKhongHienThi.Text = "Không hiển thị thông báo lần sau";
            this.ckbKhongHienThi.CheckedChanged += new System.EventHandler(this.ckbKhongHienThi_CheckedChanged);
            // 
            // value
            // 
            this.value.Caption = "treeListColumn1";
            this.value.FieldName = "value";
            this.value.Name = "value";
            this.value.OptionsColumn.AllowEdit = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.tlHelp);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox1.ImageList = this.imageList1;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.uiGroupBox1.Size = new System.Drawing.Size(243, 564);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // tlHelp
            // 
            this.tlHelp.Columns.AddRange(new DevExpress.XtraTreeList.Columns.TreeListColumn[] {
            this.name,
            this.value});
            this.tlHelp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlHelp.HorzScrollStep = 1;
            this.tlHelp.HorzScrollVisibility = DevExpress.XtraTreeList.ScrollVisibility.Always;
            this.tlHelp.Location = new System.Drawing.Point(8, 13);
            this.tlHelp.Name = "tlHelp";
            this.tlHelp.OptionsView.ShowColumns = false;
            this.tlHelp.OptionsView.ShowIndicator = false;
            this.tlHelp.SelectImageList = this.imageList1;
            this.tlHelp.Size = new System.Drawing.Size(227, 516);
            this.tlHelp.TabIndex = 1;
            this.tlHelp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tlHelp_MouseUp);
            // 
            // name
            // 
            this.name.Caption = "treeListColumn1";
            this.name.FieldName = "name";
            this.name.Name = "name";
            this.name.OptionsColumn.AllowEdit = false;
            this.name.OptionsColumn.ReadOnly = true;
            this.name.Visible = true;
            this.name.VisibleIndex = 0;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.webBrowser1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(243, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Padding = new System.Windows.Forms.Padding(5);
            this.uiGroupBox2.Size = new System.Drawing.Size(744, 564);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(8, 13);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(728, 543);
            this.webBrowser1.TabIndex = 0;
            // 
            // FrmThongBaoVNACCS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(987, 564);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmThongBaoVNACCS";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông báo về hệ thống VNACCS";
            this.Load += new System.EventHandler(this.FrmThongBaoVNACCS_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tlHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private DevExpress.XtraTreeList.TreeList tlHelp;
        private DevExpress.XtraTreeList.Columns.TreeListColumn name;
        private DevExpress.XtraTreeList.Columns.TreeListColumn value;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UICheckBox ckbKhongHienThi;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}