﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    public class NodePK_SuaSanPham
    {
        /// <summary>
        /// Mã sản phẩm muốn sửa đổi
        /// </summary>
        public const string preIdentification = "preIdentification";    
        /// <summary>
        /// Sản phẩm bổ sung 
        /// </summary>
        public const string Product = "Product";    
        /// <summary>
        /// Sản phẩm
        /// </summary>
        public const string Commodity = "Commodity";    
        /// <summary>
        /// Tên/Mô tả Sản phẩm bổ sung
        /// </summary>
        public const string description = "description";    
        /// <summary>
        /// Mã Sản phẩm bổ sung
        /// </summary>
        public const string identification = "identification";    
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";    
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";    
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";    
        /// <summary>
        /// Mã nhóm sản phẩm
        /// </summary>
        public const string productGroup = "productGroup";    
    }
}
