using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Collections.Generic;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.KDT.GC
{
    public partial class NhomSanPham
    {
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        public int DeleteBy_HopDong_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        public List<NhomSanPham> SelectCollectionBy_HopDong_ID()
        {
            return (List<NhomSanPham>)NhomSanPham.SelectCollectionBy_HopDong_ID(this.HopDong_ID);
        }
        public bool Load()
        {
            return HopDong.Load(this.HopDong_ID) != null;
        }
        public int DeleteBy_HopDong_ID()
        {
            string spName = "p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, TenSanPham);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public List<NhomSanPham> SelectCollectionBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 28/06/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            List<NhomSanPham> collection = new List<NhomSanPham>();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                NhomSanPham entity = new NhomSanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) entity.GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


    }
}
