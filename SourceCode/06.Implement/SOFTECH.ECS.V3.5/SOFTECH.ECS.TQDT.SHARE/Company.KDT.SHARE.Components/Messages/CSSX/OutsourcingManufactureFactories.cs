﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class OutsourcingManufactureFactories
    {
        /// <summary>
        /// Thông tin CSSX
        /// </summary>
       [XmlElement("OutsourcingManufactureFactory")]
       public List<OutsourcingManufactureFactory> OutsourcingManufactureFactory { get; set; }
    }
}
