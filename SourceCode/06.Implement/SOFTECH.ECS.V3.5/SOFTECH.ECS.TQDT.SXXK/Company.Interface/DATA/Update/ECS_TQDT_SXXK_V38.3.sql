GO
IF OBJECT_ID('t_KDT_VNACCS_CapSoDinhDanh_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_CapSoDinhDanh_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_CapSoDinhDanh_Log]
(
[ID] [bigint] NOT NULL ,
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[MaDoanhNghiep] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHQ] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoaiDoiTuong] [int] NULL,
[LoaiTTHH] [int] NULL,
[SoDinhDanh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayCap] [datetime] NULL,
[GuidStr] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TrangThaiXuLy] [int] NULL,
[CodeContent] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACCS_CapSoDinhDanh_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_CapSoDinhDanh_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_CapSoDinhDanh_Log
ON dbo.t_KDT_VNACCS_CapSoDinhDanh
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_CapSoDinhDanh_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_CapSoDinhDanh_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_CapSoDinhDanh_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACCS_BillOfLadingNew_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_BillOfLadingNew_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BillOfLadingNew_Log]
(
[ID] [bigint] NOT NULL,
[TrangThaiXuLy] [int] NULL,
[SoTN] [bigint] NULL,
[NgayTN] [datetime] NULL,
[MaHQ] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GuidStr] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_BillOfLadingNew_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_BillOfLadingNew_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_BillOfLadingNew_Log
ON t_KDT_VNACCS_BillOfLadingNew
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BillOfLadingNew_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_BillOfLadingNew_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BillOfLadingNew_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACCS_BillOfLadings_Details_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_BillOfLadings_Details_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BillOfLadings_Details_Log]
(
[ID] [bigint] NOT NULL ,
[BillOfLadings_ID] [bigint] NOT NULL,
[SoVanDonGoc] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayVanDonGoc] [datetime] NOT NULL,
[MaNguoiPhatHanh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SoLuongVDN] [numeric] (2, 0) NOT NULL,
[PhanLoaiTachVD] [numeric] (2, 0) NOT NULL,
[LoaiHang] [numeric] (1, 0) NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_BillOfLadings_Details_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_BillOfLadings_Details_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_BillOfLadings_Details_Log
ON t_KDT_VNACCS_BillOfLadings_Details
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BillOfLadings_Details_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_BillOfLadings_Details_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BillOfLadings_Details_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACCS_BranchDetail_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_BranchDetail_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BranchDetail_Log]
(
[ID] [bigint] NOT NULL ,
[BillOfLadings_Details_ID] [bigint] NOT NULL,
[STT] [numeric] (2, 0) NOT NULL,
[SoVanDon] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenNguoiGuiHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiaChiNguoiGuiHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenNguoiNhanHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DiaChiNguoiNhanHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TongSoLuongContainer] [numeric] (3, 0) NOT NULL,
[SoLuongHang] [numeric] (8, 0) NOT NULL,
[DVTSoLuong] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TongTrongLuongHang] [numeric] (10, 3) NULL,
[DVTTrongLuong] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_BranchDetail_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_BranchDetail_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_BranchDetail_Log
ON t_KDT_VNACCS_BranchDetail
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BranchDetail_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_BranchDetail_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BranchDetail_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACCS_BranchDetail_TransportEquipments_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_BranchDetail_TransportEquipments_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments_Log]
(
[ID] [bigint] NOT NULL,
[BranchDetail_ID] [bigint] NOT NULL,
[SoContainer] [nvarchar] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoSeal] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_BranchDetail_TransportEquipments_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_BranchDetail_TransportEquipments_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_BranchDetail_TransportEquipments_Log
ON t_KDT_VNACCS_BranchDetail_TransportEquipments
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BranchDetail_TransportEquipments_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_BranchDetail_TransportEquipments_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BranchDetail_TransportEquipments_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO
IF OBJECT_ID('t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log]
(
[ID] [bigint] NOT NULL ,
[TrangThaiXuLy] [int] NOT NULL,
[SoTN] [bigint] NULL,
[NgayTN] [datetime] NULL,
[MaHQ] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NamQuyetToan] [numeric] (4, 0) NOT NULL,
[LoaiHinhBaoCao] [numeric] (1, 0) NOT NULL,
[DVTBaoCao] [numeric] (1, 0) NOT NULL,
[GhiChuKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Content] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GuidStr] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

IF OBJECT_ID('trg_t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log
ON t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log') IS NOT NULL
DROP TABLE dbo.t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log
GO
CREATE TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log]
(
[ID] [bigint] NOT NULL ,
[GoodItem_ID] [bigint] NULL,
[STT] [numeric] (5, 0) NOT NULL,
[LoaiHangHoa] [numeric] (5, 0) NOT NULL,
[TaiKhoan] [numeric] (5, 0) NOT NULL,
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TonDauKy] [numeric] (18, 4) NOT NULL,
[NhapTrongKy] [numeric] (18, 4) NOT NULL,
[XuatTrongKy] [numeric] (18, 4) NOT NULL,
[TonCuoiKy] [numeric] (18, 4) NOT NULL,
[GhiChu] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO
IF OBJECT_ID('trg_t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log') IS NOT NULL
DROP TRIGGER trg_t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log
GO

CREATE TRIGGER trg_t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log
ON t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log
		SELECT *,GETDATE(),'Inserted' FROM Inserted
	END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
	BEGIN
        INSERT INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log
		SELECT *,GETDATE(),'Updated' FROM Inserted
	END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
	BEGIN
        INSERT INTO t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_Log
		SELECT *,GETDATE(),'Deleted' FROM Deleted
	END
END
GO

IF OBJECT_ID('T_KDT_VNACCS_WarehouseImport_Log') IS NOT NULL
DROP TABLE  dbo.T_KDT_VNACCS_WarehouseImport_Log
GO
IF OBJECT_ID('T_KDT_VNACCS_WarehouseImport_Details_Log') IS NOT NULL
DROP TABLE  dbo.T_KDT_VNACCS_WarehouseImport_Details_Log
GO
IF OBJECT_ID('T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log') IS NOT NULL
DROP TABLE  dbo.T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log
GO
IF OBJECT_ID('T_KDT_VNACCS_WarehouseExport_Log') IS NOT NULL
DROP TABLE  dbo.T_KDT_VNACCS_WarehouseExport_Log
GO
IF OBJECT_ID('T_KDT_VNACCS_WarehouseExport_Details_Log') IS NOT NULL
DROP TABLE  dbo.T_KDT_VNACCS_WarehouseExport_Details_Log
GO
IF OBJECT_ID('T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log') IS NOT NULL
DROP TABLE  dbo.T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log
GO


CREATE TABLE [dbo].[T_KDT_VNACCS_WarehouseImport_Log]
(
[ID] [bigint] NOT NULL ,
[TrangThaiXuLy] [int] NOT NULL,
[SoTN] [bigint] NULL,
[NgayTN] [datetime] NULL,
[NgayBatDauBC] [datetime] NOT NULL,
[NgayKetthucBC] [datetime] NOT NULL,
[MaHQ] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenKho] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaKho] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoTKChungTu] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Loai] [numeric] (2, 0) NOT NULL,
[SoHopDong] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHopDong] [datetime] NULL,
[MaHQTiepNhanHD] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHetHanHD] [datetime] NULL,
[GhiChuKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GuidStr] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_KDT_VNACCS_WarehouseImport_Details_Log]
(
[ID] [bigint] NOT NULL ,
[WarehouseImport_ID] [bigint] NULL,
[STT] [numeric] (5, 0) NOT NULL,
[SoPhieuNhap] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayPhieuNhap] [datetime] NOT NULL,
[TenNguoiGiaoHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguoiGiaoHang] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log]
(
[ID] [bigint] NOT NULL ,
[WarehouseImport_Details_ID] [bigint] NULL,
[STT] [numeric] (5, 0) NOT NULL,
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoaiHangHoa] [numeric] (2, 0) NOT NULL,
[MaDinhDanhSX] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NguonNhap] [numeric] (3, 0) NOT NULL,
[SoLuongDuKienNhap] [numeric] (18, 4) NOT NULL,
[SoLuongThucNhap] [numeric] (18, 4) NOT NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_KDT_VNACCS_WarehouseExport_Log]
(
[ID] [bigint] NOT NULL ,
[TrangThaiXuLy] [int] NOT NULL,
[SoTN] [bigint] NULL,
[NgayTN] [datetime] NULL,
[NgayBatDauBC] [datetime] NOT NULL,
[NgayKetthucBC] [datetime] NOT NULL,
[MaHQ] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaDoanhNghiep] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenKho] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaKho] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoTKChungTu] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Loai] [numeric] (2, 0) NOT NULL,
[SoHopDong] [nvarchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHopDong] [datetime] NULL,
[MaHQTiepNhanHD] [nvarchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHetHanHD] [datetime] NULL,
[GhiChuKhac] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GuidStr] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_KDT_VNACCS_WarehouseExport_Details_Log]
(
[ID] [bigint] NOT NULL ,
[WarehouseExport_ID] [bigint] NULL,
[STT] [numeric] (5, 0) NOT NULL,
[SoPhieuXuat] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayPhieuXuat] [datetime] NOT NULL,
[TenNguoiNhanHang] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaNguoiNhanHang] [nvarchar] (17) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log]
(
[ID] [bigint] NOT NULL ,
[WarehouseExport_Details_ID] [bigint] NULL,
[STT] [numeric] (5, 0) NOT NULL,
[TenHangHoa] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaHangHoa] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoaiHangHoa] [numeric] (2, 0) NOT NULL,
[MaDinhDanhSX] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MucDichSuDung] [numeric] (3, 0) NOT NULL,
[SoLuongDuKienXuat] [numeric] (18, 4) NOT NULL,
[SoLuongThucXuat] [numeric] (18, 4) NOT NULL,
[DVT] [nvarchar] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateLog] DATETIME NULL,
[Status] NVARCHAR(50) NULL
) ON [PRIMARY]
GO

IF OBJECT_ID('trg_T_KDT_VNACCS_WarehouseImport_Log') IS NOT NULL
DROP TRIGGER trg_T_KDT_VNACCS_WarehouseImport_Log
GO

CREATE TRIGGER trg_T_KDT_VNACCS_WarehouseImport_Log
ON T_KDT_VNACCS_WarehouseImport
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('trg_T_KDT_VNACCS_WarehouseImport_Details_Log') IS NOT NULL
DROP TRIGGER trg_T_KDT_VNACCS_WarehouseImport_Details_Log
GO

CREATE TRIGGER trg_T_KDT_VNACCS_WarehouseImport_Details_Log
ON T_KDT_VNACCS_WarehouseImport_Details
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_Details_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_Details_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_Details_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('trg_T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log') IS NOT NULL
DROP TRIGGER trg_T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log
GO

CREATE TRIGGER trg_T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log
ON T_KDT_VNACCS_WarehouseImport_GoodsDetails
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseImport_GoodsDetails_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('trg_T_KDT_VNACCS_WarehouseExport_Log') IS NOT NULL
DROP TRIGGER trg_T_KDT_VNACCS_WarehouseExport_Log
GO

CREATE TRIGGER trg_T_KDT_VNACCS_WarehouseExport_Log
ON T_KDT_VNACCS_WarehouseExport
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('trg_T_KDT_VNACCS_WarehouseExport_Details_Log') IS NOT NULL
DROP TRIGGER trg_T_KDT_VNACCS_WarehouseExport_Details_Log
GO

CREATE TRIGGER trg_T_KDT_VNACCS_WarehouseExport_Details_Log
ON T_KDT_VNACCS_WarehouseExport_Details
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_Details_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_Details_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_Details_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

IF OBJECT_ID('trg_T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log') IS NOT NULL
DROP TRIGGER trg_T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log
GO

CREATE TRIGGER trg_T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log
ON T_KDT_VNACCS_WarehouseExport_GoodsDetails
AFTER INSERT,UPDATE,DELETE AS
BEGIN
IF EXISTS (SELECT * FROM INSERTED) AND NOT EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log
  SELECT *,GETDATE(),'Inserted' FROM Inserted
 END
IF EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED) 
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log
  SELECT *,GETDATE(),'Updated' FROM Inserted
 END
IF NOT EXISTS (SELECT * FROM INSERTED) AND EXISTS (SELECT * FROM DELETED)
 BEGIN
        INSERT INTO T_KDT_VNACCS_WarehouseExport_GoodsDetails_Log
  SELECT *,GETDATE(),'Deleted' FROM Deleted
 END
END
GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.3',GETDATE(), N'CẬP NHẬT GHI LOG ĐỊNH DANH - TÁCH VẬN ĐƠN - BC QUYẾT TOÁN - PHIẾU XUẤT-NHẬP')
END