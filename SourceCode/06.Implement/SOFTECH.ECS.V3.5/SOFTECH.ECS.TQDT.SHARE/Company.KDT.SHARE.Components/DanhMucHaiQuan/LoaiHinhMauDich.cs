﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class LoaiHinhMauDich
    {
        public static List<LoaiHinhMauDich> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<LoaiHinhMauDich> collection = new List<LoaiHinhMauDich>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                LoaiHinhMauDich entity = new LoaiHinhMauDich();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.Ten = dataSet.Tables[0].Rows[i]["Ten"].ToString();
                entity.Ten_VT = dataSet.Tables[0].Rows[i]["Ten_VT"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
