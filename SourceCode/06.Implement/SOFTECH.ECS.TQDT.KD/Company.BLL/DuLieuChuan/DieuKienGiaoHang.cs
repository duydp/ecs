using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KD.BLL.DuLieuChuan
{
    public class DieuKienGiaoHang : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_DieuKienGiaoHang ORDER BY ID";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        //Lypt update date 19/01/2010
        public static string SelectName(string id)
        {
            string query = "SELECT ten FROM t_HaiQuan_DieuKienGiaoHang WHERE ID=" + id;
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteScalar(dbCommand).ToString();
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_DieuKienGiaoHang VALUES(@Id,@Mota)";
            string update = "UPDATE t_HaiQuan_DieuKienGiaoHang SET Mota = @Mota WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_DieuKienGiaoHang WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Mota", SqlDbType.NVarChar, "Mota", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Mota", SqlDbType.NVarChar, "Mota", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}