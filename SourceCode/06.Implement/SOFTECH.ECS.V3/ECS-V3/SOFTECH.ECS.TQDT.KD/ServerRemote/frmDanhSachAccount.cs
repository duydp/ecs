﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;

namespace ServerRemote
{
    public partial class frmDanhSachAccount : Form
    {
        public frmDanhSachAccount()
        {
            InitializeComponent();
        }

        private ISignMessage service = WebService.SignMessage();
        private DataTable dt;
        private void frmDanhSachAccount_Load(object sender, EventArgs e)
        {
            dt = service.GetDaiLy(Globals.UserNameLogin, Globals.PasswordLogin);
            if (dt == null)
                MessageBox.Show("Không tìm thấy danh sách đại lý của doanh nghiệp");
            else
            {
                loadData();
            }


        }
        private void loadData()
        {
            listView1.Items.Clear();
            if (dt != null)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ListViewItem item = new ListViewItem();
                    item.SubItems[0].Text = dr["HO_TEN"].ToString();
                    item.SubItems.Add(dr["USER_NAME"].ToString());
                    item.SubItems.Add(dr["MO_TA"].ToString());
                    listView1.Items.Add(item);
                    
                }
            }
        }

        private void tlsDoiMatKhau_Click(object sender, EventArgs e)
        {

            ListViewItem lvitem = listView1.SelectedItems.Count > 0 ? listView1.SelectedItems[0] : null;
            if (lvitem != null)
            {
                frmChangePassDL f = new frmChangePassDL();
                f.User = lvitem.SubItems[1].Text;
                f.Ten = lvitem.SubItems[2].Text;
                f.openType = ServerRemote.frmChangePassDL.OpenType.DaiLy;
                f.ShowDialog(this);
            }
            
              
        }
    }

        
}
