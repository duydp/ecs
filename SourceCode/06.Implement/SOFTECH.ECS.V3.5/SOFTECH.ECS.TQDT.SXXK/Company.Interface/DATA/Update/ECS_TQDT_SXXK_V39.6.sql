-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Insert]
	@MATK varchar(10),
	@TENTK nvarchar(255),
	@LOAITK varchar(10),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_TAIKHOAN]
(
	[MATK],
	[TENTK],
	[LOAITK]
)
VALUES 
(
	@MATK,
	@TENTK,
	@LOAITK
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Update]
	@ID bigint,
	@MATK varchar(10),
	@TENTK nvarchar(255),
	@LOAITK varchar(10)
AS

UPDATE
	[dbo].[T_KHOKETOAN_TAIKHOAN]
SET
	[MATK] = @MATK,
	[TENTK] = @TENTK,
	[LOAITK] = @LOAITK
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_InsertUpdate]
	@ID bigint,
	@MATK varchar(10),
	@TENTK nvarchar(255),
	@LOAITK varchar(10)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_TAIKHOAN] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_TAIKHOAN] 
		SET
			[MATK] = @MATK,
			[TENTK] = @TENTK,
			[LOAITK] = @LOAITK
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_TAIKHOAN]
		(
			[MATK],
			[TENTK],
			[LOAITK]
		)
		VALUES 
		(
			@MATK,
			@TENTK,
			@LOAITK
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_TAIKHOAN]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_TAIKHOAN] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MATK],
	[TENTK],
	[LOAITK]
FROM
	[dbo].[T_KHOKETOAN_TAIKHOAN]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MATK],
	[TENTK],
	[LOAITK]
FROM [dbo].[T_KHOKETOAN_TAIKHOAN] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_TAIKHOAN_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MATK],
	[TENTK],
	[LOAITK]
FROM
	[dbo].[T_KHOKETOAN_TAIKHOAN]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectBy_KHOSP_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectBy_KHOSP_ID]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteBy_KHOSP_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteBy_KHOSP_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Insert]
	@KHOSP_ID bigint,
	@HOPDONG_ID bigint,
	@MASP nvarchar(50),
	@TENSP nvarchar(255),
	@DVT varchar(10),
	@MASPMAP nvarchar(50),
	@TENSPMAP nvarchar(255),
	@DVTMAP varchar(10),
	@TYLEQD numeric(18, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_SANPHAM_MAP]
(
	[KHOSP_ID],
	[HOPDONG_ID],
	[MASP],
	[TENSP],
	[DVT],
	[MASPMAP],
	[TENSPMAP],
	[DVTMAP],
	[TYLEQD]
)
VALUES 
(
	@KHOSP_ID,
	@HOPDONG_ID,
	@MASP,
	@TENSP,
	@DVT,
	@MASPMAP,
	@TENSPMAP,
	@DVTMAP,
	@TYLEQD
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Update]
	@ID bigint,
	@KHOSP_ID bigint,
	@HOPDONG_ID bigint,
	@MASP nvarchar(50),
	@TENSP nvarchar(255),
	@DVT varchar(10),
	@MASPMAP nvarchar(50),
	@TENSPMAP nvarchar(255),
	@DVTMAP varchar(10),
	@TYLEQD numeric(18, 6)
AS

UPDATE
	[dbo].[T_KHOKETOAN_SANPHAM_MAP]
SET
	[KHOSP_ID] = @KHOSP_ID,
	[HOPDONG_ID] = @HOPDONG_ID,
	[MASP] = @MASP,
	[TENSP] = @TENSP,
	[DVT] = @DVT,
	[MASPMAP] = @MASPMAP,
	[TENSPMAP] = @TENSPMAP,
	[DVTMAP] = @DVTMAP,
	[TYLEQD] = @TYLEQD
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_InsertUpdate]
	@ID bigint,
	@KHOSP_ID bigint,
	@HOPDONG_ID bigint,
	@MASP nvarchar(50),
	@TENSP nvarchar(255),
	@DVT varchar(10),
	@MASPMAP nvarchar(50),
	@TENSPMAP nvarchar(255),
	@DVTMAP varchar(10),
	@TYLEQD numeric(18, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_SANPHAM_MAP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_SANPHAM_MAP] 
		SET
			[KHOSP_ID] = @KHOSP_ID,
			[HOPDONG_ID] = @HOPDONG_ID,
			[MASP] = @MASP,
			[TENSP] = @TENSP,
			[DVT] = @DVT,
			[MASPMAP] = @MASPMAP,
			[TENSPMAP] = @TENSPMAP,
			[DVTMAP] = @DVTMAP,
			[TYLEQD] = @TYLEQD
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_SANPHAM_MAP]
		(
			[KHOSP_ID],
			[HOPDONG_ID],
			[MASP],
			[TENSP],
			[DVT],
			[MASPMAP],
			[TENSPMAP],
			[DVTMAP],
			[TYLEQD]
		)
		VALUES 
		(
			@KHOSP_ID,
			@HOPDONG_ID,
			@MASP,
			@TENSP,
			@DVT,
			@MASPMAP,
			@TENSPMAP,
			@DVTMAP,
			@TYLEQD
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_SANPHAM_MAP]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteBy_KHOSP_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteBy_KHOSP_ID]
	@KHOSP_ID bigint
AS

DELETE FROM [dbo].[T_KHOKETOAN_SANPHAM_MAP]
WHERE
	[KHOSP_ID] = @KHOSP_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_SANPHAM_MAP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[KHOSP_ID],
	[HOPDONG_ID],
	[MASP],
	[TENSP],
	[DVT],
	[MASPMAP],
	[TENSPMAP],
	[DVTMAP],
	[TYLEQD]
FROM
	[dbo].[T_KHOKETOAN_SANPHAM_MAP]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectBy_KHOSP_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectBy_KHOSP_ID]
	@KHOSP_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[KHOSP_ID],
	[HOPDONG_ID],
	[MASP],
	[TENSP],
	[DVT],
	[MASPMAP],
	[TENSPMAP],
	[DVTMAP],
	[TYLEQD]
FROM
	[dbo].[T_KHOKETOAN_SANPHAM_MAP]
WHERE
	[KHOSP_ID] = @KHOSP_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[KHOSP_ID],
	[HOPDONG_ID],
	[MASP],
	[TENSP],
	[DVT],
	[MASPMAP],
	[TENSPMAP],
	[DVTMAP],
	[TYLEQD]
FROM [dbo].[T_KHOKETOAN_SANPHAM_MAP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_MAP_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[KHOSP_ID],
	[HOPDONG_ID],
	[MASP],
	[TENSP],
	[DVT],
	[MASPMAP],
	[TENSPMAP],
	[DVTMAP],
	[TYLEQD]
FROM
	[dbo].[T_KHOKETOAN_SANPHAM_MAP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_SANPHAM_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Insert]
	@MASP nvarchar(50),
	@TENSP nvarchar(255),
	@MAHS varchar(12),
	@DVT nvarchar(10),
	@TENTIENGANH varchar(255),
	@GHICHU nvarchar(255),
	@MAKHO varchar(50),
	@TAIKHOAN varchar(10),
	@DONHANG nvarchar(255),
	@NGUOIBAN nvarchar(255),
	@KICHTHUOC nvarchar(255),
	@MAUSAC nvarchar(255),
	@HOPDONG_ID bigint,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_SANPHAM]
(
	[MASP],
	[TENSP],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
)
VALUES 
(
	@MASP,
	@TENSP,
	@MAHS,
	@DVT,
	@TENTIENGANH,
	@GHICHU,
	@MAKHO,
	@TAIKHOAN,
	@DONHANG,
	@NGUOIBAN,
	@KICHTHUOC,
	@MAUSAC,
	@HOPDONG_ID
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Update]
	@ID bigint,
	@MASP nvarchar(50),
	@TENSP nvarchar(255),
	@MAHS varchar(12),
	@DVT nvarchar(10),
	@TENTIENGANH varchar(255),
	@GHICHU nvarchar(255),
	@MAKHO varchar(50),
	@TAIKHOAN varchar(10),
	@DONHANG nvarchar(255),
	@NGUOIBAN nvarchar(255),
	@KICHTHUOC nvarchar(255),
	@MAUSAC nvarchar(255),
	@HOPDONG_ID bigint
AS

UPDATE
	[dbo].[T_KHOKETOAN_SANPHAM]
SET
	[MASP] = @MASP,
	[TENSP] = @TENSP,
	[MAHS] = @MAHS,
	[DVT] = @DVT,
	[TENTIENGANH] = @TENTIENGANH,
	[GHICHU] = @GHICHU,
	[MAKHO] = @MAKHO,
	[TAIKHOAN] = @TAIKHOAN,
	[DONHANG] = @DONHANG,
	[NGUOIBAN] = @NGUOIBAN,
	[KICHTHUOC] = @KICHTHUOC,
	[MAUSAC] = @MAUSAC,
	[HOPDONG_ID] = @HOPDONG_ID
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_InsertUpdate]
	@ID bigint,
	@MASP nvarchar(50),
	@TENSP nvarchar(255),
	@MAHS varchar(12),
	@DVT nvarchar(10),
	@TENTIENGANH varchar(255),
	@GHICHU nvarchar(255),
	@MAKHO varchar(50),
	@TAIKHOAN varchar(10),
	@DONHANG nvarchar(255),
	@NGUOIBAN nvarchar(255),
	@KICHTHUOC nvarchar(255),
	@MAUSAC nvarchar(255),
	@HOPDONG_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_SANPHAM] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_SANPHAM] 
		SET
			[MASP] = @MASP,
			[TENSP] = @TENSP,
			[MAHS] = @MAHS,
			[DVT] = @DVT,
			[TENTIENGANH] = @TENTIENGANH,
			[GHICHU] = @GHICHU,
			[MAKHO] = @MAKHO,
			[TAIKHOAN] = @TAIKHOAN,
			[DONHANG] = @DONHANG,
			[NGUOIBAN] = @NGUOIBAN,
			[KICHTHUOC] = @KICHTHUOC,
			[MAUSAC] = @MAUSAC,
			[HOPDONG_ID] = @HOPDONG_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_SANPHAM]
		(
			[MASP],
			[TENSP],
			[MAHS],
			[DVT],
			[TENTIENGANH],
			[GHICHU],
			[MAKHO],
			[TAIKHOAN],
			[DONHANG],
			[NGUOIBAN],
			[KICHTHUOC],
			[MAUSAC],
			[HOPDONG_ID]
		)
		VALUES 
		(
			@MASP,
			@TENSP,
			@MAHS,
			@DVT,
			@TENTIENGANH,
			@GHICHU,
			@MAKHO,
			@TAIKHOAN,
			@DONHANG,
			@NGUOIBAN,
			@KICHTHUOC,
			@MAUSAC,
			@HOPDONG_ID
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_SANPHAM]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_SANPHAM] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MASP],
	[TENSP],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
FROM
	[dbo].[T_KHOKETOAN_SANPHAM]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MASP],
	[TENSP],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
FROM [dbo].[T_KHOKETOAN_SANPHAM] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_SANPHAM_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_SANPHAM_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MASP],
	[TENSP],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
FROM
	[dbo].[T_KHOKETOAN_SANPHAM]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Insert]
	@LOAICHUNGTU nvarchar(200),
	@TIENTO nvarchar(50),
	@GIATRIPHANSO bigint,
	@DODAISO bigint,
	@HIENTHI nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_QUYTACDANHSO]
(
	[LOAICHUNGTU],
	[TIENTO],
	[GIATRIPHANSO],
	[DODAISO],
	[HIENTHI]
)
VALUES 
(
	@LOAICHUNGTU,
	@TIENTO,
	@GIATRIPHANSO,
	@DODAISO,
	@HIENTHI
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Update]
	@ID bigint,
	@LOAICHUNGTU nvarchar(200),
	@TIENTO nvarchar(50),
	@GIATRIPHANSO bigint,
	@DODAISO bigint,
	@HIENTHI nvarchar(255)
AS

UPDATE
	[dbo].[T_KHOKETOAN_QUYTACDANHSO]
SET
	[LOAICHUNGTU] = @LOAICHUNGTU,
	[TIENTO] = @TIENTO,
	[GIATRIPHANSO] = @GIATRIPHANSO,
	[DODAISO] = @DODAISO,
	[HIENTHI] = @HIENTHI
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_InsertUpdate]
	@ID bigint,
	@LOAICHUNGTU nvarchar(200),
	@TIENTO nvarchar(50),
	@GIATRIPHANSO bigint,
	@DODAISO bigint,
	@HIENTHI nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_QUYTACDANHSO] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_QUYTACDANHSO] 
		SET
			[LOAICHUNGTU] = @LOAICHUNGTU,
			[TIENTO] = @TIENTO,
			[GIATRIPHANSO] = @GIATRIPHANSO,
			[DODAISO] = @DODAISO,
			[HIENTHI] = @HIENTHI
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_QUYTACDANHSO]
		(
			[LOAICHUNGTU],
			[TIENTO],
			[GIATRIPHANSO],
			[DODAISO],
			[HIENTHI]
		)
		VALUES 
		(
			@LOAICHUNGTU,
			@TIENTO,
			@GIATRIPHANSO,
			@DODAISO,
			@HIENTHI
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_QUYTACDANHSO]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_QUYTACDANHSO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LOAICHUNGTU],
	[TIENTO],
	[GIATRIPHANSO],
	[DODAISO],
	[HIENTHI]
FROM
	[dbo].[T_KHOKETOAN_QUYTACDANHSO]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LOAICHUNGTU],
	[TIENTO],
	[GIATRIPHANSO],
	[DODAISO],
	[HIENTHI]
FROM [dbo].[T_KHOKETOAN_QUYTACDANHSO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_QUYTACDANHSO_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LOAICHUNGTU],
	[TIENTO],
	[GIATRIPHANSO],
	[DODAISO],
	[HIENTHI]
FROM
	[dbo].[T_KHOKETOAN_QUYTACDANHSO]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectBy_PHIEUXNKHO_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectBy_PHIEUXNKHO_ID]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteBy_PHIEUXNKHO_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteBy_PHIEUXNKHO_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Insert]
	@PHIEUXNKHO_ID bigint,
	@STT bigint,
	@LOAIHANG varchar(10),
	@MAHANG nvarchar(50),
	@TENHANG nvarchar(255),
	@DVT nvarchar(10),
	@MAHANGMAP nvarchar(50),
	@TENHANGMAP nvarchar(255),
	@DVTMAP nvarchar(10),
	@SOLUONG numeric(18, 6),
	@DONGIA numeric(18, 6),
	@DONGIANT numeric(18, 6),
	@TRIGIANT numeric(18, 6),
	@THANHTIEN numeric(18, 6),
	@MAKHO nvarchar(50),
	@TAIKHOANNO nvarchar(10),
	@TAIKHOANCO nvarchar(10),
	@TYLEQD numeric(18, 6),
	@SOLUONGQD numeric(18, 6),
	@THUEXNK numeric(18, 6),
	@THUETTDB numeric(18, 6),
	@THUEKHAC numeric(18, 6),
	@TRIGIANTSAUPB numeric(18, 6),
	@GHICHU nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
(
	[PHIEUXNKHO_ID],
	[STT],
	[LOAIHANG],
	[MAHANG],
	[TENHANG],
	[DVT],
	[MAHANGMAP],
	[TENHANGMAP],
	[DVTMAP],
	[SOLUONG],
	[DONGIA],
	[DONGIANT],
	[TRIGIANT],
	[THANHTIEN],
	[MAKHO],
	[TAIKHOANNO],
	[TAIKHOANCO],
	[TYLEQD],
	[SOLUONGQD],
	[THUEXNK],
	[THUETTDB],
	[THUEKHAC],
	[TRIGIANTSAUPB],
	[GHICHU]
)
VALUES 
(
	@PHIEUXNKHO_ID,
	@STT,
	@LOAIHANG,
	@MAHANG,
	@TENHANG,
	@DVT,
	@MAHANGMAP,
	@TENHANGMAP,
	@DVTMAP,
	@SOLUONG,
	@DONGIA,
	@DONGIANT,
	@TRIGIANT,
	@THANHTIEN,
	@MAKHO,
	@TAIKHOANNO,
	@TAIKHOANCO,
	@TYLEQD,
	@SOLUONGQD,
	@THUEXNK,
	@THUETTDB,
	@THUEKHAC,
	@TRIGIANTSAUPB,
	@GHICHU
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Update]
	@ID bigint,
	@PHIEUXNKHO_ID bigint,
	@STT bigint,
	@LOAIHANG varchar(10),
	@MAHANG nvarchar(50),
	@TENHANG nvarchar(255),
	@DVT nvarchar(10),
	@MAHANGMAP nvarchar(50),
	@TENHANGMAP nvarchar(255),
	@DVTMAP nvarchar(10),
	@SOLUONG numeric(18, 6),
	@DONGIA numeric(18, 6),
	@DONGIANT numeric(18, 6),
	@TRIGIANT numeric(18, 6),
	@THANHTIEN numeric(18, 6),
	@MAKHO nvarchar(50),
	@TAIKHOANNO nvarchar(10),
	@TAIKHOANCO nvarchar(10),
	@TYLEQD numeric(18, 6),
	@SOLUONGQD numeric(18, 6),
	@THUEXNK numeric(18, 6),
	@THUETTDB numeric(18, 6),
	@THUEKHAC numeric(18, 6),
	@TRIGIANTSAUPB numeric(18, 6),
	@GHICHU nvarchar(255)
AS

UPDATE
	[dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
SET
	[PHIEUXNKHO_ID] = @PHIEUXNKHO_ID,
	[STT] = @STT,
	[LOAIHANG] = @LOAIHANG,
	[MAHANG] = @MAHANG,
	[TENHANG] = @TENHANG,
	[DVT] = @DVT,
	[MAHANGMAP] = @MAHANGMAP,
	[TENHANGMAP] = @TENHANGMAP,
	[DVTMAP] = @DVTMAP,
	[SOLUONG] = @SOLUONG,
	[DONGIA] = @DONGIA,
	[DONGIANT] = @DONGIANT,
	[TRIGIANT] = @TRIGIANT,
	[THANHTIEN] = @THANHTIEN,
	[MAKHO] = @MAKHO,
	[TAIKHOANNO] = @TAIKHOANNO,
	[TAIKHOANCO] = @TAIKHOANCO,
	[TYLEQD] = @TYLEQD,
	[SOLUONGQD] = @SOLUONGQD,
	[THUEXNK] = @THUEXNK,
	[THUETTDB] = @THUETTDB,
	[THUEKHAC] = @THUEKHAC,
	[TRIGIANTSAUPB] = @TRIGIANTSAUPB,
	[GHICHU] = @GHICHU
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_InsertUpdate]
	@ID bigint,
	@PHIEUXNKHO_ID bigint,
	@STT bigint,
	@LOAIHANG varchar(10),
	@MAHANG nvarchar(50),
	@TENHANG nvarchar(255),
	@DVT nvarchar(10),
	@MAHANGMAP nvarchar(50),
	@TENHANGMAP nvarchar(255),
	@DVTMAP nvarchar(10),
	@SOLUONG numeric(18, 6),
	@DONGIA numeric(18, 6),
	@DONGIANT numeric(18, 6),
	@TRIGIANT numeric(18, 6),
	@THANHTIEN numeric(18, 6),
	@MAKHO nvarchar(50),
	@TAIKHOANNO nvarchar(10),
	@TAIKHOANCO nvarchar(10),
	@TYLEQD numeric(18, 6),
	@SOLUONGQD numeric(18, 6),
	@THUEXNK numeric(18, 6),
	@THUETTDB numeric(18, 6),
	@THUEKHAC numeric(18, 6),
	@TRIGIANTSAUPB numeric(18, 6),
	@GHICHU nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA] 
		SET
			[PHIEUXNKHO_ID] = @PHIEUXNKHO_ID,
			[STT] = @STT,
			[LOAIHANG] = @LOAIHANG,
			[MAHANG] = @MAHANG,
			[TENHANG] = @TENHANG,
			[DVT] = @DVT,
			[MAHANGMAP] = @MAHANGMAP,
			[TENHANGMAP] = @TENHANGMAP,
			[DVTMAP] = @DVTMAP,
			[SOLUONG] = @SOLUONG,
			[DONGIA] = @DONGIA,
			[DONGIANT] = @DONGIANT,
			[TRIGIANT] = @TRIGIANT,
			[THANHTIEN] = @THANHTIEN,
			[MAKHO] = @MAKHO,
			[TAIKHOANNO] = @TAIKHOANNO,
			[TAIKHOANCO] = @TAIKHOANCO,
			[TYLEQD] = @TYLEQD,
			[SOLUONGQD] = @SOLUONGQD,
			[THUEXNK] = @THUEXNK,
			[THUETTDB] = @THUETTDB,
			[THUEKHAC] = @THUEKHAC,
			[TRIGIANTSAUPB] = @TRIGIANTSAUPB,
			[GHICHU] = @GHICHU
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
		(
			[PHIEUXNKHO_ID],
			[STT],
			[LOAIHANG],
			[MAHANG],
			[TENHANG],
			[DVT],
			[MAHANGMAP],
			[TENHANGMAP],
			[DVTMAP],
			[SOLUONG],
			[DONGIA],
			[DONGIANT],
			[TRIGIANT],
			[THANHTIEN],
			[MAKHO],
			[TAIKHOANNO],
			[TAIKHOANCO],
			[TYLEQD],
			[SOLUONGQD],
			[THUEXNK],
			[THUETTDB],
			[THUEKHAC],
			[TRIGIANTSAUPB],
			[GHICHU]
		)
		VALUES 
		(
			@PHIEUXNKHO_ID,
			@STT,
			@LOAIHANG,
			@MAHANG,
			@TENHANG,
			@DVT,
			@MAHANGMAP,
			@TENHANGMAP,
			@DVTMAP,
			@SOLUONG,
			@DONGIA,
			@DONGIANT,
			@TRIGIANT,
			@THANHTIEN,
			@MAKHO,
			@TAIKHOANNO,
			@TAIKHOANCO,
			@TYLEQD,
			@SOLUONGQD,
			@THUEXNK,
			@THUETTDB,
			@THUEKHAC,
			@TRIGIANTSAUPB,
			@GHICHU
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteBy_PHIEUXNKHO_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteBy_PHIEUXNKHO_ID]
	@PHIEUXNKHO_ID bigint
AS

DELETE FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
WHERE
	[PHIEUXNKHO_ID] = @PHIEUXNKHO_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[PHIEUXNKHO_ID],
	[STT],
	[LOAIHANG],
	[MAHANG],
	[TENHANG],
	[DVT],
	[MAHANGMAP],
	[TENHANGMAP],
	[DVTMAP],
	[SOLUONG],
	[DONGIA],
	[DONGIANT],
	[TRIGIANT],
	[THANHTIEN],
	[MAKHO],
	[TAIKHOANNO],
	[TAIKHOANCO],
	[TYLEQD],
	[SOLUONGQD],
	[THUEXNK],
	[THUETTDB],
	[THUEKHAC],
	[TRIGIANTSAUPB],
	[GHICHU]
FROM
	[dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectBy_PHIEUXNKHO_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectBy_PHIEUXNKHO_ID]
	@PHIEUXNKHO_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[PHIEUXNKHO_ID],
	[STT],
	[LOAIHANG],
	[MAHANG],
	[TENHANG],
	[DVT],
	[MAHANGMAP],
	[TENHANGMAP],
	[DVTMAP],
	[SOLUONG],
	[DONGIA],
	[DONGIANT],
	[TRIGIANT],
	[THANHTIEN],
	[MAKHO],
	[TAIKHOANNO],
	[TAIKHOANCO],
	[TYLEQD],
	[SOLUONGQD],
	[THUEXNK],
	[THUETTDB],
	[THUEKHAC],
	[TRIGIANTSAUPB],
	[GHICHU]
FROM
	[dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]
WHERE
	[PHIEUXNKHO_ID] = @PHIEUXNKHO_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[PHIEUXNKHO_ID],
	[STT],
	[LOAIHANG],
	[MAHANG],
	[TENHANG],
	[DVT],
	[MAHANGMAP],
	[TENHANGMAP],
	[DVTMAP],
	[SOLUONG],
	[DONGIA],
	[DONGIANT],
	[TRIGIANT],
	[THANHTIEN],
	[MAKHO],
	[TAIKHOANNO],
	[TAIKHOANCO],
	[TYLEQD],
	[SOLUONGQD],
	[THUEXNK],
	[THUETTDB],
	[THUEKHAC],
	[TRIGIANTSAUPB],
	[GHICHU]
FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectAll]

























AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[PHIEUXNKHO_ID],
	[STT],
	[LOAIHANG],
	[MAHANG],
	[TENHANG],
	[DVT],
	[MAHANGMAP],
	[TENHANGMAP],
	[DVTMAP],
	[SOLUONG],
	[DONGIA],
	[DONGIANT],
	[TRIGIANT],
	[THANHTIEN],
	[MAKHO],
	[TAIKHOANNO],
	[TAIKHOANCO],
	[TYLEQD],
	[SOLUONGQD],
	[THUEXNK],
	[THUETTDB],
	[THUEKHAC],
	[TRIGIANTSAUPB],
	[GHICHU]
FROM
	[dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Insert]
	@TRANGTHAI int,
	@SOCT nvarchar(255),
	@NGAYCT datetime,
	@LOAICHUNGTU varchar(5),
	@SOCTGOC nvarchar(255),
	@NGAYCTGOC datetime,
	@SOTK numeric(12, 0),
	@NGAYTK datetime,
	@MAHQ varchar(10),
	@SOHOADON nvarchar(255),
	@NGAYHOADON datetime,
	@HOPDONG_ID bigint,
	@SOHOPDONG nvarchar(255),
	@NGAYHOPDONG datetime,
	@MAKH nvarchar(50),
	@TENKH nvarchar(255),
	@DIACHIKH nvarchar(255),
	@MADN nvarchar(50),
	@TENDN nvarchar(255),
	@DIACHIDN nvarchar(255),
	@DIENGIAI nvarchar(255),
	@NGUOIVANCHUYEN nvarchar(255),
	@MAKHO nvarchar(50),
	@TENKHO nvarchar(255),
	@MAKHOCHUYEN nvarchar(50),
	@TENKHOCHUYEN nvarchar(255),
	@TKKHO nvarchar(10),
	@LOAIHANGHOA nvarchar(10),
	@TONGTIENHANG numeric(18, 6),
	@MANGUYENTE nvarchar(10),
	@TYGIA decimal(18, 0),
	@GHICHU nvarchar(255),
	@TONGLUONGHANG numeric(18, 6),
	@PHIVANCHUYEN numeric(18, 6),
	@CHIPHI numeric(18, 6),
	@GHICHUCHIPHI nvarchar(255),
	@MAPO nvarchar(255),
	@PHANBO bit,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_PHIEUXNKHO]
(
	[TRANGTHAI],
	[SOCT],
	[NGAYCT],
	[LOAICHUNGTU],
	[SOCTGOC],
	[NGAYCTGOC],
	[SOTK],
	[NGAYTK],
	[MAHQ],
	[SOHOADON],
	[NGAYHOADON],
	[HOPDONG_ID],
	[SOHOPDONG],
	[NGAYHOPDONG],
	[MAKH],
	[TENKH],
	[DIACHIKH],
	[MADN],
	[TENDN],
	[DIACHIDN],
	[DIENGIAI],
	[NGUOIVANCHUYEN],
	[MAKHO],
	[TENKHO],
	[MAKHOCHUYEN],
	[TENKHOCHUYEN],
	[TKKHO],
	[LOAIHANGHOA],
	[TONGTIENHANG],
	[MANGUYENTE],
	[TYGIA],
	[GHICHU],
	[TONGLUONGHANG],
	[PHIVANCHUYEN],
	[CHIPHI],
	[GHICHUCHIPHI],
	[MAPO],
	[PHANBO]
)
VALUES 
(
	@TRANGTHAI,
	@SOCT,
	@NGAYCT,
	@LOAICHUNGTU,
	@SOCTGOC,
	@NGAYCTGOC,
	@SOTK,
	@NGAYTK,
	@MAHQ,
	@SOHOADON,
	@NGAYHOADON,
	@HOPDONG_ID,
	@SOHOPDONG,
	@NGAYHOPDONG,
	@MAKH,
	@TENKH,
	@DIACHIKH,
	@MADN,
	@TENDN,
	@DIACHIDN,
	@DIENGIAI,
	@NGUOIVANCHUYEN,
	@MAKHO,
	@TENKHO,
	@MAKHOCHUYEN,
	@TENKHOCHUYEN,
	@TKKHO,
	@LOAIHANGHOA,
	@TONGTIENHANG,
	@MANGUYENTE,
	@TYGIA,
	@GHICHU,
	@TONGLUONGHANG,
	@PHIVANCHUYEN,
	@CHIPHI,
	@GHICHUCHIPHI,
	@MAPO,
	@PHANBO
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Update]
	@ID bigint,
	@TRANGTHAI int,
	@SOCT nvarchar(255),
	@NGAYCT datetime,
	@LOAICHUNGTU varchar(5),
	@SOCTGOC nvarchar(255),
	@NGAYCTGOC datetime,
	@SOTK numeric(12, 0),
	@NGAYTK datetime,
	@MAHQ varchar(10),
	@SOHOADON nvarchar(255),
	@NGAYHOADON datetime,
	@HOPDONG_ID bigint,
	@SOHOPDONG nvarchar(255),
	@NGAYHOPDONG datetime,
	@MAKH nvarchar(50),
	@TENKH nvarchar(255),
	@DIACHIKH nvarchar(255),
	@MADN nvarchar(50),
	@TENDN nvarchar(255),
	@DIACHIDN nvarchar(255),
	@DIENGIAI nvarchar(255),
	@NGUOIVANCHUYEN nvarchar(255),
	@MAKHO nvarchar(50),
	@TENKHO nvarchar(255),
	@MAKHOCHUYEN nvarchar(50),
	@TENKHOCHUYEN nvarchar(255),
	@TKKHO nvarchar(10),
	@LOAIHANGHOA nvarchar(10),
	@TONGTIENHANG numeric(18, 6),
	@MANGUYENTE nvarchar(10),
	@TYGIA decimal(18, 0),
	@GHICHU nvarchar(255),
	@TONGLUONGHANG numeric(18, 6),
	@PHIVANCHUYEN numeric(18, 6),
	@CHIPHI numeric(18, 6),
	@GHICHUCHIPHI nvarchar(255),
	@MAPO nvarchar(255),
	@PHANBO bit
AS

UPDATE
	[dbo].[T_KHOKETOAN_PHIEUXNKHO]
SET
	[TRANGTHAI] = @TRANGTHAI,
	[SOCT] = @SOCT,
	[NGAYCT] = @NGAYCT,
	[LOAICHUNGTU] = @LOAICHUNGTU,
	[SOCTGOC] = @SOCTGOC,
	[NGAYCTGOC] = @NGAYCTGOC,
	[SOTK] = @SOTK,
	[NGAYTK] = @NGAYTK,
	[MAHQ] = @MAHQ,
	[SOHOADON] = @SOHOADON,
	[NGAYHOADON] = @NGAYHOADON,
	[HOPDONG_ID] = @HOPDONG_ID,
	[SOHOPDONG] = @SOHOPDONG,
	[NGAYHOPDONG] = @NGAYHOPDONG,
	[MAKH] = @MAKH,
	[TENKH] = @TENKH,
	[DIACHIKH] = @DIACHIKH,
	[MADN] = @MADN,
	[TENDN] = @TENDN,
	[DIACHIDN] = @DIACHIDN,
	[DIENGIAI] = @DIENGIAI,
	[NGUOIVANCHUYEN] = @NGUOIVANCHUYEN,
	[MAKHO] = @MAKHO,
	[TENKHO] = @TENKHO,
	[MAKHOCHUYEN] = @MAKHOCHUYEN,
	[TENKHOCHUYEN] = @TENKHOCHUYEN,
	[TKKHO] = @TKKHO,
	[LOAIHANGHOA] = @LOAIHANGHOA,
	[TONGTIENHANG] = @TONGTIENHANG,
	[MANGUYENTE] = @MANGUYENTE,
	[TYGIA] = @TYGIA,
	[GHICHU] = @GHICHU,
	[TONGLUONGHANG] = @TONGLUONGHANG,
	[PHIVANCHUYEN] = @PHIVANCHUYEN,
	[CHIPHI] = @CHIPHI,
	[GHICHUCHIPHI] = @GHICHUCHIPHI,
	[MAPO] = @MAPO,
	[PHANBO] = @PHANBO
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_InsertUpdate]
	@ID bigint,
	@TRANGTHAI int,
	@SOCT nvarchar(255),
	@NGAYCT datetime,
	@LOAICHUNGTU varchar(5),
	@SOCTGOC nvarchar(255),
	@NGAYCTGOC datetime,
	@SOTK numeric(12, 0),
	@NGAYTK datetime,
	@MAHQ varchar(10),
	@SOHOADON nvarchar(255),
	@NGAYHOADON datetime,
	@HOPDONG_ID bigint,
	@SOHOPDONG nvarchar(255),
	@NGAYHOPDONG datetime,
	@MAKH nvarchar(50),
	@TENKH nvarchar(255),
	@DIACHIKH nvarchar(255),
	@MADN nvarchar(50),
	@TENDN nvarchar(255),
	@DIACHIDN nvarchar(255),
	@DIENGIAI nvarchar(255),
	@NGUOIVANCHUYEN nvarchar(255),
	@MAKHO nvarchar(50),
	@TENKHO nvarchar(255),
	@MAKHOCHUYEN nvarchar(50),
	@TENKHOCHUYEN nvarchar(255),
	@TKKHO nvarchar(10),
	@LOAIHANGHOA nvarchar(10),
	@TONGTIENHANG numeric(18, 6),
	@MANGUYENTE nvarchar(10),
	@TYGIA decimal(18, 0),
	@GHICHU nvarchar(255),
	@TONGLUONGHANG numeric(18, 6),
	@PHIVANCHUYEN numeric(18, 6),
	@CHIPHI numeric(18, 6),
	@GHICHUCHIPHI nvarchar(255),
	@MAPO nvarchar(255),
	@PHANBO bit
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_PHIEUXNKHO] 
		SET
			[TRANGTHAI] = @TRANGTHAI,
			[SOCT] = @SOCT,
			[NGAYCT] = @NGAYCT,
			[LOAICHUNGTU] = @LOAICHUNGTU,
			[SOCTGOC] = @SOCTGOC,
			[NGAYCTGOC] = @NGAYCTGOC,
			[SOTK] = @SOTK,
			[NGAYTK] = @NGAYTK,
			[MAHQ] = @MAHQ,
			[SOHOADON] = @SOHOADON,
			[NGAYHOADON] = @NGAYHOADON,
			[HOPDONG_ID] = @HOPDONG_ID,
			[SOHOPDONG] = @SOHOPDONG,
			[NGAYHOPDONG] = @NGAYHOPDONG,
			[MAKH] = @MAKH,
			[TENKH] = @TENKH,
			[DIACHIKH] = @DIACHIKH,
			[MADN] = @MADN,
			[TENDN] = @TENDN,
			[DIACHIDN] = @DIACHIDN,
			[DIENGIAI] = @DIENGIAI,
			[NGUOIVANCHUYEN] = @NGUOIVANCHUYEN,
			[MAKHO] = @MAKHO,
			[TENKHO] = @TENKHO,
			[MAKHOCHUYEN] = @MAKHOCHUYEN,
			[TENKHOCHUYEN] = @TENKHOCHUYEN,
			[TKKHO] = @TKKHO,
			[LOAIHANGHOA] = @LOAIHANGHOA,
			[TONGTIENHANG] = @TONGTIENHANG,
			[MANGUYENTE] = @MANGUYENTE,
			[TYGIA] = @TYGIA,
			[GHICHU] = @GHICHU,
			[TONGLUONGHANG] = @TONGLUONGHANG,
			[PHIVANCHUYEN] = @PHIVANCHUYEN,
			[CHIPHI] = @CHIPHI,
			[GHICHUCHIPHI] = @GHICHUCHIPHI,
			[MAPO] = @MAPO,
			[PHANBO] = @PHANBO
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_PHIEUXNKHO]
		(
			[TRANGTHAI],
			[SOCT],
			[NGAYCT],
			[LOAICHUNGTU],
			[SOCTGOC],
			[NGAYCTGOC],
			[SOTK],
			[NGAYTK],
			[MAHQ],
			[SOHOADON],
			[NGAYHOADON],
			[HOPDONG_ID],
			[SOHOPDONG],
			[NGAYHOPDONG],
			[MAKH],
			[TENKH],
			[DIACHIKH],
			[MADN],
			[TENDN],
			[DIACHIDN],
			[DIENGIAI],
			[NGUOIVANCHUYEN],
			[MAKHO],
			[TENKHO],
			[MAKHOCHUYEN],
			[TENKHOCHUYEN],
			[TKKHO],
			[LOAIHANGHOA],
			[TONGTIENHANG],
			[MANGUYENTE],
			[TYGIA],
			[GHICHU],
			[TONGLUONGHANG],
			[PHIVANCHUYEN],
			[CHIPHI],
			[GHICHUCHIPHI],
			[MAPO],
			[PHANBO]
		)
		VALUES 
		(
			@TRANGTHAI,
			@SOCT,
			@NGAYCT,
			@LOAICHUNGTU,
			@SOCTGOC,
			@NGAYCTGOC,
			@SOTK,
			@NGAYTK,
			@MAHQ,
			@SOHOADON,
			@NGAYHOADON,
			@HOPDONG_ID,
			@SOHOPDONG,
			@NGAYHOPDONG,
			@MAKH,
			@TENKH,
			@DIACHIKH,
			@MADN,
			@TENDN,
			@DIACHIDN,
			@DIENGIAI,
			@NGUOIVANCHUYEN,
			@MAKHO,
			@TENKHO,
			@MAKHOCHUYEN,
			@TENKHOCHUYEN,
			@TKKHO,
			@LOAIHANGHOA,
			@TONGTIENHANG,
			@MANGUYENTE,
			@TYGIA,
			@GHICHU,
			@TONGLUONGHANG,
			@PHIVANCHUYEN,
			@CHIPHI,
			@GHICHUCHIPHI,
			@MAPO,
			@PHANBO
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_PHIEUXNKHO]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TRANGTHAI],
	[SOCT],
	[NGAYCT],
	[LOAICHUNGTU],
	[SOCTGOC],
	[NGAYCTGOC],
	[SOTK],
	[NGAYTK],
	[MAHQ],
	[SOHOADON],
	[NGAYHOADON],
	[HOPDONG_ID],
	[SOHOPDONG],
	[NGAYHOPDONG],
	[MAKH],
	[TENKH],
	[DIACHIKH],
	[MADN],
	[TENDN],
	[DIACHIDN],
	[DIENGIAI],
	[NGUOIVANCHUYEN],
	[MAKHO],
	[TENKHO],
	[MAKHOCHUYEN],
	[TENKHOCHUYEN],
	[TKKHO],
	[LOAIHANGHOA],
	[TONGTIENHANG],
	[MANGUYENTE],
	[TYGIA],
	[GHICHU],
	[TONGLUONGHANG],
	[PHIVANCHUYEN],
	[CHIPHI],
	[GHICHUCHIPHI],
	[MAPO],
	[PHANBO]
FROM
	[dbo].[T_KHOKETOAN_PHIEUXNKHO]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TRANGTHAI],
	[SOCT],
	[NGAYCT],
	[LOAICHUNGTU],
	[SOCTGOC],
	[NGAYCTGOC],
	[SOTK],
	[NGAYTK],
	[MAHQ],
	[SOHOADON],
	[NGAYHOADON],
	[HOPDONG_ID],
	[SOHOPDONG],
	[NGAYHOPDONG],
	[MAKH],
	[TENKH],
	[DIACHIKH],
	[MADN],
	[TENDN],
	[DIACHIDN],
	[DIENGIAI],
	[NGUOIVANCHUYEN],
	[MAKHO],
	[TENKHO],
	[MAKHOCHUYEN],
	[TENKHOCHUYEN],
	[TKKHO],
	[LOAIHANGHOA],
	[TONGTIENHANG],
	[MANGUYENTE],
	[TYGIA],
	[GHICHU],
	[TONGLUONGHANG],
	[PHIVANCHUYEN],
	[CHIPHI],
	[GHICHUCHIPHI],
	[MAPO],
	[PHANBO]
FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectAll]







































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TRANGTHAI],
	[SOCT],
	[NGAYCT],
	[LOAICHUNGTU],
	[SOCTGOC],
	[NGAYCTGOC],
	[SOTK],
	[NGAYTK],
	[MAHQ],
	[SOHOADON],
	[NGAYHOADON],
	[HOPDONG_ID],
	[SOHOPDONG],
	[NGAYHOPDONG],
	[MAKH],
	[TENKH],
	[DIACHIKH],
	[MADN],
	[TENDN],
	[DIACHIDN],
	[DIENGIAI],
	[NGUOIVANCHUYEN],
	[MAKHO],
	[TENKHO],
	[MAKHOCHUYEN],
	[TENKHOCHUYEN],
	[TKKHO],
	[LOAIHANGHOA],
	[TONGTIENHANG],
	[MANGUYENTE],
	[TYGIA],
	[GHICHU],
	[TONGLUONGHANG],
	[PHIVANCHUYEN],
	[CHIPHI],
	[GHICHUCHIPHI],
	[MAPO],
	[PHANBO]
FROM
	[dbo].[T_KHOKETOAN_PHIEUXNKHO]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectBy_KHONPL_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectBy_KHONPL_ID]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteBy_KHONPL_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteBy_KHONPL_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Insert]
	@KHONPL_ID bigint,
	@HOPDONG_ID bigint,
	@MANPL nvarchar(50),
	@TENNPL nvarchar(255),
	@DVT varchar(10),
	@MANPLMAP nvarchar(50),
	@TENNPLMAP nvarchar(255),
	@DVTMAP varchar(10),
	@TYLEQD numeric(18, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
(
	[KHONPL_ID],
	[HOPDONG_ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[MANPLMAP],
	[TENNPLMAP],
	[DVTMAP],
	[TYLEQD]
)
VALUES 
(
	@KHONPL_ID,
	@HOPDONG_ID,
	@MANPL,
	@TENNPL,
	@DVT,
	@MANPLMAP,
	@TENNPLMAP,
	@DVTMAP,
	@TYLEQD
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Update]
	@ID bigint,
	@KHONPL_ID bigint,
	@HOPDONG_ID bigint,
	@MANPL nvarchar(50),
	@TENNPL nvarchar(255),
	@DVT varchar(10),
	@MANPLMAP nvarchar(50),
	@TENNPLMAP nvarchar(255),
	@DVTMAP varchar(10),
	@TYLEQD numeric(18, 6)
AS

UPDATE
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
SET
	[KHONPL_ID] = @KHONPL_ID,
	[HOPDONG_ID] = @HOPDONG_ID,
	[MANPL] = @MANPL,
	[TENNPL] = @TENNPL,
	[DVT] = @DVT,
	[MANPLMAP] = @MANPLMAP,
	[TENNPLMAP] = @TENNPLMAP,
	[DVTMAP] = @DVTMAP,
	[TYLEQD] = @TYLEQD
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_InsertUpdate]
	@ID bigint,
	@KHONPL_ID bigint,
	@HOPDONG_ID bigint,
	@MANPL nvarchar(50),
	@TENNPL nvarchar(255),
	@DVT varchar(10),
	@MANPLMAP nvarchar(50),
	@TENNPLMAP nvarchar(255),
	@DVTMAP varchar(10),
	@TYLEQD numeric(18, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP] 
		SET
			[KHONPL_ID] = @KHONPL_ID,
			[HOPDONG_ID] = @HOPDONG_ID,
			[MANPL] = @MANPL,
			[TENNPL] = @TENNPL,
			[DVT] = @DVT,
			[MANPLMAP] = @MANPLMAP,
			[TENNPLMAP] = @TENNPLMAP,
			[DVTMAP] = @DVTMAP,
			[TYLEQD] = @TYLEQD
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
		(
			[KHONPL_ID],
			[HOPDONG_ID],
			[MANPL],
			[TENNPL],
			[DVT],
			[MANPLMAP],
			[TENNPLMAP],
			[DVTMAP],
			[TYLEQD]
		)
		VALUES 
		(
			@KHONPL_ID,
			@HOPDONG_ID,
			@MANPL,
			@TENNPL,
			@DVT,
			@MANPLMAP,
			@TENNPLMAP,
			@DVTMAP,
			@TYLEQD
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteBy_KHONPL_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteBy_KHONPL_ID]
	@KHONPL_ID bigint
AS

DELETE FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
WHERE
	[KHONPL_ID] = @KHONPL_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[KHONPL_ID],
	[HOPDONG_ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[MANPLMAP],
	[TENNPLMAP],
	[DVTMAP],
	[TYLEQD]
FROM
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectBy_KHONPL_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectBy_KHONPL_ID]
	@KHONPL_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[KHONPL_ID],
	[HOPDONG_ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[MANPLMAP],
	[TENNPLMAP],
	[DVTMAP],
	[TYLEQD]
FROM
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]
WHERE
	[KHONPL_ID] = @KHONPL_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[KHONPL_ID],
	[HOPDONG_ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[MANPLMAP],
	[TENNPLMAP],
	[DVTMAP],
	[TYLEQD]
FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_MAP_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[KHONPL_ID],
	[HOPDONG_ID],
	[MANPL],
	[TENNPL],
	[DVT],
	[MANPLMAP],
	[TENNPLMAP],
	[DVTMAP],
	[TYLEQD]
FROM
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU_MAP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Insert]
	@MANPL nvarchar(50),
	@TENNPL nvarchar(255),
	@MAHS varchar(12),
	@DVT nvarchar(10),
	@TENTIENGANH varchar(255),
	@GHICHU nvarchar(255),
	@MAKHO varchar(50),
	@TAIKHOAN varchar(10),
	@DONHANG nvarchar(255),
	@NGUOIBAN nvarchar(255),
	@KICHTHUOC nvarchar(255),
	@MAUSAC nvarchar(255),
	@HOPDONG_ID bigint,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_NGUYENPHULIEU]
(
	[MANPL],
	[TENNPL],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
)
VALUES 
(
	@MANPL,
	@TENNPL,
	@MAHS,
	@DVT,
	@TENTIENGANH,
	@GHICHU,
	@MAKHO,
	@TAIKHOAN,
	@DONHANG,
	@NGUOIBAN,
	@KICHTHUOC,
	@MAUSAC,
	@HOPDONG_ID
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Update]
	@ID bigint,
	@MANPL nvarchar(50),
	@TENNPL nvarchar(255),
	@MAHS varchar(12),
	@DVT nvarchar(10),
	@TENTIENGANH varchar(255),
	@GHICHU nvarchar(255),
	@MAKHO varchar(50),
	@TAIKHOAN varchar(10),
	@DONHANG nvarchar(255),
	@NGUOIBAN nvarchar(255),
	@KICHTHUOC nvarchar(255),
	@MAUSAC nvarchar(255),
	@HOPDONG_ID bigint
AS

UPDATE
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU]
SET
	[MANPL] = @MANPL,
	[TENNPL] = @TENNPL,
	[MAHS] = @MAHS,
	[DVT] = @DVT,
	[TENTIENGANH] = @TENTIENGANH,
	[GHICHU] = @GHICHU,
	[MAKHO] = @MAKHO,
	[TAIKHOAN] = @TAIKHOAN,
	[DONHANG] = @DONHANG,
	[NGUOIBAN] = @NGUOIBAN,
	[KICHTHUOC] = @KICHTHUOC,
	[MAUSAC] = @MAUSAC,
	[HOPDONG_ID] = @HOPDONG_ID
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_InsertUpdate]
	@ID bigint,
	@MANPL nvarchar(50),
	@TENNPL nvarchar(255),
	@MAHS varchar(12),
	@DVT nvarchar(10),
	@TENTIENGANH varchar(255),
	@GHICHU nvarchar(255),
	@MAKHO varchar(50),
	@TAIKHOAN varchar(10),
	@DONHANG nvarchar(255),
	@NGUOIBAN nvarchar(255),
	@KICHTHUOC nvarchar(255),
	@MAUSAC nvarchar(255),
	@HOPDONG_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_NGUYENPHULIEU] 
		SET
			[MANPL] = @MANPL,
			[TENNPL] = @TENNPL,
			[MAHS] = @MAHS,
			[DVT] = @DVT,
			[TENTIENGANH] = @TENTIENGANH,
			[GHICHU] = @GHICHU,
			[MAKHO] = @MAKHO,
			[TAIKHOAN] = @TAIKHOAN,
			[DONHANG] = @DONHANG,
			[NGUOIBAN] = @NGUOIBAN,
			[KICHTHUOC] = @KICHTHUOC,
			[MAUSAC] = @MAUSAC,
			[HOPDONG_ID] = @HOPDONG_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_NGUYENPHULIEU]
		(
			[MANPL],
			[TENNPL],
			[MAHS],
			[DVT],
			[TENTIENGANH],
			[GHICHU],
			[MAKHO],
			[TAIKHOAN],
			[DONHANG],
			[NGUOIBAN],
			[KICHTHUOC],
			[MAUSAC],
			[HOPDONG_ID]
		)
		VALUES 
		(
			@MANPL,
			@TENNPL,
			@MAHS,
			@DVT,
			@TENTIENGANH,
			@GHICHU,
			@MAKHO,
			@TAIKHOAN,
			@DONHANG,
			@NGUOIBAN,
			@KICHTHUOC,
			@MAUSAC,
			@HOPDONG_ID
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MANPL],
	[TENNPL],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
FROM
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MANPL],
	[TENNPL],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
FROM [dbo].[T_KHOKETOAN_NGUYENPHULIEU] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_NGUYENPHULIEU_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MANPL],
	[TENNPL],
	[MAHS],
	[DVT],
	[TENTIENGANH],
	[GHICHU],
	[MAKHO],
	[TAIKHOAN],
	[DONHANG],
	[NGUOIBAN],
	[KICHTHUOC],
	[MAUSAC],
	[HOPDONG_ID]
FROM
	[dbo].[T_KHOKETOAN_NGUYENPHULIEU]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DINHMUC_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Insert]
	@MASP nvarchar(200),
	@TENSP nvarchar(200),
	@DVTSP nvarchar(10),
	@MANPL nvarchar(200),
	@TENNPL nvarchar(200),
	@DVTNPL nvarchar(10),
	@DMSD numeric(18, 5),
	@TLHH numeric(18, 5),
	@DMCHUNG numeric(18, 5),
	@GHICHU nvarchar(500),
	@HOPDONG_ID bigint,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_DINHMUC]
(
	[MASP],
	[TENSP],
	[DVTSP],
	[MANPL],
	[TENNPL],
	[DVTNPL],
	[DMSD],
	[TLHH],
	[DMCHUNG],
	[GHICHU],
	[HOPDONG_ID]
)
VALUES 
(
	@MASP,
	@TENSP,
	@DVTSP,
	@MANPL,
	@TENNPL,
	@DVTNPL,
	@DMSD,
	@TLHH,
	@DMCHUNG,
	@GHICHU,
	@HOPDONG_ID
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Update]
	@ID bigint,
	@MASP nvarchar(200),
	@TENSP nvarchar(200),
	@DVTSP nvarchar(10),
	@MANPL nvarchar(200),
	@TENNPL nvarchar(200),
	@DVTNPL nvarchar(10),
	@DMSD numeric(18, 5),
	@TLHH numeric(18, 5),
	@DMCHUNG numeric(18, 5),
	@GHICHU nvarchar(500),
	@HOPDONG_ID bigint
AS

UPDATE
	[dbo].[T_KHOKETOAN_DINHMUC]
SET
	[MASP] = @MASP,
	[TENSP] = @TENSP,
	[DVTSP] = @DVTSP,
	[MANPL] = @MANPL,
	[TENNPL] = @TENNPL,
	[DVTNPL] = @DVTNPL,
	[DMSD] = @DMSD,
	[TLHH] = @TLHH,
	[DMCHUNG] = @DMCHUNG,
	[GHICHU] = @GHICHU,
	[HOPDONG_ID] = @HOPDONG_ID
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_InsertUpdate]
	@ID bigint,
	@MASP nvarchar(200),
	@TENSP nvarchar(200),
	@DVTSP nvarchar(10),
	@MANPL nvarchar(200),
	@TENNPL nvarchar(200),
	@DVTNPL nvarchar(10),
	@DMSD numeric(18, 5),
	@TLHH numeric(18, 5),
	@DMCHUNG numeric(18, 5),
	@GHICHU nvarchar(500),
	@HOPDONG_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_DINHMUC] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_DINHMUC] 
		SET
			[MASP] = @MASP,
			[TENSP] = @TENSP,
			[DVTSP] = @DVTSP,
			[MANPL] = @MANPL,
			[TENNPL] = @TENNPL,
			[DVTNPL] = @DVTNPL,
			[DMSD] = @DMSD,
			[TLHH] = @TLHH,
			[DMCHUNG] = @DMCHUNG,
			[GHICHU] = @GHICHU,
			[HOPDONG_ID] = @HOPDONG_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_DINHMUC]
		(
			[MASP],
			[TENSP],
			[DVTSP],
			[MANPL],
			[TENNPL],
			[DVTNPL],
			[DMSD],
			[TLHH],
			[DMCHUNG],
			[GHICHU],
			[HOPDONG_ID]
		)
		VALUES 
		(
			@MASP,
			@TENSP,
			@DVTSP,
			@MANPL,
			@TENNPL,
			@DVTNPL,
			@DMSD,
			@TLHH,
			@DMCHUNG,
			@GHICHU,
			@HOPDONG_ID
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_DINHMUC]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_DINHMUC] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MASP],
	[TENSP],
	[DVTSP],
	[MANPL],
	[TENNPL],
	[DVTNPL],
	[DMSD],
	[TLHH],
	[DMCHUNG],
	[GHICHU],
	[HOPDONG_ID]
FROM
	[dbo].[T_KHOKETOAN_DINHMUC]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MASP],
	[TENSP],
	[DVTSP],
	[MANPL],
	[TENNPL],
	[DVTNPL],
	[DMSD],
	[TLHH],
	[DMCHUNG],
	[GHICHU],
	[HOPDONG_ID]
FROM [dbo].[T_KHOKETOAN_DINHMUC] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DINHMUC_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DINHMUC_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MASP],
	[TENSP],
	[DVTSP],
	[MANPL],
	[TENNPL],
	[DVTNPL],
	[DMSD],
	[TLHH],
	[DMCHUNG],
	[GHICHU],
	[HOPDONG_ID]
FROM
	[dbo].[T_KHOKETOAN_DINHMUC]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Update]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Load]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Insert]
	@MAKHO nvarchar(200),
	@TENKHO nvarchar(2000),
	@TAIKHOANKHO nvarchar(50),
	@GHICHU nvarchar(2000),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KHOKETOAN_DANHSACHKHO]
(
	[MAKHO],
	[TENKHO],
	[TAIKHOANKHO],
	[GHICHU]
)
VALUES 
(
	@MAKHO,
	@TENKHO,
	@TAIKHOANKHO,
	@GHICHU
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Update]
	@ID bigint,
	@MAKHO nvarchar(200),
	@TENKHO nvarchar(2000),
	@TAIKHOANKHO nvarchar(50),
	@GHICHU nvarchar(2000)
AS

UPDATE
	[dbo].[T_KHOKETOAN_DANHSACHKHO]
SET
	[MAKHO] = @MAKHO,
	[TENKHO] = @TENKHO,
	[TAIKHOANKHO] = @TAIKHOANKHO,
	[GHICHU] = @GHICHU
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_InsertUpdate]
	@ID bigint,
	@MAKHO nvarchar(200),
	@TENKHO nvarchar(2000),
	@TAIKHOANKHO nvarchar(50),
	@GHICHU nvarchar(2000)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KHOKETOAN_DANHSACHKHO] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KHOKETOAN_DANHSACHKHO] 
		SET
			[MAKHO] = @MAKHO,
			[TENKHO] = @TENKHO,
			[TAIKHOANKHO] = @TAIKHOANKHO,
			[GHICHU] = @GHICHU
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KHOKETOAN_DANHSACHKHO]
		(
			[MAKHO],
			[TENKHO],
			[TAIKHOANKHO],
			[GHICHU]
		)
		VALUES 
		(
			@MAKHO,
			@TENKHO,
			@TAIKHOANKHO,
			@GHICHU
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KHOKETOAN_DANHSACHKHO]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KHOKETOAN_DANHSACHKHO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MAKHO],
	[TENKHO],
	[TAIKHOANKHO],
	[GHICHU]
FROM
	[dbo].[T_KHOKETOAN_DANHSACHKHO]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MAKHO],
	[TENKHO],
	[TAIKHOANKHO],
	[GHICHU]
FROM [dbo].[T_KHOKETOAN_DANHSACHKHO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_DANHSACHKHO_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MAKHO],
	[TENKHO],
	[TAIKHOANKHO],
	[GHICHU]
FROM
	[dbo].[T_KHOKETOAN_DANHSACHKHO]	

GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '39.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('39.6',GETDATE(), N'CẬP NHẬT PROCEDURE TẠO TABLE KHO KẾ TOÁN')
END