using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ChungTuDinhKem : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string LoaiChungTu { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string NhomXuLyHoSo { set; get; }
		public string TieuDe { set; get; }
		public decimal SoToKhai { set; get; }
		public string GhiChu { set; get; }
		public string PhanLoaiThuTucKhaiBao { set; get; }
		public string SoDienThoaiNguoiKhaiBao { set; get; }
		public string SoQuanLyTrongNoiBoDoanhNghiep { set; get; }
		public string MaKetQuaXuLy { set; get; }
		public string TenThuTucKhaiBao { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public string TrangThaiKhaiBao { set; get; }
		public DateTime NgaySuaCuoiCung { set; get; }
		public string TenNguoiKhaiBao { set; get; }
		public string DiaChiNguoiKhaiBao { set; get; }
		public decimal SoDeLayTepDinhKem { set; get; }
		public DateTime NgayHoanThanhKiemTraHoSo { set; get; }
		public decimal SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public decimal TongDungLuong { set; get; }
		public string PhanLuong { set; get; }
		public string HuongDan { set; get; }
		public long TKMD_ID { set; get; }
		public DateTime NgayBatDau { set; get; }
		public DateTime NgayCapNhatCuoiCung { set; get; }
		public string CanCuPhapLenh { set; get; }
		public string CoBaoXoa { set; get; }
		public string GhiChuHaiQuan { set; get; }
		public string HinhThucTimKiem { set; get; }
		public string LyDoChinhSua { set; get; }
		public string MaDiaDiemDen { set; get; }
		public string MaNhaVanChuyen { set; get; }
		public string MaPhanLoaiDangKy { set; get; }
		public string MaPhanLoaiXuLy { set; get; }
		public string MaThongTinXuat { set; get; }
		public DateTime NgayCapPhep { set; get; }
		public DateTime NgayThongBao { set; get; }
		public string NguoiGui { set; get; }
		public string NoiDungChinhSua { set; get; }
		public string PhuongTienVanChuyen { set; get; }
		public string SoChuyenDiBien { set; get; }
		public string SoSeri { set; get; }
		public decimal SoTiepNhanToKhaiPhoThong { set; get; }
		public string TrangThaiXuLyHaiQuan { set; get; }
		public string NguoiCapNhatCuoiCung { set; get; }
		public int NhomXuLyHoSoID { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ChungTuDinhKem> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ChungTuDinhKem> collection = new List<KDT_VNACC_ChungTuDinhKem>();
			while (reader.Read())
			{
				KDT_VNACC_ChungTuDinhKem entity = new KDT_VNACC_ChungTuDinhKem();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomXuLyHoSo"))) entity.NhomXuLyHoSo = reader.GetString(reader.GetOrdinal("NhomXuLyHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiThuTucKhaiBao"))) entity.PhanLoaiThuTucKhaiBao = reader.GetString(reader.GetOrdinal("PhanLoaiThuTucKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNguoiKhaiBao"))) entity.SoDienThoaiNguoiKhaiBao = reader.GetString(reader.GetOrdinal("SoDienThoaiNguoiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuanLyTrongNoiBoDoanhNghiep"))) entity.SoQuanLyTrongNoiBoDoanhNghiep = reader.GetString(reader.GetOrdinal("SoQuanLyTrongNoiBoDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaXuLy"))) entity.MaKetQuaXuLy = reader.GetString(reader.GetOrdinal("MaKetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuTucKhaiBao"))) entity.TenThuTucKhaiBao = reader.GetString(reader.GetOrdinal("TenThuTucKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiKhaiBao"))) entity.TrangThaiKhaiBao = reader.GetString(reader.GetOrdinal("TrangThaiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySuaCuoiCung"))) entity.NgaySuaCuoiCung = reader.GetDateTime(reader.GetOrdinal("NgaySuaCuoiCung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhaiBao"))) entity.TenNguoiKhaiBao = reader.GetString(reader.GetOrdinal("TenNguoiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiKhaiBao"))) entity.DiaChiNguoiKhaiBao = reader.GetString(reader.GetOrdinal("DiaChiNguoiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDeLayTepDinhKem"))) entity.SoDeLayTepDinhKem = reader.GetDecimal(reader.GetOrdinal("SoDeLayTepDinhKem"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhKiemTraHoSo"))) entity.NgayHoanThanhKiemTraHoSo = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhKiemTraHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetDecimal(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongDungLuong"))) entity.TongDungLuong = reader.GetDecimal(reader.GetOrdinal("TongDungLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDau"))) entity.NgayBatDau = reader.GetDateTime(reader.GetOrdinal("NgayBatDau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapNhatCuoiCung"))) entity.NgayCapNhatCuoiCung = reader.GetDateTime(reader.GetOrdinal("NgayCapNhatCuoiCung"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanCuPhapLenh"))) entity.CanCuPhapLenh = reader.GetString(reader.GetOrdinal("CanCuPhapLenh"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoBaoXoa"))) entity.CoBaoXoa = reader.GetString(reader.GetOrdinal("CoBaoXoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuHaiQuan"))) entity.GhiChuHaiQuan = reader.GetString(reader.GetOrdinal("GhiChuHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThucTimKiem"))) entity.HinhThucTimKiem = reader.GetString(reader.GetOrdinal("HinhThucTimKiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoChinhSua"))) entity.LyDoChinhSua = reader.GetString(reader.GetOrdinal("LyDoChinhSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemDen"))) entity.MaDiaDiemDen = reader.GetString(reader.GetOrdinal("MaDiaDiemDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNhaVanChuyen"))) entity.MaNhaVanChuyen = reader.GetString(reader.GetOrdinal("MaNhaVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiDangKy"))) entity.MaPhanLoaiDangKy = reader.GetString(reader.GetOrdinal("MaPhanLoaiDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiXuLy"))) entity.MaPhanLoaiXuLy = reader.GetString(reader.GetOrdinal("MaPhanLoaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThongTinXuat"))) entity.MaThongTinXuat = reader.GetString(reader.GetOrdinal("MaThongTinXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapPhep"))) entity.NgayCapPhep = reader.GetDateTime(reader.GetOrdinal("NgayCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayThongBao"))) entity.NgayThongBao = reader.GetDateTime(reader.GetOrdinal("NgayThongBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiGui"))) entity.NguoiGui = reader.GetString(reader.GetOrdinal("NguoiGui"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungChinhSua"))) entity.NoiDungChinhSua = reader.GetString(reader.GetOrdinal("NoiDungChinhSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongTienVanChuyen"))) entity.PhuongTienVanChuyen = reader.GetString(reader.GetOrdinal("PhuongTienVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChuyenDiBien"))) entity.SoChuyenDiBien = reader.GetString(reader.GetOrdinal("SoChuyenDiBien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeri"))) entity.SoSeri = reader.GetString(reader.GetOrdinal("SoSeri"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhanToKhaiPhoThong"))) entity.SoTiepNhanToKhaiPhoThong = reader.GetDecimal(reader.GetOrdinal("SoTiepNhanToKhaiPhoThong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLyHaiQuan"))) entity.TrangThaiXuLyHaiQuan = reader.GetString(reader.GetOrdinal("TrangThaiXuLyHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiCapNhatCuoiCung"))) entity.NguoiCapNhatCuoiCung = reader.GetString(reader.GetOrdinal("NguoiCapNhatCuoiCung"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomXuLyHoSoID"))) entity.NhomXuLyHoSoID = reader.GetInt32(reader.GetOrdinal("NhomXuLyHoSoID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ChungTuDinhKem> collection, long id)
        {
            foreach (KDT_VNACC_ChungTuDinhKem item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuDinhKem VALUES(@LoaiChungTu, @Notes, @InputMessageID, @MessageTag, @IndexTag, @TrangThaiXuLy, @CoQuanHaiQuan, @NhomXuLyHoSo, @TieuDe, @SoToKhai, @GhiChu, @PhanLoaiThuTucKhaiBao, @SoDienThoaiNguoiKhaiBao, @SoQuanLyTrongNoiBoDoanhNghiep, @MaKetQuaXuLy, @TenThuTucKhaiBao, @NgayKhaiBao, @TrangThaiKhaiBao, @NgaySuaCuoiCung, @TenNguoiKhaiBao, @DiaChiNguoiKhaiBao, @SoDeLayTepDinhKem, @NgayHoanThanhKiemTraHoSo, @SoTiepNhan, @NgayTiepNhan, @TongDungLuong, @PhanLuong, @HuongDan, @TKMD_ID, @NgayBatDau, @NgayCapNhatCuoiCung, @CanCuPhapLenh, @CoBaoXoa, @GhiChuHaiQuan, @HinhThucTimKiem, @LyDoChinhSua, @MaDiaDiemDen, @MaNhaVanChuyen, @MaPhanLoaiDangKy, @MaPhanLoaiXuLy, @MaThongTinXuat, @NgayCapPhep, @NgayThongBao, @NguoiGui, @NoiDungChinhSua, @PhuongTienVanChuyen, @SoChuyenDiBien, @SoSeri, @SoTiepNhanToKhaiPhoThong, @TrangThaiXuLyHaiQuan, @NguoiCapNhatCuoiCung, @NhomXuLyHoSoID)";
            string update = "UPDATE t_KDT_VNACC_ChungTuDinhKem SET LoaiChungTu = @LoaiChungTu, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, TrangThaiXuLy = @TrangThaiXuLy, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHoSo = @NhomXuLyHoSo, TieuDe = @TieuDe, SoToKhai = @SoToKhai, GhiChu = @GhiChu, PhanLoaiThuTucKhaiBao = @PhanLoaiThuTucKhaiBao, SoDienThoaiNguoiKhaiBao = @SoDienThoaiNguoiKhaiBao, SoQuanLyTrongNoiBoDoanhNghiep = @SoQuanLyTrongNoiBoDoanhNghiep, MaKetQuaXuLy = @MaKetQuaXuLy, TenThuTucKhaiBao = @TenThuTucKhaiBao, NgayKhaiBao = @NgayKhaiBao, TrangThaiKhaiBao = @TrangThaiKhaiBao, NgaySuaCuoiCung = @NgaySuaCuoiCung, TenNguoiKhaiBao = @TenNguoiKhaiBao, DiaChiNguoiKhaiBao = @DiaChiNguoiKhaiBao, SoDeLayTepDinhKem = @SoDeLayTepDinhKem, NgayHoanThanhKiemTraHoSo = @NgayHoanThanhKiemTraHoSo, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TongDungLuong = @TongDungLuong, PhanLuong = @PhanLuong, HuongDan = @HuongDan, TKMD_ID = @TKMD_ID, NgayBatDau = @NgayBatDau, NgayCapNhatCuoiCung = @NgayCapNhatCuoiCung, CanCuPhapLenh = @CanCuPhapLenh, CoBaoXoa = @CoBaoXoa, GhiChuHaiQuan = @GhiChuHaiQuan, HinhThucTimKiem = @HinhThucTimKiem, LyDoChinhSua = @LyDoChinhSua, MaDiaDiemDen = @MaDiaDiemDen, MaNhaVanChuyen = @MaNhaVanChuyen, MaPhanLoaiDangKy = @MaPhanLoaiDangKy, MaPhanLoaiXuLy = @MaPhanLoaiXuLy, MaThongTinXuat = @MaThongTinXuat, NgayCapPhep = @NgayCapPhep, NgayThongBao = @NgayThongBao, NguoiGui = @NguoiGui, NoiDungChinhSua = @NoiDungChinhSua, PhuongTienVanChuyen = @PhuongTienVanChuyen, SoChuyenDiBien = @SoChuyenDiBien, SoSeri = @SoSeri, SoTiepNhanToKhaiPhoThong = @SoTiepNhanToKhaiPhoThong, TrangThaiXuLyHaiQuan = @TrangThaiXuLyHaiQuan, NguoiCapNhatCuoiCung = @NguoiCapNhatCuoiCung, NhomXuLyHoSoID = @NhomXuLyHoSoID WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuDinhKem WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, "PhanLoaiThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, "SoDienThoaiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, "TenThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, "TrangThaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, "NgaySuaCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, "TenNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, "DiaChiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, "SoDeLayTepDinhKem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, "NgayHoanThanhKiemTraHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongDungLuong", SqlDbType.Decimal, "TongDungLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongDan", SqlDbType.VarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, "NgayCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, "CanCuPhapLenh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoXoa", SqlDbType.VarChar, "CoBaoXoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, "GhiChuHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThucTimKiem", SqlDbType.VarChar, "HinhThucTimKiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoChinhSua", SqlDbType.NVarChar, "LyDoChinhSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDen", SqlDbType.VarChar, "MaDiaDiemDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, "MaPhanLoaiDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThongTinXuat", SqlDbType.VarChar, "MaThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiGui", SqlDbType.VarChar, "NguoiGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, "NoiDungChinhSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChuyenDiBien", SqlDbType.VarChar, "SoChuyenDiBien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeri", SqlDbType.VarChar, "SoSeri", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, "SoTiepNhanToKhaiPhoThong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, "TrangThaiXuLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, "NguoiCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, "PhanLoaiThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, "SoDienThoaiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, "TenThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, "TrangThaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, "NgaySuaCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, "TenNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, "DiaChiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, "SoDeLayTepDinhKem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, "NgayHoanThanhKiemTraHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongDungLuong", SqlDbType.Decimal, "TongDungLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongDan", SqlDbType.VarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, "NgayCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, "CanCuPhapLenh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoXoa", SqlDbType.VarChar, "CoBaoXoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, "GhiChuHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThucTimKiem", SqlDbType.VarChar, "HinhThucTimKiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoChinhSua", SqlDbType.NVarChar, "LyDoChinhSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDen", SqlDbType.VarChar, "MaDiaDiemDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, "MaPhanLoaiDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThongTinXuat", SqlDbType.VarChar, "MaThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiGui", SqlDbType.VarChar, "NguoiGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, "NoiDungChinhSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChuyenDiBien", SqlDbType.VarChar, "SoChuyenDiBien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeri", SqlDbType.VarChar, "SoSeri", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, "SoTiepNhanToKhaiPhoThong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, "TrangThaiXuLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, "NguoiCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuDinhKem VALUES(@LoaiChungTu, @Notes, @InputMessageID, @MessageTag, @IndexTag, @TrangThaiXuLy, @CoQuanHaiQuan, @NhomXuLyHoSo, @TieuDe, @SoToKhai, @GhiChu, @PhanLoaiThuTucKhaiBao, @SoDienThoaiNguoiKhaiBao, @SoQuanLyTrongNoiBoDoanhNghiep, @MaKetQuaXuLy, @TenThuTucKhaiBao, @NgayKhaiBao, @TrangThaiKhaiBao, @NgaySuaCuoiCung, @TenNguoiKhaiBao, @DiaChiNguoiKhaiBao, @SoDeLayTepDinhKem, @NgayHoanThanhKiemTraHoSo, @SoTiepNhan, @NgayTiepNhan, @TongDungLuong, @PhanLuong, @HuongDan, @TKMD_ID, @NgayBatDau, @NgayCapNhatCuoiCung, @CanCuPhapLenh, @CoBaoXoa, @GhiChuHaiQuan, @HinhThucTimKiem, @LyDoChinhSua, @MaDiaDiemDen, @MaNhaVanChuyen, @MaPhanLoaiDangKy, @MaPhanLoaiXuLy, @MaThongTinXuat, @NgayCapPhep, @NgayThongBao, @NguoiGui, @NoiDungChinhSua, @PhuongTienVanChuyen, @SoChuyenDiBien, @SoSeri, @SoTiepNhanToKhaiPhoThong, @TrangThaiXuLyHaiQuan, @NguoiCapNhatCuoiCung, @NhomXuLyHoSoID)";
            string update = "UPDATE t_KDT_VNACC_ChungTuDinhKem SET LoaiChungTu = @LoaiChungTu, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, TrangThaiXuLy = @TrangThaiXuLy, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHoSo = @NhomXuLyHoSo, TieuDe = @TieuDe, SoToKhai = @SoToKhai, GhiChu = @GhiChu, PhanLoaiThuTucKhaiBao = @PhanLoaiThuTucKhaiBao, SoDienThoaiNguoiKhaiBao = @SoDienThoaiNguoiKhaiBao, SoQuanLyTrongNoiBoDoanhNghiep = @SoQuanLyTrongNoiBoDoanhNghiep, MaKetQuaXuLy = @MaKetQuaXuLy, TenThuTucKhaiBao = @TenThuTucKhaiBao, NgayKhaiBao = @NgayKhaiBao, TrangThaiKhaiBao = @TrangThaiKhaiBao, NgaySuaCuoiCung = @NgaySuaCuoiCung, TenNguoiKhaiBao = @TenNguoiKhaiBao, DiaChiNguoiKhaiBao = @DiaChiNguoiKhaiBao, SoDeLayTepDinhKem = @SoDeLayTepDinhKem, NgayHoanThanhKiemTraHoSo = @NgayHoanThanhKiemTraHoSo, SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TongDungLuong = @TongDungLuong, PhanLuong = @PhanLuong, HuongDan = @HuongDan, TKMD_ID = @TKMD_ID, NgayBatDau = @NgayBatDau, NgayCapNhatCuoiCung = @NgayCapNhatCuoiCung, CanCuPhapLenh = @CanCuPhapLenh, CoBaoXoa = @CoBaoXoa, GhiChuHaiQuan = @GhiChuHaiQuan, HinhThucTimKiem = @HinhThucTimKiem, LyDoChinhSua = @LyDoChinhSua, MaDiaDiemDen = @MaDiaDiemDen, MaNhaVanChuyen = @MaNhaVanChuyen, MaPhanLoaiDangKy = @MaPhanLoaiDangKy, MaPhanLoaiXuLy = @MaPhanLoaiXuLy, MaThongTinXuat = @MaThongTinXuat, NgayCapPhep = @NgayCapPhep, NgayThongBao = @NgayThongBao, NguoiGui = @NguoiGui, NoiDungChinhSua = @NoiDungChinhSua, PhuongTienVanChuyen = @PhuongTienVanChuyen, SoChuyenDiBien = @SoChuyenDiBien, SoSeri = @SoSeri, SoTiepNhanToKhaiPhoThong = @SoTiepNhanToKhaiPhoThong, TrangThaiXuLyHaiQuan = @TrangThaiXuLyHaiQuan, NguoiCapNhatCuoiCung = @NguoiCapNhatCuoiCung, NhomXuLyHoSoID = @NhomXuLyHoSoID WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuDinhKem WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, "PhanLoaiThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, "SoDienThoaiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, "TenThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, "TrangThaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, "NgaySuaCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, "TenNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, "DiaChiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, "SoDeLayTepDinhKem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, "NgayHoanThanhKiemTraHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongDungLuong", SqlDbType.Decimal, "TongDungLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongDan", SqlDbType.VarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, "NgayCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, "CanCuPhapLenh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoXoa", SqlDbType.VarChar, "CoBaoXoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, "GhiChuHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThucTimKiem", SqlDbType.VarChar, "HinhThucTimKiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoChinhSua", SqlDbType.NVarChar, "LyDoChinhSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemDen", SqlDbType.VarChar, "MaDiaDiemDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, "MaPhanLoaiDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThongTinXuat", SqlDbType.VarChar, "MaThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiGui", SqlDbType.VarChar, "NguoiGui", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, "NoiDungChinhSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChuyenDiBien", SqlDbType.VarChar, "SoChuyenDiBien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeri", SqlDbType.VarChar, "SoSeri", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, "SoTiepNhanToKhaiPhoThong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, "TrangThaiXuLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, "NguoiCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, "NhomXuLyHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TieuDe", SqlDbType.NVarChar, "TieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, "PhanLoaiThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, "SoDienThoaiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, "SoQuanLyTrongNoiBoDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, "TenThuTucKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, "TrangThaiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, "NgaySuaCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, "TenNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, "DiaChiNguoiKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, "SoDeLayTepDinhKem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, "NgayHoanThanhKiemTraHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.Decimal, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongDungLuong", SqlDbType.Decimal, "TongDungLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.NVarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongDan", SqlDbType.VarChar, "HuongDan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDau", SqlDbType.DateTime, "NgayBatDau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, "NgayCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, "CanCuPhapLenh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoXoa", SqlDbType.VarChar, "CoBaoXoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, "GhiChuHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThucTimKiem", SqlDbType.VarChar, "HinhThucTimKiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoChinhSua", SqlDbType.NVarChar, "LyDoChinhSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemDen", SqlDbType.VarChar, "MaDiaDiemDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, "MaNhaVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, "MaPhanLoaiDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, "MaPhanLoaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThongTinXuat", SqlDbType.VarChar, "MaThongTinXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCapPhep", SqlDbType.DateTime, "NgayCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayThongBao", SqlDbType.DateTime, "NgayThongBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiGui", SqlDbType.VarChar, "NguoiGui", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, "NoiDungChinhSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChuyenDiBien", SqlDbType.VarChar, "SoChuyenDiBien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeri", SqlDbType.VarChar, "SoSeri", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, "SoTiepNhanToKhaiPhoThong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, "TrangThaiXuLyHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, "NguoiCapNhatCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHoSoID", SqlDbType.Int, "NhomXuLyHoSoID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ChungTuDinhKem Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ChungTuDinhKem> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ChungTuDinhKem> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ChungTuDinhKem> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ChungTuDinhKem(string loaiChungTu, string notes, string inputMessageID, string messageTag, string indexTag, string trangThaiXuLy, string coQuanHaiQuan, string nhomXuLyHoSo, string tieuDe, decimal soToKhai, string ghiChu, string phanLoaiThuTucKhaiBao, string soDienThoaiNguoiKhaiBao, string soQuanLyTrongNoiBoDoanhNghiep, string maKetQuaXuLy, string tenThuTucKhaiBao, DateTime ngayKhaiBao, string trangThaiKhaiBao, DateTime ngaySuaCuoiCung, string tenNguoiKhaiBao, string diaChiNguoiKhaiBao, decimal soDeLayTepDinhKem, DateTime ngayHoanThanhKiemTraHoSo, decimal soTiepNhan, DateTime ngayTiepNhan, decimal tongDungLuong, string phanLuong, string huongDan, long tKMD_ID, DateTime ngayBatDau, DateTime ngayCapNhatCuoiCung, string canCuPhapLenh, string coBaoXoa, string ghiChuHaiQuan, string hinhThucTimKiem, string lyDoChinhSua, string maDiaDiemDen, string maNhaVanChuyen, string maPhanLoaiDangKy, string maPhanLoaiXuLy, string maThongTinXuat, DateTime ngayCapPhep, DateTime ngayThongBao, string nguoiGui, string noiDungChinhSua, string phuongTienVanChuyen, string soChuyenDiBien, string soSeri, decimal soTiepNhanToKhaiPhoThong, string trangThaiXuLyHaiQuan, string nguoiCapNhatCuoiCung, int nhomXuLyHoSoID)
		{
			KDT_VNACC_ChungTuDinhKem entity = new KDT_VNACC_ChungTuDinhKem();	
			entity.LoaiChungTu = loaiChungTu;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHoSo = nhomXuLyHoSo;
			entity.TieuDe = tieuDe;
			entity.SoToKhai = soToKhai;
			entity.GhiChu = ghiChu;
			entity.PhanLoaiThuTucKhaiBao = phanLoaiThuTucKhaiBao;
			entity.SoDienThoaiNguoiKhaiBao = soDienThoaiNguoiKhaiBao;
			entity.SoQuanLyTrongNoiBoDoanhNghiep = soQuanLyTrongNoiBoDoanhNghiep;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.TenThuTucKhaiBao = tenThuTucKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.TrangThaiKhaiBao = trangThaiKhaiBao;
			entity.NgaySuaCuoiCung = ngaySuaCuoiCung;
			entity.TenNguoiKhaiBao = tenNguoiKhaiBao;
			entity.DiaChiNguoiKhaiBao = diaChiNguoiKhaiBao;
			entity.SoDeLayTepDinhKem = soDeLayTepDinhKem;
			entity.NgayHoanThanhKiemTraHoSo = ngayHoanThanhKiemTraHoSo;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TongDungLuong = tongDungLuong;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayCapNhatCuoiCung = ngayCapNhatCuoiCung;
			entity.CanCuPhapLenh = canCuPhapLenh;
			entity.CoBaoXoa = coBaoXoa;
			entity.GhiChuHaiQuan = ghiChuHaiQuan;
			entity.HinhThucTimKiem = hinhThucTimKiem;
			entity.LyDoChinhSua = lyDoChinhSua;
			entity.MaDiaDiemDen = maDiaDiemDen;
			entity.MaNhaVanChuyen = maNhaVanChuyen;
			entity.MaPhanLoaiDangKy = maPhanLoaiDangKy;
			entity.MaPhanLoaiXuLy = maPhanLoaiXuLy;
			entity.MaThongTinXuat = maThongTinXuat;
			entity.NgayCapPhep = ngayCapPhep;
			entity.NgayThongBao = ngayThongBao;
			entity.NguoiGui = nguoiGui;
			entity.NoiDungChinhSua = noiDungChinhSua;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.SoChuyenDiBien = soChuyenDiBien;
			entity.SoSeri = soSeri;
			entity.SoTiepNhanToKhaiPhoThong = soTiepNhanToKhaiPhoThong;
			entity.TrangThaiXuLyHaiQuan = trangThaiXuLyHaiQuan;
			entity.NguoiCapNhatCuoiCung = nguoiCapNhatCuoiCung;
			entity.NhomXuLyHoSoID = nhomXuLyHoSoID;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, NhomXuLyHoSo);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, PhanLoaiThuTucKhaiBao);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, SoDienThoaiNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, SoQuanLyTrongNoiBoDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, TenThuTucKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, TrangThaiKhaiBao);
			db.AddInParameter(dbCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, NgaySuaCuoiCung.Year <= 1753 ? DBNull.Value : (object) NgaySuaCuoiCung);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, TenNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, DiaChiNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, SoDeLayTepDinhKem);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, NgayHoanThanhKiemTraHoSo.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraHoSo);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TongDungLuong", SqlDbType.Decimal, TongDungLuong);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, NgayCapNhatCuoiCung.Year <= 1753 ? DBNull.Value : (object) NgayCapNhatCuoiCung);
			db.AddInParameter(dbCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, CanCuPhapLenh);
			db.AddInParameter(dbCommand, "@CoBaoXoa", SqlDbType.VarChar, CoBaoXoa);
			db.AddInParameter(dbCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, GhiChuHaiQuan);
			db.AddInParameter(dbCommand, "@HinhThucTimKiem", SqlDbType.VarChar, HinhThucTimKiem);
			db.AddInParameter(dbCommand, "@LyDoChinhSua", SqlDbType.NVarChar, LyDoChinhSua);
			db.AddInParameter(dbCommand, "@MaDiaDiemDen", SqlDbType.VarChar, MaDiaDiemDen);
			db.AddInParameter(dbCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, MaNhaVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, MaPhanLoaiDangKy);
			db.AddInParameter(dbCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, MaPhanLoaiXuLy);
			db.AddInParameter(dbCommand, "@MaThongTinXuat", SqlDbType.VarChar, MaThongTinXuat);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@NgayThongBao", SqlDbType.DateTime, NgayThongBao.Year <= 1753 ? DBNull.Value : (object) NgayThongBao);
			db.AddInParameter(dbCommand, "@NguoiGui", SqlDbType.VarChar, NguoiGui);
			db.AddInParameter(dbCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, NoiDungChinhSua);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@SoChuyenDiBien", SqlDbType.VarChar, SoChuyenDiBien);
			db.AddInParameter(dbCommand, "@SoSeri", SqlDbType.VarChar, SoSeri);
			db.AddInParameter(dbCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, SoTiepNhanToKhaiPhoThong);
			db.AddInParameter(dbCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, TrangThaiXuLyHaiQuan);
			db.AddInParameter(dbCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, NguoiCapNhatCuoiCung);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSoID", SqlDbType.Int, NhomXuLyHoSoID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ChungTuDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuDinhKem item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ChungTuDinhKem(long id, string loaiChungTu, string notes, string inputMessageID, string messageTag, string indexTag, string trangThaiXuLy, string coQuanHaiQuan, string nhomXuLyHoSo, string tieuDe, decimal soToKhai, string ghiChu, string phanLoaiThuTucKhaiBao, string soDienThoaiNguoiKhaiBao, string soQuanLyTrongNoiBoDoanhNghiep, string maKetQuaXuLy, string tenThuTucKhaiBao, DateTime ngayKhaiBao, string trangThaiKhaiBao, DateTime ngaySuaCuoiCung, string tenNguoiKhaiBao, string diaChiNguoiKhaiBao, decimal soDeLayTepDinhKem, DateTime ngayHoanThanhKiemTraHoSo, decimal soTiepNhan, DateTime ngayTiepNhan, decimal tongDungLuong, string phanLuong, string huongDan, long tKMD_ID, DateTime ngayBatDau, DateTime ngayCapNhatCuoiCung, string canCuPhapLenh, string coBaoXoa, string ghiChuHaiQuan, string hinhThucTimKiem, string lyDoChinhSua, string maDiaDiemDen, string maNhaVanChuyen, string maPhanLoaiDangKy, string maPhanLoaiXuLy, string maThongTinXuat, DateTime ngayCapPhep, DateTime ngayThongBao, string nguoiGui, string noiDungChinhSua, string phuongTienVanChuyen, string soChuyenDiBien, string soSeri, decimal soTiepNhanToKhaiPhoThong, string trangThaiXuLyHaiQuan, string nguoiCapNhatCuoiCung, int nhomXuLyHoSoID)
		{
			KDT_VNACC_ChungTuDinhKem entity = new KDT_VNACC_ChungTuDinhKem();			
			entity.ID = id;
			entity.LoaiChungTu = loaiChungTu;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHoSo = nhomXuLyHoSo;
			entity.TieuDe = tieuDe;
			entity.SoToKhai = soToKhai;
			entity.GhiChu = ghiChu;
			entity.PhanLoaiThuTucKhaiBao = phanLoaiThuTucKhaiBao;
			entity.SoDienThoaiNguoiKhaiBao = soDienThoaiNguoiKhaiBao;
			entity.SoQuanLyTrongNoiBoDoanhNghiep = soQuanLyTrongNoiBoDoanhNghiep;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.TenThuTucKhaiBao = tenThuTucKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.TrangThaiKhaiBao = trangThaiKhaiBao;
			entity.NgaySuaCuoiCung = ngaySuaCuoiCung;
			entity.TenNguoiKhaiBao = tenNguoiKhaiBao;
			entity.DiaChiNguoiKhaiBao = diaChiNguoiKhaiBao;
			entity.SoDeLayTepDinhKem = soDeLayTepDinhKem;
			entity.NgayHoanThanhKiemTraHoSo = ngayHoanThanhKiemTraHoSo;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TongDungLuong = tongDungLuong;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayCapNhatCuoiCung = ngayCapNhatCuoiCung;
			entity.CanCuPhapLenh = canCuPhapLenh;
			entity.CoBaoXoa = coBaoXoa;
			entity.GhiChuHaiQuan = ghiChuHaiQuan;
			entity.HinhThucTimKiem = hinhThucTimKiem;
			entity.LyDoChinhSua = lyDoChinhSua;
			entity.MaDiaDiemDen = maDiaDiemDen;
			entity.MaNhaVanChuyen = maNhaVanChuyen;
			entity.MaPhanLoaiDangKy = maPhanLoaiDangKy;
			entity.MaPhanLoaiXuLy = maPhanLoaiXuLy;
			entity.MaThongTinXuat = maThongTinXuat;
			entity.NgayCapPhep = ngayCapPhep;
			entity.NgayThongBao = ngayThongBao;
			entity.NguoiGui = nguoiGui;
			entity.NoiDungChinhSua = noiDungChinhSua;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.SoChuyenDiBien = soChuyenDiBien;
			entity.SoSeri = soSeri;
			entity.SoTiepNhanToKhaiPhoThong = soTiepNhanToKhaiPhoThong;
			entity.TrangThaiXuLyHaiQuan = trangThaiXuLyHaiQuan;
			entity.NguoiCapNhatCuoiCung = nguoiCapNhatCuoiCung;
			entity.NhomXuLyHoSoID = nhomXuLyHoSoID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ChungTuDinhKem_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, NhomXuLyHoSo);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, PhanLoaiThuTucKhaiBao);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, SoDienThoaiNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, SoQuanLyTrongNoiBoDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, TenThuTucKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, TrangThaiKhaiBao);
			db.AddInParameter(dbCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, NgaySuaCuoiCung.Year <= 1753 ? DBNull.Value : (object) NgaySuaCuoiCung);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, TenNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, DiaChiNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, SoDeLayTepDinhKem);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, NgayHoanThanhKiemTraHoSo.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraHoSo);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TongDungLuong", SqlDbType.Decimal, TongDungLuong);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, NgayCapNhatCuoiCung.Year <= 1753 ? DBNull.Value : (object) NgayCapNhatCuoiCung);
			db.AddInParameter(dbCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, CanCuPhapLenh);
			db.AddInParameter(dbCommand, "@CoBaoXoa", SqlDbType.VarChar, CoBaoXoa);
			db.AddInParameter(dbCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, GhiChuHaiQuan);
			db.AddInParameter(dbCommand, "@HinhThucTimKiem", SqlDbType.VarChar, HinhThucTimKiem);
			db.AddInParameter(dbCommand, "@LyDoChinhSua", SqlDbType.NVarChar, LyDoChinhSua);
			db.AddInParameter(dbCommand, "@MaDiaDiemDen", SqlDbType.VarChar, MaDiaDiemDen);
			db.AddInParameter(dbCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, MaNhaVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, MaPhanLoaiDangKy);
			db.AddInParameter(dbCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, MaPhanLoaiXuLy);
			db.AddInParameter(dbCommand, "@MaThongTinXuat", SqlDbType.VarChar, MaThongTinXuat);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@NgayThongBao", SqlDbType.DateTime, NgayThongBao.Year <= 1753 ? DBNull.Value : (object) NgayThongBao);
			db.AddInParameter(dbCommand, "@NguoiGui", SqlDbType.VarChar, NguoiGui);
			db.AddInParameter(dbCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, NoiDungChinhSua);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@SoChuyenDiBien", SqlDbType.VarChar, SoChuyenDiBien);
			db.AddInParameter(dbCommand, "@SoSeri", SqlDbType.VarChar, SoSeri);
			db.AddInParameter(dbCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, SoTiepNhanToKhaiPhoThong);
			db.AddInParameter(dbCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, TrangThaiXuLyHaiQuan);
			db.AddInParameter(dbCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, NguoiCapNhatCuoiCung);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSoID", SqlDbType.Int, NhomXuLyHoSoID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ChungTuDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuDinhKem item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ChungTuDinhKem(long id, string loaiChungTu, string notes, string inputMessageID, string messageTag, string indexTag, string trangThaiXuLy, string coQuanHaiQuan, string nhomXuLyHoSo, string tieuDe, decimal soToKhai, string ghiChu, string phanLoaiThuTucKhaiBao, string soDienThoaiNguoiKhaiBao, string soQuanLyTrongNoiBoDoanhNghiep, string maKetQuaXuLy, string tenThuTucKhaiBao, DateTime ngayKhaiBao, string trangThaiKhaiBao, DateTime ngaySuaCuoiCung, string tenNguoiKhaiBao, string diaChiNguoiKhaiBao, decimal soDeLayTepDinhKem, DateTime ngayHoanThanhKiemTraHoSo, decimal soTiepNhan, DateTime ngayTiepNhan, decimal tongDungLuong, string phanLuong, string huongDan, long tKMD_ID, DateTime ngayBatDau, DateTime ngayCapNhatCuoiCung, string canCuPhapLenh, string coBaoXoa, string ghiChuHaiQuan, string hinhThucTimKiem, string lyDoChinhSua, string maDiaDiemDen, string maNhaVanChuyen, string maPhanLoaiDangKy, string maPhanLoaiXuLy, string maThongTinXuat, DateTime ngayCapPhep, DateTime ngayThongBao, string nguoiGui, string noiDungChinhSua, string phuongTienVanChuyen, string soChuyenDiBien, string soSeri, decimal soTiepNhanToKhaiPhoThong, string trangThaiXuLyHaiQuan, string nguoiCapNhatCuoiCung, int nhomXuLyHoSoID)
		{
			KDT_VNACC_ChungTuDinhKem entity = new KDT_VNACC_ChungTuDinhKem();			
			entity.ID = id;
			entity.LoaiChungTu = loaiChungTu;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHoSo = nhomXuLyHoSo;
			entity.TieuDe = tieuDe;
			entity.SoToKhai = soToKhai;
			entity.GhiChu = ghiChu;
			entity.PhanLoaiThuTucKhaiBao = phanLoaiThuTucKhaiBao;
			entity.SoDienThoaiNguoiKhaiBao = soDienThoaiNguoiKhaiBao;
			entity.SoQuanLyTrongNoiBoDoanhNghiep = soQuanLyTrongNoiBoDoanhNghiep;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.TenThuTucKhaiBao = tenThuTucKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.TrangThaiKhaiBao = trangThaiKhaiBao;
			entity.NgaySuaCuoiCung = ngaySuaCuoiCung;
			entity.TenNguoiKhaiBao = tenNguoiKhaiBao;
			entity.DiaChiNguoiKhaiBao = diaChiNguoiKhaiBao;
			entity.SoDeLayTepDinhKem = soDeLayTepDinhKem;
			entity.NgayHoanThanhKiemTraHoSo = ngayHoanThanhKiemTraHoSo;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TongDungLuong = tongDungLuong;
			entity.PhanLuong = phanLuong;
			entity.HuongDan = huongDan;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayBatDau = ngayBatDau;
			entity.NgayCapNhatCuoiCung = ngayCapNhatCuoiCung;
			entity.CanCuPhapLenh = canCuPhapLenh;
			entity.CoBaoXoa = coBaoXoa;
			entity.GhiChuHaiQuan = ghiChuHaiQuan;
			entity.HinhThucTimKiem = hinhThucTimKiem;
			entity.LyDoChinhSua = lyDoChinhSua;
			entity.MaDiaDiemDen = maDiaDiemDen;
			entity.MaNhaVanChuyen = maNhaVanChuyen;
			entity.MaPhanLoaiDangKy = maPhanLoaiDangKy;
			entity.MaPhanLoaiXuLy = maPhanLoaiXuLy;
			entity.MaThongTinXuat = maThongTinXuat;
			entity.NgayCapPhep = ngayCapPhep;
			entity.NgayThongBao = ngayThongBao;
			entity.NguoiGui = nguoiGui;
			entity.NoiDungChinhSua = noiDungChinhSua;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.SoChuyenDiBien = soChuyenDiBien;
			entity.SoSeri = soSeri;
			entity.SoTiepNhanToKhaiPhoThong = soTiepNhanToKhaiPhoThong;
			entity.TrangThaiXuLyHaiQuan = trangThaiXuLyHaiQuan;
			entity.NguoiCapNhatCuoiCung = nguoiCapNhatCuoiCung;
			entity.NhomXuLyHoSoID = nhomXuLyHoSoID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSo", SqlDbType.VarChar, NhomXuLyHoSo);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@PhanLoaiThuTucKhaiBao", SqlDbType.VarChar, PhanLoaiThuTucKhaiBao);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhaiBao", SqlDbType.VarChar, SoDienThoaiNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@SoQuanLyTrongNoiBoDoanhNghiep", SqlDbType.VarChar, SoQuanLyTrongNoiBoDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@TenThuTucKhaiBao", SqlDbType.NVarChar, TenThuTucKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@TrangThaiKhaiBao", SqlDbType.VarChar, TrangThaiKhaiBao);
			db.AddInParameter(dbCommand, "@NgaySuaCuoiCung", SqlDbType.DateTime, NgaySuaCuoiCung.Year <= 1753 ? DBNull.Value : (object) NgaySuaCuoiCung);
			db.AddInParameter(dbCommand, "@TenNguoiKhaiBao", SqlDbType.NVarChar, TenNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhaiBao", SqlDbType.NVarChar, DiaChiNguoiKhaiBao);
			db.AddInParameter(dbCommand, "@SoDeLayTepDinhKem", SqlDbType.Decimal, SoDeLayTepDinhKem);
			db.AddInParameter(dbCommand, "@NgayHoanThanhKiemTraHoSo", SqlDbType.DateTime, NgayHoanThanhKiemTraHoSo.Year <= 1753 ? DBNull.Value : (object) NgayHoanThanhKiemTraHoSo);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.Decimal, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TongDungLuong", SqlDbType.Decimal, TongDungLuong);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.NVarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayBatDau", SqlDbType.DateTime, NgayBatDau.Year <= 1753 ? DBNull.Value : (object) NgayBatDau);
			db.AddInParameter(dbCommand, "@NgayCapNhatCuoiCung", SqlDbType.DateTime, NgayCapNhatCuoiCung.Year <= 1753 ? DBNull.Value : (object) NgayCapNhatCuoiCung);
			db.AddInParameter(dbCommand, "@CanCuPhapLenh", SqlDbType.NVarChar, CanCuPhapLenh);
			db.AddInParameter(dbCommand, "@CoBaoXoa", SqlDbType.VarChar, CoBaoXoa);
			db.AddInParameter(dbCommand, "@GhiChuHaiQuan", SqlDbType.NVarChar, GhiChuHaiQuan);
			db.AddInParameter(dbCommand, "@HinhThucTimKiem", SqlDbType.VarChar, HinhThucTimKiem);
			db.AddInParameter(dbCommand, "@LyDoChinhSua", SqlDbType.NVarChar, LyDoChinhSua);
			db.AddInParameter(dbCommand, "@MaDiaDiemDen", SqlDbType.VarChar, MaDiaDiemDen);
			db.AddInParameter(dbCommand, "@MaNhaVanChuyen", SqlDbType.VarChar, MaNhaVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiDangKy", SqlDbType.VarChar, MaPhanLoaiDangKy);
			db.AddInParameter(dbCommand, "@MaPhanLoaiXuLy", SqlDbType.VarChar, MaPhanLoaiXuLy);
			db.AddInParameter(dbCommand, "@MaThongTinXuat", SqlDbType.VarChar, MaThongTinXuat);
			db.AddInParameter(dbCommand, "@NgayCapPhep", SqlDbType.DateTime, NgayCapPhep.Year <= 1753 ? DBNull.Value : (object) NgayCapPhep);
			db.AddInParameter(dbCommand, "@NgayThongBao", SqlDbType.DateTime, NgayThongBao.Year <= 1753 ? DBNull.Value : (object) NgayThongBao);
			db.AddInParameter(dbCommand, "@NguoiGui", SqlDbType.VarChar, NguoiGui);
			db.AddInParameter(dbCommand, "@NoiDungChinhSua", SqlDbType.NVarChar, NoiDungChinhSua);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@SoChuyenDiBien", SqlDbType.VarChar, SoChuyenDiBien);
			db.AddInParameter(dbCommand, "@SoSeri", SqlDbType.VarChar, SoSeri);
			db.AddInParameter(dbCommand, "@SoTiepNhanToKhaiPhoThong", SqlDbType.Decimal, SoTiepNhanToKhaiPhoThong);
			db.AddInParameter(dbCommand, "@TrangThaiXuLyHaiQuan", SqlDbType.VarChar, TrangThaiXuLyHaiQuan);
			db.AddInParameter(dbCommand, "@NguoiCapNhatCuoiCung", SqlDbType.VarChar, NguoiCapNhatCuoiCung);
			db.AddInParameter(dbCommand, "@NhomXuLyHoSoID", SqlDbType.Int, NhomXuLyHoSoID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ChungTuDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuDinhKem item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ChungTuDinhKem(long id)
		{
			KDT_VNACC_ChungTuDinhKem entity = new KDT_VNACC_ChungTuDinhKem();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuDinhKem_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ChungTuDinhKem> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuDinhKem item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}