using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public GiayDangKyCapPhepNKThuocCoChuaThuocGayNghien_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAGP110 vagp)
        {
            int HangHoa = vagp.HangHoa.Count;
            if (HangHoa <= 1)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 1, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            DataTable dt = new DataTable();
            if (vagp != null && vagp.HangHoa.Count > 0)
            {
                int STT = 0;
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("TenThuoc", typeof(string));
                dt.Columns.Add("MaSoHH", typeof(string));
                dt.Columns.Add("HoatChat", typeof(string));
                dt.Columns.Add("TieuChuanChatLuong", typeof(string));
                dt.Columns.Add("SoDangKy", typeof(string));
                dt.Columns.Add("HanDung", typeof(string));
                dt.Columns.Add("SoLuong", typeof(string));
                dt.Columns.Add("DVTSoLuong", typeof(string));
                dt.Columns.Add("TenHoatChatGayNghien", typeof(string));
                dt.Columns.Add("CongDung", typeof(string));
                dt.Columns.Add("TongSoKLHoatChatGayNghien", typeof(string));
                dt.Columns.Add("DVTKhoiLuong", typeof(string));
                dt.Columns.Add("GiaNK", typeof(string));
                dt.Columns.Add("GiaBanBuon", typeof(string));
                dt.Columns.Add("GiaBanLe", typeof(string));
                dt.Columns.Add("TenCongTyXK", typeof(string));
                dt.Columns.Add("MaBuuChinhCongTyXK", typeof(string));
                dt.Columns.Add("SoNhaCongTyXK", typeof(string));
                dt.Columns.Add("PhuongXaCongTyXK", typeof(string));
                dt.Columns.Add("QuanHuyenCongTyXK", typeof(string));
                dt.Columns.Add("TinhCongTyXK", typeof(string));
                dt.Columns.Add("MaQuocGiaCongTyXK", typeof(string));
                dt.Columns.Add("TenCongTySX", typeof(string));
                dt.Columns.Add("MaBuuChinhCongTySX", typeof(string));
                dt.Columns.Add("SoNhaCongTySX", typeof(string));
                dt.Columns.Add("PhuongXaCongTySX", typeof(string));
                dt.Columns.Add("QuanHuyenCongTySX", typeof(string));
                dt.Columns.Add("TinhCongTySX", typeof(string));
                dt.Columns.Add("MaQuocGiaCongTySX", typeof(string));
                dt.Columns.Add("TenCongTyCC", typeof(string));
                dt.Columns.Add("MaBuuChinhCongTyCC", typeof(string));
                dt.Columns.Add("SoNhaCongTyCC", typeof(string));
                dt.Columns.Add("PhuongXaCongTyCC", typeof(string));
                dt.Columns.Add("QuanHuyenCongTyCC", typeof(string));
                dt.Columns.Add("TinhCongTyCC", typeof(string));
                dt.Columns.Add("MaQuocGiaCongTyCC", typeof(string));
                dt.Columns.Add("TenDonViUyThac", typeof(string));
                dt.Columns.Add("MaBuuChinhDVUyThac", typeof(string));
                dt.Columns.Add("SoNhaDVUyThac", typeof(string));
                dt.Columns.Add("PhuongXaDVUyThac", typeof(string));
                dt.Columns.Add("QuanHuyenDVUyThac", typeof(string));
                dt.Columns.Add("TinhDVUyThac", typeof(string));
                dt.Columns.Add("MaQuocGiaDVUyThac", typeof(string));
                dt.Columns.Add("DVTLuongTonKhoKyTruoc", typeof(string));
                dt.Columns.Add("LuongTonKhoKyTruoc", typeof(string));
                dt.Columns.Add("LuongNhapTrongKy", typeof(string));
                dt.Columns.Add("TongSo", typeof(string));
                dt.Columns.Add("TongSoXuatTrongKy", typeof(string));
                dt.Columns.Add("SoLuongTonKhoDenNgay", typeof(string));
                dt.Columns.Add("HuHao", typeof(string));
                dt.Columns.Add("GhiChuChiTiet", typeof(string));
                for (int i = 0; i < vagp.HangHoa.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["STT"] = STT.ToString().Trim();
                    dr["TenThuoc"] = vagp.HangHoa[i].MDC.GetValue().ToString().Trim();
                    dr["MaSoHH"] = vagp.HangHoa[i].HSC.GetValue().ToString().Trim();
                    dr["HoatChat"] = vagp.HangHoa[i].ACE.GetValue().ToString().Trim();
                    dr["TieuChuanChatLuong"] = vagp.HangHoa[i].QSD.GetValue().ToString().Trim();
                    dr["SoDangKy"] = vagp.HangHoa[i].RNO.GetValue().ToString().Trim();
                    if (Convert.ToDateTime(vagp.HangHoa[i].EOM.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    {
                        dr["HanDung"] = Convert.ToDateTime(vagp.HangHoa[i].EOM.GetValue()).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        dr["HanDung"] = "";
                    }
                    dr["SoLuong"] = vagp.HangHoa[i].QTT.GetValue().ToString().Trim();
                    dr["DVTSoLuong"] = vagp.HangHoa[i].QTU.GetValue().ToString().Trim();
                    dr["TenHoatChatGayNghien"] = vagp.HangHoa[i].ACA.GetValue().ToString().Trim();
                    dr["CongDung"] = vagp.HangHoa[i].EFF.GetValue().ToString().Trim();
                    dr["TongSoKLHoatChatGayNghien"] = vagp.HangHoa[i].TWE.GetValue().ToString().Trim();
                    dr["DVTKhoiLuong"] = vagp.HangHoa[i].TWU.GetValue().ToString().Trim();
                    dr["GiaNK"] = vagp.HangHoa[i].IMP.GetValue().ToString().Trim();
                    dr["GiaBanBuon"] = vagp.HangHoa[i].SWP.GetValue().ToString().Trim();
                    dr["GiaBanLe"] = vagp.HangHoa[i].ESP.GetValue().ToString().Trim();
                    dr["TenCongTyXK"] = vagp.HangHoa[i].EFN.GetValue().ToString().Trim();
                    dr["MaBuuChinhCongTyXK"] = vagp.HangHoa[i].EXC.GetValue().ToString().Trim();
                    dr["SoNhaCongTyXK"] = vagp.HangHoa[i].EFA.GetValue().ToString().Trim();
                    dr["PhuongXaCongTyXK"] = vagp.HangHoa[i].EFS.GetValue().ToString().Trim();
                    dr["QuanHuyenCongTyXK"] = vagp.HangHoa[i].EFD.GetValue().ToString().Trim();
                    dr["TinhCongTyXK"] = vagp.HangHoa[i].EFC.GetValue().ToString().Trim();
                    dr["MaQuocGiaCongTyXK"] = vagp.HangHoa[i].ECC.GetValue().ToString().Trim();
                    dr["TenCongTySX"] = vagp.HangHoa[i].MFN.GetValue().ToString().Trim();
                    dr["MaBuuChinhCongTySX"] = vagp.HangHoa[i].MPC.GetValue().ToString().Trim();
                    dr["SoNhaCongTySX"] = vagp.HangHoa[i].MAB.GetValue().ToString().Trim();
                    dr["PhuongXaCongTySX"] = vagp.HangHoa[i].MSN.GetValue().ToString().Trim();
                    dr["QuanHuyenCongTySX"] = vagp.HangHoa[i].MDN.GetValue().ToString().Trim();
                    dr["TinhCongTySX"] = vagp.HangHoa[i].MCN.GetValue().ToString().Trim();
                    dr["MaQuocGiaCongTySX"] = vagp.HangHoa[i].MCC.GetValue().ToString().Trim();
                    dr["TenCongTyCC"] = vagp.HangHoa[i].DTN.GetValue().ToString().Trim();
                    dr["MaBuuChinhCongTyCC"] = vagp.HangHoa[i].DPC.GetValue().ToString().Trim();
                    dr["SoNhaCongTyCC"] = vagp.HangHoa[i].DAB.GetValue().ToString().Trim();
                    dr["PhuongXaCongTyCC"] = vagp.HangHoa[i].DSN.GetValue().ToString().Trim();
                    dr["QuanHuyenCongTyCC"] = vagp.HangHoa[i].DDN.GetValue().ToString().Trim();
                    dr["TinhCongTyCC"] = vagp.HangHoa[i].DCN.GetValue().ToString().Trim();
                    dr["MaQuocGiaCongTyCC"] = vagp.HangHoa[i].DCC.GetValue().ToString().Trim();
                    dr["TenDonViUyThac"] = vagp.HangHoa[i].TTN.GetValue().ToString().Trim();
                    dr["MaBuuChinhDVUyThac"] = vagp.HangHoa[i].TPC.GetValue().ToString().Trim();
                    dr["SoNhaDVUyThac"] = vagp.HangHoa[i].TAB.GetValue().ToString().Trim();
                    dr["PhuongXaDVUyThac"] = vagp.HangHoa[i].TSN.GetValue().ToString().Trim();
                    dr["QuanHuyenDVUyThac"] = vagp.HangHoa[i].TDN.GetValue().ToString().Trim();
                    dr["TinhDVUyThac"] = vagp.HangHoa[i].TCN.GetValue().ToString().Trim();
                    dr["MaQuocGiaDVUyThac"] = vagp.HangHoa[i].TCC.GetValue().ToString().Trim();
                    dr["DVTLuongTonKhoKyTruoc"] = vagp.HangHoa[i].QSU.GetValue().ToString().Trim();
                    //dr["LuongTonKhoKyTruoc"] = vagp.HangHoa[i].QSU.GetValue().ToString().Trim();
                    dr["LuongNhapTrongKy"] = vagp.HangHoa[i].QIM.GetValue().ToString().Trim();
                    dr["TongSo"] = vagp.HangHoa[i].TQA.GetValue().ToString().Trim();
                    dr["TongSoXuatTrongKy"] = vagp.HangHoa[i].TEX.GetValue().ToString().Trim();
                    dr["SoLuongTonKhoDenNgay"] = vagp.HangHoa[i].ISU.GetValue().ToString().Trim();
                    dr["HuHao"] = vagp.HangHoa[i].DMD.GetValue().ToString().Trim();
                    dr["GhiChuChiTiet"] = vagp.HangHoa[i].RMK.GetValue().ToString().Trim();
           
                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblTenThuoc.DataBindings.Add("Text", DetailReport.DataSource, "TenThuoc");
            lblMaSoHH.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblHoatChat.DataBindings.Add("Text", DetailReport.DataSource, "HoatChat");
            lblTieuChuanChatLuong.DataBindings.Add("Text", DetailReport.DataSource, "TieuChuanChatLuong");
            lblSoDangKy.DataBindings.Add("Text", DetailReport.DataSource, "SoDangKy");
            lblHanDung.DataBindings.Add("Text", DetailReport.DataSource, "HanDung");
            lblSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong");
            lblDVTSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTSoLuong");
            lblTenHoatChatGayNghien.DataBindings.Add("Text", DetailReport.DataSource, "TenHoatChatGayNghien");
            lblCongDung.DataBindings.Add("Text", DetailReport.DataSource, "CongDung");
            lblTongSoKLHoatChatGayNghien.DataBindings.Add("Text", DetailReport.DataSource, "TongSoKLHoatChatGayNghien");
            lblDVTKhoiLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTKhoiLuong");
            lblGiaNK.DataBindings.Add("Text", DetailReport.DataSource, "GiaNK");
            lblGiaBanBuon.DataBindings.Add("Text", DetailReport.DataSource, "GiaBanBuon");
            lblGiaBanLe.DataBindings.Add("Text", DetailReport.DataSource, "GiaBanLe");
            lblTenCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "TenCongTyXK");
            lblMaBuuChinhCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "MaBuuChinhCongTyXK");
            lblSoNhaCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "SoNhaCongTyXK");
            lblPhuongXaCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "PhuongXaCongTyXK");
            lblQuanHuyenCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "QuanHuyenCongTyXK");
            lblTinhCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "TinhCongTyXK");
            lblMaQuocGiaCongTyXK.DataBindings.Add("Text", DetailReport.DataSource, "MaQuocGiaCongTyXK");
            lblTenCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "TenCongTySX");
            lblMaBuuChinhCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "MaBuuChinhCongTySX");
            lblSoNhaCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "SoNhaCongTySX");
            lblPhuongXaCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "PhuongXaCongTySX");
            lblQuanHuyenCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "QuanHuyenCongTySX");
            lblTinhCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "TinhCongTySX");
            lblMaQuocGiaCongTySX.DataBindings.Add("Text", DetailReport.DataSource, "MaQuocGiaCongTySX");
            lblTenCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "TenCongTyCC");
            lblMaBuuChinhCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "MaBuuChinhCongTyCC");
            lblSoNhaCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "SoNhaCongTyCC");
            lblPhuongXaCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "PhuongXaCongTyCC");
            lblQuanHuyenCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "QuanHuyenCongTyCC");
            lblTinhCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "TinhCongTyCC");
            lblMaQuocGiaCongTyCC.DataBindings.Add("Text", DetailReport.DataSource, "MaQuocGiaCongTyCC");
            lblTenDonViUyThac.DataBindings.Add("Text", DetailReport.DataSource, "TenDonViUyThac");
            lblMaBuuChinhDVUyThac.DataBindings.Add("Text", DetailReport.DataSource, "MaBuuChinhDVUyThac");
            lblSoNhaDVUyThac.DataBindings.Add("Text", DetailReport.DataSource, "SoNhaDVUyThac");
            lblPhuongXaDVUyThac.DataBindings.Add("Text", DetailReport.DataSource, "PhuongXaDVUyThac");
            lblQuanHuyenDVUyThac.DataBindings.Add("Text", DetailReport.DataSource, "QuanHuyenDVUyThac");
            lblTinhDVUyThac.DataBindings.Add("Text", DetailReport.DataSource, "TinhDVUyThac");
            lblMaQuocGiaDVUyThac.DataBindings.Add("Text", DetailReport.DataSource, "MaQuocGiaDVUyThac");
            //lblDanhSachChungTuDK.DataBindings.Add("Text", DetailReport.DataSource, MaSoHH);
            lblDVTLuongTonKhoKyTruoc.DataBindings.Add("Text", DetailReport.DataSource, "DVTLuongTonKhoKyTruoc");
            lblSoLuongTonKhoKyTruoc.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonKhoKyTruoc");
            lblSoLuongNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapTrongKy");
            lblTongSo.DataBindings.Add("Text", DetailReport.DataSource, "TongSo");
            lblTongSoXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TongSoXuatTrongKy");
            lblSoLuongTonKhoDenNgay.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTonKhoDenNgay");
            lblHuHao.DataBindings.Add("Text", DetailReport.DataSource, "HuHao");
            lblGhiChuChiTiet.DataBindings.Add("Text", DetailReport.DataSource, "GhiChuChiTiet");

        }

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 2).ToString();
        }


        


    }
}
