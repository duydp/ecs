using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class OUTBOX
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MSG_FROM { set; get; }
		public string MSG_TO { set; get; }
		public string MSG_ORIGIN { set; get; }
		public string MSG_SIGN_DATA { set; get; }
		public string MSG_SIGN_CERT { set; get; }
		public DateTime CREATE_TIME { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static OUTBOX Load(long id)
		{
			const string spName = "[dbo].[p_MSG_OUTBOX_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			OUTBOX entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new OUTBOX();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) entity.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) entity.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) entity.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
				if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<OUTBOX> SelectCollectionAll()
		{
			List<OUTBOX> collection = new List<OUTBOX>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				OUTBOX entity = new OUTBOX();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) entity.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) entity.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) entity.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
				if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<OUTBOX> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<OUTBOX> collection = new List<OUTBOX>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				OUTBOX entity = new OUTBOX();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) entity.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) entity.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
				if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) entity.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
				if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_MSG_OUTBOX_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_MSG_OUTBOX_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_MSG_OUTBOX_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_MSG_OUTBOX_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertOUTBOX(string mSG_FROM, string mSG_TO, string mSG_ORIGIN, string mSG_SIGN_DATA, string mSG_SIGN_CERT, DateTime cREATE_TIME)
		{
			OUTBOX entity = new OUTBOX();	
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.MSG_ORIGIN = mSG_ORIGIN;
			entity.MSG_SIGN_DATA = mSG_SIGN_DATA;
			entity.MSG_SIGN_CERT = mSG_SIGN_CERT;
			entity.CREATE_TIME = cREATE_TIME;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_MSG_OUTBOX_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@MSG_ORIGIN", SqlDbType.NVarChar, MSG_ORIGIN);
			db.AddInParameter(dbCommand, "@MSG_SIGN_DATA", SqlDbType.NVarChar, MSG_SIGN_DATA);
			db.AddInParameter(dbCommand, "@MSG_SIGN_CERT", SqlDbType.NVarChar, MSG_SIGN_CERT);
			db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime, CREATE_TIME.Year <= 1753 ? DBNull.Value : (object) CREATE_TIME);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<OUTBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (OUTBOX item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateOUTBOX(long id, string mSG_FROM, string mSG_TO, string mSG_ORIGIN, string mSG_SIGN_DATA, string mSG_SIGN_CERT, DateTime cREATE_TIME)
		{
			OUTBOX entity = new OUTBOX();			
			entity.ID = id;
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.MSG_ORIGIN = mSG_ORIGIN;
			entity.MSG_SIGN_DATA = mSG_SIGN_DATA;
			entity.MSG_SIGN_CERT = mSG_SIGN_CERT;
			entity.CREATE_TIME = cREATE_TIME;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_MSG_OUTBOX_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@MSG_ORIGIN", SqlDbType.NVarChar, MSG_ORIGIN);
			db.AddInParameter(dbCommand, "@MSG_SIGN_DATA", SqlDbType.NVarChar, MSG_SIGN_DATA);
			db.AddInParameter(dbCommand, "@MSG_SIGN_CERT", SqlDbType.NVarChar, MSG_SIGN_CERT);
			db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime, CREATE_TIME.Year == 1753 ? DBNull.Value : (object) CREATE_TIME);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<OUTBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (OUTBOX item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateOUTBOX(long id, string mSG_FROM, string mSG_TO, string mSG_ORIGIN, string mSG_SIGN_DATA, string mSG_SIGN_CERT, DateTime cREATE_TIME)
		{
			OUTBOX entity = new OUTBOX();			
			entity.ID = id;
			entity.MSG_FROM = mSG_FROM;
			entity.MSG_TO = mSG_TO;
			entity.MSG_ORIGIN = mSG_ORIGIN;
			entity.MSG_SIGN_DATA = mSG_SIGN_DATA;
			entity.MSG_SIGN_CERT = mSG_SIGN_CERT;
			entity.CREATE_TIME = cREATE_TIME;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_MSG_OUTBOX_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MSG_FROM", SqlDbType.VarChar, MSG_FROM);
			db.AddInParameter(dbCommand, "@MSG_TO", SqlDbType.VarChar, MSG_TO);
			db.AddInParameter(dbCommand, "@MSG_ORIGIN", SqlDbType.NVarChar, MSG_ORIGIN);
			db.AddInParameter(dbCommand, "@MSG_SIGN_DATA", SqlDbType.NVarChar, MSG_SIGN_DATA);
			db.AddInParameter(dbCommand, "@MSG_SIGN_CERT", SqlDbType.NVarChar, MSG_SIGN_CERT);
			db.AddInParameter(dbCommand, "@CREATE_TIME", SqlDbType.DateTime, CREATE_TIME.Year == 1753 ? DBNull.Value : (object) CREATE_TIME);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<OUTBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (OUTBOX item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteOUTBOX(long id)
		{
			OUTBOX entity = new OUTBOX();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_MSG_OUTBOX_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_MSG_OUTBOX_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<OUTBOX> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (OUTBOX item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}