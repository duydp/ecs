﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
    [XmlRoot("Declaration")]
   public class ImportWareHouse : DeclarationBase
    {
        ///// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("AdditionalDocuments")]
        public AdditionalDocuments AdditionalDocumentNew { get; set; }


        /// <summary>
        /// Nguyên phụ liệu
        /// </summary>
        [XmlElement("Warehouse")]
        public NameBase Warehouse { get; set; }

        /// <summary>
        /// Thông tin tham chiếu đến tờ khai
        /// </summary>
        [XmlElement("DeclarationDocument")]
        public DeclarationDocument DeclarationDocument { get; set; }


        /// <summary>
        /// Thông tin hợp đồng gia công
        /// </summary>
        [XmlElement("ContractReference")]
        public ContractReference ContractReference { get; set; }

        ///// <summary>
        ///// Thông tin tham chiếu đến tờ khai
        ///// </summary>
        //[XmlElement("AdditionalInformation")]
        //public Company.KDT.SHARE.Components.AdditionalDocument AdditionalInformation { get; set; }
    }
}
