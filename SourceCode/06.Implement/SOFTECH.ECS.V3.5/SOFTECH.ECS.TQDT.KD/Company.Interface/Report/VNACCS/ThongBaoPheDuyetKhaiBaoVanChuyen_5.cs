using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class ThongBaoPheDuyetKhaiBaoVanChuyen_5 : DevExpress.XtraReports.UI.XtraReport
    {
        public ThongBaoPheDuyetKhaiBaoVanChuyen_5()
        {
            InitializeComponent();
        }
        public void BindingReport(VAS5050 vas504)
        {
            lblTenThongTinXuat.Text = vas504.AB.GetValue().ToString();
            lblMaCoBaoYeuCauXN.Text = vas504.KA.GetValue().ToString().ToUpper();
            lblTenCoBaoYeuCauXN.Text = vas504.KB.GetValue().ToString().ToUpper();
            lblCoQuanHQ.Text = vas504.AD.GetValue().ToString().ToUpper();
            lblSoToKhaiVC.Text = vas504.AE.GetValue().ToString().ToUpper();
            lblCoBaoXNK.Text = vas504.ED.GetValue().ToString().ToUpper();

            int SoCont = vas504.HangHoa.Count;
            if (SoCont <= 20)
            {
                TongSoTrang.Text = System.Convert.ToDecimal(5).ToString();
            }
            else
            {
                TongSoTrang.Text = System.Convert.ToDecimal(5 + Math.Round((decimal)SoCont / 20, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            if (Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayLapTK.Text = Convert.ToDateTime(vas504.AF.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayLapTK.Text = "";
            }
            if (vas504.HangHoa != null && vas504.HangHoa.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("TieuDe", typeof(string));
                dt.Columns.Add("SoCotainer", typeof(string));
                dt.Columns.Add("SoDongHangTK", typeof(string));
                dt.Columns.Add("SoSeal1", typeof(string));
                dt.Columns.Add("SoSeal2", typeof(string));
                dt.Columns.Add("SoSeal3", typeof(string));
                dt.Columns.Add("SoSeal4", typeof(string));
                dt.Columns.Add("SoSeal5", typeof(string));
                dt.Columns.Add("SoSeal6", typeof(string));

                for (int i = 0; i < vas504.HangHoa.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["TieuDe"] = vas504.HangHoa[i].DA.GetValue().ToString().Trim();
                    dr["SoCotainer"] = vas504.HangHoa[i].DB.GetValue().ToString().Trim();
                    dr["SoDongHangTK"] = vas504.HangHoa[i].DC.GetValue().ToString().Trim();
                    for (int j = 0; j < 6; j++)
                    {
                        //if (vas504.HangHoa[i].DD.listAttribute[j] != null)
                            dr["SoSeal" + (j + 1)] = vas504.HangHoa[i].DD.listAttribute[0].GetValueCollection(j).ToString().Trim();
                    }

                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }      
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSoTieuDe.DataBindings.Add("Text", DetailReport.DataSource, "TieuDe");
            lblSoHieuContainer.DataBindings.Add("Text", DetailReport.DataSource, "SoCotainer");
            lblSoDongHangTK.DataBindings.Add("Text", DetailReport.DataSource, "SoDongHangTK");
            lblSoSeal1.DataBindings.Add("Text", DetailReport.DataSource, "SoSeal1");
            lblSoSeal2.DataBindings.Add("Text", DetailReport.DataSource, "SoSeal2");
            lblSoSeal3.DataBindings.Add("Text", DetailReport.DataSource, "SoSeal3");
            lblSoSeal4.DataBindings.Add("Text", DetailReport.DataSource, "SoSeal4");
            lblSoSeal5.DataBindings.Add("Text", DetailReport.DataSource, "SoSeal5");
            lblSoSeal6.DataBindings.Add("Text", DetailReport.DataSource, "SoSeal6");
        }

        private void lblSoTieuDe_AfterPrint(object sender, EventArgs e)
        {
            XRControl cell = (XRControl)sender;
            decimal soCont = System.Convert.ToDecimal(cell.Text);
            if (soCont > 20)
            {
                lblSoTrang.Text = (Math.Round(soCont/ 20, 0, MidpointRounding.AwayFromZero) + 5).ToString();
            }
        }

    }
}
