
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_t_KDT_HopDong_Detail_t_KDT_HangMauDich]') AND parent_object_id = OBJECT_ID(N'[dbo].[t_KDT_HopDongThuongMaiDetail]'))
ALTER TABLE [dbo].[t_KDT_HopDongThuongMaiDetail] DROP CONSTRAINT [FK_t_KDT_HopDong_Detail_t_KDT_HangMauDich]
GO


UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '6.1'
      ,[Date] = getdate()
      ,[Notes] = ''
 GO