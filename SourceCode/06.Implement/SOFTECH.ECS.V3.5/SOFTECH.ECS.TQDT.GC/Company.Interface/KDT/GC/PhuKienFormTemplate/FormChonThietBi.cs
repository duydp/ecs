﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.GC
{
    public partial class FormChonThietBi : BaseForm
    {
        public HopDong HD;
        public HangPhuKienCollection HPKCollection = new HangPhuKienCollection();
        private List<ThietBi> thietBis;

        public FormChonThietBi()
        {
            InitializeComponent();

        }
        private void btnChonAll_Click(object sender, EventArgs e)
        {
            GridEXRow[] items = dgList.GetCheckedRows();
            if (items.Length < 0) return;
            if (items.Length <= 0) return;
            {
                foreach (GridEXRow i in items)
                {

                    if (i.IsChecked)
                    {

                        ThietBi thietbi = (ThietBi)i.DataRow;
                        HangPhuKien hangPK = new HangPhuKien()
                        {
                            MaHang = thietbi.Ma,
                            TenHang = thietbi.Ten,
                            MaHS = thietbi.MaHS,
                            DVT_ID = thietbi.DVT_ID,
                            SoLuong = thietbi.SoLuongDangKy,
                            ThongTinCu = thietbi.Ma,
                            NuocXX_ID = thietbi.NuocXX_ID,
                            DonGia = thietbi.DonGia,
                            TriGia = thietbi.TriGia,
                            NguyenTe_ID = thietbi.NguyenTe_ID,
                            TinhTrang = thietbi.TinhTrang
                        };
                        HPKCollection.Add(hangPK);
                    }
                }
            }
            DialogResult = DialogResult.OK;

        }

        private void FormChonThietBi_Load(object sender, EventArgs e)
        {
            thietBis = ThietBi.SelectCollectionDynamic("HopDong_ID=" + HD.ID, "");
            dgList.DataSource = thietBis;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaTB.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
