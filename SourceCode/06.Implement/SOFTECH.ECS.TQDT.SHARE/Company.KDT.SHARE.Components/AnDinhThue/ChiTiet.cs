using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.AnDinhThue
{
    public partial class ChiTiet
    {
        public int InsertUpdateBy(SqlTransaction transaction)
        {
            const string spName = "p_KDT_AnDinhThue_ChiTiet_InsertUpdateBy";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@IDAnDinhThue", SqlDbType.Int, IDAnDinhThue);
            db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Char, SacThue);
            db.AddInParameter(dbCommand, "@TienThue", SqlDbType.Decimal, TienThue);
            db.AddInParameter(dbCommand, "@Chuong", SqlDbType.VarChar, Chuong);
            db.AddInParameter(dbCommand, "@Loai", SqlDbType.Int, Loai);
            db.AddInParameter(dbCommand, "@Khoan", SqlDbType.Int, Khoan);
            db.AddInParameter(dbCommand, "@Muc", SqlDbType.Int, Muc);
            db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.Int, TieuMuc);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}