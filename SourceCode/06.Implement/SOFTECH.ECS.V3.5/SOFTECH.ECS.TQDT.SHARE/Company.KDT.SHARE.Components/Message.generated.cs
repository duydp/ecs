﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components
{
    public partial class Message
    {
        #region Properties.

        public long ID { set; get; }
        public Guid ReferenceID { set; get; }
        public long ItemID { set; get; }
        public string MessageFrom { set; get; }
        public string MessageTo { set; get; }
        public MessageTypes MessageType { set; get; }
        public MessageFunctions MessageFunction { set; get; }
        public string MessageContent { set; get; }
        public DateTime CreatedTime { set; get; }
        public string TieuDeThongBao { set; get; }
        public string NoiDungThongBao { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static Message Load(long id)
        {
            const string spName = "[dbo].[p_KDT_Message_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            Message entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new Message();
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                }
                catch (Exception)
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                }

                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFrom"))) entity.MessageFrom = reader.GetString(reader.GetOrdinal("MessageFrom"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageTo"))) entity.MessageTo = reader.GetString(reader.GetOrdinal("MessageTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageType"))) entity.MessageType = (MessageTypes)reader.GetInt32(reader.GetOrdinal("MessageType"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFunction"))) entity.MessageFunction = (MessageFunctions) reader.GetInt32(reader.GetOrdinal("MessageFunction"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageContent"))) entity.MessageContent = reader.GetString(reader.GetOrdinal("MessageContent"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedTime"))) entity.CreatedTime = reader.GetDateTime(reader.GetOrdinal("CreatedTime"));
                if (!reader.IsDBNull(reader.GetOrdinal("TieuDeThongBao"))) entity.TieuDeThongBao = reader.GetString(reader.GetOrdinal("TieuDeThongBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDungThongBao"))) entity.NoiDungThongBao = reader.GetString(reader.GetOrdinal("NoiDungThongBao"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<Message> SelectCollectionAll()
        {
            List<Message> collection = new List<Message>();
            SqlDataReader reader = (SqlDataReader)SelectReaderAll();
            while (reader.Read())
            {
                Message entity = new Message();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFrom"))) entity.MessageFrom = reader.GetString(reader.GetOrdinal("MessageFrom"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageTo"))) entity.MessageTo = reader.GetString(reader.GetOrdinal("MessageTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageType"))) entity.MessageType = (MessageTypes)reader.GetInt32(reader.GetOrdinal("MessageType"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFunction"))) entity.MessageFunction = (MessageFunctions) reader.GetInt32(reader.GetOrdinal("MessageFunction"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageContent"))) entity.MessageContent = reader.GetString(reader.GetOrdinal("MessageContent"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedTime"))) entity.CreatedTime = reader.GetDateTime(reader.GetOrdinal("CreatedTime"));
                if (!reader.IsDBNull(reader.GetOrdinal("TieuDeThongBao"))) entity.TieuDeThongBao = reader.GetString(reader.GetOrdinal("TieuDeThongBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDungThongBao"))) entity.NoiDungThongBao = reader.GetString(reader.GetOrdinal("NoiDungThongBao"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<Message> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<Message> collection = new List<Message>();

            SqlDataReader reader = (SqlDataReader)SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                Message entity = new Message();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFrom"))) entity.MessageFrom = reader.GetString(reader.GetOrdinal("MessageFrom"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageTo"))) entity.MessageTo = reader.GetString(reader.GetOrdinal("MessageTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageType"))) entity.MessageType = (MessageTypes)reader.GetInt32(reader.GetOrdinal("MessageType"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageFunction"))) entity.MessageFunction = (MessageFunctions) reader.GetInt32(reader.GetOrdinal("MessageFunction"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageContent"))) entity.MessageContent = reader.GetString(reader.GetOrdinal("MessageContent"));
                if (!reader.IsDBNull(reader.GetOrdinal("CreatedTime"))) entity.CreatedTime = reader.GetDateTime(reader.GetOrdinal("CreatedTime"));
                if (!reader.IsDBNull(reader.GetOrdinal("TieuDeThongBao"))) entity.TieuDeThongBao = reader.GetString(reader.GetOrdinal("TieuDeThongBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDungThongBao"))) entity.NoiDungThongBao = reader.GetString(reader.GetOrdinal("NoiDungThongBao"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_Message_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_Message_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_Message_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_Message_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertMessage(Guid referenceID, long itemID, string messageFrom, string messageTo, MessageTypes messageType, MessageFunctions messageFunction, string messageContent, DateTime createdTime, string tieuDeThongBao, string noiDungThongBao)
        {
            Message entity = new Message();
            entity.ReferenceID = referenceID;
            entity.ItemID = itemID;
            entity.MessageFrom = messageFrom;
            entity.MessageTo = messageTo;
            entity.MessageType = messageType;
            entity.MessageFunction = messageFunction;
            entity.MessageContent = messageContent;
            entity.CreatedTime = createdTime;
            entity.TieuDeThongBao = tieuDeThongBao;
            entity.NoiDungThongBao = noiDungThongBao;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_Message_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@MessageFrom", SqlDbType.VarChar, MessageFrom);
            db.AddInParameter(dbCommand, "@MessageTo", SqlDbType.VarChar, MessageTo);
            db.AddInParameter(dbCommand, "@MessageType", SqlDbType.Int, MessageType);
            db.AddInParameter(dbCommand, "@MessageFunction", SqlDbType.Int, MessageFunction);
            db.AddInParameter(dbCommand, "@MessageContent", SqlDbType.NVarChar, MessageContent);
            db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year <= 1753 ? DBNull.Value : (object)CreatedTime);
            db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
            db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<Message> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (Message item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateMessage(int id, Guid referenceID, long itemID, string messageFrom, string messageTo, MessageTypes messageType, MessageFunctions messageFunction, string messageContent, DateTime createdTime, string tieuDeThongBao, string noiDungThongBao)
        {
            Message entity = new Message();
            entity.ID = id;
            entity.ReferenceID = referenceID;
            entity.ItemID = itemID;
            entity.MessageFrom = messageFrom;
            entity.MessageTo = messageTo;
            entity.MessageType = messageType;
            entity.MessageFunction = messageFunction;
            entity.MessageContent = messageContent;
            entity.CreatedTime = createdTime;
            entity.TieuDeThongBao = tieuDeThongBao;
            entity.NoiDungThongBao = noiDungThongBao;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_Message_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@MessageFrom", SqlDbType.VarChar, MessageFrom);
            db.AddInParameter(dbCommand, "@MessageTo", SqlDbType.VarChar, MessageTo);
            db.AddInParameter(dbCommand, "@MessageType", SqlDbType.Int, MessageType);
            db.AddInParameter(dbCommand, "@MessageFunction", SqlDbType.Int, MessageFunction);
            db.AddInParameter(dbCommand, "@MessageContent", SqlDbType.NVarChar, MessageContent);
            db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year == 1753 ? DBNull.Value : (object)CreatedTime);
            db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
            db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<Message> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (Message item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateMessage(int id, Guid referenceID, long itemID, string messageFrom, string messageTo, MessageTypes messageType, MessageFunctions messageFunction, string messageContent, DateTime createdTime, string tieuDeThongBao, string noiDungThongBao)
        {
            Message entity = new Message();
            entity.ID = id;
            entity.ReferenceID = referenceID;
            entity.ItemID = itemID;
            entity.MessageFrom = messageFrom;
            entity.MessageTo = messageTo;
            entity.MessageType = messageType;
            entity.MessageFunction = messageFunction;
            entity.MessageContent = messageContent;
            entity.CreatedTime = createdTime;
            entity.TieuDeThongBao = tieuDeThongBao;
            entity.NoiDungThongBao = noiDungThongBao;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_Message_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@MessageFrom", SqlDbType.VarChar, MessageFrom);
            db.AddInParameter(dbCommand, "@MessageTo", SqlDbType.VarChar, MessageTo);
            db.AddInParameter(dbCommand, "@MessageType", SqlDbType.Int, MessageType);
            db.AddInParameter(dbCommand, "@MessageFunction", SqlDbType.Int, MessageFunction);
            db.AddInParameter(dbCommand, "@MessageContent", SqlDbType.NVarChar, MessageContent);
            db.AddInParameter(dbCommand, "@CreatedTime", SqlDbType.DateTime, CreatedTime.Year == 1753 ? DBNull.Value : (object)CreatedTime);
            db.AddInParameter(dbCommand, "@TieuDeThongBao", SqlDbType.NVarChar, TieuDeThongBao);
            db.AddInParameter(dbCommand, "@NoiDungThongBao", SqlDbType.NVarChar, NoiDungThongBao);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<Message> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (Message item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteMessage(int id)
        {
            Message entity = new Message();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_Message_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_Message_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<Message> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (Message item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}