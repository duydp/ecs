<%-- 
Name: WebServiceUseEntityFramwork4 Template
Author: Trinh Quoc Hung
Description: Tao file code WebService su dung Entity Framwork 4.0
--%>
<%@ CodeTemplate Language="C#" TargetLanguage="C#" Src="WebServiceUseEntityFramwork4.cst.cs" Inherits="CodeSmith.MyBaseTemplates.SqlCodeTemplate" Description="Template description here." %>
<%-- Assembly References --%>
<%@ Assembly Name="System.Data" %>
<%@ Assembly Name="SchemaExplorer" %>
<%@ Assembly Name="CodeSmith.BaseTemplates" %>
<%@ Assembly Name="CodeSmith.CustomProperties" %>
<%-- Namespace Imports --%>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="SchemaExplorer" %>
<%@ Import Namespace="CodeSmith.CustomProperties" %>
<%@ Import Namespace="CodeSmith.BaseTemplates" %>
<%@ Import Namespace="CodeSmith.MyBaseTemplates" %>
<%@ Import Namespace="System.Text.RegularExpressions" %>
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace <%= Namespace %>
{
	/// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        <% foreach (TableSchema table in SourceTables ) {%>
		#region <%= this.GetEntityName(table) %>
		
        [WebMethod]
		public List<<%= this.GetEntityName(table) %>> Get<%= this.GetEntityName(table) %>Collection(System.Guid moduleID)
		{
            try
            {
                using (var db = new Entities())
                {
                    return db.<%= this.GetEntityName(table) %>s.Where(x => x.ModuleID == moduleID).ToList();
                }
            }
            catch { return null; }
        }
        
        [WebMethod]
        public List<<%= this.GetEntityName(table) %>> Get<%= this.GetEntityName(table) %>Dynamic(System.Guid moduleID, string whereCondition, string orderByExpression)
        {
            try
            {
                using (var db = new Entities())
                {
                    return db.<%= this.GetEntityName(table) %>_SelectDynamic(whereCondition, orderByExpression).Where(x => x.ModuleID == moduleID).ToList();
                }
            }
            catch { return null; }
        }
	
		<% ColumnSchemaCollection filteredColumns = FilterExcludedColumns(table.Columns); %>
		[WebMethod]
        public string AddOrUpdate<%= this.GetEntityName(table) %>(System.Guid moduleID, ref <%= this.GetEntityName(table) %> obj<%= this.GetEntityName(table) %>, bool isNew)
		{
            try
            {                   
                using (var db = new Entities())
                {

                    if (isNew)
                    {
                        <%= this.GetEntityName(table) %> entity = new <%= this.GetEntityName(table) %>();
                        <% for (int i = 0; i < filteredColumns.Count; i++) { %>	
                        <% if (filteredColumns[i].Name.ToUpper().Equals("ID")) { %>
                        entity.<%= filteredColumns[i].Name %> = obj<%= this.GetEntityName(table) %>.<%= filteredColumns[i].Name %>;
                        <% } else if (filteredColumns[i].Name.ToUpper().Equals("MODULEID")) { %>
                        entity.<%= filteredColumns[i].Name %> = moduleID;
                        <% } else { %>
                        entity.<%= filteredColumns[i].Name %> = obj<%= this.GetEntityName(table) %>.<%= filteredColumns[i].Name %>;
                        <% }} %>

                        db.<%= this.GetEntityName(table) %>s.AddObject(entity);
                    }
                    else
                    {
                        <% if (GetCSharpPrimaryKey(filteredColumns).ToUpper() == "ID") {%>
                        string id = obj<%= this.GetEntityName(table) %>.<%= GetCSharpPrimaryKey(filteredColumns) %>;
                        <%= this.GetEntityName(table) %> entity = db.<%= this.GetEntityName(table) %>s.First(x => x.ModuleID == moduleID && x.<%= GetCSharpPrimaryKey(filteredColumns) %> == id);
                        <% } else { %>
                        <%= this.GetEntityName(table) %> entity = db.<%= this.GetEntityName(table) %>s.First(x => x.ModuleID == moduleID && x.<%= GetCSharpPrimaryKey(filteredColumns) %> == obj<%= this.GetEntityName(table) %>.<%= GetCSharpPrimaryKey(filteredColumns) %>);
                        <% } %>

                        <% for (int i = 0; i < filteredColumns.Count; i++) { %>		
                        <% if (!filteredColumns[i].IsUnique) { %>
                        <% if (!filteredColumns[i].Name.ToUpper().Equals("ID") 
                                    && !filteredColumns[i].Name.ToUpper().Equals("NGAYTAO")
                                    && !filteredColumns[i].Name.ToUpper().Equals("NGUOITAO")) { %>
                        <% if (filteredColumns[i].Name.ToUpper().Equals("MODULEID")) { %>
                        entity.<%= filteredColumns[i].Name %> = moduleID;
                        <% } else { %>
                        entity.<%= filteredColumns[i].Name %> = obj<%= this.GetEntityName(table) %>.<%= filteredColumns[i].Name %>;
                        <% }}}} %>
                    }

                    db.SaveChanges();
                }

                return string.Empty;
            }
            catch (Exception ex) { return ex.Message; }
		}
		
        [WebMethod]
        public string Delete<%= this.GetEntityName(table) %>(System.Guid moduleID, <%= GetCSharpParamsBySuffix(table.PrimaryKey.MemberColumns) %>)
        {
            try
            {
                using (var db = new Entities())
                {
                    <%= this.GetEntityName(table) %> entity = db.<%= this.GetEntityName(table) %>s.First(x => x.ModuleID == moduleID && x.ID == <%= GetCSharpParamsBySuffix(table.PrimaryKey.MemberColumns) %>);

                    db.<%= this.GetEntityName(table) %>s.DeleteObject(entity);

                    db.SaveChanges();
                }

                return string.Empty;
            }
            catch (Exception ex) { return ex.Message; }
        }
		
		#endregion
        
        <% } %>
	}	
}