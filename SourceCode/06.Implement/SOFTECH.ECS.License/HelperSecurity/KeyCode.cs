using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using System.Management;

namespace KeySecurity
{

    public class KeyCode
    {

        public static string EncryptKey
        {
            private get;
            set;
        }

        public KeyCode()
        {
        }

        static KeyCode()
        {
            EncryptKey = "Don't know.";
        }
        public static string HashMD5(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] bArr = md5.ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i <= (bArr.Length - 1); i++)
            {
                stringBuilder.Append(bArr[i].ToString("x2"));
            }
            return stringBuilder.ToString();
        }

        public static string ConvertFromBase64(string strSource)
        {
            byte[] bArr = Convert.FromBase64String(strSource);
            return Encoding.Unicode.GetString(bArr);
        }

        public static string ConvertToBase64(string strSource)
        {
            byte[] bArr = Encoding.Unicode.GetBytes(strSource);
            return Convert.ToBase64String(bArr);
        }
        /// <summary>
        /// Chuyển chuổi format xml sang object T
        /// </summary>
        /// <typeparam name="T">object</typeparam>
        /// <param name="sfmtObjectXml">format xml</param>
        /// <returns>T</returns>
        public static T Deserialize<T>(string sfmtObjectXml)
        {
            if (string.IsNullOrEmpty(sfmtObjectXml)) throw new ArgumentNullException("sfmtObjectXml không được rỗng");
            using (StringReader reader = new StringReader(sfmtObjectXml))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(reader);
            }
        }
        /// <summary>
        /// Chuyển format xml sang object
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="encoding"></param>
        /// <param name="removeDeclartion">Gở bỏ format <?xml version="1.0" encoding="utf-8"?></param>
        /// <param name="removeNameSpace">Gở bỏ namespace</param>
        /// <returns></returns>
        public static string Serializer(object obj, Encoding encoding, bool removeDeclartion, bool removeNameSpace)
        {

            XmlWriterSettings xmlWS = new XmlWriterSettings();
            if (removeDeclartion)
                xmlWS.OmitXmlDeclaration = true;
            xmlWS.Encoding = encoding;

            // xmlWS.Encoding = new UTF7Encoding(true);

            UtfStringWriter sWriter = new UtfStringWriter(encoding);
            using (XmlWriter xmlWriter = XmlWriter.Create(sWriter, xmlWS))
            {
                XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
                if (removeNameSpace)
                    namespaces.Add("", "");//Remove NameSpace
                else
                    namespaces.Add("dt", "urn:schemas-microsoft-com:datatypes");// Add Namespace
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(xmlWriter, obj, namespaces);
                return sWriter.ToString();
            }
        }
        public static string Serializer(object obj)
        {
            return Serializer(obj, Encoding.UTF8, false, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="removeDeclartion">Gở bỏ format <?xml version="1.0" encoding="utf-8"?></param>
        /// <param name="removeNameSpace">Gở bỏ namespace</param>
        /// <returns></returns>
        public static string Serializer(object obj, bool removeDeclartion, bool removeNameSpace)
        {
            return Serializer(obj, Encoding.UTF8, removeDeclartion, removeNameSpace);
        }
        /// <summary>
        /// Thực hiện hành động và đóng kết nối
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        /// <param name="action"></param>
        public static string Decrypt(string source)
        {
            byte[] bArr2 = Convert.FromBase64String(source);
            MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] bArr1 = md5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(EncryptKey));
            md5CryptoServiceProvider.Clear();
            TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
            tripleDESCryptoServiceProvider.Key = bArr1;
            tripleDESCryptoServiceProvider.Mode = CipherMode.ECB;
            tripleDESCryptoServiceProvider.Padding = PaddingMode.PKCS7;
            ICryptoTransform icryptoTransform = tripleDESCryptoServiceProvider.CreateDecryptor();
            byte[] bArr3 = icryptoTransform.TransformFinalBlock(bArr2, 0, bArr2.Length);
            tripleDESCryptoServiceProvider.Clear();
            return Encoding.UTF8.GetString(bArr3);
        }

        public static string DecryptRijndael(string cipherText)
        {
            string s1 = "Here we are.";
            string s2 = "What are you want";
            string s3 = "from me?";
            int i1 = 5, i2 = 256;
            byte[] bArr1 = Encoding.Unicode.GetBytes(s3);
            byte[] bArr2 = Encoding.Unicode.GetBytes(s2);
            byte[] bArr3 = Convert.FromBase64String(cipherText);
            Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(s1, bArr2, i1);
            byte[] bArr4 = rfc2898DeriveBytes.GetBytes(i2 / 8);
            RijndaelManaged rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.Mode = CipherMode.CBC;
            ICryptoTransform icryptoTransform = rijndaelManaged.CreateDecryptor(bArr4, bArr1);
            MemoryStream memoryStream = new MemoryStream(bArr3);
            CryptoStream cryptoStream = new CryptoStream(memoryStream, icryptoTransform, CryptoStreamMode.Read);
            byte[] bArr5 = new byte[bArr3.Length];
            int i3 = cryptoStream.Read(bArr5, 0, bArr5.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.Unicode.GetString(bArr5, 0, i3);
        }

        public static string Encrypt(string source)
        {
            byte[] bArr2 = Encoding.UTF8.GetBytes(source);
            MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
            byte[] bArr1 = md5CryptoServiceProvider.ComputeHash(Encoding.UTF8.GetBytes(EncryptKey));
            md5CryptoServiceProvider.Clear();
            TripleDESCryptoServiceProvider tripleDESCryptoServiceProvider = new TripleDESCryptoServiceProvider();
            tripleDESCryptoServiceProvider.Key = bArr1;
            tripleDESCryptoServiceProvider.Mode = CipherMode.ECB;
            tripleDESCryptoServiceProvider.Padding = PaddingMode.PKCS7;
            ICryptoTransform icryptoTransform = tripleDESCryptoServiceProvider.CreateEncryptor();
            byte[] bArr3 = icryptoTransform.TransformFinalBlock(bArr2, 0, bArr2.Length);
            tripleDESCryptoServiceProvider.Clear();
            return Convert.ToBase64String(bArr3, 0, bArr3.Length);
        }

        public static string EncryptRijndael(string plainText)
        {
            string s1 = "Here we are.";
            string s2 = "What are you want";
            string s3 = "from me?";
            int i1 = 5, i2 = 256;
            byte[] bArr1 = Encoding.Unicode.GetBytes(s3);
            byte[] bArr2 = Encoding.Unicode.GetBytes(s2);
            byte[] bArr3 = Encoding.Unicode.GetBytes(plainText);
            Rfc2898DeriveBytes rfc2898DeriveBytes = new Rfc2898DeriveBytes(s1, bArr2, i1);
            byte[] bArr4 = rfc2898DeriveBytes.GetBytes(i2 / 8);
            RijndaelManaged rijndaelManaged = new RijndaelManaged();
            rijndaelManaged.Mode = CipherMode.CBC;
            ICryptoTransform icryptoTransform = rijndaelManaged.CreateEncryptor(bArr4, bArr1);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, icryptoTransform, CryptoStreamMode.Write);
            cryptoStream.Write(bArr3, 0, bArr3.Length);
            cryptoStream.FlushFinalBlock();
            byte[] bArr5 = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            return Convert.ToBase64String(bArr5);
        }

        public static string GetMD5(string input)
        {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] bytes = ue.GetBytes(input);
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] hashBytes = md5.ComputeHash(bytes);
            string hashString = "";
            for (int i = 0; i < hashBytes.Length; i++)
            {
                hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
            }
            return hashString.PadLeft(32, '0').Substring(0, 25).ToUpper();

        }
        public static string ProcessInfo()
        {
            try
            {


                string CPUID = "BoardID:";
                ManagementObjectSearcher srch = new ManagementObjectSearcher("SELECT * FROM Win32_BaseBoard");
                foreach (ManagementObject obj in srch.Get())
                {
                    CPUID = obj.Properties["SerialNumber"].Value.ToString();
                }
                if (CPUID == "BoardID:" || CPUID.Contains("Default string"))
                {
                    CPUID = GetBIOSserNo();
                }
                return GetMD5(CPUID);
            }
            catch (Exception ex)
            {

                return GetMD5(ex.Message);
            }
        }

        public static string GetBIOSserNo()
        {

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_BIOS");

            foreach (ManagementObject wmi in searcher.Get())
            {
                try
                {
                    return "BIOS Serial Number: " +
                        wmi.GetPropertyValue("SerialNumber").ToString();
                }
                catch { }
            }
            return "BIOS Serial Number: ";
        }

        public static void WriteToFile(string content)
        {
            WriteToFile(content, Config.Install.LicenseFile);
        }
        public static void WriteToFile(string content, string filename)
        {
            TextWriter tw = new StreamWriter(filename);
            tw.Write(content);
            tw.Close();
        }
        public static string ReadFile()
        {
            return ReadFile(Config.Install.LicenseFile);
        }
        public static string ReadFile(string filename)
        {
            if (!File.Exists(filename))
                return string.Empty;
            TextReader tr = new StreamReader(filename);
            string ret = tr.ReadToEnd();
            tr.Close();
            return ret;
        }
        public static string NewID()
        {
            return Guid.NewGuid().ToString();
        }

    }
    class UtfStringWriter : StringWriter
    {
        Encoding _enCoding = Encoding.UTF8;
        public UtfStringWriter(Encoding encoding)
        {
            _enCoding = encoding;
        }
        public override Encoding Encoding
        {
            get { return _enCoding; }
        }
    }
}

