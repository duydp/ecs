﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
  public  class BranchDetail_Containers
    {
        /// <summary>
        /// Thông tin về vận đơn
        /// </summary>
      [XmlElement("TransportEquipment")]
      public List<BranchDetail_Container> TransportEquipment { get; set; }
    }
}
