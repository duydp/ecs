﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class CoppyDinhMucRegistedForm : Company.Interface.BaseForm
    {
        public Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
        public List<DinhMuc> DMCollectionCopy = new List<DinhMuc>();
        public List<DinhMuc> DMCollectionRemove = new List<DinhMuc>();
        public String MaSanPham = "";
        public DinhMucDangKy dmDangKy;

        public CoppyDinhMucRegistedForm()
        {
            InitializeComponent();
        }

        private void CoppyDinhMucRegistedForm_Load(object sender, EventArgs e)
        {
            this.Text = "SAO CHÉP ĐỊNH MỨC CHO MÃ SẢN PHẨM : " + MaSanPham.ToString();
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dm.SelectCollectionDynamic(" MaSanPham NOT IN ('" + MaSanPham.ToString() + "') AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                bool isAdd = true;
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    isAdd = true;
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    Company.BLL.SXXK.DinhMuc dinhmuc = (Company.BLL.SXXK.DinhMuc)item.DataRow;

                    DinhMuc dmCoppy = new DinhMuc();
                    dmCoppy.MaSanPham = MaSanPham;
                    dmCoppy.MaNguyenPhuLieu = dinhmuc.MaNguyenPhuLieu;
                    dmCoppy.TenNPL = dinhmuc.TenNPL;
                    dmCoppy.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                    dmCoppy.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                    dmCoppy.GhiChu = dmCoppy.GhiChu;

                    //KIỂM TRA CHỌN TRÙNG NHIỀU MÃ NPL KHI CHỌN
                    foreach (DinhMuc it in DMCollectionCopy)
                    {
                        if (it.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                        {
                            if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC CHỌN TRÊN LƯỚI .\nBẠN CÓ GHỘP ĐỊNH MỨC CỦA MÃ NPL NÀY KHÔNG ?", true) == "Yes")
                            {
                                it.DinhMucSuDung = it.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                it.TyLeHaoHut = it.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                isAdd = false;
                            }
                            else
                            {
                                isAdd = false;
                            }
                        }
                    }
                    if (isAdd)
                        DMCollectionCopy.Add(dmCoppy);
                }
                if (DMCollectionCopy.Count > 0)
                {
                    //KIỂM TRA TRÙNG MÃ NPL ĐÃ NHẬP ĐỊNH MỨC TRƯỚC ĐÓ
                    foreach (DinhMuc ite in dmDangKy.DMCollection)
                    {
                        foreach (DinhMuc dinhmuc in DMCollectionCopy)
                        {
                            if (ite.MaSanPham == dinhmuc.MaSanPham && ite.MaNguyenPhuLieu == dinhmuc.MaNguyenPhuLieu)
                            {
                                if (ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN SAO CHÉP THAY THẾ BẰNG ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                                {
                                    ite.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                                else if ((ShowMessageTQDT("MÃ NPL : [" + dinhmuc.MaNguyenPhuLieu.ToString() + "] ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC .\nBẠN CÓ MUỐN GHỘP ĐỊNH MỨC CỦA MÃ NPL KHÔNG ?", true) == "Yes"))
                                {
                                    ite.DinhMucSuDung = ite.DinhMucSuDung + dinhmuc.DinhMucSuDung;
                                    ite.TyLeHaoHut = ite.TyLeHaoHut + dinhmuc.TyLeHaoHut;
                                    DMCollectionRemove.Add(dinhmuc);
                                }
                            }
                        }
                    }
                    foreach (DinhMuc item in DMCollectionRemove)
                    {
                        DMCollectionCopy.Remove(item);
                    }
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
