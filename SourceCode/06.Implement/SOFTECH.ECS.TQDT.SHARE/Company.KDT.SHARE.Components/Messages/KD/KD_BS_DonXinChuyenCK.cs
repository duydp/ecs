﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class KD_BS_DonXinChuyenCK:DeclarationBase
    {
       [XmlElement("CustomsOfficeChangedRequest")]
       public CustomsOfficeChangedRequest CustomsOfficeChangedRequest { get; set; }
    }
}
