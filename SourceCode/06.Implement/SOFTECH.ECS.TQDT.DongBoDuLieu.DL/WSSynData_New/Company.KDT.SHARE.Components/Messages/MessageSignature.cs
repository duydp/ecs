﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Chữ ký số xác thực nội dung khai trong tag content
    /// </summary>
    [Serializable]
    [XmlRoot("Signature")]   
    public class MessageSignature
    {
        /// <summary>
        /// Chứa nội dung chữ ký số
        /// </summary>
        [XmlElement("data")]
        public string Data { get; set; }
        /// <summary>
        /// Chứa nội dung chứng thực
        /// </summary>
        [XmlElement("fileCert")]
        public string FileCert { get; set; }

    }
}
