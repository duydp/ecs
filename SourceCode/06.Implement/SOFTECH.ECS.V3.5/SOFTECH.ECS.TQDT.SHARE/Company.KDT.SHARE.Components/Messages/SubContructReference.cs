﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    public class SubContructReference
    {
        /// <summary>
        /// So Phu Luc Hop Dong
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }
        /// <summary>
        /// Ngay Hop Dong Phu Luc
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }
        /// <summary>
        /// Ngay Het Han Phu Luc
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
    }
}
