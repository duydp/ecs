﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeThongBaoThucXuat
    {        
        /// <summary>
        /// Thông tin chung
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ 
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu chứng từ || Số đăng ký tờ khai
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày gửi chứng từ || Ngày đăng ký tờ khai
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng của chứng từ (=33)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái của chứng từ || Trạng thái đại lý
        /// </summary>
        public const string status = "status";       
        /// <summary>
        /// Hải quan gửi chứng từ || Hải quan tiếp nhận
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Người khai HQ
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên người khai hải quan || Tên doanh nghiệp
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã người khai hải quan || Mã doanh nghiệp
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Doanh nghiệp XNK
        /// </summary>
        public const string Importer = "Importer";
        /// <summary>
        /// Thông tin tờ khai
        /// </summary>
        public const string DeclarationDocument = "DeclarationDocument";
        /// <summary>
        /// Loại hình xuất khẩu, nhập khẩu
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";
        /// <summary>
        /// Thông tin
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Thông tin thực xuất
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Ghi chu mo ta seqquen
        /// </summary>
        public const string sequence = "sequence";
        /// <summary>
        /// Mã nội dung phản hồi
        /// </summary>
        public const string statement = "statement";

    }
}
