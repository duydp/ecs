using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TK_Container : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public string MaDiaDiem1 { set; get; }
		public string MaDiaDiem2 { set; get; }
		public string MaDiaDiem3 { set; get; }
		public string MaDiaDiem4 { set; get; }
		public string MaDiaDiem5 { set; get; }
		public string TenDiaDiem { set; get; }
		public string DiaChiDiaDiem { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TK_Container> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TK_Container> collection = new List<KDT_VNACC_TK_Container>();
			while (reader.Read())
			{
				KDT_VNACC_TK_Container entity = new KDT_VNACC_TK_Container();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiem1"))) entity.MaDiaDiem1 = reader.GetString(reader.GetOrdinal("MaDiaDiem1"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiem2"))) entity.MaDiaDiem2 = reader.GetString(reader.GetOrdinal("MaDiaDiem2"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiem3"))) entity.MaDiaDiem3 = reader.GetString(reader.GetOrdinal("MaDiaDiem3"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiem4"))) entity.MaDiaDiem4 = reader.GetString(reader.GetOrdinal("MaDiaDiem4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiem5"))) entity.MaDiaDiem5 = reader.GetString(reader.GetOrdinal("MaDiaDiem5"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDiaDiem"))) entity.TenDiaDiem = reader.GetString(reader.GetOrdinal("TenDiaDiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDiaDiem"))) entity.DiaChiDiaDiem = reader.GetString(reader.GetOrdinal("DiaChiDiaDiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TK_Container> collection, long id)
        {
            foreach (KDT_VNACC_TK_Container item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_Container VALUES(@TKMD_ID, @MaDiaDiem1, @MaDiaDiem2, @MaDiaDiem3, @MaDiaDiem4, @MaDiaDiem5, @TenDiaDiem, @DiaChiDiaDiem, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_TK_Container SET TKMD_ID = @TKMD_ID, MaDiaDiem1 = @MaDiaDiem1, MaDiaDiem2 = @MaDiaDiem2, MaDiaDiem3 = @MaDiaDiem3, MaDiaDiem4 = @MaDiaDiem4, MaDiaDiem5 = @MaDiaDiem5, TenDiaDiem = @TenDiaDiem, DiaChiDiaDiem = @DiaChiDiaDiem, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_Container WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem1", SqlDbType.VarChar, "MaDiaDiem1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem2", SqlDbType.VarChar, "MaDiaDiem2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem3", SqlDbType.VarChar, "MaDiaDiem3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem4", SqlDbType.VarChar, "MaDiaDiem4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem5", SqlDbType.VarChar, "MaDiaDiem5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiem", SqlDbType.NVarChar, "TenDiaDiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, "DiaChiDiaDiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem1", SqlDbType.VarChar, "MaDiaDiem1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem2", SqlDbType.VarChar, "MaDiaDiem2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem3", SqlDbType.VarChar, "MaDiaDiem3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem4", SqlDbType.VarChar, "MaDiaDiem4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem5", SqlDbType.VarChar, "MaDiaDiem5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiem", SqlDbType.NVarChar, "TenDiaDiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, "DiaChiDiaDiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_Container VALUES(@TKMD_ID, @MaDiaDiem1, @MaDiaDiem2, @MaDiaDiem3, @MaDiaDiem4, @MaDiaDiem5, @TenDiaDiem, @DiaChiDiaDiem, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_TK_Container SET TKMD_ID = @TKMD_ID, MaDiaDiem1 = @MaDiaDiem1, MaDiaDiem2 = @MaDiaDiem2, MaDiaDiem3 = @MaDiaDiem3, MaDiaDiem4 = @MaDiaDiem4, MaDiaDiem5 = @MaDiaDiem5, TenDiaDiem = @TenDiaDiem, DiaChiDiaDiem = @DiaChiDiaDiem, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_Container WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem1", SqlDbType.VarChar, "MaDiaDiem1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem2", SqlDbType.VarChar, "MaDiaDiem2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem3", SqlDbType.VarChar, "MaDiaDiem3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem4", SqlDbType.VarChar, "MaDiaDiem4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiem5", SqlDbType.VarChar, "MaDiaDiem5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDiaDiem", SqlDbType.NVarChar, "TenDiaDiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, "DiaChiDiaDiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem1", SqlDbType.VarChar, "MaDiaDiem1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem2", SqlDbType.VarChar, "MaDiaDiem2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem3", SqlDbType.VarChar, "MaDiaDiem3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem4", SqlDbType.VarChar, "MaDiaDiem4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiem5", SqlDbType.VarChar, "MaDiaDiem5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDiaDiem", SqlDbType.NVarChar, "TenDiaDiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, "DiaChiDiaDiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TK_Container Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_Container_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TK_Container> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TK_Container> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TK_Container> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_Container_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_Container_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TK_Container(long tKMD_ID, string maDiaDiem1, string maDiaDiem2, string maDiaDiem3, string maDiaDiem4, string maDiaDiem5, string tenDiaDiem, string diaChiDiaDiem, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_TK_Container entity = new KDT_VNACC_TK_Container();	
			entity.TKMD_ID = tKMD_ID;
			entity.MaDiaDiem1 = maDiaDiem1;
			entity.MaDiaDiem2 = maDiaDiem2;
			entity.MaDiaDiem3 = maDiaDiem3;
			entity.MaDiaDiem4 = maDiaDiem4;
			entity.MaDiaDiem5 = maDiaDiem5;
			entity.TenDiaDiem = tenDiaDiem;
			entity.DiaChiDiaDiem = diaChiDiaDiem;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TK_Container_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaDiaDiem1", SqlDbType.VarChar, MaDiaDiem1);
			db.AddInParameter(dbCommand, "@MaDiaDiem2", SqlDbType.VarChar, MaDiaDiem2);
			db.AddInParameter(dbCommand, "@MaDiaDiem3", SqlDbType.VarChar, MaDiaDiem3);
			db.AddInParameter(dbCommand, "@MaDiaDiem4", SqlDbType.VarChar, MaDiaDiem4);
			db.AddInParameter(dbCommand, "@MaDiaDiem5", SqlDbType.VarChar, MaDiaDiem5);
			db.AddInParameter(dbCommand, "@TenDiaDiem", SqlDbType.NVarChar, TenDiaDiem);
			db.AddInParameter(dbCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, DiaChiDiaDiem);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TK_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_Container item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TK_Container(long id, long tKMD_ID, string maDiaDiem1, string maDiaDiem2, string maDiaDiem3, string maDiaDiem4, string maDiaDiem5, string tenDiaDiem, string diaChiDiaDiem, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_TK_Container entity = new KDT_VNACC_TK_Container();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaDiaDiem1 = maDiaDiem1;
			entity.MaDiaDiem2 = maDiaDiem2;
			entity.MaDiaDiem3 = maDiaDiem3;
			entity.MaDiaDiem4 = maDiaDiem4;
			entity.MaDiaDiem5 = maDiaDiem5;
			entity.TenDiaDiem = tenDiaDiem;
			entity.DiaChiDiaDiem = diaChiDiaDiem;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TK_Container_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaDiaDiem1", SqlDbType.VarChar, MaDiaDiem1);
			db.AddInParameter(dbCommand, "@MaDiaDiem2", SqlDbType.VarChar, MaDiaDiem2);
			db.AddInParameter(dbCommand, "@MaDiaDiem3", SqlDbType.VarChar, MaDiaDiem3);
			db.AddInParameter(dbCommand, "@MaDiaDiem4", SqlDbType.VarChar, MaDiaDiem4);
			db.AddInParameter(dbCommand, "@MaDiaDiem5", SqlDbType.VarChar, MaDiaDiem5);
			db.AddInParameter(dbCommand, "@TenDiaDiem", SqlDbType.NVarChar, TenDiaDiem);
			db.AddInParameter(dbCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, DiaChiDiaDiem);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TK_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_Container item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TK_Container(long id, long tKMD_ID, string maDiaDiem1, string maDiaDiem2, string maDiaDiem3, string maDiaDiem4, string maDiaDiem5, string tenDiaDiem, string diaChiDiaDiem, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_TK_Container entity = new KDT_VNACC_TK_Container();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.MaDiaDiem1 = maDiaDiem1;
			entity.MaDiaDiem2 = maDiaDiem2;
			entity.MaDiaDiem3 = maDiaDiem3;
			entity.MaDiaDiem4 = maDiaDiem4;
			entity.MaDiaDiem5 = maDiaDiem5;
			entity.TenDiaDiem = tenDiaDiem;
			entity.DiaChiDiaDiem = diaChiDiaDiem;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_Container_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@MaDiaDiem1", SqlDbType.VarChar, MaDiaDiem1);
			db.AddInParameter(dbCommand, "@MaDiaDiem2", SqlDbType.VarChar, MaDiaDiem2);
			db.AddInParameter(dbCommand, "@MaDiaDiem3", SqlDbType.VarChar, MaDiaDiem3);
			db.AddInParameter(dbCommand, "@MaDiaDiem4", SqlDbType.VarChar, MaDiaDiem4);
			db.AddInParameter(dbCommand, "@MaDiaDiem5", SqlDbType.VarChar, MaDiaDiem5);
			db.AddInParameter(dbCommand, "@TenDiaDiem", SqlDbType.NVarChar, TenDiaDiem);
			db.AddInParameter(dbCommand, "@DiaChiDiaDiem", SqlDbType.NVarChar, DiaChiDiaDiem);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TK_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_Container item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TK_Container(long id)
		{
			KDT_VNACC_TK_Container entity = new KDT_VNACC_TK_Container();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_Container_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TK_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_Container item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}