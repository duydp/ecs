﻿namespace Company.Interface.Report
{
    partial class ReportViewTKNForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkInMaHang = new Janus.Windows.EditControls.UICheckBox();
            this.chkInHinhNen = new Janus.Windows.EditControls.UICheckBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnPrint = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.cboToKhai = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uiButton3 = new Janus.Windows.EditControls.UIButton();
            this.txtTenNhomHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkBanLuuHaiQuan = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Size = new System.Drawing.Size(833, 442);
            // 
            // printControl1
            // 
            this.printControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.printControl1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(1, 74);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.PrintingSystem = this.printingSystem1;
            this.printControl1.Size = new System.Drawing.Size(830, 366);
            this.printControl1.TabIndex = 0;
            // 
            // printingSystem1
            // 
            this.printingSystem1.ShowMarginsWarning = false;
            this.printingSystem1.ShowPrintStatusDialog = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chkBanLuuHaiQuan);
            this.uiGroupBox1.Controls.Add(this.chkInMaHang);
            this.uiGroupBox1.Controls.Add(this.chkInHinhNen);
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.btnPrint);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.cboToKhai);
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 3);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(459, 65);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkInMaHang
            // 
            this.chkInMaHang.Location = new System.Drawing.Point(239, 42);
            this.chkInMaHang.Name = "chkInMaHang";
            this.chkInMaHang.Size = new System.Drawing.Size(75, 23);
            this.chkInMaHang.TabIndex = 9;
            this.chkInMaHang.Text = "In mã hàng";
            this.chkInMaHang.VisualStyleManager = this.vsmMain;
            this.chkInMaHang.CheckedChanged += new System.EventHandler(this.chkInMaHang_CheckedChanged);
            // 
            // chkInHinhNen
            // 
            this.chkInHinhNen.Location = new System.Drawing.Point(158, 42);
            this.chkInHinhNen.Name = "chkInHinhNen";
            this.chkInHinhNen.Size = new System.Drawing.Size(75, 23);
            this.chkInHinhNen.TabIndex = 8;
            this.chkInHinhNen.Text = "In hình nền";
            this.chkInHinhNen.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(359, 17);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 4;
            this.uiButton1.Text = "&Export Excel";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(278, 17);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 23);
            this.btnPrint.TabIndex = 3;
            this.btnPrint.Text = "&In";
            this.btnPrint.VisualStyleManager = this.vsmMain;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Chọn tờ khai chính / phụ lục";
            // 
            // cboToKhai
            // 
            this.cboToKhai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Tờ khai chính";
            uiComboBoxItem2.Value = 0;
            this.cboToKhai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem2});
            this.cboToKhai.Location = new System.Drawing.Point(158, 18);
            this.cboToKhai.Name = "cboToKhai";
            this.cboToKhai.Size = new System.Drawing.Size(114, 21);
            this.cboToKhai.TabIndex = 0;
            this.cboToKhai.VisualStyleManager = this.vsmMain;
            this.cboToKhai.SelectedIndexChanged += new System.EventHandler(this.cboToKhai_SelectedIndexChanged);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.uiButton3);
            this.uiGroupBox2.Controls.Add(this.txtTenNhomHang);
            this.uiGroupBox2.Location = new System.Drawing.Point(465, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(366, 65);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên hàng 1";
            // 
            // uiButton3
            // 
            this.uiButton3.Location = new System.Drawing.Point(245, 16);
            this.uiButton3.Name = "uiButton3";
            this.uiButton3.Size = new System.Drawing.Size(75, 23);
            this.uiButton3.TabIndex = 3;
            this.uiButton3.Text = "Áp &dụng";
            this.uiButton3.VisualStyleManager = this.vsmMain;
            this.uiButton3.Click += new System.EventHandler(this.uiButton3_Click);
            // 
            // txtTenNhomHang
            // 
            this.txtTenNhomHang.Location = new System.Drawing.Point(75, 18);
            this.txtTenNhomHang.Name = "txtTenNhomHang";
            this.txtTenNhomHang.Size = new System.Drawing.Size(164, 21);
            this.txtTenNhomHang.TabIndex = 4;
            this.txtTenNhomHang.VisualStyleManager = this.vsmMain;
            // 
            // chkBanLuuHaiQuan
            // 
            this.chkBanLuuHaiQuan.Location = new System.Drawing.Point(320, 42);
            this.chkBanLuuHaiQuan.Name = "chkBanLuuHaiQuan";
            this.chkBanLuuHaiQuan.Size = new System.Drawing.Size(114, 23);
            this.chkBanLuuHaiQuan.TabIndex = 10;
            this.chkBanLuuHaiQuan.Text = "In bản lưu Hải quan";
            this.chkBanLuuHaiQuan.VisualStyleManager = this.vsmMain;
            this.chkBanLuuHaiQuan.CheckedChanged += new System.EventHandler(this.chkBanLuuHaiQuan_CheckedChanged);
            // 
            // ReportViewTKNForm
            // 
            this.AcceptButton = this.btnPrint;
            this.ClientSize = new System.Drawing.Size(833, 442);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ReportViewTKNForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "In tờ khai nhập";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ReportViewForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cboToKhai;
        private Janus.Windows.EditControls.UIButton btnPrint;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton uiButton3;
        private Janus.Windows.EditControls.UIButton uiButton1;
        public System.Windows.Forms.Label label3;
        public Janus.Windows.GridEX.EditControls.EditBox txtTenNhomHang;
        private Janus.Windows.EditControls.UICheckBox chkInMaHang;
        private Janus.Windows.EditControls.UICheckBox chkInHinhNen;
        private Janus.Windows.EditControls.UICheckBox chkBanLuuHaiQuan;
    }
}