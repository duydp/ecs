﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.Interface;
using System.Data;
using Logger;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class Mau16_TT38_ChiTiet : DevExpress.XtraReports.UI.XtraReport
    {
        public int NamDangKy;
        string MaHang = "";
        int STT = 0;
        decimal SoLuongChuyenHD_G_F = 0;
        decimal SoLuongTamNhap_G_F = 0;
        decimal SoLuongTaiXuat_G_F = 0;
        decimal SoLuongConLai_G_F = 0;
        public DataTable tb_Mau15 = new DataTable();
        public Mau16_TT38_ChiTiet()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            //NamDangKy = Int32.Parse(where);
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontToKhai);
            DataTable dtTable = new DataTable();
            dtTable = tb_Mau15;//GetDataSource();


            //Int32 index = 0;
            ////string MaHang = "";
            ////dtTable.Columns.Add(new DataColumn("STT"));
            //for (int i = 0; i < dtTable.Rows.Count; i++)
            //{

            //    if (dtTable.Rows[i]["Ma"].ToString() != MaHang)
            //    {
            //        index++;
            //    }
            //    MaHang = dtTable.Rows[i]["Ma"].ToString();
            //    dtTable.Rows[i]["STT"] = index;

            //}
            
            DataView dv = dtTable.DefaultView;
            dv.Sort = "SoHopDong";
            //dv.Sort = "SoTaiKhoan";
            dv.Sort = "Ma";
            dtTable = dv.ToTable();
            Int32 index = 0;
            //string MaHang = "";
            //dtTable.Columns.Add(new DataColumn("STT"));
            for (int i = 0; i < dtTable.Rows.Count; i++)
            {

                if (dtTable.Rows[i]["Ma"].ToString() != MaHang)
                {
                    index++;
                }
                MaHang = dtTable.Rows[i]["Ma"].ToString();
                dtTable.Rows[i]["STT"] = index;

            }
            DetailReport.DataSource = dtTable;
            

            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            //lbNam.Text = NamDangKy.ToString();
            this.GroupHeader3.Visible = true;
            this.GroupHeader3.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoHopDong")
                
            });
            
            this.GroupHeader1.Visible = false;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("Ma"),
                new DevExpress.XtraReports.UI.GroupField("Ten"),
            });
            txtHopDong.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDong");
            //txtSoTaiKhoan_G_H_2.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            
            //lbNam.DataBindings.Add("Text",DetailReport.DataSource,"NamDangKy");
            //txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            //txtTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            //txtSoHopDong_D.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDong");
            txtTenMM_TB.DataBindings.Add("Text",DetailReport.DataSource,"Ten");
            txtMaMM_TB_G_F.DataBindings.Add("Text", DetailReport.DataSource,"Ma");
            txt_TenMMTB_G_F.DataBindings.Add("Text", DetailReport.DataSource, "Ten");
            txtMaMM_TB.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID", Company.KDT.SHARE.Components.Globals.FormatNumber(6));
            txtSoLuongTamNhap.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTamNhap");
            txt_SoLuongTamNhap_G_F.DataBindings.Add("Text", null, "SoLuongTamNhap");
            txt_SoLuongTamNhap_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N0}");
            //txtMaLoaiHinh.DataBindings.Add("Text", DetailReport.DataSource, "MaLoaiHinh");
            txtSoLuongTaiXuat.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongChuyenHD");
            txt_SoLuongTaiXuat_G_F.DataBindings.Add("Text", null, "SoLuongChuyenHD");
            txt_SoLuongTaiXuat_G_F.Summary = new XRSummary(SummaryRunning.Group, SummaryFunc.Sum, "{0:N0}");
            //txtDVT_ID.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            txtSoLuong_ChuyenHD.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongTaiXuat", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            txtSoLuongChuyenHD_G_F.DataBindings.Add("Text", null, "SoLuongTaiXuat");
            txtSoLuongChuyenHD_G_F.Summary = new XRSummary(SummaryRunning.Group,SummaryFunc.Sum,"{0:N0}");
            //txtSo_NgayHD.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDong", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TonTriGia", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource,"GhiChu");
        }
        

        private void txtSTT_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            STT++;
            txtSTT.Text = STT.ToString();
        }

        private void txtSoLuong_G_F_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    SoLuongChuyenHD_G_F += Convert.ToDecimal(txtSoLuong_ChuyenHD.Text);

            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtSoLuong_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (SoLuongChuyenHD_G_F == 0)
                {
                    txtSoLuongChuyenHD_G_F.Text = "0";
                }
                else
                {
                    txtSoLuongChuyenHD_G_F.Text = SoLuongChuyenHD_G_F.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtHopDong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            txtHopDong.Text = "Số hợp đồng: " + txtHopDong.Text;
        }

        private void txt_TenMMTB_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txt_TenMMTB_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txt_DVT_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txt_DVT_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txt_SoLuongTamNhap_G_F_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                if (SoLuongTamNhap_G_F == 0)
                {
                    txt_SoLuongTamNhap_G_F.Text ="0";
                }
                else
                {
                    txt_SoLuongTamNhap_G_F.Text = SoLuongTamNhap_G_F.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txt_SoLuongTamNhap_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (SoLuongTamNhap_G_F == 0)
                {
                    txt_SoLuongTamNhap_G_F.Text = "0";
                }
                else
                {
                    txt_SoLuongTamNhap_G_F.Text = SoLuongTamNhap_G_F.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txt_SoLuongTaiXuat_G_F_AfterPrint(object sender, EventArgs e)
        {
        }

        private void txt_SoLuongTaiXuat_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (SoLuongTaiXuat_G_F == 0)
                {
                    txt_SoLuongTaiXuat_G_F.Text = "0";
                }
                else
                {
                    txt_SoLuongTaiXuat_G_F.Text = SoLuongTaiXuat_G_F.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txt_SoLuongConLai_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                SoLuongConLai_G_F = (decimal)txt_SoLuongTamNhap_G_F.Summary.GetResult() - (decimal)txt_SoLuongTaiXuat_G_F.Summary.GetResult() - (decimal)txtSoLuongChuyenHD_G_F.Summary.GetResult();//Convert.ToDecimal(txt_SoLuongTamNhap_G_F.Text) - Convert.ToDecimal(txt_SoLuongTaiXuat_G_F.Text);
                txt_SoLuongConLai_G_F.Text = SoLuongConLai_G_F.ToString();
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txt_SoLuongConLai_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtMaMM_TB_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtMaMM_TB_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txtSoLuongTamNhap_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    SoLuongTamNhap_G_F += Convert.ToDecimal(txtSoLuongTamNhap.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtSoLuongTaiXuat_AfterPrint(object sender, EventArgs e)
        {
        }

        private void txtSoLuong_ChuyenHD_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                if (SoLuongChuyenHD_G_F == 0)
                {
                    txtSoLuongChuyenHD_G_F.Text = "0";
                }
                else
                {
                    txtSoLuongChuyenHD_G_F.Text = SoLuongChuyenHD_G_F.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtSoLuongConLai_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                if (SoLuongConLai_G_F == 0)
                {
                    txt_SoLuongConLai_G_F.Text = "0";
                }
                else
                {
                    txt_SoLuongConLai_G_F.Text = SoLuongConLai_G_F.ToString("N0");
                }
            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void txtSoLuongTamNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //        SoLuongTamNhap_G_F += Convert.ToDecimal(txtSoLuongTamNhap.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtSoLuongTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    SoLuongTaiXuat_G_F += Convert.ToDecimal(txtSoLuongTaiXuat.Text);

            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtSoLuong_ChuyenHD_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    SoLuongChuyenHD_G_F += Convert.ToDecimal(txtSoLuong_ChuyenHD.Text);

            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtSoLuongConLai_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }

        private void txtDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtDVT.Text))
                {
                    decimal c = Convert.ToDecimal(txtDVT.Text);
                    if (c == 0)
                    {
                        txtDVT.Text = "0";
                    }
                    else
                        txtDVT.Text = c.ToString("N6");
                }

            }
            catch (Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSTT_AfterPrint(object sender, EventArgs e)
        {
            //txtSTT.Text = STT.ToString();
        }

        private void txtSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            STT++;
            txtSTT.Text = STT.ToString();
        }

    }
}
