﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe08_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public DataSet dsBK = new DataSet();
        private int STT = 0;
        public BangKe08_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP; 

            //lblDVTSP.Text = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.GetName("MaSP"); 
            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            //lblDateSecond.Text = Company.GC.BLL.KDT.GC.HopDong.GetSignHDDate(IDHopDong);
            //lblTimeLimitND.Text = Company.GC.BLL.KDT.GC.HopDong.GetEndHDDate(IDHopDong);
            //lblDateFirst.Text = Company.GC.BLL.KDT.GC.HopDong.GetSignHDDate(IDHopDong);
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            //DataTable dt = new Company.GC.BLL.GC.BKToKhai().getToKhaiNK(this.HD.ID).Tables[0];
            DataTable dt = dsBK.Tables[0];
            dt.TableName = "t_GC_BKToKhai";
            this.DataSource = dt;

            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSoTK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".VT");
            lblCBD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".CanBoDuyet");
            lblCKN.DataBindings.Add("Text", this.DataSource, dt.TableName + ".CKXN");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GhiChu");
            lblNgayTK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTK", "{0:dd/MM/yyyy}");
            xrLabel19.Text = GlobalSettings.TieudeNgay;

        }

        private void BangKe08_HQGC_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }
    }
}
