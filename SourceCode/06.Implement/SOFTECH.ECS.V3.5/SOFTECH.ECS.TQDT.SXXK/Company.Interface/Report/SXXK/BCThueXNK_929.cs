using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;

namespace Company.Interface.Report.SXXK
{
    public partial class BCThueXNK_929 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private decimal TongTienThueTKTiep = 0;
        private decimal TongTienThueHoan = 0;
        private decimal TongLuongSuDung = 0;
        private decimal TongTienThueHoanTatCa = 0;
        private decimal TongTienThueHoanCuaToKhai = 0;
        private decimal TongThueNKPhaiNopCuaToKhai = 0;
        private decimal TongTienThueTKTiepCuaToKhai = 0;
        public int SoHoSo;
        public BCThueXNK_929()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BCThueXNK().SelectDynamic("(a.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, "").Tables[0];
            dt.TableName = "BCThueXNK";
            this.DataSource = dt;

            lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";
            //if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            //{
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("MaNPL", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});



            //}
            //else
            //{
            //    this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField("MaNPL", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("DonGiaTT", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending),
            //    new DevExpress.XtraReports.UI.GroupField("ThueNKNop", DevExpress.XtraReports.UI.XRColumnSortOrder.Ascending)});
            //}
            
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if(SoHoSo>0)
                lblSHSTK.Text =  SoHoSo +"";
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            //lblNgayThucNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucNhap", "{0:dd/MM/yy}");
            //lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblDonGiaTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaTT", "{0:g20}");
            lblTyGiaTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaTT", "{0:n3}");
            //lblThueSuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuat", "{0:n0}");
            //lblThueNKNop.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNKNop", "{0:n0}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucXuat", "{0:dd/MM/yy}");
            lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTon", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTienThueHoan.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
            //lblTongTienThueTKTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueTKTiep", "{0:n0}");
            //xrTableCell21.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
           // xrTableCell22.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            xrTableCell23.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            xrTableCell24.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DonGiaTT", "{0:g20}");
            xrTableCell25.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TyGiaTT", "{0:n3}");
           // xrTableCell26.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueSuat", "{0:n0}");
           // xrTableCell27.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNKNop", "{0:n0}");
           // lblTongThueKhongThu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TienThueHoan", "{0:n0}");
            xrLabel76.Text = xrLabel5.Text = GlobalSettings.TieudeNgay;
        }
        private decimal total2 = 0;
        private decimal total1 = 0;
        private void lblTongThueKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            //lblTongThueKhongThu.Text = this.TongTienThueHoanTatCa.ToString("N0");
        }

        private void GroupFooter1_AfterPrint(object sender, EventArgs e)
        {
            total2 += Convert.ToDecimal(lblTongTienThueTKTiep.Text);
            this.TongTienThueHoanCuaToKhai += Convert.ToDecimal(lblTongTienThueHoan.Text);
            //this.TongThueNKPhaiNopCuaToKhai += Convert.ToDecimal(xrTableCell27.Text);
            this.TongTienThueTKTiepCuaToKhai += Convert.ToDecimal(lblTongTienThueTKTiep.Text);
        }

        private void lblTongThueLanSauTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRLabel)sender).Text = total2.ToString("N0"); 
        }

        private void lblThueSuat_AfterPrint(object sender, EventArgs e)
        {
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "%";
        }

        private void xrTableCell26_AfterPrint(object sender, EventArgs e)
        {
            ((XRLabel)sender).Text = ((XRLabel)sender).Text + "%";
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void xrTableCell21_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrTableCell21.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();

        }

        private void BCThueXNK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
            this.TongTienThueHoanTatCa = 0;
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            this.TongLuongSuDung = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblSTT.Text = this.STT + "";
        }

        private void Detail_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("SoToKhaiXuat").ToString() == "0") e.Cancel = true;
        }

        private void lblTongTienThueTKTiep_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.TongTienThueTKTiep = (decimal)GetCurrentColumnValue("ThueNKNop") - this.TongTienThueHoan;
            lblTongTienThueTKTiep.Text = this.TongTienThueTKTiep.ToString("N0");
        }

        private void lblTongTienThueHoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.TongTienThueHoan = Math.Round(this.TongLuongSuDung * (decimal)GetCurrentColumnValue("ThueXNK") / (decimal)GetCurrentColumnValue("Luong"), MidpointRounding.AwayFromZero);
            if (this.TongLuongSuDung == (decimal)GetCurrentColumnValue("LuongNhap")) this.TongTienThueHoan = (decimal)GetCurrentColumnValue("ThueNKNop");
            lblTongTienThueHoan.Text = this.TongTienThueHoan.ToString("N0");
            this.TongTienThueHoanTatCa += Math.Round(this.TongTienThueHoan, MidpointRounding.AwayFromZero);
        }

        private void lblLuongNPLSuDung_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.TongLuongSuDung += (decimal)GetCurrentColumnValue("LuongNPLSuDung");
        }

        private void lblTongTonCuoi_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            decimal t = (decimal)GetCurrentColumnValue("LuongNhap") - this.TongLuongSuDung;
            lblTongTonCuoi.Text = t.ToString("n" + GlobalSettings.SoThapPhan.LuongNPL);
        }

        private void xrLabel34_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel34.Text = this.TongTienThueHoanCuaToKhai.ToString("N0");
        }

        private void GroupHeader2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.TongTienThueHoanCuaToKhai = 0;
            this.TongThueNKPhaiNopCuaToKhai = 0;
            this.TongTienThueTKTiepCuaToKhai = 0;
        }

        private void xrLabel35_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            xrLabel35.Text = "Tổng thuế không thu / xin hoàn của tờ khai " + Convert.ToString(GetCurrentColumnValue("SoToKhaiNhap")) + "/" + GetCurrentColumnValue("MaLoaiHinhNhap").ToString() + "/" + Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyNhap")).Year;
        }

        private void lblTongTienThueNKPhaiNopToKhai_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           // lblTongTienThueNKPhaiNopToKhai.Text = this.TongThueNKPhaiNopCuaToKhai.ToString("N0");
        }

        private void lblTongTienThueTKTiepToKhai_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongTienThueTKTiepToKhai.Text = this.TongTienThueTKTiepCuaToKhai.ToString("N0");
        }

    }
}
