using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
using System.IO;
using Company.BLL;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.SXXK
{
    public partial class FormSendDinhMuc : BaseForm
    {
        DinhMucDangKyCollection collection = new DinhMucDangKyCollection();//tat ca to khai
        DinhMucDangKyCollection collectionSelected = new DinhMucDangKyCollection();//to khai dc chon
        DinhMucDangKy dmdk = new DinhMucDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        // Đếm thứ tự tờ khai định mức
        int k = 0;
        public FormSendDinhMuc()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.LayPhanHoiV3();
            else
                LaySoTiepNhanDT();
        }


        private void BK02WizardForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }

        private void RemoveDMCollection(long id)
        {
            for (int i = 0; i < this.collection.Count; i++)
            {
                if (this.collection[i].ID == id)
                {
                    this.collection.Remove(collection[i]);
                    break;
                }
            }
        }
        private void RemoveDMCollectionSelect(long id)
        {
            for (int i = 0; i < this.collectionSelected.Count; i++)
            {
                if (this.collectionSelected[i].ID == id)
                {
                    this.collectionSelected.Remove(collectionSelected[i]);
                    break;
                }
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            string sql = " 1=1 and MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'";
            if (btnXacNhan.Enabled == true)
            {
                //sql += " and id in (select master_id from t_KDT_SXXK_MsgSend where LoaiHS = 'DM')";
                sql += " and trangthaixuly = -1";
            }
            else
            {
                //sql += " and id not in (select master_id from t_KDT_SXXK_MsgSend where LoaiHS = 'DM')";
                if (btnSend.Enabled == true)
                {
                    sql += " and trangthaixuly = -1";
                }
                else if (btnNhan.Enabled == true)
                {
                    sql += " and trangthaixuly = 0";
                }
                else if (btnHuy.Enabled == true)
                {
                    sql += " and trangthaixuly = 0 or trangthaixuly = 2";
                }
            }
            collection = DinhMucDangKy.SelectCollectionDynamic(sql, "");
            foreach (DinhMucDangKy dm in collectionSelected)
            {
                RemoveDMCollection(dm.ID);
            }
            dgTK.DataSource = collection;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.SendV3();
            else
                sendItemsSelect();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (GridEXRow row in dgTK.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    DinhMucDangKy dmOld = (DinhMucDangKy)row.DataRow;
                    DinhMucDangKy dmNew = new DinhMucDangKy();
                    dmNew.ID = dmOld.ID;
                    dmNew.NgayTiepNhan = dmOld.NgayTiepNhan;
                    dmNew.SoTiepNhan = dmOld.SoTiepNhan;
                    dmNew.TrangThaiXuLy = dmOld.TrangThaiXuLy;
                    dmNew.MaHaiQuan = dmOld.MaHaiQuan;
                    dmNew.MaDoanhNghiep = dmOld.MaDoanhNghiep;
                    dmNew.GUIDSTR = dmOld.GUIDSTR;
                    this.collectionSelected.Add(dmNew);
                }
            }
            foreach (DinhMucDangKy dmSelect in collectionSelected)
            {
                RemoveDMCollection(dmSelect.ID);
            }
            dgTK.DataSource = collection;
            try
            {
                dgTK.Refetch();
            }
            catch
            {
                dgTK.Refresh();
            }
            gridEX1.DataSource = collectionSelected;
            gridEX1.Refetch();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DinhMucDangKyCollection collectionUnSelected = new DinhMucDangKyCollection();//tat ca to khai bo chon
            foreach (GridEXRow row in gridEX1.GetCheckedRows())
            {
                if (row.RowType == RowType.Record)
                {
                    DinhMucDangKy dmOld = (DinhMucDangKy)row.DataRow;
                    DinhMucDangKy dmNew = new DinhMucDangKy();
                    dmNew.ID = dmOld.ID;
                    dmNew.NgayTiepNhan = dmOld.NgayTiepNhan;
                    dmNew.TrangThaiXuLy = dmOld.TrangThaiXuLy;
                    this.collection.Add(dmNew);
                    collectionUnSelected.Add(dmNew);
                }
            }
            foreach (DinhMucDangKy dm in collectionUnSelected)
            {
                RemoveDMCollectionSelect(dm.ID);
            }
            gridEX1.DataSource = collectionSelected;
            try
            {
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }

            btnSearch_Click(null, null);
        }
        private void LaySoTiepNhanDT()
        {
            if (collectionSelected.Count == 0)
            {
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                this.Cursor = Cursors.WaitCursor;

                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                bool ok = false;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (DinhMucDangKy dmSelect in collectionSelected)
                {
                    DinhMucDangKy dmdkSelect = DinhMucDangKy.Load(dmSelect.ID);
                    dmdkSelect.LoadDMCollection();
                    MsgSend sendXML = new MsgSend();
                    try
                    {
                        k++;
                        itemOK++;
                        {
                            sendXML.LoaiHS = "DM";
                            sendXML.master_id = dmSelect.ID;
                            string st = "";
                            if (sendXML.Load())
                            {
                                st = dmSelect.LayPhanHoi(password, sendXML.msg);
                                if (st != "")
                                {
                                    --itemOK;
                                    ok = false;
                                }
                                else
                                    ok = true;
                            }
                        }
                    }
                    #region

                    catch (Exception ex)
                    {
                        #region lỗi
                        if (ex.ToString().Contains("Lỗi do"))
                        {
                            MLMessages(ex.ToString(), "MSG_SEN09", "", false);
                            //return;
                        }
                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            if (k == collectionSelected.Count)
                            {
                                message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?";
                            }
                            else
                            {
                                message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?";
                            }
                        }
                        else
                        {
                            if (k == collectionSelected.Count)
                            {
                                message = "Mailing list " + k.ToString() + "Don't receive information?\nDo you want to push queue ?";
                            }
                            else
                            {
                                //message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?\nBạn có muốn đưa vào hàng đợi và \ntiếp tục gửi dữ liệu của các danh sách tiếp không";
                                message = "Mailing list " + k.ToString() + "Don't receive information?\nDo you want to push queue ? \n Do you want to continue sending data?";
                            }
                        }

                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                                ok = false;
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                            //hd.TrangThai = dmSelect.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //hd.PassWord = password;
                            //hd.ID = dmSelect.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        #endregion
                    }
                    #endregion
                    if (ok)//if (itemOK > 0)
                    {
                        if (dmSelect.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            MLMessages("Danh sách định mức có ID=" + dmSelect.ID + ": Đã duyệt chính thức.", "MSG_SEN18", "", false);
                            sendXML.LoaiHS = "DM";
                            sendXML.master_id = dmSelect.ID;
                            if (sendXML.Load())
                                sendXML.Delete();
                        }
                        else if (dmSelect.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            if (dmSelect.PhanLuong != "")
                            {
                                string tenluong = "Xanh";
                                if (dmSelect.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                                    tenluong = "Vàng";
                                else if (dmSelect.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                                    tenluong = "Đỏ";
                                MLMessages("Danh sách định mức có ID=" + dmSelect.ID + " đã được phân luồng: " + tenluong + "\n" + dmSelect.HUONGDAN, "MSG_SEN08", "", false);
                            }
                            else
                            {
                                MLMessages("Danh sách định mức có ID=" + dmSelect.ID + " có Số tiếp nhận là: " + dmSelect.SoTiepNhan + "\n Ngày tiếp nhận:" + dmSelect.NgayTiepNhan, "MSG_SEN08", "", false);
                            }
                        }
                        else if (dmSelect.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            MLMessages("Danh sách định mức có ID=" + dmSelect.ID + ": Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN09", "", false);
                            sendXML.LoaiHS = "DM";
                            sendXML.master_id = dmSelect.ID;
                            if (sendXML.Load())
                                sendXML.Delete();
                        }
                        ok = false;
                    }
                    else
                        MLMessages("Danh sách định mức có ID = " + dmSelect.ID + " chưa có phản hồi từ hải quan !", "MSG_SEN08", "", false);
                }
                if (itemOK > 0)
                    gridEX1.Refetch();

            }
            catch (Exception ex)
            {
                #region Lỗi khi khai báo

                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                #endregion
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void sendItemsSelect()
        {
            for (int i = 0; i < dgTK.RowCount; i++)
            {
                GridEXRow row = dgTK.GetRow(i);
                if (bool.Parse(row.Cells["Select"].Value.ToString()))
                    collectionSelected.Add((DinhMucDangKy)row.DataRow);
            }
            if (collectionSelected.Count == 0)
            {
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (DinhMucDangKy dmSelect in collectionSelected)
                {
                    DinhMucDangKy dmdkSelect = DinhMucDangKy.Load(dmSelect.ID);
                    dmdkSelect.LoadDMCollection();
                    try
                    {
                        k++;
                        itemOK++;
                        {
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "DM";
                            sendXML.master_id = dmdkSelect.ID;

                            if (!sendXML.Load())
                            {
                                xmlCurrent = dmdkSelect.WSSendXML(password);
                                sendXML.msg = xmlCurrent;
                            }
                            sendXML.func = 1;
                            xmlCurrent = "";
                            sendXML.InsertUpdate();
                        }
                    }
                    catch (Exception ex)
                    {
                        #region lỗi

                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            if (k == collectionSelected.Count)
                            {
                                message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?";
                            }
                            else
                            {
                                message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?";
                            }
                        }
                        else
                        {
                            if (k == collectionSelected.Count)
                            {
                                message = "Mailing list " + k.ToString() + "Don't receive information?\nDo you want to push queue ?";
                            }
                            else
                            {
                                message = "The mailing list " + k.ToString() + " hasn’t  received information yet?\nDo you want to push in queue and continue sending data?";
                            }
                        }
                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                                ok = false;
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                            //hd.TrangThai = dmdkSelect.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.KHAI_BAO;
                            //hd.PassWord = password;
                            //hd.ID = dmdkSelect.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        #endregion
                    }
                }
                //LayPhanHoi(password);
                LaySoTiepNhanDT();
                btnXacNhan.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }
        private void downloadItemsSelect()
        {
            if (collectionSelected.Count == 0)
            {
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                bool ok = true;
                int k = 0;
                int itemOK = 0;

                this.Cursor = Cursors.WaitCursor;
                foreach (DinhMucDangKy dmSelect in collectionSelected)
                {
                    DinhMucDangKy dmdkSelect = DinhMucDangKy.Load(dmSelect.ID);
                    dmdkSelect.LoadDMCollection();
                    try
                    {
                        k++;
                        itemOK++;
                        {
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "DM";
                            sendXML.master_id = dmdkSelect.ID;
                            if (!sendXML.Load())
                            {
                                xmlCurrent = dmdkSelect.WSRequestXML(password);
                                sendXML.msg = xmlCurrent;
                            }
                            sendXML.func = 2;
                            xmlCurrent = "";
                            sendXML.InsertUpdate();
                        }
                    }
                    catch (Exception ex)
                    {
                        #region lỗi

                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            if (k == collectionSelected.Count)
                            {
                                message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?";
                            }
                            else
                            {
                                message = "Danh sách thứ " + k.ToString() + " không nhận thông tin về được?";
                            }
                        }
                        else
                        {
                            if (k == collectionSelected.Count)
                            {
                                message = "Mailing list " + k.ToString() + "Don't receive information?\nDo you want to push queue ?";
                            }
                            else
                            {
                                message = "Mailing list " + k.ToString() + "Don't receive information?\nDo you want to push queue ? \n Do you want to continue sending data?";
                            }
                        }
                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                                ok = false;
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                            //hd.TrangThai = dmdkSelect.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //hd.PassWord = password;
                            //hd.ID = dmdkSelect.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        #endregion
                    }
                }
                LayPhanHoi(password);
                btnXacNhan.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận trạng thái danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cancelItemsSelect()
        {
            if (collectionSelected.Count == 0)
            {
                //ShowMessage("Bạn chưa chọn danh sách định mức.", false);
                //Message("MSG_DMC01", "", false);
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }


                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                bool ok = true;
                int k = 0;
                int itemOK = 0;
                foreach (DinhMucDangKy dmSelect in collectionSelected)
                {
                    DinhMucDangKy dmdkSelect = DinhMucDangKy.Load(dmSelect.ID);
                    dmdkSelect.LoadDMCollection();
                    {
                        try
                        {
                            k++;
                            itemOK++;
                            {
                                xmlCurrent = dmdkSelect.WSCancelXML(password);
                                MsgSend sendXML = new MsgSend();
                                sendXML.LoaiHS = "DM";
                                sendXML.master_id = dmdkSelect.ID;
                                sendXML.msg = xmlCurrent;
                                sendXML.func = 3;
                                xmlCurrent = "";
                                sendXML.InsertUpdate();
                            }
                        }
                        catch (Exception ex)
                        {
                            itemOK--;
                            this.Cursor = Cursors.Default;
                            ok = false;
                            string message = "";
                            if (k == collectionSelected.Count)
                            {
                                message = "Danh sách định mức có số tiếp nhận " + dmdkSelect.SoTiepNhan.ToString() + " không hủy khai báo được?";
                            }
                            else
                            {
                                message = "Danh sách định mức có số tiếp nhận " + dmdkSelect.SoTiepNhan.ToString() + " hủy khai báo được ?";
                            }
                            string st = ShowMessage(message, true);
                            if (st != "Yes")
                                break;
                            else
                            {
                                if (k < collectionSelected.Count)
                                    ok = true;
                                else
                                    ok = false;
                                //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                                //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                                //hd.TrangThai = dmdkSelect.TrangThaiXuLy;
                                //hd.ChucNang = ChucNang.HUY_KHAI_BAO;
                                //hd.PassWord = password;
                                //hd.ID = dmdkSelect.ID;
                                //MainForm.AddToQueueForm(hd);
                                //MainForm.ShowQueueForm();
                            }
                            StreamWriter write = File.AppendText("Error.txt");
                            write.WriteLine("--------------------------------");
                            write.WriteLine("Lỗi khi hủy khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                            write.WriteLine(ex.StackTrace);
                            write.WriteLine("Lỗi là : ");
                            write.WriteLine(ex.Message);
                            write.WriteLine("--------------------------------");
                            write.Flush();
                            write.Close();


                        }

                    }
                }
                LayPhanHoi(password);
                btnXacNhan.Enabled = true;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy dữ liệu danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoi(string pass)
        {
            if (collectionSelected.Count == 0)
            {
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            try
            {
                bool ok = false;
                int k = 0;
                int itemOK = 0;
                this.Cursor = Cursors.WaitCursor;
                foreach (DinhMucDangKy dmSelect in collectionSelected)
                {
                    DinhMucDangKy dmdkSelect = DinhMucDangKy.Load(dmSelect.ID);
                    dmdkSelect.LoadDMCollection();
                    try
                    {
                        k++;
                        itemOK++;
                        {
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = "DM";
                            sendXML.master_id = dmdkSelect.ID;
                            string st = "";
                            if (sendXML.Load())
                            {
                                st = dmdkSelect.LayPhanHoi(pass, sendXML.msg);
                                if (st != "")
                                {
                                    --itemOK;
                                    ok = false;
                                }
                                else
                                    ok = true;
                                //if (st != "")
                                //    --itemOK;
                                //else
                                //    sendXML.Delete();
                            }
                            else
                                itemOK--;
                        }
                    }
                    catch (Exception ex)
                    {
                        #region lỗi

                        itemOK--;
                        this.Cursor = Cursors.Default;
                        ok = false;
                        string message = "";
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            if (k == collectionSelected.Count)
                                message = "Danh sách định mức thứ " + k.ToString() + " không nhận thông tin về được?";
                            else
                                message = "Danh sách định mức thứ " + k.ToString() + " không nhận thông tin về được?";
                        }
                        else
                        {
                            if (k == collectionSelected.Count)
                                message = "Norm list " + k.ToString() + "Don't receive information?\nDo you want to push queue ?";
                            else
                                message = "Norm list " + k.ToString() + "Don't receive information?\nDo you want to push queue ? \n Do you want to continue sending data?";
                        }
                        string st = ShowMessage(message, true);
                        if (st != "Yes")
                            break;
                        else
                        {
                            if (k < collectionSelected.Count)
                                ok = true;
                            else
                                ok = false;
                            //Company.BLL.KDT.HangDoi hd = new Company.BLL.KDT.HangDoi();
                            //hd.LoaiToKhai = LoaiToKhai.DINH_MUC;
                            //hd.TrangThai = dmdkSelect.TrangThaiXuLy;
                            //hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //hd.PassWord = pass;
                            //hd.ID = dmdkSelect.ID;
                            //MainForm.AddToQueueForm(hd);
                            //MainForm.ShowQueueForm();
                        }
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        #endregion
                    }
                }
                this.Cursor = Cursors.Default;
                if (ok)
                {
                    if (itemOK > 0)
                    {
                        ShowMessage("Nhận thành công " + itemOK.ToString() + " danh sách định mức.", false);
                        btnXacNhan.Enabled = false;
                    }
                    else
                        ShowMessage(setText("Chưa có phản hồi từ hải quan ! ", "Not confirmed form Customs"), false);
                }
                else
                {
                    if (itemOK > 0)
                    {
                        ShowMessage("Nhận thành công " + itemOK.ToString() + " danh sách định mức.", false);
                        btnXacNhan.Enabled = false;
                    }
                    else
                        ShowMessage(setText("Chưa có phản hồi từ hải quan ! ", "Not confirmed form Customs"), false);
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi lấy phản hồi danh sách định mức. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnNhan_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.LayPhanHoiV3();
            else
                LaySoTiepNhanDT();
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
                this.cancelV3();
            else
                cancelItemsSelect();
        }

        private void dgTK_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["NgayTiepNhan"].Text != "")
            {
                DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Text);
                if (dt.Year <= 1900)
                    e.Row.Cells["NgayTiepNhan"].Text = "";
            }
            switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
            {
                case -1:
                    //e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    }
                    else
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Not declared yet";

                    }
                    break;
                case 0:
                    //e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    }
                    else
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Wait for approval";

                    }
                    break;
                case 1:
                    {
                        //e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";    
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        }
                        else
                        {
                            e.Row.Cells["TrangThaiXuLy"].Text = "Approved";

                        }
                    }
                    break;
                case 2:
                    //e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    }
                    else
                    {
                        e.Row.Cells["TrangThaiXuLy"].Text = "Not Approved";

                    }
                    break;
            }
        }
        #region Send V3 Create by LANNT
        /// <summary>
        /// Kiểm tra tờ khai có lỗi hay không
        /// </summary>
        private void SendV3()
        {
            k = 0;
            //collectionSelected = new DinhMucDangKyCollection();
            //for (int i = 0; i < dgTK.RowCount; i++)
            //{
            //    GridEXRow row = dgTK.GetRow(i);
            //    if (bool.Parse(row.Cells["Select"].Value.ToString()))
            //        collectionSelected.Add((DinhMucDangKy)row.DataRow);
            //}
            if (collectionSelected.Count == 0)
            {
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }

            try
            {
                foreach (DinhMucDangKy dmSelect in collectionSelected)
                {
                    dmdk = DinhMucDangKy.Load(dmSelect.ID);
                    dmdk.LoadDMCollection();
                    try
                    {
                        // Tờ khai thứ k
                        k++;
                        {
                            MsgSend sendXML = new MsgSend();
                            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                            sendXML.master_id = dmdk.ID;

                            if (!sendXML.Load())
                            {
                                dmdk.GUIDSTR = Guid.NewGuid().ToString();
                                Company.KDT.SHARE.Components.SXXK_DinhMucSP dmSp = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dmdk, false);
                                //string msgSend = Company.KDT.SHARE.Components.Helpers.BuildSend(
                                //               new Company.KDT.SHARE.Components.NameBase()
                                //               {
                                //                   Name = GlobalSettings.TEN_DON_VI,
                                //                   Identity = dmdk.MaDoanhNghiep
                                //               }
                                //                 , new Company.KDT.SHARE.Components.NameBase()
                                //                 {
                                //                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdk.MaHaiQuan),
                                //                     Identity = dmdk.MaHaiQuan
                                //                 }
                                //              ,
                                //                new Company.KDT.SHARE.Components.SubjectBase()
                                //                {
                                //                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINHMUC_SP,
                                //                    Function = Company.KDT.SHARE.Components.DeclarationFunction.KHAI_BAO,
                                //                    Reference = dmdk.GUIDSTR,
                                //                }
                                //                ,
                                //                dmSp, null, true);
                                ObjectSend msgSend = new ObjectSend(
                                     new Company.KDT.SHARE.Components.NameBase()
                                               {
                                                   Name = GlobalSettings.TEN_DON_VI,
                                                   Identity = dmdk.MaDoanhNghiep
                                               }
                                                 , new Company.KDT.SHARE.Components.NameBase()
                                                 {
                                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdk.MaHaiQuan),
                                                     Identity = dmdk.MaHaiQuan
                                                 }
                                              ,
                                                new SubjectBase()
                                                {
                                                    Type = DeclarationIssuer.SXXK_SP,
                                                    Function = DeclarationFunction.KHAI_BAO,
                                                    Reference = dmdk.GUIDSTR,
                                                }
                                                ,
                                                dmSp
                                    );
                                dmdk.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                SendMessageForm sendForm = new SendMessageForm();
                                sendForm.Send += SendMessage;
                                bool isSend = sendForm.DoSend(msgSend);
                                if (isSend)
                                {
                                    sendForm.Message.XmlSaveMessage(dmdk.ID, MessageTitle.KhaiBaoDinhMuc);
                                    //sendXML.msg = msgSend;
                                    sendXML.func = 1;
                                    sendXML.InsertUpdate();
                                    dmdk.TransferDataToSXXK();
                                    FeedBackV3();
                                }
                                else if (!string.IsNullOrEmpty(msgInfor))
                                    ShowMessageTQDT(msgInfor, false);
                            }
                            else
                            {
                                btnNhan.Enabled = true;
                                ShowMessage("Dòng định mức thứ " + k.ToString() + " đã được gửi đến hải quan. Bấm [Nhận Dữ Liệu] để lấy thông tin phản hồi từ hải quan", false);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new SendEventArgs(ex));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi \r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            {
                feedbackContent = SingleMessage.DinhMucSendHandler(dmdk, ref msgInfor, e);
            }

        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = dmdk.GUIDSTR;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SXXK_DINH_MUC,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = dmdk.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dmdk.MaHaiQuan.Trim()),
                                                  Identity = dmdk.MaHaiQuan
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (dmdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dmdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;

                }

            }
        }
        private void LayPhanHoiV3()
        {
            k = 0;
            foreach (DinhMucDangKy dmSelect in collectionSelected)
            {
                k++;
                dmdk = DinhMucDangKy.Load(dmSelect.ID);
                dmdk.LoadDMCollection();
                FeedBackV3();
            }
        }
        private void cancelV3()
        {
            k = 0;
            if (collectionSelected.Count == 0)
            {
                MLMessages("Bạn chưa chọn danh sách định mức.", "MSG_DMC01", "", false);
                return;
            }
            MsgSend sendXML = new MsgSend();
            foreach (DinhMucDangKy dmSelect in collectionSelected)
            {
                k++;
                dmdk = DinhMucDangKy.Load(dmSelect.ID);
                if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    dmdk.LoadDMCollection();
                    sendXML.LoaiHS = LoaiKhaiBao.NguyenPhuLieu;
                    sendXML.master_id = dmdk.ID;
                    DeclarationBase sp = Company.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(DeclarationIssuer.SXXK_DINH_MUC, dmdk.GUIDSTR, dmdk.SoTiepNhan, dmdk.MaHaiQuan, dmdk.NgayTiepNhan);
                    ObjectSend msgSend = new ObjectSend(
                                   new NameBase()
                                   {
                                       Name = GlobalSettings.TEN_DON_VI,
                                       Identity = dmdk.MaDoanhNghiep
                                   }
                                     , new NameBase()
                                     {
                                         Name = GlobalSettings.TEN_HAI_QUAN,
                                         Identity = dmdk.MaHaiQuan
                                     }
                                  ,
                                    new SubjectBase()
                                    {
                                        Type = DeclarationIssuer.SXXK_DINH_MUC,
                                        Function = DeclarationFunction.HUY,
                                        Reference = dmdk.GUIDSTR,
                                    }
                                    ,
                                    sp);
                    dmdk.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend)
                    {
                        dmdk.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                        sendForm.Message.XmlSaveMessage(dmdk.ID, MessageTitle.KhaiBaoHuyNguyenPhuLieu);
                        sendXML.func = 3;
                        //sendXML.msg = msgSend;
                        sendXML.InsertUpdate();
                        FeedBackV3();
                    }
                }
            }
        }
        #endregion
    }
}
