﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using Company.Interface.GC;
using NPLCungUng = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng;
using NPLCungUngDetail = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail;
using Company.Interface.KDT.GC;
namespace Company.Interface
{
    public partial class NPLCungUngFormGCCT : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;
        private NPLCungUng nplCungUng = new NPLCungUng();
        bool isEdit = false;
        public NPLCungUngFormGCCT()
        {
            InitializeComponent();
        }
        private void KhoiTaoDuLieuChuan()
        {
            
        }
        
       
        private void NPLCungUngForm_Load(object sender, EventArgs e)
        {
            KhoiTaoDuLieuChuan();
            dgList.DataSource = TKCT.NPLCungUngs;
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["DVT_ID"].Value != null)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);

        }

        

       

        private void btnXoa_Click(object sender, EventArgs e)
        {

            try
            {
                GridEXRow r = dgList.GetRow();
                if (r.DataRow == null) return;
                else nplCungUng = (NPLCungUng)r.DataRow;
                if (ShowMessage("Bạn có muốn xóa dòng hàng này không?", true) != "Yes") return;
                if (nplCungUng.ID > 0)
                {
                    nplCungUng.Delete();
                    NPLCungUngDetail.DeleteCollection(nplCungUng.NPLCungUngDetails);
                }
                TKCT.NPLCungUngs.Remove(nplCungUng);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        

        private void btnChiTiet_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null)
            {
                ShowMessage("Vui lòng chọn hàng", false);
                return;
            }
            NPLCungUng nplcu = (NPLCungUng)dgList.GetRow().DataRow;
            NPLCungUngDetailFormGCCT f = new NPLCungUngDetailFormGCCT();
            f.TKCT = this.TKCT;
            f.nplCungUng = nplcu;
            f.ShowDialog();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            NPLCungUng nplcu = (NPLCungUng)e.Row.DataRow;
            NPLCungUngDetailFormGCCT f = new NPLCungUngDetailFormGCCT();
            f.TKCT = this.TKCT;
            f.nplCungUng = nplcu;
            f.ShowDialog();

        }
        private void btnCaculate_Click(object sender, EventArgs e)
        {
            NPLCungUngTheoTKForm f = new NPLCungUngTheoTKForm();
            f.TKCT = TKCT;
            f.ShowDialog(this);
            TKCT.LoadChungTuKem();
            dgList.DataSource = TKCT.NPLCungUngs;

        }
    }
}
