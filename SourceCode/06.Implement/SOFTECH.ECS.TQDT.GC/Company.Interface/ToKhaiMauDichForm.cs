﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using Company.Interface.KDT.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Company.GC.BLL.KDT.SXXK;
using System.Net.Mail;
using System.Data;
using Company.Interface.GC;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;
using GemBox.Spreadsheet;
using Infragistics.Excel;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Text.RegularExpressions;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Utils;
namespace Company.Interface
{
    public partial class ToKhaiMauDichForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        private string xmlCurrent = "";
        public Company.GC.BLL.KDT.GC.HopDong Hdgc = new Company.GC.BLL.KDT.GC.HopDong();
        public string NhomLoaiHinh = string.Empty;
        private string _xmlCurrent = "";
        public long PtkmdId;
        public bool BNew = true;
        public static string tenPTVT = "";
        private static int i;
        public bool luu = false;
        public bool isEdited = false;
        public static int soDongHang;
        public bool isDeny = false;

        #region Biến phục vụ cho việc In tờ khai sửa đổi bổ sung
        //BEGIN: DATLMQ bổ sung các biến dùng để lưu giá trị TK cũ 16/02/2011
        private string nguoiNhapKhau = string.Empty;
        private string nguoiXuatKhau = string.Empty;
        private string nguoiUyThac = string.Empty;
        private string PTVT = string.Empty;
        private string soHieuPTVT = string.Empty;
        private DateTime ngayDenPTVT = new DateTime(1900, 1, 1);
        private string nuocXuatKhau = string.Empty;
        private string nuocNhapKhau = string.Empty;
        private string dieuKienGiaoHang = string.Empty;
        private string phuongThucThanhToan = string.Empty;
        private string soGiayPhep = string.Empty;
        private DateTime ngayGiayPhep = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanGiayPhep = new DateTime(1900, 1, 1);
        private string hoaDonTM = string.Empty;
        private DateTime ngayHoaDonTM = new DateTime(1900, 1, 1);
        private string diaDiemDoHang = string.Empty;
        private string nguyenTe = string.Empty;
        private decimal tyGiaTinhThue = 0;
        private decimal tyGiaUSD = 0;
        private string soHopDong = string.Empty;
        private DateTime ngayHopDong = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanHopDong = new DateTime(1900, 1, 1);
        private string soVanDon = string.Empty;
        private DateTime ngayVanDon = new DateTime(1900, 1, 1);
        private string diaDiemXepHang = string.Empty;
        private decimal soContainer20;
        private decimal soContainer40;
        private decimal soKienHang;
        private decimal trongLuong;
        private double trongLuongNet;
        private decimal lePhiHaiQuan;
        private decimal phiBaoHiem;
        private decimal phiVanChuyen;
        private decimal phiKhac;
        //END

        //DATLMQ bổ sung biến phục vụ việc tự động lưu nội dung sửa đổi bổ sung 04/03/2011
        //public static List<Company.KD.BLL.KDT.HangMauDich> ListHangMauDichEdit = new List<Company.KD.BLL.KDT.HangMauDich>();
        //private static Company.KD.BLL.KDT.HangMauDich hangMDTK = new Company.KD.BLL.KDT.HangMauDich();
        public static string maHS_Edit = string.Empty;
        public static string tenHang_Edit = string.Empty;
        public static string maHang_Edit = string.Empty;
        public static string xuatXu_Edit = string.Empty;
        public static decimal soLuong_Edit;
        public static string dvt_Edit = string.Empty;
        public static decimal dongiaNT_Edit;
        public static decimal trigiaNT_Edit;
        //END
        #endregion

        //public NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();

        public ToKhaiMauDichForm()
        {
            InitializeComponent();

            CreateCommandBosung();

            SetEvent_TextBox_GiayPhep();
            SetEvent_TextBox_HoaDon();
        }
        private void SetEvent_TextBox_GiayPhep()
        {
            txtSoGiayPhep.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtSoGiayPhep.ButtonClick += new EventHandler(txtSoGiayPhep_ButtonClick2);
        }
        private void SetEvent_TextBox_HoaDon()
        {
            txtSoHoaDonThuongMai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtSoHoaDonThuongMai.ButtonClick += new EventHandler(txtSoHoaDonThuongMai_ButtonClick2);
        }
        private void txtSoGiayPhep_ButtonClick2(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDForm f = new ListGiayPhepTKMDForm();
            f.isKhaiBoSung = false;
            f.IsBrowseForm = true;
            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.GiayPhepSelected.SoGiayPhep != "")
            {
                if (f.TKMD.SoGiayPhep != f.GiayPhepSelected.SoGiayPhep)
                {
                    if (Globals.ShowMessage("Bạn có muốn cập nhật lại thông tin 'Số Giấy phép' không?.", true) == "Yes")
                    {
                        txtSoGiayPhep.Text = f.GiayPhepSelected.SoGiayPhep;
                        ccNgayGiayPhep.Text = f.GiayPhepSelected.NgayGiayPhep.ToShortDateString();
                        ccNgayHHGiayPhep.Text = f.GiayPhepSelected.NgayHetHan.ToShortDateString();
                    }
                }
            }
        }
        private void txtSoHoaDonThuongMai_ButtonClick2(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonThuongMaiTKMDForm f = new ListHoaDonThuongMaiTKMDForm();
            f.isKhaiBoSung = false;
            f.IsBrowseForm = true;
            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.HoaDonThuongMaiSelected.SoHoaDon != "")
            {
                if (f.TKMD.SoHoaDonThuongMai != f.HoaDonThuongMaiSelected.SoHoaDon)
                {
                    if (Globals.ShowMessage("Bạn có muốn cập nhật lại thông tin 'Số Hóa đơn' không?.", true) == "Yes")
                    {
                        txtSoHoaDonThuongMai.Text = f.HoaDonThuongMaiSelected.SoHoaDon;
                        ccNgayHDTM.Text = f.HoaDonThuongMaiSelected.NgayHoaDon.ToShortDateString();
                    }
                }
            }


        }

        #region TẠO COMMAND TOOLBAR

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVanDonBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdGiayPhepBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHopDongBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHoaDonThuongMaiBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCOBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenCuaKhauBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuDangAnhBS1;

        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung()
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdChungTuBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuBoSung");
            this.cmdVanDonBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDonBoSung");
            this.cmdGiayPhepBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhepBoSung");
            this.cmdHopDongBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongBoSung");
            this.cmdHoaDonThuongMaiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMaiBoSung");
            this.cmdCOBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCOBoSung");
            this.cmdChuyenCuaKhauBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhauBoSung");
            this.cmdChungTuDangAnhBS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnhBS");

            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                this.cmdChungTuBoSung1
            });

            if (!this.cmMain.Commands.Contains("cmdChungTuBoSung"))
                this.cmMain.Commands.Add(this.cmdChungTuBoSung1);
            if (!this.cmMain.Commands.Contains("cmdGiayPhepBoSung"))
                this.cmMain.Commands.Add(this.cmdGiayPhepBoSung1);
            if (!this.cmMain.Commands.Contains("cmdHopDongBoSung"))
                this.cmMain.Commands.Add(this.cmdHopDongBoSung1);
            if (!this.cmMain.Commands.Contains("cmdHoaDonThuongMaiBoSung"))
                this.cmMain.Commands.Add(this.cmdHoaDonThuongMaiBoSung1);
            if (!this.cmMain.Commands.Contains("cmdCOBoSung"))
                this.cmMain.Commands.Add(this.cmdCOBoSung1);
            if (!this.cmMain.Commands.Contains("cmdChuyenCuaKhauBoSung"))
                this.cmMain.Commands.Add(this.cmdChuyenCuaKhauBoSung1);
            if (!this.cmMain.Commands.Contains("cmdChungTuDangAnhBS"))
                this.cmMain.Commands.Add(this.cmdChungTuDangAnhBS1);

            //this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdChungTuBoSung1,                
            //    this.cmdGiayPhepBoSung1,
            //    this.cmdHopDongBoSung1,
            //    this.cmdHoaDonThuongMaiBoSung1,
            //    this.cmdCOBoSung1,
            //    this.cmdChuyenCuaKhauBoSung1,
            //    this.cmdChungTuDangAnhBS1
            //});

            // 
            // cmdChungTuBoSung
            // 
            this.cmdChungTuBoSung1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGiayPhepBoSung1,
            this.cmdHopDongBoSung1,
            this.cmdHoaDonThuongMaiBoSung1,
            this.cmdCOBoSung1,
            this.cmdChuyenCuaKhauBoSung1,
            this.cmdChungTuDangAnhBS1});

            this.cmdChungTuBoSung1.Key = "cmdChungTuBoSung";
            this.cmdChungTuBoSung1.Name = "cmdChungTuBoSung1";
            this.cmdChungTuBoSung1.Text = "Chứng từ bổ sung";
            this.cmdChungTuBoSung1.ImageIndex = 4;
            // 
            // cmdHopDongBoSung1
            // 
            this.cmdHopDongBoSung1.Key = "cmdHopDongBoSung";
            this.cmdHopDongBoSung1.Name = "cmdHopDongBoSung1";
            this.cmdHopDongBoSung1.Text = "Hợp đồng ";
            this.cmdHopDongBoSung1.ImageIndex = 0;
            // 
            // cmdVanDonBoSung1
            // 
            this.cmdVanDonBoSung1.Key = "cmdVanDonBoSung";
            this.cmdVanDonBoSung1.Name = "cmdVanDonBoSung1";
            this.cmdVanDonBoSung1.Text = "Vận đơn ";
            this.cmdVanDonBoSung1.ImageIndex = 0;
            // 
            // CO1
            // 
            this.cmdCOBoSung1.Key = "cmdCOBoSung";
            this.cmdCOBoSung1.Name = "cmdCOBoSung1";
            this.cmdCOBoSung1.Text = "CO";
            this.cmdCOBoSung1.ImageIndex = 0;
            // 
            // cmdChuyenCuaKhauBoSung1
            // 
            this.cmdChuyenCuaKhauBoSung1.Key = "cmdChuyenCuaKhauBoSung";
            this.cmdChuyenCuaKhauBoSung1.Name = "cmdChuyenCuaKhauBoSung1";
            this.cmdChuyenCuaKhauBoSung1.Text = "Đề nghị chuyển cửa khẩu";
            this.cmdChuyenCuaKhauBoSung1.ImageIndex = 0;
            // 
            // cmdGiayPhepBoSung1
            // 
            this.cmdGiayPhepBoSung1.Key = "cmdGiayPhepBoSung";
            this.cmdGiayPhepBoSung1.Name = "cmdGiayPhepBoSung1";
            this.cmdGiayPhepBoSung1.Text = "Giấy phép";
            this.cmdGiayPhepBoSung1.ImageIndex = 0;
            // 
            // cmdHoaDonThuongMaiBoSung1
            // 
            this.cmdHoaDonThuongMaiBoSung1.Key = "cmdHoaDonThuongMaiBoSung";
            this.cmdHoaDonThuongMaiBoSung1.Name = "cmdHoaDonThuongMaiBoSung1";
            this.cmdHoaDonThuongMaiBoSung1.Text = "Hóa đơn thương mại";
            this.cmdHoaDonThuongMaiBoSung1.ImageIndex = 0;
            // 
            // cmdChungTuDangAnhBS1
            // 
            this.cmdChungTuDangAnhBS1.Key = "cmdChungTuDangAnhBS";
            this.cmdChungTuDangAnhBS1.Name = "cmdChungTuDangAnhBS1";
            this.cmdChungTuDangAnhBS1.Text = "Chứng từ dạng ảnh";
            this.cmdChungTuDangAnhBS1.ImageIndex = 0;

            #endregion

            cmdChungTuBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);

        }

        private void cmdChungTuBoSung1_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdVanDonBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoVanTaiDon_ButtonClick("1", null);
                        break;
                    case "cmdHoaDonThuongMaiBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                        break;
                    case "CO":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.QuanLyCo("1");
                        break;
                    case "cmdHopDongBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoHopDong_ButtonClick("1", null);
                        break;
                    case "cmdGiayPhepBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoGiayPhep_ButtonClick("1", null);
                        break;
                    case "cmdChuyenCuaKhauBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.DeNghiChuyenCuaKhau("1");
                        break;

                    case "cmdChungTuDangAnhBS":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.AddChungTuAnh(true);
                        break;
                }
            }
            catch (Exception ex) { }
        }

        #region Validate Khai bo sung

        /// <summary>
        /// Kiem tra thong tin truoc khi khai bo sung chung tu.
        /// </summary>
        /// <param name="soToKhai"></param>
        /// <returns></returns>
        /// HUNGTQ, Update 07/06/2010.
        private bool ValidateKhaiBoSung(int soToKhai)
        {
            if (soToKhai == 0)
            {
                string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                Globals.ShowMessageTQDT(msg, false);

                return false;
            }

            return true;
        }

        #endregion

        #region Validate ToKhaiMauDich
        /// <summary>
        /// Kiem tra do dai cua truong nguoiNK
        /// </summary>
        /// DATLMQ, Update 30072010
        ErrorProvider err = new ErrorProvider();

        private bool ValidateNguoiNK()
        {
            bool isValid = false;
            try
            {
                if (NhomLoaiHinh.Substring(0, 1) == "N")
                    isValid = Globals.ValidateLength(txtTenDonViDoiTac, 130, err, "Người xuất khẩu");
                else
                    isValid = Globals.ValidateLength(txtTenDonViDoiTac, 130, err, "Người nhập khẩu");
            }
            catch (Exception ex) { }
            return isValid;
        }
        #endregion

        #endregion

        private void TinhLaiThue()
        {
            TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            TKMD.TongTriGiaKhaiBao = 0;
            decimal phi = Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            decimal tongTriGiaHang = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
            {
                tongTriGiaHang += hmd.TriGiaKB;
                TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
            }
            if (tongTriGiaHang == 0) return;
            decimal triGiaTtMotDong = (1 + phi / tongTriGiaHang) * Convert.ToDecimal(TKMD.TyGiaTinhThue);
            TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
            {
                hmd.DonGiaTT = triGiaTtMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = Math.Round(hmd.DonGiaTT * (hmd.SoLuong), MidpointRounding.AwayFromZero);
                TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                hmd.TriGiaThuKhac = Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = TKMD.HMDCollection;
            dgList.Refetch();
        }

        private void TinhTongTriGiaKhaiBao()
        {
            TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            TKMD.TongTriGiaKhaiBao = 0;
            TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
            {
                TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
                TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
            }
        }
        private bool KiemTraLuongTon()
        {
            if (radSP.Checked)
            {
                DataSet dsLuongTon = NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.IDHopDong);
                foreach (Company.GC.BLL.KDT.HangMauDich hangMd in TKMD.HMDCollection)
                {
                    if (hangMd.ID > 0)
                    {
                        DataTable dtLuongNplTheoDm = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaPhu, hangMd.SoLuong, TKMD.IDHopDong).Tables[0];
                        foreach (DataRow row in dtLuongNplTheoDm.Rows)
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                        }
                    }
                }
                TKMD.HMDCollection.Sort(new SortHangMauDich());
                string tenHangBiAm = "";
                foreach (Company.GC.BLL.KDT.HangMauDich hangMd in TKMD.HMDCollection)
                {
                    DataSet dsLuongNplTheoDm = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaPhu, hangMd.SoLuong, TKMD.IDHopDong);
                    foreach (DataRow rowNplTon in dsLuongTon.Tables[0].Rows)
                    {
                        bool ok = false;
                        foreach (DataRow rowNplTheoDm in dsLuongNplTheoDm.Tables[0].Rows)
                        {
                            if (rowNplTon["MaNguyenPhuLieu"].ToString().Trim() == rowNplTheoDm["MaNguyenPhuLieu"].ToString().Trim())
                            {
                                rowNplTon["LuongTon"] = Convert.ToDecimal(rowNplTon["LuongTon"]) - Convert.ToDecimal(rowNplTheoDm["LuongCanDung"]);
                                if (Convert.ToDecimal(rowNplTon["LuongTon"]) < 0)
                                {
                                    if (tenHangBiAm.IndexOf(hangMd.TenHang) < 0)
                                    {
                                        if (tenHangBiAm == "")
                                            tenHangBiAm += hangMd.TenHang;
                                        else
                                            tenHangBiAm += " ," + hangMd.TenHang;
                                    }
                                    ok = true;
                                    break;
                                }
                            }
                        }
                        if (ok)
                            break;
                    }
                }
                if (tenHangBiAm != "")
                {
                    //if (MLMessages("Nguyên phụ liệu tồn trong hệ thống không đủ để sản xuất các hàng sau : " + TenHangBiAm + ". Bạn có muốn lưu không?", "MSG_WRN01", TenHangBiAm, true) != "Yes")

                    //if (showMsg("MSG_WRN01", tenHangBiAm, true) != "Yes")
                    if (Globals.ShowMessageTQDT("Nguyên phụ liệu tồn trong hệ thống không đủ để sản xuất các hàng sau :\r\n " + tenHangBiAm + "\r\n \nBạn có muốn lưu không?", true) != "Yes")
                    {
                        return false;
                    }
                    return true;
                }
            }
            return true;

        }
        private void LoadTKMDData()
        {
            txtSoLuongPLTK.Value = TKMD.SoLuongPLTK;

            ccNgayDangKy.Text = TKMD.NgayDangKy.Year > 1900 ? TKMD.NgayDangKy.ToShortDateString() : "";

            // Doanh nghiệp.
            txtMaDonVi.Text = TKMD.MaDoanhNghiep;
            txtTenDonVi.Text = TKMD.TenDoanhNghiep;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = TKMD.TenDonViDoiTac;

            // Đại lý TTHQ.
            txtMaDaiLy.Text = TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = TKMD.TenDaiLyTTHQ;

            //Don vi uy thac
            txtMaDonViUyThac.Text = TKMD.MaDonViUT;
            txtTenDonViUyThac.Text = TKMD.MaDonViUT;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = TKMD.MaLoaiHinh;

            // Phương tiện vận tải.
            cbPTVT.SelectedValue = TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = TKMD.SoHieuPTVT;
            if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                ccNgayDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
            else
                ccNgayDen.Text = "";
            // Nước.
            ctrNuocXuatKhau.Ma = TKMD.MaLoaiHinh.Substring(0, 1) == "N" ? TKMD.NuocXK_ID : TKMD.NuocNK_ID;

            // ĐKGH.
            cbDKGH.SelectedValue = TKMD.DKGH_ID;

            // PTTT.
            cbPTTT.SelectedValue = TKMD.PTTT_ID;

            // Giấy phép.
            txtSoGiayPhep.Text = TKMD.SoGiayPhep;
            ccNgayGiayPhep.Text = TKMD.NgayGiayPhep.Year > 1900 ? TKMD.NgayGiayPhep.ToShortDateString() : "";
            ccNgayHHGiayPhep.Text = TKMD.NgayHetHanGiayPhep.Year > 1900 ? TKMD.NgayHetHanGiayPhep.ToShortDateString() : "";
            //if ( TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
            ccNgayHDTM.Text = TKMD.NgayHoaDonThuongMai.Year > 1900 ? TKMD.NgayHoaDonThuongMai.ToShortDateString() : "";
            //if ( TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = TKMD.CuaKhau_ID;

            // Nguyên tệ.
            ctrNguyenTe.Ma = TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = TKMD.TyGiaUSD;

            // Hợp đồng.
            txtSoHopDong.Text = TKMD.SoHopDong;
            ccNgayHopDong.Text = TKMD.NgayHopDong.Year > 1900 ? TKMD.NgayHopDong.ToShortDateString() : "";
            ccNgayHHHopDong.Text = TKMD.NgayHetHanHopDong.Year > 1900 ? TKMD.NgayHetHanHopDong.ToShortDateString() : "";
            //if ( TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

            // Vận tải đơn.
            txtSoVanTaiDon.Text = TKMD.SoVanDon;
            if (TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Value = TKMD.NgayVanDon;
            else ccNgayVanTaiDon.Text = "";
            // if ( TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;
            //if ( TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

            // Tên chủ hàng.
            txtTenChuHang.Text = TKMD.TenChuHang;

            // Container 20.
            txtSoContainer20.Value = TKMD.SoContainer20;

            // Container 40.
            txtSoContainer40.Value = TKMD.SoContainer40;

            // Số kiện hàng.
            txtSoKien.Value = TKMD.SoKien;

            // Trọng lượng.
            txtTrongLuong.Value = TKMD.TrongLuong;

            // Lệ phí HQ.
            txtLePhiHQ.Value = TKMD.LePhiHaiQuan;

            // Phí BH.
            txtPhiBaoHiem.Value = TKMD.PhiBaoHiem;

            // Phí VC.
            txtPhiVanChuyen.Value = TKMD.PhiVanChuyen;

            //Phí ngân hàng
            txtPhiNganHang.Value = TKMD.PhiKhac;

            txtTrongLuongTinh.Value = TKMD.TrongLuongNet;
            //MaMid
            txtMaMid.Text = TKMD.MaMid;

            if (TKMD.ID > 0)
            {
                if (TKMD.HMDCollection == null || TKMD.HMDCollection.Count == 0)
                    TKMD.LoadHMDCollection();
                if (TKMD.ChungTuTKCollection == null || TKMD.ChungTuTKCollection.Count == 0)
                    TKMD.LoadChungTuTKCollection();
                TKMD.LoadChungTuHaiQuan();
                if (TKMD.ListCO == null || TKMD.ListCO.Count == 0)
                    TKMD.LoadCO();

                Hdgc.ID = TKMD.IDHopDong;
                Hdgc.Load();
            }
            dgList.DataSource = TKMD.HMDCollection;
            dgList.Refetch();
            gridEX1.DataSource = TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            if (TKMD.LoaiHangHoa == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
            }
            else if (TKMD.LoaiHangHoa == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
            }
            else if (TKMD.LoaiHangHoa == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
            }
            radTB.CheckedChanged += radSP_CheckedChanged;
            radNPL.CheckedChanged += radSP_CheckedChanged;
            radSP.CheckedChanged += radSP_CheckedChanged;

            //chung tu kem
            txtChungTu.Text = TKMD.GiayTo;
            //so to khai 
            if (TKMD.SoToKhai > 0)
                txtSoToKhai.Text = TKMD.SoToKhai.ToString();

            if (TKMD.SoTiepNhan > 0)
                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();

            txtDeXuatKhac.Text = TKMD.DeXuatKhac;
            txtLyDoSua.Text = TKMD.LyDoSua;

            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = "Chờ duyệt";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Wait for approval"; }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                lblTrangThai.Text = "Chưa khai báo";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not declared yet"; }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                lblTrangThai.Text = "Không được phê duyệt";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not approved"; }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                lblTrangThai.Text = "Đã duyệt";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                if (TKMD.PhanLuong == "")
                {
                    lblTrangThai.Text = "Đã duyệt";
                    if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                }
                else
                {
                    //xanh
                    if (TKMD.PhanLuong == "1" || TKMD.PhanLuong == "3" || TKMD.PhanLuong == "5")
                    {
                        lblTrangThai.Text = "Tờ khai đã được phân luồng xanh";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Declaration sheet has been integrated into green group  "; }
                    }
                    else if (TKMD.PhanLuong == "2" || TKMD.PhanLuong == "4" || TKMD.PhanLuong == "6" || TKMD.PhanLuong == "10")
                    {
                        lblTrangThai.Text = "Tờ khai đã được phân luồng đỏ";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Declaration sheet has been integrated into red group  "; }
                    }
                    else if (TKMD.PhanLuong == "8" || TKMD.PhanLuong == "12")
                    {
                        lblTrangThai.Text = "Tờ khai đã được phân luồng vàng";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Declaration sheet has been integrated into yellow group  "; }
                    }
                }

            }
        }

        #region Khởi tạo dữ liệu trước khi hiển thị

        private void KhoitaoDuLieuChuan()
        {
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = NhomLoaiHinh + "01";

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll();
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll();
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll();
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            ctrCuaKhau.Ma = NhomLoaiHinh.StartsWith("N") ? GlobalSettings.DIA_DIEM_DO_HANG : GlobalSettings.CUA_KHAU;
        }

        private void KhoitaoGiaoDienToKhai()
        {
            txtMaMid.Text = GlobalSettings.MaMID;
            NhomLoaiHinh = NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = NhomLoaiHinh.Substring(0, 1);
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                Text = setText("Tờ khai nhập khẩu", "Import declaration") + "(" + NhomLoaiHinh + ")";
                uiGroupBox11.Visible = true;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = true;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = true;
                //chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = true;
                // chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = true;
                // chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = true;                
                txtMaMid.Visible = lblMaMid.Visible = false;
                CanDoiNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;
                PhanBo.Visible = Janus.Windows.UI.InheritableBoolean.False;
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                // Tiêu đề.
                Text = setText("Tờ khai xuất khẩu", "Export declaration") + "(" + NhomLoaiHinh + ")";
                uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                grbNguoiNK.Text = "Người xuất khẩu";
                grbNguoiXK.Text = "Người nhập khẩu";
                if (GlobalSettings.NGON_NGU == "1")
                {
                    grbNguoiNK.Text = "Exporter";
                    grbNguoiXK.Text = "Importer";
                }

                // Phương tiện vận tải.              
                //uiGroupBox11.Enabled = false;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                //chkHoaDonThuongMai.Visible =
                grbHoaDonThuongMai.Enabled = true;
                //chkVanTaiDon.Visible = 
                grbVanTaiDon.Enabled = false;

                // Vận tải đơn.


                // Nước XK.
                grbNuocXK.Text = "Nước nhập khẩu";

                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                if (GlobalSettings.NGON_NGU == "1")
                {
                    grbNuocXK.Text = "Import national";
                    grbDiaDiemDoHang.Text = "Export gate";
                }

                //chkDiaDiemXepHang.Enabled = 
                grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = 
                // chkDiaDiemXepHang.Visible = false;
                PhanBo.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }

            // Loại hình gia công.
            if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
            {
                //chkHopDong.Checked = true;
                //chkHopDong.Visible = false;
                txtSoHopDong.BackColor = ccNgayHopDong.BackColor = ccNgayHHHopDong.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
                txtSoHopDong.ButtonStyle = EditButtonStyle.Ellipsis;
                if (NhomLoaiHinh == "NGC")
                {
                    radNPL.Checked = true;
                    radSP.Visible = false;
                    radTB.Checked = false;
                }
            }
            //BO SUNG LOAI HINH TAM NHAP - TAI XUAT, 14/10/2011
            else if (this.NhomLoaiHinh.Contains("NTA01") || this.NhomLoaiHinh.Contains("NTA25"))
            {
                radSP.Visible = true;
            }
        }
        #endregion

        private DataTable GetTableCanDoi()
        {
            DataTable tableResult = new DataTable();
            tableResult.Columns.Add("MaSP");
            tableResult.Columns.Add("MaNPL");
            tableResult.Columns.Add("DinhMucChung", typeof(decimal));
            tableResult.Columns.Add("LuongTonDau", typeof(decimal));
            tableResult.Columns.Add("LuongSuDung", typeof(decimal));
            tableResult.Columns.Add("LuongTonCuoi", typeof(decimal));

            if (radSP.Checked)
            {
                DataSet dsLuongTon = NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.IDHopDong);
                foreach (Company.GC.BLL.KDT.HangMauDich hangMd in TKMD.HMDCollection)
                {
                    if (hangMd.ID > 0)
                    {
                        DataTable dtLuongNPLTheoDM = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaPhu, hangMd.SoLuong, TKMD.IDHopDong).Tables[0];
                        foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                        }
                    }
                }
                TKMD.HMDCollection.Sort(new SortHangMauDich());
                foreach (Company.GC.BLL.KDT.HangMauDich hangMD in TKMD.HMDCollection)
                {
                    DataTable dtLuongNPLTheoDM = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMD.MaPhu, hangMD.SoLuong, TKMD.IDHopDong).Tables[0];
                    foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                    {
                        try
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];
                            if (rowTon != null)
                            {
                                DataRow rowNew = tableResult.NewRow();
                                rowNew["MaSP"] = hangMD.MaPhu.Trim();
                                rowNew["MaNPL"] = row["MaNguyenPhuLieu"].ToString().Trim();
                                rowNew["DinhMucChung"] = row["dinhMucChung"];
                                rowNew["LuongTonDau"] = rowTon["LuongTon"];
                                rowNew["LuongSuDung"] = row["LuongCanDung"];
                                rowNew["LuongTonCuoi"] = Convert.ToDecimal(rowTon["LuongTon"]) - Convert.ToDecimal(row["LuongCanDung"]);
                                rowTon["LuongTon"] = rowNew["LuongTonCuoi"];
                                tableResult.Rows.Add(rowNew);
                            }
                        }
                        catch
                        {
                            //MLMessages("Nguyên phụ liệu có mã : " + row["MaNguyenPhuLieu"] + " không có trong hệ thống.Hãy cập nhật lại mã này.", "MSG_240247", row["MaNguyenPhuLieu"].ToString(), false);
                            //showMsg("MSG_240247", row["MaNguyenPhuLieu"].ToString());
                            Globals.ShowMessageTQDT(row["MaNguyenPhuLieu"].ToString(), false);
                            return null;
                        }
                    }
                }
            }
            return tableResult;

        }

        private void ToKhaiNhapKhauSxxkFormLoad(object sender, EventArgs e)
        {
            ctrLoaiHinhMauDich.ValueChanged += new Company.Interface.Controls.LoaiHinhMauDichVControl.ValueChangedEventHandler(ctrLoaiHinhMauDich_ValueChanged);

            if (this.NhomLoaiHinh.Substring(0, 1).Equals("N"))
            {
                cmdVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (this.NhomLoaiHinh.Substring(0, 1).Equals("X"))
            {
                cmdVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

            GlobalSettings.LOAI_HINH = TKMD.MaLoaiHinh;

            txtDeXuatKhac.Enabled = true;

            radTB.CheckedChanged -= radSP_CheckedChanged;
            radNPL.CheckedChanged -= radSP_CheckedChanged;
            radSP.CheckedChanged -= radSP_CheckedChanged;
            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
            ccNgayDen.Value = DateTime.Today;
            KhoitaoDuLieuChuan();
            KhoitaoGiaoDienToKhai();
            switch (this.OpenType)
            {
                case OpenFormType.View:
                    LoadTKMDData();
                    ViewTKMD();
                    this.NhomLoaiHinh = TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case OpenFormType.Edit:
                    LoadTKMDData();
                    this.NhomLoaiHinh = TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case OpenFormType.Insert:
                    txtTyGiaUSD.Text = NguyenTe.GetTyGia("USD").ToString();
                    break;
            }

            setCommandStatus();

            radTB.CheckedChanged += new System.EventHandler(radSP_CheckedChanged);
            radNPL.CheckedChanged += new System.EventHandler(radSP_CheckedChanged);
            radSP.CheckedChanged += new System.EventHandler(radSP_CheckedChanged);
            if (TKMD.ID > 0)
            {
                var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
                if (sendXml.Load())
                {
                    lblTrangThai.Text = "Đang chờ xác nhận hải quan";
                    if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Waiting for confirmation";
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }
            else
            {
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            if (GlobalSettings.TuDongTinhThue == "0")
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
            //{
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //    TopRebar1.Visible = false;
            //}    

            TinhTongTriGiaKhaiBao();
        }

        private void ctrLoaiHinhMauDich_ValueChanged(object sender, EventArgs e)
        {
            //BO SUNG LOAI HINH TAM NHAP - TAI XUAT, 14/10/2011
            if (this.ctrLoaiHinhMauDich.Ma == "NTA01" || this.ctrLoaiHinhMauDich.Ma == "NTA25")
            {
                radSP.Visible = true;
            }
        }

        private void CtrNguyenTeValueChanged(object sender, EventArgs e)
        {
            txtTyGiaTinhThue.Text = ctrNguyenTe.TyGia.ToString();
        }

        #region Xem tờ khai mậu dịch
        private void ViewTKMD()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            txtSoLuongPLTK.ReadOnly = true;
            cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;
            ccNgayDangKy.ReadOnly = true;
            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;

            // Container 20.
            txtSoContainer20.ReadOnly = true;

            // Container 40.
            txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }
        #endregion

        #region Đọc file Excell
        private void ReadExcel()
        {
            if (NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                bool isValid = cvError.IsValid;
            }
            else
            {
                cvError.ContainerToValidate = txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                bool b = valid1 && valid2;
            }
            if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
            {
                epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid tax assessment rate"));
                epError.SetIconPadding(txtTyGiaTinhThue, -8);
                return;

            }
            if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
            {
                if (txtSoHopDong.Text == "")
                {
                    epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "Contract no. must be filled"));
                    epError.SetIconPadding(txtSoHopDong, -8);
                    return;
                }
                epError.Clear();
            }
            if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) TKMD.LoaiHangHoa = "T";

            var reform = new ReadExcelForm
                             {
                                 TiGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value),
                                 TKMD = TKMD
                             };
            reform.ShowDialog();
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            setCommandStatus();
        }
        #endregion

        #region Thêm hàng
        private void ThemHang()
        {
            AddDataToToKhai();

            if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
            {
                if (txtSoHopDong.Text == "")
                {
                    epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "Contract no. must be filled"));
                    epError.SetIconPadding(txtSoHopDong, -8);
                    return;
                }
                epError.Clear();
            }
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) TKMD.LoaiHangHoa = "T";
            var f = new HangMauDichForm
                        {
                            OpenType = OpenFormType.Edit,
                            HD = Hdgc,
                            TKMD = TKMD,
                            MaHaiQuan = Hdgc.MaHaiQuan,
                            TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value)
                        };
            TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            f.ShowDialog();
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        #endregion

        #region Đưa dữ liệu vào tờ khai
        private void AddDataToToKhai()
        {
            //if ( NhomLoaiHinh.Substring(0, 1) == "N")
            //{
            //    cvError.ContainerToValidate = this;
            //    cvError.Validate();
            //    valid = cvError.IsValid;
            //}
            //else
            //{
            //    cvError.ContainerToValidate =  txtTenDonViDoiTac;
            //    cvError.Validate();
            //    bool valid1 = cvError.IsValid;
            //    cvError.ContainerToValidate =  grbNguyenTe;
            //    cvError.Validate();
            //    bool valid2 = cvError.IsValid;
            //    valid = valid1 && valid2;
            //}
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
            {
                epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid tax assessment rate"));
                epError.SetIconPadding(txtTyGiaTinhThue, -8);
                return;

            }
            TKMD.SoTiepNhan = TKMD.SoTiepNhan;
            TKMD.MaHaiQuan = Hdgc.MaHaiQuan;
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
            }
            TKMD.SoHang = (short)TKMD.HMDCollection.Count;
            TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

            // Doanh nghiệp.
            TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

            // Đơn vị đối tác.
            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


            TKMD.MaDonViUT = txtMaDonViUyThac.Text;
            TKMD.TenDonViUT = txtTenDonViUyThac.Text;

            // Đại lý TTHQ.
            TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
            TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

            // Loại hình mậu dịch.
            TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

            // Giấy phép.
            if (txtSoGiayPhep.Text.Trim().Length > 0)
            {
                TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                TKMD.NgayGiayPhep = !ccNgayGiayPhep.IsNullDate ? ccNgayGiayPhep.Value : DateTime.Parse("01/01/1900");
                TKMD.NgayHetHanGiayPhep = !ccNgayHHGiayPhep.IsNullDate ? ccNgayHHGiayPhep.Value : DateTime.Parse("01/01/1900");
            }
            else
            {
                TKMD.SoGiayPhep = "";
                TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
            }

            // 7. Hợp đồng.
            if (txtSoHopDong.Text.Trim().Length > 0)
            {
                TKMD.SoHopDong = txtSoHopDong.Text;
                // Ngày HD.
                TKMD.NgayHopDong = ccNgayHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHopDong.Value;
                // Ngày hết hạn HD.
                TKMD.NgayHetHanHopDong = ccNgayHHHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHHHopDong.Value;
            }
            else
            {
                TKMD.SoHopDong = "";
                TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
            }

            // Hóa đơn thương mại.
            if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
            {
                TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                TKMD.NgayHoaDonThuongMai = ccNgayHDTM.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHDTM.Value;
            }
            else
            {
                TKMD.SoHoaDonThuongMai = "";
                TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
            }
            // Phương tiện vận tải.
            TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
            TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
            TKMD.NgayDenPTVT = ccNgayDen.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayDen.Value;

            // Vận tải đơn.
            if (txtSoVanTaiDon.Text.Trim().Length > 0)
            {
                TKMD.SoVanDon = txtSoVanTaiDon.Text;
                TKMD.NgayVanDon = ccNgayVanTaiDon.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayVanTaiDon.Value;
            }
            else
            {
                TKMD.SoVanDon = "";
                TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
            }
            // Nước.
            if (NhomLoaiHinh.Substring(0, 1) == "N")
            {
                TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                TKMD.NuocNK_ID = "VN".PadRight(3);
            }
            else
            {
                TKMD.NuocXK_ID = "VN".PadRight(3);
                TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
            }

            // Địa điểm xếp hàng.
            //if (chkDiaDiemXepHang.Checked)
            TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
            // else
            //     TKMD.DiaDiemXepHang = "";

            // Địa điểm dỡ hàng.
            TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

            // Điều kiện giao hàng.
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            // Đồng tiền thanh toán.
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

            // Phương thức thanh toán.
            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

            //
            TKMD.TenChuHang = txtTenChuHang.Text;
            TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
            TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
            TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
            TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

            // Tổng trị giá khai báo (Chưa tính).
            TKMD.TongTriGiaKhaiBao = 0;
            //
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                TKMD.TrangThaiXuLy = -1;
            }
            TKMD.LoaiToKhaiGiaCong = "NPL";
            //
            TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
            TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
            TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
            TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

            // tkmd.NgayGui = DateTime.Parse("01/01/1900");
            if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) TKMD.LoaiHangHoa = "T";

            TKMD.GiayTo = txtChungTu.Text;
            TKMD.MaMid = txtMaMid.Text.Trim();
        }
        #endregion

        #region Lưu thông tin tờ khai

        private Company.GC.BLL.KDT.HangMauDich FindHangMauDichBy(Company.GC.BLL.KDT.HangMauDichCollection list, long idTKMD, string maPhu)
        {
            foreach (Company.GC.BLL.KDT.HangMauDich item in list)
            {
                if (item.TKMD_ID == idTKMD && item.MaPhu == maPhu)
                {
                    return item;
                }
            }

            return null;
        }

        //-----------------------------------------------------------------------------------------
        //DATLMQ bổ sung 2 Collection phục vụ việc Thêm, Xóa hàng khi sửa TKMD ngày 12/08/2011
        public static Company.GC.BLL.KDT.HangMauDichCollection deleteHMDCollection = new Company.GC.BLL.KDT.HangMauDichCollection();
        public static Company.GC.BLL.KDT.HangMauDichCollection insertHMDCollection = new Company.GC.BLL.KDT.HangMauDichCollection();
        //-----------------------------------------------------------------------------------------
        private void AutoInsertHMDToTKEdit(string _maHS_Old, string _tenHang_Old, string _maHang_Old, string _xuatXu_Old, decimal _soLuong_Old, string _dvt_Old, decimal _donGiaNT_Old, decimal _triGiaNT_Old, ToKhaiMauDich toKhaiMauDich)
        {
            try
            {
                //Company.KD.BLL.KDT.HangMauDich HMD_Edit = new Company.KD.BLL.KDT.HangMauDich();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
                List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail noiDungEditDetail = new Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail();
                HangMauDichEditForm hmdEditForm = new HangMauDichEditForm();
                if (toKhaiMauDich.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    #region 21. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "21. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHS_Edit = _maHS_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHS_Edit = _maHS_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "20. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            tenHang_Edit = _tenHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                tenHang_Edit = _tenHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "20. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHang_Edit = _maHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHang_Edit = _maHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 22. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "22. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                            xuatXu_Edit = _xuatXu_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                                xuatXu_Edit = _xuatXu_Old;
                            }
                        }
                    }
                    #endregion

                    #region 23. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "23. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            soLuong_Edit = _soLuong_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                soLuong_Edit = _soLuong_Old;
                            }
                        }
                    }
                    #endregion

                    #region 24. Đơn vị tính
                    if (dvt_Edit != _dvt_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "24. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                            dvt_Edit = _dvt_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                                dvt_Edit = _dvt_Old;
                            }
                        }
                    }
                    #endregion

                    #region 25. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "25. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            dongiaNT_Edit = _donGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                dongiaNT_Edit = _donGiaNT_Old;
                            }
                        }
                    }
                    #endregion

                    #region 26. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "26. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            trigiaNT_Edit = _triGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                trigiaNT_Edit = _triGiaNT_Old;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 18. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "18. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "17. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "17. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 19. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "19. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "20. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 21. Đơn vị tính
                    if (dvt_Edit != _dvt_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "21. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 22. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "22. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 23. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "23. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion
                }
            }
            catch
            {
                ShowMessage("Có lỗi trong quá trình ghi dữ liệu vào tờ khai sửa đổi, bổ sung", false);
            }
        }

        private bool checkHMDEsistsInTKMD(long _TKMD_ID, string _maHang)
        {
            Company.GC.BLL.KDT.HangMauDich hmd = new Company.GC.BLL.KDT.HangMauDich();
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _maHang);
            HangMauDichCollection hmdColl = hmd.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }

        //DATLMQ bổ sung AutoInsertAddHMD ngày 12/08/2011
        private void AutoInsertAddHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            foreach (Company.GC.BLL.KDT.HangMauDich hangMD in TKMD.HMDCollection)
            {
                if (!checkHMDEsistsInTKMD(TKMD.ID, hangMD.MaPhu))
                {
                    insertHMDCollection.Add(hangMD);
                    listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                    if (listNoiDungTK.Count == 0)
                    {
                        noiDungTK.Ma = "MaHang";
                        noiDungTK.Ten = "20. Mã hàng";
                        noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                        noiDungTK.InsertUpdate();

                        foreach (Company.GC.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                    else
                    {
                        foreach (Company.GC.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                }
            }
        }

        //DATLMQ bổ sung AutoInsertDeleteHMD ngày 12/08/2011
        private void AutoInsertDeleteHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            if (HangMauDichForm.isDeleted)
            {
                listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                if (listNoiDungTK.Count == 0)
                {
                    noiDungTK.Ma = "MaHang";
                    noiDungTK.Ten = "20. Mã hàng";
                    noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungTK.InsertUpdate();

                    foreach (Company.GC.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin " + noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
                else
                {
                    foreach (Company.GC.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = "Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
            }
        }

        //-----------------------------------------------------------------------------------------       
        public void Save()
        {
            bool valid;
            bool isKhaibaoSua = TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ? true : false;
            //TKMD.TrangThaiXuLy = 5;
            //TKMD.Update();
            if (isKhaibaoSua)
            {
                if (txtLyDoSua.Text.Trim() == "")
                {
                    MLMessages("Chưa nhập lý do sửa tờ khai", "MSG_SAV06", "", false);
                    return;
                }

                #region OldCode
                //else if (NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID).Count == 0)
                //{
                //    if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //    {
                //        NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //        noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //        noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
                //        noiDungChinhSuaTKForm.ShowDialog();
                //    }
                //    return;
                //}
                //else
                //{
                //    int count = 0;
                //    NoiDungChinhSuaTKDetailForm ndDieuChinhTKChiTietForm = new NoiDungChinhSuaTKDetailForm();
                //    ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                //    foreach (NoiDungDieuChinhTK nddctk in ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK)
                //    {
                //        if (nddctk.TrangThai != 1)
                //            ++count;
                //    }
                //    if (count == 0)
                //    {
                //        if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //        {
                //            NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //            noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //            noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
                //            noiDungChinhSuaTKForm.ShowDialog();
                //        }
                //        return;
                //    }
                //}
                #endregion

                //if (ShowMessage("Bạn có muốn chương trình tự động lưu nội dung sửa đổi, bổ sung không?", true).Equals("No"))
                //{
                //    #region Người sử dụng nhập nội dung sửa đổi bằng tay
                //    string whereCondition = "TKMD_ID = " + TKMD.ID + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh.Trim() + "'";
                //    if (NoiDungDieuChinhTK.SelectCollectionDynamic(whereCondition, "").Count == 0)
                //    {
                //        if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //        {
                //            NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //            noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //            noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                //            noiDungChinhSuaTKForm.ShowDialog();
                //        }
                //        else
                //            return;
                //    }
                //    else
                //    {
                //        int count = 0;
                //        NoiDungChinhSuaTKDetailForm ndDieuChinhTKChiTietForm = new NoiDungChinhSuaTKDetailForm();
                //        string where = "TKMD_ID = " + TKMD.ID + " AND MALOAIHINH = '" + TKMD.MaLoaiHinh.Trim()+"' ";
                //        ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
                //        foreach (NoiDungDieuChinhTK nddctk in ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK)
                //        {
                //            if (nddctk.TrangThai != 1)
                //                ++count;
                //        }
                //        if (count == 0)
                //        {
                //            if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                //            {
                //                NoiDungChinhSuaTKForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKForm();
                //                noiDungChinhSuaTKForm.TKMD = this.TKMD;
                //                noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                //                noiDungChinhSuaTKForm.ShowDialog();
                //            }
                //            else
                //                return;
                //        }
                //    }
                //    #endregion
                //}
                //else
                //{
                //DATLMQ bổ sung phần tự động lưu dữ liệu vào TK sửa đổi bổ sung khi có thay đổi 19/05/2011
                try
                {
                    ToKhaiMauDich TKMD_SuaDoi = new ToKhaiMauDich();
                    NoiDungToKhai noiDungTK = new NoiDungToKhai();
                    List<NoiDungToKhai> listNoiDungTK = new List<NoiDungToKhai>();
                    NoiDungDieuChinhTKDetail noiDungEditDetail = new NoiDungDieuChinhTKDetail();
                    NoiDungDieuChinhTK noiDungEdit = new NoiDungDieuChinhTK();
                    noiDungEdit.TKMD_ID = TKMD.ID;
                    noiDungEdit.SoTK = TKMD.SoToKhai;
                    noiDungEdit.NgayDK = TKMD.NgayDangKy;
                    noiDungEdit.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungEdit.NgaySua = DateTime.Now;
                    i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID, TKMD.MaLoaiHinh);
                    if (i == 0)
                    {
                        i++;
                        noiDungEdit.SoDieuChinh = i;
                        noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        noiDungEdit.InsertUpdate();
                        luu = true;
                    }
                    else
                    {
                        if (luu == false)
                        {
                            i++;
                            noiDungEdit.SoDieuChinh = i;
                            noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                            //if (isEdited == true)
                            noiDungEdit.InsertUpdate();
                            luu = true;
                        }
                    }

                    #region Lưu tạm giá trị của TK cũ vào các biến tương ứng
                    //Người nhập khẩu & Người xuất khẩu
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        nguoiNhapKhau = TKMD.TenDoanhNghiep;
                        nguoiXuatKhau = TKMD.TenDonViDoiTac;
                    }
                    else
                    {
                        nguoiNhapKhau = TKMD.TenDonViDoiTac;
                        nguoiXuatKhau = TKMD.TenDoanhNghiep;
                    }
                    //Người ủy thác
                    nguoiUyThac = TKMD.TenDonViUT;
                    //PTVT
                    PTVT = TKMD.PTVT_ID;
                    tenPTVT = TKMD.SoHieuPTVT;
                    ngayDenPTVT = TKMD.NgayDenPTVT;
                    //Nước xuất khẩu & Nước nhập khẩu
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        nuocXuatKhau = TKMD.NuocXK_ID;
                    }
                    else
                    {
                        nuocNhapKhau = TKMD.NuocNK_ID;
                    }
                    //Điều kiện giao hàng
                    dieuKienGiaoHang = TKMD.DKGH_ID;
                    //Phương thức thanh toán
                    phuongThucThanhToan = TKMD.PTTT_ID;
                    //Giấy phép
                    soGiayPhep = TKMD.SoGiayPhep;
                    ngayGiayPhep = TKMD.NgayGiayPhep;
                    ngayHetHanGiayPhep = TKMD.NgayHetHanGiayPhep;
                    //Hóa đơn thương mại
                    hoaDonTM = TKMD.SoHoaDonThuongMai;
                    ngayHoaDonTM = TKMD.NgayHoaDonThuongMai;
                    //Địa điểm dở hàng & Địa điểm xếp hàng
                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        diaDiemDoHang = TKMD.CuaKhau_ID;
                        diaDiemXepHang = TKMD.DiaDiemXepHang;
                    }
                    else
                    {
                        diaDiemXepHang = TKMD.CuaKhau_ID;
                        diaDiemDoHang = TKMD.DiaDiemXepHang;
                    }
                    //Nguyên tệ
                    nguyenTe = TKMD.NguyenTe_ID;
                    //Tỷ giá tính thuế
                    tyGiaTinhThue = TKMD.TyGiaTinhThue;
                    //Tỷ giá USD
                    tyGiaUSD = TKMD.TyGiaUSD;
                    //Hợp đồng
                    soHopDong = TKMD.SoHopDong;
                    ngayHopDong = TKMD.NgayHopDong;
                    ngayHetHanHopDong = TKMD.NgayHetHanHopDong;
                    //Vận tải đơn
                    soVanDon = TKMD.SoVanDon;
                    ngayVanDon = TKMD.NgayVanDon;
                    //Tổng trọng lượng: số Cont20, số Cont40, số kiện hàng
                    soContainer20 = TKMD.SoContainer20;
                    soContainer40 = TKMD.SoContainer40;
                    soKienHang = TKMD.SoKien;
                    //Trọng lượng
                    trongLuong = TKMD.TrongLuong;
                    //Trọng lượng tịnh
                    trongLuongNet = TKMD.TrongLuongNet;
                    //Lệ phí HQ
                    lePhiHaiQuan = TKMD.LePhiHaiQuan;
                    //Phí vận chuyện
                    phiVanChuyen = TKMD.PhiVanChuyen;
                    //Phí bảo hiểm
                    phiBaoHiem = TKMD.PhiBaoHiem;
                    //Phí khác
                    phiKhac = TKMD.PhiKhac;
                    #endregion

                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    {
                        #region 1. Người xuất khẩu
                        //Tên Đơn vị đối tác
                        TKMD_SuaDoi.TenDonViDoiTac = txtTenDonViDoiTac.Text;
                        if (TKMD_SuaDoi.TenDonViDoiTac != TKMD.TenDonViDoiTac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbNguoiXK.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbNguoiXK.Tag.ToString();
                                noiDungTK.Ten = "1. Người xuất khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiXuatKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiXuatKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 3. Đơn vị ủy thác
                        //Tên đơn vị Ủy thác
                        TKMD_SuaDoi.TenDonViUT = txtTenDonViUyThac.Text.Trim();
                        if (TKMD_SuaDoi.TenDonViUT != TKMD.TenDonViUT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label6.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label6.Tag.ToString();
                                noiDungTK.Ten = "3. Người ủy thác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiUyThac;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiUyThac;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 6. Hóa đơn Thương mại
                        //Số Hóa đơn Thương mại
                        TKMD_SuaDoi.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text.Trim();
                        if (TKMD_SuaDoi.SoHoaDonThuongMai != TKMD.SoHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label15.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label15.Tag.ToString();
                                noiDungTK.Ten = "6. Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + hoaDonTM;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + hoaDonTM;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hóa đơn Thương mại
                        if (ccNgayHDTM.Text.Equals(""))
                            TKMD_SuaDoi.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHoaDonThuongMai = Convert.ToDateTime(ccNgayHDTM.Text);
                        if (TKMD_SuaDoi.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label16.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label16.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 7. Giấy phép
                        //Số Giấy phép
                        TKMD_SuaDoi.SoGiayPhep = txtSoGiayPhep.Text.Trim();
                        if (TKMD_SuaDoi.SoGiayPhep != TKMD.SoGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label11.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label11.Tag.ToString();
                                noiDungTK.Ten = "7. Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soGiayPhep;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Giấy phép
                        if (ccNgayGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayGiayPhep != TKMD.NgayGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label12.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label12.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hết hạn Giấy phép
                        if (ccNgayHHGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanGiayPhep = Convert.ToDateTime(ccNgayHHGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label33.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label33.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày hết hạn Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 8. Hợp đồng
                        //Số Hợp đồng
                        TKMD_SuaDoi.SoHopDong = txtSoHopDong.Text;
                        if (TKMD_SuaDoi.SoHopDong != TKMD.SoHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label13.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label13.Tag.ToString();
                                noiDungTK.Ten = "8. Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHopDong;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHopDong;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hợp đồng
                        if (ccNgayHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHopDong = Convert.ToDateTime(ccNgayHopDong.Text);
                        if (TKMD_SuaDoi.NgayHopDong != TKMD.NgayHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label14.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label14.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày hết hạn Hợp đồng
                        if (ccNgayHHHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanHopDong = Convert.ToDateTime(ccNgayHHHopDong.Text);
                        if (TKMD_SuaDoi.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label34.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label34.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày hết hạn Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 9. Vận tải đơn
                        //Số vận tải đơn
                        TKMD_SuaDoi.SoVanDon = txtSoVanTaiDon.Text;
                        if (TKMD_SuaDoi.SoVanDon != TKMD.SoVanDon)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label19.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label19.Tag.ToString();
                                noiDungTK.Ten = "9. Vận tải đơn:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soVanDon;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoVanDon;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soVanDon;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoVanDon;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày vận tải đơn
                        if (ccNgayVanTaiDon.Text.Equals(""))
                            TKMD_SuaDoi.NgayVanDon = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayVanDon = Convert.ToDateTime(ccNgayVanTaiDon.Text);
                        if (TKMD_SuaDoi.NgayVanDon != TKMD.NgayVanDon)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label20.Tag.ToString();
                                noiDungTK.Ten = "9. Ngày vận tải đơn:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayVanDon.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayVanDon.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayVanDon.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayVanDon.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 10. Cảng xếp hàng
                        TKMD_SuaDoi.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                        if (TKMD_SuaDoi.DiaDiemXepHang != TKMD.DiaDiemXepHang)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbDiaDiemXepHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbDiaDiemXepHang.Tag.ToString();
                                noiDungTK.Ten = "10. Cảng xếp hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemXepHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemXepHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 11. Cảng dở hàng
                        TKMD_SuaDoi.CuaKhau_ID = ctrCuaKhau.Ma;
                        if (TKMD_SuaDoi.CuaKhau_ID != TKMD.CuaKhau_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + grbDiaDiemDoHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = grbDiaDiemDoHang.Tag.ToString();
                                noiDungTK.Ten = "11. Cảng dở hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemDoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.CuaKhau_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemDoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.CuaKhau_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 12. Phương tiện vận tải
                        //Loại Phương tiện vận tải
                        TKMD_SuaDoi.PTVT_ID = cbPTVT.SelectedValue.ToString();
                        if (TKMD_SuaDoi.PTVT_ID != TKMD.PTVT_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label17.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label17.Tag.ToString();
                                noiDungTK.Ten = "12. Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + PTVT;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTVT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + PTVT;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTVT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Số hiệu Phương tiện vận tải
                        TKMD_SuaDoi.SoHieuPTVT = txtSoHieuPTVT.Text;
                        if (TKMD_SuaDoi.SoHieuPTVT != TKMD.SoHieuPTVT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + lblSoHieuPTVT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = lblSoHieuPTVT.Tag.ToString();
                                noiDungTK.Ten = "12. Số hiệu Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHieuPTVT;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHieuPTVT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHieuPTVT;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHieuPTVT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày đến Phương tiện vận tải
                        if (ccNgayDen.Text.Equals(""))
                            TKMD_SuaDoi.NgayDenPTVT = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayDenPTVT = Convert.ToDateTime(ccNgayDen.Text);
                        if (TKMD_SuaDoi.NgayDenPTVT != TKMD.NgayDenPTVT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + lblNgayDenPTVT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = lblNgayDenPTVT.Tag.ToString();
                                noiDungTK.Ten = "12. Ngày đến Phương tiện vận tải:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayDenPTVT.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayDenPTVT.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayDenPTVT.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayDenPTVT.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 13. Nước xuất khẩu
                        TKMD_SuaDoi.NuocXK_ID = ctrNuocXuatKhau.Ma.Trim();
                        if (TKMD_SuaDoi.NuocXK_ID != TKMD.NuocXK_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNuocXuatKhau.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNuocXuatKhau.Tag.ToString();
                                noiDungTK.Ten = "13. Nước xuất khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nuocXuatKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NuocXK_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nuocXuatKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NuocXK_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 14. Điều kiện giao hàng
                        TKMD_SuaDoi.DKGH_ID = cbDKGH.SelectedValue.ToString();
                        if (TKMD_SuaDoi.DKGH_ID != TKMD.DKGH_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbDKGH.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbDKGH.Tag.ToString();
                                noiDungTK.Ten = "14. Điều kiện giao hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + dieuKienGiaoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + dieuKienGiaoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 15. Phương thức thanh toán
                        TKMD_SuaDoi.PTTT_ID = cbPTTT.SelectedValue.ToString();
                        if (TKMD_SuaDoi.PTTT_ID != TKMD.PTTT_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbPTTT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbPTTT.Tag.ToString();
                                noiDungTK.Ten = "15. Phương thức thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phuongThucThanhToan;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phuongThucThanhToan;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 16. Đồng tiền thanh toán
                        TKMD_SuaDoi.NguyenTe_ID = ctrNguyenTe.Ma;
                        if (TKMD_SuaDoi.NguyenTe_ID != TKMD.NguyenTe_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNguyenTe.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNguyenTe.Tag.ToString();
                                noiDungTK.Ten = "16. Đồng tiền thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguyenTe;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguyenTe;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 17. Tỷ giá tính thuế
                        TKMD_SuaDoi.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        if (TKMD_SuaDoi.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTyGiaTinhThue.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTyGiaTinhThue.Tag.ToString();
                                noiDungTK.Ten = "17. Tỷ giá tính thuế:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + tyGiaTinhThue.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + tyGiaTinhThue.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container20
                        TKMD_SuaDoi.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        if (TKMD_SuaDoi.SoContainer20 != TKMD.SoContainer20)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer20.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont20:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer20.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer20.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container40
                        TKMD_SuaDoi.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        if (TKMD_SuaDoi.SoContainer40 != TKMD.SoContainer40)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer40.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer40.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont40:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer40.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer40.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số kiện hàng
                        TKMD_SuaDoi.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        if (TKMD_SuaDoi.SoKien != TKMD.SoKien)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoKien.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoKien.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Tổng số kiện:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soKienHang.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soKienHang.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng
                        TKMD_SuaDoi.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);
                        if (TKMD_SuaDoi.TrongLuong != TKMD.TrongLuong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuong.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuong.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng trọng lượng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuong.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng tịnh
                        TKMD_SuaDoi.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Text);
                        if (TKMD_SuaDoi.TrongLuongNet != TKMD.TrongLuongNet)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuongTinh.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuongTinh.Tag.ToString();
                                noiDungTK.Ten = "32. Ghi chép khác: Trọng lượng tịnh:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuongNet.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuongNet.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Lệ phí hải quan
                        TKMD_SuaDoi.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        if (TKMD_SuaDoi.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtLePhiHQ.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtLePhiHQ.Tag.ToString();
                                noiDungTK.Ten = "Phí hải quan:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + lePhiHaiQuan.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + lePhiHaiQuan.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí bảo hiểm
                        TKMD_SuaDoi.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        if (TKMD_SuaDoi.PhiBaoHiem != TKMD.PhiBaoHiem)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiBaoHiem.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiBaoHiem.Tag.ToString();
                                noiDungTK.Ten = "Phí bảo hiểm:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiBaoHiem.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiBaoHiem.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí vận chuyển
                        TKMD_SuaDoi.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        if (TKMD_SuaDoi.PhiVanChuyen != TKMD.PhiVanChuyen)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiVanChuyen.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiVanChuyen.Tag.ToString();
                                noiDungTK.Ten = "Phí vận chuyển:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiVanChuyen.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiVanChuyen.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí khác
                        TKMD_SuaDoi.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Text);
                        if (TKMD_SuaDoi.PhiKhac != TKMD.PhiKhac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiNganHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiNganHang.Tag.ToString();
                                noiDungTK.Ten = "Phí khác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiKhac.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiKhac.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region 2. Người nhập khẩu
                        //Tên Đơn vị đối tác
                        TKMD_SuaDoi.TenDonViDoiTac = txtTenDonViDoiTac.Text;
                        if (TKMD_SuaDoi.TenDonViDoiTac != TKMD.TenDonViDoiTac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NguoiNhapKhau" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "NguoiNhapKhau";
                                noiDungTK.Ten = "2. Người nhập khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiNhapKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiNhapKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViDoiTac;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 3. Đơn vị ủy thác
                        //Tên đơn vị Ủy thác
                        TKMD_SuaDoi.TenDonViUT = txtTenDonViUyThac.Text.Trim();
                        if (TKMD_SuaDoi.TenDonViUT != TKMD.TenDonViUT)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label6.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label6.Tag.ToString();
                                noiDungTK.Ten = "3. Người ủy thác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguoiUyThac;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguoiUyThac;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TenDonViUT;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 6. Giấy phép
                        //Số Giấy phép
                        TKMD_SuaDoi.SoGiayPhep = txtSoGiayPhep.Text.Trim();
                        if (TKMD_SuaDoi.SoGiayPhep != TKMD.SoGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label11.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label11.Tag.ToString();
                                noiDungTK.Ten = "6. Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soGiayPhep;
                                    noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoGiayPhep;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Giấy phép
                        if (ccNgayGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayGiayPhep != TKMD.NgayGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label12.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label12.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hết hạn Giấy phép
                        if (ccNgayHHGiayPhep.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanGiayPhep = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanGiayPhep = Convert.ToDateTime(ccNgayHHGiayPhep.Text);
                        if (TKMD_SuaDoi.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label33.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label33.Tag.ToString();
                                noiDungTK.Ten = "6. Ngày hết hạn Giấy phép:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanGiayPhep.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 7. Hợp đồng
                        //Số Hợp đồng
                        TKMD_SuaDoi.SoHopDong = txtSoHopDong.Text;
                        if (TKMD_SuaDoi.SoHopDong != TKMD.SoHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label13.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label13.Tag.ToString();
                                noiDungTK.Ten = "7. Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soHopDong;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soHopDong;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHopDong;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hợp đồng
                        if (ccNgayHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHopDong = Convert.ToDateTime(ccNgayHopDong.Text);
                        if (TKMD_SuaDoi.NgayHopDong != TKMD.NgayHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label14.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label14.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày hết hạn Hợp đồng
                        if (ccNgayHHHopDong.Text.Equals(""))
                            TKMD_SuaDoi.NgayHetHanHopDong = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHetHanHopDong = Convert.ToDateTime(ccNgayHHHopDong.Text);
                        if (TKMD_SuaDoi.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label34.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label34.Tag.ToString();
                                noiDungTK.Ten = "7. Ngày hết hạn Hợp đồng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHetHanHopDong.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 8. Hóa đơn thương mại
                        //Số Hóa đơn Thương mại
                        TKMD_SuaDoi.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text.Trim();
                        if (TKMD_SuaDoi.SoHoaDonThuongMai != TKMD.SoHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label15.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label15.Tag.ToString();
                                noiDungTK.Ten = "8. Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + hoaDonTM;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + hoaDonTM;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoHoaDonThuongMai;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        //Ngày Hóa đơn Thương mại
                        if (ccNgayHDTM.Text.Equals(""))
                            TKMD_SuaDoi.NgayHoaDonThuongMai = new DateTime(1900, 1, 1);
                        else
                            TKMD_SuaDoi.NgayHoaDonThuongMai = Convert.ToDateTime(ccNgayHDTM.Text);
                        if (TKMD_SuaDoi.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + label16.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = label16.Tag.ToString();
                                noiDungTK.Ten = "8. Ngày Hóa đơn thương mại:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + ngayHoaDonTM.ToShortDateString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NgayHoaDonThuongMai.ToShortDateString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 9. Cảng xếp hàng
                        TKMD_SuaDoi.DiaDiemXepHang = Company.GC.BLL.DuLieuChuan.CuaKhau.GetName(ctrCuaKhau.Ma);
                        if (TKMD_SuaDoi.DiaDiemXepHang != TKMD.DiaDiemXepHang)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "CangXepHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "CangXepHang";
                                noiDungTK.Ten = "9. Cảng xếp hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + diaDiemXepHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + diaDiemXepHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DiaDiemXepHang;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 10. Nước nhập khẩu
                        TKMD_SuaDoi.NuocNK_ID = ctrNuocXuatKhau.Ma.Trim();
                        if (TKMD_SuaDoi.NuocNK_ID != TKMD.NuocNK_ID.Trim())
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocNK" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = "NuocNK";
                                noiDungTK.Ten = "10. Nước nhập khẩu:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nuocNhapKhau;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NuocNK_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nuocNhapKhau;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NuocNK_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 11. Điều kiện giao hàng
                        TKMD_SuaDoi.DKGH_ID = cbDKGH.SelectedValue.ToString();
                        if (TKMD_SuaDoi.DKGH_ID != TKMD.DKGH_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbDKGH.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbDKGH.Tag.ToString();
                                noiDungTK.Ten = "11. Điều kiện giao hàng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + dieuKienGiaoHang;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + dieuKienGiaoHang;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.DKGH_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 12. Phương thức thanh toán
                        TKMD_SuaDoi.PTTT_ID = cbPTTT.SelectedValue.ToString();
                        if (TKMD_SuaDoi.PTTT_ID != TKMD.PTTT_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + cbPTTT.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = cbPTTT.Tag.ToString();
                                noiDungTK.Ten = "12. Phương thức thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phuongThucThanhToan;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phuongThucThanhToan;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PTTT_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 13. Đồng tiền thanh toán
                        TKMD_SuaDoi.NguyenTe_ID = ctrNguyenTe.Ma;
                        if (TKMD_SuaDoi.NguyenTe_ID != TKMD.NguyenTe_ID)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + ctrNguyenTe.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = ctrNguyenTe.Tag.ToString();
                                noiDungTK.Ten = "13. Đồng tiền thanh toán:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + nguyenTe;
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + nguyenTe;
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.NguyenTe_ID;
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region 14. Tỷ giá tính thuế
                        TKMD_SuaDoi.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        if (TKMD_SuaDoi.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTyGiaTinhThue.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTyGiaTinhThue.Tag.ToString();
                                noiDungTK.Ten = "14. Tỷ giá tính thuế:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + tyGiaTinhThue.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + tyGiaTinhThue.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TyGiaTinhThue.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container20
                        TKMD_SuaDoi.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        if (TKMD_SuaDoi.SoContainer20 != TKMD.SoContainer20)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer20.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer20.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont20:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer20.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer20.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer20.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số Container40
                        TKMD_SuaDoi.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        if (TKMD_SuaDoi.SoContainer40 != TKMD.SoContainer40)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoContainer40.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoContainer40.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Cont40:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soContainer40.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soContainer40.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoContainer40.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Số kiện hàng
                        TKMD_SuaDoi.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        if (TKMD_SuaDoi.SoKien != TKMD.SoKien)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtSoKien.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtSoKien.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng số container: Tổng số kiện:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + soKienHang.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + soKienHang.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.SoKien.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng
                        TKMD_SuaDoi.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);
                        if (TKMD_SuaDoi.TrongLuong != TKMD.TrongLuong)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuong.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuong.Tag.ToString();
                                noiDungTK.Ten = "31. Tổng trọng lượng:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuong.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuong.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuong.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Trọng lượng tịnh
                        TKMD_SuaDoi.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Text);
                        if (TKMD_SuaDoi.TrongLuongNet != TKMD.TrongLuongNet)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtTrongLuongTinh.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtTrongLuongTinh.Tag.ToString();
                                noiDungTK.Ten = "32. Ghi chép khác: Trọng lượng tịnh:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + trongLuongNet.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + trongLuongNet.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.TrongLuongNet.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Lệ phí hải quan
                        TKMD_SuaDoi.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        if (TKMD_SuaDoi.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtLePhiHQ.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtLePhiHQ.Tag.ToString();
                                noiDungTK.Ten = "Phí hải quan:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + lePhiHaiQuan.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + lePhiHaiQuan.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.LePhiHaiQuan.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí bảo hiểm
                        TKMD_SuaDoi.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        if (TKMD_SuaDoi.PhiBaoHiem != TKMD.PhiBaoHiem)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiBaoHiem.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiBaoHiem.Tag.ToString();
                                noiDungTK.Ten = "Phí bảo hiểm:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiBaoHiem.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiBaoHiem.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiBaoHiem.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí vận chuyển
                        TKMD_SuaDoi.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        if (TKMD_SuaDoi.PhiVanChuyen != TKMD.PhiVanChuyen)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiVanChuyen.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiVanChuyen.Tag.ToString();
                                noiDungTK.Ten = "Phí vận chuyển:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiVanChuyen.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiVanChuyen.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiVanChuyen.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion

                        #region Phí khác
                        TKMD_SuaDoi.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Text);
                        if (TKMD_SuaDoi.PhiKhac != TKMD.PhiKhac)
                        {
                            //isEdited = true;
                            listNoiDungTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + txtPhiNganHang.Tag.ToString() + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                            if (listNoiDungTK.Count == 0)
                            {
                                noiDungTK.Ma = txtPhiNganHang.Tag.ToString();
                                noiDungTK.Ten = "Phí khác:";
                                noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungTK.InsertUpdate();

                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " " + phiKhac.ToString();
                                noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                            else
                            {
                                foreach (NoiDungToKhai ndtk in listNoiDungTK)
                                {
                                    noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                    noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " " + phiKhac.ToString();
                                    noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " " + TKMD_SuaDoi.PhiKhac.ToString();
                                    noiDungEditDetail.InsertUpdate();
                                }
                            }
                        }
                        #endregion
                    }

                    //hangMDTK = TKMD.HMDCollection[soDongHang];
                    AutoInsertHMDToTKEdit(HangMauDichEditForm.maHS_Old, HangMauDichEditForm.tenHang_Old, HangMauDichEditForm.maHang_Old, HangMauDichEditForm.xuatXu_Old,
                                          HangMauDichEditForm.soLuong_Old, HangMauDichEditForm.dvt_Old, HangMauDichEditForm.donGiaNT_Old, HangMauDichEditForm.triGiaNT_Old, TKMD);

                    //DATLMQ bổ sung tự động Lưu thông tin Xóa và Thêm Hàng hóa ngày 12/08/2011
                    AutoInsertDeleteHMD();
                    AutoInsertAddHMD();
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình lưu tờ khai sửa: " + ex.Message, false);
                }
                //}
            }
            if (NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                cvError.ContainerToValidate = txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }
            if (valid)
            {
                if (TKMD.HMDCollection.Count == 0)
                {
                    Globals.ShowMessageTQDT("Chưa nhập thông tin hàng của tờ khai", false);
                    return;
                }
                if (TKMD.ID > 0)
                {
                    if (TKMD.MaLoaiHinh.Contains("XGC"))
                    {
                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                        {
                            Globals.ShowMessageTQDT("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            return;
                        }
                    }


                }
                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid tax assessment rate"));
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;

                }
                if (!KiemTraLuongTon())
                    return;
                Hdgc.Load();

                TKMD.SoTiepNhan = TKMD.SoTiepNhan;
                TKMD.SoToKhai = TKMD.SoToKhai;
                //TKMD.ActionStatus = 0;
                if (!isKhaibaoSua)
                {
                    if (TKMD.SoTiepNhan > 0)
                    {
                        TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                        TKMD.ActionStatus = 0;
                    }
                    else
                    {
                        TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        TKMD.ActionStatus = -1;
                        TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                    }

                    if (TKMD.SoToKhai > 0)
                        TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    else
                    {
                        TKMD.PhanLuong = "";
                        TKMD.HUONGDAN = "";
                        TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                    }
                }
                // Đề xuất khác
                TKMD.DeXuatKhac = txtDeXuatKhac.Text;
                //Lý do sửa
                if (isKhaibaoSua)
                    TKMD.LyDoSua = txtLyDoSua.Text;
                else
                    TKMD.LyDoSua = "";
                // TKMD.MaHaiQuan = HDGC.MaHaiQuan;
                TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TKMD.SoHang = (short)TKMD.HMDCollection.Count;
                TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

                // Doanh nghiệp.
                TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                // Đại lý TTHQ.
                TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                // Loại hình mậu dịch.
                TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                // Giấy phép.
                if (txtSoGiayPhep.Text.Trim().Length > 0)
                {
                    TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    TKMD.NgayGiayPhep = !ccNgayGiayPhep.IsNullDate ? ccNgayGiayPhep.Value : DateTime.Parse("01/01/1900");
                    TKMD.NgayHetHanGiayPhep = !ccNgayHHGiayPhep.IsNullDate ? ccNgayHHGiayPhep.Value : DateTime.Parse("01/01/1900");
                }
                else
                {
                    TKMD.SoGiayPhep = "";
                    TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    TKMD.NgayHopDong = ccNgayHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    TKMD.NgayHetHanHopDong = ccNgayHHHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHHHopDong.Value;
                }
                else
                {
                    TKMD.SoHopDong = "";
                    TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                {
                    TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    TKMD.NgayHoaDonThuongMai = ccNgayHDTM.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHDTM.Value;
                }
                else
                {
                    TKMD.SoHoaDonThuongMai = "";
                    TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }
                // Phương tiện vận tải.
                TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                TKMD.NgayDenPTVT = ccNgayDen.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayDen.Value;

                // Vận tải đơn.
                if (txtSoVanTaiDon.Text.Trim().Length > 0)
                {
                    TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    TKMD.NgayVanDon = ccNgayVanTaiDon.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayVanTaiDon.Value;
                }
                else
                {
                    TKMD.SoVanDon = "";
                    TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }
                // Nước.
                if (NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma.Trim();
                    TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    TKMD.NuocXK_ID = "VN".PadRight(3);
                    TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma.Trim();
                }

                // Địa điểm xếp hàng.
                //if (chkDiaDiemXepHang.Checked)
                TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                // else
                //     TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                TKMD.TenChuHang = txtTenChuHang.Text;
                TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                if (GlobalSettings.TuDongTinhThue == "1" && TKMD.MaLoaiHinh.Contains("N"))
                    TinhLaiThue();
                else
                {
                    TinhTongTriGiaKhaiBao();
                }
                //
                if (!isKhaibaoSua)
                    TKMD.TrangThaiXuLy = -1;
                TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                // tkmd.NgayGui = DateTime.Parse("01/01/1900");
                if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
                if (radSP.Checked) TKMD.LoaiHangHoa = "S";
                if (radTB.Checked) TKMD.LoaiHangHoa = "T";

                TKMD.GiayTo = txtChungTu.Text;

                TKMD.MaMid = txtMaMid.Text.Trim();

                long ret;
                try
                {
                    //HungTQ, Update Nam dang ky.
                    TKMD.NamDK = DateTime.Now.Year;
                    if (string.IsNullOrEmpty(TKMD.GUIDSTR)) TKMD.GUIDSTR = Guid.NewGuid().ToString();
                    TKMD.InsertUpdateFull();

                    //Neu la chinh sua, cap nhat lai ID luu tam la ID cua TKMD dang mo: tranh bi copy trung du lieu cua TK cu.
                    if (BNew == false)
                        PtkmdId = TKMD.ID;

                    //Lypt update date 22/01/2010
                    if (BNew)
                    {
                        //Cap nhat lai trang thai sau khi them moi
                        BNew = false;

                        //Copy Van don                        
                        List<VanDon> VanDoncollection = new List<VanDon>();
                        VanDoncollection = VanDon.SelectCollectionBy_TKMD_ID(PtkmdId);
                        foreach (VanDon vd in VanDoncollection)
                        {
                            long vdResult = VanDon.InsertVanDon(vd.SoVanDon, vd.NgayVanDon, vd.HangRoi, vd.SoHieuPTVT, vd.NgayDenPTVT, vd.MaHangVT, vd.TenHangVT, vd.TenPTVT, vd.QuocTichPTVT, vd.NuocXuat_ID, vd.MaNguoiNhanHang,
                                vd.TenNguoiNhanHang, vd.MaNguoiGiaoHang, vd.TenNguoiNhanHang, vd.CuaKhauNhap_ID, vd.CuaKhauXuat, vd.MaNguoiNhanHangTrungGian, vd.TenNguoiNhanHangTrungGian, vd.MaCangXepHang, vd.TenCangXepHang,
                                vd.MaCangDoHang, vd.TenCangDoHang, TKMD.ID, vd.DKGH_ID, vd.DiaDiemGiaoHang, vd.NoiDi, vd.SoHieuChuyenDi, vd.NgayKhoiHanh);
                            List<Container> ContColl = Company.KDT.SHARE.QuanLyChungTu.Container.SelectCollectionBy_VanDon_ID(vd.ID);
                            for (int v = 0; v < ContColl.Count; v++)
                            {
                                ContColl[v].VanDon_ID = vdResult;
                            }
                            Company.KDT.SHARE.QuanLyChungTu.Container.InsertCollection(ContColl);
                        }
                        //copy Hop dong
                        List<HopDongThuongMai> HopDongCollection = new List<HopDongThuongMai>();
                        HopDongCollection = HopDongThuongMai.SelectCollectionBy_TKMD_ID(PtkmdId);

                        foreach (HopDongThuongMai hd in HopDongCollection)
                        {
                            //Copy hop dong
                            long hdResult = HopDongThuongMai.InsertHopDongThuongMai(hd.SoHopDongTM, hd.NgayHopDongTM, hd.ThoiHanThanhToan, hd.NguyenTe_ID, hd.PTTT_ID, hd.DKGH_ID, hd.DiaDiemGiaoHang, hd.MaDonViMua, hd.TenDonViMua,
                                hd.MaDonViBan, hd.TenDonViBan, hd.TongTriGia, hd.ThongTinKhac, TKMD.ID, hd.GuidStr, hd.LoaiKB, hd.SoTiepNhan, hd.NgayTiepNhan, hd.TrangThai, hd.NamTiepNhan, hd.MaDoanhNghiep);

                            //---Hang hop dong   
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hop dong
                            List<HopDongThuongMaiDetail> HopDongDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hd.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HopDongThuongMaiDetail> HopDongDTMColl = new List<HopDongThuongMaiDetail>();

                            for (int l = 0; l < HopDongDetailUpdateCollection.Count; l++)
                            {
                                HopDongThuongMaiDetail hopDongCopy = HopDongDetailUpdateCollection[l];

                                if (hopDongCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.GC.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hopDongCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HopDongThuongMaiDetail hdtmDetail = new HopDongThuongMaiDetail();
                                    hdtmDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdtmDetail.HopDongTM_ID = hdResult;

                                    hdtmDetail.SoThuTuHang = hopDongCopy.SoThuTuHang;
                                    hdtmDetail.MaHS = hopDongCopy.MaHS;
                                    hdtmDetail.MaPhu = hopDongCopy.MaPhu;
                                    hdtmDetail.TenHang = hopDongCopy.TenHang;
                                    hdtmDetail.NuocXX_ID = hopDongCopy.NuocXX_ID;
                                    hdtmDetail.DVT_ID = hopDongCopy.DVT_ID;
                                    hdtmDetail.SoLuong = hopDongCopy.SoLuong;
                                    hdtmDetail.DonGiaKB = hopDongCopy.DonGiaKB;
                                    hdtmDetail.TriGiaKB = hopDongCopy.TriGiaKB;

                                    HopDongDTMColl.Add(hdtmDetail);
                                }
                            }

                            HopDongThuongMaiDetail.InsertCollection(HopDongDTMColl);
                        }

                        //copy Giay phep
                        List<GiayPhep> GPColl = new List<GiayPhep>();
                        GPColl = GiayPhep.SelectCollectionBy_TKMD_ID(PtkmdId);

                        foreach (GiayPhep gp in GPColl)
                        {
                            long gpResult = GiayPhep.InsertGiayPhep(gp.SoGiayPhep, gp.NgayGiayPhep, gp.NgayHetHan, gp.NguoiCap, gp.NoiCap, gp.MaDonViDuocCap, gp.TenDonViDuocCap, gp.MaCoQuanCap, gp.TenQuanCap, gp.ThongTinKhac, gp.MaDoanhNghiep,
                                TKMD.ID, gp.GuidStr, gp.LoaiKB, gp.SoTiepNhan, gp.NgayTiepNhan, gp.TrangThai, gp.NamTiepNhan);

                            //--Hang Giay phep
                            //Lay hang mau dich co cap nhat thong tin rieng cua Giay phep
                            List<HangGiayPhepDetail> GiayPhepDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(gp.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HangGiayPhepDetail> HangGP = new List<HangGiayPhepDetail>();

                            for (int k = 0; k < GiayPhepDetailUpdateCollection.Count; k++)
                            {
                                HangGiayPhepDetail giayPhepCopy = GiayPhepDetailUpdateCollection[k];

                                if (giayPhepCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.GC.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, giayPhepCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HangGiayPhepDetail gpDetail = new HangGiayPhepDetail();
                                    gpDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    gpDetail.GiayPhep_ID = gpResult;

                                    gpDetail.SoThuTuHang = giayPhepCopy.SoThuTuHang;
                                    gpDetail.MaHS = giayPhepCopy.MaHS;
                                    gpDetail.MaPhu = giayPhepCopy.MaPhu;
                                    gpDetail.TenHang = giayPhepCopy.TenHang;
                                    gpDetail.NuocXX_ID = giayPhepCopy.NuocXX_ID;
                                    gpDetail.DVT_ID = giayPhepCopy.DVT_ID;
                                    gpDetail.SoLuong = giayPhepCopy.SoLuong;
                                    gpDetail.DonGiaKB = giayPhepCopy.DonGiaKB;
                                    gpDetail.TriGiaKB = giayPhepCopy.TriGiaKB;

                                    gpDetail.MaChuyenNganh = giayPhepCopy.MaChuyenNganh;
                                    gpDetail.MaNguyenTe = giayPhepCopy.MaNguyenTe;

                                    HangGP.Add(gpDetail);
                                }
                            }
                            HangGiayPhepDetail.InsertCollection(HangGP);
                        }

                        // copy Hoa don thuong mai
                        List<HoaDonThuongMai> HDTMColl = new List<HoaDonThuongMai>();
                        HDTMColl = HoaDonThuongMai.SelectCollectionBy_TKMD_ID(PtkmdId);

                        foreach (HoaDonThuongMai hdtm in HDTMColl)
                        {
                            long hdonResult = HoaDonThuongMai.InsertHoaDonThuongMai(hdtm.SoHoaDon, hdtm.NgayHoaDon, hdtm.NguyenTe_ID, hdtm.PTTT_ID, hdtm.DKGH_ID, hdtm.MaDonViMua, hdtm.TenDonViMua, hdtm.MaDonViBan, hdtm.TenDonViBan,
                                hdtm.ThongTinKhac, TKMD.ID, hdtm.GuidStr, hdtm.LoaiKB, hdtm.SoTiepNhan, hdtm.NgayTiepNhan, hdtm.TrangThai, hdtm.NamTiepNhan, hdtm.MaDoanhNghiep);

                            //--Hang hoa don thuong mai
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hoa don
                            List<HoaDonThuongMaiDetail> HoaDonDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(hdtm.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HoaDonThuongMaiDetail> HoaDonTMColl = new List<HoaDonThuongMaiDetail>();

                            for (int l = 0; l < HoaDonDetailUpdateCollection.Count; l++)
                            {
                                HoaDonThuongMaiDetail hoaDonCopy = HoaDonDetailUpdateCollection[l];

                                if (hoaDonCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.GC.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hoaDonCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HoaDonThuongMaiDetail hdonDetail = new HoaDonThuongMaiDetail();
                                    hdonDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdonDetail.HoaDonTM_ID = hdonResult;

                                    hdonDetail.SoThuTuHang = hoaDonCopy.SoThuTuHang;
                                    hdonDetail.MaHS = hoaDonCopy.MaHS;
                                    hdonDetail.MaPhu = hoaDonCopy.MaPhu;
                                    hdonDetail.TenHang = hoaDonCopy.TenHang;
                                    hdonDetail.NuocXX_ID = hoaDonCopy.NuocXX_ID;
                                    hdonDetail.DVT_ID = hoaDonCopy.DVT_ID;
                                    hdonDetail.SoLuong = hoaDonCopy.SoLuong;
                                    hdonDetail.DonGiaKB = hoaDonCopy.DonGiaKB;
                                    hdonDetail.TriGiaKB = hoaDonCopy.TriGiaKB;

                                    HoaDonTMColl.Add(hdonDetail);
                                }
                            }
                            HoaDonThuongMaiDetail.InsertCollection(HoaDonTMColl);
                        }                        // copy CO
                        List<CO> COColl = new List<CO>();
                        COColl = Company.KDT.SHARE.QuanLyChungTu.CO.SelectCollectionBy_TKMD_ID(PtkmdId);
                        foreach (CO co in COColl)
                        {
                            Company.KDT.SHARE.QuanLyChungTu.CO.InsertCO(co.SoCO, co.NgayCO, co.ToChucCap, co.NuocCapCO, co.MaNuocXKTrenCO, co.MaNuocNKTrenCO, co.TenDiaChiNguoiXK, co.TenDiaChiNguoiNK, co.LoaiCO,
                                co.ThongTinMoTaChiTiet, co.MaDoanhNghiep, co.NguoiKy, co.NoCo, co.ThoiHanNop, TKMD.ID, co.GuidStr, co.LoaiKB, co.SoTiepNhan, co.NgayTiepNhan, co.TrangThai, co.NamTiepNhan);
                        }
                        //Copy De nghi chuyen cua khau
                        List<DeNghiChuyenCuaKhau> chuyenCK = new List<DeNghiChuyenCuaKhau>();
                        chuyenCK = Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(PtkmdId);
                        foreach (DeNghiChuyenCuaKhau ck in chuyenCK)
                        {
                            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.InsertDeNghiChuyenCuaKhau(ck.ThongTinKhac, ck.MaDoanhNghiep, TKMD.ID, ck.GuidStr, ck.LoaiKB, ck.SoTiepNhan, ck.NgayTiepNhan, ck.TrangThai,
                                ck.NamTiepNhan, ck.SoVanDon, ck.NgayVanDon, ck.ThoiGianDen, ck.DiaDiemKiemTra, ck.TuyenDuong);
                        }

                        //Copy Chung tu kem
                        List<ChungTuKem> chungTuKem = new List<ChungTuKem>();
                        chungTuKem = ChungTuKem.SelectCollectionBy_TKMDID(PtkmdId);
                        long ctkID = 0;
                        foreach (ChungTuKem ctk in chungTuKem)
                        {
                            ctkID = ChungTuKem.InsertChungTuKem(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.DIENGIAI, ctk.LoaiKB, ctk.TrangThaiXuLy, ctk.Tempt, ctk.MessageID, ctk.GUIDSTR, ctk.KDT_WAITING, ctk.KDT_LASTINFO, ctk.SOTN, ctk.NGAYTN, ctk.TotalSize, ctk.Phanluong, ctk.HuongDan, TKMD.ID);

                            ctk.LoadListCTChiTiet();
                            foreach (ChungTuKemChiTiet chiTiet in ctk.listCTChiTiet)
                            {
                                ChungTuKemChiTiet.InsertChungTuKemChiTiet(ctkID, chiTiet.FileName, chiTiet.FileSize, chiTiet.NoiDung);
                            }
                        }

                        if (TKMD.HMDCollection == null || TKMD.HMDCollection.Count == 0)
                            TKMD.LoadHMDCollection();
                        if (TKMD.ChungTuTKCollection == null || TKMD.ChungTuTKCollection.Count == 0)
                            TKMD.LoadChungTuTKCollection();
                        TKMD.LoadChungTuHaiQuan();
                        if (TKMD.ListCO == null || TKMD.ListCO.Count == 0)
                            TKMD.LoadCO();


                    }
                    //Het Lypt Update

                    TKMD.LoadChungTuTKCollection();
                    TKMD.LoadHMDCollection();
                    dgList.DataSource = TKMD.HMDCollection;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch
                    {
                        dgList.Refresh();
                    }
                    gridEX1.DataSource = TKMD.ChungTuTKCollection;
                    gridEX1.Refetch();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai;
                            log.ID_DK = TKMD.ID;
                            log.GUIDSTR_DK = "";
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                        try
                        {
                            string where = "1 = 1";
                            where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                            List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                            if (listLog.Count > 0)
                            {
                                long idLog = listLog[0].IDLog;
                                string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                long idDK = listLog[0].ID_DK;
                                string guidstr = listLog[0].GUIDSTR_DK;
                                string userKhaiBao = listLog[0].UserNameKhaiBao;
                                DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                string userSuaDoi = GlobalSettings.UserLog;
                                DateTime ngaySuaDoi = DateTime.Now;
                                string ghiChu = listLog[0].GhiChu;
                                bool isDelete = listLog[0].IsDelete;
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                            userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                            return;
                        }
                    }
                    #endregion

                    MLMessages("Lưu tờ khai thành công.", "MSG_SAV02", "", false);
                    setCommandStatus();
                }
                catch (Exception ex)
                {
                    ShowMessage(setText("Dữ liệu không hợp lệ. Lỗi ", "Invalid value. Error") + ex.Message, false);
                }
            }
        }

        #endregion

        #region In Tờ Khai

        private void inToKhai()
        {
            if (TKMD.HMDCollection.Count == 0) return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = TKMD;
                    f.ShowDialog();
                    break;

                case "X":
                    var f1 = new ReportViewTKXForm();
                    f1.TKMD = TKMD;
                    f1.ShowDialog();
                    break;
            }

        }
        #endregion
        #region In tờ khai TQDT theo TT15 - Update by KhanhHN 29/03/2012
        private void InTKTT15()
        {
            if (TKMD.HMDCollection.Count == 0) return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNTQDTFormTT15 f = new ReportViewTKNTQDTFormTT15();
                    f.TKMD = TKMD;
                    f.ShowDialog();
                    break;

                case "X":
                    ReportViewTKXTQDTFormTT15 f1 = new ReportViewTKXTQDTFormTT15();
                    f1.TKMD = TKMD;
                    f1.ShowDialog();
                    break;
            }

        }
        #endregion
        private void inPhieuTN()
        {
            if (TKMD.SoTiepNhan == 0) return;
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "TỜ KHAI";
            phieuTN.soTN = TKMD.SoTiepNhan.ToString();
            phieuTN.ngayTN = TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = TKMD.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void InPhulucContainer()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        #region Set  trạng thái thanh Command
        private void setCommandStatus()
        {
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Chưa khai báo";
                txtSoTiepNhan.Text = txtSoToKhai.Text = "";
                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Không phê duyệt";
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                //cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (TKMD.PhanLuong != "")
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    //cmdChungTuBoSung.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }

                lblTrangThai.Text = "Đã duyệt";

                txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //cmdHuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                {
                    lblTrangThai.Text = "Đã sửa, chờ duyệt";
                    Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    lblTrangThai.Text = "Chờ duyệt";
                    Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                txtLyDoSua.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = "Sửa tờ khai đã duyệt";
                txtSoToKhai.Text = TKMD.SoToKhai.ToString();

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = "Đã hủy";
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = "Chờ hủy";
            }
            if (TKMD.PhanLuong == "1")
            {
                lblPhanLuong.Text = "Luồng Xanh";
            }
            else if (TKMD.PhanLuong == "2")
            {
                lblPhanLuong.Text = "Luồng Vàng";
            }
            else if (TKMD.PhanLuong == "3")
            {
                lblPhanLuong.Text = "Luồng Đỏ";
            }
            else
            {
                lblPhanLuong.Text = "Chưa phân luồng";
            }

            SetCommandStatusSuaHuyToKhai();
        }

        private void SetCommandStatusSuaHuyToKhai()
        {
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.HUYTKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Đã hủy";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //Nếu tờ khai chờ duyệt có số tờ khai thì không cho phép hủy khai báo
                if (TKMD.SoToKhai > 0)
                    Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Không phê duyệt";

                if (TKMD.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }
        }

        #endregion

        #region sự kiện kích chuột trên thanh công cụ
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, Update 06/11/2011
                case "cmdInAnDinhThue":
                    InAnDinhThue(this.TKMD.ID, this.TKMD.GUIDSTR);
                    break;
                    // KhanhHN, update 29/03/2012
                case "cmdInTKTT15":
                    InTKTT15();
                    break;

                case "cmdSave":
                    Save();
                    break;
                case "cmdTinhLaiThue":
                    TinhLaiThue();
                    //showMsg("MSG_WRN36", TKMD.TongTriGiaKhaiBao);
                    Globals.ShowMessageTQDT("Tổng trị giá khai báo là : " + TKMD.TongTriGiaKhaiBao, false);
                    break;
                case "cmdReadExcel":
                    ReadExcel();
                    break;
                case "cmdThemHang":
                    ThemHang();
                    break;
                case "ThemHang":
                    ThemHang();
                    break;
                case "ToKhaiViet":
                    inToKhai();
                    break;
                case "InPhieuTN":
                    inPhieuTN();
                    break;
                case "cmContainer":
                    InPhulucContainer();
                    break;
                case "cmdSend":
                    Send();
                    checkTrangThaiXuLyTK();
                    break;
                case "ChungTuKemTheo":
                    AddChungTu();
                    break;
                case "NhanDuLieu":
                    NhanPhanLuongToKhai();
                    checkTrangThaiXuLyTK();
                    break;
                case "XacNhan":
                    LaySoTiepNhanDT();
                    checkTrangThaiXuLyTK();
                    break;
                case "FileDinhKem":
                    GetFileDinhKem();
                    break;
                case "Huy":
                    HuyToKhai();
                    break;
                case "CanDoiNhapXuat":
                    ShowCanDoiNhapXuat();
                    break;
                case "cmdNhieuHang":
                    ShowThemNhieuHang();
                    break;
                case "PhanBo":
                    ThucHienPhanBoToKhai();
                    break;
                case "cmdXuatExcelHang":
                    XuatExcelHang();
                    break;
                case "cmdInTKA4":
                    InTrangA4();
                    break;
                case "cmdInTKDTSuaDoiBoSung":
                    InToKhaiDienTuSuaDoiBoSung();
                    break;
                case "cmdVanDon":
                    txtSoVanTaiDon_ButtonClick(null, null);
                    break;
                case "cmdHopDong":
                    ViewHopDongTM(false);
                    break;
                case "cmdGiayPhep":
                    txtSoGiayPhep_ButtonClick(null, null);
                    break;
                case "cmdHoaDonThuongMai":
                    txtSoHoaDonThuongMai_ButtonClick(null, null);
                    break;
                case "cmdCO":
                    QuanLyCo("");
                    break;
                case "cmDeNghiChuyenCuaKhau":
                    DeNghiChuyenCuaKhau("");
                    break;
                //
                case "cmdHuyTokhaiDaDuyet":
                    HuyToKhaiDaDuyet();
                    break;
                case "cmdSuaToKhaiDaDuyet":
                    SuaToKhaiDaDuyet();
                    break;
                case "cmdFileAnh":
                    AddChungTuAnh(false);
                    break;
                case "cmdKetQuaXuLy":
                    XemKetQuaXuLy();
                    break;
                case "cmdChungTuDangAnh":
                    AddChungTuAnh(false);
                    break;

                //Chung tu bo sung
                case "cmdVanDonBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoVanTaiDon_ButtonClick("1", null);
                    break;
                case "cmdHoaDonThuongMaiBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                    break;
                case "cmdCOBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.QuanLyCo("1");
                    break;
                case "cmdHopDongBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    //this.txtSoHopDong_ButtonClick("1", null);
                    this.ViewHopDongTM(true);
                    break;
                case "cmdGiayPhepBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoGiayPhep_ButtonClick("1", null);
                    break;
                case "cmdChuyenCuaKhauBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.DeNghiChuyenCuaKhau("1");
                    break;
                case "cmdChungTuDangAnhBS":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.AddChungTuAnh(true);
                    break;

            }
        }
        #endregion

        //datlmq bo sung ham KiemTraTrangThaiToKhai 07/09/2010
        private void checkTrangThaiXuLyTK()
        {
            NoiDungChinhSuaTKForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            //Nếu là tờ khai sửa đã được duyệt
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa đang chờ duyệt
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa mới
            else
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        ndDieuChinhTK.Update();
                    }
                }
            }
        }

        //datlmq update InToKhaiDienTuSuaDoiBoSung 30/08/2010
        private void InToKhaiDienTuSuaDoiBoSung()
        {
            InToKhaiSuaDoiBoSungForm frmInToKhaiSuaDoiBoSung = new InToKhaiSuaDoiBoSungForm();
            InToKhaiSuaDoiBoSungForm.TKMD = TKMD;
            frmInToKhaiSuaDoiBoSung.ShowDialog();
        }

        private void HuyToKhaiDaDuyet()
        {
            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + TKMD.SoToKhai.ToString();
            msg += "\n----------------------";
            //msg += "\nCó " + TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    //Updated by Hungtq 28/03/2012.
                    //MsgSend sendXML = new MsgSend();
                    //sendXML.LoaiHS = "TK";
                    //sendXML.master_id = TKMD.ID;
                    //sendXML.Load();
                    //if (sendXML != null)
                    //    sendXML.Delete();

                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = TKMD;
                    f.ShowDialog();
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = TKMD;
                f.ShowDialog();
            }
        }

        #region Bổ sung Khai báo sửa tờ khai
        private void NhanPhanHoiSuaTK()
        {
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                for (int i = 0; i < 3; i++)
                {
                    LayPhanHoiSuaTK(password);
                }
            }
            catch (Exception ex)
            {
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            //if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                            ShowMessage("Có lỗi trong nhận thông tin: " + msg[0], false, true, ex.ToString());
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiSuaTK(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi(MessgaseType.ThongTin, MessgaseFunction.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    //LayPhanHoiSuaTK(pass);
                    return;
                }

                //}
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Tờ khai bị từ chối: " + Company.GC.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.GC.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.GC.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.GC.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void SuaToKhaiDaDuyet()
        {
            //DATLMQ bổ sung Request thông tin trả về từ Hải quan trước khi chuyển sang trạng thái Sửa TK 25/05/2011
            if ((!TKMD.GUIDSTR.Equals("")) && TKMD.SoTiepNhan > 0)
                NhanPhanHoiSuaTK();

            string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
            msg += "\n\nSố tờ khai: " + TKMD.SoToKhai.ToString();
            msg += "\n----------------------";
            //msg += "\nCó " + TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {
                //Updated by Hungtq 28/03/2012.
                //MsgSend sendXML = new MsgSend();
                //sendXML.LoaiHS = "TK";
                //sendXML.master_id = TKMD.ID;
                //sendXML.Load();
                //if (sendXML != null)
                //    sendXML.Delete();

                long id = TKMD.ID;
                TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                //Cap nhat trang thai cho to khai
                TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;

                TKMD.Update();

                //Cap nhat trang thai sua to khai
                Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                kqxl.ItemID = TKMD.ID;
                kqxl.ReferenceID = new Guid(TKMD.GUIDSTR);
                kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                kqxl.LoaiThongDiep = "Chuyển trạng thái sửa tờ khai"; // Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                kqxl.NoiDung = "";
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();

            }
        }

        private void XemKetQuaXuLy()
        {
            //KetQuaXuLyForm form = new KetQuaXuLyForm();
            //form.ItemID = TKMD.ID;
            //form.ShowDialog(this);            
            Globals.ShowKetQuaXuLyBoSung(TKMD.GUIDSTR);

        }

        private void AddChungTuAnh(bool isKhaiBoSung)
        {
            var ctForm = new ListChungTuKemForm { TKMD = TKMD };
            ctForm.isKhaiBoSung = isKhaiBoSung;
            ctForm.ShowDialog();
        }

        //private void HuyToKhaiMd()
        //{
        //    if (ShowMessage("Tờ khai đã được cấp số, bạn có chắc chắn muốn hủy không ?", true) == "Yes")
        //    {
        //        var f = new HuyToKhaiForm { TKMD = TKMD };
        //        f.ShowDialog();
        //    }
        //}
        //private void SuaToKhaiDaDuyet()
        //{
        //    if (ShowMessage("Tờ khai đã được cấp số, bạn có chắc chắn muốn sửa không ?", true) == "Yes")
        //    {
        //        var f = new SuaToKhaiForm { TKMD = TKMD };
        //        f.ShowDialog();
        //    }

        //}

        private void DeNghiChuyenCuaKhau(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            var f = new ListDeNghiChuyenCuaKhauTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();
        }

        private void QuanLyCo(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            var f = new ListCOTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();
        }

        private void InTrangA4()
        {
            if (TKMD.HMDCollection.Count == 0) return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    //ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    var f = new ReportViewTKNTQDTForm { TKMD = TKMD };
                    f.ShowDialog();
                    break;

                case "X":
                    //ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    var f1 = new ReportViewTKXTQDTForm { TKMD = TKMD };
                    f1.ShowDialog();
                    break;
            }
        }

        private void XuatExcelHang()
        {
            var sfNpl = new SaveFileDialog
                            {
                                RestoreDirectory = true,
                                InitialDirectory = Application.StartupPath,
                                Filter = "Excel files| *.xls"
                            };
            sfNpl.ShowDialog();
            if (sfNpl.FileName != "")
            {
                var str = sfNpl.OpenFile();
                gridEXExporterHangToKhai.Export(str);
                str.Close();
                //if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                if (Globals.ShowMessageTQDT("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNpl.FileName);
                }
            }
        }

        #region Phân bổ tờ khai
        public void ThucHienPhanBoToKhai()
        {
            try
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    //showMsg("MSG_ALL03");
                    //MLMessages("Tờ khai này đã được phân bổ.", "MSG_ALL03", "", false);
                    Globals.ShowMessageTQDT("Tờ khai này đã được phân bổ.", false);
                    return;
                }
                if (TKMD.LoaiHangHoa == "S")
                {
                    PhanBoToKhaiXuat.PhanBoChoToKhaiXuatGC(TKMD, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc);
                    //showMsg("MSG_ALL04");
                    //MLMessages("Thực hiện phân bổ thành công.","MSG_ALL04","", false);
                    Globals.ShowMessageTQDT("Thực hiện phân bổ thành công.", false);
                }
                else if (TKMD.LoaiHangHoa == "N")
                {

                    var f = new SelectNPLTaiXuatForm { TKMD = TKMD };
                    TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                    foreach (var hmd in TKMD.HMDCollection)
                    {
                        var pbToKhaiTaiXuat = new PhanBoToKhaiXuat
                                                  {
                                                      ID_TKMD = TKMD.ID,
                                                      MaDoanhNghiep = GlobalSettings.MA_DON_VI,
                                                      MaSP = hmd.MaPhu,
                                                      SoLuongXuat = hmd.SoLuong
                                                  };
                        TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                    }
                    f.pbToKhaiXuatCollection = TKMD.PhanBoToKhaiXuatCollection;
                    f.ShowDialog();
                    if (f.isPhanBo)
                        //showMsg("MSG_ALL04");
                        Globals.ShowMessageTQDT("Thực hiện phân bổ thành công.", false);
                }
                // Message("MSG_ALL04", "", false);
                //ShowMessage("Thực hiện phân bổ thành công.", false);
            }
            catch (Exception ex)
            {
                //showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Có lỗi khi phân bổ : "+ex.Message, false);
                Globals.ShowMessageTQDT("Có lỗi khi phân bổ : " + ex.Message, false);
            }
        }
        #endregion

        private void ShowThemNhieuHang()
        {
            AddDataToToKhai();
            if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
            {
                if (txtSoHopDong.Text == "")
                {
                    epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "Contract no. must be filled"));
                    epError.SetIconPadding(txtSoHopDong, -8);
                    return;
                }
                epError.Clear();
            }
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;
            var f = new KDT.ToKhai.SelectNhieuHangMauDichForm { HD = Hdgc, HMDCollection = new HangMauDichCollection() };
            foreach (var hmd in TKMD.HMDCollection)
            {
                var hmdNew = hmd.Copy();
                f.HMDCollection.Add(hmdNew);
            }
            f.TKMD = TKMD;
            f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            f.ShowDialog();
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void ShowCanDoiNhapXuat()
        {
            var f = new CanDoiNhapXuatForm { TKMD = TKMD, tableCanDoi = GetTableCanDoi() };
            if (f.tableCanDoi == null)
                return;
            f.ShowDialog();
        }
        private void GetFileDinhKem()
        {
            var f = new SendMailChungTuForm { TKMD = TKMD };
            f.ShowDialog();
        }

        #region Bổ sung Hủy khai báo Tờ khai: DATLMQ 26/05/2011
        private void LayPhanHoiHuyKhaiBao(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    return;
                }
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                        msg += " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string msg = "Tờ khai sửa chờ duyệt:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                        msg += " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI

                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.ActionStatus == -1)
                            {
                                this.ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa nhận được phản hồi từ hệ thống Hải quan. ", false);
                            }
                            else
                            {
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                                {
                                    if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                                    else
                                        this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                                }
                                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                                {
                                    string msg = "Phản hồi từ hải quan: " + Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                    if (TKMD.ActionStatus != (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                    {
                                        TKMD.SoTiepNhan = 0;
                                        TKMD.SoToKhai = 0;
                                        TKMD.NgayTiepNhan = new DateTime(1900, 1, 1);
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                        TKMD.PhanLuong = "";
                                        TKMD.HUONGDAN = "";
                                    }
                                    else
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                    TKMD.Update();
                                    isDeny = true;
                                    this.ShowMessageTQDT(msg, false);
                                }
                                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                                {
                                    string msg = Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                    this.ShowMessageTQDT(msg, false);
                                }
                                else
                                {
                                    this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                                }

                            }
                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void HuyToKhai()
        {
            ToKhaiMauDich tkmd = TKMD;
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();

            if (tkmd != null)
            {
                tkmd = TKMD;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.Load();
            }
            else
            {
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
            this.Cursor = Cursors.WaitCursor;
            //DATLMQ bổ sung kiểm tra TK đã được HQ duyệt chưa 25/05/2011
            for (int i = 0; i < 2; i++)
            {
                LayPhanHoiHuyKhaiBao(password);
            }
            if (TKMD.SoToKhai > 0)
            {
                ShowMessage("Tờ khai đã có số tờ khai. Không thể hủy khai báo", false);
                return;
            }
            else if (ToKhaiMauDich.tuChoi)
            {
                ShowMessage("Tờ khai đã bị từ chối. Không thể hủy khai báo", false);
                isDeny = false;
                return;
            }
            try
            {
                xmlCurrent = tkmd.WSCancelXMLGC(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                #region Nhận thông tin phản hồi cũ
                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(password, xmlCurrent);


                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }

                string mess = "";
                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }

                    mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();

                #endregion
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            //showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //showMsg("MSG_WRN13", ex.Message);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);

                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", "", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now);
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void NhanDuLieuToKhai()
        {
            var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
            string password = "";
            if (sendXml.Load())
            {
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN01", "", false);
                //showMsg("MSG_SEN01");
                Globals.ShowMessageTQDT("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                var wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                Cursor = Cursors.WaitCursor;
                _xmlCurrent = TKMD.WSRequestXMLGC(password);
                sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID, msg = _xmlCurrent, func = 2 };
                _xmlCurrent = "";

                sendXml.InsertUpdate();

                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            //showMsg("MSG_WRN12");
                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            //showMsg("MSG_2702016", msg[0]);
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //showMsg("MSG_WRN13", ex.Message);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private bool ProcessMessage(string sfmtMessage)
        {
            //LanNT: pattern format header is ERROR:Content=
            string pattern = @"(?<error>\w+):Content=(?<content>(\w|\.)+)";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern);
            string msg = string.Empty;
            string msgType = string.Empty;
            foreach (Match item in regex.Matches(sfmtMessage))
            {
                msg += item.Groups["content"].Value;
                msgType = item.Groups["error"].Value;
            }

            // Thực hiện kiểm tra. 

            if (msgType.Equals("NoInfo"))
            {
                string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                if (kq == "Yes")
                {
                    return true;
                }

            }
            else if (msgType.Equals("TuChoi"))
            {
                msg = "Hải quan từ chối với thông báo:\r\n " + msg;
                ShowMessage(msg, false);
            }
            else if (msgType.Equals("ERROR"))
            {
                msg = "Lỗi từ hệ thống hải quan trả về :\r\n" + msg;
                ShowMessage(msg, false);
            }
            else
            {
                string kq = MLMessages(msg + "\r\nnBạn có muốn tiếp tục lấy phân luồng thông tin không?", "MSG_STN02", "", true);
                if (kq == "Yes")
                {
                    return true;
                }
            }
            return false;
        }

        #region Nhận phân luồng tờ khai
        public bool isDisplay = true;
        private void LayPhanHoiPhanLuong(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                sendXML.func = 2;
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);
                this.Cursor = Cursors.Default;

                //if (ProcessMessage(xmlCurrent))
                //{
                //    this.Refresh();
                //    LayPhanHoiPhanLuong(pass);
                //}
                #region Tam thoi
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                string mess = "";



                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                        mess = "Khai báo thông tin sửa tờ khai thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    else
                        mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        InAnDinhThue();
                    }
                }


                #endregion
                //Không dùng LanNT
                //Lý do đã cập nhật thông tin 1 lần. từ lấy phản hồi
                //Không được cập nhật lần 2 khi phân tích message thông báo;
                //Lấy thông báo chưa chính xác
                //if (isDisplay)
                //showThongBao();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void InAnDinhThue()
        {
            Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG);
        }

        private void InAnDinhThue(long tkmdID, string guidstr)
        {
            AnDinhThue anDinhThue = new AnDinhThue();
            List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
            if (dsADT.Count > 0)
            {
                anDinhThue = dsADT[0];
                anDinhThue.LoadChiTiet();

                Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue);
            }
        }

        private void showThongBao()
        {
            string mess = "";
            try
            {
                isDisplay = true;
                //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    string msg = "Tờ khai sửa bị từ chối:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                    msg += " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                    TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                    TKMD.Update();
                    this.ShowMessageTQDT(msg, false);
                }
                else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    string msg = "Tờ khai sửa chờ duyệt:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                    msg += "Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                    this.ShowMessageTQDT(msg, false);
                }
                else
                {// LÀ TỜ KHAI MỚI

                    if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                    {
                        mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + "Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessageTQDT(mess, false);
                        lblPhanLuong.Text = "Luồng Xanh";
                        isDisplay = false;
                    }
                    else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                    {
                        mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + "Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessageTQDT(mess, false);
                        lblPhanLuong.Text = "Luồng Vàng";
                        isDisplay = false;
                    }
                    else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                    {
                        mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessageTQDT(mess, false);
                        lblPhanLuong.Text = "Luồng Đỏ";
                        isDisplay = false;
                    }
                    else
                    {
                        if (TKMD.ActionStatus == -1)
                        {
                            this.ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa nhận được phản hồi từ hệ thống Hải quan. ", false);
                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                    this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                                else
                                    this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Phản hồi từ hải quan: " + Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                if (TKMD.ActionStatus != (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                {
                                    TKMD.SoTiepNhan = 0;
                                    TKMD.SoToKhai = 0;
                                    TKMD.NgayTiepNhan = new DateTime(1900, 1, 1);
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                    TKMD.PhanLuong = "";
                                    TKMD.HUONGDAN = "";
                                }
                                else
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
            }
            catch (Exception exx) { ShowMessage(exx.Message, false); }

        }

        private void NhanPhanLuongToKhai()
        {
            var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
            string password = "";
            if (sendXml.Load())
            {
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN01", "", false);
                //showMsg("MSG_SEN01");
                //Globals.ShowMessageTQDT("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                //XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                Cursor = Cursors.WaitCursor;
                LayPhanHoiPhanLuong(password);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {

                            //showMsg("MSG_WRN12");
                            Globals.ShowMessageTQDT("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", false);
                            return;
                        }
                        else
                        {
                            //showMsg("MSG_2702016", msg[0]);
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //showMsg("MSG_WRN13", ex.Message);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        #endregion Phan Luông

        private void AddChungTu()
        {
            ChungTuForm f = new ChungTuForm();
            f.OpenType = OpenFormType.Insert;
            f.collection = TKMD.ChungTuTKCollection;
            f.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
            f.ShowDialog();
            TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            setCommandStatus();

        }

        private void Send()
        {
            if (TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin trước khi gửi!", "MSG_SEN14", "", false);
                return;
            }
            string password = "";
            Cursor = Cursors.Default;
            var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
            if (sendXml.Load())
            {
                ShowMessageTQDT("Hệ thống ECS thông báo", "Tờ khai đã gửi  tới hệ thống hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin.", false);

                //Hien thi nut Nhan du lieu
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                return;
            }
            var wsForm = new WSForm();
            try
            {

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                Cursor = Cursors.WaitCursor;
                if (MainForm.versionHD != 2)
                {
                    try
                    {
                        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                        if (st != "")
                        {
                            ShowMessage(st, false);
                            Cursor = Cursors.Default;
                            return;
                        }

                    }
                    catch { }
                }

                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    _xmlCurrent = TKMD.WSKhaiBaoToKhaiSua(password, GlobalSettings.MaMID);
                }
                else
                {
                    if (TKMD.MaLoaiHinh.StartsWith("NGC") || TKMD.MaLoaiHinh.StartsWith("NTA"))
                    {
                        _xmlCurrent = TKMD.WSSendXMLNHAP(password);
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("XGC"))
                    {
                        _xmlCurrent = TKMD.WSSendXMLXuat(password, GlobalSettings.MaMID);
                    }
                }
                Cursor = Cursors.Default;

                sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID, msg = _xmlCurrent, func = 1 };

                _xmlCurrent = "";

                sendXml.InsertUpdate();
                dgList.AllowDelete = InheritableBoolean.False;
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            ShowMessage("Không kết nối được với Hệ thống Hải quan.\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                            return;
                        }
                        else
                        {
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() == "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void SoTiepNhanDTTQDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    //MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
                    //  showMsg("MSG_SEN01");
                    //// return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                Cursor = Cursors.WaitCursor;
                {
                    _xmlCurrent = TKMD.LayPhanHoiGC(password, sendXML.msg);
                }
                Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (_xmlCurrent != "")
                {
                    doc.LoadXml(_xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        string kq = Globals.ShowMessageTQDT("Chưa nhận được phản hồi từ hệ thống hải quan", true);
                        if (kq == "Yes")
                        {
                            Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            lblTrangThai.Text = "Chờ xác nhận";
                            // if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait for confirmation";
                        }
                        return;
                    }
                }
                if (sendXML.func == 1)
                {
                    //showMsg("MSG_SEN02", TKMD.SoTiepNhan);
                    ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Đăng ký thành công ! \r\n Số TNĐKĐT : " + TKMD.SoTiepNhan.ToString() + "\r\n Năm đăng ký : " + TKMD.NamDK.ToString(), false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = "Chờ duyệt chính thức";
                    // if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Wait for approval"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    //MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);
                    //showMsg("MSG_2702024");
                    Globals.ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not declared yet"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {

                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }

                        //showMsg("MSG_SEN11", new string[] { TKMD.SoToKhai.ToString(), mess });
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN11", TKMD.SoToKhai.ToString(), false);
                        Globals.ShowMessageTQDT("Số tờ khai : " + TKMD.SoToKhai.ToString() + mess, false);

                        lblTrangThai.Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }

                        Globals.ShowMessageTQDT("Hải quan chưa duyệt danh sách này", false);
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa duyệt danh sách này", "MSG_SEN04", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        Globals.ShowMessageTQDT("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        //showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN05", "", false);
                        lblTrangThai.Text = "Hải quan không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not approved"; }
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}

                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            //showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                            //showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message,false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            var doc = new XmlDocument();
            XmlNode node = null;
            var wsForm = new WSForm();
            var sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    // MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_STN01", "", false);
                    //showMsg("MSG_SEN01");
                    Globals.ShowMessageTQDT("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                Cursor = Cursors.WaitCursor;
                {
                    _xmlCurrent = TKMD.LayPhanHoiGC(password, sendXML.msg);
                }
                Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (_xmlCurrent != "")
                {
                    doc.LoadXml(_xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        string kq = Globals.ShowMessageTQDT("Chưa nhận được phản hồi từ hệ thống hải quan. \nBạn có muốn tiếp tục lấy phản hồi không?.", true);
                        if (kq == "Yes")
                        {
                            Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            lblTrangThai.Text = "Chờ xác nhận";
                            // if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait for confirmation";
                        }
                        return;
                    }
                }
                if (sendXML.func == 1)
                {
                    //showMsg("MSG_SEN02", TKMD.SoTiepNhan);
                    ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Khai báo thành công ! \r\n Số tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n Năm đăng ký : " + TKMD.NamDK.ToString(), false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = "Chờ duyệt chính thức";
                    // if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Wait for approval"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    Globals.ShowMessageTQDT("Hủy khai báo thành công", false);
                    //MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);
                    //showMsg("MSG_2702024");
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not declared yet"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_SEN11", new string[] { TKMD.SoToKhai.ToString(), mess });

                        Globals.ShowMessageTQDT("Số tờ khai : " + TKMD.SoToKhai.ToString() + mess, false);
                        lblTrangThai.Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        if (string.IsNullOrEmpty(mess))
                            mess = "Hải quan chưa duyệt tờ khai này";
                        Globals.ShowMessageTQDT(mess, false);
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa duyệt danh sách này", "MSG_SEN04", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        Globals.ShowMessageTQDT("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        //showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN05", "", false);
                        lblTrangThai.Text = "Hải quan không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not approved"; }
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}

                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            //showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            //showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message,false);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void sendMailChungTu()
        {
            Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    //showMsg("MSG_MAL01");
                    Globals.ShowMessageTQDT("Chưa cấu hình mail của doanh nghiệp", false);

                if (GlobalSettings.MailHaiQuan == "")
                    //showMsg("MSG_MAL02");
                    Globals.ShowMessageTQDT("Chưa cấu hình mail của hải quan tiếp nhận", false);

                return;
            }
            var mail = new MailMessage
                           {
                               From = new MailAddress(GlobalSettings.MailDoanhNghiep, GlobalSettings.TEN_DON_VI, System.Text.Encoding.UTF8)
                           };
            string[] mailArray = GlobalSettings.MailHaiQuan.Split(';');
            for (int k = 0; k < mailArray.Length; ++k)
                if (mailArray[k] != "")
                    mail.To.Add(mailArray[k]);
            mail.Subject = "Khai báo điện tử - " + GlobalSettings.MA_DON_VI + "- Tờ khai có số tiếp nhận : " + TKMD.SoTiepNhan.ToString();
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Hải quan điện tử - Duyệt tờ khai - Chứng từ ";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            mail.Priority = MailPriority.High;

            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                try
                {
                    if (ct.FileUpLoad != "")
                    {
                        //FileStream file = File.Open(ct.FileUpLoad, FileMode.Open);
                        var att = new Attachment(ct.FileUpLoad);
                        mail.Attachments.Add(att);
                        mail.Body += "\n" + ct.TenChungTu + " ";
                        if (ct.SoBanChinh > 0)
                            mail.Body += "Số bản chính : " + ct.SoBanChinh;
                        if (ct.SoBanSao > 0)
                            mail.Body += "Số bản sao : " + ct.SoBanSao;
                        ++i;
                    }
                }
                catch
                {
                }
            }
            try
            {
                if (i > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    var smtp = new SmtpClient
                                   {
                                       Port = 587,
                                       Host = "smtp.gmail.com",
                                       EnableSsl = true,
                                       Credentials =
                                           new System.Net.NetworkCredential("haiquandientu@gmail.com",
                                                                            "haiquandientudanang")
                                   };
                    smtp.Send(mail);
                    //showMsg("MSG_MAL05");
                    //MLMessages("Gửi mail thành công.","MSG_MAL05","", false);
                    Globals.ShowMessageTQDT("Gửi mail thành công.", false);
                    Cursor = Cursors.Default;
                    Close();
                }
                Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                //showMsg("MSG_MAL06");
                Globals.ShowMessageTQDT("Không gửi chứng từ tới hải quan được.", false);
            }
        }

        //HUNGTQ, Update 25/05/2010
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    return;

                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //xmlCurrent = TKMD.LayPhanHoi(pass, sendXML.msg);
                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                string mess = "";



                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                        mess = "Khai báo thông tin sửa tờ khai thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    else
                        mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;

                        //Updated by Hungtq 28/03/2012.
                        sendXML.Delete();
                    }
                }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                            TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                            TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                            TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                            TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG);
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            //DATLMQ comment ngày 29/03/2011
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + msg[0], false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false);
                        //DATLMQ comment ngày 29/03/2011
                        //MLMessages("Xảy ra lỗi : " + ex.Message.ToString(), "MSG_SEN20", "", false);
                        ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


            /* 
             var sendXml = new MsgSend();
             var doc = new XmlDocument();
             XmlNode node = null;
             try
             {
                 if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                     return;


                 sendXml.LoaiHS = "TK";
                 sendXml.master_id = TKMD.ID;
                 sendXml.Load();
                 Cursor = Cursors.WaitCursor;

                 _xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXml.msg);

                 Cursor = Cursors.Default;
                 // Thực hiện kiểm tra.                  
                 // Thực hiện kiểm tra.  
                 if (xmlCurrent != "")
                 {
                     string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                     if (kq == "Yes")
                     {
                         this.Refresh();
                         LayPhanHoiPhanLuong(pass);
                     }
                     return;
                 }
                 string mess = "";
                 if (sendXml.func == 1)
                 {
                     if (TKMD.SoTiepNhan == 0)
                     {
                         MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                         cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                         dgList.AllowDelete = InheritableBoolean.False;
                         return;
                     }

                     mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                     ShowMessageTQDT(mess, false);
                     txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                     lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");

                 }
                 //Nhận thông tin hủy tờ khai
                 else if (sendXml.func == 3)
                 {
                     ShowMessageTQDT("Hủy tờ khai thành công !", false);
                     txtSoTiepNhan.Text = "";
                     lblTrangThai.Text = "Chưa khai báo";
                     if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Not declared yet";
                 }
                 else if (sendXml.func == 2)
                 {
                     if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                     {
                         ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Tờ khai đã được duyệt chính thức\r\n Số tờ khai : " + TKMD.SoToKhai.ToString(), false);
                         if (GlobalSettings.NGON_NGU == "0")
                         {
                             lblTrangThai.Text = "Đã duyệt";
                         }
                         else
                         {
                             lblTrangThai.Text = "Approved";
                         }
                         txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                     }
                     else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                     {
                         ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Tờ khai chưa được duyệt", false);

                         cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                         dgList.AllowDelete = InheritableBoolean.False;
                     }
                     else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                     {
                         ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Tờ khai không phê duyệt duyệt", false);

                         if (GlobalSettings.NGON_NGU == "0")
                         {
                             lblTrangThai.Text = "Hải quan không phê duyệt";
                         }
                         else
                         {
                             lblTrangThai.Text = "Customs not yet Approved";
                         }

                         cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                         dgList.AllowDelete = InheritableBoolean.False;
                     }
                 }
                 //xoa thông tin msg nay trong database
                 sendXml.Delete();
                 setCommandStatus();
             }
             catch (Exception ex)
             {
                 Cursor = Cursors.Default;
                 //if (loaiWS == "1")
                 {
                     #region FPTService
                     string[] msg = ex.Message.Split('|');
                     if (msg.Length == 2)
                     {
                         if (msg[1] == "DOTNET_LEVEL")
                         {
                             //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                             //{
                             //    HangDoi hd = new HangDoi();
                             //    hd.ID = TKMD.ID;
                             //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                             //    hd.TrangThai = TKMD.TrangThaiXuLy;
                             //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                             //    hd.PassWord = pass;
                             //    MainForm.AddToQueueForm(hd);
                             //    MainForm.ShowQueueForm();
                             //}
                             showMsg("MSG_WRN12");
                             //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                             return;
                         }
                         else
                         {
                             showMsg("MSG_2702016", msg[0]);
                             //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                             sendXml.Delete();
                             setCommandStatus();
                         }
                     }
                     else
                     {
                         if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                         {
                             showMsg("MSG_WRN13", ex.Message);
                             //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                             sendXml.Delete();
                             setCommandStatus();
                         }
                         else
                         {
                             GlobalSettings.PassWordDT = "";
                             showMsg("MSG_2702004", ex.Message);
                             //ShowMessage(ex.Message, false);
                         }
                     }
                     #endregion FPTService
                 }

                 StreamWriter write = File.AppendText("Error.txt");
                 write.WriteLine("--------------------------------");
                 write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                 write.WriteLine(ex.StackTrace);
                 write.WriteLine("Lỗi là : ");
                 write.WriteLine(ex.Message);
                 write.WriteLine("--------------------------------");
                 write.Flush();
                 write.Close();
             }
             finally
             {
                 Cursor = Cursors.Default;
             }

             */
        }
        //------------------------------------------------k-----------------------------------------
        private void ViewHopDongTM(bool loaikhai)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHopDongTKMDForm f = new ListHopDongTKMDForm();
            if (loaikhai == true)
            {
                f.isKhaiBoSung = true;
            }
            f.TKMD = TKMD;
            f.ShowDialog();

            //if (f.TKMD.SoHopDong != "")
            //{
            //    txtSoHopDong.Text = f.TKMD.SoHopDong;
            //    ccNgayHopDong.Text = f.TKMD.NgayHopDong.ToShortDateString();
            //    ccNgayHHHopDong.Text = f.TKMD.NgayHetHanHopDong.ToShortDateString();
            //}
        }

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.TrangThaiXuLy == 0)
            {
                Globals.ShowMessageTQDT("Tờ khai đã dược khai báo tới hải quan nên không chỉnh sửa được.Hãy hủy khai báo trước khi chỉnh sửa dữ liệu.", false);
                //showMsg("MSG_0203066");
                return;
            }
            if (TKMD.TrangThaiXuLy == 1)
            {
                //showMsg("MSG_STN10");
                Globals.ShowMessageTQDT("Tờ khai đã dược duyệt nên không thể chỉnh sửa dữ liệu !", false);
                return;
            }
            switch (NhomLoaiHinh)
            {
                case "NGC":
                    var f = new HopDongManageForm { IsBrowseForm = true };
                    if (sender != null)
                    {
                        f.isKhaiBoSung = true;
                    }
                    f.ShowDialog();
                    if (f.HopDongSelected.ID > 0)
                    {
                        if (Hdgc.ID != f.HopDongSelected.ID)
                        {
                            if (TKMD.HMDCollection.Count > 0)
                            {
                                //if (showMsg("MSG_SAV05", true) == "Yes")
                                //if (ShowMessage("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                if (Globals.ShowMessageTQDT("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                {
                                    foreach (Company.GC.BLL.KDT.HangMauDich HangDelete in TKMD.HMDCollection)
                                    {
                                        if (HangDelete.ID > 0)
                                            HangDelete.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                                    }
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            Hdgc = f.HopDongSelected;
                            txtSoHopDong.Text = f.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Text = f.HopDongSelected.NgayKy.ToShortDateString();
                            ccNgayHHHopDong.Text = f.HopDongSelected.NgayHetHan.ToShortDateString();
                            txtTenDonViDoiTac.Text = f.HopDongSelected.DonViDoiTac + "\r\n" + f.HopDongSelected.DiaChiDoiTac;
                            TKMD.IDHopDong = Hdgc.ID;
                        }
                    }
                    break;
                case "XGC":
                    var f1 = new HopDongManageForm { IsBrowseForm = true };
                    if (sender != null)
                    {
                        f1.isKhaiBoSung = true;
                    }
                    f1.ShowDialog();
                    if (f1.HopDongSelected.SoHopDong != "")
                    {
                        if (Hdgc.ID != f1.HopDongSelected.ID)
                        {
                            if (TKMD.HMDCollection.Count > 0)
                            {
                                //if (showMsg("MSG_SAV05", true) == "Yes")
                                if (Globals.ShowMessageTQDT("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                {
                                    foreach (Company.GC.BLL.KDT.HangMauDich HangDelete in TKMD.HMDCollection)
                                    {
                                        if (HangDelete.ID > 0)
                                            HangDelete.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                                    }
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            Hdgc = f1.HopDongSelected;
                            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Text = f1.HopDongSelected.NgayKy.ToShortDateString();
                            ccNgayHHHopDong.Text = f1.HopDongSelected.NgayHetHan.ToShortDateString();
                            TKMD.IDHopDong = Hdgc.ID;
                            txtTenDonViDoiTac.Text = f1.HopDongSelected.DonViDoiTac + "\r\n" + f1.HopDongSelected.DiaChiDoiTac;
                        }
                    }
                    break;
            }
        }

        private void DgListRowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    var f = new HangMauDichEditForm { HMD = TKMD.HMDCollection[i.Position], HD = Hdgc, TKMD = TKMD };
                    soDongHang = i.Position + 1;
                    Hdgc.ID = TKMD.IDHopDong;
                    if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
                    if (radSP.Checked) TKMD.LoaiHangHoa = "S";
                    if (radTB.Checked) TKMD.LoaiHangHoa = "T";
                    f.LoaiHangHoa = TKMD.LoaiHangHoa;
                    f.NhomLoaiHinh = NhomLoaiHinh;
                    f.MaHaiQuan = TKMD.MaHaiQuan;
                    f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
                    TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
                    TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                    TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
                    f.TKMD = TKMD;
                    f.OpenType = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? OpenFormType.Edit : OpenFormType.View;
                    f.ShowDialog();
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.Refresh(); }
                    setCommandStatus();
                }
                break;
            }
        }

        private void DgListLoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }

        private void GridEx1RowDoubleClick(object sender, RowActionEventArgs e)
        {
            var f = new ChungTuForm
                        {
                            OpenType =
                                TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET ? OpenFormType.View : OpenFormType.Edit
                        };
            var ctDetail = new ChungTu
                               {
                                   ID = Convert.ToInt64(e.Row.Cells["ID"].Text),
                                   TenChungTu = e.Row.Cells["TenChungTu"].Text,
                                   SoBanChinh = Convert.ToInt16(e.Row.Cells["SoBanChinh"].Text),
                                   SoBanSao = Convert.ToInt16(e.Row.Cells["SoBanSao"].Text),
                                   STTHang = Convert.ToInt16(e.Row.Cells["STTHang"].Text),
                                   Master_ID = Convert.ToInt64(e.Row.Cells["Master_ID"].Text),
                                   FileUpLoad = e.Row.Cells["FileUpLoad"].Text
                               };
            f.ctDetail = ctDetail;
            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                if (ct.TenChungTu.ToUpper() == ctDetail.TenChungTu.ToUpper())
                {
                    TKMD.ChungTuTKCollection.RemoveAt(i);
                    break;
                }
                ++i;
            }
            f.MaLoaiHinh = TKMD.MaLoaiHinh;
            f.collection = TKMD.ChungTuTKCollection;
            f.ShowDialog();
            TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            setCommandStatus();
        }

        private void GridEx1DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                Globals.ShowMessageTQDT("Tờ khai đã được duyệt không được sửa", false);
                //showMsg("MSG_2702027");
                e.Cancel = true;
                return;
            }
            var ctDetail = new ChungTu { ID = Convert.ToInt64(e.Row.Cells["ID"].Text) };
            if (Globals.ShowMessageTQDT("Bạn có muốn xóa các thông tin đã chọn không ?", true) == "Yes")
            //if (showMsg("MSG_DEL01", true) == "Yes")
            {
                if (ctDetail.ID > 0)
                    ctDetail.Delete();
                TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
                e.Cancel = true;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKMD.ID > 0)
            {
                if (TKMD.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                    {
                        //showMsg("MSG_ALL01");
                        Globals.ShowMessageTQDT("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        e.Cancel = true;
                        return;
                    }
                }


            }
            if (Globals.ShowMessageTQDT("Bạn có muốn xóa các thông tin đã chọn không?", true) == "Yes")
            //if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        var hmd = (Company.GC.BLL.KDT.HangMauDich)i.GetRow().DataRow;

                        //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                        if (HangMauDichForm.checkExistHMD(TKMD.ID, hmd.MaPhu))
                        {
                            ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                            HangMauDichForm.isDeleted = true;
                        }

                        if (hmd.ID > 0)
                        {
                            hmd.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
            setCommandStatus();
        }

        private void editBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                foreach (Company.GC.BLL.KDT.HangMauDich HMD in TKMD.HMDCollection)
                {
                    HMD.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, Hdgc.ID);
                }
            }
            catch { }
            TKMD.HMDCollection.Clear();
            dgList.DataSource = TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtChungTu_ButtonClick(object sender, EventArgs e)
        {
            GiayToForm f = new GiayToForm();
            f.collection = TKMD.ChungTuTKCollection;
            if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();
            TKMD.ChungTuTKCollection = f.collection;
            try
            {
                gridEX1.DataSource = TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }
            txtChungTu.Text = f.GiayTo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ToKhaiMauDichForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool ok = false;
            //if (TKMD.MaLoaiHinh.StartsWith("N"))
            //{
            //    //if (ToKhaiMauDichForm.isTKNhap)
            //    //    ok = true;
            //}
            //else
            //{
            //    //if (ToKhaiMauDichForm.isTKXuat)
            //    //    ok = true;
            //}
            ok = false;
            try
            {
                if (ok)
                {
                    if (Globals.ShowMessageTQDT("Dữ liệu có sự thay đổi. Bạn có muốn cập nhật lại không?", true) == "Yes")
                    //if (showMsg("MSG_0203067", true) == "Yes")
                    {
                        TKMD.SoTiepNhan = TKMD.SoTiepNhan;
                        TKMD.MaHaiQuan = Hdgc.MaHaiQuan;
                        TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                        TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                        TKMD.SoHang = (short)TKMD.HMDCollection.Count;
                        TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

                        // Doanh nghiệp.
                        TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                        // Đơn vị đối tác.
                        TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                        TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                        TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                        // Đại lý TTHQ.
                        TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                        TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                        // Loại hình mậu dịch.
                        TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                        // Giấy phép.
                        if (txtSoGiayPhep.Text.Trim().Length > 0)
                        {
                            TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                            TKMD.NgayGiayPhep = !ccNgayGiayPhep.IsNullDate ? ccNgayGiayPhep.Value : DateTime.Parse("01/01/1900");
                            TKMD.NgayHetHanGiayPhep = !ccNgayHHGiayPhep.IsNullDate ? ccNgayHHGiayPhep.Value : DateTime.Parse("01/01/1900");
                        }
                        else
                        {
                            TKMD.SoGiayPhep = "";
                            TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                            TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                        }

                        // 7. Hợp đồng.
                        if (txtSoHopDong.Text.Trim().Length > 0)
                        {
                            TKMD.SoHopDong = txtSoHopDong.Text;
                            // Ngày HD.
                            TKMD.NgayHopDong = ccNgayHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHopDong.Value;
                            // Ngày hết hạn HD.
                            TKMD.NgayHetHanHopDong = ccNgayHHHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHHHopDong.Value;
                        }
                        else
                        {
                            TKMD.SoHopDong = "";
                            TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                            TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                        }

                        // Hóa đơn thương mại.
                        if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                        {
                            TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                            TKMD.NgayHoaDonThuongMai = ccNgayHDTM.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHDTM.Value;
                        }
                        else
                        {
                            TKMD.SoHoaDonThuongMai = "";
                            TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                        }
                        // Phương tiện vận tải.
                        TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                        TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                        TKMD.NgayDenPTVT = ccNgayDen.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayDen.Value;

                        // Vận tải đơn.
                        if (txtSoVanTaiDon.Text.Trim().Length > 0)
                        {
                            TKMD.SoVanDon = txtSoVanTaiDon.Text;
                            TKMD.NgayVanDon = ccNgayVanTaiDon.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayVanTaiDon.Value;
                        }
                        else
                        {
                            TKMD.SoVanDon = "";
                            TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                        }
                        // Nước.
                        if (NhomLoaiHinh.Substring(0, 1) == "N")
                        {
                            TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                            TKMD.NuocNK_ID = "VN".PadRight(3);
                        }
                        else
                        {
                            TKMD.NuocXK_ID = "VN".PadRight(3);
                            TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                        }

                        // Địa điểm xếp hàng.
                        //if (chkDiaDiemXepHang.Checked)
                        TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                        // else
                        //     TKMD.DiaDiemXepHang = "";

                        // Địa điểm dỡ hàng.
                        TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                        // Điều kiện giao hàng.
                        TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                        // Đồng tiền thanh toán.
                        TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                        // Phương thức thanh toán.
                        TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                        //
                        TKMD.TenChuHang = txtTenChuHang.Text;
                        TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                        // Tổng trị giá khai báo (Chưa tính).

                        TKMD.TongTriGiaKhaiBao = 0;
                        TKMD.TongTriGiaTinhThue = 0;
                        foreach (Company.GC.BLL.KDT.HangMauDich hang in TKMD.HMDCollection)
                        {
                            TKMD.TongTriGiaKhaiBao += hang.TriGiaKB;
                            TKMD.TongTriGiaTinhThue += hang.TriGiaTT;
                        }
                        TKMD.TongTriGiaKhaiBao = 0;
                        //
                        TKMD.TrangThaiXuLy = -1;
                        TKMD.LoaiToKhaiGiaCong = "NPL";
                        //
                        TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                        TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                        // tkmd.NgayGui = DateTime.Parse("01/01/1900");
                        if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
                        if (radSP.Checked) TKMD.LoaiHangHoa = "S";
                        if (radTB.Checked) TKMD.LoaiHangHoa = "T";

                        TKMD.GiayTo = txtChungTu.Text;

                        TKMD.MaMid = txtMaMid.Text.Trim();
                        TKMD.InsertUpdateFull();
                    }
                }
                //if (TKMD.MaLoaiHinh.StartsWith("N"))
                //    isTKNhap = false;
                //else
                //    isTKXuat = false;
            }
            catch (Exception ex)
            {
                //if (showMsg("MSG_2702028", ex.Message, true) != "Yes")
                if (Globals.ShowMessageTQDT("Dữ liệu cập nhật có lỗi :" + ex.Message + ". Bạn có muốn thoát không?", true) != "Yes")
                    e.Cancel = true;
                else
                {
                    //if (TKMD.MaLoaiHinh.StartsWith("N"))
                    //    isTKNhap = false;
                    //else
                    //    isTKXuat = false;
                }
            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtSoVanTaiDon_ButtonClick(object sender, EventArgs e)
        {
            VanTaiDonForm f = new VanTaiDonForm();
            f.TKMD = TKMD;
            f.ShowDialog();
            if (f.TKMD.VanTaiDon != null && f.TKMD.VanTaiDon.SoVanDon.Trim() != "")
            {
                txtSoHieuPTVT.Text = TKMD.SoHieuPTVT;
                if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                    ccNgayDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
                else
                    ccNgayDen.Text = "";

                // ĐKGH.
                cbDKGH.SelectedValue = TKMD.DKGH_ID;

                // Địa điểm dỡ hàng.
                ctrCuaKhau.Ma = TKMD.CuaKhau_ID;

                // Vận tải đơn.
                txtSoVanTaiDon.Text = TKMD.SoVanDon;
                ccNgayVanTaiDon.Text = TKMD.NgayVanDon.Year > 1900 ? TKMD.NgayVanDon.ToShortDateString() : "";
                // if ( TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

                // Địa điểm xếp hàng.
                txtDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;
                //if ( TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

                // Container 20.
                txtSoContainer20.Value = TKMD.SoContainer20;

                // Container 40.
                txtSoContainer40.Value = TKMD.SoContainer40;
            }
            else if (TKMD.VanTaiDon == null)
            {
                TKMD.SoVanDon = string.Empty;
                TKMD.NgayVanDon = new DateTime(1900, 01, 01);
                if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
                {
                    TKMD.PTVT_ID = string.Empty;
                    TKMD.QuocTichPTVT_ID = string.Empty;
                    TKMD.SoHieuPTVT = string.Empty;
                    this.txtSoHieuPTVT.Text = string.Empty;
                }
                this.ccNgayVanTaiDon.Text = string.Empty;
                this.txtSoVanTaiDon.Text = string.Empty;
            }
        }

        private void txtSoGiayPhep_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDForm f = new ListGiayPhepTKMDForm();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }
            else
                f.isKhaiBoSung = false;
            f.TKMD = TKMD;
            f.ShowDialog();

            //if (f.TKMD.SoGiayPhep != "")
            //{
            //    if (Globals.ShowMessage("Bạn có muốn cập nhật lại thông tin 'Số Giấy phép' không?.", true) == "Yes")
            //    {
            //        txtSoGiayPhep.Text = f.TKMD.SoGiayPhep;
            //        ccNgayGiayPhep.Text = f.TKMD.NgayGiayPhep.ToShortDateString();
            //        ccNgayHHGiayPhep.Text = f.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            //    }
            //}

        }

        private void txtSoHoaDonThuongMai_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonThuongMaiTKMDForm f = new ListHoaDonThuongMaiTKMDForm();
            if (sender != null)
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.TKMD.SoHoaDonThuongMai != "")
            {
                txtSoHoaDonThuongMai.Text = f.TKMD.SoHoaDonThuongMai;
                ccNgayHDTM.Text = f.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            }
        }

        private void txtTenDonViDoiTac_Leave(object sender, EventArgs e)
        {
            if (!ValidateNguoiNK())
                return;
        }

        private void btnNoiDungChinhSua_Click(object sender, EventArgs e)
        {
            NoiDungChinhSuaTKForm noidungchinhsuatk = new NoiDungChinhSuaTKForm();
            noidungchinhsuatk.TKMD = this.TKMD;
            noidungchinhsuatk.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
            noidungchinhsuatk.ShowDialog();
        }
    }
}
