﻿using System;
using System.Collections.Generic;
using System.Text;
using SignRemote;

namespace ServerRemote
{
    class SignFileEventArgs : EventArgs
    {
        public Exception Error { get; private set; }
        public TimeSpan TotalTime { get; private set; }
        public MessageSendFile Content { get; set; }
        public SignFileEventArgs(MessageSendFile msgSend, Exception ex)
            : this(msgSend, new TimeSpan(), ex)
        {

        }
        public SignFileEventArgs(MessageSendFile content, TimeSpan totalTime, Exception ex)
        {
            this.Content = content;
            this.TotalTime = totalTime;
            this.Error = ex;
        }
    }
}
