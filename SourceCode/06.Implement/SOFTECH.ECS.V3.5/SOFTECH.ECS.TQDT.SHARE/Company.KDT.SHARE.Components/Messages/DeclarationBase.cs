﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class DeclarationBase : IssueBase
    {
        #region Header

        /// <summary>
        /// Ngày đăng ký YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        ///<summary>
        ///Số lượng hàng(n..5)

        ///</summary>
        [XmlElement("goodsItem")]
        public string GoodsItem { set; get; }
        ///<summary>
        ///Số lượng chứng từ, phụ lục đính kèm(n..5)
        ///</summary>
        [XmlElement("loadingList")]
        public string LoadingList { set; get; }
        ///<summary>
        ///Trọng lượng (kg)(float) 
        ///n..11,3	Tối đa 3 chữ số thập phân
        ///</summary>
        [XmlElement("totalGrossMass")]
        public string TotalGrossMass { set; get; }
        ///<summary>
        ///Trọng lượng tịnh
        ///</summary>
        [XmlElement("totalNetGrossMass")]
        public string TotalNetGrossMass { set; get; }
        ///<summary>
        ///Mã loại hình
        ///</summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { set; get; }
        ///<summary>
        ///Mã phương thức thanh toán
        ///</summary>
        [XmlElement("paymentMethod")]
        public string PaymentMethod { set; get; }
        /// <summary>
        /// Trạng thái của chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        /// <summary>
        /// Số tờ khai 
        /// Dùng trong trường hợp khai sửa bản đăng ký
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        #endregion Header

        #region Body
        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }
        ///<summary>
        ///Nguyên tệ
        ///</summary>
        [XmlElement("CurrencyExchange")]
        public CurrencyExchange CurrencyExchange { set; get; }
        ///<summary>
        /// Số kiện
        ///</summary>
        [XmlElement("DeclarationPackaging")]
        public Packaging DeclarationPackaging { set; get; }

        /// <summary>
        /// Hợp đồng, vận đơn
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public List<Company.KDT.SHARE.Components.AdditionalDocument> AdditionalDocuments { get; set; }

        /// <summary>
        ///Nội dung ghi chú
        /// </summary>
        [XmlElement("AdditionalInformations")]
        public List<AdditionalInformation> AdditionalInformations { get; set; }

        /// <summary>
        ///Nội dung ghi chú
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalDocument AdditionalInformationNew { get; set; }

        ///// <summary>
        /////Nội dung ghi chú
        ///// </summary>
        //[XmlElement("AdditionalInformation")]
        //public AdditionalInformation AdditionalInformation { get; set; }


        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        [XmlElement("Invoice")]
        public Invoice Invoice { get; set; }
        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }
        /// <summary>
        /// Doanh nghiệp xuất khẩu
        /// </summary>
        [XmlElement("Exporter")]
        public NameBase Exporter { get; set; }

        /// <summary>
        /// Thông tin kho
        /// </summary>
        [XmlElement("Warehouse")]
        public NameBase Warehouse { get; set; }

        #endregion
        #region Doanh nghiep che xuat

        /// <summary>
        /// Ngày bắt đầu báo cáo 	1	an10	YYYY-MM-DD
        /// </summary>
        [XmlElement("fromDate")]
        public string FromDate { get; set; }
        /// <summary>
        /// Ngày kết thúc báo cáo	1	an10	YYYY-MM-DD
        /// </summary>
        [XmlElement("toDate")]
        public string ToDate { get; set; }

        /// <summary>
        /// Quý báo cáo		n1
        /// </summary>
        [XmlElement("quarter")]
        public string Quarter { get; set; }
        /// <summary>
        /// Năm báo cáo		n4
        /// </summary>
        [XmlElement("year")]
        public string Year { get; set; }

        #endregion
        #region Danh cho to khai thu cong
        [XmlElement("customsReferenceManual")]
        public string CustomsReferenceManual { get; set; }
        [XmlElement("clearance")]
        public string Clearance { get; set; }
        [XmlElement("channel")]
        public string Channel { get; set; }
        #endregion
       
    }
}
