using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ChungTuThanhToan : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string LoaiChungTu { set; get; }
		public string LoaiHinhBaoLanh { set; get; }
		public string MaNganHangCungCapBaoLanh { set; get; }
		public decimal NamPhatHanh { set; get; }
		public string KiHieuChungTuPhatHanhBaoLanh { set; get; }
		public string SoChungTuPhatHanhBaoLanh { set; get; }
		public string MaTienTe { set; get; }
		public string MaNganHangTraThay { set; get; }
		public string TenNganHangTraThay { set; get; }
		public string MaDonViSuDungHanMuc { set; get; }
		public string TenDonViSuDungHanMuc { set; get; }
		public string KiHieuChungTuPhatHanhHanMuc { set; get; }
		public string SoHieuPhatHanhHanMuc { set; get; }
		public string MaKetQuaXuLy { set; get; }
		public string TenNganHangCungCapBaoLanh { set; get; }
		public decimal SoToKhai { set; get; }
		public DateTime NgayDuDinhKhaiBao { set; get; }
		public string MaLoaiHinh { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string TenCucHaiQuan { set; get; }
		public string SoVanDon_1 { set; get; }
		public string SoVanDon_2 { set; get; }
		public string SoVanDon_3 { set; get; }
		public string SoVanDon_4 { set; get; }
		public string SoVanDon_5 { set; get; }
		public string SoHoaDon { set; get; }
		public string MaDonViSuDungBaoLanh { set; get; }
		public string TenDonViSuDungBaoLanh { set; get; }
		public string MaDonViDaiDienSuDungBaoLanh { set; get; }
		public string TenDonViDaiDienSuDungBaoLanh { set; get; }
		public DateTime NgayBatDauHieuLuc { set; get; }
		public DateTime NgayHetHieuLuc { set; get; }
		public decimal SoTienDangKyBaoLanh { set; get; }
		public string CoBaoVoHieu { set; get; }
		public decimal SoDuBaoLanh { set; get; }
		public decimal SoTienHanMucDangKi { set; get; }
		public decimal SoDuHanMuc { set; get; }
		public string SoThuTuHanMuc { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string TrangThaiXuLy { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ChungTuThanhToan> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ChungTuThanhToan> collection = new List<KDT_VNACC_ChungTuThanhToan>();
			while (reader.Read())
			{
				KDT_VNACC_ChungTuThanhToan entity = new KDT_VNACC_ChungTuThanhToan();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiHinhBaoLanh"))) entity.LoaiHinhBaoLanh = reader.GetString(reader.GetOrdinal("LoaiHinhBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangCungCapBaoLanh"))) entity.MaNganHangCungCapBaoLanh = reader.GetString(reader.GetOrdinal("MaNganHangCungCapBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamPhatHanh"))) entity.NamPhatHanh = reader.GetDecimal(reader.GetOrdinal("NamPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("KiHieuChungTuPhatHanhBaoLanh"))) entity.KiHieuChungTuPhatHanhBaoLanh = reader.GetString(reader.GetOrdinal("KiHieuChungTuPhatHanhBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTuPhatHanhBaoLanh"))) entity.SoChungTuPhatHanhBaoLanh = reader.GetString(reader.GetOrdinal("SoChungTuPhatHanhBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTienTe"))) entity.MaTienTe = reader.GetString(reader.GetOrdinal("MaTienTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNganHangTraThay"))) entity.MaNganHangTraThay = reader.GetString(reader.GetOrdinal("MaNganHangTraThay"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangTraThay"))) entity.TenNganHangTraThay = reader.GetString(reader.GetOrdinal("TenNganHangTraThay"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViSuDungHanMuc"))) entity.MaDonViSuDungHanMuc = reader.GetString(reader.GetOrdinal("MaDonViSuDungHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViSuDungHanMuc"))) entity.TenDonViSuDungHanMuc = reader.GetString(reader.GetOrdinal("TenDonViSuDungHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("KiHieuChungTuPhatHanhHanMuc"))) entity.KiHieuChungTuPhatHanhHanMuc = reader.GetString(reader.GetOrdinal("KiHieuChungTuPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPhatHanhHanMuc"))) entity.SoHieuPhatHanhHanMuc = reader.GetString(reader.GetOrdinal("SoHieuPhatHanhHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaXuLy"))) entity.MaKetQuaXuLy = reader.GetString(reader.GetOrdinal("MaKetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNganHangCungCapBaoLanh"))) entity.TenNganHangCungCapBaoLanh = reader.GetString(reader.GetOrdinal("TenNganHangCungCapBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDuDinhKhaiBao"))) entity.NgayDuDinhKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayDuDinhKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCucHaiQuan"))) entity.TenCucHaiQuan = reader.GetString(reader.GetOrdinal("TenCucHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon_1"))) entity.SoVanDon_1 = reader.GetString(reader.GetOrdinal("SoVanDon_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon_2"))) entity.SoVanDon_2 = reader.GetString(reader.GetOrdinal("SoVanDon_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon_3"))) entity.SoVanDon_3 = reader.GetString(reader.GetOrdinal("SoVanDon_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon_4"))) entity.SoVanDon_4 = reader.GetString(reader.GetOrdinal("SoVanDon_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon_5"))) entity.SoVanDon_5 = reader.GetString(reader.GetOrdinal("SoVanDon_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViSuDungBaoLanh"))) entity.MaDonViSuDungBaoLanh = reader.GetString(reader.GetOrdinal("MaDonViSuDungBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViSuDungBaoLanh"))) entity.TenDonViSuDungBaoLanh = reader.GetString(reader.GetOrdinal("TenDonViSuDungBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViDaiDienSuDungBaoLanh"))) entity.MaDonViDaiDienSuDungBaoLanh = reader.GetString(reader.GetOrdinal("MaDonViDaiDienSuDungBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDaiDienSuDungBaoLanh"))) entity.TenDonViDaiDienSuDungBaoLanh = reader.GetString(reader.GetOrdinal("TenDonViDaiDienSuDungBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDauHieuLuc"))) entity.NgayBatDauHieuLuc = reader.GetDateTime(reader.GetOrdinal("NgayBatDauHieuLuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHieuLuc"))) entity.NgayHetHieuLuc = reader.GetDateTime(reader.GetOrdinal("NgayHetHieuLuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienDangKyBaoLanh"))) entity.SoTienDangKyBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoTienDangKyBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoBaoVoHieu"))) entity.CoBaoVoHieu = reader.GetString(reader.GetOrdinal("CoBaoVoHieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDuBaoLanh"))) entity.SoDuBaoLanh = reader.GetDecimal(reader.GetOrdinal("SoDuBaoLanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienHanMucDangKi"))) entity.SoTienHanMucDangKi = reader.GetDecimal(reader.GetOrdinal("SoTienHanMucDangKi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDuHanMuc"))) entity.SoDuHanMuc = reader.GetDecimal(reader.GetOrdinal("SoDuHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHanMuc"))) entity.SoThuTuHanMuc = reader.GetString(reader.GetOrdinal("SoThuTuHanMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ChungTuThanhToan> collection, long id)
        {
            foreach (KDT_VNACC_ChungTuThanhToan item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuThanhToan VALUES(@LoaiChungTu, @LoaiHinhBaoLanh, @MaNganHangCungCapBaoLanh, @NamPhatHanh, @KiHieuChungTuPhatHanhBaoLanh, @SoChungTuPhatHanhBaoLanh, @MaTienTe, @MaNganHangTraThay, @TenNganHangTraThay, @MaDonViSuDungHanMuc, @TenDonViSuDungHanMuc, @KiHieuChungTuPhatHanhHanMuc, @SoHieuPhatHanhHanMuc, @MaKetQuaXuLy, @TenNganHangCungCapBaoLanh, @SoToKhai, @NgayDuDinhKhaiBao, @MaLoaiHinh, @CoQuanHaiQuan, @TenCucHaiQuan, @SoVanDon_1, @SoVanDon_2, @SoVanDon_3, @SoVanDon_4, @SoVanDon_5, @SoHoaDon, @MaDonViSuDungBaoLanh, @TenDonViSuDungBaoLanh, @MaDonViDaiDienSuDungBaoLanh, @TenDonViDaiDienSuDungBaoLanh, @NgayBatDauHieuLuc, @NgayHetHieuLuc, @SoTienDangKyBaoLanh, @CoBaoVoHieu, @SoDuBaoLanh, @SoTienHanMucDangKi, @SoDuHanMuc, @SoThuTuHanMuc, @Notes, @InputMessageID, @MessageTag, @IndexTag, @TrangThaiXuLy, @NgayKhaiBao)";
            string update = "UPDATE t_KDT_VNACC_ChungTuThanhToan SET LoaiChungTu = @LoaiChungTu, LoaiHinhBaoLanh = @LoaiHinhBaoLanh, MaNganHangCungCapBaoLanh = @MaNganHangCungCapBaoLanh, NamPhatHanh = @NamPhatHanh, KiHieuChungTuPhatHanhBaoLanh = @KiHieuChungTuPhatHanhBaoLanh, SoChungTuPhatHanhBaoLanh = @SoChungTuPhatHanhBaoLanh, MaTienTe = @MaTienTe, MaNganHangTraThay = @MaNganHangTraThay, TenNganHangTraThay = @TenNganHangTraThay, MaDonViSuDungHanMuc = @MaDonViSuDungHanMuc, TenDonViSuDungHanMuc = @TenDonViSuDungHanMuc, KiHieuChungTuPhatHanhHanMuc = @KiHieuChungTuPhatHanhHanMuc, SoHieuPhatHanhHanMuc = @SoHieuPhatHanhHanMuc, MaKetQuaXuLy = @MaKetQuaXuLy, TenNganHangCungCapBaoLanh = @TenNganHangCungCapBaoLanh, SoToKhai = @SoToKhai, NgayDuDinhKhaiBao = @NgayDuDinhKhaiBao, MaLoaiHinh = @MaLoaiHinh, CoQuanHaiQuan = @CoQuanHaiQuan, TenCucHaiQuan = @TenCucHaiQuan, SoVanDon_1 = @SoVanDon_1, SoVanDon_2 = @SoVanDon_2, SoVanDon_3 = @SoVanDon_3, SoVanDon_4 = @SoVanDon_4, SoVanDon_5 = @SoVanDon_5, SoHoaDon = @SoHoaDon, MaDonViSuDungBaoLanh = @MaDonViSuDungBaoLanh, TenDonViSuDungBaoLanh = @TenDonViSuDungBaoLanh, MaDonViDaiDienSuDungBaoLanh = @MaDonViDaiDienSuDungBaoLanh, TenDonViDaiDienSuDungBaoLanh = @TenDonViDaiDienSuDungBaoLanh, NgayBatDauHieuLuc = @NgayBatDauHieuLuc, NgayHetHieuLuc = @NgayHetHieuLuc, SoTienDangKyBaoLanh = @SoTienDangKyBaoLanh, CoBaoVoHieu = @CoBaoVoHieu, SoDuBaoLanh = @SoDuBaoLanh, SoTienHanMucDangKi = @SoTienHanMucDangKi, SoDuHanMuc = @SoDuHanMuc, SoThuTuHanMuc = @SoThuTuHanMuc, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, TrangThaiXuLy = @TrangThaiXuLy, NgayKhaiBao = @NgayKhaiBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuThanhToan WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, "LoaiHinhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, "MaNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanh", SqlDbType.Decimal, "NamPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "KiHieuChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "SoChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, "MaDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, "TenDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, "TenNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, "NgayDuDinhKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_1", SqlDbType.VarChar, "SoVanDon_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_2", SqlDbType.VarChar, "SoVanDon_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_3", SqlDbType.VarChar, "SoVanDon_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_4", SqlDbType.VarChar, "SoVanDon_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_5", SqlDbType.VarChar, "SoVanDon_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, "MaDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, "MaDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, "NgayBatDauHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, "SoTienDangKyBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoVoHieu", SqlDbType.VarChar, "CoBaoVoHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDuBaoLanh", SqlDbType.Decimal, "SoDuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, "SoTienHanMucDangKi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDuHanMuc", SqlDbType.Decimal, "SoDuHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, "SoThuTuHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, "LoaiHinhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, "MaNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanh", SqlDbType.Decimal, "NamPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "KiHieuChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "SoChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, "MaDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, "TenDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, "TenNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, "NgayDuDinhKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_1", SqlDbType.VarChar, "SoVanDon_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_2", SqlDbType.VarChar, "SoVanDon_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_3", SqlDbType.VarChar, "SoVanDon_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_4", SqlDbType.VarChar, "SoVanDon_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_5", SqlDbType.VarChar, "SoVanDon_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, "MaDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, "MaDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, "NgayBatDauHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, "SoTienDangKyBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoVoHieu", SqlDbType.VarChar, "CoBaoVoHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDuBaoLanh", SqlDbType.Decimal, "SoDuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, "SoTienHanMucDangKi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDuHanMuc", SqlDbType.Decimal, "SoDuHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, "SoThuTuHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ChungTuThanhToan VALUES(@LoaiChungTu, @LoaiHinhBaoLanh, @MaNganHangCungCapBaoLanh, @NamPhatHanh, @KiHieuChungTuPhatHanhBaoLanh, @SoChungTuPhatHanhBaoLanh, @MaTienTe, @MaNganHangTraThay, @TenNganHangTraThay, @MaDonViSuDungHanMuc, @TenDonViSuDungHanMuc, @KiHieuChungTuPhatHanhHanMuc, @SoHieuPhatHanhHanMuc, @MaKetQuaXuLy, @TenNganHangCungCapBaoLanh, @SoToKhai, @NgayDuDinhKhaiBao, @MaLoaiHinh, @CoQuanHaiQuan, @TenCucHaiQuan, @SoVanDon_1, @SoVanDon_2, @SoVanDon_3, @SoVanDon_4, @SoVanDon_5, @SoHoaDon, @MaDonViSuDungBaoLanh, @TenDonViSuDungBaoLanh, @MaDonViDaiDienSuDungBaoLanh, @TenDonViDaiDienSuDungBaoLanh, @NgayBatDauHieuLuc, @NgayHetHieuLuc, @SoTienDangKyBaoLanh, @CoBaoVoHieu, @SoDuBaoLanh, @SoTienHanMucDangKi, @SoDuHanMuc, @SoThuTuHanMuc, @Notes, @InputMessageID, @MessageTag, @IndexTag, @TrangThaiXuLy, @NgayKhaiBao)";
            string update = "UPDATE t_KDT_VNACC_ChungTuThanhToan SET LoaiChungTu = @LoaiChungTu, LoaiHinhBaoLanh = @LoaiHinhBaoLanh, MaNganHangCungCapBaoLanh = @MaNganHangCungCapBaoLanh, NamPhatHanh = @NamPhatHanh, KiHieuChungTuPhatHanhBaoLanh = @KiHieuChungTuPhatHanhBaoLanh, SoChungTuPhatHanhBaoLanh = @SoChungTuPhatHanhBaoLanh, MaTienTe = @MaTienTe, MaNganHangTraThay = @MaNganHangTraThay, TenNganHangTraThay = @TenNganHangTraThay, MaDonViSuDungHanMuc = @MaDonViSuDungHanMuc, TenDonViSuDungHanMuc = @TenDonViSuDungHanMuc, KiHieuChungTuPhatHanhHanMuc = @KiHieuChungTuPhatHanhHanMuc, SoHieuPhatHanhHanMuc = @SoHieuPhatHanhHanMuc, MaKetQuaXuLy = @MaKetQuaXuLy, TenNganHangCungCapBaoLanh = @TenNganHangCungCapBaoLanh, SoToKhai = @SoToKhai, NgayDuDinhKhaiBao = @NgayDuDinhKhaiBao, MaLoaiHinh = @MaLoaiHinh, CoQuanHaiQuan = @CoQuanHaiQuan, TenCucHaiQuan = @TenCucHaiQuan, SoVanDon_1 = @SoVanDon_1, SoVanDon_2 = @SoVanDon_2, SoVanDon_3 = @SoVanDon_3, SoVanDon_4 = @SoVanDon_4, SoVanDon_5 = @SoVanDon_5, SoHoaDon = @SoHoaDon, MaDonViSuDungBaoLanh = @MaDonViSuDungBaoLanh, TenDonViSuDungBaoLanh = @TenDonViSuDungBaoLanh, MaDonViDaiDienSuDungBaoLanh = @MaDonViDaiDienSuDungBaoLanh, TenDonViDaiDienSuDungBaoLanh = @TenDonViDaiDienSuDungBaoLanh, NgayBatDauHieuLuc = @NgayBatDauHieuLuc, NgayHetHieuLuc = @NgayHetHieuLuc, SoTienDangKyBaoLanh = @SoTienDangKyBaoLanh, CoBaoVoHieu = @CoBaoVoHieu, SoDuBaoLanh = @SoDuBaoLanh, SoTienHanMucDangKi = @SoTienHanMucDangKi, SoDuHanMuc = @SoDuHanMuc, SoThuTuHanMuc = @SoThuTuHanMuc, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, TrangThaiXuLy = @TrangThaiXuLy, NgayKhaiBao = @NgayKhaiBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ChungTuThanhToan WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, "LoaiHinhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, "MaNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamPhatHanh", SqlDbType.Decimal, "NamPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "KiHieuChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "SoChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, "MaDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, "TenDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, "TenNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, "NgayDuDinhKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_1", SqlDbType.VarChar, "SoVanDon_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_2", SqlDbType.VarChar, "SoVanDon_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_3", SqlDbType.VarChar, "SoVanDon_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_4", SqlDbType.VarChar, "SoVanDon_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon_5", SqlDbType.VarChar, "SoVanDon_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, "MaDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, "MaDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, "NgayBatDauHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, "SoTienDangKyBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoBaoVoHieu", SqlDbType.VarChar, "CoBaoVoHieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDuBaoLanh", SqlDbType.Decimal, "SoDuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, "SoTienHanMucDangKi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDuHanMuc", SqlDbType.Decimal, "SoDuHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, "SoThuTuHanMuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, "LoaiHinhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, "MaNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamPhatHanh", SqlDbType.Decimal, "NamPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "KiHieuChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, "SoChungTuPhatHanhBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTienTe", SqlDbType.VarChar, "MaTienTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNganHangTraThay", SqlDbType.VarChar, "MaNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, "TenNganHangTraThay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, "MaDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, "TenDonViSuDungHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, "KiHieuChungTuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, "SoHieuPhatHanhHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, "TenNganHangCungCapBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, "NgayDuDinhKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, "TenCucHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_1", SqlDbType.VarChar, "SoVanDon_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_2", SqlDbType.VarChar, "SoVanDon_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_3", SqlDbType.VarChar, "SoVanDon_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_4", SqlDbType.VarChar, "SoVanDon_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon_5", SqlDbType.VarChar, "SoVanDon_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoaDon", SqlDbType.VarChar, "SoHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, "MaDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, "MaDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, "TenDonViDaiDienSuDungBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, "NgayBatDauHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, "NgayHetHieuLuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, "SoTienDangKyBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoBaoVoHieu", SqlDbType.VarChar, "CoBaoVoHieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDuBaoLanh", SqlDbType.Decimal, "SoDuBaoLanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, "SoTienHanMucDangKi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDuHanMuc", SqlDbType.Decimal, "SoDuHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, "SoThuTuHanMuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.VarChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ChungTuThanhToan Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ChungTuThanhToan> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ChungTuThanhToan> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ChungTuThanhToan> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ChungTuThanhToan(string loaiChungTu, string loaiHinhBaoLanh, string maNganHangCungCapBaoLanh, decimal namPhatHanh, string kiHieuChungTuPhatHanhBaoLanh, string soChungTuPhatHanhBaoLanh, string maTienTe, string maNganHangTraThay, string tenNganHangTraThay, string maDonViSuDungHanMuc, string tenDonViSuDungHanMuc, string kiHieuChungTuPhatHanhHanMuc, string soHieuPhatHanhHanMuc, string maKetQuaXuLy, string tenNganHangCungCapBaoLanh, decimal soToKhai, DateTime ngayDuDinhKhaiBao, string maLoaiHinh, string coQuanHaiQuan, string tenCucHaiQuan, string soVanDon_1, string soVanDon_2, string soVanDon_3, string soVanDon_4, string soVanDon_5, string soHoaDon, string maDonViSuDungBaoLanh, string tenDonViSuDungBaoLanh, string maDonViDaiDienSuDungBaoLanh, string tenDonViDaiDienSuDungBaoLanh, DateTime ngayBatDauHieuLuc, DateTime ngayHetHieuLuc, decimal soTienDangKyBaoLanh, string coBaoVoHieu, decimal soDuBaoLanh, decimal soTienHanMucDangKi, decimal soDuHanMuc, string soThuTuHanMuc, string notes, string inputMessageID, string messageTag, string indexTag, string trangThaiXuLy, DateTime ngayKhaiBao)
		{
			KDT_VNACC_ChungTuThanhToan entity = new KDT_VNACC_ChungTuThanhToan();	
			entity.LoaiChungTu = loaiChungTu;
			entity.LoaiHinhBaoLanh = loaiHinhBaoLanh;
			entity.MaNganHangCungCapBaoLanh = maNganHangCungCapBaoLanh;
			entity.NamPhatHanh = namPhatHanh;
			entity.KiHieuChungTuPhatHanhBaoLanh = kiHieuChungTuPhatHanhBaoLanh;
			entity.SoChungTuPhatHanhBaoLanh = soChungTuPhatHanhBaoLanh;
			entity.MaTienTe = maTienTe;
			entity.MaNganHangTraThay = maNganHangTraThay;
			entity.TenNganHangTraThay = tenNganHangTraThay;
			entity.MaDonViSuDungHanMuc = maDonViSuDungHanMuc;
			entity.TenDonViSuDungHanMuc = tenDonViSuDungHanMuc;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoHieuPhatHanhHanMuc = soHieuPhatHanhHanMuc;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.TenNganHangCungCapBaoLanh = tenNganHangCungCapBaoLanh;
			entity.SoToKhai = soToKhai;
			entity.NgayDuDinhKhaiBao = ngayDuDinhKhaiBao;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.TenCucHaiQuan = tenCucHaiQuan;
			entity.SoVanDon_1 = soVanDon_1;
			entity.SoVanDon_2 = soVanDon_2;
			entity.SoVanDon_3 = soVanDon_3;
			entity.SoVanDon_4 = soVanDon_4;
			entity.SoVanDon_5 = soVanDon_5;
			entity.SoHoaDon = soHoaDon;
			entity.MaDonViSuDungBaoLanh = maDonViSuDungBaoLanh;
			entity.TenDonViSuDungBaoLanh = tenDonViSuDungBaoLanh;
			entity.MaDonViDaiDienSuDungBaoLanh = maDonViDaiDienSuDungBaoLanh;
			entity.TenDonViDaiDienSuDungBaoLanh = tenDonViDaiDienSuDungBaoLanh;
			entity.NgayBatDauHieuLuc = ngayBatDauHieuLuc;
			entity.NgayHetHieuLuc = ngayHetHieuLuc;
			entity.SoTienDangKyBaoLanh = soTienDangKyBaoLanh;
			entity.CoBaoVoHieu = coBaoVoHieu;
			entity.SoDuBaoLanh = soDuBaoLanh;
			entity.SoTienHanMucDangKi = soTienHanMucDangKi;
			entity.SoDuHanMuc = soDuHanMuc;
			entity.SoThuTuHanMuc = soThuTuHanMuc;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, LoaiHinhBaoLanh);
			db.AddInParameter(dbCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, MaNganHangCungCapBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanh", SqlDbType.Decimal, NamPhatHanh);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, KiHieuChungTuPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, SoChungTuPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@MaTienTe", SqlDbType.VarChar, MaTienTe);
			db.AddInParameter(dbCommand, "@MaNganHangTraThay", SqlDbType.VarChar, MaNganHangTraThay);
			db.AddInParameter(dbCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, TenNganHangTraThay);
			db.AddInParameter(dbCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, MaDonViSuDungHanMuc);
			db.AddInParameter(dbCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, TenDonViSuDungHanMuc);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, SoHieuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, TenNganHangCungCapBaoLanh);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, NgayDuDinhKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayDuDinhKhaiBao);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, TenCucHaiQuan);
			db.AddInParameter(dbCommand, "@SoVanDon_1", SqlDbType.VarChar, SoVanDon_1);
			db.AddInParameter(dbCommand, "@SoVanDon_2", SqlDbType.VarChar, SoVanDon_2);
			db.AddInParameter(dbCommand, "@SoVanDon_3", SqlDbType.VarChar, SoVanDon_3);
			db.AddInParameter(dbCommand, "@SoVanDon_4", SqlDbType.VarChar, SoVanDon_4);
			db.AddInParameter(dbCommand, "@SoVanDon_5", SqlDbType.VarChar, SoVanDon_5);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, MaDonViSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, TenDonViSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, MaDonViDaiDienSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, TenDonViDaiDienSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, NgayBatDauHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayBatDauHieuLuc);
			db.AddInParameter(dbCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, NgayHetHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLuc);
			db.AddInParameter(dbCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, SoTienDangKyBaoLanh);
			db.AddInParameter(dbCommand, "@CoBaoVoHieu", SqlDbType.VarChar, CoBaoVoHieu);
			db.AddInParameter(dbCommand, "@SoDuBaoLanh", SqlDbType.Decimal, SoDuBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, SoTienHanMucDangKi);
			db.AddInParameter(dbCommand, "@SoDuHanMuc", SqlDbType.Decimal, SoDuHanMuc);
			db.AddInParameter(dbCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, SoThuTuHanMuc);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ChungTuThanhToan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ChungTuThanhToan(long id, string loaiChungTu, string loaiHinhBaoLanh, string maNganHangCungCapBaoLanh, decimal namPhatHanh, string kiHieuChungTuPhatHanhBaoLanh, string soChungTuPhatHanhBaoLanh, string maTienTe, string maNganHangTraThay, string tenNganHangTraThay, string maDonViSuDungHanMuc, string tenDonViSuDungHanMuc, string kiHieuChungTuPhatHanhHanMuc, string soHieuPhatHanhHanMuc, string maKetQuaXuLy, string tenNganHangCungCapBaoLanh, decimal soToKhai, DateTime ngayDuDinhKhaiBao, string maLoaiHinh, string coQuanHaiQuan, string tenCucHaiQuan, string soVanDon_1, string soVanDon_2, string soVanDon_3, string soVanDon_4, string soVanDon_5, string soHoaDon, string maDonViSuDungBaoLanh, string tenDonViSuDungBaoLanh, string maDonViDaiDienSuDungBaoLanh, string tenDonViDaiDienSuDungBaoLanh, DateTime ngayBatDauHieuLuc, DateTime ngayHetHieuLuc, decimal soTienDangKyBaoLanh, string coBaoVoHieu, decimal soDuBaoLanh, decimal soTienHanMucDangKi, decimal soDuHanMuc, string soThuTuHanMuc, string notes, string inputMessageID, string messageTag, string indexTag, string trangThaiXuLy, DateTime ngayKhaiBao)
		{
			KDT_VNACC_ChungTuThanhToan entity = new KDT_VNACC_ChungTuThanhToan();			
			entity.ID = id;
			entity.LoaiChungTu = loaiChungTu;
			entity.LoaiHinhBaoLanh = loaiHinhBaoLanh;
			entity.MaNganHangCungCapBaoLanh = maNganHangCungCapBaoLanh;
			entity.NamPhatHanh = namPhatHanh;
			entity.KiHieuChungTuPhatHanhBaoLanh = kiHieuChungTuPhatHanhBaoLanh;
			entity.SoChungTuPhatHanhBaoLanh = soChungTuPhatHanhBaoLanh;
			entity.MaTienTe = maTienTe;
			entity.MaNganHangTraThay = maNganHangTraThay;
			entity.TenNganHangTraThay = tenNganHangTraThay;
			entity.MaDonViSuDungHanMuc = maDonViSuDungHanMuc;
			entity.TenDonViSuDungHanMuc = tenDonViSuDungHanMuc;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoHieuPhatHanhHanMuc = soHieuPhatHanhHanMuc;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.TenNganHangCungCapBaoLanh = tenNganHangCungCapBaoLanh;
			entity.SoToKhai = soToKhai;
			entity.NgayDuDinhKhaiBao = ngayDuDinhKhaiBao;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.TenCucHaiQuan = tenCucHaiQuan;
			entity.SoVanDon_1 = soVanDon_1;
			entity.SoVanDon_2 = soVanDon_2;
			entity.SoVanDon_3 = soVanDon_3;
			entity.SoVanDon_4 = soVanDon_4;
			entity.SoVanDon_5 = soVanDon_5;
			entity.SoHoaDon = soHoaDon;
			entity.MaDonViSuDungBaoLanh = maDonViSuDungBaoLanh;
			entity.TenDonViSuDungBaoLanh = tenDonViSuDungBaoLanh;
			entity.MaDonViDaiDienSuDungBaoLanh = maDonViDaiDienSuDungBaoLanh;
			entity.TenDonViDaiDienSuDungBaoLanh = tenDonViDaiDienSuDungBaoLanh;
			entity.NgayBatDauHieuLuc = ngayBatDauHieuLuc;
			entity.NgayHetHieuLuc = ngayHetHieuLuc;
			entity.SoTienDangKyBaoLanh = soTienDangKyBaoLanh;
			entity.CoBaoVoHieu = coBaoVoHieu;
			entity.SoDuBaoLanh = soDuBaoLanh;
			entity.SoTienHanMucDangKi = soTienHanMucDangKi;
			entity.SoDuHanMuc = soDuHanMuc;
			entity.SoThuTuHanMuc = soThuTuHanMuc;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ChungTuThanhToan_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, LoaiHinhBaoLanh);
			db.AddInParameter(dbCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, MaNganHangCungCapBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanh", SqlDbType.Decimal, NamPhatHanh);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, KiHieuChungTuPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, SoChungTuPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@MaTienTe", SqlDbType.VarChar, MaTienTe);
			db.AddInParameter(dbCommand, "@MaNganHangTraThay", SqlDbType.VarChar, MaNganHangTraThay);
			db.AddInParameter(dbCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, TenNganHangTraThay);
			db.AddInParameter(dbCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, MaDonViSuDungHanMuc);
			db.AddInParameter(dbCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, TenDonViSuDungHanMuc);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, SoHieuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, TenNganHangCungCapBaoLanh);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, NgayDuDinhKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayDuDinhKhaiBao);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, TenCucHaiQuan);
			db.AddInParameter(dbCommand, "@SoVanDon_1", SqlDbType.VarChar, SoVanDon_1);
			db.AddInParameter(dbCommand, "@SoVanDon_2", SqlDbType.VarChar, SoVanDon_2);
			db.AddInParameter(dbCommand, "@SoVanDon_3", SqlDbType.VarChar, SoVanDon_3);
			db.AddInParameter(dbCommand, "@SoVanDon_4", SqlDbType.VarChar, SoVanDon_4);
			db.AddInParameter(dbCommand, "@SoVanDon_5", SqlDbType.VarChar, SoVanDon_5);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, MaDonViSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, TenDonViSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, MaDonViDaiDienSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, TenDonViDaiDienSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, NgayBatDauHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayBatDauHieuLuc);
			db.AddInParameter(dbCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, NgayHetHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLuc);
			db.AddInParameter(dbCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, SoTienDangKyBaoLanh);
			db.AddInParameter(dbCommand, "@CoBaoVoHieu", SqlDbType.VarChar, CoBaoVoHieu);
			db.AddInParameter(dbCommand, "@SoDuBaoLanh", SqlDbType.Decimal, SoDuBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, SoTienHanMucDangKi);
			db.AddInParameter(dbCommand, "@SoDuHanMuc", SqlDbType.Decimal, SoDuHanMuc);
			db.AddInParameter(dbCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, SoThuTuHanMuc);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ChungTuThanhToan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ChungTuThanhToan(long id, string loaiChungTu, string loaiHinhBaoLanh, string maNganHangCungCapBaoLanh, decimal namPhatHanh, string kiHieuChungTuPhatHanhBaoLanh, string soChungTuPhatHanhBaoLanh, string maTienTe, string maNganHangTraThay, string tenNganHangTraThay, string maDonViSuDungHanMuc, string tenDonViSuDungHanMuc, string kiHieuChungTuPhatHanhHanMuc, string soHieuPhatHanhHanMuc, string maKetQuaXuLy, string tenNganHangCungCapBaoLanh, decimal soToKhai, DateTime ngayDuDinhKhaiBao, string maLoaiHinh, string coQuanHaiQuan, string tenCucHaiQuan, string soVanDon_1, string soVanDon_2, string soVanDon_3, string soVanDon_4, string soVanDon_5, string soHoaDon, string maDonViSuDungBaoLanh, string tenDonViSuDungBaoLanh, string maDonViDaiDienSuDungBaoLanh, string tenDonViDaiDienSuDungBaoLanh, DateTime ngayBatDauHieuLuc, DateTime ngayHetHieuLuc, decimal soTienDangKyBaoLanh, string coBaoVoHieu, decimal soDuBaoLanh, decimal soTienHanMucDangKi, decimal soDuHanMuc, string soThuTuHanMuc, string notes, string inputMessageID, string messageTag, string indexTag, string trangThaiXuLy, DateTime ngayKhaiBao)
		{
			KDT_VNACC_ChungTuThanhToan entity = new KDT_VNACC_ChungTuThanhToan();			
			entity.ID = id;
			entity.LoaiChungTu = loaiChungTu;
			entity.LoaiHinhBaoLanh = loaiHinhBaoLanh;
			entity.MaNganHangCungCapBaoLanh = maNganHangCungCapBaoLanh;
			entity.NamPhatHanh = namPhatHanh;
			entity.KiHieuChungTuPhatHanhBaoLanh = kiHieuChungTuPhatHanhBaoLanh;
			entity.SoChungTuPhatHanhBaoLanh = soChungTuPhatHanhBaoLanh;
			entity.MaTienTe = maTienTe;
			entity.MaNganHangTraThay = maNganHangTraThay;
			entity.TenNganHangTraThay = tenNganHangTraThay;
			entity.MaDonViSuDungHanMuc = maDonViSuDungHanMuc;
			entity.TenDonViSuDungHanMuc = tenDonViSuDungHanMuc;
			entity.KiHieuChungTuPhatHanhHanMuc = kiHieuChungTuPhatHanhHanMuc;
			entity.SoHieuPhatHanhHanMuc = soHieuPhatHanhHanMuc;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.TenNganHangCungCapBaoLanh = tenNganHangCungCapBaoLanh;
			entity.SoToKhai = soToKhai;
			entity.NgayDuDinhKhaiBao = ngayDuDinhKhaiBao;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.TenCucHaiQuan = tenCucHaiQuan;
			entity.SoVanDon_1 = soVanDon_1;
			entity.SoVanDon_2 = soVanDon_2;
			entity.SoVanDon_3 = soVanDon_3;
			entity.SoVanDon_4 = soVanDon_4;
			entity.SoVanDon_5 = soVanDon_5;
			entity.SoHoaDon = soHoaDon;
			entity.MaDonViSuDungBaoLanh = maDonViSuDungBaoLanh;
			entity.TenDonViSuDungBaoLanh = tenDonViSuDungBaoLanh;
			entity.MaDonViDaiDienSuDungBaoLanh = maDonViDaiDienSuDungBaoLanh;
			entity.TenDonViDaiDienSuDungBaoLanh = tenDonViDaiDienSuDungBaoLanh;
			entity.NgayBatDauHieuLuc = ngayBatDauHieuLuc;
			entity.NgayHetHieuLuc = ngayHetHieuLuc;
			entity.SoTienDangKyBaoLanh = soTienDangKyBaoLanh;
			entity.CoBaoVoHieu = coBaoVoHieu;
			entity.SoDuBaoLanh = soDuBaoLanh;
			entity.SoTienHanMucDangKi = soTienHanMucDangKi;
			entity.SoDuHanMuc = soDuHanMuc;
			entity.SoThuTuHanMuc = soThuTuHanMuc;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@LoaiHinhBaoLanh", SqlDbType.VarChar, LoaiHinhBaoLanh);
			db.AddInParameter(dbCommand, "@MaNganHangCungCapBaoLanh", SqlDbType.VarChar, MaNganHangCungCapBaoLanh);
			db.AddInParameter(dbCommand, "@NamPhatHanh", SqlDbType.Decimal, NamPhatHanh);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhBaoLanh", SqlDbType.VarChar, KiHieuChungTuPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@SoChungTuPhatHanhBaoLanh", SqlDbType.VarChar, SoChungTuPhatHanhBaoLanh);
			db.AddInParameter(dbCommand, "@MaTienTe", SqlDbType.VarChar, MaTienTe);
			db.AddInParameter(dbCommand, "@MaNganHangTraThay", SqlDbType.VarChar, MaNganHangTraThay);
			db.AddInParameter(dbCommand, "@TenNganHangTraThay", SqlDbType.NVarChar, TenNganHangTraThay);
			db.AddInParameter(dbCommand, "@MaDonViSuDungHanMuc", SqlDbType.VarChar, MaDonViSuDungHanMuc);
			db.AddInParameter(dbCommand, "@TenDonViSuDungHanMuc", SqlDbType.NVarChar, TenDonViSuDungHanMuc);
			db.AddInParameter(dbCommand, "@KiHieuChungTuPhatHanhHanMuc", SqlDbType.VarChar, KiHieuChungTuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@SoHieuPhatHanhHanMuc", SqlDbType.VarChar, SoHieuPhatHanhHanMuc);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@TenNganHangCungCapBaoLanh", SqlDbType.NVarChar, TenNganHangCungCapBaoLanh);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@NgayDuDinhKhaiBao", SqlDbType.DateTime, NgayDuDinhKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayDuDinhKhaiBao);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@TenCucHaiQuan", SqlDbType.NVarChar, TenCucHaiQuan);
			db.AddInParameter(dbCommand, "@SoVanDon_1", SqlDbType.VarChar, SoVanDon_1);
			db.AddInParameter(dbCommand, "@SoVanDon_2", SqlDbType.VarChar, SoVanDon_2);
			db.AddInParameter(dbCommand, "@SoVanDon_3", SqlDbType.VarChar, SoVanDon_3);
			db.AddInParameter(dbCommand, "@SoVanDon_4", SqlDbType.VarChar, SoVanDon_4);
			db.AddInParameter(dbCommand, "@SoVanDon_5", SqlDbType.VarChar, SoVanDon_5);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@MaDonViSuDungBaoLanh", SqlDbType.VarChar, MaDonViSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@TenDonViSuDungBaoLanh", SqlDbType.NVarChar, TenDonViSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@MaDonViDaiDienSuDungBaoLanh", SqlDbType.VarChar, MaDonViDaiDienSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@TenDonViDaiDienSuDungBaoLanh", SqlDbType.NVarChar, TenDonViDaiDienSuDungBaoLanh);
			db.AddInParameter(dbCommand, "@NgayBatDauHieuLuc", SqlDbType.DateTime, NgayBatDauHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayBatDauHieuLuc);
			db.AddInParameter(dbCommand, "@NgayHetHieuLuc", SqlDbType.DateTime, NgayHetHieuLuc.Year <= 1753 ? DBNull.Value : (object) NgayHetHieuLuc);
			db.AddInParameter(dbCommand, "@SoTienDangKyBaoLanh", SqlDbType.Decimal, SoTienDangKyBaoLanh);
			db.AddInParameter(dbCommand, "@CoBaoVoHieu", SqlDbType.VarChar, CoBaoVoHieu);
			db.AddInParameter(dbCommand, "@SoDuBaoLanh", SqlDbType.Decimal, SoDuBaoLanh);
			db.AddInParameter(dbCommand, "@SoTienHanMucDangKi", SqlDbType.Decimal, SoTienHanMucDangKi);
			db.AddInParameter(dbCommand, "@SoDuHanMuc", SqlDbType.Decimal, SoDuHanMuc);
			db.AddInParameter(dbCommand, "@SoThuTuHanMuc", SqlDbType.VarChar, SoThuTuHanMuc);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ChungTuThanhToan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ChungTuThanhToan(long id)
		{
			KDT_VNACC_ChungTuThanhToan entity = new KDT_VNACC_ChungTuThanhToan();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ChungTuThanhToan_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ChungTuThanhToan> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ChungTuThanhToan item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}