﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.KD
{
    public class NodeGiayPhep
    {								
        /// <summary>
        /// Khai bao.
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ (XK=811, NK=911) || Người cấp giấy phép || Ngày cấp giấy phép
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu chứng từ	|| Số TK || Số giấy phép	
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày khai chứng từ 	|| Ngày TK
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng của chứng từ	1	n..2	Danh mục chuẩn
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo || Nơi cấp giấy phép
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái của chứng từ	1 || Trạng thái đại lý
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số đăng ký chứng từ 	0	an..35
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký chứng từ 	0	an19	YYYY-MM-DD HH:mm:ss
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Hải quan tiếp nhận chứng từ 	1	an..6	Danh mục chuẩn
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Đại lý
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên đại lý	|| Tên người được cấp giấy phép
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã đại lý	|| Mã người được cấp giấy phép
        /// </summary>
        public const string Identity = "identity";
        /// <summary>
        /// Người được cấp giấy phép
        /// </summary>
        public const string Importer = "Importer";
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        public const string DeclarationDocument = "DeclarationDocument";
        /// <summary>
        /// Mã LH
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";
        /// <summary>
        /// Thông tin giấy phép
        /// </summary>
        public const string Licenses = "Licenses";
        /// <summary>
        /// Thông tin giấy phép
        /// </summary>
        public const string License = "License";
        /// <summary>
        /// Hình thức trừ lùi
        /// </summary>
        public const string type = "type";
        /// <summary>
        /// Ngày hết hạn giấy phép
        /// </summary>
        public const string expire = "expire";
        /// <summary>
        /// Ghi chú khác
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Ghi chú khác
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Hàng hóa
        /// </summary>
        public const string GoodsItem = "GoodsItem";
        /// <summary>
        /// Số thứ tự hàng
        /// </summary>
        public const string sequence = "sequence";
        /// <summary>
        /// Trị giá
        /// </summary>
        public const string statisticalValue = "statisticalValue";
        /// <summary>
        /// 
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Thông tin hàng hóa kèm theo giấy phép
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Tên hàng hoá
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã tham chiếu hàng hoá
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// Lượng cấp phép
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Số lượng
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string UnLoadingLocation = "UnLoadingLocation";
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string CurrencyType = "CurrencyType";
        
    }
}
