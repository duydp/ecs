﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class Products
    {
        /// <summary>
        /// Chu kỳ sản xuất
        /// </summary>
       [XmlElement("Product")]
       public List<Product> Product { get; set; }
    }
}
