using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT.GC;
using DevExpress.XtraCharts.Native;
using System.IO;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using System.Collections.Generic;

namespace Company.Interface.Report.GC.ThongTu38
{
    public partial class BaoCaoQuyetToan_Mau15_TT38_Total : DevExpress.XtraReports.UI.XtraReport
    {
       public string LIST_HOPDONG_ID;
       public int NAMQUYETTOAN;
       public HopDong HD;
       public String MaNPL;
       public String MaSP;
       public String TenSP;
       public decimal LUONGTONDK = 0;
       public decimal TRIGIATONDK = 0;
       public decimal LUONGNHAPTK = 0;
       public decimal TRIGIANHAPTK = 0;
       public decimal LUONGXUATTK = 0;
       public decimal TRIGIAXUATTK = 0;
       public decimal LUONGTONCK = 0;
       public decimal TRIGIATONCK = 0;
       public bool isNumber = true;
       public DataTable dtNPLTotal = new DataTable();
       public DataTable dtSPTotal = new DataTable();
       public KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem;
        public BaoCaoQuyetToan_Mau15_TT38_Total()
        {
            InitializeComponent();
        }

        public void BinReportBCQTKho(bool isNumber ,List<T_KHOKETOAN_BCQT> NPLBCQT ,List<T_KHOKETOAN_BCQT> SPBCQT)
        {
            lbNam.Text = NAMQUYETTOAN.ToString();
            lblTonDauKy_T.Text = "Tồn đầu kỳ (01/01/" + NAMQUYETTOAN + ")";
            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            DetailReport.DataSource = NPLBCQT;
            DetailReport1.DataSource = SPBCQT;
            if (isNumber)
            {
                lblT_SoTien.Text = "Số lượng";
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENHANGHOA");
                txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MAHANGHOA");
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGTONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGNHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGTONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GHICHU");

                txtST.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
                txtTenSP.DataBindings.Add("Text", DetailReport1.DataSource, "TENHANGHOA");
                txtMaSP.DataBindings.Add("Text", DetailReport1.DataSource, "MAHANGHOA");
                txtTonDK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGTONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGNHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGTONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            }
            else
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    lblT_SoTien.Text = "Số tiền (VNĐ)";
                }
                else
                {
                    lblT_SoTien.Text = "Số tiền (USD)";
                }
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENHANGHOA");
                txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MAHANGHOA");
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIATONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIANHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIAXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIATONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));

                txtST.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
                txtTenSP.DataBindings.Add("Text", DetailReport1.DataSource, "TENHANGHOA");
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    txtMaSP.DataBindings.Add("Text", DetailReport1.DataSource, "MAHANGHOA");
                }
                txtTonDK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIATONDAU", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIANHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIAXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIATONCUOI", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            }
        }
        public void BindReport(bool isNumber)
        {
            if (HD != null)
            {
                GetDataSource();
            }
            else
            {
                ProcessReport();
            }
            //if (dtNPLTotal.Rows.Count > 0 )
            //{
            //    DetailReport.DataSource = dtNPLTotal;
            //}
            //if (dtSPTotal.Rows.Count > 0)
            //{
            //    DetailReport1.DataSource = dtSPTotal;
            //}
            lbNam.Text = NAMQUYETTOAN.ToString();
            lblTonDauKy_T.Text = "Tồn đầu kỳ (01/01/"+ NAMQUYETTOAN +")";
            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;
            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            if (isNumber)
            {
                lblT_SoTien.Text = "Số lượng";
                txtSTT.DataBindings.Add("Text",DetailReport.DataSource,"STT");
                txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENNPL");
                txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MANPL");
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGTONDK", Company.KDT.SHARE.Components.Globals.FormatNumber(5,true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGNHAPTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGXUATTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LUONGTONCK", Company.KDT.SHARE.Components.Globals.FormatNumber(5,true));
                txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GHICHU");

                txtST.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
                txtTenSP.DataBindings.Add("Text", DetailReport1.DataSource, "TENSP");
                if (GlobalSettings.MA_DON_VI != "4000395355") 
                {
                    txtMaSP.DataBindings.Add("Text", DetailReport1.DataSource, "MASP");
                }
                txtTonDK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGTONDK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGNHAPTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGXUATTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCK.DataBindings.Add("Text", DetailReport1.DataSource, "LUONGTONCK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            }
            else
            {
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    lblT_SoTien.Text = "Số tiền (VNĐ)";
                }
                else
                {
                    lblT_SoTien.Text = "Số tiền (USD)";
                }
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENNPL");
                txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MANPL");
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIATONDK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIANHAPTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIAXUATTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIATONCK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));

                txtST.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
                txtTenSP.DataBindings.Add("Text", DetailReport1.DataSource, "TENSP");
                if (GlobalSettings.MA_DON_VI != "4000395355")
                {
                    txtMaSP.DataBindings.Add("Text", DetailReport1.DataSource, "MASP");
                }
                txtTonDK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIATONDK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtNhapTK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIANHAPTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtXuatTK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIAXUATTK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
                txtTonCK.DataBindings.Add("Text", DetailReport1.DataSource, "TRIGIATONCK", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            }

        }
        private DataTable GetGroupedBy(DataTable dt, string columnNamesInDt, string groupByColumnNames, string typeOfCalculation)
        {
            //Return its own if the column names are empty
            if (columnNamesInDt == string.Empty || groupByColumnNames == string.Empty)
            {
                return dt;
            }

            //Once the columns are added find the distinct rows and group it bu the numbet
            DataTable _dt = dt.DefaultView.ToTable(true, groupByColumnNames);

            //The column names in data table
            string[] _columnNamesInDt = columnNamesInDt.Split(',');

            for (int i = 0; i < _columnNamesInDt.Length; i = i + 1)
            {
                if (_columnNamesInDt[i] != groupByColumnNames)
                {
                    _dt.Columns.Add(_columnNamesInDt[i]);
                }
            }

            //Gets the collection and send it back
            for (int i = 0; i < _dt.Rows.Count; i = i + 1)
            {
                for (int j = 0; j < _columnNamesInDt.Length; j = j + 1)
                {
                    if (_columnNamesInDt[j] != groupByColumnNames)
                    {
                        _dt.Rows[i][j] = dt.Compute(typeOfCalculation + "(" + _columnNamesInDt[j] + ")", groupByColumnNames + " = '" + _dt.Rows[i][groupByColumnNames].ToString() + "'");
                    }
                }
            }

            return _dt;
        }
        public void ProcessReport()
        {
            DataTable dtNPL = new DataTable();
            DataColumn[] dtColumnNPL = new DataColumn[15];
            dtColumnNPL[1] = new DataColumn("STT", typeof(string));
            dtColumnNPL[2] = new DataColumn("MANPL", typeof(string));
            dtColumnNPL[3] = new DataColumn("TENNPL", typeof(string));
            dtColumnNPL[4] = new DataColumn("LUONGTONDK", typeof(decimal));
            dtColumnNPL[5] = new DataColumn("TRIGIATONDK", typeof(decimal));
            dtColumnNPL[6] = new DataColumn("LUONGNHAPTK", typeof(decimal));
            dtColumnNPL[7] = new DataColumn("TRIGIANHAPTK", typeof(decimal));
            dtColumnNPL[8] = new DataColumn("LUONGXUATTK", typeof(decimal));
            dtColumnNPL[9] = new DataColumn("TRIGIAXUATTK", typeof(decimal));
            dtColumnNPL[10] = new DataColumn("LUONGTONCK", typeof(decimal));
            dtColumnNPL[11] = new DataColumn("TRIGIATONCK", typeof(decimal));
            dtColumnNPL[12] = new DataColumn("GHICHU", typeof(string));

            dtNPL.Columns.Add(dtColumnNPL[1]);
            dtNPL.Columns.Add(dtColumnNPL[2]);
            dtNPL.Columns.Add(dtColumnNPL[3]);
            dtNPL.Columns.Add(dtColumnNPL[4]);
            dtNPL.Columns.Add(dtColumnNPL[5]);
            dtNPL.Columns.Add(dtColumnNPL[6]);
            dtNPL.Columns.Add(dtColumnNPL[7]);
            dtNPL.Columns.Add(dtColumnNPL[8]);
            dtNPL.Columns.Add(dtColumnNPL[9]);
            dtNPL.Columns.Add(dtColumnNPL[10]);
            dtNPL.Columns.Add(dtColumnNPL[11]);
            dtNPL.Columns.Add(dtColumnNPL[12]);

            DataTable dtSP = dtNPL.Clone();
            if (GlobalSettings.MA_DON_VI != "4000395355")
            {
                DataColumn dtColumnMaSP = new DataColumn("MASP");
                dtColumnMaSP.DataType = typeof(System.String);
                dtSP.Columns.Add(dtColumnMaSP);
            }
            DataColumn dtColumnTenSP = new DataColumn("TENSP");
            dtColumnTenSP.DataType = typeof(System.String); 
            dtSP.Columns.Add(dtColumnTenSP);
            foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail item in goodItem.GoodItemsCollection)
            {
                if (item.LoaiHangHoa == 1)
                {
                    DataRow dr = dtNPL.NewRow();
                    dr["STT"] = item.STT.ToString();
                    dr["MANPL"] = item.MaHangHoa.ToString();
                    dr["TENNPL"] = item.TenHangHoa.ToString();
                    dr["LUONGTONDK"] = item.TonDauKy.ToString();
                    dr["TRIGIATONDK"] = item.TonDauKy.ToString();
                    dr["LUONGNHAPTK"] = item.NhapTrongKy.ToString();
                    dr["TRIGIANHAPTK"] = item.NhapTrongKy.ToString();
                    dr["LUONGXUATTK"] = item.XuatTrongKy.ToString();
                    dr["TRIGIAXUATTK"] = item.XuatTrongKy.ToString();
                    dr["LUONGTONCK"] = item.TonCuoiKy.ToString();
                    dr["TRIGIATONCK"] = item.TonCuoiKy.ToString();
                    dr["GHICHU"] = item.GhiChu.ToString();
                    dtNPL.Rows.Add(dr);
                }
                else
                {
                    DataRow dr = dtSP.NewRow();
                    dr["STT"] = item.STT.ToString();
                    if (GlobalSettings.MA_DON_VI != "4000395355")
                    {
                        dr["MASP"] = item.MaHangHoa.ToString();
                    }
                    dr["TENSP"] = item.TenHangHoa.ToString();
                    dr["LUONGTONDK"] = item.TonDauKy.ToString();
                    dr["TRIGIATONDK"] = item.TonDauKy.ToString();
                    dr["LUONGNHAPTK"] = item.NhapTrongKy.ToString();
                    dr["TRIGIANHAPTK"] = item.NhapTrongKy.ToString();
                    dr["LUONGXUATTK"] = item.XuatTrongKy.ToString();
                    dr["TRIGIAXUATTK"] = item.XuatTrongKy.ToString();
                    dr["LUONGTONCK"] = item.TonCuoiKy.ToString();
                    dr["TRIGIATONCK"] = item.TonCuoiKy.ToString();
                    dr["GHICHU"] = item.GhiChu.ToString();
                    dtSP.Rows.Add(dr);
                }
            }
            DetailReport.DataSource = dtNPL;
            DetailReport1.DataSource = dtSP;
        }
        public void GetDataSource()
        {
            DataTable dtbNPL = Company.GC.BLL.GC.NguyenPhuLieu.SelectAll().Tables[0];
            DataTable dtbSP = Company.GC.BLL.GC.SanPham.SelectAll().Tables[0];
            DataTable dtNPL = new DataTable();
            DataTable dtSPGroupedBy = new DataTable();
            DataTable dtSP = new DataTable();
            if (GlobalSettings.MA_DON_VI == "4000395355")
            {
                dtNPL = T_GC_NPL_QUYETOAN.SelectDynamicNew("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
                dtSP = T_GC_SANPHAM_QUYETOAN.SelectDynamicNew("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
            }
            else
            {
                dtNPL = T_GC_NPL_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MANPL").Tables[0];
                dtSP = T_GC_SANPHAM_QUYETOAN.SelectDynamic("HOPDONG_ID IN (" + LIST_HOPDONG_ID + ") AND NAMQUYETTOAN =" + NAMQUYETTOAN + "", "MASP").Tables[0];
            }

            DataColumn dtColumnSTT = new DataColumn("STT");
            dtColumnSTT.DataType = typeof(System.String);
            dtNPL.Columns.Add(dtColumnSTT);

            DataColumn dtColumnST = new DataColumn("STT");
            dtColumnST.DataType = typeof(System.String);
            dtSP.Columns.Add(dtColumnST);
            int i = 0;
            DataTable dtNPLTotal = dtNPL.Clone();
            DataTable dtSPTotal = dtSP.Clone();
            DataTable dtGroupedBy = GetGroupedBy(dtNPL, "MANPL,LUONGTONDK,TRIGIATONDK,LUONGNHAPTK,TRIGIANHAPTK,LUONGXUATTK,TRIGIAXUATTK,LUONGTONCK,TRIGIATONCK", "MANPL", "Sum");
            if (GlobalSettings.MA_DON_VI != "4000395355")
            {
                dtSPGroupedBy = GetGroupedBy(dtSP, "MASP,LUONGTONDK,TRIGIATONDK,LUONGNHAPTK,TRIGIANHAPTK,LUONGXUATTK,TRIGIAXUATTK,LUONGTONCK,TRIGIATONCK", "MASP", "Sum");
            }
            else
            {
                dtSPGroupedBy = GetGroupedBy(dtSP, "MASP,LUONGTONDK,TRIGIATONDK,LUONGNHAPTK,TRIGIANHAPTK,LUONGXUATTK,TRIGIAXUATTK,LUONGTONCK,TRIGIATONCK", "MASP", "Sum");
            }
            try
            {
                foreach (DataRow dr in dtGroupedBy.Rows)
                {
                    try
                    {
                        DataRow drNew = dtNPLTotal.NewRow();
                        i++;
                        drNew["STT"] = i;
                        MaNPL = dr["MANPL"].ToString();
                        drNew["MANPL"] = MaNPL;
                        DataRow[] drNPL ;
                        if (HD != null)
                        {
                             drNPL = dtbNPL.Select("Ma='" + MaNPL + "' AND HopDong_ID = " + HD.ID);
                             drNew["TENNPL"] = drNPL[0]["Ten"].ToString();
                             drNew["DVT"] = drNPL[0]["TenDVT"].ToString();
                        }
                        else
                        {
                            string[] temp = LIST_HOPDONG_ID.Split(new string[] { "," }, StringSplitOptions.None);
                            decimal HopDong_ID;
                            HopDong_ID = Convert.ToDecimal(temp[0].ToString());
                            //HD.ID = LIST_HOPDONG_ID.Substring(1,)
                            drNPL = dtbNPL.Select("Ma='" + MaNPL + "' AND HopDong_ID = " + HopDong_ID);
                            drNew["TENNPL"] = drNPL[0]["Ten"].ToString();
                            drNew["DVT"] = drNPL[0]["TenDVT"].ToString();
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONDK"].ToString()))
                        {
                            LUONGTONDK = 0;
                            TRIGIATONDK = 0;
                        }
                        else
                        {
                            LUONGTONDK = Convert.ToDecimal(dr["LUONGTONDK"].ToString());
                            TRIGIATONDK = Convert.ToDecimal(dr["TRIGIATONDK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGNHAPTK"].ToString()))
                        {
                            LUONGNHAPTK = 0;
                            TRIGIANHAPTK = 0;
                        }
                        else
                        {
                            LUONGNHAPTK = Convert.ToDecimal(dr["LUONGNHAPTK"].ToString());
                            TRIGIANHAPTK = Convert.ToDecimal(dr["TRIGIANHAPTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGXUATTK"].ToString()))
                        {
                            LUONGXUATTK = 0;
                            TRIGIAXUATTK = 0;
                        }
                        else
                        {
                            LUONGXUATTK = Convert.ToDecimal(dr["LUONGXUATTK"].ToString());
                            TRIGIAXUATTK = Convert.ToDecimal(dr["TRIGIAXUATTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONCK"].ToString()))
                        {
                            LUONGTONCK = 0;
                            TRIGIATONCK = 0;
                        }
                        else
                        {
                            LUONGTONCK = Convert.ToDecimal(dr["LUONGTONCK"].ToString());
                            TRIGIATONCK = Convert.ToDecimal(dr["TRIGIATONCK"].ToString());
                        }
                        drNew["LUONGTONDK"] = LUONGTONDK;
                        drNew["TRIGIATONDK"] = TRIGIATONDK;
                        drNew["LUONGNHAPTK"] = LUONGNHAPTK;
                        drNew["TRIGIANHAPTK"] = TRIGIANHAPTK;
                        drNew["LUONGXUATTK"] = LUONGXUATTK;
                        drNew["TRIGIAXUATTK"] = TRIGIAXUATTK;
                        drNew["LUONGTONCK"] = LUONGTONCK;
                        drNew["TRIGIATONCK"] = TRIGIATONCK;
                        dtNPLTotal.Rows.Add(drNew);
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi tại Mã NPL : "+ MaNPL.ToString());
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }                    
                }
                i = 0;
                foreach (DataRow dr in dtSPGroupedBy.Rows)
                {
                    try
                    {
                        DataRow drNew = dtSPTotal.NewRow();
                        i++;
                        if (GlobalSettings.MA_DON_VI != "4000395355")
                        {
                            drNew["STT"] = i;
                            MaSP = dr["MASP"].ToString();
                            //drNew["TENSP"] = MaSP;
                            DataRow[] drSP = dtbSP.Select("Ma= '" + MaSP + "' AND HopDong_ID =" + HD.ID);
                            //MaSP = drSP[0]["Ma"].ToString();
                            TenSP = drSP[0]["Ten"].ToString();
                            drNew["MASP"] = MaSP;
                            drNew["TENSP"] = TenSP;
                            drNew["DVT"] = drSP[0]["TenDVT"].ToString();
                        }
                        else
                        {
                            drNew["STT"] = i;
                            MaSP = dr["MASP"].ToString();
                            DataRow[] drSP = dtbSP.Select(" SUBSTRING(Ma,1,1)= '" + MaSP + "'");
                            MaSP = drSP[0]["Ma"].ToString();
                            TenSP = drSP[0]["Ten"].ToString().ToUpper();
                            //string[] Temp = TenSP.Split(new string[] { MaSP.Substring(1,MaSP.Length-1) }, StringSplitOptions.None);
                            string[] TempNew = TenSP.Split(new string[] { "GIÀY" }, StringSplitOptions.None);
                            string[] TempOne = TenSP.Split(new string[] { "ĐẾ" }, StringSplitOptions.None);
                            string[] TempTwo = TenSP.Split(new string[] { "DÂY" }, StringSplitOptions.None);
                            if (TempNew.Length > 0)
                            {
                                //TenSP = Temp[0].ToString();
                                if (TenSP.Contains("GIÀY"))
                                {
                                    if (TempNew[0].ToString().ToUpper().Contains("NHÃN"))
                                    {
                                        TenSP = TempNew[0].Replace("NHÃN", "") +" GIÀY CÁC LOẠI";
                                    }
                                    else
                                    {
                                        TenSP = TempNew[0].ToString() + " GIÀY CÁC LOẠI";
                                    }
                                }
                                else
                                {
                                    if (TempNew[0].ToUpper().Contains("09 CM"))
                                    {
                                        TenSP = TempNew[0].ToUpper().ToString().Replace("09 CM", "") + " CÁC LOẠI";
                                    }
                                    else
                                    {
                                        TenSP = TempNew[0].ToString() + " CÁC LOẠI";
                                    }
                                }
                            }
                            drNew["MASP"] = String.Empty;
                            drNew["TENSP"] = TenSP;
                            drNew["DVT"] = drSP[0]["TenDVT"].ToString();
                        }

                        if (String.IsNullOrEmpty(dr["LUONGTONDK"].ToString()))
                        {
                            LUONGTONDK = 0;
                            TRIGIATONDK = 0;
                        }
                        else
                        {
                            LUONGTONDK = Convert.ToDecimal(dr["LUONGTONDK"].ToString());
                            TRIGIATONDK = Convert.ToDecimal(dr["TRIGIATONDK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGNHAPTK"].ToString()))
                        {
                            LUONGNHAPTK = 0;
                            TRIGIANHAPTK = 0;
                        }
                        else
                        {
                            LUONGNHAPTK = Convert.ToDecimal(dr["LUONGNHAPTK"].ToString());
                            TRIGIANHAPTK = Convert.ToDecimal(dr["TRIGIANHAPTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGXUATTK"].ToString()))
                        {
                            LUONGXUATTK = 0;
                            TRIGIAXUATTK = 0;
                        }
                        else
                        {
                            LUONGXUATTK = Convert.ToDecimal(dr["LUONGXUATTK"].ToString());
                            TRIGIAXUATTK = Convert.ToDecimal(dr["TRIGIAXUATTK"].ToString());
                        }
                        if (String.IsNullOrEmpty(dr["LUONGTONCK"].ToString()))
                        {
                            LUONGTONCK = 0;
                            TRIGIATONCK = 0;
                        }
                        else
                        {
                            LUONGTONCK = Convert.ToDecimal(dr["LUONGTONCK"].ToString());
                            TRIGIATONCK = Convert.ToDecimal(dr["TRIGIATONCK"].ToString());
                        }
                        drNew["LUONGTONDK"] = LUONGTONDK;
                        drNew["TRIGIATONDK"] = TRIGIATONDK;
                        drNew["LUONGNHAPTK"] = LUONGNHAPTK;
                        drNew["TRIGIANHAPTK"] = TRIGIANHAPTK;
                        drNew["LUONGXUATTK"] = LUONGXUATTK;
                        drNew["TRIGIAXUATTK"] = TRIGIAXUATTK;
                        drNew["LUONGTONCK"] = LUONGTONCK;
                        drNew["TRIGIATONCK"] = TRIGIATONCK;
                        dtSPTotal.Rows.Add(drNew);
                    }
                    catch (Exception ex)
                    {
                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("Lỗi tại Mã SP: "+ MaSP.ToString());
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }            
            DetailReport.DataSource = dtNPLTotal;
            DetailReport1.DataSource = dtSPTotal;
            }
    }
}
