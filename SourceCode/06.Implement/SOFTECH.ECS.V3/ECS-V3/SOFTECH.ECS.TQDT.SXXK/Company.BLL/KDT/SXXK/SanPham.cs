using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.KDT.SXXK
{
    public partial class SanPham
    {
        public static SanPhamCollection SelectCollectionBy_Master_ID(long master_ID, SqlTransaction transaction)
        {
            string spName = "[dbo].p_KDT_SXXK_SanPham_SelectBy_Master_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, master_ID);

            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            SanPhamCollection collection = new SanPhamCollection();
            while (reader.Read())
            {
                SanPham entity = new SanPham();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;

        }

        #region Delete methods.

        public static int Delete(long iD, string MaDoanhNghiep, string MaHaiQuan)
        {
            SanPham entity = SanPham.Load(iD);
            BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
            spSXXK.Ma = entity.Ma;
            spSXXK.MaDoanhNghiep = MaDoanhNghiep;
            spSXXK.MaHaiQuan = MaHaiQuan;
            spSXXK.Delete();
            return entity.Delete(MaDoanhNghiep, MaHaiQuan);
        }

        public int Delete(string MaDoanhNghiep, string MaHaiQuan)
        {
            return this.DeleteTransaction(null, MaDoanhNghiep, MaHaiQuan);
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(SanPhamCollection collection, string MaDoanhNghiep, string MaHaiQuan)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (SanPham item in collection)
                    {
                        if (item.DeleteTransaction(transaction, MaDoanhNghiep, MaHaiQuan) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                        BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
                        spSXXK.Ma = item.Ma;
                        spSXXK.MaDoanhNghiep = MaDoanhNghiep;
                        spSXXK.MaHaiQuan = MaHaiQuan;
                        spSXXK.DeleteTransaction(transaction);
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------

        public static void DeleteCollection(SanPhamCollection collection, SqlTransaction transaction, string MaDoanhNghiep, string MaHaiQuan)
        {
            foreach (SanPham item in collection)
            {
                item.DeleteTransaction(transaction, MaDoanhNghiep, MaHaiQuan);
            }
        }

        public int DeleteTransaction(SqlTransaction transaction, string MaDoanhNghiep, string MaHaiQuan)
        {
            string spName = "[dbo].p_KDT_SXXK_SanPham_Delete";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            BLL.SXXK.SanPham spSXXK = new Company.BLL.SXXK.SanPham();
            spSXXK.Ma = this.Ma;
            spSXXK.MaDoanhNghiep = MaDoanhNghiep;
            spSXXK.MaHaiQuan = MaHaiQuan;


            if (transaction != null)
            {
                spSXXK.DeleteTransaction(transaction);
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                spSXXK.Delete();
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        #endregion

        public bool CloneToDB(SqlTransaction transaction)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand("p_KDT_SXXK_SanPham_Copy");
                db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
                if (transaction != null)
                    db.ExecuteNonQuery(dbCommand, transaction);
                else
                    db.ExecuteNonQuery(dbCommand);
                return true;
            }
            catch
            {
                return false;
            }
        }
        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransactionBy(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_KDT_SXXK_SanPham_InsertUpdateBy";

            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
                
        public SanPhamCollection LoadSanPhamCollection(string MaHaiQuan, string MaDoanhNghiep, bool trangThaiDaDuyet, DateTime tuNgay, DateTime denNgay)
        {
            string query = "SELECT  SP.ID, SP.Master_ID,  " +
                            "SP.Ma AS Ma,  " +
                            "SP.Ten AS Ten,  " +
                            "SP.MaHS AS MaHS,  " +
                            "SP.DVT_ID AS DVT_ID " +
                            "FROM    dbo.t_KDT_SXXK_SanPham SP " +
                            "INNER JOIN dbo.t_KDT_SXXK_SanPhamDangKy SPDK ON SP.Master_ID = SPDK.ID " +
                            "WHERE   SPDK.MaHaiQuan = '" + MaHaiQuan + "' " +
                            "AND SPDK.MaDoanhNghiep = '" + MaDoanhNghiep + "' ";
            if (trangThaiDaDuyet)
                query += " AND SPDK.TrangThaiXuLy = 1";
            if (tuNgay > DateTime.MinValue)
                query += " and SPDK.NgayTiepNhan >= '" + tuNgay.ToString("yyyy-MM-dd 00:00:00") + "' AND SPDK.NgayTiepNhan <= '" + denNgay.ToString("yyyy-MM-dd 23:59:59") + "'";
            query += " ORDER BY SPDK.NgayTiepNhan";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            DbCommand cmd = db.GetSqlStringCommand(query);

            IDataReader reader = db.ExecuteReader(cmd);
            SanPhamCollection collection = new SanPhamCollection();
            while (reader.Read())
            {
                SanPham entity = new SanPham();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        private string _MucDich;
        private string _LoaiSP;
        public string LoaiSP
        {
            set { this._LoaiSP = value; }
            get { return this._LoaiSP; }
        }
        public string MucDich
        {
            set { this._MucDich = value; }
            get { return this._MucDich; }
        }
        private string _SoLuong;
        public string SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }
        private string _LuongTonThucTe;
        public string LuongTonThucTe
        {
            set { this._LuongTonThucTe = value; }
            get { return this._LuongTonThucTe; }
        }
    }
}