﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.VNACCS
{
    public partial class ShowMessagesEDI : BaseForm
    {
        public ShowMessagesEDI(string Msg)
        {
            InitializeComponent();
            text = Msg;
        }
        string text;
        private void ShowMessagesEDI_Load(object sender, EventArgs e)
        {
            richTextBox1.Text = text;
        }
        
    }
}
