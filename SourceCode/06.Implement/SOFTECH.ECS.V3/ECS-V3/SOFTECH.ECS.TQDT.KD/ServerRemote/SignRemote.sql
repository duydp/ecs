/*
Run this script on:

        192.168.72.151.Data_empty    -  This database will be modified

to synchronize it with:

        192.168.72.151.LICENSESv2

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 08/06/2012 3:45:10 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_DaiLy_CauHinh]'
GO
CREATE TABLE [dbo].[t_DaiLy_CauHinh]
(
[MaDoanhNghiep] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenDoanhNghiep] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Value] [int] NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsUse] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_DaiLy_CauHinh] on [dbo].[t_DaiLy_CauHinh]'
GO
ALTER TABLE [dbo].[t_DaiLy_CauHinh] ADD CONSTRAINT [PK_t_DaiLy_CauHinh] PRIMARY KEY CLUSTERED  ([MaDoanhNghiep])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_Insert]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_Insert]
	@MaDoanhNghiep char(15),
	@TenDoanhNghiep nvarchar(50),
	@Value int,
	@GhiChu nvarchar(150),
	@IsUse int
AS
INSERT INTO [dbo].[t_DaiLy_CauHinh]
(
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[Value],
	[GhiChu],
	[IsUse]
)
VALUES
(
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@Value,
	@GhiChu,
	@IsUse
)

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_Update]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_Update]
	@MaDoanhNghiep char(15),
	@TenDoanhNghiep nvarchar(50),
	@Value int,
	@GhiChu nvarchar(150),
	@IsUse int
AS

UPDATE
	[dbo].[t_DaiLy_CauHinh]
SET
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[Value] = @Value,
	[GhiChu] = @GhiChu,
	[IsUse] = @IsUse
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_InsertUpdate]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_InsertUpdate]
	@MaDoanhNghiep char(15),
	@TenDoanhNghiep nvarchar(50),
	@Value int,
	@GhiChu nvarchar(150),
	@IsUse int
AS
IF EXISTS(SELECT [MaDoanhNghiep] FROM [dbo].[t_DaiLy_CauHinh] WHERE [MaDoanhNghiep] = @MaDoanhNghiep)
	BEGIN
		UPDATE
			[dbo].[t_DaiLy_CauHinh] 
		SET
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[Value] = @Value,
			[GhiChu] = @GhiChu,
			[IsUse] = @IsUse
		WHERE
			[MaDoanhNghiep] = @MaDoanhNghiep
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_DaiLy_CauHinh]
	(
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[Value],
			[GhiChu],
			[IsUse]
	)
	VALUES
	(
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@Value,
			@GhiChu,
			@IsUse
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_Delete]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_Delete]
	@MaDoanhNghiep char(15)
AS

DELETE FROM 
	[dbo].[t_DaiLy_CauHinh]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_Load]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_Load]
	@MaDoanhNghiep char(15)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[Value],
	[GhiChu],
	[IsUse]
FROM
	[dbo].[t_DaiLy_CauHinh]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_SelectAll]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[Value],
	[GhiChu],
	[IsUse]
FROM
	[dbo].[t_DaiLy_CauHinh]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_MSG_DBDL]'
GO
CREATE TABLE [dbo].[t_MSG_DBDL]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[MSG_FROM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_ORIGIN] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_TIME] [datetime] NULL,
[STATUS] [int] NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TYPE] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_MSG_DBDL] on [dbo].[t_MSG_DBDL]'
GO
ALTER TABLE [dbo].[t_MSG_DBDL] ADD CONSTRAINT [PK_t_MSG_DBDL] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Insert]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Insert]
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN varchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@MSG_TYPE varchar(50),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_MSG_DBDL]
(
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR],
	[MSG_TYPE]
)
VALUES 
(
	@MSG_FROM,
	@MSG_TO,
	@MSG_ORIGIN,
	@CREATE_TIME,
	@STATUS,
	@GUIDSTR,
	@MSG_TYPE
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Update]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Update]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN varchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@MSG_TYPE varchar(50)
AS

UPDATE
	[dbo].[t_MSG_DBDL]
SET
	[MSG_FROM] = @MSG_FROM,
	[MSG_TO] = @MSG_TO,
	[MSG_ORIGIN] = @MSG_ORIGIN,
	[CREATE_TIME] = @CREATE_TIME,
	[STATUS] = @STATUS,
	[GUIDSTR] = @GUIDSTR,
	[MSG_TYPE] = @MSG_TYPE
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_InsertUpdate]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_InsertUpdate]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN varchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@MSG_TYPE varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_MSG_DBDL] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_MSG_DBDL] 
		SET
			[MSG_FROM] = @MSG_FROM,
			[MSG_TO] = @MSG_TO,
			[MSG_ORIGIN] = @MSG_ORIGIN,
			[CREATE_TIME] = @CREATE_TIME,
			[STATUS] = @STATUS,
			[GUIDSTR] = @GUIDSTR,
			[MSG_TYPE] = @MSG_TYPE
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_MSG_DBDL]
		(
			[MSG_FROM],
			[MSG_TO],
			[MSG_ORIGIN],
			[CREATE_TIME],
			[STATUS],
			[GUIDSTR],
			[MSG_TYPE]
		)
		VALUES 
		(
			@MSG_FROM,
			@MSG_TO,
			@MSG_ORIGIN,
			@CREATE_TIME,
			@STATUS,
			@GUIDSTR,
			@MSG_TYPE
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Delete]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_MSG_DBDL]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Load]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR],
	[MSG_TYPE]
FROM
	[dbo].[t_MSG_DBDL]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_SelectAll]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR],
	[MSG_TYPE]
FROM
	[dbo].[t_MSG_DBDL]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_MSG_DBDL_Detail]'
GO
CREATE TABLE [dbo].[t_MSG_DBDL_Detail]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoToKhai] [bigint] NULL,
[SoTiepNhan] [bigint] NULL,
[MaLoaiHinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDangKy] [datetime] NULL,
[PhanLuong] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHaiQuan] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhanHe] [char] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDoanhNghiep] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenDaiLy] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDaiLy] [nchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayGui] [datetime] NULL,
[TrangThai] [int] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_MSG_DBDL_Detail] on [dbo].[t_MSG_DBDL_Detail]'
GO
ALTER TABLE [dbo].[t_MSG_DBDL_Detail] ADD CONSTRAINT [PK_t_MSG_DBDL_Detail] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_LoadCollection]'
GO
CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_LoadCollection]
	@MaDoanhNghiep nchar(15),
	@TuNgay DATETIME,
	@DenNgay DATETIME,
	@PhanHe CHAR(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[SoTiepNhan],
	[MaLoaiHinh],
	[NgayDangKy],
	[PhanLuong],
	[MaHaiQuan],
	[PhanHe],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[TenDaiLy],
	[MaDaiLy],
	[GUIDSTR],
	[NgayGui],
	[TrangThai]
FROM
	[dbo].[t_MSG_DBDL_Detail]
WHERE
	([PhanHe]= @PhanHe AND [MaDoanhNghiep] = @MaDoanhNghiep) AND ([NgayGui] BETWEEN @TuNgay AND @DenNgay) 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_LoadByGuiStr]'
GO
CREATE PROCEDURE [dbo].[p_MSG_DBDL_LoadByGuiStr]
	@GUIDSTR nvarchar(36)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR],
	[MSG_TYPE]
FROM
	[dbo].[t_MSG_DBDL]
WHERE
	GUIDSTR = @GUIDSTR
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_DeleteByGuiStr]'
GO
CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_DeleteByGuiStr]
	@GUIDSTR nvarchar(36)
AS

DELETE FROM 
	[dbo].[t_MSG_DBDL_Detail]
WHERE
	[GUIDSTR] = @GUIDSTR

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_LoadByGuiStr]'
GO
CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_LoadByGuiStr]
	@GUIDSTR nvarchar(36)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[SoTiepNhan],
	[MaLoaiHinh],
	[NgayDangKy],
	[PhanLuong],
	[MaHaiQuan],
	[PhanHe],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[TenDaiLy],
	[MaDaiLy],
	[GUIDSTR],
	[NgayGui],
	[TrangThai]
FROM
	[dbo].[t_MSG_DBDL_Detail]
WHERE
	GUIDSTR = @GUIDSTR
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_Insert]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_Insert]
	@SoToKhai bigint,
	@SoTiepNhan bigint,
	@MaLoaiHinh nvarchar(50),
	@NgayDangKy datetime,
	@PhanLuong nvarchar(255),
	@MaHaiQuan char(6),
	@PhanHe char(6),
	@TenDoanhNghiep nvarchar(500),
	@MaDoanhNghiep nchar(15),
	@TenDaiLy nvarchar(500),
	@MaDaiLy nchar(15),
	@GUIDSTR nvarchar(36),
	@NgayGui datetime,
	@TrangThai int,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_MSG_DBDL_Detail]
(
	[SoToKhai],
	[SoTiepNhan],
	[MaLoaiHinh],
	[NgayDangKy],
	[PhanLuong],
	[MaHaiQuan],
	[PhanHe],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[TenDaiLy],
	[MaDaiLy],
	[GUIDSTR],
	[NgayGui],
	[TrangThai]
)
VALUES 
(
	@SoToKhai,
	@SoTiepNhan,
	@MaLoaiHinh,
	@NgayDangKy,
	@PhanLuong,
	@MaHaiQuan,
	@PhanHe,
	@TenDoanhNghiep,
	@MaDoanhNghiep,
	@TenDaiLy,
	@MaDaiLy,
	@GUIDSTR,
	@NgayGui,
	@TrangThai
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_Update]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_Update]
	@ID bigint,
	@SoToKhai bigint,
	@SoTiepNhan bigint,
	@MaLoaiHinh nvarchar(50),
	@NgayDangKy datetime,
	@PhanLuong nvarchar(255),
	@MaHaiQuan char(6),
	@PhanHe char(6),
	@TenDoanhNghiep nvarchar(500),
	@MaDoanhNghiep nchar(15),
	@TenDaiLy nvarchar(500),
	@MaDaiLy nchar(15),
	@GUIDSTR nvarchar(36),
	@NgayGui datetime,
	@TrangThai int
AS

UPDATE
	[dbo].[t_MSG_DBDL_Detail]
SET
	[SoToKhai] = @SoToKhai,
	[SoTiepNhan] = @SoTiepNhan,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[PhanLuong] = @PhanLuong,
	[MaHaiQuan] = @MaHaiQuan,
	[PhanHe] = @PhanHe,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDaiLy] = @TenDaiLy,
	[MaDaiLy] = @MaDaiLy,
	[GUIDSTR] = @GUIDSTR,
	[NgayGui] = @NgayGui,
	[TrangThai] = @TrangThai
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_InsertUpdate]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_InsertUpdate]
	@ID bigint,
	@SoToKhai bigint,
	@SoTiepNhan bigint,
	@MaLoaiHinh nvarchar(50),
	@NgayDangKy datetime,
	@PhanLuong nvarchar(255),
	@MaHaiQuan char(6),
	@PhanHe char(6),
	@TenDoanhNghiep nvarchar(500),
	@MaDoanhNghiep nchar(15),
	@TenDaiLy nvarchar(500),
	@MaDaiLy nchar(15),
	@GUIDSTR nvarchar(36),
	@NgayGui datetime,
	@TrangThai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_MSG_DBDL_Detail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_MSG_DBDL_Detail] 
		SET
			[SoToKhai] = @SoToKhai,
			[SoTiepNhan] = @SoTiepNhan,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[PhanLuong] = @PhanLuong,
			[MaHaiQuan] = @MaHaiQuan,
			[PhanHe] = @PhanHe,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDaiLy] = @TenDaiLy,
			[MaDaiLy] = @MaDaiLy,
			[GUIDSTR] = @GUIDSTR,
			[NgayGui] = @NgayGui,
			[TrangThai] = @TrangThai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_MSG_DBDL_Detail]
		(
			[SoToKhai],
			[SoTiepNhan],
			[MaLoaiHinh],
			[NgayDangKy],
			[PhanLuong],
			[MaHaiQuan],
			[PhanHe],
			[TenDoanhNghiep],
			[MaDoanhNghiep],
			[TenDaiLy],
			[MaDaiLy],
			[GUIDSTR],
			[NgayGui],
			[TrangThai]
		)
		VALUES 
		(
			@SoToKhai,
			@SoTiepNhan,
			@MaLoaiHinh,
			@NgayDangKy,
			@PhanLuong,
			@MaHaiQuan,
			@PhanHe,
			@TenDoanhNghiep,
			@MaDoanhNghiep,
			@TenDaiLy,
			@MaDaiLy,
			@GUIDSTR,
			@NgayGui,
			@TrangThai
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_Delete]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_MSG_DBDL_Detail]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_Load]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[SoTiepNhan],
	[MaLoaiHinh],
	[NgayDangKy],
	[PhanLuong],
	[MaHaiQuan],
	[PhanHe],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[TenDaiLy],
	[MaDaiLy],
	[GUIDSTR],
	[NgayGui],
	[TrangThai]
FROM
	[dbo].[t_MSG_DBDL_Detail]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_SelectAll]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[SoTiepNhan],
	[MaLoaiHinh],
	[NgayDangKy],
	[PhanLuong],
	[MaHaiQuan],
	[PhanHe],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[TenDaiLy],
	[MaDaiLy],
	[GUIDSTR],
	[NgayGui],
	[TrangThai]
FROM
	[dbo].[t_MSG_DBDL_Detail]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_SignLogger]'
GO
CREATE TABLE [dbo].[t_SignLogger]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[MSG_FROM] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TO] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TIME_PROCESS] [datetime] NULL,
[MESSAGE] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DATECREATE] [datetime] NULL CONSTRAINT [DF_t_SignLogger_DATECREATE] DEFAULT (getdate())
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_SignLogger] on [dbo].[t_SignLogger]'
GO
ALTER TABLE [dbo].[t_SignLogger] ADD CONSTRAINT [PK_t_SignLogger] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_Insert]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_Insert]
	@MSG_FROM nvarchar(50),
	@MSG_TO nvarchar(50),
	@TIME_PROCESS datetime,
	@MESSAGE nvarchar(max),
	@DATECREATE datetime,
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_SignLogger]
(
	[MSG_FROM],
	[MSG_TO],
	[TIME_PROCESS],
	[MESSAGE],
	[DATECREATE]
)
VALUES 
(
	@MSG_FROM,
	@MSG_TO,
	@TIME_PROCESS,
	@MESSAGE,
	@DATECREATE
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_Update]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_Update]
	@ID bigint,
	@MSG_FROM nvarchar(50),
	@MSG_TO nvarchar(50),
	@TIME_PROCESS datetime,
	@MESSAGE nvarchar(max),
	@DATECREATE datetime
AS

UPDATE
	[dbo].[t_SignLogger]
SET
	[MSG_FROM] = @MSG_FROM,
	[MSG_TO] = @MSG_TO,
	[TIME_PROCESS] = @TIME_PROCESS,
	[MESSAGE] = @MESSAGE,
	[DATECREATE] = @DATECREATE
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_InsertUpdate]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_InsertUpdate]
	@ID bigint,
	@MSG_FROM nvarchar(50),
	@MSG_TO nvarchar(50),
	@TIME_PROCESS datetime,
	@MESSAGE nvarchar(max),
	@DATECREATE datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_SignLogger] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_SignLogger] 
		SET
			[MSG_FROM] = @MSG_FROM,
			[MSG_TO] = @MSG_TO,
			[TIME_PROCESS] = @TIME_PROCESS,
			[MESSAGE] = @MESSAGE,
			[DATECREATE] = @DATECREATE
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_SignLogger]
		(
			[MSG_FROM],
			[MSG_TO],
			[TIME_PROCESS],
			[MESSAGE],
			[DATECREATE]
		)
		VALUES 
		(
			@MSG_FROM,
			@MSG_TO,
			@TIME_PROCESS,
			@MESSAGE,
			@DATECREATE
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_Delete]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_SignLogger]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_Load]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[TIME_PROCESS],
	[MESSAGE],
	[DATECREATE]
FROM
	[dbo].[t_SignLogger]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_SelectAll]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[TIME_PROCESS],
	[MESSAGE],
	[DATECREATE]
FROM
	[dbo].[t_SignLogger]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_MSG_INBOX]'
GO
CREATE TABLE [dbo].[t_MSG_INBOX]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[MSG_FROM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_ORIGIN] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_SIGN_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_SIGN_CERT] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_TIME] [datetime] NULL CONSTRAINT [DF_t_MSG_INBOX_CREATE_TIME] DEFAULT (getdate()),
[STATUS] [int] NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t] on [dbo].[t_MSG_INBOX]'
GO
ALTER TABLE [dbo].[t_MSG_INBOX] ADD CONSTRAINT [PK_t] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_Insert]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_Insert]
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN nvarchar(max),
	@MSG_SIGN_DATA nvarchar(max),
	@MSG_SIGN_CERT nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_MSG_INBOX]
(
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
)
VALUES 
(
	@MSG_FROM,
	@MSG_TO,
	@MSG_ORIGIN,
	@MSG_SIGN_DATA,
	@MSG_SIGN_CERT,
	@CREATE_TIME,
	@STATUS,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_Update]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_Update]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN nvarchar(max),
	@MSG_SIGN_DATA nvarchar(max),
	@MSG_SIGN_CERT nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_MSG_INBOX]
SET
	[MSG_FROM] = @MSG_FROM,
	[MSG_TO] = @MSG_TO,
	[MSG_ORIGIN] = @MSG_ORIGIN,
	[MSG_SIGN_DATA] = @MSG_SIGN_DATA,
	[MSG_SIGN_CERT] = @MSG_SIGN_CERT,
	[CREATE_TIME] = @CREATE_TIME,
	[STATUS] = @STATUS,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_InsertUpdate]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_InsertUpdate]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN nvarchar(max),
	@MSG_SIGN_DATA nvarchar(max),
	@MSG_SIGN_CERT nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_MSG_INBOX] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_MSG_INBOX] 
		SET
			[MSG_FROM] = @MSG_FROM,
			[MSG_TO] = @MSG_TO,
			[MSG_ORIGIN] = @MSG_ORIGIN,
			[MSG_SIGN_DATA] = @MSG_SIGN_DATA,
			[MSG_SIGN_CERT] = @MSG_SIGN_CERT,
			[CREATE_TIME] = @CREATE_TIME,
			[STATUS] = @STATUS,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_MSG_INBOX]
		(
			[MSG_FROM],
			[MSG_TO],
			[MSG_ORIGIN],
			[MSG_SIGN_DATA],
			[MSG_SIGN_CERT],
			[CREATE_TIME],
			[STATUS],
			[GUIDSTR]
		)
		VALUES 
		(
			@MSG_FROM,
			@MSG_TO,
			@MSG_ORIGIN,
			@MSG_SIGN_DATA,
			@MSG_SIGN_CERT,
			@CREATE_TIME,
			@STATUS,
			@GUIDSTR
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_Delete]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_MSG_INBOX]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_Load]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_INBOX]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_SelectAll]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_INBOX]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_MSG_OUTBOX]'
GO
CREATE TABLE [dbo].[t_MSG_OUTBOX]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[MSG_FROM] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_TO] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_ORIGIN] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_SIGN_DATA] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSG_SIGN_CERT] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CREATE_TIME] [datetime] NULL CONSTRAINT [DF_Table_1_CREATE_TIME] DEFAULT (getdate()),
[STATUS] [int] NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_Table_1] on [dbo].[t_MSG_OUTBOX]'
GO
ALTER TABLE [dbo].[t_MSG_OUTBOX] ADD CONSTRAINT [PK_Table_1] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_Insert]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_Insert]
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN nvarchar(max),
	@MSG_SIGN_DATA nvarchar(max),
	@MSG_SIGN_CERT nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_MSG_OUTBOX]
(
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
)
VALUES 
(
	@MSG_FROM,
	@MSG_TO,
	@MSG_ORIGIN,
	@MSG_SIGN_DATA,
	@MSG_SIGN_CERT,
	@CREATE_TIME,
	@STATUS,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_Update]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_Update]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN nvarchar(max),
	@MSG_SIGN_DATA nvarchar(max),
	@MSG_SIGN_CERT nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_MSG_OUTBOX]
SET
	[MSG_FROM] = @MSG_FROM,
	[MSG_TO] = @MSG_TO,
	[MSG_ORIGIN] = @MSG_ORIGIN,
	[MSG_SIGN_DATA] = @MSG_SIGN_DATA,
	[MSG_SIGN_CERT] = @MSG_SIGN_CERT,
	[CREATE_TIME] = @CREATE_TIME,
	[STATUS] = @STATUS,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_InsertUpdate]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_InsertUpdate]
	@ID bigint,
	@MSG_FROM varchar(50),
	@MSG_TO varchar(50),
	@MSG_ORIGIN nvarchar(max),
	@MSG_SIGN_DATA nvarchar(max),
	@MSG_SIGN_CERT nvarchar(max),
	@CREATE_TIME datetime,
	@STATUS int,
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_MSG_OUTBOX] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_MSG_OUTBOX] 
		SET
			[MSG_FROM] = @MSG_FROM,
			[MSG_TO] = @MSG_TO,
			[MSG_ORIGIN] = @MSG_ORIGIN,
			[MSG_SIGN_DATA] = @MSG_SIGN_DATA,
			[MSG_SIGN_CERT] = @MSG_SIGN_CERT,
			[CREATE_TIME] = @CREATE_TIME,
			[STATUS] = @STATUS,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_MSG_OUTBOX]
		(
			[MSG_FROM],
			[MSG_TO],
			[MSG_ORIGIN],
			[MSG_SIGN_DATA],
			[MSG_SIGN_CERT],
			[CREATE_TIME],
			[STATUS],
			[GUIDSTR]
		)
		VALUES 
		(
			@MSG_FROM,
			@MSG_TO,
			@MSG_ORIGIN,
			@MSG_SIGN_DATA,
			@MSG_SIGN_CERT,
			@CREATE_TIME,
			@STATUS,
			@GUIDSTR
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_Delete]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_MSG_OUTBOX]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_Load]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_OUTBOX]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_SelectAll]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM
	[dbo].[t_MSG_OUTBOX]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_User_DaiLy]'
GO
CREATE TABLE [dbo].[t_User_DaiLy]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[USER_NAME] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PASSWORD] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[HO_TEN] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MO_TA] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isAdmin] [bit] NOT NULL CONSTRAINT [DF_t_DongBoDuLieu_User_isAdmin] DEFAULT ((0)),
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_DongBoDuLieu_User] on [dbo].[t_User_DaiLy]'
GO
ALTER TABLE [dbo].[t_User_DaiLy] ADD CONSTRAINT [PK_t_DongBoDuLieu_User] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_Insert]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_Insert]
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin bit,
	@MaDoanhNghiep varchar(50),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_User_DaiLy]
(
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[MaDoanhNghiep]
)
VALUES 
(
	@USER_NAME,
	@PASSWORD,
	@HO_TEN,
	@MO_TA,
	@isAdmin,
	@MaDoanhNghiep
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_Update]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_Update]
	@ID int,
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin bit,
	@MaDoanhNghiep varchar(50)
AS

UPDATE
	[dbo].[t_User_DaiLy]
SET
	[USER_NAME] = @USER_NAME,
	[PASSWORD] = @PASSWORD,
	[HO_TEN] = @HO_TEN,
	[MO_TA] = @MO_TA,
	[isAdmin] = @isAdmin,
	[MaDoanhNghiep] = @MaDoanhNghiep
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_InsertUpdate]
	@ID int,
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin bit,
	@MaDoanhNghiep varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_User_DaiLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_User_DaiLy] 
		SET
			[USER_NAME] = @USER_NAME,
			[PASSWORD] = @PASSWORD,
			[HO_TEN] = @HO_TEN,
			[MO_TA] = @MO_TA,
			[isAdmin] = @isAdmin,
			[MaDoanhNghiep] = @MaDoanhNghiep
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_User_DaiLy]
		(
			[USER_NAME],
			[PASSWORD],
			[HO_TEN],
			[MO_TA],
			[isAdmin],
			[MaDoanhNghiep]
		)
		VALUES 
		(
			@USER_NAME,
			@PASSWORD,
			@HO_TEN,
			@MO_TA,
			@isAdmin,
			@MaDoanhNghiep
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_Delete]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_User_DaiLy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_Load]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[MaDoanhNghiep]
FROM
	[dbo].[t_User_DaiLy]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_SelectAll]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[MaDoanhNghiep]
FROM
	[dbo].[t_User_DaiLy]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_InsertUpdate_ByUSER_NAME]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_InsertUpdate]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 29, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_InsertUpdate_ByUSER_NAME]
	@ID int,
	@USER_NAME varchar(50),
	@PASSWORD varchar(200),
	@HO_TEN nvarchar(1000),
	@MO_TA nvarchar(1000),
	@isAdmin BIT,
	@MaDoanhNghiep varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_User_DaiLy] WHERE [USER_NAME] = @USER_NAME)
	BEGIN
		UPDATE
			[dbo].[t_User_DaiLy] 
		SET
			[USER_NAME] = @USER_NAME,
			[PASSWORD] = @PASSWORD,
			[HO_TEN] = @HO_TEN,
			[MO_TA] = @MO_TA,
			[isAdmin] = @isAdmin,
			[MaDoanhNghiep] = @MaDoanhNghiep
		WHERE
			[ID] = (SELECT [ID] FROM [dbo].[t_User_DaiLy] WHERE [USER_NAME] = @USER_NAME)
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_User_DaiLy]
	(
			[USER_NAME],
			[PASSWORD],
			[HO_TEN],
			[MO_TA],
			[isAdmin],
			[MaDoanhNghiep]
	)
	VALUES
	(
			@USER_NAME,
			@PASSWORD,
			@HO_TEN,
			@MO_TA,
			@isAdmin,
			@MaDoanhNghiep
	)	
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_Delete_ByUSER_NAME]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_Delete]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, December 29, 2010
------------------------------------------------------------------------------------------------------------------------

create PROCEDURE [dbo].[p_User_DaiLy_Delete_ByUSER_NAME]
	@USER_NAME varchar(50),
	@PASSWORD varchar(200)
AS

DELETE FROM 
	[dbo].[t_User_DaiLy]
WHERE
	[USER_NAME] = @USER_NAME
	AND [PASSWORD] = @PASSWORD

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_DeleteDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_DaiLy_CauHinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_DaiLy_CauHinh_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_DaiLy_CauHinh_SelectDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_DaiLy_CauHinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[Value],
	[GhiChu],
	[IsUse]
FROM [dbo].[t_DaiLy_CauHinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_DeleteDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_MSG_DBDL] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_SelectDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, May 23, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR],
	[MSG_TYPE]
FROM [dbo].[t_MSG_DBDL] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_DeleteDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SignLogger] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_SignLogger_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SignLogger_SelectDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SignLogger_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[TIME_PROCESS],
	[MESSAGE],
	[DATECREATE]
FROM [dbo].[t_SignLogger] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_DeleteDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_MSG_INBOX] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_INBOX_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_INBOX_SelectDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_INBOX_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM [dbo].[t_MSG_INBOX] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_DeleteDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_MSG_OUTBOX] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_OUTBOX_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_OUTBOX_SelectDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, April 10, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_OUTBOX_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MSG_FROM],
	[MSG_TO],
	[MSG_ORIGIN],
	[MSG_SIGN_DATA],
	[MSG_SIGN_CERT],
	[CREATE_TIME],
	[STATUS],
	[GUIDSTR]
FROM [dbo].[t_MSG_OUTBOX] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_DeleteDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_User_DaiLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_User_DaiLy_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_User_DaiLy_SelectDynamic]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: Thursday, December 30, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_User_DaiLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[USER_NAME],
	[PASSWORD],
	[HO_TEN],
	[MO_TA],
	[isAdmin],
	[MaDoanhNghiep]
FROM [dbo].[t_User_DaiLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_DeleteDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_MSG_DBDL_Detail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_MSG_DBDL_Detail_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_MSG_DBDL_Detail_SelectDynamic]
-- Database: LICENSESv2
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 21, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_MSG_DBDL_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[SoTiepNhan],
	[MaLoaiHinh],
	[NgayDangKy],
	[PhanLuong],
	[MaHaiQuan],
	[PhanHe],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[TenDaiLy],
	[MaDaiLy],
	[GUIDSTR],
	[NgayGui],
	[TrangThai]
FROM [dbo].[t_MSG_DBDL_Detail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
