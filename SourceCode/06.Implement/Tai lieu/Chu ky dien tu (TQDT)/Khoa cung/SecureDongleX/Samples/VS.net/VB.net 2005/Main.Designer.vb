<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmMain
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
    Public WithEvents cmdReadMap As System.Windows.Forms.Button
	Public WithEvents cmdWriteMap As System.Windows.Forms.Button
	Public WithEvents cmdTransform As System.Windows.Forms.Button
	Public WithEvents cmdDecrypt As System.Windows.Forms.Button
	Public WithEvents cmdEncrypt As System.Windows.Forms.Button
	Public WithEvents cmdRead As System.Windows.Forms.Button
	Public WithEvents lstOutput As System.Windows.Forms.ListBox
	Public WithEvents cmdWrite As System.Windows.Forms.Button
   Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
      Me.cmdReadMap = New System.Windows.Forms.Button
      Me.cmdWriteMap = New System.Windows.Forms.Button
      Me.cmdTransform = New System.Windows.Forms.Button
      Me.cmdDecrypt = New System.Windows.Forms.Button
      Me.cmdEncrypt = New System.Windows.Forms.Button
      Me.cmdRead = New System.Windows.Forms.Button
      Me.lstOutput = New System.Windows.Forms.ListBox
      Me.cmdWrite = New System.Windows.Forms.Button
      Me.Label1 = New System.Windows.Forms.Label
      Me.SuspendLayout()
      '
      'cmdReadMap
      '
      Me.cmdReadMap.BackColor = System.Drawing.SystemColors.Control
      Me.cmdReadMap.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdReadMap.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdReadMap.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdReadMap.Location = New System.Drawing.Point(510, 328)
      Me.cmdReadMap.Name = "cmdReadMap"
      Me.cmdReadMap.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdReadMap.Size = New System.Drawing.Size(89, 33)
      Me.cmdReadMap.TabIndex = 7
      Me.cmdReadMap.Text = "Read with Map"
      Me.cmdReadMap.UseVisualStyleBackColor = False
      '
      'cmdWriteMap
      '
      Me.cmdWriteMap.BackColor = System.Drawing.SystemColors.Control
      Me.cmdWriteMap.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdWriteMap.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdWriteMap.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdWriteMap.Location = New System.Drawing.Point(510, 288)
      Me.cmdWriteMap.Name = "cmdWriteMap"
      Me.cmdWriteMap.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdWriteMap.Size = New System.Drawing.Size(89, 33)
      Me.cmdWriteMap.TabIndex = 6
      Me.cmdWriteMap.Text = "Write with Map"
      Me.cmdWriteMap.UseVisualStyleBackColor = False
      '
      'cmdTransform
      '
      Me.cmdTransform.BackColor = System.Drawing.SystemColors.Control
      Me.cmdTransform.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdTransform.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdTransform.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdTransform.Location = New System.Drawing.Point(510, 128)
      Me.cmdTransform.Name = "cmdTransform"
      Me.cmdTransform.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdTransform.Size = New System.Drawing.Size(89, 33)
      Me.cmdTransform.TabIndex = 2
      Me.cmdTransform.Text = "Tranform Data"
      Me.cmdTransform.UseVisualStyleBackColor = False
      '
      'cmdDecrypt
      '
      Me.cmdDecrypt.BackColor = System.Drawing.SystemColors.Control
      Me.cmdDecrypt.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdDecrypt.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdDecrypt.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdDecrypt.Location = New System.Drawing.Point(510, 248)
      Me.cmdDecrypt.Name = "cmdDecrypt"
      Me.cmdDecrypt.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdDecrypt.Size = New System.Drawing.Size(89, 33)
      Me.cmdDecrypt.TabIndex = 5
      Me.cmdDecrypt.Text = "Decrypt Data"
      Me.cmdDecrypt.UseVisualStyleBackColor = False
      '
      'cmdEncrypt
      '
      Me.cmdEncrypt.BackColor = System.Drawing.SystemColors.Control
      Me.cmdEncrypt.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdEncrypt.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdEncrypt.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdEncrypt.Location = New System.Drawing.Point(510, 208)
      Me.cmdEncrypt.Name = "cmdEncrypt"
      Me.cmdEncrypt.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdEncrypt.Size = New System.Drawing.Size(89, 33)
      Me.cmdEncrypt.TabIndex = 4
      Me.cmdEncrypt.Text = "Encrypt Data"
      Me.cmdEncrypt.UseVisualStyleBackColor = False
      '
      'cmdRead
      '
      Me.cmdRead.BackColor = System.Drawing.SystemColors.Control
      Me.cmdRead.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdRead.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdRead.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdRead.Location = New System.Drawing.Point(510, 168)
      Me.cmdRead.Name = "cmdRead"
      Me.cmdRead.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdRead.Size = New System.Drawing.Size(89, 33)
      Me.cmdRead.TabIndex = 3
      Me.cmdRead.Text = "Read Data"
      Me.cmdRead.UseVisualStyleBackColor = False
      '
      'lstOutput
      '
      Me.lstOutput.BackColor = System.Drawing.SystemColors.Window
      Me.lstOutput.Cursor = System.Windows.Forms.Cursors.Default
      Me.lstOutput.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.lstOutput.ForeColor = System.Drawing.SystemColors.WindowText
      Me.lstOutput.ItemHeight = 14
      Me.lstOutput.Location = New System.Drawing.Point(16, 88)
      Me.lstOutput.Name = "lstOutput"
      Me.lstOutput.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.lstOutput.Size = New System.Drawing.Size(481, 270)
      Me.lstOutput.TabIndex = 8
      Me.lstOutput.TabStop = False
      '
      'cmdWrite
      '
      Me.cmdWrite.BackColor = System.Drawing.SystemColors.Control
      Me.cmdWrite.Cursor = System.Windows.Forms.Cursors.Default
      Me.cmdWrite.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.cmdWrite.ForeColor = System.Drawing.SystemColors.ControlText
      Me.cmdWrite.Location = New System.Drawing.Point(510, 88)
      Me.cmdWrite.Name = "cmdWrite"
      Me.cmdWrite.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.cmdWrite.Size = New System.Drawing.Size(89, 33)
      Me.cmdWrite.TabIndex = 1
      Me.cmdWrite.Text = "Write Data"
      Me.cmdWrite.UseVisualStyleBackColor = False
      '
      'Label1
      '
      Me.Label1.BackColor = System.Drawing.SystemColors.Control
      Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
      Me.Label1.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
      Me.Label1.Location = New System.Drawing.Point(24, 24)
      Me.Label1.Name = "Label1"
      Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.Label1.Size = New System.Drawing.Size(569, 33)
      Me.Label1.TabIndex = 9
      Me.Label1.Text = "SecureDongle X  VB.net 2005 SAMPLE"
      Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
      '
      'frmMain
      '
      Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
      Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
      Me.BackColor = System.Drawing.SystemColors.Control
      Me.ClientSize = New System.Drawing.Size(617, 374)
      Me.Controls.Add(Me.cmdReadMap)
      Me.Controls.Add(Me.cmdWriteMap)
      Me.Controls.Add(Me.cmdTransform)
      Me.Controls.Add(Me.cmdDecrypt)
      Me.Controls.Add(Me.cmdEncrypt)
      Me.Controls.Add(Me.cmdRead)
      Me.Controls.Add(Me.lstOutput)
      Me.Controls.Add(Me.cmdWrite)
      Me.Controls.Add(Me.Label1)
      Me.Cursor = System.Windows.Forms.Cursors.Default
      Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
      Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
      Me.Location = New System.Drawing.Point(262, 150)
      Me.MaximizeBox = False
      Me.MinimizeBox = False
      Me.Name = "frmMain"
      Me.RightToLeft = System.Windows.Forms.RightToLeft.No
      Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
      Me.Text = "SecureDongle X VB.net 2005 Sample"
      Me.ResumeLayout(False)

   End Sub
#End Region 
End Class