﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseSPRegistedForm : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_SANPHAM SPSelected;
        public T_KHOKETOAN_SANPHAM_MAP SPMapSelected;
        public string SPMap;
        public WareHouseSPRegistedForm()
        {
            InitializeComponent();
        }

        private void WareHouseSPRegistedForm_Load(object sender, EventArgs e)
        {
            btnSearchSP_Click(null,null);
        }

        private void btnSearchSP_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(SPMap))
                {
                    grListSP.Refetch();
                    grListSP.DataSource = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASP = '" + SPMap + "'", "");
                    grListSP.Refresh();
                }
                else
                {
                    grListSP.Refetch();
                    grListSP.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("MASP LIKE '%" + txtMaSP.Text.ToString() + "%'", "");
                    grListSP.Refresh();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(SPMap))
                {
                    SPMapSelected = new T_KHOKETOAN_SANPHAM_MAP();
                    SPMapSelected = (T_KHOKETOAN_SANPHAM_MAP)e.Row.DataRow;
                }
                else
                {
                    SPSelected = new T_KHOKETOAN_SANPHAM();
                    SPSelected = (T_KHOKETOAN_SANPHAM)e.Row.DataRow;
                    SPSelected.SPCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionBy_KHOSP_ID(SPSelected.ID);
                }
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
