﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP
{
   public class TTTK_AdditionalInformation
    {
        /// <summary>
        /// Mã nội dung phản hồi
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }
    }
}
