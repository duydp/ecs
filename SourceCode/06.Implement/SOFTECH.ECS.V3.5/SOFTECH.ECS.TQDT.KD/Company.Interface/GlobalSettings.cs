﻿using System.Configuration;
using System.Windows.Forms;
using System;
using System.Xml;
using System.Collections.Generic;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public class GlobalSettings
    {
        // ĐÌnh Minh Tạo prop fontzise
        public static string fontsize { get; set; }

        //Dong bo du lieu
        public static bool SuDungSoTK;
        public static int SoTK;
        public static bool DongBoChungTuDinhKem;

        public static string DATETIME_FORMAT_VN = "dd/MM/yyyy";
        public static bool IsDaiLy { get; set; }
        public static bool IsOnlyMe = false;
        public static int ThongBaoHetHan;
        public static decimal SoTienKhoanTKN;
        public static decimal SoTienKhoanTKX;
        public static bool SuDungChuKySo;

        public static bool IsSignOnLan;
        public static string DataSignLan;
        public static string DiaChiWS;
        public static string DiaChiWS_Host;
        public static string DiaChiWS_Name;
        public static string CHO_TIEP_NHAN_COLOR;
        public static string CHUA_DUYET_COLOR;
        public static string CHUA_KHAI_BAO_COLOR;
        public static string CUA_KHAU = Properties.Settings.Default.CUA_KHAU;
        public static string LOAI_HINH = Properties.Settings.Default.LOAI_HINH;
        public static string NHOM_LOAI_HINH = Properties.Settings.Default.NHOM_LOAI_HINH;
        public static string NHOM_LOAI_HINH_KHAC_XUAT = ConfigurationManager.AppSettings["NHOM_LOAI_HINH_KHAC_XUAT"];
        public static string LOAI_HINH_KHAC_XUAT = ConfigurationManager.AppSettings["LOAI_HINH_KHAC_XUAT"];
        public static string NHOM_LOAI_HINH_KHAC_NHAP = ConfigurationManager.AppSettings["NHOM_LOAI_HINH_KHAC_NHAP"];
        public static string LOAI_HINH_KHAC_NHAP = ConfigurationManager.AppSettings["LOAI_HINH_KHAC_NHAP"];
        public static string DA_DUYET_COLOR;
        public static bool DAI_LY_TTHQ = false;
        public static string DKGH_MAC_DINH = Properties.Settings.Default.DKGH_MAC_DINH;
        public static string DVT_MAC_DINH = Properties.Settings.Default.DVT_MAC_DINH;
        public static string MA_DAI_LY = string.Empty;
        public static string TEN_DAI_LY = string.Empty;
        public static string NUOC = Properties.Settings.Default.NUOC;
        public static string NGUYEN_TE_MAC_DINH = Properties.Settings.Default.NGUYEN_TE_MAC_DINH;
        public static string PTTT_MAC_DINH = Properties.Settings.Default.PTTT_MAC_DINH;
        public static string PTVT_MAC_DINH = Properties.Settings.Default.PTVT_MAC_DINH;
        public static string TEN_DOI_TAC = Properties.Settings.Default.TEN_DOI_TAC;
        public static decimal TY_GIA_USD = Properties.Settings.Default.TY_GIA_USD;
        public static string CHENHLECH_THN_THX = ConfigurationManager.AppSettings["CL_THN_THX"];
        public static string LoaiWS = ConfigurationManager.AppSettings["LoaiWS"];
        //public static string DiaChiWS = Properties.Settings.Default.DiaChiWS;
        //public static string DATABASE_NAME = Properties.Settings.Default.DATABASE_NAME;
        //public static string USER = Properties.Settings.Default.user;
        //public static string PASS = Properties.Settings.Default.pass;
        //public static string SERVER_NAME = Properties.Settings.Default.ServerName;
        public static string DATABASE_NAME;
        public static string USER;
        public static string PASS;
        public static string SERVER_NAME;

        //public static string MA_CUC_HAI_QUAN = Properties.Settings.Default.MA_CUC_HAI_QUAN;
        //public static string TEN_CUC_HAI_QUAN = Properties.Settings.Default.TEN_CUC_HAI_QUAN;
        //public static string MA_DON_VI = Properties.Settings.Default.MA_DON_VI;
        //public static string DIA_CHI = Properties.Settings.Default.DIA_CHI;
        //public static string MA_HAI_QUAN = Properties.Settings.Default.MA_HAI_QUAN;
        //public static string TEN_HAI_QUAN = Properties.Settings.Default.TEN_HAI_QUAN;
        //public static string TEN_HAI_QUAN_NGAN = Properties.Settings.Default.TEN_HAI_QUAN_NGAN;
        //public static string TEN_DON_VI = Properties.Settings.Default.TEN_DON_VI;
        public static string PassWordDT = "";
        public static string UserId = "";
        public static bool IsRemember = false;
        //public static string MailDoanhNghiep = Properties.Settings.Default.MailDoanhNghiep;
        //public static string MailHaiQuan = Properties.Settings.Default.MailHaiQuan;
        //public static string MaMID = Properties.Settings.Default.MaMID;

        public static string MA_CUC_HAI_QUAN;
        public static string TEN_CUC_HAI_QUAN;
        public static string MA_DON_VI;
        public static string DIA_CHI;
        public static string MA_HAI_QUAN;
        public static string TEN_HAI_QUAN;
        public static string TEN_HAI_QUAN_NGAN;
        public static string TEN_DON_VI;
        public static string MailDoanhNghiep;
        public static string MailHaiQuan;
        public static string MaMID;
        public static string SoDienThoaiDN;
        public static string SoFaxDN;
        public static string NguoiLienHe;
        public static string ChucVu;
        public static bool iSignRemote;
        public static string userNameSign;
        public static string passwordSign;

        public static bool iSignSmartCA;
        public static string userNameSmartCA;
        public static string passwordSmartCA;

        public static string APP_ID;
        public static string APP_SECRET;
        public static string SERVICE_GET_TOKENURL;
        public static string URL_CREDENTIALSLIST;
        public static string URL_CREDENTIALSINFO;
        public static string URL_SIGNHASH;
        public static string URL_SIGN;
        public static string URL_GETTRANINFO;

        //User name & password đồng bộ
        public static string USERNAME_DONGBO;
        public static string PASSWOR_DONGBO;
        public static int SOTOKHAI_DONGBO;
        public static bool ISKHAIBAO = true;
        public static bool THONGBAOVNACC = false;
        public static bool IsDelete = false;

        public static bool SendV4 = false;
        // public static bool SendV4_DL = false;

        public static int MaHTS = Properties.Settings.Default.MA_HTS;
        public static string DIA_DIEM_DO_HANG = Properties.Settings.Default.DIA_DIEM_DO_HANG;
        public static string TieuDeInDinhMuc = Properties.Settings.Default.TieuDeInDinhMuc;
        public static string TuDongTinhThue = Properties.Settings.Default.TuDongTinhThue;
        /// <summary>
        /// KhanhHN - 22/06/2012
        /// Bổ sung cấu hình tính thuế theo đơn giá/ trị giá
        /// </summary>
        public static string TinhThueTGNT = Properties.Settings.Default.TinhThueTGNT;

        public static string DATABASE_NAME_DB = Properties.Settings.Default.DB_Name_DB;

        public static string NguoiDuyetBieu = Properties.Settings.Default.NguoiDuyetBieu;
        public static string NguoiLapBieu = Properties.Settings.Default.NguoiLapBieu;
        public static string DonViBaoCao = Properties.Settings.Default.DonViBaoCao;

        public static string USER_DB = Properties.Settings.Default.User_DB;
        public static string PASS_DB = Properties.Settings.Default.Pass_DB;
        public static string SERVER_NAME_DB = Properties.Settings.Default.Server_Name_DB;
        public static string CONG_THUC_TINH_THUE_HAIQUAN = ConfigurationManager.AppSettings["CONG_THUC_TINH_THUE_HAIQUAN"];
        public static System.Drawing.Printing.Margins MarginTKN = Properties.Settings.Default.MarginTKN;
        public static System.Drawing.Printing.Margins MarginTKX = Properties.Settings.Default.MarginTKX;
        public static System.Drawing.Printing.Margins MarginPhuLucTKN = Properties.Settings.Default.MarginPhuLucTKN;
        public static System.Drawing.Printing.Margins MarginPhuLucTKX = Properties.Settings.Default.MarginPhuLucTKX;

        public static long FileSize = Properties.Settings.Default.FileSize;
        public static string MienThueGTGT = Properties.Settings.Default.MienThueGTGT;
        public static List<string> ListTableNameSource = new List<string>();

        public static string NGAYSAOLUU;
        public static string NHAC_NHO_SAO_LUU;
        public static string LastBackup;
        public static string PathBackup;
        public static string NGON_NGU;

        public static bool NgayHeThong = true;
        public static bool IsKhongDungBangKeCont = false;
        public static string MA_HAI_QUAN_VNACCS;

        //minhnd 29/09/2015 kiểm tra chữ ký số
        public static double CheckChuKySo()
        {
            TimeSpan ts = new TimeSpan();
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string name = item.GetName();
                if (name.Contains(GlobalSettings.MA_DON_VI))
                {
                    DateTime date = DateTime.Parse(item.GetExpirationDateString());
                    ts = date - DateTime.Now;
                    return Math.Round(ts.TotalDays);
                    //if(ts.TotalDays<=30)
                    //    MessageBox.Show("Chữ ký số có MST:" + GlobalSettings.MA_DON_VI+" còn "+Math.Round(ts.TotalDays)+" nữa hết hạn sử dụng.", "Expiration Date Certificate", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            return 0;
        }
        public static void KhoiTao_GiaTriMacDinh()
        {
            try
            {
                string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
                ApplicationSettings settings = new ApplicationSettings();
                settings.ReadXml(settingFileName);

                NguyenPhuLieu.Ma = settings.NguyenPhuLieu[0].Ma;
                NguyenPhuLieu.Ten = settings.NguyenPhuLieu[0].Ten;
                NguyenPhuLieu.MaHS = settings.NguyenPhuLieu[0].MaHS;
                NguyenPhuLieu.DVT = settings.NguyenPhuLieu[0].DVT;

                SanPham.Ma = settings.SanPham[0].Ma;
                SanPham.Ten = settings.SanPham[0].Ten;
                SanPham.MaHS = settings.SanPham[0].MaHS;
                SanPham.DVT = settings.SanPham[0].DVT;

                DinhMuc.MaSP = settings.DinhMuc[0].MaSP;
                DinhMuc.MaNPL = settings.DinhMuc[0].MaNPL;
                DinhMuc.DinhMucSuDung = settings.DinhMuc[0].DinhMucSuDung;
                DinhMuc.DinhMucChung = settings.DinhMuc[0].DinhMucChung;
                DinhMuc.TyLeHH = settings.DinhMuc[0].TyLeHH;

                ThongTinDinhMuc.MaSP = settings.ThongTinDinhMuc[0].MaSP;
                ThongTinDinhMuc.SoDinhMuc = settings.ThongTinDinhMuc[0].SoDinhMuc;
                ThongTinDinhMuc.NgayDangKy = settings.ThongTinDinhMuc[0].NgayDangKy;
                ThongTinDinhMuc.NgayApDung = settings.ThongTinDinhMuc[0].NgayApDung;

                GiaoDien.Id = settings.GiaoDien[0].Id;
                SoThapPhan.DinhMuc = Convert.ToInt32(settings.SoThapPhan[0].DinhMuc);
                SoThapPhan.LuongNPL = Convert.ToInt32(settings.SoThapPhan[0].LuongNPL);
                SoThapPhan.LuongSP = Convert.ToInt32(settings.SoThapPhan[0].LuongSP);
                SoThapPhan.SapXepTheoTK = Convert.ToInt32(settings.SoThapPhan[0].SapXepTheoTK);
                SoThapPhan.NPLKoTK = Convert.ToInt32(settings.SoThapPhan[0].NPLKoTK);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        //minhnd 29/09/2015 kiểm tra chữ ký số
        //public static double CheckChuKySo()
        //{
        //    TimeSpan ts = new TimeSpan();
        //    List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
        //    foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
        //    {
        //        string name = item.GetName();
        //        if (name.Contains(GlobalSettings.MA_DON_VI))
        //        {
        //            DateTime date = DateTime.Parse(item.GetExpirationDateString());
        //            ts = date - DateTime.Now;
        //            return Math.Round(ts.TotalDays);
        //            //if(ts.TotalDays<=30)
        //            //    MessageBox.Show("Chữ ký số có MST:" + GlobalSettings.MA_DON_VI+" còn "+Math.Round(ts.TotalDays)+" nữa hết hạn sử dụng.", "Expiration Date Certificate", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }
        //    }
        //    return -1;
        //}
        public static void Luu_NPL(string ma, string ten, string maHS, string dvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.NguyenPhuLieu[0].Ma = ma;
            settings.NguyenPhuLieu[0].Ten = ten;
            settings.NguyenPhuLieu[0].MaHS = maHS;
            settings.NguyenPhuLieu[0].DVT = dvt;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_SP(string ma, string ten, string maHS, string dvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.SanPham[0].Ma = ma;
            settings.SanPham[0].Ten = ten;
            settings.SanPham[0].MaHS = maHS;
            settings.SanPham[0].DVT = dvt;
            settings.WriteXml(settingFileName);
        }

        public static void Luu_DM(string maSP, string maNPL, string DMSD, string TLHH, string DMC)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.DinhMuc[0].MaSP = maSP;
            settings.DinhMuc[0].MaNPL = maNPL;
            settings.DinhMuc[0].DinhMucSuDung = DMSD;
            settings.DinhMuc[0].DinhMucChung = DMC;
            settings.DinhMuc[0].TyLeHH = TLHH;
            settings.WriteXml(settingFileName);
        }

        public static void Luu_TTDM(string maSP, string soDinhMuc, string ngayDangKy, string ngayApDung)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.ThongTinDinhMuc[0].MaSP = maSP;
            settings.ThongTinDinhMuc[0].SoDinhMuc = soDinhMuc;
            settings.ThongTinDinhMuc[0].NgayDangKy = ngayDangKy;
            settings.ThongTinDinhMuc[0].NgayApDung = ngayApDung;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_GiaoDien(string id)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.GiaoDien[0].Id = id;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_SoThapPhan(int dinhMuc, int luongNPL, int luongSP, int sapXepTheoTK, int nplKoTK)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.SoThapPhan[0].DinhMuc = dinhMuc + "";
            settings.SoThapPhan[0].LuongSP = luongSP + "";
            settings.SoThapPhan[0].LuongNPL = luongNPL + "";
            settings.SoThapPhan[0].SapXepTheoTK = sapXepTheoTK + "";
            settings.SoThapPhan[0].NPLKoTK = nplKoTK + "";
            settings.WriteXml(settingFileName);
        }



        #region Nested type: NguyenPhuLieu

        public struct NguyenPhuLieu
        {
            public static string Ma;
            public static string Ten;
            public static string MaHS;
            public static string DVT;
        }

        #endregion

        #region Nested type: SanPham

        public struct SanPham
        {
            public static string Ma;
            public static string Ten;
            public static string MaHS;
            public static string DVT;
        }

        #endregion

        #region Nested type: DinhMuc

        public struct DinhMuc
        {
            public static string MaSP;
            public static string MaNPL;
            public static string DinhMucSuDung;
            public static string TyLeHH;
            public static string DinhMucChung;
        }

        #endregion

        #region Nested type: ThongTinDinhMuc

        public struct ThongTinDinhMuc
        {
            public static string MaSP;
            public static string SoDinhMuc;
            public static string NgayDangKy;
            public static string NgayApDung;
        }

        #endregion

        #region Nested type: GiaoDien

        public struct GiaoDien
        {
            public static string Id;
            public static string NgonNgu;
        }

        #endregion

        #region Nested type: SoThapPhan

        public struct SoThapPhan
        {
            public static int DinhMuc;
            public static int LuongNPL;
            public static int LuongSP;
            public static int SapXepTheoTK;
            public static int NPLKoTK;
            public static int SoLuongHMD;
            public static int TrongLuongHangTK;
            public static int TriGiaNT = 4;
            public static int DonGiaNT = 4;
            public static int TLHH;
        }

        #endregion

        public static void RefreshKey()
        {
            try
            {
                CUA_KHAU = Properties.Settings.Default.CUA_KHAU;
                LOAI_HINH = Properties.Settings.Default.LOAI_HINH;
                NHOM_LOAI_HINH = Properties.Settings.Default.NHOM_LOAI_HINH;
                NHOM_LOAI_HINH_KHAC_XUAT = ConfigurationManager.AppSettings["NHOM_LOAI_HINH_KHAC_XUAT"];
                LOAI_HINH_KHAC_XUAT = ConfigurationManager.AppSettings["LOAI_HINH_KHAC_XUAT"];
                NHOM_LOAI_HINH_KHAC_NHAP = ConfigurationManager.AppSettings["NHOM_LOAI_HINH_KHAC_NHAP"];
                LOAI_HINH_KHAC_NHAP = ConfigurationManager.AppSettings["LOAI_HINH_KHAC_NHAP"];
                DKGH_MAC_DINH = Properties.Settings.Default.DKGH_MAC_DINH;
                DVT_MAC_DINH = Properties.Settings.Default.DVT_MAC_DINH;
                MA_DAI_LY = "";
                NUOC = Properties.Settings.Default.NUOC;
                NGUYEN_TE_MAC_DINH = Properties.Settings.Default.NGUYEN_TE_MAC_DINH;
                PTTT_MAC_DINH = Properties.Settings.Default.PTTT_MAC_DINH;
                PTVT_MAC_DINH = Properties.Settings.Default.PTVT_MAC_DINH;
                TEN_DOI_TAC = Properties.Settings.Default.TEN_DOI_TAC;
                TY_GIA_USD = Properties.Settings.Default.TY_GIA_USD;
                CHENHLECH_THN_THX = ConfigurationManager.AppSettings["CL_THN_THX"];
                LoaiWS = ConfigurationManager.AppSettings["LoaiWS"];

                //DiaChiWS = Properties.Settings.Default.DiaChiWS;
                //DATABASE_NAME = Properties.Settings.Default.DATABASE_NAME;
                //USER = Properties.Settings.Default.user;
                //PASS = Properties.Settings.Default.pass;
                //SERVER_NAME = Properties.Settings.Default.ServerName;

                //MA_CUC_HAI_QUAN = Properties.Settings.Default.MA_CUC_HAI_QUAN;
                //TEN_CUC_HAI_QUAN = Properties.Settings.Default.TEN_CUC_HAI_QUAN;
                //MA_DON_VI = Properties.Settings.Default.MA_DON_VI;
                //DIA_CHI = Properties.Settings.Default.DIA_CHI;
                //MA_HAI_QUAN = Properties.Settings.Default.MA_HAI_QUAN;
                //TEN_HAI_QUAN = Properties.Settings.Default.TEN_HAI_QUAN;
                //TEN_HAI_QUAN_NGAN = Properties.Settings.Default.TEN_HAI_QUAN_NGAN;
                //TEN_DON_VI = Properties.Settings.Default.TEN_DON_VI;
                PassWordDT = Properties.Settings.Default.PassWordDT;
                //MailDoanhNghiep = Properties.Settings.Default.MailDoanhNghiep;
                //MailHaiQuan = Properties.Settings.Default.MailHaiQuan;
                //MaMID = Properties.Settings.Default.MaMID;
                MaHTS = Properties.Settings.Default.MA_HTS;
                DIA_DIEM_DO_HANG = Properties.Settings.Default.DIA_DIEM_DO_HANG;
                TieuDeInDinhMuc = Properties.Settings.Default.TieuDeInDinhMuc;
                TuDongTinhThue = Properties.Settings.Default.TuDongTinhThue;
                //KhanhHN - 22/06/2012 
                //Bổ sung cấu hình tính thuế theo trị giá / đơn giá
                TinhThueTGNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TinhThueTGNT");
                //TuDongTinhThue = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TuDongTinhThue");

                CONG_THUC_TINH_THUE_HAIQUAN = ConfigurationManager.AppSettings["CONG_THUC_TINH_THUE_HAIQUAN"];
                MarginTKN = Properties.Settings.Default.MarginTKN;
                MarginTKX = Properties.Settings.Default.MarginTKX;
                MarginPhuLucTKN = Properties.Settings.Default.MarginPhuLucTKN;
                MarginPhuLucTKX = Properties.Settings.Default.MarginPhuLucTKX;
                DonViBaoCao = Properties.Settings.Default.DonViBaoCao;

                /*DATLMQ update Đọc dữ liệu từ file Config 19/01/2011.*/
                XmlDocument doc = new XmlDocument();
                string path = Company.KD.BLL.EntityBase.GetPathProgram() + "\\ConfigDoanhNghiep";
                //Hungtq update 28/01/2011.
                string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);
                doc.Load(fileName);

                //Minhnd get fontsize 04/10/2014
                fontsize = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontHuongDan", "8");

                //HUNGTQ Updated 07/06/2011
                //Get thong tin WebService
                DiaChiWS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS").InnerText;
                DiaChiWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host").InnerText;
                DiaChiWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name").InnerText;
                //Get thong tin Server
                SERVER_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Server").InnerText;
                //Get thong tin Database
                DATABASE_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database").InnerText;
                //Get thong tin UserName
                USER = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName").InnerText;
                //Get thong tin PassWord
                PASS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password").InnerText;

                //Get thông tin MaCucHQ
                MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText;
                //Get thông tin TenCucHQ
                TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText;
                //Get thông tin MaChiCucHQ
                MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText;
                //Get thông tin TenChiCucHQ
                TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText;
                //Get thông tin TenNganChiCucHQ
                TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText;
                //Get thông tin MailHQ
                MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText;

                //Get thông tin TenDoanhNghiep

                TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText;
                //Get thông tin DiaChiDoanhNghiep
                DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText;
                //Get thông tin MaMid
                MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText;
                IsOnlyMe = bool.Parse(Company.KDT.SHARE.Components.Globals.GetConfig(doc, "OnlyMe", "False"));
                MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText;
                SoDienThoaiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText;
                SoFaxDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText;
                NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText;
                ChucVu = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "ChucVu").InnerText;

                IsSignOnLan = bool.Parse(Company.KDT.SHARE.Components.Globals.GetConfig(doc, "IsSignOnLan", "False"));
                DataSignLan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DataSignLan");

                iSignRemote = bool.Parse(Company.KDT.SHARE.Components.Globals.GetConfig(doc, "IsSignRemote", "False"));
                userNameSign = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserNameSignRemote").InnerText;
                passwordSign = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "PasswordSignRemote").InnerText;

                //iSignSmartCA = bool.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignSmartCA"));
                //userNameSmartCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("UserNameSmartCA");
                //passwordSmartCA = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PasswordSmartCA");

                //APP_SECRET = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("APP_SECRET");
                //APP_ID = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("APP_ID");
                //SERVICE_GET_TOKENURL = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SERVICE_GET_TOKENURL");
                //URL_CREDENTIALSLIST = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_CREDENTIALSLIST");
                //URL_CREDENTIALSINFO = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_CREDENTIALSINFO");
                //URL_SIGNHASH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_SIGNHASH");
                //URL_SIGN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_SIGN");
                //URL_GETTRANINFO = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("URL_GETTRANINFO");

                FileSize = Properties.Settings.Default.FileSize;
                MienThueGTGT = Properties.Settings.Default.MienThueGTGT;

                NGAYSAOLUU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NgaySaoLuu");
                NHAC_NHO_SAO_LUU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");
                LastBackup = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LastBackup");
                PathBackup = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PathBackup");
                NGON_NGU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ngonngu");

                SuDungChuKySo = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SuDungChuKySo"));
                //  SuDungChuKySo = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SuDungChuKySo"));
                ThongBaoHetHan = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoHetHan"));

                SoThapPhan.TriGiaNT = string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TriGiaNT")) ? 4 : Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TriGiaNT"));
                SoThapPhan.DonGiaNT = string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DonGiaNT")) ? 4 : Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DonGiaNT"));

                Company.KDT.SHARE.Components.Globals.TriGiaNT = SoThapPhan.TriGiaNT;
                Company.KDT.SHARE.Components.Globals.DonGiaNT = SoThapPhan.DonGiaNT;
                NgayHeThong = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NgayHeThong", "false"));
                IsKhongDungBangKeCont = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsKhongDungBangKeCont", "false"));
                THONGBAOVNACC = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoVNACCS", "false"));
                try
                {
                    SendV4 = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SendV4"));
                }
                catch (System.Exception ex)
                {
                    SendV4 = false;
                    Logger.LocalLogger.Instance().WriteMessage("lỗi config SendV4 : ", ex);
                }

                if (IsDaiLy && !string.IsNullOrEmpty(MA_DON_VI))
                {

                    Company.Interface.DaiLy.Login lg = new Company.Interface.DaiLy.Login();
                    lg.GetValueConfig(MA_DON_VI);
                    //Company.KDT.SHARE.Components.Globals.isKhaiDL = System.Convert.ToBoolean(Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MA_DON_VI, "isKhaiDl").Value_Config.ToString());
                }
                else
                    MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText;

                GlobalVNACC.PathConfig = Application.StartupPath + "\\Config";
                GlobalVNACC.TimerRequest = 5;
                GlobalVNACC.isStopRespone = false;
                GlobalVNACC.NguoiSuDung_DiaChi = string.Empty;
                //TODO: Dua lay thong tin cau hinh cua VNACC vao Form Login.
                //GlobalVNACC.NguoiSuDung_Ma = "E8400";
                //GlobalVNACC.NguoiSuDung_ID = "001";
                //GlobalVNACC.TerminalID = "AAA17K"; //"BAAA5C";
                //GlobalVNACC.NguoiSuDung_Pass = "CTVNACCS";
                //GlobalVNACC.Url = "http://localhost:32333/test.aspx"; //"https://ediconn.vnaccs.customs.gov.vn/test";
                //GlobalVNACC.TerminalAccessKey = "9999CTVNACCS";

                SuDungSoTK = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SuDungSoTK", "false") == "True" ? true : false;
                SoTK = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTK", "0"));
                DongBoChungTuDinhKem = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CHUNGTUDINHKEM", "False") == "True" ? true : false;
                IsDelete = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsDelete") == "True" ? true : false;

                GlobalVNACC.IsGenerateTextDataVNACC = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsGenerateTextDataVNACC", "false").ToLower() == "true" ? true : false;
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            MA_HAI_QUAN_VNACCS = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MA_HAI_QUAN, "MaHQ");
        }
    }
}