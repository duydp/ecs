﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class CertificateOfOrigins
    {
        /// <summary>
        /// Thông tin về CO
        /// </summary>
       [XmlElement("CertificateOfOrigin")]
       public List<Company.KDT.SHARE.Components.Messages.Vouchers.CertificateOfOrigin> CertificateOfOrigin { get; set; }
    }
}
