using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Security.Cryptography;

namespace Company.Interface.PhongKhai
{
    public partial class ChangePINForm : BaseForm
    {

        public ChangePINForm()
        {
            InitializeComponent();
        }
        private string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }
        private void PasswordForm_Load(object sender, EventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            AppSettingsSection section = config.AppSettings;
            
            if (ConfigurationManager.AppSettings["PIN"] == GetMD5Value(txtMatKhauCu.Text))
            {
                section.Settings["PIN"].Value = GetMD5Value(txtMatKhauMoi.Text);
                config.Save(ConfigurationSaveMode.Full);
                ConfigurationManager.RefreshSection(config.AppSettings.SectionInformation.Name);
                ShowMessage("Đổi mã bảo vệ thành công.", false);
                this.Close();
            }
            else
            {
                lblThongBao.Visible = true;
            }


        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }
    }
}