﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    public class NodePK_BoSungNguyenPhuLieu
    {
        /// <summary>
        /// Nguyên liệu bổ sung 
        /// </summary>
        public const string Material = "Material";    
        /// <summary>
        /// Nguyên Phụ liệu
        /// </summary>
        public const string Commodity = "Commodity";    
        /// <summary>
        /// Tên/Mô tả nguyên phụ liệu bổ sung
        /// </summary>
        public const string description = "description";    
        /// <summary>
        /// Mã nguyên phụ liệu bổ sung
        /// </summary>
        public const string identification = "identification";    
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";    
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";    
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";    
        
    }
}
