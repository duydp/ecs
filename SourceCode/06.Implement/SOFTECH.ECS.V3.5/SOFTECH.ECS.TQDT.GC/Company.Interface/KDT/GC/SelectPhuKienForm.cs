using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.GC
{
    public partial class SelectPhuKienForm : BaseForm
    {
        private long _hopDongID;

        private PhuKienDangKyCollection PKDKCollection = new PhuKienDangKyCollection();
        private PhuKienDangKy _phuKienDangKySelected;

        public long HopDongID
        {
            get { return _hopDongID; }
            set { _hopDongID = value; }
        }

        public PhuKienDangKy PhuKienDangKySelected
        {
            get { return _phuKienDangKySelected; }
            set { _phuKienDangKySelected = value; }
        }

        public SelectPhuKienForm()
        {
            InitializeComponent();
        }

        private void SelectPhuKienForm_Load(object sender, EventArgs e)
        {
            PKDKCollection = new PhuKienDangKy().SelectCollectionDynamic("TrangThaiXuLy = 1 and HopDong_ID = " + HopDongID + " and MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "' and MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "'", "");

            dgPhuKien.DataSource = this.PKDKCollection;
        }
        private void dgPhuKien_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            PhuKienDangKySelected = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            DialogResult = DialogResult.OK;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgPhuKien.RootTable.Columns["SoPhuKien"], ConditionOperator.Contains, txtSoPK.Text);
                dgPhuKien.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}

