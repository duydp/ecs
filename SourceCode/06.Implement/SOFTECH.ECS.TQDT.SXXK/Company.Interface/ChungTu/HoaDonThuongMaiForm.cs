﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraCharts;
using Company.Interface.Report;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.BLL.DuLieuChuan;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class HoaDonThuongMaiForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public HoaDonThuongMai HDonTM = new HoaDonThuongMai();
        public List<HoaDonThuongMaiDetail> listHDTMDetail = new List<HoaDonThuongMaiDetail>();
        public bool isKhaiBoSung = false;
        public HoaDonThuongMaiForm()
        {
            InitializeComponent();

            SetEvent_TextBox_DoiTac();
        }

        private void HoaDonThuongMaiForm_Load(object sender, EventArgs e)
        {
            dgList.RootTable.Columns["MaPhu"].Selectable = false;

            ccNgayVanDon.Text = DateTime.Today.ToShortDateString();

            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            if (HDonTM != null && HDonTM.ID > 0)
            {
                txtMaDVBan.Text = HDonTM.MaDonViBan;
                txtMaDVMua.Text = HDonTM.MaDonViMua;
                ccNgayVanDon.Value = HDonTM.NgayHoaDon;
                nguyenTeControl2.Ma = HDonTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HDonTM.PTTT_ID;
                cbDKGH.SelectedValue = HDonTM.DKGH_ID;
                txtSoVanDon.Text = HDonTM.SoHoaDon;
                txtTenDVBan.Text = HDonTM.TenDonViBan;
                txtTenDVMua.Text = HDonTM.TenDonViMua;
                txtThongTinKhac.Text = HDonTM.ThongTinKhac;
                listHDTMDetail = HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(HDonTM.ID);
                BindData();
            }
            else
            {
                if (this.OpenType == Company.BLL.OpenFormType.Insert)
                {
                    txtSoVanDon.Text = TKMD.SoHoaDonThuongMai;
                    ccNgayVanDon.Text = TKMD.NgayHoaDonThuongMai.ToShortDateString();

                    cbPTTT.SelectedValue = TKMD.PTTT_ID;
                    cbDKGH.SelectedValue = TKMD.DKGH_ID;

                    nguyenTeControl2.Ma = TKMD.NguyenTe_ID;
                }
            }

            //Set Don vi mua, Don vi ban
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                txtMaDVMua.Text = GlobalSettings.MA_DON_VI;
                txtTenDVMua.Text = GlobalSettings.TEN_DON_VI;

                txtTenDVBan.Text = TKMD.TenDonViDoiTac;
            }
            else
            {
                txtMaDVBan.Text = GlobalSettings.MA_DON_VI;
                txtTenDVBan.Text = GlobalSettings.TEN_DON_VI;

                txtTenDVMua.Text = TKMD.TenDonViDoiTac;
            }

            //Set ValueList
            Globals.FillHSValueList(this.dgList.RootTable.Columns["MaHS"]);
            Globals.FillDonViTinhValueList(this.dgList.RootTable.Columns["DVT_ID"]);
            Globals.FillNuocXXValueList(this.dgList.RootTable.Columns["NuocXX_ID"]);

            if (HDonTM.SoTiepNhan > 0 && HDonTM.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = HDonTM.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = HDonTM.SoTiepNhan + "";
            }
            else if (HDonTM.SoTiepNhan > 0 && HDonTM.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
                ccNgayTiepNhan.Text = HDonTM.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = HDonTM.SoTiepNhan + "";
            }
            //HUNGTQ, Update 07/06/2010.
            //Thiết lập trạng thái các nút trên form
            SetButtonStateHOADON(TKMD, isKhaiBoSung, HDonTM);
        }

        private void BindData()
        {
            Company.BLL.KDT.HangMauDich hmd = new Company.BLL.KDT.HangMauDich();
            hmd.TKMD_ID = TKMD.ID;

            dgList.DataSource = HDonTM.ConvertListToDataSet(hmd.SelectBy_TKMD_ID().Tables[0]);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private bool checkSoHoaDon(string soHoaDon)
        {
            foreach (HoaDonThuongMai HDTM in TKMD.HoaDonThuongMaiCollection)
            {
                if (HDTM.SoHoaDon.Trim().ToUpper() == soHoaDon.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HDonTM.ListHangMDOfHoaDon.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }

            if (!ValidateHoaDon())
                return;

            TKMD.HoaDonThuongMaiCollection.Remove(HDonTM);

            if (checkSoHoaDon(txtSoVanDon.Text.Trim()))
            {
                if (HDonTM.ID > 0)
                    TKMD.HoaDonThuongMaiCollection.Add(HDonTM);
                ShowMessage("Số hóa đơn này đã tồn tại.", false);
                return;
            }
            HDonTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HDonTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HDonTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HDonTM.NgayHoaDon = ccNgayVanDon.Value;
            HDonTM.NguyenTe_ID = nguyenTeControl2.Ma;
            HDonTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HDonTM.SoHoaDon = txtSoVanDon.Text.Trim();
            HDonTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HDonTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HDonTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HDonTM.TKMD_ID = TKMD.ID;
            HDonTM.GuidStr = Guid.NewGuid().ToString();
            HDonTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            if (isKhaiBoSung)
                HDonTM.LoaiKB = 1;
            else
                HDonTM.LoaiKB = 0;

            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HoaDonThuongMaiDetail item in HDonTM.ListHangMDOfHoaDon)
                {
                    if (item.HMD_ID.ToString().Trim() == rowview["HMD_ID"].ToString().Trim())
                    {
                        item.GhiChu = rowview["GhiChu"].ToString();
                        item.GiaiTriDieuChinhGiam = Convert.ToDouble(rowview["GiaiTriDieuChinhGiam"]);
                        item.GiaTriDieuChinhTang = Convert.ToDouble(rowview["GiaTriDieuChinhTang"]);

                        item.MaHS = rowview["MaHS"].ToString();
                        item.MaPhu = rowview["MaPhu"].ToString();
                        item.TenHang = rowview["TenHang"].ToString();
                        item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
                        item.DVT_ID = rowview["DVT_ID"].ToString();
                        item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
                        item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
                        item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);

                        break;
                    }
                }
            }
            try
            {
                HDonTM.InsertUpdateFull();
                TKMD.HoaDonThuongMaiCollection.Add(HDonTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                btnKhaiBao.Enabled = true && isKhaiBoSung;
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HoaDonThuongMaiDetail> HoaDonThuongMaiDetailCollection = new List<HoaDonThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HoaDonThuongMaiDetail hdtmdtmp = new HoaDonThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HoaDonThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HoaDonThuongMaiDetail hdtmtmp in HoaDonThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HoaDonThuongMaiDetail hdtmd in HDonTM.ListHangMDOfHoaDon)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HDonTM.ListHangMDOfHoaDon.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public HangMauDichCollection ConvertListToHangMauDichCollection(List<HoaDonThuongMaiDetail> listHangHoaDon, HangMauDichCollection hangCollection)
        {

            HangMauDichCollection tmp = new HangMauDichCollection();

            foreach (HoaDonThuongMaiDetail item in listHangHoaDon)
            {
                foreach (Company.BLL.KDT.HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất HMD
            //if (HDonTM.ListHangMDOfHoaDon.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HDonTM.ListHangMDOfHoaDon, TKMD.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (Company.BLL.KDT.HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HoaDonThuongMaiDetail hangHoaDonDetail in HDonTM.ListHangMDOfHoaDon)
                    {
                        if (hangHoaDonDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HoaDonThuongMaiDetail hoaDonDetail = new HoaDonThuongMaiDetail();
                        hoaDonDetail.HMD_ID = HMD.ID;
                        hoaDonDetail.HoaDonTM_ID = HDonTM.ID;
                        hoaDonDetail.MaPhu = HMD.MaPhu;
                        hoaDonDetail.MaHS = HMD.MaHS;
                        hoaDonDetail.TenHang = HMD.TenHang;
                        hoaDonDetail.DVT_ID = HMD.DVT_ID;
                        hoaDonDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hoaDonDetail.SoLuong = HMD.SoLuong;
                        hoaDonDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hoaDonDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hoaDonDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);

                        HDonTM.ListHangMDOfHoaDon.Add(hoaDonDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HDonTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                //HDonTM.send(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NgayDangKy.Year, TKMD.ID, TKMD.ConvertHMDKDToHangMauDich());
                //btnLayPhanHoi.Enabled = true;
                bool thanhcong = HDonTM.WSKhaiBaoHoaDonTM(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, listHDTMDetail, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, TKMD.ConvertHMDKDToHangMauDich());

                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.HDonTM.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.HDonTM.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
                //HDonTM.LayPhanHoi(password, TKMD.MaHaiQuan);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HDonTM.WSLaySoTiepNhan(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.HDonTM.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.HDonTM.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.HDonTM.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = HDonTM.WSLayPhanHoi(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHoaDonTMDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HDonTM.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        #region Begin Doi tac TextBox

        /// <summary>
        /// Tạo sự kiện ButtonClick, Leave cho các TextBox Mã đơn vị mua, bán.
        /// </summary>
        /// Hungtq, Update 30052010
        private void SetEvent_TextBox_DoiTac()
        {
            txtMaDVMua.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            txtMaDVBan.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;

            txtMaDVMua.ButtonClick += new EventHandler(txtMaDVMua_ButtonClick);

            txtMaDVBan.ButtonClick += new EventHandler(txtMaDVBan_ButtonClick);

            txtMaDVMua.Leave += new EventHandler(txtMaDVMua_Leave);

            txtMaDVBan.Leave += new EventHandler(txtMaDVBan_Leave);
        }

        private void txtMaDVMua_ButtonClick(object sender, EventArgs e)
        {
            Company.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null && objDoiTac.MaCongTy != "")
            {
                txtMaDVMua.Text = objDoiTac.MaCongTy;
                txtTenDVMua.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVBan_ButtonClick(object sender, EventArgs e)
        {
            Company.BLL.DuLieuChuan.DoiTac objDoiTac = Globals.GetMaDonViObject();

            if (objDoiTac != null && objDoiTac.MaCongTy != "")
            {
                txtMaDVBan.Text = objDoiTac.MaCongTy;
                txtTenDVBan.Text = objDoiTac.TenCongTy;
            }
        }

        private void txtMaDVMua_Leave(object sender, EventArgs e)
        {
            if (txtMaDVMua.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVMua.Text.Trim()) != "")
                txtTenDVMua.Text = DoiTac.GetName(txtMaDVMua.Text.Trim());
        }

        private void txtMaDVBan_Leave(object sender, EventArgs e)
        {
            if (txtMaDVBan.Text.Trim().Length != 0 && DoiTac.GetName(txtMaDVBan.Text.Trim()) != "")
                txtTenDVBan.Text = DoiTac.GetName(txtMaDVBan.Text.Trim());
        }

        #endregion End Doi tac TextBox

        #region Begin VALIDATE HOA DON

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateHoaDon()
        {
            bool isValid = true;

            //So_CT	varchar(50)
            isValid = Globals.ValidateLength(txtSoVanDon, 50, err, "Số vận đơn");

            //Ma_PTTT	varchar(10)
            isValid &= Globals.ValidateLength(cbPTTT, 10, err, "Phương thức thanh toán");

            //Ma_GH	varchar(7)
            isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

            //Ma_NT	char(3)

            //Ma_DV	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVMua, 14, err, "Mã đơn vị mua");

            //Ma_DV_DT	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVBan, 14, err, "Mã đơn vị bán");

            return isValid;
        }

        #endregion End VALIDATE HOA DON

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOA DON.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOADON(BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai hoadon)
        {
            if (hoadon == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.SoTiepNhan == 0
                            || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                            || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);

                btnXoa.Enabled = status;
                btnChonHang.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXyLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    if (hoadon.SoTiepNhan > 0)
                        status = false;
                    else
                        status = true;

                    btnXoa.Enabled = status;
                    btnChonHang.Enabled = status;
                    btnGhi.Enabled = status;
                    btnChonHang.Enabled = status;
                    btnKetQuaXyLy.Enabled = true;
                    //Neu hop dong chua co so tiep nhan -> phai khai bao
                    if (hoadon.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        //btnLayPhanHoi.Enabled = true;
                    }


                }
            }

            return true;
        }

        #endregion

        private void btnKetQuaXyLy_Click(object sender, EventArgs e)
        {
            if (HDonTM.GuidStr != null && HDonTM.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(HDonTM.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void dgList_CellEdited(object sender, ColumnActionEventArgs e)
        {
            if (e.Column.Key.Equals("SoLuong") || e.Column.Key.Equals("DonGiaKB"))
            {
                decimal SoLuong = Convert.ToDecimal(dgList.CurrentRow.Cells["SoLuong"].Value);
                decimal DonGiaKB = Convert.ToDecimal(dgList.CurrentRow.Cells["DonGiaKB"].Value);
                dgList.CurrentRow.Cells["TriGiaKB"].Value = SoLuong * DonGiaKB;
            }
        }

    }
}

