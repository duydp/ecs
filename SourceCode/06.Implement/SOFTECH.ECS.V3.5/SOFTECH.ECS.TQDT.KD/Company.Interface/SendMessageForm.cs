using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using DevExpress.Utils.Drawing;
using DevExpress.XtraEditors;
using System;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using System.ServiceModel;
using System.Threading;
using System.Collections.Generic;
using SignRemote;
using Company.KDT.SHARE.Components.Messages.PhieuKho;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Xml;
using Company.KDT.SHARE.Components.Messages.SmartCA;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface
{
    public class SendMessageForm : XtraForm
    {
        public bool khaikho = false;
        public string MaHaiQuan;
        public bool Compress = true;
        public bool IsPort = false;
        public bool IsPortSend = false;
        public bool RegisterAccount = false;
        public bool LoginAccount = false;
        public bool SearchInformation = false;
        public bool CreateNotification = false;
        private Font _boldFont;
        private string _caption;
        private Font _font;
        private PictureBox _pic;
        private string _title;
        public string Caption
        {

            get
            {
                return _caption;
            }
            set
            {
                _caption = value;
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(Refresh));
                }
            }
        }



        public override bool AllowFormSkin
        {
            get
            {
                return false;
            }
        }

        public SendMessageForm()
            : this(string.Empty)
        {

        }

        public SendMessageForm(string caption)
            : this(caption, string.Empty)
        {
        }

        public SendMessageForm(string caption, string title)
            : this(caption, title, new Size(260, 50), null)
        {
        }

        public SendMessageForm(string caption, Size size)
            : this(caption, string.Empty, size, null)
        {
        }

        public SendMessageForm(string caption, string title, Size size)
            : this(caption, title, size, null)
        {
        }

        public SendMessageForm(string caption, string title, Size size, Form parent)
        {
            this._caption = string.Empty;
            this._title = string.Empty;

            this._caption = caption;
            this._title = title == string.Empty ? "Đang thực hiện. Vui lòng chờ giây lát." : title;
            _boldFont = new Font("Arial", 9.0F, FontStyle.Bold);
            _font = new Font("Arial", 9.0F);
            _pic = new PictureBox();
            FormBorderStyle = FormBorderStyle.FixedDialog;
            ControlBox = false;
            ClientSize = size;
            if (parent == null)
            {
                StartPosition = FormStartPosition.CenterScreen;
            }
            else
            {
                StartPosition = FormStartPosition.Manual;
                Left = parent.Left + ((parent.Width - Width) / 2);
                Top = parent.Top + ((parent.Height - Height) / 2);
            }
            ShowInTaskbar = false;
            TopMost = false;
            Paint += new PaintEventHandler(WaitDialogPaint);
            _pic.Size = new Size(16, 16);
            Size size1 = ClientSize;
            _pic.Location = new Point(8, (size1.Height / 2) - 16);

            _pic.Image = Globals.ResourceWait;
            if (_pic.Image == null && System.IO.File.Exists(@"Image/Wait.gif"))
                _pic.Image = Image.FromFile(@"Image/Wait.gif");

            Controls.Add(_pic);
            Refresh();
        }
        public string GetCaption()
        {
            return Caption;
        }

        public void SetCaption(string newCaption)
        {
            Caption = newCaption;
        }

        private void WaitDialogPaint(object sender, PaintEventArgs e)
        {
            Rectangle rectangle = e.ClipRectangle;
            rectangle.Inflate(-1, -1);
            if (_boldFont == null)
                _boldFont = new Font("Arial", 9.0F, FontStyle.Bold);
            if (_font == null)
                _font = new Font("Arial", 9.0F);
            GraphicsCache graphicsCache = new GraphicsCache(e);
            using (StringFormat stringFormat = new StringFormat())
            {

                Brush brush = graphicsCache.GetSolidBrush(LookAndFeelHelper.GetSystemColor(LookAndFeel, SystemColors.WindowText));
                StringAlignment stringAlignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;
                stringFormat.Alignment = stringAlignment;
                stringFormat.Trimming = StringTrimming.EllipsisCharacter;
                if (LookAndFeel.ActiveLookAndFeel.ActiveStyle == ActiveLookAndFeelStyle.Skin)
                    ObjectPainter.DrawObject(graphicsCache, new SkinTextBorderPainter(LookAndFeel), new BorderObjectInfoArgs(null, rectangle, null));
                else
                    ControlPaint.DrawBorder3D(e.Graphics, rectangle, Border3DStyle.RaisedInner);
                rectangle.X += 30;
                rectangle.Width -= 30;
                rectangle.Height /= 3;
                rectangle.Y += rectangle.Height / 2;
                e.Graphics.DrawString(_title, _boldFont, brush, rectangle, stringFormat);
                rectangle.Y += rectangle.Height;
                e.Graphics.DrawString(_caption, _font, brush, rectangle, stringFormat);
                graphicsCache.Dispose();
            }
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            _pic.Image = null;
            _boldFont = null;
            _font = null;
            base.OnClosing(e);
        }

        #region Process Send File Signature
        /// <summary>
        /// Gửi thông tin mặc định gỡ bỏ thuộc tính namespace
        /// </summary>
        /// <param name="objContent"></param>
        /// <returns></returns>
        #endregion

        #region Process Send Message


        private string _messageSend = string.Empty;
        private string _password = GlobalSettings.PassWordDT;
        private ObjectSend _objectSend;
        /// <summary>
        /// Gửi thông tin mặc định gỡ bỏ thuộc tính namespace
        /// </summary>
        /// <param name="objContent"></param>
        /// <returns></returns>
        public bool DoSend(ObjectSend objSend)
        {
            return DoSend(objSend, true);
        }
        /// <summary>
        /// Gửi thông tin phiếu kho service HQ Bình Dương
        /// </summary>
        /// <param name="objContent"></param>
        /// <returns></returns>
        public bool DoSend(ObjectSend objSend, int iskhaikho, string MaHaiQuan)
        {
            if (iskhaikho == 1)
            {
                this.khaikho = true;
                this.MaHaiQuan = MaHaiQuan;
            }
            return DoSend(objSend, true);
        }
        /// <summary>
        /// Gửi thông tin 
        /// </summary>
        /// <param name="objContent"></param>
        /// <param name="removeNameSpace"></param>
        /// <returns></returns>
        public bool DoSend(ObjectSend objSend, bool removeNameSpace)
        {
            return DoSend(objSend, true, removeNameSpace);
        }


        private bool _removeDeclartion;
        private bool _removeNameSpace;
        public bool DoSend(ObjectSend objSend, bool removeDeclartion, bool removeNameSpace)
        {

            if (objSend == null)
                throw new ArgumentNullException("Không có nội dung gửi đi");

            _objectSend = objSend;
            _removeDeclartion = removeDeclartion;
            _removeNameSpace = removeNameSpace;

            //Nếu cho phép sử dụng chữ ký số và
            //Không ghi nhớ mật khẩu và
            //Không ghi nhớ cho lần sau
            //Và không kết nối tới USB TOKEN
            //Thì quay ra. KHÔNG KÝ
            //LANNT
            if (Company.KDT.SHARE.Components.Globals.IsSignature
                && !Company.KDT.SHARE.Components.Globals.IsRememberSign
                && !Company.KDT.SHARE.Components.Globals.IsRememberSignOnline
                && new FrmLoginUSB().ShowDialog() != DialogResult.OK)
            {
                return false;
            }
            if (!GlobalSettings.IsRemember)
            {
                WSForm wsForm = new WSForm();
                wsForm.ShowDialog();
                if (!wsForm.IsReady) return false;
                _password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
            }
            ThreadPool.QueueUserWorkItem(DoWork);
            return ShowDialog() == DialogResult.OK;
        }

        public bool DoSendFile(ObjectSend objSend)
        {
            return DoSendFile(objSend, true);
        }
        public bool DoSendFile(ObjectSend objSend, int iskhaikho, string MaHaiQuan)
        {
            if (iskhaikho == 1)
            {
                this.khaikho = true;
                this.MaHaiQuan = MaHaiQuan;
            }
            return DoSendFile(objSend, true);
        }
        public bool DoSendFile(ObjectSend objSend, bool removeNameSpace)
        {
            return DoSendFile(objSend, true, removeNameSpace);
        }

        public bool DoSendFile(ObjectSend objSend, bool removeDeclartion, bool removeNameSpace)
        {
            if (!GlobalSettings.IsRemember)
            {
                WSForm wsForm = new WSForm();
                wsForm.ShowDialog();
                if (!wsForm.IsReady) return false;
                _password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
            }
            ThreadPool.QueueUserWorkItem(DoWorkFile);
            return ShowDialog() == DialogResult.OK;
        }
        public event EventHandler<SendEventArgs> Send;
        private IHaiQuanContract Service { get; set; }

        private Company.KDT.SHARE.Components.WSKTX_V3.CISService ServiceKTX { get; set; }

        private Company.KDT.SHARE.Components.WS.KDTService ServiceV4 { get; set; }

        private Company.KDT.SHARE.Components.WSHaiQuan.CISService CISService { get; set; }
        private Company.KDT.SHARE.Components.WSKDTServiceCKS.Service ServiceCKS { get; set; }

        private Company.KDT.SHARE.Components.WSKDTServiceAcc.Service ServiceV5 { get; set; }

        private Company.KDT.SHARE.Components.WS_HQBD_KHO.CFSService ServiceV5_BD { get; set; }
        private Company.KDT.SHARE.Components.WS_HQĐN_KHO.CFSService ServiceV5_ĐN { get; set; }

        private Company.KDT.SHARE.Components.WS_QLTP_HP.KDTService ServiceV5_QLTP_HP { get; set; }

        private TimeSpan _usedTime = new TimeSpan();
        private DateTime _lastStartTime;
        public TimeSpan TotalUsedTime
        {
            get
            {
                return _usedTime.Add(DateTime.Now - _lastStartTime);
            }
        }
        private int CountRequest(decimal len)
        {
            int timeRequest = 0;
            Random rnd1 = new Random();

            if (len < 1024)
                timeRequest = rnd1.Next(3, 7);
            else if (len > 1024 & len < 1048576)
                timeRequest = rnd1.Next(5, 9);
            else if (len > 1048576 & len < 107341824)
                timeRequest = rnd1.Next(10, 20);
            else timeRequest = rnd1.Next(30, 60);
            return timeRequest;
        }
        private MessageSignature GetSignatureRemote(string content)
        {

            ISignMessage signRemote = WebService.SignMessage();
            string guidStr = Guid.NewGuid().ToString();
            MessageSend msgSend = new MessageSend()
           {
               From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
               Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
               SendStatus = SendStatusInfo.Send,
               Message = new SignRemote.Message()
               {
                   Content = content,
                   GuidStr = guidStr
               },
               To = GlobalSettings.MA_DON_VI.Trim()
           };

            string msg = Helpers.Serializer(msgSend, true, true);
            SetCaption("Gửi ký thông tin");
            string msgRet = signRemote.ClientSend(msg);
            msgSend = Helpers.Deserialize<MessageSend>(msgRet);
            if (msgSend.SendStatus == SendStatusInfo.Error)
            {
                throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
            }
            MessageSend msgFeedBack = new MessageSend()
            {
                From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                SendStatus = SendStatusInfo.Request,
                Message = new SignRemote.Message()
                {
                    GuidStr = guidStr
                },
                To = GlobalSettings.MA_DON_VI.Trim()
            };
            msg = Helpers.Serializer(msgFeedBack, true, true);

            bool isWait = true;
            int countSend = 1;
            int countRequest = CountRequest(msg.Length);
            while (isWait && countSend < 5)
            {
                int count = countRequest;
                string title = this._title;
                this._title = string.Format("Thử lấy phản hồi ký số lần {0}", countSend);
                while (count > 0)
                {
                    SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                    Thread.Sleep(1000);
                    count--;
                }
                this._title = title;
                msgRet = signRemote.ClientSend(msg);
                msgSend = Helpers.Deserialize<MessageSend>(msgRet);
                if (msgSend.SendStatus == SendStatusInfo.Error)
                {
                    throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                }
                else if (msgSend.SendStatus == SendStatusInfo.Sign)
                {
                    return new MessageSignature()
                    {
                        Data = msgSend.Message.SignData,
                        FileCert = msgSend.Message.SignCert
                    };

                }
                else if (msgSend.SendStatus == SendStatusInfo.Wait)
                    countSend++;
            }
            throw new Exception(string.Format("ID={0}. Ký số từ xa thất bại\nMáy chủ đã không phản hồi thông tin trong thời gian giao dịch", guidStr));
        }
        private MessageSignature GetSignatureOnLan(string content)
        {
            //try
            //{
            string guidStr = Guid.NewGuid().ToString();
            string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, Company.KDT.SHARE.Components.Globals.DataSignLan, GlobalSettings.USER, GlobalSettings.PASS });
            string MSG_FROM = System.Environment.MachineName.ToString();
            INBOX.InsertSignContent(cnnString, guidStr, content, -1, MSG_FROM);
            int count = 0;
            int timeWait = 15;
            while (count < timeWait)
            {
                if (Company.KDT.SHARE.Components.Globals.MA_DON_VI != "4000395355")
                {
                    OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                    if (outbox != null && outbox.MSG_SIGN_DATA != null)
                    {
                        return new MessageSignature()
                        {
                            Data = outbox.MSG_SIGN_DATA,
                            FileCert = outbox.MSG_SIGN_CERT
                        };
                    }
                    else
                    {
                        count++;
                        SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                        Thread.Sleep(1000);
                    }
                }
                else
                {
                    //OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                    List<OUTBOX> outBoxCollection = OUTBOX.GetCollectionContentSignLocal(cnnString, guidStr);
                    foreach (OUTBOX outbox in outBoxCollection)
                    {
                        if (outbox != null && outbox.MSG_SIGN_DATA != null)
                        {
                            return new MessageSignature()
                            {
                                Data = outbox.MSG_SIGN_DATA,
                                FileCert = outbox.MSG_SIGN_CERT
                            };
                        }
                        else
                        {
                            count++;
                            SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                            Thread.Sleep(1000);
                        }
                    }
                }
            }
            throw new Exception("Đã gửi yêu cầu ký để khai báo\r\nTuy nhiên máy chủ đã không ký thông tin yêu cầu\r\nVui lòng xem thiết lập chữ ký số trên máy chủ");
            //}
            //catch (Exception ex)
            //{
            //    StreamWriter write = File.AppendText("Error.txt");
            //    write.WriteLine("--------------------------------");
            //    write.WriteLine("Lỗi GetSignatureOnLan lần 1. Thời gian thực hiện : " + DateTime.Now.ToString());
            //    write.WriteLine(ex.StackTrace);
            //    write.WriteLine("Lỗi là : ");
            //    write.WriteLine(ex.Message);
            //    write.WriteLine("--------------------------------");
            //    write.Flush();
            //    write.Close();
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    return null;
            //}

        }
        //public const string URL_CREDENTIALSLIST = "https://rmgateway.vnptit.vn/csc/credentials/list";
        //public const string URL_CREDENTIALSINFO = "https://rmgateway.vnptit.vn/csc/credentials/info";
        //public const string URL_SIGNHASH = "https://rmgateway.vnptit.vn/csc/signature/signhash";
        //public const string URL_SIGN = "https://rmgateway.vnptit.vn/csc/signature/sign";
        //public const string URL_GETTRANINFO = "https://rmgateway.vnptit.vn/csc/credentials/gettraninfo";

        private MessageSignature GetSignatureSmartCA(string content)
        {
            //var customerEmail = "0400392263123";
            //var customerPass = "Tchq@12345";
            var refresh_token = Company.KDT.SHARE.Components.Globals.REFRESH_TOKEN;
            var access_token = Company.KDT.SHARE.Components.Globals.ACCESS_TOKEN;
            DateTime expires_in = Convert.ToDateTime(Company.KDT.SHARE.Components.Globals.EXPIRES_TOKEN);
            SetCaption("Kiểm tra hiệu lực của Token");
            if (!String.IsNullOrEmpty(access_token))
            {
                if (DateTime.Now > expires_in)
                {
                    SetCaption("Lấy thông tin Token mới");
                    access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                    if (access_token == null)
                    {
                        throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                    }
                }
            }
            else
            {
                SetCaption("Lấy thông tin Token mới");
                access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                if (access_token == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                }
            }

            SetCaption("Lấy danh sách Chữ ký số");
            String credential = HelperVNACCS.GetCredentialSmartCA(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSLIST);
            if (credential == null)
            {
                throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được danh sách Chữ ký số"));
            }

            SetCaption("Lấy Thông tin Chữ ký số");
            String certBase64 = HelperVNACCS.GetAccoutSmartCACert(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSINFO, credential);
            if (certBase64 == null)
            {
                throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Chữ ký số"));
            }

            SetCaption("Lấy Thông tin giao dịch Chữ ký số");
            var tranId = HelperVNACCS.Sign(access_token, Company.KDT.SHARE.Components.Globals.URL_SIGN, content, credential);
            if (tranId == null)
            {
                throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Thông tin giao dịch Chữ ký số"));
            }
            bool isWait = true;
            int countSend = 1;
            var datasigned = "";
            int countRequest = 60;
            while (isWait && countSend < 6)
            {
                int count = countRequest;
                string title = string.Format("Chờ người dùng xác nhận {0}", countSend);
                while (count > 0)
                {
                    SetCaption(string.Format(title + ": {0} giây", count));
                    var getTranInfo = HelperVNACCS.GetTranInfo(access_token, Company.KDT.SHARE.Components.Globals.URL_GETTRANINFO, tranId);
                    if (getTranInfo != null)
                    {
                        if (getTranInfo.Content.TranStatus == TranStatus.WAITING_FOR_SIGNER_CONFIRM.ToString())
                        {
                            Thread.Sleep(1000);
                            count--;
                        }
                        else
                        {
                            switch (getTranInfo.Content.TranStatus)
                            {
                                case "1":
                                    return new MessageSignature()
                                    {
                                        Data = getTranInfo.Content.Documents[0].DataSigned,
                                        FileCert = certBase64
                                    };
                                    break;
                                case "4001":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.EXPIRED));
                                    break;
                                case "4002":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGNER_REJECTED));
                                    break;
                                case "4003":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.AUTHORIZE_KEY_FAILED));
                                    break;
                                case "4004":
                                    throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGN_FAILED));
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception(string.Format("ID={0}. Ký số không thành công\nKhông xác nhận được Thông tin chữ ký số giao dịch", tranId));
                    }
                }
                countSend++;
            }
            throw new Exception(string.Format("ID={0}. Ký số không thành công\nKhông xác nhận được Thông tin chữ ký số giao dịch", tranId));
        }

        public static string BămMD5(string input)
        {
            // Create a new instance of the MD5 object. 
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string. 
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string. 
            int i;
            for (i = 0; i <= data.Length - 1; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();

        }

        public string filebase64 = "";
        long filesize = 0;
        public long size;
        public byte[] data;
        public byte[] dataSigned;
        public string FileName = "";
        public string FileData = "";
        public string Extends = "";
        public MSG_FILE_OUTBOX MSG_FILE_OUTBOX = null;
        public SignRemote.MessageFile MessageFile = null;
        public class FileSigned
        {
            public string FileName { get; set; }
            public string FileData { get; set; }
        }
        private void DoWorkFile(object obj)
        {
            DialogResult result = DialogResult.Cancel;
            string sfmtFeedback = string.Empty;
            try
            {
                SetCaption("Đang gửi File ký");
                Thread.Sleep(300);
                FileSigned FileSigned = null;
                if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    SetCaption("Ký File trong mạng nội bộ");
                    Thread.Sleep(300);
                    SetCaption("Mã hóa thông tin File đính kèm");
                    Thread.Sleep(300);
                    MSG_FILE_OUTBOX = GetFileSignatureOnLan(FileName, filebase64);
                    if (MSG_FILE_OUTBOX != null)
                    {
                        FileName = MSG_FILE_OUTBOX.FILE_NAME;
                        FileData = MSG_FILE_OUTBOX.FILE_DATA;
                    }
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    SetCaption("Mã hóa thông tin File đính kèm");
                    Thread.Sleep(300);
                    SetCaption("Ký thông tin File đính kèm từ xa");
                    Thread.Sleep(300);
                    MessageFile = GetSignatureRemote(FileName, filebase64);
                    if (MessageFile != null)
                    {
                        FileName = MessageFile.FileName;
                        FileData = MessageFile.FileData;
                    }
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignSmartCA)
                {
                    SetCaption("Mã hóa thông tin File đính kèm");
                    Thread.Sleep(300);
                    SetCaption("Ký thông tin File đính kèm từ xa");
                    Thread.Sleep(300);
                    FileSigned = GetFileSignatureSmartCA(FileName, filebase64, Extends);
                    if (FileSigned != null)
                    {
                        FileName = FileSigned.FileName;
                        FileData = FileSigned.FileData;
                    }
                }
                if (FileData != null)
                {
                    SetCaption("Ký dữ liệu thành công");
                    Thread.Sleep(300);
                }
                else
                {
                    SetCaption("Ký dữ liệu không thành công");
                    Thread.Sleep(300);
                }
                //this.OnSend(new SendEventArgs(sfmtFeedback, this.TotalUsedTime, null));
                result = DialogResult.OK;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                this.DialogResult = result;
            }
        }
        private MSG_FILE_OUTBOX GetFileSignatureOnLan(string FileName, string FileData)
        {
            try
            {
                string guidStr = Guid.NewGuid().ToString();
                string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, GlobalSettings.DataSignLan, GlobalSettings.USER, GlobalSettings.PASS });
                string MSG_FROM = System.Environment.MachineName.ToString();
                MSG_FILE_INBOX.InsertSignFileContent(cnnString, guidStr, FileName, FileData, -1, MSG_FROM);
                int count = 0;
                int timeWait = 15;
                while (count < timeWait)
                {
                    MSG_FILE_OUTBOX outbox = MSG_FILE_OUTBOX.GetContentSignLocal(cnnString, guidStr);
                    if (outbox != null && outbox.FILE_DATA != null)
                    {
                        return outbox;
                    }
                    else
                    {
                        count++;
                        SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                        Thread.Sleep(1000);
                    }
                }
                throw new Exception("ĐÃ GỬI YÊU CẦU KÝ ĐỂ KHAI BÁO\r\nTUY NHIÊN MÁY CHỦ ĐÃ KHÔNG KÝ THÔNG TIN YÊU CẦU\r\nVUI LÒNG XEM THIẾT LẬP CHỮ KÝ SỐ TRÊN MÁY CHỦ");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private SignRemote.MessageFile GetSignatureRemote(string FileName, string FileData)
        {
            try
            {
                ISignMessage signRemote = WebService.SignMessage();
                string guidStr = Guid.NewGuid().ToString();
                MessageSendFile msgSend = new MessageSendFile()
                {
                    From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                    Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                    SendStatus = SendStatusInfo.Send,
                    Message = new SignRemote.MessageFile()
                    {
                        FileName = FileName,
                        FileData = FileData,
                        GuidStr = guidStr,
                    },
                    To = GlobalSettings.MA_DON_VI.Trim()
                };

                string msg = Helpers.Serializer(msgSend, true, true);
                SetCaption("Gửi ký thông tin");
                Thread.Sleep(300);
                string msgRet = signRemote.ClientSendFile(msg);
                msgSend = Helpers.Deserialize<MessageSendFile>(msgRet);
                if (msgSend.SendStatus == SendStatusInfo.Error)
                {
                    throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                }
                MessageSendFile msgFeedBack = new MessageSendFile()
                {
                    From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                    Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                    SendStatus = SendStatusInfo.Request,
                    Message = new SignRemote.MessageFile()
                    {
                        GuidStr = guidStr
                    },
                    To = GlobalSettings.MA_DON_VI.Trim()
                };
                msg = Helpers.Serializer(msgFeedBack, true, true);

                bool isWait = true;
                int countSend = 1;
                int countRequest = CountRequest(msg.Length);
                while (isWait && countSend < 5)
                {
                    int count = countRequest;
                    string title = this._title;
                    this._title = string.Format("Thử lấy phản hồi ký số lần {0}", countSend);
                    while (count > 0)
                    {
                        SetCaption(string.Format("Chờ lấy thông tin {0} giây", count));
                        Thread.Sleep(1000);
                        count--;
                    }
                    msgRet = signRemote.ClientSendFile(msg);
                    msgSend = Helpers.Deserialize<MessageSendFile>(msgRet);
                    if (msgSend.SendStatus == SendStatusInfo.Error)
                    {
                        throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                    }
                    else if (msgSend.SendStatus == SendStatusInfo.NoSign)
                    {
                        return new SignRemote.MessageFile
                        {
                            FileName = msgSend.Message.FileName,
                            FileData = msgSend.Message.FileData
                        };

                    }
                    else if (msgSend.SendStatus == SendStatusInfo.Wait)
                        countSend++;
                }
                throw new Exception(string.Format("ID={0}. Ký số từ xa thất bại\nMáy chủ đã không phản hồi thông tin trong thời gian giao dịch", guidStr));
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private FileSigned GetFileSignatureSmartCA(string FileName, string Content, string Extends)
        {
            try
            {
                //var customerEmail = "0400392263123";
                //var customerPass = "Tchq@12345";
                var refresh_token = Company.KDT.SHARE.Components.Globals.REFRESH_TOKEN;
                var access_token = Company.KDT.SHARE.Components.Globals.ACCESS_TOKEN;
                DateTime expires_in = Convert.ToDateTime(Company.KDT.SHARE.Components.Globals.EXPIRES_TOKEN);
                SetCaption("Kiểm tra hiệu lực của Token");
                Thread.Sleep(300);
                if (!String.IsNullOrEmpty(access_token))
                {
                    if (DateTime.Now > expires_in)
                    {
                        SetCaption("Lấy thông tin Token mới");
                        Thread.Sleep(300);
                        access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                        if (access_token == null)
                        {
                            throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                        }
                    }
                }
                else
                {
                    SetCaption("Lấy thông tin Token mới");
                    Thread.Sleep(300);
                    access_token = HelperVNACCS.GetAccessToken(Company.KDT.SHARE.Components.Globals.UserNameSmartCA, Company.KDT.SHARE.Components.Globals.PaswordSmartCA, refresh_token);
                    if (access_token == null)
                    {
                        throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được chuỗi Access Token"));
                    }
                }
                SetCaption("Lấy danh sách Chữ ký số");
                Thread.Sleep(300);
                String credential = HelperVNACCS.GetCredentialSmartCA(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSLIST);
                if (credential == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được danh sách Chữ ký số"));
                }
                SetCaption("Lấy Thông tin Chữ ký số");
                Thread.Sleep(300);
                String certBase64 = HelperVNACCS.GetAccoutSmartCACert(access_token, Company.KDT.SHARE.Components.Globals.URL_CREDENTIALSINFO, credential);
                if (certBase64 == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Chữ ký số"));
                }
                SetCaption("Lấy Thông tin giao dịch Chữ ký số");
                Thread.Sleep(300);
                var tranId = HelperVNACCS.SignDocument(access_token, Company.KDT.SHARE.Components.Globals.URL_SIGN, Content, credential, FileName, Content);
                if (tranId == null)
                {
                    throw new Exception(string.Format("Ký số không thành công\n {0}", "Không lấy được Thông tin giao dịch Chữ ký số"));
                }
                bool isWait = true;
                int countSend = 1;
                var datasigned = "";
                int countRequest = 60;
                while (isWait && countSend < 6)
                {
                    int count = countRequest;
                    string title = string.Format("Chờ người dùng xác nhận {0}", countSend);
                    while (count > 0)
                    {
                        SetCaption(string.Format(title + ": {0} giây", count));
                        var getTranInfo = HelperVNACCS.GetTranInfo(access_token, Company.KDT.SHARE.Components.Globals.URL_GETTRANINFO, tranId);
                        if (getTranInfo != null)
                        {
                            if (getTranInfo.Content.TranStatus == TranStatus.WAITING_FOR_SIGNER_CONFIRM.ToString())
                            {
                                Thread.Sleep(1000);
                                count--;
                            }
                            else
                            {
                                switch (getTranInfo.Content.TranStatus)
                                {
                                    case "1":
                                        return new FileSigned
                                        {
                                            FileData = getTranInfo.Content.Documents[0].DataSigned,
                                            FileName = getTranInfo.Content.Documents[0].Name
                                        };
                                        break;
                                    case "4001":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.EXPIRED));
                                        break;
                                    case "4002":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGNER_REJECTED));
                                        break;
                                    case "4003":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.AUTHORIZE_KEY_FAILED));
                                        break;
                                    case "4004":
                                        throw new Exception(string.Format("ID={0}. Ký số không thành công\n {1}", tranId, TranStatusString.SIGN_FAILED));
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        else
                        {
                        }
                    }
                    countSend++;
                }
                return null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void DoWork(object obj)
        {
            Compress = false;
            DialogResult result = DialogResult.Cancel;
            string sfmtFeedback = string.Empty;
            Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information contentXML = new Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information();
            try
            {

                #region Build message Send
                SetCaption("Đóng gói dữ liệu");
                Thread.Sleep(300);
                if (khaikho)
                {
                    Helpers.khaikho = true;
                }
                else if (IsPort)
                {
                    Helpers.isPort = true;
                }
                MessageSend<string> msgSend = Helpers.Envelope<string>(_objectSend.From, _objectSend.To, _objectSend.Subject);

                string content = string.Empty;
                if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                {
                    string subType = _objectSend.Subject.Type;
                    _objectSend.Subject.Type = null;
                    content = Helpers.Serializer(_objectSend.Subject, System.Text.Encoding.UTF8, true, true);
                    _objectSend.Subject.Type = subType;
                }
                else
                {
                    try
                    {
                        if (_objectSend.Content != null)
                        {
                            if (RegisterAccount)
                            {
                                contentXML = (Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information)_objectSend.Content;
                            }
                            else
                            {
                                content = Helpers.Serializer(_objectSend.Content, _removeDeclartion, _removeNameSpace);
                                decimal ToTalXMLSize = System.Text.Encoding.Unicode.GetByteCount(content);
                                //decimal getSize = System.Text.Encoding.ASCII.GetByteCount(content);
                                // Nếu File XML lớn hơn 4M thì nén File rồi gửi đi .
                                if (ToTalXMLSize > 4194304)
                                    Compress = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }

                MessageSignature signature = null;
                if (Company.KDT.SHARE.Components.Globals.IsSignature)
                {
                    SetCaption("Ký thông tin");
                    Thread.Sleep(300);
                    //test
                    signature = Helpers.GetSignature(content);
                    SetCaption("Mã hóa thông tin");
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                    {
                        if (!Compress)
                        {
                            if (!RegisterAccount)
                            {
                                content = Helpers.ConvertToBase64VNACCS(content);
                            }
                        }
                    }
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
                {
                    SetCaption("Mã hóa thông tin");
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                    {
                        if (!Compress)
                        {
                            if (!RegisterAccount)
                            {
                                content = Helpers.ConvertToBase64(content);
                            }
                        }
                    }
                    SetCaption("Ký thông tin từ xa");
                    Thread.Sleep(300);
                    signature = GetSignatureRemote(content);
                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
                {
                    if (string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.DataSignLan))
                        throw new Exception("Loại ký số trong mạng nội bộ chưa xác nhận cơ sở dữ liệu\r\nVui lòng vào cấu hình chữ ký số để thiết lập");
                    SetCaption("Ký dữ liệu nội bộ");
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                    {
                        if (!Compress)
                        {
                            if (!RegisterAccount)
                            {
                                content = Helpers.ConvertToBase64(content);
                            }
                        }
                        signature = GetSignatureOnLan(content);
                    }
                    else if (Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                    {
#if DEBUG
                        //signature = new MessageSignature();
                        //signature.Data = "oWaxAD594cf5B2DKrCVe0WRVqOYU1DwKiAbTFrD1ykBNYQw16oZX7dV64hPDsmfKp5N3DSUuwJPnHLTpCMzJKGs8MOfxP5BEPjMUkeLhQGUnpgzHc8A1hTtOXgLLdHg8ot3tQ+74d8jnGTNrjKFc55zyW+nOqPWGOzgmHLxq8y8=";
                        //signature.FileCert = "MIIFozCCA4ugAwIBAgIQVAGyNE0txifZVEjiUOPNfjANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE3MDczMTA3NTcxN1oXDTE5MDEzMTE5NDgwMFowgYAxCzAJBgNVBAYTAlZOMRQwEgYDVQQIDAvEkMOAIE7hurRORzEUMBIGA1UEBwwLSOG6o2kgQ2jDonUxJTAjBgNVBAMMHEPDlE5HIFRZIEPhu5QgUEjhuqZOIFNPRlRFQ0gxHjAcBgoJkiaJk/IsZAEBDA5NU1Q6MDQwMDM5MjI2MzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApAmxBCAYxDcqS4zcWeIU7qcA1kVRAoMnBtTaV43Aw8qflNkpIMrpadX2nHKqvNv1zcu8pxAb1/rzGyI1elbvQTRtmAgrnC4Y0uFXHy81Nrluawks2JxCWk8XdHw7LwjEi8tbpFR66YS8lWbHCNjCQ6HXKnzrmy21A2lj+AldKXECAwEAAaOCAbEwggGtMHAGCCsGAQUFBwEBBGQwYjAyBggrBgEFBQcwAoYmaHR0cDovL3B1Yi52bnB0LWNhLnZuL2NlcnRzL3ZucHRjYS5jZXIwLAYIKwYBBQUHMAGGIGh0dHA6Ly9vY3NwLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBTctNZsI9L2rnMFU1yPVpiiJQ0QkTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAZpwNXVAooVjUZ96XziaApVrGqvMGgGA1UdIARhMF8wXQYOKwYBBAGB7QMBAQMBAQEwSzAiBggrBgEFBQcCAjAWHhQATwBJAEQALQBTAFQALQAxAC4AMDAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTAxBgNVHR8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZucHQtY2Eudm4vdm5wdGNhLmNybDAOBgNVHQ8BAf8EBAMCBPAwIAYDVR0lBBkwFwYKKwYBBAGCNwoDDAYJKoZIhvcvAQEFMBwGA1UdEQQVMBOBEXZpbmhudkBzb2Z0ZWNoLnZuMA0GCSqGSIb3DQEBBQUAA4ICAQDKYRYeyLJo1nruD4/tiKndnfypHar+RL+Q2mVVpd2l74O15e2kCb/WlNs+1DN8WlupB8GNAfsCHjV83Vbkr3bUZfd1LJz4m2YbxkC1uL1MT8Uclp8Ae4DcPvu6FlDn/tKoiOVOf99giweya6gahpsEcH8yrr6Da2Huma4OSTtBGZsF4beiqUGsSERiPO4HnOyDqTxODt9TE/N8am9yEgftja5N/Vn7tVe4mfnVDp2Vq9YXCQiWKkO35XifiCCTqYLuE25BwcWKgUaCy9rgXogYyQxZxa90LkBbrdsnA2ltDRORtmNBoH7neaRiJo8O5YciFK8ZvOy8YLsv7dYysZp2vE5XOu1X4NUbFnaS5BnM58hZCvdRUMeQ4o/Kwuuz/XkeUHGixC4LaqFGidA15EgKy4Cupta+vDe5mCHhhbdSq97KOAYb9OQV/nGFO2InwfnPDRF2qQ+Y1xk1QGE0mai4l5AMjmBVwmUyUYFLebJLQ/GO1vd5Hx2ykVuyrf4x7okbDmY8ZD9VPMQXEir2YMgQ12mVttr2DyQSz2X/8GY+tWeaBqNzSYnioqR69MjKPoqmXtRt4nk6TiQr4ThLnUAtJfzro0luaNJvxuU5GnDk9z2gQpTekl4oejdkDGmGbH9D7mKeX1ftdKQoQj9Mal557bZCYDnnfpa1iU1P+T5+Ig==";
                        if (!Compress)
                        {
                            content = Helpers.ConvertToBase64(content);
                        }
                        signature = GetSignatureOnLan(content);
#endif
                    }
                    else if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    {
                        //signature = new MessageSignature();
                        //signature.Data = "oWaxAD594cf5B2DKrCVe0WRVqOYU1DwKiAbTFrD1ykBNYQw16oZX7dV64hPDsmfKp5N3DSUuwJPnHLTpCMzJKGs8MOfxP5BEPjMUkeLhQGUnpgzHc8A1hTtOXgLLdHg8ot3tQ+74d8jnGTNrjKFc55zyW+nOqPWGOzgmHLxq8y8=";
                        //signature.FileCert = "MIIFozCCA4ugAwIBAgIQVAGyNE0txifZVEjiUOPNfjANBgkqhkiG9w0BAQUFADBpMQswCQYDVQQGEwJWTjETMBEGA1UEChMKVk5QVCBHcm91cDEeMBwGA1UECxMVVk5QVC1DQSBUcnVzdCBOZXR3b3JrMSUwIwYDVQQDExxWTlBUIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE3MDczMTA3NTcxN1oXDTE5MDEzMTE5NDgwMFowgYAxCzAJBgNVBAYTAlZOMRQwEgYDVQQIDAvEkMOAIE7hurRORzEUMBIGA1UEBwwLSOG6o2kgQ2jDonUxJTAjBgNVBAMMHEPDlE5HIFRZIEPhu5QgUEjhuqZOIFNPRlRFQ0gxHjAcBgoJkiaJk/IsZAEBDA5NU1Q6MDQwMDM5MjI2MzCBnzANBgkqhkiG9w0BAQEFAAOBjQAwgYkCgYEApAmxBCAYxDcqS4zcWeIU7qcA1kVRAoMnBtTaV43Aw8qflNkpIMrpadX2nHKqvNv1zcu8pxAb1/rzGyI1elbvQTRtmAgrnC4Y0uFXHy81Nrluawks2JxCWk8XdHw7LwjEi8tbpFR66YS8lWbHCNjCQ6HXKnzrmy21A2lj+AldKXECAwEAAaOCAbEwggGtMHAGCCsGAQUFBwEBBGQwYjAyBggrBgEFBQcwAoYmaHR0cDovL3B1Yi52bnB0LWNhLnZuL2NlcnRzL3ZucHRjYS5jZXIwLAYIKwYBBQUHMAGGIGh0dHA6Ly9vY3NwLnZucHQtY2Eudm4vcmVzcG9uZGVyMB0GA1UdDgQWBBTctNZsI9L2rnMFU1yPVpiiJQ0QkTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFAZpwNXVAooVjUZ96XziaApVrGqvMGgGA1UdIARhMF8wXQYOKwYBBAGB7QMBAQMBAQEwSzAiBggrBgEFBQcCAjAWHhQATwBJAEQALQBTAFQALQAxAC4AMDAlBggrBgEFBQcCARYZaHR0cDovL3B1Yi52bnB0LWNhLnZuL3JwYTAxBgNVHR8EKjAoMCagJKAihiBodHRwOi8vY3JsLnZucHQtY2Eudm4vdm5wdGNhLmNybDAOBgNVHQ8BAf8EBAMCBPAwIAYDVR0lBBkwFwYKKwYBBAGCNwoDDAYJKoZIhvcvAQEFMBwGA1UdEQQVMBOBEXZpbmhudkBzb2Z0ZWNoLnZuMA0GCSqGSIb3DQEBBQUAA4ICAQDKYRYeyLJo1nruD4/tiKndnfypHar+RL+Q2mVVpd2l74O15e2kCb/WlNs+1DN8WlupB8GNAfsCHjV83Vbkr3bUZfd1LJz4m2YbxkC1uL1MT8Uclp8Ae4DcPvu6FlDn/tKoiOVOf99giweya6gahpsEcH8yrr6Da2Huma4OSTtBGZsF4beiqUGsSERiPO4HnOyDqTxODt9TE/N8am9yEgftja5N/Vn7tVe4mfnVDp2Vq9YXCQiWKkO35XifiCCTqYLuE25BwcWKgUaCy9rgXogYyQxZxa90LkBbrdsnA2ltDRORtmNBoH7neaRiJo8O5YciFK8ZvOy8YLsv7dYysZp2vE5XOu1X4NUbFnaS5BnM58hZCvdRUMeQ4o/Kwuuz/XkeUHGixC4LaqFGidA15EgKy4Cupta+vDe5mCHhhbdSq97KOAYb9OQV/nGFO2InwfnPDRF2qQ+Y1xk1QGE0mai4l5AMjmBVwmUyUYFLebJLQ/GO1vd5Hx2ykVuyrf4x7okbDmY8ZD9VPMQXEir2YMgQ12mVttr2DyQSz2X/8GY+tWeaBqNzSYnioqR69MjKPoqmXtRt4nk6TiQr4ThLnUAtJfzro0luaNJvxuU5GnDk9z2gQpTekl4oejdkDGmGbH9D7mKeX1ftdKQoQj9Mal557bZCYDnnfpa1iU1P+T5+Ig==";

                        signature = GetSignatureOnLan(content);
                    }
                    //test //signature
                    Thread.Sleep(300);

                }
                else if (Company.KDT.SHARE.Components.Globals.IsSignSmartCA)
                {
                    SetCaption("Ký thông tin");
                    Thread.Sleep(300);
                    //test
                    signature = GetSignatureSmartCA(content);
                    SetCaption("Mã hóa thông tin");
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                    {
                        if (!Compress)
                        {
                            if (!RegisterAccount)
                            {
                                content = Helpers.ConvertToBase64VNACCS(content);
                            }
                        }
                    }
                }
                else
                {
                    SetCaption("Mã hóa thông tin");
                    if (!Compress)
                    {
                        content = Helpers.ConvertToBase64(content);
                    }
                }
                if (!Compress)
                {
                    if (RegisterAccount)
                    {
                        msgSend.MessageBody.MessageContent.Register_Information = contentXML;
                    }
                    else
                    {
                        msgSend.MessageBody.MessageContent.Text = content;
                    }
                }
                else
                {
                    msgSend.MessageBody.MessageContent.Text = Helpers.CompressString(content);
                }
                if (signature != null)
                {
                    msgSend.MessageBody.MessageSignature = signature;
                }
                SetCaption("Đồng bộ thông tin");
                Thread.Sleep(300);
                if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                {
                    this._messageSend = Helpers.Serializer(msgSend, true, true);
                }
                else
                {
                    if (!RegisterAccount)
                    {
                        this._messageSend = Helpers.Serializer(msgSend);
                    }
                }
                #endregion
                //minhnd 27/0/2015 them khai báo phiếu kho
                if (khaikho)
                {
                    if (MaHaiQuan.Substring(0, 2) == "43")
                    {
                        ServiceV5_BD = WebService.HQServiceV5_BD(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                    }
                    else if (MaHaiQuan.Substring(0, 2) == "47")
                    {
                        ServiceV5_ĐN = WebService.HQServiceV5_ĐN(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                    }
                }
                else if (IsPort)
                {

                    ServiceV5_QLTP_HP = WebService.HQServiceV5_QLTP_HP(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                }
                else
                {
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS || Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                    {

                        ServiceV5 = WebService.HQServiceV5(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                    }
                    else
                    {
                        //CISService = WebService.CISService(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                        //ServiceCKS = WebService.ServiceCKS(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                        ServiceKTX = WebService.HQServiceKTX(GlobalSettings.IsDaiLy, GlobalSettings.MA_DON_VI);
                    }
                }
                if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                {
                    int count = Company.KDT.SHARE.Components.Globals.Delay;
                    while (count > 0)
                    {
                        SetCaption(string.Format("Còn lại {0} giây", count));
                        Thread.Sleep(1000);
                        count--;
                    }
                }
                SetCaption("Đang truyền dữ liệu lên Hải quan");
                this._lastStartTime = DateTime.Now;
                if (khaikho)
                {
                    if (MaHaiQuan.Substring(0, 2) == "43")
                    {
                        sfmtFeedback = ServiceV5_BD.Send(_messageSend, GlobalSettings.UserId, _password);
                    }
                    else if (MaHaiQuan.Substring(0, 2) == "47")
                    {
                        sfmtFeedback = ServiceV5_ĐN.Send(_messageSend, GlobalSettings.UserId, _password);
                    }
                }
                else if (IsPort)
                {
                    if (LoginAccount)
                    {
                        sfmtFeedback = ServiceV5_QLTP_HP.LoginAcc(_messageSend, signature.FileCert);
                    }
                    else if (RegisterAccount)
                    {
                        sfmtFeedback = ServiceV5_QLTP_HP.RegisterAcc(Helpers.Serializer(contentXML));
                    }
                    else
                    {
                        if (Compress)
                        {
                            sfmtFeedback = ServiceV5_QLTP_HP.SendCompress(_messageSend, true);
                        }
                        else
                        {
                            if (_objectSend.Subject.Function == DeclarationFunction.HOI_TRANG_THAI)
                            {
                                sfmtFeedback = ServiceV5_QLTP_HP.Request(_messageSend);
                            }
                            else
                            {
                                sfmtFeedback = ServiceV5_QLTP_HP.Send(_messageSend);
                            }
                        }
                    }
                }
                else
                {
                    if (Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS)
                    {
                        if (!Compress)
                        {
                            sfmtFeedback = ServiceV5.Process(_messageSend);
                        }
                        else
                        {
                            sfmtFeedback = ServiceV5.Send(_messageSend, true);
                        }
                    }
                    else if (Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                    {
                        sfmtFeedback = ServiceV5.Process(_messageSend);
                    }
                    else if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    {
                        //sfmtFeedback = CISService.Send(_messageSend, GlobalSettings.UserId, _password);
                        //sfmtFeedback = ServiceCKS.Send(_messageSend,false);
                        sfmtFeedback = ServiceKTX.Send(_messageSend, GlobalSettings.UserId, _password);
                    }
                }
                //});
#if DEBUG
                //System.Xml.XmlDocument xmlTest = new System.Xml.XmlDocument();
                //xmlTest.Load("c:\\TK_Sua_6113_XSX01.xml");
                //sfmtFeedback = xmlTest.InnerXml;
#endif
                SetCaption("Truyền dữ liệu thành công");
                this.OnSend(new SendEventArgs(sfmtFeedback, this.TotalUsedTime, null));
                result = DialogResult.OK;
            }
            catch (Exception ex)
            {
                this._title = "Thực hiện thất bại";
                SetCaption("Truyền dữ liệu thất bại");
                string filename = string.Empty;
                string sfmt = string.Empty;
                if (ex.Message.Contains("Could not connect"))
                {

                    if (!GlobalSettings.IsDaiLy)
                        sfmt = string.Format("Địa chỉ:{0}\r\nKhông kết nối đến hệ thống Hải quan", WebService.LoadConfigure("WS_URL"));
                    else
                        sfmt = string.Format("Địa chỉ:{0}\r\nKhông kết nối đến hệ thống Hải quan", GlobalSettings.DiaChiWS);


                }
                else if (ex.Message.Contains("SendTimeout"))
                {
                    sfmt = string.Format("Đường truyền mạng internet bị ngắt kết nối khi đang truyền dữ liệu\r\n" +
                                               "Thời gian giữ kết nối internet là {0} giây\r\n", Company.KDT.SHARE.Components.Globals.TimeConnect);
                }
                else if (ex.Message.Contains("There was no endpoint listening at"))
                {
                    sfmt = string.Format("Địa chỉ:\r\n{0}\r\nĐịa chỉ không hợp lệ vì địa chỉ trên không phải là loại địa chỉ khai báo\r\nVui lòng thiết lập lại địa chỉ khai báo", WebService.LoadConfigure("WS_URL"));
                }
                else if (ex.Message.Contains("Maximum request length exceeded"))
                {

                    sfmt = string.Format("Dung lượng bạn gửi đi là {0} đã vượt mức cho phép của hệ thống Hải quan\r\n" +
                                        "Vui lòng giảm dung lượng(nếu có tệp tin đính kèm) đến mức tối thiểu cho phù hợp với hệ thống Hải quan", CalculateFileSize((decimal)_messageSend.Length));
                }
                else if (ex.Message.Contains("connection was closed"))
                {
                    sfmt = string.Format("Hệ thống đã đóng kết nối truy cập");
                }

                filename = Company.KDT.SHARE.Components.Globals.Message2File(_messageSend);
                if (!string.IsNullOrEmpty(sfmtFeedback))
                    filename = Company.KDT.SHARE.Components.Globals.Message2File(sfmtFeedback);

                if (!string.IsNullOrEmpty(sfmt))
                    ex = new Exception(sfmt);
                OnSend(new SendEventArgs(
                    filename, this.TotalUsedTime, ex));
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.DialogResult = result;
            }
        }
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        public string Message
        {
            get
            {
                return this._messageSend;
            }
        }
        private void OnSend(SendEventArgs e)
        {
            if (Send != null)
            {
                Send(this, e);
            }
        }
        #endregion Process Send Message

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SendMessageForm
            // 
            this.ClientSize = new System.Drawing.Size(284, 265);
            this.LookAndFeel.SkinName = "Office 2007 Blue";
            this.Name = "SendMessageForm";
            this.ResumeLayout(false);

        }

    }
    public enum CommandStatus
    {
        Send = 0,
        FeedBack,
        Cancel
    }
    interface ICommand
    {
        void Send();
    }
    class Command : ICommand
    {
        private SendMessageForm _sendForm = null;
        private string _message = string.Empty;
        public Command(SendMessageForm sendForm, string message)
        {
            _sendForm = sendForm;
            _message = message;
        }
        public void Send()
        {
            // _sendForm.DoSend(_message);
        }
    }
    public class SendCommand
    {
        public SendMessageForm SendMessageForm { get; set; }
        private Dictionary<CommandStatus, ICommand> _commands = new Dictionary<CommandStatus, ICommand>();
        public void Add(CommandStatus status, string message)
        {
            Remove(status);
            ICommand cmd = new Command(SendMessageForm, message);
            _commands.Add(status, cmd);
        }
        public void Clear()
        {
            _commands.Clear();
        }
        public void Remove(CommandStatus status)
        {
            _commands.Remove(status);
        }
        public void Send(CommandStatus status)
        {
            ICommand cmd = _commands[status];
            cmd.Send();
        }
    }

}

