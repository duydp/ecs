namespace Company.Interface.Report.VNACCS
{
    partial class ReportViewDinhDanhHangHoa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLine26 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine25 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine24 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine23 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoThamChieuNoiBoCPN = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoThamChieuNoiBoCP = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoThamChieuNoiBoCoppy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoThamChieuNoiBo = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer8 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picBarCodeCPN = new System.Windows.Forms.PictureBox();
            this.winControlContainer7 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picBarCodeCP = new System.Windows.Forms.PictureBox();
            this.winControlContainer6 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picQrCodeCPN = new System.Windows.Forms.PictureBox();
            this.winControlContainer5 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picQrCodeCP = new System.Windows.Forms.PictureBox();
            this.xrLine20 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoTKXKCPN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiepCPN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaDoanhNghiepCPN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoDinhDanhCPN = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoTKXKCP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiepCP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaDoanhNghiepCP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoDinhDanhCP = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.winControlContainer4 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picBarCodeCoppy = new System.Windows.Forms.PictureBox();
            this.winControlContainer3 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picQrCodeCoppy = new System.Windows.Forms.PictureBox();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoTKXK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoDinhDanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picQrCode = new System.Windows.Forms.PictureBox();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.winControlContainer2 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.picBarCode = new System.Windows.Forms.PictureBox();
            this.xrLine7 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoTKXKCoppy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine8 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiepCoppy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiepCoppy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDinhDanhCoppy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine9 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCodeCPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCodeCP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCodeCPN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCodeCP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCodeCoppy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCodeCoppy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine26,
            this.xrLine25,
            this.xrLine24,
            this.xrLine23,
            this.lblSoThamChieuNoiBoCPN,
            this.lblSoThamChieuNoiBoCP,
            this.lblSoThamChieuNoiBoCoppy,
            this.lblSoThamChieuNoiBo,
            this.winControlContainer8,
            this.winControlContainer7,
            this.winControlContainer6,
            this.winControlContainer5,
            this.xrLine20,
            this.lblSoTKXKCPN,
            this.xrLine19,
            this.xrLabel23,
            this.lblTenDoanhNghiepCPN,
            this.xrLabel21,
            this.xrLine18,
            this.lblMaDoanhNghiepCPN,
            this.xrLabel19,
            this.xrLine17,
            this.lblSoDinhDanhCPN,
            this.xrLabel17,
            this.xrLine16,
            this.xrLine15,
            this.lblSoTKXKCP,
            this.xrLine14,
            this.xrLabel15,
            this.lblTenDoanhNghiepCP,
            this.xrLabel13,
            this.xrLine13,
            this.lblMaDoanhNghiepCP,
            this.xrLabel10,
            this.xrLine12,
            this.lblSoDinhDanhCP,
            this.xrLabel2,
            this.xrLine11,
            this.winControlContainer4,
            this.winControlContainer3,
            this.xrLine6,
            this.xrLine5,
            this.lblSoTKXK,
            this.xrLine4,
            this.xrLabel7,
            this.lblTenDoanhNghiep,
            this.xrLabel5,
            this.xrLine1,
            this.lblMaDoanhNghiep,
            this.xrLabel3,
            this.xrLine2,
            this.lblSoDinhDanh,
            this.xrLabel1,
            this.winControlContainer1,
            this.xrLine3,
            this.winControlContainer2,
            this.xrLine7,
            this.lblSoTKXKCoppy,
            this.xrLine8,
            this.xrLabel4,
            this.xrLabel6,
            this.xrLabel8,
            this.lblTenDoanhNghiepCoppy,
            this.lblMaDoanhNghiepCoppy,
            this.lblSoDinhDanhCoppy,
            this.xrLabel12,
            this.xrLine9,
            this.xrLine10});
            this.Detail.HeightF = 1100F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine26
            // 
            this.xrLine26.LineWidth = 2;
            this.xrLine26.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 1004.717F);
            this.xrLine26.Name = "xrLine26";
            this.xrLine26.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLine25
            // 
            this.xrLine25.LineWidth = 2;
            this.xrLine25.LocationFloat = new DevExpress.Utils.PointFloat(46.85669F, 1004.717F);
            this.xrLine25.Name = "xrLine25";
            this.xrLine25.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLine24
            // 
            this.xrLine24.LineWidth = 2;
            this.xrLine24.LocationFloat = new DevExpress.Utils.PointFloat(447.9218F, 446.3335F);
            this.xrLine24.Name = "xrLine24";
            this.xrLine24.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLine23
            // 
            this.xrLine23.LineWidth = 2;
            this.xrLine23.LocationFloat = new DevExpress.Utils.PointFloat(47.3917F, 446.3335F);
            this.xrLine23.Name = "xrLine23";
            this.xrLine23.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoThamChieuNoiBoCPN
            // 
            this.lblSoThamChieuNoiBoCPN.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThamChieuNoiBoCPN.LocationFloat = new DevExpress.Utils.PointFloat(447.7917F, 1019.717F);
            this.lblSoThamChieuNoiBoCPN.Multiline = true;
            this.lblSoThamChieuNoiBoCPN.Name = "lblSoThamChieuNoiBoCPN";
            this.lblSoThamChieuNoiBoCPN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoThamChieuNoiBoCPN.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblSoThamChieuNoiBoCPN.StylePriority.UseFont = false;
            this.lblSoThamChieuNoiBoCPN.StylePriority.UseTextAlignment = false;
            this.lblSoThamChieuNoiBoCPN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoThamChieuNoiBoCP
            // 
            this.lblSoThamChieuNoiBoCP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThamChieuNoiBoCP.LocationFloat = new DevExpress.Utils.PointFloat(46.85167F, 1019.717F);
            this.lblSoThamChieuNoiBoCP.Multiline = true;
            this.lblSoThamChieuNoiBoCP.Name = "lblSoThamChieuNoiBoCP";
            this.lblSoThamChieuNoiBoCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoThamChieuNoiBoCP.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblSoThamChieuNoiBoCP.StylePriority.UseFont = false;
            this.lblSoThamChieuNoiBoCP.StylePriority.UseTextAlignment = false;
            this.lblSoThamChieuNoiBoCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoThamChieuNoiBoCoppy
            // 
            this.lblSoThamChieuNoiBoCoppy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThamChieuNoiBoCoppy.LocationFloat = new DevExpress.Utils.PointFloat(447.9166F, 461.3335F);
            this.lblSoThamChieuNoiBoCoppy.Multiline = true;
            this.lblSoThamChieuNoiBoCoppy.Name = "lblSoThamChieuNoiBoCoppy";
            this.lblSoThamChieuNoiBoCoppy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoThamChieuNoiBoCoppy.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblSoThamChieuNoiBoCoppy.StylePriority.UseFont = false;
            this.lblSoThamChieuNoiBoCoppy.StylePriority.UseTextAlignment = false;
            this.lblSoThamChieuNoiBoCoppy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoThamChieuNoiBo
            // 
            this.lblSoThamChieuNoiBo.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoThamChieuNoiBo.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 461.3335F);
            this.lblSoThamChieuNoiBo.Multiline = true;
            this.lblSoThamChieuNoiBo.Name = "lblSoThamChieuNoiBo";
            this.lblSoThamChieuNoiBo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoThamChieuNoiBo.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblSoThamChieuNoiBo.StylePriority.UseFont = false;
            this.lblSoThamChieuNoiBo.StylePriority.UseTextAlignment = false;
            this.lblSoThamChieuNoiBo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // winControlContainer8
            // 
            this.winControlContainer8.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 948.2999F);
            this.winControlContainer8.Name = "winControlContainer8";
            this.winControlContainer8.SizeF = new System.Drawing.SizeF(328F, 31F);
            this.winControlContainer8.WinControl = this.picBarCodeCPN;
            // 
            // picBarCodeCPN
            // 
            this.picBarCodeCPN.Location = new System.Drawing.Point(0, 0);
            this.picBarCodeCPN.Name = "picBarCodeCPN";
            this.picBarCodeCPN.Size = new System.Drawing.Size(315, 30);
            this.picBarCodeCPN.TabIndex = 7;
            this.picBarCodeCPN.TabStop = false;
            // 
            // winControlContainer7
            // 
            this.winControlContainer7.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 948.3F);
            this.winControlContainer7.Name = "winControlContainer7";
            this.winControlContainer7.SizeF = new System.Drawing.SizeF(328F, 31F);
            this.winControlContainer7.WinControl = this.picBarCodeCP;
            // 
            // picBarCodeCP
            // 
            this.picBarCodeCP.Location = new System.Drawing.Point(0, 0);
            this.picBarCodeCP.Name = "picBarCodeCP";
            this.picBarCodeCP.Size = new System.Drawing.Size(315, 30);
            this.picBarCodeCP.TabIndex = 6;
            this.picBarCodeCP.TabStop = false;
            // 
            // winControlContainer6
            // 
            this.winControlContainer6.LocationFloat = new DevExpress.Utils.PointFloat(536.4583F, 556.3F);
            this.winControlContainer6.Name = "winControlContainer6";
            this.winControlContainer6.SizeF = new System.Drawing.SizeF(156F, 156F);
            this.winControlContainer6.WinControl = this.picQrCodeCPN;
            // 
            // picQrCodeCPN
            // 
            this.picQrCodeCPN.Location = new System.Drawing.Point(0, 0);
            this.picQrCodeCPN.Name = "picQrCodeCPN";
            this.picQrCodeCPN.Size = new System.Drawing.Size(150, 150);
            this.picQrCodeCPN.TabIndex = 5;
            this.picQrCodeCPN.TabStop = false;
            // 
            // winControlContainer5
            // 
            this.winControlContainer5.LocationFloat = new DevExpress.Utils.PointFloat(125.5117F, 556.3F);
            this.winControlContainer5.Name = "winControlContainer5";
            this.winControlContainer5.SizeF = new System.Drawing.SizeF(156F, 156F);
            this.winControlContainer5.WinControl = this.picQrCodeCP;
            // 
            // picQrCodeCP
            // 
            this.picQrCodeCP.Location = new System.Drawing.Point(0, 0);
            this.picQrCodeCP.Name = "picQrCodeCP";
            this.picQrCodeCP.Size = new System.Drawing.Size(150, 150);
            this.picQrCodeCP.TabIndex = 4;
            this.picQrCodeCP.TabStop = false;
            // 
            // xrLine20
            // 
            this.xrLine20.LineWidth = 2;
            this.xrLine20.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 933.2999F);
            this.xrLine20.Name = "xrLine20";
            this.xrLine20.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoTKXKCPN
            // 
            this.lblSoTKXKCPN.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTKXKCPN.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 910.2999F);
            this.lblSoTKXKCPN.Name = "lblSoTKXKCPN";
            this.lblSoTKXKCPN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTKXKCPN.SizeF = new System.Drawing.SizeF(328F, 23F);
            this.lblSoTKXKCPN.StylePriority.UseFont = false;
            this.lblSoTKXKCPN.StylePriority.UseTextAlignment = false;
            this.lblSoTKXKCPN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine19
            // 
            this.xrLine19.LineWidth = 2;
            this.xrLine19.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 872.3F);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 887.2999F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(162.5F, 23F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Số tờ khai xuất khẩu";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDoanhNghiepCPN
            // 
            this.lblTenDoanhNghiepCPN.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDoanhNghiepCPN.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 849.2999F);
            this.lblTenDoanhNghiepCPN.Multiline = true;
            this.lblTenDoanhNghiepCPN.Name = "lblTenDoanhNghiepCPN";
            this.lblTenDoanhNghiepCPN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDoanhNghiepCPN.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblTenDoanhNghiepCPN.StylePriority.UseFont = false;
            this.lblTenDoanhNghiepCPN.StylePriority.UseTextAlignment = false;
            this.lblTenDoanhNghiepCPN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 826.3F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(151.0417F, 22.99997F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Tên doanh nghiệp";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine18
            // 
            this.xrLine18.LineWidth = 2;
            this.xrLine18.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 712.2999F);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblMaDoanhNghiepCPN
            // 
            this.lblMaDoanhNghiepCPN.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDoanhNghiepCPN.LocationFloat = new DevExpress.Utils.PointFloat(536.3384F, 788.2999F);
            this.lblMaDoanhNghiepCPN.Name = "lblMaDoanhNghiepCPN";
            this.lblMaDoanhNghiepCPN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiepCPN.SizeF = new System.Drawing.SizeF(239.5783F, 23F);
            this.lblMaDoanhNghiepCPN.StylePriority.UseFont = false;
            this.lblMaDoanhNghiepCPN.StylePriority.UseTextAlignment = false;
            this.lblMaDoanhNghiepCPN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 788.2999F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(88.54172F, 23F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Mã số thuế ";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine17
            // 
            this.xrLine17.LineWidth = 2;
            this.xrLine17.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 773.3F);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoDinhDanhCPN
            // 
            this.lblSoDinhDanhCPN.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDinhDanhCPN.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 750.3F);
            this.lblSoDinhDanhCPN.Name = "lblSoDinhDanhCPN";
            this.lblSoDinhDanhCPN.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhDanhCPN.SizeF = new System.Drawing.SizeF(328.1251F, 23F);
            this.lblSoDinhDanhCPN.StylePriority.UseFont = false;
            this.lblSoDinhDanhCPN.StylePriority.UseTextAlignment = false;
            this.lblSoDinhDanhCPN.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 727.2999F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(151.0417F, 23F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Số quản lý hàng hóa";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine16
            // 
            this.xrLine16.LineWidth = 2;
            this.xrLine16.LocationFloat = new DevExpress.Utils.PointFloat(447.7967F, 811.3F);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLine15
            // 
            this.xrLine15.LineWidth = 2;
            this.xrLine15.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 933.2999F);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoTKXKCP
            // 
            this.lblSoTKXKCP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTKXKCP.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 910.3F);
            this.lblSoTKXKCP.Name = "lblSoTKXKCP";
            this.lblSoTKXKCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTKXKCP.SizeF = new System.Drawing.SizeF(328.125F, 23F);
            this.lblSoTKXKCP.StylePriority.UseFont = false;
            this.lblSoTKXKCP.StylePriority.UseTextAlignment = false;
            this.lblSoTKXKCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine14
            // 
            this.xrLine14.LineWidth = 2;
            this.xrLine14.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 872.3F);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 887.3F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(162.5F, 23F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Số tờ khai xuất khẩu";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDoanhNghiepCP
            // 
            this.lblTenDoanhNghiepCP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDoanhNghiepCP.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 849.2999F);
            this.lblTenDoanhNghiepCP.Multiline = true;
            this.lblTenDoanhNghiepCP.Name = "lblTenDoanhNghiepCP";
            this.lblTenDoanhNghiepCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDoanhNghiepCP.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblTenDoanhNghiepCP.StylePriority.UseFont = false;
            this.lblTenDoanhNghiepCP.StylePriority.UseTextAlignment = false;
            this.lblTenDoanhNghiepCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 826.3F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(151.0417F, 22.99997F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Tên doanh nghiệp";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine13
            // 
            this.xrLine13.LineWidth = 2;
            this.xrLine13.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 712.3F);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblMaDoanhNghiepCP
            // 
            this.lblMaDoanhNghiepCP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDoanhNghiepCP.LocationFloat = new DevExpress.Utils.PointFloat(135.9283F, 788.2999F);
            this.lblMaDoanhNghiepCP.Name = "lblMaDoanhNghiepCP";
            this.lblMaDoanhNghiepCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiepCP.SizeF = new System.Drawing.SizeF(239.5783F, 23F);
            this.lblMaDoanhNghiepCP.StylePriority.UseFont = false;
            this.lblMaDoanhNghiepCP.StylePriority.UseTextAlignment = false;
            this.lblMaDoanhNghiepCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 788.2999F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(88.54172F, 23F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Mã số thuế ";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine12
            // 
            this.xrLine12.LineWidth = 2;
            this.xrLine12.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 773.3F);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoDinhDanhCP
            // 
            this.lblSoDinhDanhCP.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDinhDanhCP.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 750.3F);
            this.lblSoDinhDanhCP.Name = "lblSoDinhDanhCP";
            this.lblSoDinhDanhCP.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhDanhCP.SizeF = new System.Drawing.SizeF(328.125F, 23F);
            this.lblSoDinhDanhCP.StylePriority.UseFont = false;
            this.lblSoDinhDanhCP.StylePriority.UseTextAlignment = false;
            this.lblSoDinhDanhCP.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 727.2999F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(151.0417F, 23F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Số quản lý hàng hóa";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine11
            // 
            this.xrLine11.LineWidth = 2;
            this.xrLine11.LocationFloat = new DevExpress.Utils.PointFloat(47.38668F, 811.3F);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // winControlContainer4
            // 
            this.winControlContainer4.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 392.3335F);
            this.winControlContainer4.Name = "winControlContainer4";
            this.winControlContainer4.SizeF = new System.Drawing.SizeF(328F, 31F);
            this.winControlContainer4.WinControl = this.picBarCodeCoppy;
            // 
            // picBarCodeCoppy
            // 
            this.picBarCodeCoppy.Location = new System.Drawing.Point(0, 0);
            this.picBarCodeCoppy.Name = "picBarCodeCoppy";
            this.picBarCodeCoppy.Size = new System.Drawing.Size(315, 30);
            this.picBarCodeCoppy.TabIndex = 3;
            this.picBarCodeCoppy.TabStop = false;
            // 
            // winControlContainer3
            // 
            this.winControlContainer3.LocationFloat = new DevExpress.Utils.PointFloat(536.3384F, 0.3334045F);
            this.winControlContainer3.Name = "winControlContainer3";
            this.winControlContainer3.SizeF = new System.Drawing.SizeF(156F, 156F);
            this.winControlContainer3.WinControl = this.picQrCodeCoppy;
            // 
            // picQrCodeCoppy
            // 
            this.picQrCodeCoppy.Location = new System.Drawing.Point(0, 0);
            this.picQrCodeCoppy.Name = "picQrCodeCoppy";
            this.picQrCodeCoppy.Size = new System.Drawing.Size(150, 150);
            this.picQrCodeCoppy.TabIndex = 2;
            this.picQrCodeCoppy.TabStop = false;
            // 
            // xrLine6
            // 
            this.xrLine6.LineWidth = 2;
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 255.3335F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLine5
            // 
            this.xrLine5.LineWidth = 2;
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 377.3334F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoTKXK
            // 
            this.lblSoTKXK.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTKXK.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 354.3335F);
            this.lblSoTKXK.Name = "lblSoTKXK";
            this.lblSoTKXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTKXK.SizeF = new System.Drawing.SizeF(328F, 23F);
            this.lblSoTKXK.StylePriority.UseFont = false;
            this.lblSoTKXK.StylePriority.UseTextAlignment = false;
            this.lblSoTKXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.LineWidth = 2;
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 316.3335F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 331.3334F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(162.5F, 23F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Số tờ khai xuất khẩu";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDoanhNghiep.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 293.3334F);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblTenDoanhNghiep.StylePriority.UseFont = false;
            this.lblTenDoanhNghiep.StylePriority.UseTextAlignment = false;
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 270.3335F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(151.0417F, 22.99997F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Tên doanh nghiệp";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LineWidth = 2;
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 156.3334F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDoanhNghiep.LocationFloat = new DevExpress.Utils.PointFloat(136.4583F, 232.3334F);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.SizeF = new System.Drawing.SizeF(239.5783F, 23F);
            this.lblMaDoanhNghiep.StylePriority.UseFont = false;
            this.lblMaDoanhNghiep.StylePriority.UseTextAlignment = false;
            this.lblMaDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 232.3334F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(88.54172F, 23F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Mã số thuế ";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine2
            // 
            this.xrLine2.LineWidth = 2;
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 217.3335F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoDinhDanh
            // 
            this.lblSoDinhDanh.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDinhDanh.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 194.3335F);
            this.lblSoDinhDanh.Name = "lblSoDinhDanh";
            this.lblSoDinhDanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhDanh.SizeF = new System.Drawing.SizeF(328.125F, 22.99998F);
            this.lblSoDinhDanh.StylePriority.UseFont = false;
            this.lblSoDinhDanh.StylePriority.UseTextAlignment = false;
            this.lblSoDinhDanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 171.3334F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(151.0417F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Số quản lý hàng hóa";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(125.5117F, 0.3334045F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(156F, 156F);
            this.winControlContainer1.WinControl = this.picQrCode;
            // 
            // picQrCode
            // 
            this.picQrCode.Location = new System.Drawing.Point(0, 0);
            this.picQrCode.Name = "picQrCode";
            this.picQrCode.Size = new System.Drawing.Size(150, 150);
            this.picQrCode.TabIndex = 0;
            this.picQrCode.TabStop = false;
            // 
            // xrLine3
            // 
            this.xrLine3.LineWidth = 2;
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 255.3335F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // winControlContainer2
            // 
            this.winControlContainer2.LocationFloat = new DevExpress.Utils.PointFloat(47.91667F, 392.3335F);
            this.winControlContainer2.Name = "winControlContainer2";
            this.winControlContainer2.SizeF = new System.Drawing.SizeF(328.125F, 31F);
            this.winControlContainer2.WinControl = this.picBarCode;
            // 
            // picBarCode
            // 
            this.picBarCode.Location = new System.Drawing.Point(0, 0);
            this.picBarCode.Name = "picBarCode";
            this.picBarCode.Size = new System.Drawing.Size(315, 30);
            this.picBarCode.TabIndex = 1;
            this.picBarCode.TabStop = false;
            // 
            // xrLine7
            // 
            this.xrLine7.LineWidth = 2;
            this.xrLine7.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 377.3334F);
            this.xrLine7.Name = "xrLine7";
            this.xrLine7.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // lblSoTKXKCoppy
            // 
            this.lblSoTKXKCoppy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTKXKCoppy.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 354.3335F);
            this.lblSoTKXKCoppy.Name = "lblSoTKXKCoppy";
            this.lblSoTKXKCoppy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTKXKCoppy.SizeF = new System.Drawing.SizeF(328.125F, 23F);
            this.lblSoTKXKCoppy.StylePriority.UseFont = false;
            this.lblSoTKXKCoppy.StylePriority.UseTextAlignment = false;
            this.lblSoTKXKCoppy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine8
            // 
            this.xrLine8.LineWidth = 2;
            this.xrLine8.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 316.3335F);
            this.xrLine8.Name = "xrLine8";
            this.xrLine8.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 331.3334F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(162.5F, 23F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số tờ khai xuất khẩu";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 171.3334F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(151.0417F, 23F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Số quản lý hàng hóa";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 232.3334F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(88.54172F, 23F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "Mã số thuế ";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDoanhNghiepCoppy
            // 
            this.lblTenDoanhNghiepCoppy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDoanhNghiepCoppy.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 293.3334F);
            this.lblTenDoanhNghiepCoppy.Multiline = true;
            this.lblTenDoanhNghiepCoppy.Name = "lblTenDoanhNghiepCoppy";
            this.lblTenDoanhNghiepCoppy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDoanhNghiepCoppy.SizeF = new System.Drawing.SizeF(328.125F, 23.00003F);
            this.lblTenDoanhNghiepCoppy.StylePriority.UseFont = false;
            this.lblTenDoanhNghiepCoppy.StylePriority.UseTextAlignment = false;
            this.lblTenDoanhNghiepCoppy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMaDoanhNghiepCoppy
            // 
            this.lblMaDoanhNghiepCoppy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDoanhNghiepCoppy.LocationFloat = new DevExpress.Utils.PointFloat(536.4583F, 232.3334F);
            this.lblMaDoanhNghiepCoppy.Name = "lblMaDoanhNghiepCoppy";
            this.lblMaDoanhNghiepCoppy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDoanhNghiepCoppy.SizeF = new System.Drawing.SizeF(239.5783F, 23F);
            this.lblMaDoanhNghiepCoppy.StylePriority.UseFont = false;
            this.lblMaDoanhNghiepCoppy.StylePriority.UseTextAlignment = false;
            this.lblMaDoanhNghiepCoppy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoDinhDanhCoppy
            // 
            this.lblSoDinhDanhCoppy.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDinhDanhCoppy.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 194.3335F);
            this.lblSoDinhDanhCoppy.Name = "lblSoDinhDanhCoppy";
            this.lblSoDinhDanhCoppy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDinhDanhCoppy.SizeF = new System.Drawing.SizeF(328.0001F, 22.99998F);
            this.lblSoDinhDanhCoppy.StylePriority.UseFont = false;
            this.lblSoDinhDanhCoppy.StylePriority.UseTextAlignment = false;
            this.lblSoDinhDanhCoppy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 270.3335F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(151.0417F, 22.99997F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Tên doanh nghiệp";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLine9
            // 
            this.xrLine9.LineWidth = 2;
            this.xrLine9.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 156.3334F);
            this.xrLine9.Name = "xrLine9";
            this.xrLine9.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // xrLine10
            // 
            this.xrLine10.LineWidth = 2;
            this.xrLine10.LocationFloat = new DevExpress.Utils.PointFloat(447.9167F, 217.3335F);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.SizeF = new System.Drawing.SizeF(328.12F, 15F);
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 11.08333F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportViewDinhDanhHangHoa
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(14, 12, 11, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.picBarCodeCPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCodeCP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCodeCPN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCodeCP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCodeCoppy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCodeCoppy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picQrCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBarCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.PictureBox picQrCode;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhDanh;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lblSoTKXK;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer2;
        private System.Windows.Forms.PictureBox picBarCode;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLine xrLine9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhDanhCoppy;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiepCoppy;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiepCoppy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLine xrLine8;
        private DevExpress.XtraReports.UI.XRLabel lblSoTKXKCoppy;
        private DevExpress.XtraReports.UI.XRLine xrLine7;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLine xrLine20;
        private DevExpress.XtraReports.UI.XRLabel lblSoTKXKCPN;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiepCPN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiepCPN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhDanhCPN;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLabel lblSoTKXKCP;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiepCP;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiepCP;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLabel lblSoDinhDanhCP;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer4;
        private System.Windows.Forms.PictureBox picBarCodeCoppy;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer3;
        private System.Windows.Forms.PictureBox picQrCodeCoppy;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer6;
        private System.Windows.Forms.PictureBox picQrCodeCPN;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer5;
        private System.Windows.Forms.PictureBox picQrCodeCP;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer8;
        private System.Windows.Forms.PictureBox picBarCodeCPN;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer7;
        private System.Windows.Forms.PictureBox picBarCodeCP;
        private DevExpress.XtraReports.UI.XRLabel lblSoThamChieuNoiBo;
        private DevExpress.XtraReports.UI.XRLabel lblSoThamChieuNoiBoCP;
        private DevExpress.XtraReports.UI.XRLabel lblSoThamChieuNoiBoCoppy;
        private DevExpress.XtraReports.UI.XRLabel lblSoThamChieuNoiBoCPN;
        private DevExpress.XtraReports.UI.XRLine xrLine26;
        private DevExpress.XtraReports.UI.XRLine xrLine25;
        private DevExpress.XtraReports.UI.XRLine xrLine24;
        private DevExpress.XtraReports.UI.XRLine xrLine23;
    }
}
