﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamLenhSanXuatRegistedForm : Company.Interface.BaseForm
    {
        public KDT_LenhSanXuat lenhsanxuat = new KDT_LenhSanXuat();
        public KDT_LenhSanXuat_SP lenhsanxuatSP = new KDT_LenhSanXuat_SP();
        public List<KDT_LenhSanXuat_SP> SanPhamSelectCollection = new List<KDT_LenhSanXuat_SP>();
        public string whereCondition = "";
        public SanPhamLenhSanXuatRegistedForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string whereConditionSearch = "";
                whereConditionSearch = whereCondition;
                string where = "";
                if (!String.IsNullOrEmpty(txtMaSP.Text))
                {
                    where += " AND MaSanPham LIKE '%" + txtMaSP.Text.ToString() + "%'";
                }
                if (!String.IsNullOrEmpty(txtTenSP.Text))
                {
                    where += " AND TenSanPham LIKE N'%" + txtTenSP.Text.ToString() + "%'";
                }
                if (!String.IsNullOrEmpty(where))
                {
                    whereConditionSearch += where;
                }
                dgList.Refresh();
                dgList.DataSource = KDT_LenhSanXuat_SP.SelectCollectionDynamic(whereConditionSearch, "");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(lenhsanxuat.ID);
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    KDT_LenhSanXuat_SP sanPhamSelect = (KDT_LenhSanXuat_SP)item.DataRow;
                    SanPhamSelectCollection.Add(sanPhamSelect);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void SanPhamLenhSanXuatRegistedForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                lenhsanxuatSP = (KDT_LenhSanXuat_SP)e.Row.DataRow;
                SanPhamSelectCollection.Add(lenhsanxuatSP);
                this.Close();
            }

        }
    }
}
