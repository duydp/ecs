﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC ;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{
    
    public partial class ReportViewGCCTA4Form : BaseForm
    {
        public ToKhaiGCCTA4 ToKhaiChinhReport = new ToKhaiGCCTA4();
        //public ToKhaiGCCTA4 ToKhaiChinhReport = new ToKhaiGCCTA4();
       // public PhuLucToKhaiCTTQ PhuLucReport = new PhuLucToKhaiCTTQ();
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();

        public XRControl Cell = new XRControl();
        public ReportViewGCCTA4Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {

             cboToKhai.SelectedIndex = 0;
            //if (this.TKMD.HMDCollection.Count > 3)
            //{
            //    int count = (this.TKMD.HMDCollection.Count - 1) / 9 + 1;
            //    for (int i = 0; i < count; i++)
            //        this.AddItemComboBox();
            //}
             this.ToKhaiChinhReport.TKCT = this.TKCT;
             this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
             printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
            this.ToKhaiChinhReport.CreateDocument();
            
            
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.report = this;
                this.ToKhaiChinhReport.CreateDocument();

            }
                
            //else
            //{
            //    HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
            //    int begin = (cboToKhai.SelectedIndex - 1) * 9;
            //    int end = cboToKhai.SelectedIndex * 9;
            //    if (end > this.TKCT.HCTCollection.Count) end = this.TKCT.HCTCollection.Count;
            //    for (int i = begin; i < end; i++)
            //        HMDReportCollection.Add(this.TKCT.HCTCollection[i]);
            //    this.PhuLucReport = new PhuLucToKhaiCTTQ();
            //    this.PhuLucReport.report = this;
            //    this.PhuLucReport.MaLoaiHinh = this.TKMD.MaLoaiHinh;
            //    this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
            //    if(this.TKCT.NgayDangKy!= new DateTime(1900,1,1))
            //        this.PhuLucReport.NgayDangKy = this.TKCT.NgayDangKy;
            //    this.PhuLucReport.HMDCollection = HMDReportCollection;
            //    this.PhuLucReport.BindReport();//chkInMaHang.Checked
            //    printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
            //    this.PhuLucReport.CreateDocument();

            //}
                 
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {

                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.CreateDocument();
            }
            /*
            else
            {
                
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.PhuLucReport.CreateDocument();
                 
            }
             */
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }


        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
                /*
            else
            {
                this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
                 */
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
               // this.ToKhaiChinhReportNhap.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
                /*
            else
            {
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.setVisibleImage(true);
                this.PhuLucReport.CreateDocument();
            }
                 */ 
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
                /*
            else
            {
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
                 */
        }

        private void chkHinhNen_CheckedChanged(object sender, EventArgs e)
        {

        }

 

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.ToKhaiChinhReport.BindReport(chkInMaHang.Checked);//chkInMaHang.Checked
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
                /*
            else
            {
                //this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
                this.PhuLucReport.BindReport(chkInMaHang.Checked);
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
                 */ 
        }
    }
}