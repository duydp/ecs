using System;
using System.Windows.Forms;
using Company.Interface;

namespace Company.Controls
{
    public partial class KDTMessageBoxControl : BaseForm
    {
        public string ReturnValue = "Cancel";
        public bool ShowYesNoButton
        {
            set
            {
                this.btnNo.Visible = this.btnYes.Visible = value;
                this.btnCancel.Visible = !value;
                if (!value) this.CancelButton = btnCancel;
                else this.CancelButton = btnNo;
            }
        }

        public string MessageString
        {
            set { this.txtMessage.Text = value; }
        }
        public string HQMessageString
        {
            set { this.rtxtHQMess.Text = value; }
        }

        public void dispEnglish()
        {
            this.Text = "Announcement";
            this.btnYes.Text = "&Yes";
            this.btnNo.Text = "&No";
            this.btnCancel.Text = "&Close";
        }

        public KDTMessageBoxControl()
        {
            InitializeComponent();
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "Yes";
            this.Close();
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.ReturnValue = "No";
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MessageBoxControl_Load(object sender, EventArgs e)
        {
            if (Company.Interface.GlobalSettings.NGON_NGU == "1") this.dispEnglish();
        }
    }
}