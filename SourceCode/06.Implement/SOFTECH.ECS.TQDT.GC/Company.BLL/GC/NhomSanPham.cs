﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.GC
{
    public partial class NhomSanPham
    {
        //public static DataSet WS_GetDanhSachDaDangKy(string maHaiQuan, string maDoanhNghiep)
        //{
        //    GCService service = new GCService();
        //    return service.LoaiSanPham_GetDanhSach(maHaiQuan, maDoanhNghiep);
        //}

        //public static bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep,SqlTransaction transaction)
        //{
        //    // Lấy danh sách từ WEB SERVICE.
        //    DataSet ds = WS_GetDanhSachDaDangKy(maHaiQuan, maDoanhNghiep);

        //    // Cập nhật vào CSDL.
        //    NhomSanPhamCollection collection = new NhomSanPhamCollection();
        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        NhomSanPham nhomsp = new NhomSanPham();
        //        nhomsp.SoHopDong = row["SoHopDong"].ToString();
        //        nhomsp.MaHaiQuan = row["MaHaiQuan"].ToString();
        //        nhomsp.MaDoanhNghiep = row["MaDoanhNghiep"].ToString();
        //        nhomsp.NgayKy = Convert.ToDateTime(row["NgayKy"]);
        //        nhomsp.Ma = row["Ma"].ToString().Substring(1);
        //        nhomsp.Ten = Company.GC.BLL.DuLieuChuan.NhomSanPham.getTenSanPham(nhomsp.Ma);                            
        //        nhomsp.SoLuongDangKy = Convert.ToDecimal(row["SoLuong"]);
        //        nhomsp.TriGia= Convert.ToDecimal(row["Gia"]);
        //        nhomsp.InsertUpdateTransaction(transaction);
        //    }
        //    return true;
        //}

        public int GetSoSanPham(long IDHopDong)
        {
            string sql = " Select SoLuong from t_GC_NhomSanPham where HopDong_ID = @HopDong_ID  ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            IDataReader reader = db.ExecuteReader(dbCommand);
            int  soLuong = 0;
            try
            {

                if (reader.Read())
                {
                    soLuong = reader.GetInt32(0);
                }
            }
            catch { }
            return soLuong;
        }
        public DataSet GetTenNhomSanPham(long IDHopDong)
        {
            string sql = " Select TenSanPham from t_GC_NhomSanPham where HopDong_ID = @HopDong_ID  ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            return db.ExecuteDataSet(dbCommand);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //string[] tenNhomSanPham = "";
            //try
            //{

            //    if (reader.Read())
            //    {
            //        tenNhomSanPham  = reader.GetString(0);
            //    }
            //}
            //catch { }
            //return tenNhomSanPham;
        }
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_GC_NhomSanPham_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) this._GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) this._TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression, string databaseName)
        {
            string spName = "p_GC_NhomSanPham_SelectDynamic";
            
            //Updated by HUNGTQ, 06/05/2011.
            SetDabaseMoi(databaseName);

            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_NhomSanPham_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }
        
        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_NhomSanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

    }
}
