﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace Helper.Controls
{
    public partial class UpdateDatabaseSQLForm : Form
    {
        private string serverName, dbName, dbUser, dbPassWord;
        private static UpdateDatabaseSQLForm _instance = null;
        private string dataCurrenVersion = "0";
        private string dataNewVersion = "0";

        private bool _isHasNewDataVersion;

        public bool IsHasNewDataVersion
        {
            get
            {
                try
                {
                    //Lay thong tin cau hinh ket noi du lieu
                    GetConnectionConfig();

                    dataCurrenVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseVersion_ECS(serverName, dbName, dbUser, dbPassWord);
                    if (dataCurrenVersion == null || dataCurrenVersion == "") dataCurrenVersion = "0";

                    dataNewVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseNewVersion(serverName, dbName, dbUser, dbPassWord);

                    //if (string.Compare(System.Convert.ToDecimal(dataNewVersion).ToString("##0.0#"), System.Convert.ToDecimal(dataCurrenVersion).ToString("##0.0#")) > 0)
                    if (System.Convert.ToDecimal(dataNewVersion) > System.Convert.ToDecimal(dataCurrenVersion))
                    {
                        Logger.LocalLogger.Instance().WriteMessage("Kiểm tra phiên bản dữ liệu: cũ = " + dataCurrenVersion + "; mới = " + dataNewVersion, new Exception(""));
                        _isHasNewDataVersion = true;
                    }
                    else
                        _isHasNewDataVersion = false;
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                return _isHasNewDataVersion;
            }
            set { _isHasNewDataVersion = value; }
        }

        public UpdateDatabaseSQLForm()
        {
            InitializeComponent();

            InitializeBackgoundWorker();
        }

        private void GetConnectionConfig()
        {
            try
            {
                /*Lay cấu hình tu file config XML*/
                XmlDocument doc = new XmlDocument();
                string path = Company.KDT.SHARE.Components.Globals.GetPathProgram() + "\\ConfigDoanhNghiep";
                //Hungtq update 28/01/2011.
                string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

                doc.Load(fileName);

                //Set thong tin Server
                XmlNode nodeServerName = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "SERVER");
                serverName = nodeServerName.InnerText;
                //Set thong tin Database
                XmlNode nodeDatabase = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database");
                dbName = nodeDatabase.InnerText;
                //Set thong tin UserName
                XmlNode nodeUserName = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName");
                dbUser = nodeUserName.InnerText;
                //Set thong tin Password
                XmlNode nodePassword = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password");
                dbPassWord = nodePassword.InnerText;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                /*Lay cấu hình tu file config exe*/
                serverName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ServerName");
                dbName = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DATABASE_NAME");
                dbUser = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("user");
                dbPassWord = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("pass");
            }
        }

        private void UpdateDatabaseSQLForm_Load(object sender, EventArgs e)
        {
            try
            {
                highestPercentageReached = 0;
                SetProgressBar(highestPercentageReached);

                //Lay thong tin cau hinh ket noi du lieu
                GetConnectionConfig();

                dataCurrenVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseVersion_ECS(serverName, dbName, dbUser, dbPassWord);
                if (dataCurrenVersion == null || dataCurrenVersion == "") dataCurrenVersion = "0";

                dataNewVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseNewVersion(serverName, dbName, dbUser, dbPassWord);
                //Logger.LocalLogger.Instance().WriteMessage(dataNewVersion, new Exception("Ghi log"));

                lblCurrentVersion.Text = dataCurrenVersion;
                lblNewVersion.Text = dataNewVersion;

                //btnYes.Enabled = (string.Compare(System.Convert.ToDecimal(dataNewVersion).ToString("##0.0#"), System.Convert.ToDecimal(dataCurrenVersion).ToString("##0.0#")) > 0);
                btnYes.Enabled = System.Convert.ToDecimal(dataNewVersion) > System.Convert.ToDecimal(dataCurrenVersion);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public static bool CheckHasNewDataVersion()
        {


            return false;
        }

        public static UpdateDatabaseSQLForm Instance(bool isTopMost)
        {
            if (_instance == null)
            {
                _instance = new UpdateDatabaseSQLForm();

                _instance.TopMost = isTopMost;
            }

            return _instance;
        }

        private void btnNo_Click(object sender, EventArgs e)
        {
            this.Close();

            DialogResult = DialogResult.No;
        }

        private void btnYes_Click(object sender, EventArgs e)
        {
            // Disable the DongY, TuChoi button until 
            // the asynchronous operation is done.
            this.btnYes.Enabled = false;
            this.btnNo.Enabled = false;

            // Reset the variable for percentage tracking.
            highestPercentageReached = 0;

            // Start the asynchronous operation.
            backgroundWorker1.RunWorkerAsync();
        }

        /// <summary>
        /// Kiem tra loi trong file log script sql
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>True: co loi; Flase: khong co loi</returns>
        private bool CheckErrorInScriptSQL(string filePath, out string errorReturn)
        {
            try
            {
                if (!System.IO.File.Exists(filePath))
                {
                    errorReturn = "Không tồn tại file: " + filePath;
                    return true;
                }

                String line = "";
                using (StreamReader sr = new StreamReader(filePath))
                {
                    line = sr.ReadToEnd();
                }
                Logger.LocalLogger.Instance().WriteMessage("Log nâng cấp dữ liệu: " + filePath + "\r\n" + line, new Exception(""));
                errorReturn = line;

                return (line.Contains("Msg") || line.Contains("timeout"));
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); errorReturn = ex.Message; return true; }
        }

        #region BackgroundWorker

        private int highestPercentageReached = 0;

        delegate void SetProgressBarCallback(int percent);
        private void SetProgressBar(int percent)
        {
            if (progressBar.InvokeRequired)
            {
                SetProgressBarCallback d = new SetProgressBarCallback(SetProgressBar);
                Invoke(d, new object[] { percent });
            }
            else
                progressBar.Value = percent;
        }

        delegate void SetRichTextCallback(string msg);
        private void SetRichText(string msg)
        {
            if (richTextBoxLog.InvokeRequired)
            {
                SetRichTextCallback d = new SetRichTextCallback(SetRichText);
                Invoke(d, new object[] { msg });
            }
            else
                richTextBoxLog.AppendText(msg);
        }

        delegate void ReSetRichTextCallback();
        private void ReSetRichText()
        {
            if (richTextBoxLog.InvokeRequired)
            {
                ReSetRichTextCallback d = new ReSetRichTextCallback(ReSetRichText);
                Invoke(d, new object[] { "" });
            }
            else
                richTextBoxLog.Text = "";
        }

        delegate void SetCurrentVersionLabelCallback(string msg);
        private void SetCurrentVersionLabel(string msg)
        {
            if (lblCurrentVersion.InvokeRequired)
            {
                SetCurrentVersionLabelCallback d = new SetCurrentVersionLabelCallback(SetCurrentVersionLabel);
                Invoke(d, new object[] { msg });
            }
            else
                lblCurrentVersion.Text = msg;
        }

        // Set up the BackgroundWorker object by 
        // attaching event handlers. 
        private void InitializeBackgoundWorker()
        {
            backgroundWorker1.DoWork +=
                new DoWorkEventHandler(backgroundWorker1_DoWork);
            backgroundWorker1.RunWorkerCompleted +=
                new RunWorkerCompletedEventHandler(
            backgroundWorker1_RunWorkerCompleted);
            backgroundWorker1.ProgressChanged +=
                new ProgressChangedEventHandler(
            backgroundWorker1_ProgressChanged);
        }

        //private void startAsyncButton_Click(System.Object sender,
        //    System.EventArgs e)
        //{
        //    // Reset the text in the result label.
        //    resultLabel.Text = String.Empty;

        //    // Disable the Start button until 
        //    // the asynchronous operation is done.
        //    this.startAsyncButton.Enabled = false;

        //    // Enable the Cancel button while 
        //    // the asynchronous operation runs.
        //    this.cancelAsyncButton.Enabled = true;

        //    // Reset the variable for percentage tracking.
        //    highestPercentageReached = 0;

        //    // Start the asynchronous operation.
        //    backgroundWorker1.RunWorkerAsync();
        //}

        //private void cancelAsyncButton_Click(System.Object sender,
        //    System.EventArgs e)
        //{
        //    // Cancel the asynchronous operation.
        //    this.backgroundWorker1.CancelAsync();

        //    // Disable the Cancel button.
        //    cancelAsyncButton.Enabled = false;
        //}

        // This event handler is where the actual,
        // potentially time-consuming work is done.
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                // Get the BackgroundWorker that raised this event.
                BackgroundWorker worker = sender as BackgroundWorker;

                // Assign the result of the computation
                // to the Result property of the DoWorkEventArgs
                // object. This is will be available to the 
                // RunWorkerCompleted eventhandler.
                //e.Result = UpdateDatabase(worker, e);

                UpdateDatabase(worker, e);
            }
            catch (Exception ex) { Helper.Controls.MessageBoxControlH.ShowMessage(ex.Message, false, true); e.Cancel = true; }
        }

        // This event handler updates the progress bar.
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                SetProgressBar(e.ProgressPercentage);
            }
            catch (Exception ex) { Helper.Controls.MessageBoxControlH.ShowMessage(ex.Message, false, true); }
        }

        // This event handler deals with the results of the
        // background operation.
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                // First, handle the case where an exception was thrown.
                if (e.Error != null)
                {
                    SetRichText("---Có lỗi xảy ra trong quá trình thực hiện nâng cấp CSDL:---\r\n" + e.Error.Message + "\r\n");

                    Helper.Controls.MessageBoxControlH.ShowMessage(e.Error.Message, false, true);
                }
                else if (e.Cancelled)
                {
                    // Next, handle the case where the user canceled 
                    // the operation.
                    // Note that due to a race condition in 
                    // the DoWork event handler, the Cancelled
                    // flag may not have been set, even though
                    // CancelAsync was called.

                    //resultLabel.Text = "Canceled";

                    //btnYes.Enabled = (string.Compare(System.Convert.ToDecimal(dataNewVersion).ToString("##0.0#"), System.Convert.ToDecimal(dataCurrenVersion).ToString("##0.0#")) > 0);
                    btnYes.Enabled = System.Convert.ToDecimal(dataNewVersion) > System.Convert.ToDecimal(dataCurrenVersion);
                }
                else
                {
                    // Finally, handle the case where the operation 
                    // succeeded.

                    //resultLabel.Text = e.Result.ToString();

                    dataCurrenVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseVersion_ECS(serverName, dbName, dbUser, dbPassWord);
                    lblCurrentVersion.Text = dataCurrenVersion;

                    dataNewVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseNewVersion(serverName, dbName, dbUser, dbPassWord);
                    lblNewVersion.Text = dataNewVersion;

                    //btnYes.Enabled = (string.Compare(System.Convert.ToDecimal(dataNewVersion).ToString("##0.0#"), System.Convert.ToDecimal(dataCurrenVersion).ToString("##0.0#")) > 0);
                    btnYes.Enabled = System.Convert.ToDecimal(dataNewVersion) > System.Convert.ToDecimal(dataCurrenVersion);

                    SetRichText("\r\n\nĐã hoàn thành quá trình thực hiện cập nhật.");

                    SetProgressBar(100);
                }

                // Enable the TuChoi button.
                btnNo.Enabled = true;
            }
            catch (Exception ex) { Helper.Controls.MessageBoxControlH.ShowMessage(ex.StackTrace, false, true); }
        }

        // This is the method that does the actual work. For this
        // example, it computes a Fibonacci number and
        // reports progress as it does its work.
        private void UpdateDatabase(BackgroundWorker worker, DoWorkEventArgs e)
        {
            // Abort the operation if the user has canceled.
            // Note that a call to CancelAsync may have set 
            // CancellationPending to true just after the
            // last invocation of this method exits, so this 
            // code will not have the opportunity to set the 
            // DoWorkEventArgs.Cancel flag to true. This means
            // that RunWorkerCompletedEventArgs.Cancelled will
            // not be set to true in your RunWorkerCompleted
            // event handler. This is a race condition.
            highestPercentageReached = 0;
            SetProgressBar(highestPercentageReached);


            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                SetRichText("\r\n\n======================================================================");
                SetRichText("\r\n" + DateTime.Now.ToString("dd/MM/yy hh:mm:ss tt"));
                SetRichText("\r\n======================================================================");

                bool isBackupSuccess = false;

                //Thu hien cap nhat script
                string[] sqlFiles = Company.KDT.SHARE.Components.SQL.GetSQLUpdateFiles();
                int total = sqlFiles.Length;
                string error = "";
                string logError = "";

                if (!chkSkipBackup.Checked)
                {
                    SetRichText("\r\n\n--> I> Đang thực hiện sao lưu dữ liệu trước khi thực hiện nâng cấp dữ liệu...");
                    try
                    {
                        // Report progress as a percentage of the total task.
                        int percentComplete = (int)((1) * 100 / (total + 1));
                        if (percentComplete > highestPercentageReached)
                        {
                            highestPercentageReached = percentComplete;
                            worker.ReportProgress(percentComplete);
                        }

                        isBackupSuccess = Company.KDT.SHARE.Components.SQL.BackupDatabase_SQLScript(serverName, dbName, dbUser, dbPassWord);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.ToLower().Contains("Timeout"))
                        {
                            SetRichText("Lỗi vượt quá thời gian kết nối dữ liệu\r\n" + ex.Message);
                        }
                        else
                            SetRichText("\r\n" + ex.Message);
                    }

                    if (isBackupSuccess)
                        SetRichText("\r\nThực hiện sao lưu dữ liệu thành công.");
                    else
                        SetRichText("\r\nThực hiện sao lưu dữ liệu không thành công.");
                }

                if (!chkSkipBackup.Checked)
                    SetRichText("\r\n\n--> II> Đang thực hiện nâng cấp dữ liệu...");
                else
                    SetRichText("\r\n\n--> I> Đang thực hiện nâng cấp dữ liệu...");
                int countDem = 0;

                if (!Directory.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate"))
                    Directory.CreateDirectory(System.AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate");

                DataTable dtVersion = new DataTable();
                dtVersion.Columns.Add("FilePath", typeof(string));
                dtVersion.Columns.Add("FileName", typeof(string));
                dtVersion.Columns.Add("Version", typeof(decimal));

                if (sqlFiles.Length > 0)
                {
                    foreach (string strV in sqlFiles)
                    {
                        System.IO.FileInfo fiScript = new System.IO.FileInfo(strV);

                        DataRow drV = dtVersion.NewRow();
                        drV["FilePath"] = strV;
                        drV["FileName"] = fiScript.Name;
                        drV["Version"] = Convert.ToDecimal(Company.KDT.SHARE.Components.SQL.GetSQLFileVersion(strV));
                        dtVersion.Rows.Add(drV);
                    }

                    DataView dvV = dtVersion.DefaultView;
                    dvV.Sort = "Version asc";

                    int i = 0;
                    foreach (DataRowView item in dvV)
                    {
                        i += 1;
                        string inputFileVersion = item["Version"].ToString();
                        string inputFile = item["FilePath"].ToString();

                        //Lay verion hien tai moi nhat (Neu co cap nhat), la version da thuc hien thanh cong truoc khi cap nhat version tiep theo.
                        dataCurrenVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseVersion_ECS(serverName, dbName, dbUser, dbPassWord);
                        SetCurrentVersionLabel(dataCurrenVersion);

                        //if (string.Compare(System.Convert.ToDecimal(inputFileVersion).ToString("##0.0#"), System.Convert.ToDecimal(dataCurrenVersion).ToString("##0.0#")) > 0)
                        if (System.Convert.ToDecimal(inputFileVersion) > System.Convert.ToDecimal(dataCurrenVersion))
                        {
                            //Ghi Tieu de file thuc hien cap nhat
                            error = string.Format("{0}. {1}", ++countDem, item["FileName"].ToString());
                            SetRichText("\r\n" + error);

                            bool execIsSuccess = Company.KDT.SHARE.Components.SQL.Execute_SQLScript(serverName, dbName, dbUser, dbPassWord, inputFile, out error);

                            //Check error in Log Script SQL
                            string logFileScript = AppDomain.CurrentDomain.BaseDirectory + "Data\\Update\\LogUpdate\\" + item["FileName"].ToString().Replace("sql", "log");

                            execIsSuccess = !CheckErrorInScriptSQL(logFileScript, out logError);

                            if (execIsSuccess)
                            {
                                if (error.Length != 0)
                                {
                                    SetRichText("\r\n-> Có lỗi xảy ra: " + error + "\r\n");
                                    e.Result = error;
                                    e.Cancel = true;
                                    break;
                                }
                                else
                                {
                                    //Ghi ket qua file thuc hien cap nhat
                                    SetRichText("\r\n-> Thực hiện thành công.");
                                    //Lay version da thuc hien thanh cong truoc khi cap nhat version tiep theo.
                                    dataCurrenVersion = Company.KDT.SHARE.Components.SQL.GetDatabaseVersion_ECS(serverName, dbName, dbUser, dbPassWord);
                                    SetCurrentVersionLabel(dataCurrenVersion);
                                }
                                try
                                {
                                    System.IO.File.SetAttributes(inputFile, System.IO.FileAttributes.Normal);
                                    //System.IO.File.Delete(inputFile);
                                }
                                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                                // Report progress as a percentage of the total task.
                                int percentComplete = (int)((i + 2) * 100 / (total + 1));

                                percentComplete = (percentComplete > 100 ? 100 : percentComplete);
                                if (percentComplete > highestPercentageReached)
                                {
                                    highestPercentageReached = percentComplete;
                                    worker.ReportProgress(percentComplete);
                                }
                            }
                            else
                            {
                                //Cap nhat dung lai version truoc khi xay ra loi
                                Company.KDT.SHARE.Components.Version.UpdateVesion(dataCurrenVersion);
                                SetRichText("\r\n-> Có lỗi xảy ra: " + (error.Length > 0 ? error : logError) + "\r\n");
                                e.Result = error;
                                e.Cancel = true;
                                break;
                            }
                        }
                    }
                }

            }
        }

        #endregion

    }
}
