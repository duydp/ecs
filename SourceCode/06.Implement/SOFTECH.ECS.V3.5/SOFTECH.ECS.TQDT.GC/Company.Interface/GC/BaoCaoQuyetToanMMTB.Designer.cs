﻿namespace Company.Interface.GC
{
    partial class BaoCaoQuyetToanMMTB
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListHD_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoQuyetToanMMTB));
            Janus.Windows.GridEX.GridEXLayout dgListMMTB_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label23 = new System.Windows.Forms.Label();
            this.dateNgayTN = new System.Windows.Forms.DateTimePicker();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListHD = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgListMMTB = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExportExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdAdd1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSendEdit1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdFeedback1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdUpdateGuidString1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdChuyenTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenTT");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdFeedback = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedback");
            this.cmdResult = new Janus.Windows.UI.CommandBars.UICommand("cmdResult");
            this.cmdSendEdit = new Janus.Windows.UI.CommandBars.UICommand("cmdSendEdit");
            this.cmdUpdateGuidString = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateGuidString");
            this.cmdAdd = new Janus.Windows.UI.CommandBars.UICommand("cmdAdd");
            this.cmdChuyenTT = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenTT");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdUpdateResult = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateResult");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.cmdUpdateResult1 = new Janus.Windows.UI.CommandBars.UICommand("cmdUpdateResult");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHD)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListMMTB)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 659), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 659);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 635);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 635);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(910, 659);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.dateNgayTN);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(910, 136);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "THÔNG TIN CHUNG";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(153, 62);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = true;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(426, 22);
            this.donViHaiQuanControl1.TabIndex = 5;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(27, 67);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(56, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Hải quan :";
            // 
            // dateNgayTN
            // 
            this.dateNgayTN.CustomFormat = "dd/MM/yyyy";
            this.dateNgayTN.Enabled = false;
            this.dateNgayTN.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateNgayTN.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateNgayTN.Location = new System.Drawing.Point(457, 25);
            this.dateNgayTN.Name = "dateNgayTN";
            this.dateNgayTN.Size = new System.Drawing.Size(189, 22);
            this.dateNgayTN.TabIndex = 2;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(153, 25);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(150, 22);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.Text = "0";
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(153, 94);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(745, 22);
            this.txtGhiChu.TabIndex = 3;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(814, 29);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(84, 14);
            this.lblTrangThai.TabIndex = 0;
            this.lblTrangThai.Text = "Chưa khai báo";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(703, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Trạng thái: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(344, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Ngày tiếp nhận";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(26, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ghi chú :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(26, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số tiếp nhận";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(113)))), ((int)(((byte)(103)))));
            this.uiGroupBox3.Controls.Add(this.uiTab1);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.ForeColor = System.Drawing.Color.DarkGreen;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 136);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(910, 523);
            this.uiGroupBox3.TabIndex = 9;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiTab1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiTab1.Location = new System.Drawing.Point(3, 8);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(904, 512);
            this.uiTab1.TabIndex = 3;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgListHD);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(902, 490);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "DANH SÁCH HỢP ĐỒNG";
            // 
            // dgListHD
            // 
            this.dgListHD.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHD.AlternatingColors = true;
            this.dgListHD.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHD.ColumnAutoResize = true;
            dgListHD_DesignTimeLayout.LayoutString = resources.GetString("dgListHD_DesignTimeLayout.LayoutString");
            this.dgListHD.DesignTimeLayout = dgListHD_DesignTimeLayout;
            this.dgListHD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHD.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHD.FrozenColumns = 3;
            this.dgListHD.GroupByBoxVisible = false;
            this.dgListHD.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHD.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHD.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHD.Location = new System.Drawing.Point(0, 0);
            this.dgListHD.Margin = new System.Windows.Forms.Padding(0);
            this.dgListHD.Name = "dgListHD";
            this.dgListHD.RecordNavigator = true;
            this.dgListHD.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHD.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHD.Size = new System.Drawing.Size(902, 490);
            this.dgListHD.TabIndex = 8;
            this.dgListHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListHD.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListHD_RowDoubleClick);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgListMMTB);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(902, 490);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "TỔNG HỢP SỐ LIỆU (GỬI LÊN HQ)";
            // 
            // dgListMMTB
            // 
            this.dgListMMTB.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListMMTB.AlternatingColors = true;
            this.dgListMMTB.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListMMTB.ColumnAutoResize = true;
            this.dgListMMTB.ContextMenuStrip = this.contextMenuStrip1;
            dgListMMTB_DesignTimeLayout.LayoutString = resources.GetString("dgListMMTB_DesignTimeLayout.LayoutString");
            this.dgListMMTB.DesignTimeLayout = dgListMMTB_DesignTimeLayout;
            this.dgListMMTB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListMMTB.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListMMTB.FrozenColumns = 3;
            this.dgListMMTB.GroupByBoxVisible = false;
            this.dgListMMTB.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListMMTB.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListMMTB.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListMMTB.Location = new System.Drawing.Point(0, 0);
            this.dgListMMTB.Margin = new System.Windows.Forms.Padding(0);
            this.dgListMMTB.Name = "dgListMMTB";
            this.dgListMMTB.RecordNavigator = true;
            this.dgListMMTB.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListMMTB.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListMMTB.Size = new System.Drawing.Size(902, 490);
            this.dgListMMTB.TabIndex = 9;
            this.dgListMMTB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListMMTB.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListMMTB_RowDoubleClick);
            this.dgListMMTB.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListMMTB_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExportExcel});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(140, 26);
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(139, 22);
            this.mnuExportExcel.Text = "XUẤT EXCEL";
            this.mnuExportExcel.Click += new System.EventHandler(this.mnuExportExcel_Click);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSave,
            this.cmdFeedback,
            this.cmdResult,
            this.cmdSendEdit,
            this.cmdUpdateGuidString,
            this.cmdAdd,
            this.cmdChuyenTT,
            this.cmdUpdateResult,
            this.cmdHistory});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAdd1,
            this.cmdSave1,
            this.cmdSend1,
            this.cmdSendEdit1,
            this.cmdFeedback1,
            this.cmdResult1,
            this.cmdUpdateGuidString1,
            this.cmdChuyenTT1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(936, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdAdd1
            // 
            this.cmdAdd1.Key = "cmdAdd";
            this.cmdAdd1.Name = "cmdAdd1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave1.Image")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend1.Image")));
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdSendEdit1
            // 
            this.cmdSendEdit1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit1.Image")));
            this.cmdSendEdit1.Key = "cmdSendEdit";
            this.cmdSendEdit1.Name = "cmdSendEdit1";
            // 
            // cmdFeedback1
            // 
            this.cmdFeedback1.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback1.Image")));
            this.cmdFeedback1.Key = "cmdFeedback";
            this.cmdFeedback1.Name = "cmdFeedback1";
            // 
            // cmdResult1
            // 
            this.cmdResult1.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult1.Image")));
            this.cmdResult1.Key = "cmdResult";
            this.cmdResult1.Name = "cmdResult1";
            // 
            // cmdUpdateGuidString1
            // 
            this.cmdUpdateGuidString1.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString1.Name = "cmdUpdateGuidString1";
            this.cmdUpdateGuidString1.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdChuyenTT1
            // 
            this.cmdChuyenTT1.Key = "cmdChuyenTT";
            this.cmdChuyenTT1.Name = "cmdChuyenTT1";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu lại";
            // 
            // cmdFeedback
            // 
            this.cmdFeedback.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedback.Image")));
            this.cmdFeedback.Key = "cmdFeedback";
            this.cmdFeedback.Name = "cmdFeedback";
            this.cmdFeedback.Text = "Nhận phản hồi";
            // 
            // cmdResult
            // 
            this.cmdResult.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdUpdateResult1,
            this.cmdHistory1});
            this.cmdResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdResult.Image")));
            this.cmdResult.Key = "cmdResult";
            this.cmdResult.Name = "cmdResult";
            this.cmdResult.Text = "Kết quả xử lý";
            // 
            // cmdSendEdit
            // 
            this.cmdSendEdit.Image = ((System.Drawing.Image)(resources.GetObject("cmdSendEdit.Image")));
            this.cmdSendEdit.Key = "cmdSendEdit";
            this.cmdSendEdit.Name = "cmdSendEdit";
            this.cmdSendEdit.Text = "Khai báo sửa";
            // 
            // cmdUpdateGuidString
            // 
            this.cmdUpdateGuidString.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateGuidString.Image")));
            this.cmdUpdateGuidString.Key = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Name = "cmdUpdateGuidString";
            this.cmdUpdateGuidString.Text = "Cập nhật chuỗi phản hồi";
            // 
            // cmdAdd
            // 
            this.cmdAdd.Image = ((System.Drawing.Image)(resources.GetObject("cmdAdd.Image")));
            this.cmdAdd.Key = "cmdAdd";
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Text = "Thêm hợp đồng";
            // 
            // cmdChuyenTT
            // 
            this.cmdChuyenTT.Image = ((System.Drawing.Image)(resources.GetObject("cmdChuyenTT.Image")));
            this.cmdChuyenTT.Key = "cmdChuyenTT";
            this.cmdChuyenTT.Name = "cmdChuyenTT";
            this.cmdChuyenTT.Text = "Chuyển Khai báo sửa";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1116, 28);
            // 
            // cmdUpdateResult
            // 
            this.cmdUpdateResult.Image = ((System.Drawing.Image)(resources.GetObject("cmdUpdateResult.Image")));
            this.cmdUpdateResult.Key = "cmdUpdateResult";
            this.cmdUpdateResult.Name = "cmdUpdateResult";
            this.cmdUpdateResult.Text = "Cập nhật Thông tin";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistory.Image")));
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Lịch sử Khai báo";
            // 
            // cmdUpdateResult1
            // 
            this.cmdUpdateResult1.Key = "cmdUpdateResult";
            this.cmdUpdateResult1.Name = "cmdUpdateResult1";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // BaoCaoQuyetToanMMTB
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1116, 693);
            this.Controls.Add(this.TopRebar1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaoCaoQuyetToanMMTB";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Báo cáo quyết toán máy móc thiết bị hợp đồng gia công";
            this.Load += new System.EventHandler(this.BaoCaoQuyetToanMMTB_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHD)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListMMTB)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgListHD;
        private Janus.Windows.GridEX.GridEX dgListMMTB;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback1;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedback;
        private Janus.Windows.UI.CommandBars.UICommand cmdResult;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private System.Windows.Forms.DateTimePicker dateNgayTN;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString1;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateGuidString;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mnuExportExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd;
        private Janus.Windows.UI.CommandBars.UICommand cmdAdd1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSendEdit1;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenTT;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenTT1;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateResult;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private Janus.Windows.UI.CommandBars.UICommand cmdUpdateResult1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
    }
}