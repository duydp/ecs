﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace KeyServer
{
	public partial class LM_Keys_Activity : ICloneable
	{
		#region Properties.
		
		public long Id { set; get; }
		public string ActivedKey { set; get; }
		public int RecordCount { set; get; }
		public string CustomerCode { set; get; }
		public string CustomerName { set; get; }
		public string CustomerAddress { set; get; }
		public string CustomerPhone { set; get; }
		public string CustomerEmail { set; get; }
		public DateTime LastCheckStatus { set; get; }
		public string CustomerField1 { set; get; }
		public string CustomerField2 { set; get; }
		public string CustomerField3 { set; get; }
		public string CustomerField4 { set; get; }
		public string CustomerField5 { set; get; }
		public string CustomerField6 { set; get; }
		public string CustomerField7 { set; get; }
		public string CustomerField8 { set; get; }
		public string CustomerField9 { set; get; }
		public string CustomerField10 { set; get; }
		public DateTime CreatedDate { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<LM_Keys_Activity> ConvertToCollection(IDataReader reader)
		{
			List<LM_Keys_Activity> collection = new List<LM_Keys_Activity>();
			while (reader.Read())
			{
				LM_Keys_Activity entity = new LM_Keys_Activity();
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt64(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActivedKey"))) entity.ActivedKey = reader.GetString(reader.GetOrdinal("ActivedKey"));
				if (!reader.IsDBNull(reader.GetOrdinal("RecordCount"))) entity.RecordCount = reader.GetInt32(reader.GetOrdinal("RecordCount"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerCode"))) entity.CustomerCode = reader.GetString(reader.GetOrdinal("CustomerCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerName"))) entity.CustomerName = reader.GetString(reader.GetOrdinal("CustomerName"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerAddress"))) entity.CustomerAddress = reader.GetString(reader.GetOrdinal("CustomerAddress"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerPhone"))) entity.CustomerPhone = reader.GetString(reader.GetOrdinal("CustomerPhone"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerEmail"))) entity.CustomerEmail = reader.GetString(reader.GetOrdinal("CustomerEmail"));
				if (!reader.IsDBNull(reader.GetOrdinal("LastCheckStatus"))) entity.LastCheckStatus = reader.GetDateTime(reader.GetOrdinal("LastCheckStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField1"))) entity.CustomerField1 = reader.GetString(reader.GetOrdinal("CustomerField1"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField2"))) entity.CustomerField2 = reader.GetString(reader.GetOrdinal("CustomerField2"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField3"))) entity.CustomerField3 = reader.GetString(reader.GetOrdinal("CustomerField3"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField4"))) entity.CustomerField4 = reader.GetString(reader.GetOrdinal("CustomerField4"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField5"))) entity.CustomerField5 = reader.GetString(reader.GetOrdinal("CustomerField5"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField6"))) entity.CustomerField6 = reader.GetString(reader.GetOrdinal("CustomerField6"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField7"))) entity.CustomerField7 = reader.GetString(reader.GetOrdinal("CustomerField7"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField8"))) entity.CustomerField8 = reader.GetString(reader.GetOrdinal("CustomerField8"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField9"))) entity.CustomerField9 = reader.GetString(reader.GetOrdinal("CustomerField9"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomerField10"))) entity.CustomerField10 = reader.GetString(reader.GetOrdinal("CustomerField10"));
				if (!reader.IsDBNull(reader.GetOrdinal("CreatedDate"))) entity.CreatedDate = reader.GetDateTime(reader.GetOrdinal("CreatedDate"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<LM_Keys_Activity> collection, long id)
        {
            foreach (LM_Keys_Activity item in collection)
            {
                if (item.Id == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_LM_Keys_Activity VALUES(@ActivedKey, @RecordCount, @CustomerCode, @CustomerName, @CustomerAddress, @CustomerPhone, @CustomerEmail, @LastCheckStatus, @CustomerField1, @CustomerField2, @CustomerField3, @CustomerField4, @CustomerField5, @CustomerField6, @CustomerField7, @CustomerField8, @CustomerField9, @CustomerField10, @CreatedDate)";
            string update = "UPDATE t_LM_Keys_Activity SET ActivedKey = @ActivedKey, RecordCount = @RecordCount, CustomerCode = @CustomerCode, CustomerName = @CustomerName, CustomerAddress = @CustomerAddress, CustomerPhone = @CustomerPhone, CustomerEmail = @CustomerEmail, LastCheckStatus = @LastCheckStatus, CustomerField1 = @CustomerField1, CustomerField2 = @CustomerField2, CustomerField3 = @CustomerField3, CustomerField4 = @CustomerField4, CustomerField5 = @CustomerField5, CustomerField6 = @CustomerField6, CustomerField7 = @CustomerField7, CustomerField8 = @CustomerField8, CustomerField9 = @CustomerField9, CustomerField10 = @CustomerField10, CreatedDate = @CreatedDate WHERE Id = @Id";
            string delete = "DELETE FROM t_LM_Keys_Activity WHERE Id = @Id";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_LM_Keys_Activity VALUES(@ActivedKey, @RecordCount, @CustomerCode, @CustomerName, @CustomerAddress, @CustomerPhone, @CustomerEmail, @LastCheckStatus, @CustomerField1, @CustomerField2, @CustomerField3, @CustomerField4, @CustomerField5, @CustomerField6, @CustomerField7, @CustomerField8, @CustomerField9, @CustomerField10, @CreatedDate)";
            string update = "UPDATE t_LM_Keys_Activity SET ActivedKey = @ActivedKey, RecordCount = @RecordCount, CustomerCode = @CustomerCode, CustomerName = @CustomerName, CustomerAddress = @CustomerAddress, CustomerPhone = @CustomerPhone, CustomerEmail = @CustomerEmail, LastCheckStatus = @LastCheckStatus, CustomerField1 = @CustomerField1, CustomerField2 = @CustomerField2, CustomerField3 = @CustomerField3, CustomerField4 = @CustomerField4, CustomerField5 = @CustomerField5, CustomerField6 = @CustomerField6, CustomerField7 = @CustomerField7, CustomerField8 = @CustomerField8, CustomerField9 = @CustomerField9, CustomerField10 = @CustomerField10, CreatedDate = @CreatedDate WHERE Id = @Id";
            string delete = "DELETE FROM t_LM_Keys_Activity WHERE Id = @Id";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActivedKey", SqlDbType.VarChar, "ActivedKey", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@RecordCount", SqlDbType.Int, "RecordCount", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerCode", SqlDbType.NVarChar, "CustomerCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerName", SqlDbType.NVarChar, "CustomerName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerAddress", SqlDbType.NVarChar, "CustomerAddress", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerPhone", SqlDbType.NVarChar, "CustomerPhone", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerEmail", SqlDbType.NVarChar, "CustomerEmail", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastCheckStatus", SqlDbType.DateTime, "LastCheckStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField1", SqlDbType.NVarChar, "CustomerField1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField2", SqlDbType.NVarChar, "CustomerField2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField3", SqlDbType.NVarChar, "CustomerField3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField4", SqlDbType.NVarChar, "CustomerField4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField5", SqlDbType.NVarChar, "CustomerField5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField6", SqlDbType.NVarChar, "CustomerField6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField7", SqlDbType.NVarChar, "CustomerField7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField8", SqlDbType.NVarChar, "CustomerField8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField9", SqlDbType.NVarChar, "CustomerField9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomerField10", SqlDbType.NVarChar, "CustomerField10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CreatedDate", SqlDbType.DateTime, "CreatedDate", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@Id", SqlDbType.BigInt, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LM_Keys_Activity Load(long id)
		{
			const string spName = "[dbo].[p_LM_Keys_Activity_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<LM_Keys_Activity> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_Keys_Activity_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Keys_Activity_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_Keys_Activity_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_Keys_Activity_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertLM_Keys_Activity(string activedKey, int recordCount, string customerCode, string customerName, string customerAddress, string customerPhone, string customerEmail, DateTime lastCheckStatus, string customerField1, string customerField2, string customerField3, string customerField4, string customerField5, string customerField6, string customerField7, string customerField8, string customerField9, string customerField10, DateTime createdDate)
		{
			LM_Keys_Activity entity = new LM_Keys_Activity();	
			entity.ActivedKey = activedKey;
			entity.RecordCount = recordCount;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.CustomerPhone = customerPhone;
			entity.CustomerEmail = customerEmail;
			entity.LastCheckStatus = lastCheckStatus;
			entity.CustomerField1 = customerField1;
			entity.CustomerField2 = customerField2;
			entity.CustomerField3 = customerField3;
			entity.CustomerField4 = customerField4;
			entity.CustomerField5 = customerField5;
			entity.CustomerField6 = customerField6;
			entity.CustomerField7 = customerField7;
			entity.CustomerField8 = customerField8;
			entity.CustomerField9 = customerField9;
			entity.CustomerField10 = customerField10;
			entity.CreatedDate = createdDate;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_Keys_Activity_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@Id", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ActivedKey", SqlDbType.VarChar, ActivedKey);
			db.AddInParameter(dbCommand, "@RecordCount", SqlDbType.Int, RecordCount);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.NVarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.NVarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@CustomerField1", SqlDbType.NVarChar, CustomerField1);
			db.AddInParameter(dbCommand, "@CustomerField2", SqlDbType.NVarChar, CustomerField2);
			db.AddInParameter(dbCommand, "@CustomerField3", SqlDbType.NVarChar, CustomerField3);
			db.AddInParameter(dbCommand, "@CustomerField4", SqlDbType.NVarChar, CustomerField4);
			db.AddInParameter(dbCommand, "@CustomerField5", SqlDbType.NVarChar, CustomerField5);
			db.AddInParameter(dbCommand, "@CustomerField6", SqlDbType.NVarChar, CustomerField6);
			db.AddInParameter(dbCommand, "@CustomerField7", SqlDbType.NVarChar, CustomerField7);
			db.AddInParameter(dbCommand, "@CustomerField8", SqlDbType.NVarChar, CustomerField8);
			db.AddInParameter(dbCommand, "@CustomerField9", SqlDbType.NVarChar, CustomerField9);
			db.AddInParameter(dbCommand, "@CustomerField10", SqlDbType.NVarChar, CustomerField10);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				Id = (long) db.GetParameterValue(dbCommand, "@Id");
				return Id;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				Id = (long) db.GetParameterValue(dbCommand, "@Id");
				return Id;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLM_Keys_Activity(long id, string activedKey, int recordCount, string customerCode, string customerName, string customerAddress, string customerPhone, string customerEmail, DateTime lastCheckStatus, string customerField1, string customerField2, string customerField3, string customerField4, string customerField5, string customerField6, string customerField7, string customerField8, string customerField9, string customerField10, DateTime createdDate)
		{
			LM_Keys_Activity entity = new LM_Keys_Activity();			
			entity.Id = id;
			entity.ActivedKey = activedKey;
			entity.RecordCount = recordCount;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.CustomerPhone = customerPhone;
			entity.CustomerEmail = customerEmail;
			entity.LastCheckStatus = lastCheckStatus;
			entity.CustomerField1 = customerField1;
			entity.CustomerField2 = customerField2;
			entity.CustomerField3 = customerField3;
			entity.CustomerField4 = customerField4;
			entity.CustomerField5 = customerField5;
			entity.CustomerField6 = customerField6;
			entity.CustomerField7 = customerField7;
			entity.CustomerField8 = customerField8;
			entity.CustomerField9 = customerField9;
			entity.CustomerField10 = customerField10;
			entity.CreatedDate = createdDate;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_Keys_Activity_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, Id);
			db.AddInParameter(dbCommand, "@ActivedKey", SqlDbType.VarChar, ActivedKey);
			db.AddInParameter(dbCommand, "@RecordCount", SqlDbType.Int, RecordCount);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.NVarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.NVarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@CustomerField1", SqlDbType.NVarChar, CustomerField1);
			db.AddInParameter(dbCommand, "@CustomerField2", SqlDbType.NVarChar, CustomerField2);
			db.AddInParameter(dbCommand, "@CustomerField3", SqlDbType.NVarChar, CustomerField3);
			db.AddInParameter(dbCommand, "@CustomerField4", SqlDbType.NVarChar, CustomerField4);
			db.AddInParameter(dbCommand, "@CustomerField5", SqlDbType.NVarChar, CustomerField5);
			db.AddInParameter(dbCommand, "@CustomerField6", SqlDbType.NVarChar, CustomerField6);
			db.AddInParameter(dbCommand, "@CustomerField7", SqlDbType.NVarChar, CustomerField7);
			db.AddInParameter(dbCommand, "@CustomerField8", SqlDbType.NVarChar, CustomerField8);
			db.AddInParameter(dbCommand, "@CustomerField9", SqlDbType.NVarChar, CustomerField9);
			db.AddInParameter(dbCommand, "@CustomerField10", SqlDbType.NVarChar, CustomerField10);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLM_Keys_Activity(long id, string activedKey, int recordCount, string customerCode, string customerName, string customerAddress, string customerPhone, string customerEmail, DateTime lastCheckStatus, string customerField1, string customerField2, string customerField3, string customerField4, string customerField5, string customerField6, string customerField7, string customerField8, string customerField9, string customerField10, DateTime createdDate)
		{
			LM_Keys_Activity entity = new LM_Keys_Activity();			
			entity.Id = id;
			entity.ActivedKey = activedKey;
			entity.RecordCount = recordCount;
			entity.CustomerCode = customerCode;
			entity.CustomerName = customerName;
			entity.CustomerAddress = customerAddress;
			entity.CustomerPhone = customerPhone;
			entity.CustomerEmail = customerEmail;
			entity.LastCheckStatus = lastCheckStatus;
			entity.CustomerField1 = customerField1;
			entity.CustomerField2 = customerField2;
			entity.CustomerField3 = customerField3;
			entity.CustomerField4 = customerField4;
			entity.CustomerField5 = customerField5;
			entity.CustomerField6 = customerField6;
			entity.CustomerField7 = customerField7;
			entity.CustomerField8 = customerField8;
			entity.CustomerField9 = customerField9;
			entity.CustomerField10 = customerField10;
			entity.CreatedDate = createdDate;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Keys_Activity_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, Id);
			db.AddInParameter(dbCommand, "@ActivedKey", SqlDbType.VarChar, ActivedKey);
			db.AddInParameter(dbCommand, "@RecordCount", SqlDbType.Int, RecordCount);
			db.AddInParameter(dbCommand, "@CustomerCode", SqlDbType.NVarChar, CustomerCode);
			db.AddInParameter(dbCommand, "@CustomerName", SqlDbType.NVarChar, CustomerName);
			db.AddInParameter(dbCommand, "@CustomerAddress", SqlDbType.NVarChar, CustomerAddress);
			db.AddInParameter(dbCommand, "@CustomerPhone", SqlDbType.NVarChar, CustomerPhone);
			db.AddInParameter(dbCommand, "@CustomerEmail", SqlDbType.NVarChar, CustomerEmail);
			db.AddInParameter(dbCommand, "@LastCheckStatus", SqlDbType.DateTime, LastCheckStatus.Year <= 1753 ? DBNull.Value : (object) LastCheckStatus);
			db.AddInParameter(dbCommand, "@CustomerField1", SqlDbType.NVarChar, CustomerField1);
			db.AddInParameter(dbCommand, "@CustomerField2", SqlDbType.NVarChar, CustomerField2);
			db.AddInParameter(dbCommand, "@CustomerField3", SqlDbType.NVarChar, CustomerField3);
			db.AddInParameter(dbCommand, "@CustomerField4", SqlDbType.NVarChar, CustomerField4);
			db.AddInParameter(dbCommand, "@CustomerField5", SqlDbType.NVarChar, CustomerField5);
			db.AddInParameter(dbCommand, "@CustomerField6", SqlDbType.NVarChar, CustomerField6);
			db.AddInParameter(dbCommand, "@CustomerField7", SqlDbType.NVarChar, CustomerField7);
			db.AddInParameter(dbCommand, "@CustomerField8", SqlDbType.NVarChar, CustomerField8);
			db.AddInParameter(dbCommand, "@CustomerField9", SqlDbType.NVarChar, CustomerField9);
			db.AddInParameter(dbCommand, "@CustomerField10", SqlDbType.NVarChar, CustomerField10);
			db.AddInParameter(dbCommand, "@CreatedDate", SqlDbType.DateTime, CreatedDate.Year <= 1753 ? DBNull.Value : (object) CreatedDate);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLM_Keys_Activity(long id)
		{
			LM_Keys_Activity entity = new LM_Keys_Activity();
			entity.Id = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_Keys_Activity_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, Id);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_Keys_Activity_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}