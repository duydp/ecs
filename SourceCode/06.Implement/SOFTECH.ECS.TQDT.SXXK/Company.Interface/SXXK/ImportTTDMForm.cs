using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
namespace Company.Interface.SXXK
{
    public partial class ImportTTDMForm : BaseForm
    {
        public ThongTinDinhMucCollection TTDMCollection = new ThongTinDinhMucCollection();
        public ThongTinDinhMucCollection TTDMReadCollection = new ThongTinDinhMucCollection();
        public ThongTinDinhMucCollection TTDMChuaCoCollection = new ThongTinDinhMucCollection();
        public ImportTTDMForm()
        {
            InitializeComponent();
        }

        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private int checkTTDMExit1(string maSP)
        {
            for (int i = 0; i < this.TTDMCollection.Count; i++)
            {
                if (this.TTDMCollection[i].MaSanPham.ToUpper() == maSP.ToUpper()) return i;
            }
            return -1;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                btnSave.Enabled = false;
                if (chkSaveSetting.Checked == true)
                {
                    GlobalSettings.Luu_TTDM(txtMaSPColumn.Text, txtSoDMColumn.Text, txtNgayDKColumn.Text, txtNgayADColumn.Text);
                }
                if (chkOverwrite.Checked == true)
                {
                    new ThongTinDinhMuc().InsertUpdate(this.TTDMReadCollection);
                }
                else
                {
                    new ThongTinDinhMuc().InsertUpdate(this.TTDMChuaCoCollection);
                }
                this.TTDMCollection.AddRange(this.TTDMChuaCoCollection);
                ShowMessage("Import thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
                this.Close();
            }
            
        }

        private void ImportTTDMForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            txtMaSPColumn.Text = GlobalSettings.ThongTinDinhMuc.MaSP;
            txtSoDMColumn.Text = GlobalSettings.ThongTinDinhMuc.SoDinhMuc;
            txtNgayDKColumn.Text = GlobalSettings.ThongTinDinhMuc.NgayDangKy;
            txtNgayADColumn.Text = GlobalSettings.ThongTinDinhMuc.NgayApDung;
            this.dgList.DataSource = this.TTDMReadCollection;
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                //error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, 8);
                return;

            }
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text, true );
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                this.Cursor = Cursors.Default;
                uiButton1.Enabled = true;
                return;
            }
            WorksheetRowCollection wsrc = ws.Rows;
            char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
            int maSPCol = ConvertCharToInt(maSPColumn);
            char soDMColumn = Convert.ToChar(txtSoDMColumn.Text);
            int soDMCol = ConvertCharToInt(soDMColumn);
            char ngayDKColumn = Convert.ToChar(txtNgayDKColumn.Text);
            int ngayDKCol = ConvertCharToInt(ngayDKColumn);
            char ngayADColumn = Convert.ToChar(txtNgayADColumn.Text);
            int ngayADCol = ConvertCharToInt(ngayADColumn);
            this.TTDMReadCollection.Clear();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {

                    try
                    {

                        ThongTinDinhMuc TTDM = new ThongTinDinhMuc();
                        TTDM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        TTDM.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        TTDM.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                        TTDM.SoDinhMuc = Convert.ToInt32(wsr.Cells[soDMCol].Value);
                        TTDM.NgayDangKy = DateTime.FromOADate((double)(wsr.Cells[ngayDKCol].Value));

                        if((wsr.Cells[ngayADCol].Value)!=null)
                        {
                            TTDM.NgayApDung = DateTime.FromOADate((double)(wsr.Cells[ngayADCol].Value));
                        }
                        if (checkTTDMExit1(TTDM.MaSanPham) >= 0)
                        {
                            TTDM.IsExist = 1;
                        }
                        else this.TTDMChuaCoCollection.Add(TTDM);
                        this.TTDMReadCollection.Add(TTDM);
                    }
                    catch
                    {
                        if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        {
                            this.TTDMReadCollection.Clear();
                            this.Cursor = Cursors.Default;
                            uiButton1.Enabled = true;
                            return;
                        }
                    }
                }
            }
            dgList.DataSource = this.TTDMReadCollection;
            dgList.Refetch();
            this.Cursor = Cursors.Default;
            uiButton1.Enabled = true;
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (Convert.ToDateTime(e.Row.Cells["NgayApDung"].Value) <= new DateTime(1901, 1, 1)) e.Row.Cells["NgayApDung"].Text = "";
            }
        }


    }
}