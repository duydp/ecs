﻿namespace Company.Interface.GC
{
    partial class BaoCaoQuyetToanMMTB_HangHoaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListTotal_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoQuyetToanMMTB_HangHoaForm));
            Janus.Windows.GridEX.GridEXLayout cbHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgListHopDong_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListTotal = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaSoHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label11 = new System.Windows.Forms.Label();
            this.clcNgayHH = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcNgayHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbHopDong = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ctrDVTLuong1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.btnImportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTaiXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTonCuoiKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtNhapTrongKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtChuyenMDSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListHopDong = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnAddHD = new Janus.Windows.EditControls.UIButton();
            this.btnDeleteHD = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 701), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 701);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 677);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 677);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox6);
            this.grbMain.Controls.Add(this.uiGroupBox7);
            this.grbMain.Controls.Add(this.uiGroupBox5);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(994, 701);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.dgListTotal);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(0, 472);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(994, 183);
            this.uiGroupBox6.TabIndex = 114;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListTotal
            // 
            this.dgListTotal.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTotal.AlternatingColors = true;
            this.dgListTotal.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListTotal.ColumnAutoResize = true;
            dgListTotal_DesignTimeLayout.LayoutString = resources.GetString("dgListTotal_DesignTimeLayout.LayoutString");
            this.dgListTotal.DesignTimeLayout = dgListTotal_DesignTimeLayout;
            this.dgListTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTotal.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTotal.FrozenColumns = 3;
            this.dgListTotal.GroupByBoxVisible = false;
            this.dgListTotal.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTotal.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTotal.Location = new System.Drawing.Point(3, 8);
            this.dgListTotal.Margin = new System.Windows.Forms.Padding(0);
            this.dgListTotal.Name = "dgListTotal";
            this.dgListTotal.RecordNavigator = true;
            this.dgListTotal.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTotal.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTotal.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTotal.Size = new System.Drawing.Size(988, 172);
            this.dgListTotal.TabIndex = 11;
            this.dgListTotal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTotal.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListTotal_RowDoubleClick);
            this.dgListTotal.SelectionChanged += new System.EventHandler(this.dgListTotal_SelectionChanged);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnClose);
            this.uiGroupBox7.Controls.Add(this.btnDelete);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 655);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(994, 46);
            this.uiGroupBox7.TabIndex = 113;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(902, 15);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(816, 15);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(80, 23);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.ctrMaSoHang);
            this.uiGroupBox5.Controls.Add(this.label11);
            this.uiGroupBox5.Controls.Add(this.clcNgayHH);
            this.uiGroupBox5.Controls.Add(this.clcNgayHD);
            this.uiGroupBox5.Controls.Add(this.cbHopDong);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label4);
            this.uiGroupBox5.Controls.Add(this.label3);
            this.uiGroupBox5.Controls.Add(this.ctrDVTLuong1);
            this.uiGroupBox5.Controls.Add(this.btnImportExcel);
            this.uiGroupBox5.Controls.Add(this.btnAdd);
            this.uiGroupBox5.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox5.Controls.Add(this.txtGhiChu);
            this.uiGroupBox5.Controls.Add(this.label13);
            this.uiGroupBox5.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox5.Controls.Add(this.label14);
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Controls.Add(this.label16);
            this.uiGroupBox5.Controls.Add(this.txtTaiXuat);
            this.uiGroupBox5.Controls.Add(this.txtTonCuoiKy);
            this.uiGroupBox5.Controls.Add(this.txtNhapTrongKy);
            this.uiGroupBox5.Controls.Add(this.txtChuyenMDSD);
            this.uiGroupBox5.Controls.Add(this.label24);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.label2);
            this.uiGroupBox5.Controls.Add(this.label1);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox5.Location = new System.Drawing.Point(0, 206);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(994, 266);
            this.uiGroupBox5.TabIndex = 112;
            this.uiGroupBox5.Text = "Thông tin hàng hóa";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrMaSoHang
            // 
            this.ctrMaSoHang.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrMaSoHang.Appearance.Options.UseBackColor = true;
            this.ctrMaSoHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ctrMaSoHang.Code = "";
            this.ctrMaSoHang.ColorControl = System.Drawing.Color.Empty;
            this.ctrMaSoHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaSoHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaSoHang.IsOnlyWarning = false;
            this.ctrMaSoHang.IsValidate = true;
            this.ctrMaSoHang.Location = new System.Drawing.Point(313, 55);
            this.ctrMaSoHang.Name = "ctrMaSoHang";
            this.ctrMaSoHang.Name_VN = "";
            this.ctrMaSoHang.SetOnlyWarning = false;
            this.ctrMaSoHang.SetValidate = false;
            this.ctrMaSoHang.ShowColumnCode = true;
            this.ctrMaSoHang.ShowColumnName = false;
            this.ctrMaSoHang.Size = new System.Drawing.Size(126, 21);
            this.ctrMaSoHang.TabIndex = 27;
            this.ctrMaSoHang.TagName = "";
            this.ctrMaSoHang.Where = null;
            this.ctrMaSoHang.WhereCondition = "";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(228, 59);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 13);
            this.label11.TabIndex = 28;
            this.label11.Text = "Mã số hàng hóa";
            // 
            // clcNgayHH
            // 
            this.clcNgayHH.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHH.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHH.DropDownCalendar.Name = "";
            this.clcNgayHH.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHH.Location = new System.Drawing.Point(634, 167);
            this.clcNgayHH.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.clcNgayHH.Name = "clcNgayHH";
            this.clcNgayHH.ReadOnly = true;
            this.clcNgayHH.Size = new System.Drawing.Size(112, 21);
            this.clcNgayHH.TabIndex = 17;
            this.clcNgayHH.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // clcNgayHD
            // 
            this.clcNgayHD.CustomFormat = "dd/MM/yyyy";
            this.clcNgayHD.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcNgayHD.DropDownCalendar.Name = "";
            this.clcNgayHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHD.Location = new System.Drawing.Point(444, 167);
            this.clcNgayHD.MinDate = new System.DateTime(1990, 1, 1, 0, 0, 0, 0);
            this.clcNgayHD.Name = "clcNgayHD";
            this.clcNgayHD.ReadOnly = true;
            this.clcNgayHD.Size = new System.Drawing.Size(99, 21);
            this.clcNgayHD.TabIndex = 17;
            this.clcNgayHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // cbHopDong
            // 
            cbHopDong_DesignTimeLayout.LayoutString = resources.GetString("cbHopDong_DesignTimeLayout.LayoutString");
            this.cbHopDong.DesignTimeLayout = cbHopDong_DesignTimeLayout;
            this.cbHopDong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbHopDong.Location = new System.Drawing.Point(445, 131);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.SelectedIndex = -1;
            this.cbHopDong.SelectedItem = null;
            this.cbHopDong.Size = new System.Drawing.Size(301, 22);
            this.cbHopDong.TabIndex = 16;
            this.cbHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbHopDong.ValueChanged += new System.EventHandler(this.cbHopDong_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(550, 170);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 14);
            this.label5.TabIndex = 15;
            this.label5.Text = "Ngày hết hạn";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(348, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 14);
            this.label4.TabIndex = 15;
            this.label4.Text = "Ngày hợp đồng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(348, 135);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 14);
            this.label3.TabIndex = 15;
            this.label3.Text = "Hợp đồng";
            // 
            // ctrDVTLuong1
            // 
            this.ctrDVTLuong1.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.ctrDVTLuong1.Appearance.Options.UseBackColor = true;
            this.ctrDVTLuong1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ctrDVTLuong1.Code = "";
            this.ctrDVTLuong1.ColorControl = System.Drawing.Color.Empty;
            this.ctrDVTLuong1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDVTLuong1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDVTLuong1.IsOnlyWarning = false;
            this.ctrDVTLuong1.IsValidate = true;
            this.ctrDVTLuong1.Location = new System.Drawing.Point(122, 55);
            this.ctrDVTLuong1.Name = "ctrDVTLuong1";
            this.ctrDVTLuong1.Name_VN = "";
            this.ctrDVTLuong1.SetOnlyWarning = false;
            this.ctrDVTLuong1.SetValidate = false;
            this.ctrDVTLuong1.ShowColumnCode = true;
            this.ctrDVTLuong1.ShowColumnName = false;
            this.ctrDVTLuong1.Size = new System.Drawing.Size(95, 21);
            this.ctrDVTLuong1.TabIndex = 5;
            this.ctrDVTLuong1.TagName = "";
            this.ctrDVTLuong1.Where = null;
            this.ctrDVTLuong1.WhereCondition = "";
            // 
            // btnImportExcel
            // 
            this.btnImportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnImportExcel.Image")));
            this.btnImportExcel.Location = new System.Drawing.Point(223, 231);
            this.btnImportExcel.Name = "btnImportExcel";
            this.btnImportExcel.Size = new System.Drawing.Size(145, 23);
            this.btnImportExcel.TabIndex = 14;
            this.btnImportExcel.Text = "Nhập hàng hóa từ Excel";
            this.btnImportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnImportExcel.Click += new System.EventHandler(this.btnImportExcel_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.Location = new System.Drawing.Point(122, 231);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(95, 23);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.BackColor = System.Drawing.Color.White;
            this.txtTenHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangHoa.Location = new System.Drawing.Point(445, 20);
            this.txtTenHangHoa.Multiline = true;
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(536, 56);
            this.txtTenHangHoa.TabIndex = 4;
            this.txtTenHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(122, 202);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(866, 21);
            this.txtGhiChu.TabIndex = 13;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(16, 216);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Ghi chú :";
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.BackColor = System.Drawing.Color.White;
            this.txtMaHangHoa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangHoa.Location = new System.Drawing.Point(122, 20);
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(231, 21);
            this.txtMaHangHoa.TabIndex = 3;
            this.txtMaHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangHoa.ButtonClick += new System.EventHandler(this.txtMaHangHoa_ButtonClick);
            this.txtMaHangHoa.Leave += new System.EventHandler(this.txtMaHangHoa_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(359, 24);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(80, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tên hàng hóa :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(16, 24);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(76, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã hàng hóa :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(16, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(66, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Đơn vị tính :";
            // 
            // txtTaiXuat
            // 
            this.txtTaiXuat.DecimalDigits = 4;
            this.txtTaiXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTaiXuat.Location = new System.Drawing.Point(445, 95);
            this.txtTaiXuat.Name = "txtTaiXuat";
            this.txtTaiXuat.Size = new System.Drawing.Size(160, 21);
            this.txtTaiXuat.TabIndex = 8;
            this.txtTaiXuat.Text = "0.0000";
            this.txtTaiXuat.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTaiXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTaiXuat.TextChanged += new System.EventHandler(this.txtTaiXuat_TextChanged);
            // 
            // txtTonCuoiKy
            // 
            this.txtTonCuoiKy.DecimalDigits = 4;
            this.txtTonCuoiKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTonCuoiKy.Location = new System.Drawing.Point(122, 171);
            this.txtTonCuoiKy.Name = "txtTonCuoiKy";
            this.txtTonCuoiKy.ReadOnly = true;
            this.txtTonCuoiKy.Size = new System.Drawing.Size(160, 21);
            this.txtTonCuoiKy.TabIndex = 12;
            this.txtTonCuoiKy.Text = "0.0000";
            this.txtTonCuoiKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTonCuoiKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNhapTrongKy
            // 
            this.txtNhapTrongKy.DecimalDigits = 4;
            this.txtNhapTrongKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNhapTrongKy.Location = new System.Drawing.Point(122, 95);
            this.txtNhapTrongKy.Name = "txtNhapTrongKy";
            this.txtNhapTrongKy.Size = new System.Drawing.Size(160, 21);
            this.txtNhapTrongKy.TabIndex = 7;
            this.txtNhapTrongKy.Text = "0.0000";
            this.txtNhapTrongKy.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtNhapTrongKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNhapTrongKy.TextChanged += new System.EventHandler(this.txtNhapTrongKy_TextChanged);
            // 
            // txtChuyenMDSD
            // 
            this.txtChuyenMDSD.DecimalDigits = 4;
            this.txtChuyenMDSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChuyenMDSD.Location = new System.Drawing.Point(122, 131);
            this.txtChuyenMDSD.Name = "txtChuyenMDSD";
            this.txtChuyenMDSD.Size = new System.Drawing.Size(160, 21);
            this.txtChuyenMDSD.TabIndex = 9;
            this.txtChuyenMDSD.Text = "0.0000";
            this.txtChuyenMDSD.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtChuyenMDSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChuyenMDSD.TextChanged += new System.EventHandler(this.txtChuyenMDSD_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(16, 176);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(77, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Lượng còn lại :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(16, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Lượng chuyển tiếp :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(348, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Lượng tái xuất :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lượng tạm nhập :";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.dgListHopDong);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(994, 206);
            this.uiGroupBox2.TabIndex = 111;
            this.uiGroupBox2.Text = "Danh sách hợp đồng";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListHopDong
            // 
            this.dgListHopDong.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHopDong.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListHopDong.AlternatingColors = true;
            this.dgListHopDong.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgListHopDong.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListHopDong.ColumnAutoResize = true;
            dgListHopDong_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgListHopDong_DesignTimeLayout_Reference_0.Instance")));
            dgListHopDong_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgListHopDong_DesignTimeLayout_Reference_0});
            dgListHopDong_DesignTimeLayout.LayoutString = resources.GetString("dgListHopDong_DesignTimeLayout.LayoutString");
            this.dgListHopDong.DesignTimeLayout = dgListHopDong_DesignTimeLayout;
            this.dgListHopDong.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListHopDong.GroupByBoxVisible = false;
            this.dgListHopDong.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHopDong.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListHopDong.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListHopDong.Location = new System.Drawing.Point(3, 17);
            this.dgListHopDong.Name = "dgListHopDong";
            this.dgListHopDong.RecordNavigator = true;
            this.dgListHopDong.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListHopDong.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListHopDong.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListHopDong.Size = new System.Drawing.Size(988, 146);
            this.dgListHopDong.TabIndex = 106;
            this.dgListHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListHopDong.VisualStyleManager = this.vsmMain;
            this.dgListHopDong.SelectionChanged += new System.EventHandler(this.dgListHopDong_SelectionChanged);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnAddHD);
            this.uiGroupBox1.Controls.Add(this.btnDeleteHD);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 163);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(988, 40);
            this.uiGroupBox1.TabIndex = 105;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnAddHD
            // 
            this.btnAddHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddHD.Image = ((System.Drawing.Image)(resources.GetObject("btnAddHD.Image")));
            this.btnAddHD.Location = new System.Drawing.Point(765, 12);
            this.btnAddHD.Name = "btnAddHD";
            this.btnAddHD.Size = new System.Drawing.Size(116, 23);
            this.btnAddHD.TabIndex = 1;
            this.btnAddHD.Text = "Chọn Hợp đồng";
            this.btnAddHD.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddHD.Click += new System.EventHandler(this.btnAddHD_Click);
            // 
            // btnDeleteHD
            // 
            this.btnDeleteHD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteHD.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteHD.Image")));
            this.btnDeleteHD.Location = new System.Drawing.Point(887, 12);
            this.btnDeleteHD.Name = "btnDeleteHD";
            this.btnDeleteHD.Size = new System.Drawing.Size(93, 23);
            this.btnDeleteHD.TabIndex = 2;
            this.btnDeleteHD.Text = "Xóa HĐ";
            this.btnDeleteHD.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDeleteHD.Click += new System.EventHandler(this.btnDeleteHD_Click);
            // 
            // BaoCaoQuyetToanMMTB_HangHoaForm
            // 
            this.ClientSize = new System.Drawing.Size(1200, 707);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaoCaoQuyetToanMMTB_HangHoaForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin hợp đồng ";
            this.Load += new System.EventHandler(this.BaoCaoQuyetToanMMTB_HangHoaForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.GridEX.GridEX dgListTotal;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDVTLuong1;
        private Janus.Windows.EditControls.UIButton btnImportExcel;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTaiXuat;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTonCuoiKy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNhapTrongKy;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtChuyenMDSD;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgListHopDong;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnAddHD;
        private Janus.Windows.EditControls.UIButton btnDeleteHD;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbHopDong;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHD;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHH;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label13;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaSoHang;
        public System.Windows.Forms.Label label11;
    }
}
