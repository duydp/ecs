﻿namespace Company.Interface.VNACCS.ExportTotalData
{
    partial class VNACC_BaoCaoChiTietHHXNKForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem7 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem8 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem9 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem10 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem11 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem12 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem13 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_BaoCaoChiTietHHXNKForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrMaHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.cbbGroupBy = new Janus.Windows.EditControls.UIComboBox();
            this.clcDenNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.clcTuNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbbLoaiBC = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnExportExcel = new Janus.Windows.EditControls.UIButton();
            this.btnTimKiem = new Janus.Windows.EditControls.UIButton();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoQLNB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaLoaiHinh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.grList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1332, 554);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrMaHaiQuan);
            this.uiGroupBox1.Controls.Add(this.cbbGroupBy);
            this.uiGroupBox1.Controls.Add(this.clcDenNgay);
            this.uiGroupBox1.Controls.Add(this.clcTuNgay);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.cbbLoaiBC);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.btnExportExcel);
            this.uiGroupBox1.Controls.Add(this.btnTimKiem);
            this.uiGroupBox1.Controls.Add(this.txtTenDoiTac);
            this.uiGroupBox1.Controls.Add(this.txtMaHS);
            this.uiGroupBox1.Controls.Add(this.txtSoQLNB);
            this.uiGroupBox1.Controls.Add(this.txtTenHangHoa);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox1.Controls.Add(this.txtMaHangHoa);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.txtMaLoaiHinh);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1332, 148);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.Text = "Tim kiếm";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ctrMaHaiQuan
            // 
            this.ctrMaHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrMaHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHaiQuan.Code = "";
            this.ctrMaHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHaiQuan.IsOnlyWarning = false;
            this.ctrMaHaiQuan.IsValidate = true;
            this.ctrMaHaiQuan.Location = new System.Drawing.Point(105, 16);
            this.ctrMaHaiQuan.Name = "ctrMaHaiQuan";
            this.ctrMaHaiQuan.Name_VN = "";
            this.ctrMaHaiQuan.SetOnlyWarning = false;
            this.ctrMaHaiQuan.SetValidate = false;
            this.ctrMaHaiQuan.ShowColumnCode = true;
            this.ctrMaHaiQuan.ShowColumnName = true;
            this.ctrMaHaiQuan.Size = new System.Drawing.Size(441, 26);
            this.ctrMaHaiQuan.TabIndex = 32;
            this.ctrMaHaiQuan.TagCode = "";
            this.ctrMaHaiQuan.TagName = "";
            this.ctrMaHaiQuan.Where = null;
            this.ctrMaHaiQuan.WhereCondition = "";
            // 
            // cbbGroupBy
            // 
            this.cbbGroupBy.ForeColor = System.Drawing.SystemColors.ControlText;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Số tờ khai";
            uiComboBoxItem1.Value = "SoToKhai";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Mã loại hình";
            uiComboBoxItem2.Value = "MaLoaiHinh";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Mã hải quan";
            uiComboBoxItem3.Value = "CoQuanHaiQuan";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Ngày đăng ký";
            uiComboBoxItem4.Value = "NgayDangKy";
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Tên đơn vị";
            uiComboBoxItem5.Value = "TenDonVi";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Tên đối tác";
            uiComboBoxItem6.Value = "TenDoiTac";
            uiComboBoxItem7.FormatStyle.Alpha = 0;
            uiComboBoxItem7.IsSeparator = false;
            uiComboBoxItem7.Text = "Số hợp đồng";
            uiComboBoxItem7.Value = "SoHopDong";
            uiComboBoxItem8.FormatStyle.Alpha = 0;
            uiComboBoxItem8.IsSeparator = false;
            uiComboBoxItem8.Text = "Mã hàng hóa";
            uiComboBoxItem8.Value = "MaHangHoa";
            uiComboBoxItem9.FormatStyle.Alpha = 0;
            uiComboBoxItem9.IsSeparator = false;
            uiComboBoxItem9.Text = "Tên hàng hóa";
            uiComboBoxItem9.Value = "TenHang";
            uiComboBoxItem10.FormatStyle.Alpha = 0;
            uiComboBoxItem10.IsSeparator = false;
            uiComboBoxItem10.Text = "Mã HS";
            uiComboBoxItem10.Value = "MaSoHang";
            this.cbbGroupBy.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4,
            uiComboBoxItem5,
            uiComboBoxItem6,
            uiComboBoxItem7,
            uiComboBoxItem8,
            uiComboBoxItem9,
            uiComboBoxItem10});
            this.cbbGroupBy.Location = new System.Drawing.Point(1057, 109);
            this.cbbGroupBy.Name = "cbbGroupBy";
            this.cbbGroupBy.Size = new System.Drawing.Size(179, 21);
            this.cbbGroupBy.TabIndex = 31;
            this.cbbGroupBy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbGroupBy.SelectedIndexChanged += new System.EventHandler(this.cbbGroupBy_SelectedIndexChanged);
            // 
            // clcDenNgay
            // 
            this.clcDenNgay.AutoSize = false;
            this.clcDenNgay.BackColor = System.Drawing.Color.White;
            this.clcDenNgay.CustomFormat = "dd/MM/yyyy";
            this.clcDenNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcDenNgay.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcDenNgay.DropDownCalendar.Name = "";
            this.clcDenNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcDenNgay.Location = new System.Drawing.Point(729, 106);
            this.clcDenNgay.Name = "clcDenNgay";
            this.clcDenNgay.Nullable = true;
            this.clcDenNgay.NullButtonText = "Xóa";
            this.clcDenNgay.ShowNullButton = true;
            this.clcDenNgay.Size = new System.Drawing.Size(109, 22);
            this.clcDenNgay.TabIndex = 30;
            this.clcDenNgay.TodayButtonText = "Hôm nay";
            this.clcDenNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcDenNgay.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // clcTuNgay
            // 
            this.clcTuNgay.AutoSize = false;
            this.clcTuNgay.BackColor = System.Drawing.Color.White;
            this.clcTuNgay.CustomFormat = "dd/MM/yyyy";
            this.clcTuNgay.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.clcTuNgay.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.clcTuNgay.DropDownCalendar.Name = "";
            this.clcTuNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcTuNgay.Location = new System.Drawing.Point(729, 78);
            this.clcTuNgay.Name = "clcTuNgay";
            this.clcTuNgay.Nullable = true;
            this.clcTuNgay.NullButtonText = "Xóa";
            this.clcTuNgay.ShowNullButton = true;
            this.clcTuNgay.Size = new System.Drawing.Size(109, 22);
            this.clcTuNgay.TabIndex = 29;
            this.clcTuNgay.TodayButtonText = "Hôm nay";
            this.clcTuNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcTuNgay.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(954, 113);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 13);
            this.label13.TabIndex = 28;
            this.label13.Text = "Hiển thị theo nhóm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(655, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Đến ngày";
            // 
            // cbbLoaiBC
            // 
            this.cbbLoaiBC.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem11.FormatStyle.Alpha = 0;
            uiComboBoxItem11.IsSeparator = false;
            uiComboBoxItem11.Text = "Tất cả";
            uiComboBoxItem11.Value = "V";
            uiComboBoxItem12.FormatStyle.Alpha = 0;
            uiComboBoxItem12.IsSeparator = false;
            uiComboBoxItem12.Text = "Nhập khẩu";
            uiComboBoxItem12.Value = "NV";
            uiComboBoxItem13.FormatStyle.Alpha = 0;
            uiComboBoxItem13.IsSeparator = false;
            uiComboBoxItem13.Text = "Xuất khẩu";
            uiComboBoxItem13.Value = "XV";
            this.cbbLoaiBC.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem11,
            uiComboBoxItem12,
            uiComboBoxItem13});
            this.cbbLoaiBC.Location = new System.Drawing.Point(518, 48);
            this.cbbLoaiBC.Name = "cbbLoaiBC";
            this.cbbLoaiBC.Size = new System.Drawing.Size(109, 21);
            this.cbbLoaiBC.TabIndex = 23;
            this.cbbLoaiBC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiBC.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(655, 83);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 11;
            this.label9.Text = "Từ ngày";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(423, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 22;
            this.label2.Text = "Loại báo cáo";
            // 
            // btnExportExcel
            // 
            this.btnExportExcel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportExcel.Image = ((System.Drawing.Image)(resources.GetObject("btnExportExcel.Image")));
            this.btnExportExcel.ImageSize = new System.Drawing.Size(20, 20);
            this.btnExportExcel.Location = new System.Drawing.Point(853, 77);
            this.btnExportExcel.Name = "btnExportExcel";
            this.btnExportExcel.Size = new System.Drawing.Size(90, 23);
            this.btnExportExcel.TabIndex = 17;
            this.btnExportExcel.Text = "Xuất Excel";
            this.btnExportExcel.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnExportExcel.Click += new System.EventHandler(this.btnExportExcel_Click);
            // 
            // btnTimKiem
            // 
            this.btnTimKiem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiem.Image = ((System.Drawing.Image)(resources.GetObject("btnTimKiem.Image")));
            this.btnTimKiem.ImageIndex = 4;
            this.btnTimKiem.ImageSize = new System.Drawing.Size(20, 20);
            this.btnTimKiem.Location = new System.Drawing.Point(853, 107);
            this.btnTimKiem.Name = "btnTimKiem";
            this.btnTimKiem.Size = new System.Drawing.Size(90, 23);
            this.btnTimKiem.TabIndex = 17;
            this.btnTimKiem.Text = "Tìm kiếm";
            this.btnTimKiem.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnTimKiem.Click += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Location = new System.Drawing.Point(729, 48);
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(109, 21);
            this.txtTenDoiTac.TabIndex = 10;
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDoiTac.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtMaHS
            // 
            this.txtMaHS.Location = new System.Drawing.Point(518, 109);
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(109, 21);
            this.txtMaHS.TabIndex = 10;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHS.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtSoQLNB
            // 
            this.txtSoQLNB.Location = new System.Drawing.Point(518, 79);
            this.txtSoQLNB.Name = "txtSoQLNB";
            this.txtSoQLNB.Size = new System.Drawing.Size(109, 21);
            this.txtSoQLNB.TabIndex = 10;
            this.txtSoQLNB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoQLNB.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtTenHangHoa
            // 
            this.txtTenHangHoa.Location = new System.Drawing.Point(304, 109);
            this.txtTenHangHoa.Name = "txtTenHangHoa";
            this.txtTenHangHoa.Size = new System.Drawing.Size(109, 21);
            this.txtTenHangHoa.TabIndex = 10;
            this.txtTenHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenHangHoa.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(655, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 9;
            this.label8.Text = "Tên đối tác";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(423, 113);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 9;
            this.label12.Text = "Mã HS";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(304, 79);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(109, 21);
            this.txtSoHopDong.TabIndex = 10;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHopDong.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // txtMaHangHoa
            // 
            this.txtMaHangHoa.Location = new System.Drawing.Point(105, 109);
            this.txtMaHangHoa.Name = "txtMaHangHoa";
            this.txtMaHangHoa.Size = new System.Drawing.Size(109, 21);
            this.txtMaHangHoa.TabIndex = 10;
            this.txtMaHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHangHoa.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(423, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Số quản lý nội bộ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(228, 113);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Tên hàng hóa";
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.Location = new System.Drawing.Point(105, 79);
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(109, 21);
            this.txtSoHoaDon.TabIndex = 10;
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDon.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(228, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Số hợp đồng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 113);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Mã hàng hóa";
            // 
            // txtMaLoaiHinh
            // 
            this.txtMaLoaiHinh.Location = new System.Drawing.Point(105, 48);
            this.txtMaLoaiHinh.Name = "txtMaLoaiHinh";
            this.txtMaLoaiHinh.Size = new System.Drawing.Size(308, 21);
            this.txtMaLoaiHinh.TabIndex = 10;
            this.txtMaLoaiHinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaLoaiHinh.TextChanged += new System.EventHandler(this.btnTimKiem_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Số hóa đơn";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 52);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Mã loại hình";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Cơ quan Hải quan";
            // 
            // grList
            // 
            this.grList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.grList.CardViewGridlines = Janus.Windows.GridEX.CardViewGridlines.Both;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.grList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grList.FrozenColumns = 5;
            this.grList.GroupByBoxVisible = false;
            this.grList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.grList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grList.Hierarchical = true;
            this.grList.Location = new System.Drawing.Point(0, 148);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.ScrollBars = Janus.Windows.GridEX.ScrollBars.Both;
            this.grList.Size = new System.Drawing.Size(1332, 406);
            this.grList.TabIndex = 4;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.grList_FormattingRow);
            // 
            // VNACC_BaoCaoChiTietHHXNKForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1332, 554);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_BaoCaoChiTietHHXNKForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Báo cáo Chi tiết hàng hóa xuất nhập khẩu";
            this.Load += new System.EventHandler(this.VNACC_BaoCaoChiTietHHXNKForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiBC;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIButton btnExportExcel;
        private Janus.Windows.EditControls.UIButton btnTimKiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHS;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQLNB;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangHoa;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangHoa;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinh;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.CalendarCombo.CalendarCombo clcDenNgay;
        private Janus.Windows.CalendarCombo.CalendarCombo clcTuNgay;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIComboBox cbbGroupBy;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHaiQuan;
    }
}