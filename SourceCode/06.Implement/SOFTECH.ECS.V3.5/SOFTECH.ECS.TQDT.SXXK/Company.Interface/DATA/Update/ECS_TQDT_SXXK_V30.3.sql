
/****** Object:  StoredProcedure [dbo].[p_SXXK_SelectThueToKhaiNK]    Script Date: 01/09/2015 08:01:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[p_SXXK_SelectThueToKhaiNK]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime,
	@MaDoanhNghiep varchar(14),
	@TenChuHang nvarchar(100)
AS
SELECT 
CASE WHEN MaLoaiHinh LIKE '%V%' THEN(Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK =Sotokhai) ELSE SoToKhai END as SoToKhai, 
 MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy, Sum(ThueXNKTon)as TongThueTon FROM(
SELECT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy,b.NgayDangKy,b.MaDoanhNghiep, a.MaHaiQuan, a.ThueXNKTon FROM dbo.t_SXXK_ThanhLy_NPLNhapTon a 
INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
ON a.SoToKhai = b.SoToKhai
AND a.MaLoaiHinh = b.MaLoaiHinh
AND a.NamDangKy = b.NamDangKy 
AND a.MaHaiQuan = b.MaHaiQuan AND TenChuHang LIKE @TenChuHang)c
WHERE NgayDangKy BETWEEN @FromDate AND @ToDate AND MaDoanhNghiep = @MaDoanhNghiep 
GROUP BY SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy


Go

/****** Object:  StoredProcedure [dbo].[p_SXXK_SelectTriGiaHangToKhaiXK]    Script Date: 01/10/2015 07:48:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[p_SXXK_SelectTriGiaHangToKhaiXK]
	-- Add the parameters for the stored procedure here
	@FromDate DateTime,
	@ToDate DateTime,
	@MaDoanhNghiep varchar(14),
	@TenChuHang nvarchar(100)
AS
SELECT CASE WHEN MaLoaiHinh LIKE '%V%' THEN( SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhai) ELSE SoToKhai END as SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy,NguyenTe_ID, Sum(TriGiaKB)as TongTriGia FROM(
	SELECT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy,b.NgayDangKy,b.MaDoanhNghiep, a.MaHaiQuan, a.TriGiaKB,b.NguyenTe_ID FROM dbo.t_SXXK_HangMauDich a 
	INNER JOIN dbo.t_SXXK_ToKhaiMauDich b 
	ON a.SoToKhai = b.SoToKhai
	AND a.MaLoaiHinh = b.MaLoaiHinh
	AND a.NamDangKy = b.NamDangKy 
	AND a.MaHaiQuan = b.MaHaiQuan
	WHERE a.MaLoaiHinh LIKE 'X%' AND TenChuHang LIKE @TenChuHang)c
WHERE NgayDangKy BETWEEN @FromDate AND @ToDate AND MaDoanhNghiep = @MaDoanhNghiep
GROUP BY SoToKhai, MaLoaiHinh, NamDangKy, MaHaiQuan, NgayDangKy, NguyenTe_ID

Go
/****** Object:  StoredProcedure [dbo].[p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic]    Script Date: 01/10/2015 07:50:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_SXXK_ThanhLy_NPLNhapTon_SelectDynamic]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT 	[SoToKhai],
	CASE WHEN MaLoaiHinh LIKE ''%V%'' THEN(SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhai) END AS SoTKVNACCS,
	[SoToKhai],
	[MaLoaiHinh],
	[NamDangKy],
	[MaHaiQuan],
	[MaNPL],
	[MaDoanhNghiep],
	[Luong],
	[Ton],
	[ThueXNK],
	[ThueTTDB],
	[ThueVAT],
	[PhuThu],
	[ThueCLGia],
	[ThueXNKTon],
	Case when MaLoaiHinh like ''%V%'' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = SoToKhai)) else (select top 1 SoHopDong from t_SXXK_ToKhaiMauDich where SoToKhai = t_SXXK_ThanhLy_NPLNhapTon.SoToKhai)  end AS SoHopDong
 FROM [dbo].[t_SXXK_ThanhLy_NPLNhapTon]  ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]    
-- Database: Haiquan    
-- Author: ecs
-- Alter Phiph 13/01/2015
-- Time created: Tuesday, July 01, 2008    
------------------------------------------------------------------------------------------------------------------------    
    
alter PROCEDURE [dbo].[p_KDT_SXXK_BCThueXNK_SelectDynamic]    
 @WhereCondition nvarchar(max),    
 @OrderByExpression nvarchar(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL nvarchar(max)    
    
SET @SQL = 'SELECT a.ID,    
 a.STT,    
 a.LanThanhLy,    
 a.NamThanhLy,    
 a.MaDoanhNghiep,    
 Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,    
 a.NgayDangKyNhap,    
 Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,    
 --a.NgayThucNhap,  
 case WHEN year(a.NgayThucNhap)<=1900 then (select NgayHoanThanh from t_sxxk_tokhaimaudich where sotokhai = a.sotokhainhap and MaLoaiHinh= a.MaLoaiHinhNhap and Ngaydangky= a.Ngaydangkynhap ) else a.NgayThucNhap end as NgayThucNhap,    
 UPPER(a.MaNPL) AS MaNPL,    
 case WHEN a.TenNPL is NULL OR a.TenNPL ='''' then b.Ten else a.TenNPL end as TenNPL,    
 a.LuongNhap,    
 a.TenDVT_NPL,    
 a.DonGiaTT,    
 a.TyGiaTT,    
 a.ThueSuat,    
 a.ThueNKNop,    
 Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,    
 a.NgayDangKyXuat,    
 case WHEN year(a.NgayThucXuat)<=1900 then null else a.NgayThucXuat end as NgayThucXuat,    
 Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,    
 Case When a.LuongNPLSuDung+ a.LuongNPLTon>Luong then a.Luong - a.LuongNPLTon else a.LuongNPLSuDung end AS LuongNPLSuDung ,    
 --a.LuongNPLSuDung,    
 a.LuongNPLTon,    
 a.TienThueHoan,    
 a.TienThueTKTiep,    
 a.GhiChu,    
 a.ThueXNK,    
 a.Luong    
 FROM t_KDT_SXXK_BCThueXNK a    
 INNER JOIN t_SXXK_NguyenPhuLieu b    
 ON a.MaNPL  = b.Ma AND a.MaDoanhNghiep = b.MaDoanhNghiep WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
    
    
    Go
      
      
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]      
-- Database: ECS_TQDT_SXXK      
-- Author: ecs 
-- Alter: Phiph 13/01/2015     
-- Time created: Friday, October 07, 2011      
------------------------------------------------------------------------------------------------------------------------      
      alter PROCEDURE [dbo].[p_SXXK_BCThueXNK_TT196]      
   @LanThanhLy bigint      
  ,@NamThanhLy int      
  ,@MaDoanhNghiep varchar(50)      
  ,@SapXepNgayDangKyTKN bit      
AS      
      
BEGIN      
       
       
 if OBJECT_ID('tempdb..#BCThueXNK_TT196') is not null drop table #BCThueXNK_TT196      
      
CREATE TABLE #BCThueXNK_TT196      
(      
 STT BIGINT      
 ,SoToKhai NVARCHAR(255)      
 ,TenNPL NVARCHAR(1000)      
 ,DVT NVARCHAR(30)      
 ,LuongNhap DECIMAL(18,5)      
 ,TienThueHoan DECIMAL(18,8)      
 ,ThuePhaiThu DECIMAL(18,8)      
 ,GhiChu NVARCHAR(255)      
 ,ThueNKNop DECIMAL(18,0)      
 ,LuongNPLSuDung DECIMAL(18,5)      
)      
IF(@SapXepNgayDangKyTKN = 1)      
       
 BEGIN -- Sắp xếp theo ngày đăng ký      
        
INSERT INTO #BCThueXNK_TT196      
(      
 STT,      
 SoToKhai,      
 TenNPL,      
 DVT,      
 LuongNhap,      
 TienThueHoan,      
 ThuePhaiThu,      
 GhiChu,      
 ThueNKNop,      
 LuongNPLSuDung      
       
)      
      
SELECT       
   (ROW_NUMBER () OVER( ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL)) AS STT      
      ,(Convert(Nvarchar(12),Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';'        + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM    
      t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai      
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL      
      ,MAX(TenDVT_NPL) AS DVT      
      ,Max([LuongNhap]) AS LuongNhap      
      ,0 AS TienThueHoan      
      ,0 AS ThuePhaiThu      
      ,'' as GhiChu      
      ,MAX(ThueNKNop) AS ThueNKNop      
   ,SUM(LuongNPLSuDung) AS LuongSuDung      
         
  FROM t_KDT_SXXK_BCThueXNK      
  where LanThanhLy = @LanThanhLy  and MaDoanhNghiep = @MaDoanhNghiep -- and NamThanhLy = @NamThanhLy    
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap , DonGiaTT       
ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL      
      
  END      
ELSE -- Sắp xếp theo mã NPL      
 BEGIN      
        
  INSERT INTO #BCThueXNK_TT196      
(      
 STT,      
 SoToKhai,      
 TenNPL,      
 DVT,      
 LuongNhap,      
 TienThueHoan,      
 ThuePhaiThu,      
 GhiChu,      
 ThueNKNop,      
 LuongNPLSuDung      
       
)      
      
SELECT       
   (ROW_NUMBER () OVER( ORDER BY  MaNPL ,NgayDangKyNhap,SoToKhaiNhap)) AS STT      
      ,(Convert(Nvarchar(12),Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';'        + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM    
      t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai      
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL      
      ,MAX(TenDVT_NPL) AS DVT      
      ,Max([LuongNhap]) AS LuongNhap      
      ,0 AS TienThueHoan      
      ,0 AS ThuePhaiThu      
      ,'' as GhiChu      
      --,MAX(ThueNKNop) AS ThueNKNop
      ,Case when  MAX(ThueNKNop)=0 then MAX(ThueXNK) else MAX(ThueNKNop) end AS ThueNKNop  --MAX(ThueNKNop) AS ThueNKNop  Phiph ThueNKNop = ThueXNK trong truong hop NPL tk tái xuat    
   ,SUM(LuongNPLSuDung) AS LuongSuDung      
         
  FROM t_KDT_SXXK_BCThueXNK      
  where LanThanhLy = @LanThanhLy and MaDoanhNghiep = @MaDoanhNghiep    -- and NamThanhLy = @NamThanhLy   
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap , DonGiaTT       
ORDER BY  MaNPL,NgayDangKyNhap,SoToKhaiNhap      
        
 END      
 UPDATE #BCThueXNK_TT196      
SET      
 LuongNPLSuDung =LuongNhap   where    LuongNPLSuDung>LuongNhap
UPDATE #BCThueXNK_TT196      
SET      
 TienThueHoan = ROUND((LuongNPLSuDung/LuongNhap) * ThueNKNop,0)   --where    LuongNPLSuDung<=LuongNhap
 
UPDATE #BCThueXNK_TT196      
SET       
 ThuePhaiThu =  ThueNKNop - TienThueHoan      
      
SELECT * FROM #BCThueXNK_TT196      
      
DROP TABLE #BCThueXNK_TT196      
       
END      
GO
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK200')
insert into t_VNACC_Category_Common VALUES ('A521','VK200',N'Phân bón','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK210')
insert into t_VNACC_Category_Common VALUES ('A521','VK210',N'Máy móc, thiết bị chuyên dùng phục vụ cho sản xuất nông nghiệp','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK220')
insert into t_VNACC_Category_Common VALUES ('A521','VK220',N'Thức ăn gia súc, gia cầm và thức ăn cho vật nuôi khác','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK230')
insert into t_VNACC_Category_Common VALUES ('A521','VK230',N'Tàu đánh bắt xa bờ','','','','','')
if NOT EXISTS (select * from t_VNACC_Category_Common where referenceDB='A521' and Code ='VK900')
insert into t_VNACC_Category_Common VALUES ('A521','VK900',N'Hàng hóa khác','','','','','')

Go
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '30.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('30.3',GETDATE(), N' Cập nhật các store trong chức năng can doi NXTon, Thue,')
END	
