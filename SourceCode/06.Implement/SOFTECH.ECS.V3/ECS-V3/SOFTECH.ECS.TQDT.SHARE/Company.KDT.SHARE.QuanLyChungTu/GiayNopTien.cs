﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class GiayNopTien
    {
        private List<GiayNopTienChiTiet> _chitietcollection = new List<GiayNopTienChiTiet>();
        public List<GiayNopTienChiTiet> ChiTietCollection
        {
            get { return _chitietcollection; }
            set { _chitietcollection = value; }
        }

        private List<GiayNopTienChungTu> _chungtucollection = new List<GiayNopTienChungTu>();
        public List<GiayNopTienChungTu> ChungTuCollection
        {
            get { return _chungtucollection; }
            set { _chungtucollection = value; }
        }



        public void LoadChiTiet()
        {
            string condition = "GiayNopTien_ID = " + this.ID;
            _chitietcollection = GiayNopTienChiTiet.SelectCollectionDynamic(condition, string.Empty);
        }
        public void LoadChungTu()
        {
            string condition = "GiayNopTien_ID = " + this.ID;
            _chungtucollection = GiayNopTienChungTu.SelectCollectionDynamic(condition, string.Empty);
        }

        public void SaveFull()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {

                        if (ID > 0)
                            Update(transaction);
                        else
                            Insert(transaction);
                        if(ChiTietCollection!=null)
                        foreach (GiayNopTienChiTiet chitiet in ChiTietCollection)
                        {

                            if (chitiet.ID > 0)
                                chitiet.Update(transaction);
                            else
                            {
                                chitiet.GiayNopTien_ID = ID;
                                chitiet.Insert(transaction);
                            }
                        }
                        if (ChungTuCollection != null)                        
                        foreach (GiayNopTienChungTu chungtu in ChungTuCollection)
                        {
                            chungtu.GiayNopTien_ID = ID;
                            if (chungtu.ID > 0)
                                chungtu.Update(transaction);
                            else
                            {
                                chungtu.GiayNopTien_ID = ID;
                                chungtu.Insert(transaction);
                            }

                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

        }
        public void DeleteFull()
        {

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {

                        foreach (GiayNopTienChiTiet chitiet in ChiTietCollection)
                        {
                            chitiet.Delete(transaction);

                        }
                        foreach (GiayNopTienChungTu chungtu in ChungTuCollection)
                        {

                            chungtu.Delete(transaction);
                        }
                        Delete(transaction);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

        }
    }
}
