﻿//Lypt created
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using System.Windows.Forms;

namespace Company.Interface.Report.GC
{
    public partial class TQDTToKhaiXK : DevExpress.XtraReports.UI.XtraReport
    {
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public string PTVT_Name = "";
        //public string PTTT_Name = "";
        //public string DKGH = "";
        //public string DongTienTT = "";
        //public string NuocXK = "";

        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKXTQDTForm report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public TQDTToKhaiXK()
        {
            InitializeComponent();
        }

        public string GetChiCucHQCK()
        {
            string chiCucHQCK = "";
            if (this.TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                chiCucHQCK = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            }
            else
            {
                chiCucHQCK = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            }
            return chiCucHQCK;
        }

        public void BindReport()
        {

            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongTienThueXNK = 0;
            decimal tongTienThueTatCa = 0;
            decimal tongTriGiaTT = 0;
            decimal tongTriGiaTTGTGT = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;

            //lblMienThue2.Text = GlobalSettings.DonViBaoCao;
            //lblMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            //lblMienThue1.Visible = MienThue1;
            //lblMienThue2.Visible = MienThue2;
            lblMienThueNK.Visible = MienThue1;
            lblMienThueGTGT.Visible = MienThue2;

            DateTime minDate = new DateTime(1900, 1, 1);
            //if (this.TKMD.SoTiepNhan != 0)
            //    this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;

            /*CHI CUC HAI QUAN*/
            lblChiCucHQCK.Text = GetChiCucHQCK();
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            //So tham chieu
            if (this.TKMD.ID > 0)
                lblThamChieu.Text = this.TKMD.ID + "";
            else
                lblThamChieu.Text = "";

            //Ngay gui
            if (this.TKMD.NgayTiepNhan > minDate)
                lblNgayGui.Text = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
            else
                lblNgayGui.Text = "";

            //So To khai
            if (this.TKMD.SoToKhai > 0)
                lblToKhai.Text = this.TKMD.SoToKhai + "";
            else
                lblToKhai.Text = "";

            //Ngay dang ky
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";

            //1. Nguoi XK
            string val = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontNguoiXuatKhau");
            int valFont = 8;
            if (!string.IsNullOrEmpty((val)))
            {
                valFont = Convert.ToInt32(val);

            }
            lblNguoiXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            lblNguoiXK.Font = new Font("Times New Roman", valFont);

            if (GlobalSettings.MaMID.Trim().Equals(""))
            {
                if (GlobalSettings.WordWrap)
                    lblNguoiXK.Text = (this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
                else
                    lblNguoiXK.Text = (this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper();
            }
            else
            {
                if (GlobalSettings.WordWrap)
                    lblNguoiXK.Text = (this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI + "\r\nMã MID: " + GlobalSettings.MaMID).ToUpper();
                else
                    lblNguoiXK.Text = (this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI + ". Mã MID: " + GlobalSettings.MaMID).ToUpper();
            }

            lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);// +"\r\n" + GlobalSettings.DIA_CHI.ToUpper();   
            //if (lblNguoiXK.Text.Length > 130)
            //    lblNguoiXK.Text = lblNguoiXK.Text.Trim().Substring(0, 130);

            //2.Nguoi NK
            lblNguoiNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            lblNguoiNK.Font = new Font("Times New Roman", 8);
            lblNguoiNK.Text = TKMD.TenDonViDoiTac.ToUpper();
            //if (lblNguoiNK.Text.Length > 130)
            //    lblNguoiNK.Text = lblNguoiNK.Text.Trim().Substring(0, 130);

            //3. Nguoi Uy thac
            lblNguoiUyThac.Text = TKMD.TenDonViUT;

            //4. Dai ly lam thu tuc hai quan


            //5. Loai hinh
            string stlh = "";
            stlh = Company.GC.BLL.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
            lblLoaiHinh.Text = TKMD.MaLoaiHinh + stlh;

            //6. Giay phep
            if (TKMD.SoGiayPhep != "")
                lblGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
            else
                lblGiayPhep.Text = "";
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayGiayPhep.Text = "";
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHetHanGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayHetHanGiayPhep.Text = "";

            //7. Hop dong
            if (this.TKMD.SoHopDong.Length > 36)
                lblHopDong.Font = new Font("Times New Roman", 6.5f);
            lblHopDong.Text = "" + this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHopDong.Text = " ";
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHetHanHopDong.Text = "" + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHetHanHopDong.Text = " ";

            //8. Hoa don thuong mai
            lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai > minDate)
                lblNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
            else
                lblNgayHDTM.Text = "";

            //9. Cang xep hang
            lblMaCuaKhauXepHang.Text = TKMD.CuaKhau_ID;
            lblCuaKhauXepHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);

            //10. Nuoc nhap khau
            lblNuocNK.Text = this.TKMD.NuocNK_ID;
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocNK_ID);

            //11. Dieu kien giao hang
            lblDieuKienGiaoHang.Text = this.TKMD.DKGH_ID;

            //12. Phuong thuc thanh toan
            lblPhuongThucThanhToan.Text = this.TKMD.PTTT_ID;

            //13. Dong tien thanh toan
            lblDongTienThanhToan.Text = this.TKMD.NguyenTe_ID;

            //14. Ty gia tinh thue
            lblTyGiaTinhThue.Text = this.TKMD.TyGiaTinhThue.ToString("G10");

            //15. Ket qua phan luong
            if (TKMD.PhanLuong == "1")
                lblLyDoTK.Text = "Tờ khai được thông quan";
            else if (TKMD.PhanLuong == "2")
                lblLyDoTK.Text = "Tờ khai phải xuất trình giấy tờ";
            else if (TKMD.PhanLuong == "3")
                lblLyDoTK.Text = "Tờ khai phải kiểm hóa";
            else
                lblLyDoTK.Text = "";
            lblHuongDan.Text = TKMD.HUONGDAN;

            //Tong so CONTAINER
            string tsContainer = "";
            string cont20 = "", cont40 = "", soKien = "";
            if (TKMD.SoContainer20 > 0)
            {
                cont20 = "Cont20: " + TKMD.SoContainer20;

                tsContainer += cont20 + "; ";
            }
            else
                cont20 = "";

            if (TKMD.SoContainer40 > 0)
            {
                cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                tsContainer += cont40 + "; ";
            }
            else
                cont40 = "";

            if (TKMD.SoKien > 0)
            {
                soKien = "Tổng số kiện: " + TKMD.SoKien;

                tsContainer += soKien;
            }
            else
                soKien = "";
            lblTongSoContainer.Text = tsContainer.Length > 0 ? "Tổng số container: " + tsContainer : "Tổng số container:";

            //27. Tong trong luong
            if (TKMD.TrongLuong > 0)
                lbltongtrongluong.Text = TKMD.TrongLuong + " kg ";
            else
                lbltongtrongluong.Text = "";

            //Chi Tiet phu luc dinh kem
            //if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
            //    lblChitietCon.Text = "danh sách container theo bảng kê đính kèm";
            //else
            //{
            //    string soHieuKienCont = "";
            //    Company.KDT.SHARE.QuanLyChungTu.Container objCont = null;

            //    if (TKMD.VanTaiDon == null)
            //    {
            //        System.Data.DataTable dt = TKMD.GetContainerInfo(TKMD.ID);

            //        if (dt != null && dt.Rows.Count > 0)
            //        {
            //            for (int cont = 0; cont < dt.Rows.Count; cont++)
            //            {
            //                System.Data.DataRow row = dt.Rows[cont];

            //                soHieuKienCont += row["SoHieu"].ToString() + "/" + row["Seal_No"].ToString();

            //                if (cont < dt.Rows.Count - 1)
            //                    soHieuKienCont += "; ";
            //            }
            //        }
            //    }
            //    else if (TKMD.VanTaiDon != null)
            //    {
            //        for (int cont = 0; cont < TKMD.VanTaiDon.ContainerCollection.Count; cont++)
            //        {
            //            objCont = TKMD.VanTaiDon.ContainerCollection[cont];

            //            soHieuKienCont += objCont.SoHieu + "/" + objCont.Seal_No;

            //            if (cont < TKMD.VanTaiDon.ContainerCollection.Count - 1)
            //                soHieuKienCont += "; ";
            //        }
            //    }

            //    lblChitietCon.Text = soHieuKienCont;

            if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                lblChitietCon.Text = "chi tiết phụ lục đính kèm";
            else
            {
                //DATLMQ bo sung So hieu kien, cont 26/10/2010
                if (TKMD.VanTaiDon != null)
                {
                    if (TKMD.VanTaiDon.ContainerCollection.Count != 0)
                    {
                        for (int i = 0; i < TKMD.VanTaiDon.ContainerCollection.Count; i++)
                        {
                            if (i == TKMD.VanTaiDon.ContainerCollection.Count - 1)
                                lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No;
                            else
                                lblChitietCon.Text += TKMD.VanTaiDon.ContainerCollection[i].SoHieu + "/" + TKMD.VanTaiDon.ContainerCollection[i].Seal_No + ";";
                        }
                    }
                    else
                    {
                        lblChitietCon.Text = "";
                    }
                }
                else
                {
                    lblChitietCon.Text = "";
                }
            }
            lblChitietCon.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.WebService.LoadConfigure("FontReport")));
            //}

            //Chi Phi Khac
            string st = "";
            if (this.TKMD.PhiBaoHiem > 0)
                st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
            if (this.TKMD.PhiVanChuyen > 0)
                st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
            if (this.TKMD.PhiKhac > 0)
                st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
            if (GlobalSettings.ChiPhiKhac == 1)
                lblPhiBaoHiem.Text = st;

            //THONG TIN HANG
            if (this.TKMD.HMDCollection.Count <= 3)
            {
                #region Chi tiet hang hoa
                if (this.TKMD.HMDCollection.Count >= 1)
                {
                    #region THONG TIN HANG
                    HangMauDich hmd = this.TKMD.HMDCollection[0];
                    if (!inMaHang)
                    {
                        if (hmd.Ma_HTS.Trim().Length > 0)
                            TenHang1.Text = hmd.Ma_HTS + ";" + hmd.TenHang;
                        else
                            TenHang1.Text = hmd.TenHang;
                    }
                    else
                    {
                        //if (hmd.MaPhu.Trim().Length > 0)
                        //    TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        //else
                        //    TenHang1.Text = hmd.TenHang;
                        if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                            TenHang1.Text = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                        else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                            TenHang1.Text = hmd.Ma_HTS + ";" + hmd.TenHang;
                        else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                            TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            TenHang1.Text = hmd.TenHang;
                    }

                    TenHang1.WordWrap = true;
                    try
                    {
                        TenHang1.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }

                    MaHS1.Text = hmd.MaHS;
                    XuatXu1.Text = hmd.NuocXX_ID;
                    Luong1.Text = hmd.SoLuong.ToString("G15");
                    DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);

                    DonGiaNT1.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT1.Text = hmd.TriGiaKB.ToString("N2");
                    TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueTuyetDoi)
                    //{
                    //    ThueSuatXNK1.Text = "";
                    //}
                    //else
                    //{
                    //if (hmd.ThueSuatXNKGiam.Trim() == "")
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK1.Text = hmd.ThueSuatXNKGiam;
                    //}
                    TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                    tongTienThueXNK += hmd.ThueXNK;

                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                        //if (hmd.ThueSuatVATGiam.Trim() == "")
                        //    ThueSuatGTGT1.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    ThueSuatGTGT1.Text = hmd.ThueSuatVATGiam;
                        //TienThueGTGT1.Text = hmd.ThueGTGT.ToString("N0");
                        TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                        //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                        //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam;
                        //TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                        TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                        //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                        //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam;
                        //TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        //    TyLeThuKhac1.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    TyLeThuKhac1.Text = hmd.ThueSuatVATGiam;
                        TriGiaThuKhac1.Text = hmd.ThueGTGT.ToString("N0");
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB;// +hmd.ThueGTGT;
                    #endregion
                }
                if (MienThue1)
                {
                    TriGiaTT1.Text = "";
                    ThueSuatXNK1.Text = "";
                    TienThueXNK1.Text = "";
                }
                if (MienThue2)
                {

                }

                //}
                if (this.TKMD.HMDCollection.Count >= 2)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[1];
                    //if (!inMaHang)
                    //    TenHang2.Text = hmd.TenHang;
                    //else
                    //{
                    //    if (hmd.MaPhu.Trim().Length > 0)
                    //        TenHang2.Text = hmd.TenHang + "/" + hmd.MaPhu;
                    //    else
                    //        TenHang2.Text = hmd.TenHang;
                    //}
                    if (!inMaHang)
                    {
                        if (hmd.Ma_HTS.Trim().Length > 0)
                            TenHang2.Text = hmd.Ma_HTS + ";" + hmd.TenHang;
                        else
                            TenHang2.Text = hmd.TenHang;
                    }
                    else
                    {
                        //if (hmd.MaPhu.Trim().Length > 0)
                        //    TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        //else
                        //    TenHang1.Text = hmd.TenHang;
                        if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                            TenHang2.Text = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                        else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                            TenHang2.Text = hmd.Ma_HTS + ";" + hmd.TenHang;
                        else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                            TenHang2.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            TenHang2.Text = hmd.TenHang;
                    }

                    TenHang2.WordWrap = true;
                    try
                    {
                        TenHang2.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }

                    MaHS2.Text = hmd.MaHS;
                    XuatXu2.Text = hmd.NuocXX_ID;
                    Luong2.Text = hmd.SoLuong.ToString("G15");
                    DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);

                    DonGiaNT2.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT2.Text = hmd.TriGiaKB.ToString("N2");
                    TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueTuyetDoi)
                    //{
                    //    ThueSuatXNK2.Text = "";
                    //}
                    //else
                    //{
                    //if (hmd.ThueSuatXNKGiam.Trim() == "")
                    //    ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK2.Text = hmd.ThueSuatXNKGiam;
                    //}
                    TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                    tongTienThueXNK += hmd.ThueXNK;
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                        //if (hmd.ThueSuatVATGiam.Trim() == "")
                        //    ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    ThueSuatGTGT2.Text = hmd.ThueSuatVATGiam;
                        //TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");
                        TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                        //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                        //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                        //TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                        TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                        //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                        //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                        //TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        //    TyLeThuKhac2.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    TyLeThuKhac2.Text = hmd.ThueSuatVATGiam;
                        TriGiaThuKhac2.Text = hmd.ThueGTGT.ToString("N0");
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }
                    //}
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac;// +hmd.ThueTTDB + hmd.ThueGTGT;
                }

                if (this.TKMD.HMDCollection.Count == 3)
                {
                    HangMauDich hmd = this.TKMD.HMDCollection[2];
                    //if (!inMaHang)
                    //    TenHang3.Text = hmd.TenHang;
                    //else
                    //{
                    //    if (hmd.MaPhu.Trim().Length > 0)
                    //        TenHang3.Text = hmd.TenHang + "/" + hmd.MaPhu;
                    //    else
                    //        TenHang3.Text = hmd.TenHang;
                    //}
                    if (!inMaHang)
                    {
                        if (hmd.Ma_HTS.Trim().Length > 0)
                            TenHang3.Text = hmd.Ma_HTS + ";" + hmd.TenHang;
                        else
                            TenHang3.Text = hmd.TenHang;
                    }
                    else
                    {
                        //if (hmd.MaPhu.Trim().Length > 0)
                        //    TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        //else
                        //    TenHang1.Text = hmd.TenHang;
                        if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length > 0)
                            TenHang3.Text = hmd.Ma_HTS + ";" + hmd.TenHang + "/" + hmd.MaPhu;
                        else if (hmd.Ma_HTS.Trim().Length > 0 && hmd.MaPhu.Trim().Length <= 0)
                            TenHang3.Text = hmd.Ma_HTS + ";" + hmd.TenHang;
                        else if (hmd.Ma_HTS.Trim().Length <= 0 && hmd.MaPhu.Trim().Length > 0)
                            TenHang3.Text = hmd.TenHang + "/" + hmd.MaPhu;
                        else
                            TenHang3.Text = hmd.TenHang;
                    }

                    TenHang3.WordWrap = true;
                    try
                    {
                        TenHang3.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                    }
                    catch
                    {
                        MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    }

                    MaHS3.Text = hmd.MaHS;
                    XuatXu3.Text = hmd.NuocXX_ID;
                    Luong3.Text = hmd.SoLuong.ToString("G15");
                    DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);

                    DonGiaNT3.Text = hmd.DonGiaKB.ToString("G10");
                    TriGiaNT3.Text = hmd.TriGiaKB.ToString("N2");
                    TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.ThueTuyetDoi)
                    //{
                    //    ThueSuatXNK3.Text = "";
                    //}
                    //else
                    //{
                    //if (hmd.ThueSuatXNKGiam.Trim() == "")
                    //    ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");
                    //else
                    //    ThueSuatXNK3.Text = hmd.ThueSuatXNKGiam;
                    //}
                    TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                    tongTienThueXNK += hmd.ThueXNK;
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                        //if (hmd.ThueSuatVATGiam.Trim() == "")
                        //    ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    ThueSuatGTGT3.Text = hmd.ThueSuatVATGiam;
                        //TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");
                        TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                        //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                        //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                        //TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                        TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                        TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        //TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                        //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                        //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                        //else
                        //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                        //TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                        //if (hmd.ThueSuatVATGiam.Length == 0)
                        //    TyLeThuKhac3.Text = hmd.ThueSuatGTGT.ToString("N0");
                        //else
                        //    TyLeThuKhac3.Text = hmd.ThueSuatVATGiam;
                        TriGiaThuKhac3.Text = hmd.ThueGTGT.ToString("N0");
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }
                    //}
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);

                    tongTienThueTatCa += hmd.ThueXNK + hmd.TriGiaThuKhac;// +hmd.ThueTTDB + hmd.ThueGTGT;
                }
                #endregion
            }
            //CHI TIET DINH KEM
            else
            {
                #region CHI TIET DINH KEM
                TenHang1.Text = "HÀNG HÓA XUẤT";
                TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                {
                    //if (!hmd.FOC)
                    //{
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, 2, MidpointRounding.AwayFromZero);
                    tongTienThueXNK += hmd.ThueXNK;
                    tongTriGiaTT += hmd.TriGiaTT;
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                    {
                        tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                    }

                    //}
                }
                //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);


                //Khi co phu luc thi khong hien thi :
                //TriGiaNT1.Text = tongTriGiaNT.ToString("N2");

                //TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                //TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
                //TriGiaTTGTGT1.Text = tongTriGiaTTGTGT.ToString("N0");
                //TienThueGTGT1.Text = tongThueGTGT.ToString("N0");
                //TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");

                lblTongThueXNK.Text = lblTongTriGiaThuKhac.Text = "";
                #endregion
            }

            tongTriGiaNT += Convert.ToDecimal(this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");

            lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
            //if (tongThueGTGT > 0)
            //    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            //else
            //    lblTongTienThueGTGT.Text = "";
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");

            string s = Company.GC.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            if (MienThue1)
            {
                TriGiaTT1.Text = "";
                ThueSuatXNK1.Text = "";
                TienThueXNK1.Text = "";
                TriGiaTT2.Text = "";
                ThueSuatXNK2.Text = "";
                TienThueXNK2.Text = "";
                TriGiaTT3.Text = "";
                ThueSuatXNK3.Text = "";
                TienThueXNK3.Text = "";
                lblTongThueXNK.Text = "";
            }
            //if (MienThue2)
            //{
            //    TriGiaTTGTGT1.Text = "";
            //    ThueSuatGTGT1.Text = "";
            //    TienThueGTGT1.Text = "";
            //    TriGiaTTGTGT2.Text = "";
            //    ThueSuatGTGT2.Text = "";
            //    TienThueGTGT2.Text = "";
            //    TriGiaTTGTGT3.Text = "";
            //    ThueSuatGTGT3.Text = "";
            //    TienThueGTGT3.Text = "";
            //    lblTongTienThueGTGT.Text = "";
            //}
            if (MienThue1 && MienThue2)
            {
                lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
            }

            if (this.TKMD.HMDCollection.Count > 3)
            {
                lblTongThueXNK.Text = lblTongTriGiaThuKhac.Text = "";
            }


            //Ngay thang nam in to khai
            if (TKMD.NgayTiepNhan == new DateTime(1900, 1, 1))
                xrLabel38.Text = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
            else
                xrLabel38.Text = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;

            //Đề xuất khác
            lblDeXuatKhac.Text = "28. Ghi chép khác: " + TKMD.DeXuatKhac;
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                tong += hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB;// + hmd.ThueGTGT
            }
            return tong;
        }
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            //lblMienThue2.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKChu.Visible = !t;//xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        public static string deXuatKhac = "";

        public void setDeXuatKhac(string _deXuatKhac)
        {
            lblDeXuatKhac.Text = "28. Ghi chép khác: " + _deXuatKhac;
        }

        public void setLuong1(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        public void setLuong2(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }
        public void setLuong3(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TriGiaTT1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void Luong1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            //XRControl cell = (XRControl)sender;
            //report.Cell = cell;
            //report.txtTenNhomHang.Text = cell.Text;
            //report.label3.Text = cell.Tag.ToString();
        }

        private void Luong2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void Luong3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            //XRControl cell = (XRControl)sender;
            //report.Cell = cell;
            //report.txtTenNhomHang.Text = cell.Text;
            //report.label3.Text = cell.Tag.ToString();
        }

        //private void xrTableCell2_PreviewClick(object sender, PreviewMouseEventArgs e)
        //{
        //    XRControl cell = (XRControl)sender;
        //    report.Cell = cell;
        //    report.txtDeXuatKhac.Text = cell.Text;
        //}

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lbltongtrongluong_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void xrLabel38_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblDeXuatKhac_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtDeXuatKhac.Text = cell.Text;
        }

        //public void BindData()
        //{
        //    DateTime minDate = new DateTime(1900, 1, 1);            
        //    //BindData of Detail
        //    lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
        //    lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
        //    lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
        //    lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
        //    lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
        //    lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
        //    lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
        //    lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

        //    //BindData header
        //    lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
        //    lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
        //    //lblChiCucHQ.Text
        //    //lblChiCucHQCK.Text
        //    lblDieuKienGiaoHang.Text = DKGH;
        //    lblDongTienThanhToan.Text = DongTienTT;
        //    lblGiayPhep.Text = TKMD.SoGiayPhep;
        //    lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
        //    lblHopDong.Tag = TKMD.SoHopDong;
        //    lblLoaiHinh.Text =  TKMD.LoaiVanDon;
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    lblNguoiNK.Text = TKMD.TenChuHang;
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;
        //    lblNguoiXK.Text = TKMD.TenDonViDoiTac;
        //    lblNuocXK.Text = NuocXK;
        //    lblPhuongThucThanhToan.Text = PTTT_Name;
        //    lblPhuongTienVanTai.Text = PTVT_Name;
        //    //lblThamChieu.Text
        //    lblToKhai.Text = TKMD.SoToKhai.ToString();
        //    lblVanTaiDon.Text = TKMD.LoaiVanDon;
        //    lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
        //    lblHuongDan.Text = TKMD.HUONGDAN;
        //    string ctu = "";
        //    foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
        //    {
        //        ctu += ct.TenChungTu + ", ";
        //    }
        //    lblChungTu.Text = ctu;

        //}
    }
}
