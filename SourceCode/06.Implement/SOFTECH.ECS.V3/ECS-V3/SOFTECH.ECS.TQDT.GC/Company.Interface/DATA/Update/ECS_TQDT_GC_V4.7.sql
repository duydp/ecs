﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/14/2012 10:08:23 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Delete]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Insert]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Load]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Update]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Delete]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
	@ID char(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Insert]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViHaiQuan] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Load]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
	@ID char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]	



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DonViHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Update]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DonViHaiQuan]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID



GO

PRINT N'Creating [dbo].[p_GC_Convert_MaHS8SoAuto]'
GO
CREATE PROC [dbo].[p_GC_Convert_MaHS8SoAuto]  
(  
 @MaHaiQuan VARCHAR(10),  
 @MaDoanhNghiep VARCHAR(20)
)  
AS  
begin  
  
/*Updated by HungTQ, 13/09/2012*/  
  
--NPL
Update dbo.t_KDT_GC_NguyenPhuLieu SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_NguyenPhuLieu SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--SP
Update dbo.t_KDT_GC_SanPham SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_SanPham SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--TB
Update dbo.t_KDT_GC_ThietBi SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_ThietBi SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--Hang mau
Update dbo.t_KDT_GC_HangMau SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_HangMau SET MaHS = LEFT(MaHS, 8) WHERE HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--NPL Cung ung
Update dbo.t_KDT_GC_NPLCungUng SET MaHS = LEFT(MaHS, 8) WHERE TKMDID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)

Update dbo.t_KDT_HangMauDich SET MaHS = LEFT(MaHS, 8) WHERE TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDich_BanLuu SET MaHS = LEFT(MaHS, 8) WHERE TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDichCheXuat SET MaHS = LEFT(MaHS, 8) WHERE TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_GC_HangChuyenTiep SET MaHS = LEFT(MaHS, 8)WHERE Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_ToKhaiChuyenTiep where MaHaiQuanTiepNhan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep) 
Update dbo.t_KDT_GC_HangPhuKien SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_GC_HangGSTieuHuy SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_GC_HangThanhKhoan SET MaHS = LEFT(MaHS, 8)

--Update HS 8 so hang trong Chung tu kem
Update dbo.t_KDT_HangGiayPhepDetail SET MaHS = LEFT(MaHS, 8) WHERE GiayPhep_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_GiayPhep where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HangVanDonDetail SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_HangCoDetail SET MaHS = LEFT(MaHS, 8)
Update dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = LEFT(MaHS, 8) WHERE HoaDonTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HoaDon where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = LEFT(MaHS, 8) WHERE HopDongTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HopDong where MaDoanhNghiep = @MaDoanhNghiep)  
  
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_Convert_MaHS8So]'
GO

create PROC [dbo].[p_GC_Convert_MaHS8So]
(
	@MaHaiQuan	VARCHAR(10),
	@MaDoanhNghiep	VARCHAR(20),
	@Ma	NVARCHAR(250),
	@DVT_ID	NVARCHAR(50),
	@MaHSCu	VARCHAR(10),
	@MaHSMoi	VARCHAR(10)
)
AS
begin

/*Updated by HungTQ, 13/09/2012*/

--NPL
Update dbo.t_KDT_GC_NguyenPhuLieu SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_NguyenPhuLieu SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--SP
Update dbo.t_KDT_GC_SanPham SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_SanPham SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--TB
Update dbo.t_KDT_GC_ThietBi SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_ThietBi SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--Hang mau
Update dbo.t_KDT_GC_HangMau SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_GC_HangMau SET MaHS = @MaHSMoi WHERE Ma = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDong_ID IN (SELECT ID FROM dbo.t_KDT_GC_HopDong where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
--NPL Cung ung
Update dbo.t_KDT_GC_NPLCungUng SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMDID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)

Update dbo.t_KDT_HangMauDich SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDich_BanLuu SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_HangMauDichCheXuat SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  TKMD_ID IN (SELECT ID FROM dbo.t_KDT_ToKhaiMauDich where MaHaiQuan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep)
Update dbo.t_KDT_GC_HangChuyenTiep SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND ID_DVT = @DVT_ID AND MaHS = @MaHSCu AND Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_ToKhaiChuyenTiep where MaHaiQuanTiepNhan = @MaHaiQuan AND MaDoanhNghiep = @MaDoanhNghiep) 
Update dbo.t_KDT_GC_HangPhuKien SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_GC_HangGSTieuHuy SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_GC_HangThanhKhoan SET MaHS = @MaHSMoi WHERE MaHang = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 

--Update HS 8 so hang trong Chung tu kem
Update dbo.t_KDT_HangGiayPhepDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  GiayPhep_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_GiayPhep where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HangVanDonDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_HangCoDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu 
Update dbo.t_KDT_HoaDonThuongMaiDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HoaDonTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HoaDon where MaDoanhNghiep = @MaDoanhNghiep)   
Update dbo.t_KDT_HopDongThuongMaiDetail SET MaHS = @MaHSMoi WHERE MaPhu = @Ma AND DVT_ID = @DVT_ID AND MaHS = @MaHSCu AND  HopDongTM_ID IN (SELECT ID FROM dbo.t_KDT_ChungTuKem_HopDong where MaDoanhNghiep = @MaDoanhNghiep)  

end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_SelectHang]'
GO
create PROC [dbo].[p_GC_SelectHang]      
(      
 @MaHaiQuan VARCHAR(10),      
 @MaDoanhNghiep VARCHAR(20),      
 @MaLoaiHinh VARCHAR(5)      
)      
AS      
      
/*Updateed by Hungtq, 08/09/2012*/      
      
DECLARE @sql VARCHAR(max), @TableName VARCHAR(50)     
      
SET @sql =      
' SELECT distinct t.MaHaiQuan, t.MaDoanhNghiep, h.MaPhu AS Ma, h.TenHang AS Ten, h.MaHS, h.DVT_ID '      
+ ' FROM dbo.t_KDT_ToKhaiMauDich t INNER JOIN dbo.t_KDT_HangMauDich h '      
+ ' ON t.ID = h.TKMD_ID '      
+ ' WHERE t.MaHaiQuan = ''' + @MaHaiQuan + ''' AND t.MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''      
+ ' AND t.TrangThaiXuLy = 1 AND t.MaLoaiHinh LIKE ''' + @MaLoaiHinh + '%'''      
    
SET @sql = @sql + ' UNION '      
    
IF(@MaLoaiHinh = 'N')        
 SET @TableName = 't_GC_NguyenPhuLieu'    
IF(@MaLoaiHinh = 'X')        
 SET @TableName = 't_GC_SanPham'    
IF(@MaLoaiHinh = 'T')        
 SET @TableName = 't_GC_ThietBi'    
    
SET @sql = @sql     
+ ' SELECT DISTINCT MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID '    
+ ' FROM dbo.' + @TableName + ' d INNER JOIN dbo.t_KDT_GC_HopDong h ON d.HopDong_ID = h.ID '
+ ' WHERE MaHaiQuan = ''' + @MaHaiQuan + ''' AND MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''    
    
SET @sql = 'SELECT distinct MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID from (' + @sql + ') as v'    

--PRINT @sql      
EXEC (@sql)      
      
--exec p_GC_SelectHang 'C34C', '0100101308', 'N'      
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.7') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.7', getdate(), N'Cập nhật cấu trúc dữ liệu.')
	end 
	
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
