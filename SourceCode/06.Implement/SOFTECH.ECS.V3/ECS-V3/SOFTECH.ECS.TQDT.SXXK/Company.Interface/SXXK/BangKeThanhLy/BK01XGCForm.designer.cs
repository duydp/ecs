namespace Company.Interface.SXXK.BangKe
{
    partial class BK01XGCForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKNTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BK01XGCForm));
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnBack = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnFinish = new Janus.Windows.EditControls.UIButton();
            this.btnNext = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgTKNTL = new Janus.Windows.GridEX.GridEX();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.label5);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.btnNext);
            this.grbMain.Controls.Add(this.btnBack);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnFinish);
            this.grbMain.Size = new System.Drawing.Size(740, 423);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBack.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBack.Icon = ((System.Drawing.Icon)(resources.GetObject("btnBack.Icon")));
            this.btnBack.Location = new System.Drawing.Point(318, 393);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(105, 23);
            this.btnBack.TabIndex = 3;
            this.btnBack.Text = "Trở lại";
            this.btnBack.VisualStyleManager = this.vsmMain;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(652, 393);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(76, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnFinish
            // 
            this.btnFinish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnFinish.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFinish.Icon = ((System.Drawing.Icon)(resources.GetObject("btnFinish.Icon")));
            this.btnFinish.Location = new System.Drawing.Point(541, 393);
            this.btnFinish.Name = "btnFinish";
            this.btnFinish.Size = new System.Drawing.Size(105, 23);
            this.btnFinish.TabIndex = 5;
            this.btnFinish.Text = "Hoàn thành";
            this.btnFinish.VisualStyleManager = this.vsmMain;
            this.btnFinish.Click += new System.EventHandler(this.btnFinish_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.Enabled = false;
            this.btnNext.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Icon = ((System.Drawing.Icon)(resources.GetObject("btnNext.Icon")));
            this.btnNext.Location = new System.Drawing.Point(430, 393);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(105, 23);
            this.btnNext.TabIndex = 4;
            this.btnNext.Text = "Tiếp tục";
            this.btnNext.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgTKNTL);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 26);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(716, 361);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // dgTKNTL
            // 
            this.dgTKNTL.AlternatingColors = true;
            this.dgTKNTL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgTKNTL.AutomaticSort = false;
            this.dgTKNTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKNTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKNTL.ColumnAutoResize = true;
            dgTKNTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKNTL_DesignTimeLayout.LayoutString");
            this.dgTKNTL.DesignTimeLayout = dgTKNTL_DesignTimeLayout;
            this.dgTKNTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKNTL.GroupByBoxVisible = false;
            this.dgTKNTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKNTL.ImageList = this.ImageList1;
            this.dgTKNTL.Location = new System.Drawing.Point(2, 8);
            this.dgTKNTL.Name = "dgTKNTL";
            this.dgTKNTL.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgTKNTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKNTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKNTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKNTL.Size = new System.Drawing.Size(713, 352);
            this.dgTKNTL.TabIndex = 0;
            this.dgTKNTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKNTL.VisualStyleManager = this.vsmMain;
            this.dgTKNTL.RecordUpdated += new System.EventHandler(this.dgTKNTL_RecordUpdated);
            this.dgTKNTL.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgTKNTL_LoadingRow);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label5.Location = new System.Drawing.Point(12, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(289, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Danh sách tờ khai nhập đưa vào thanh khoản";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(12, 393);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(300, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ghi chú: Kích đôi vào tờ khai để xem thông tin chi tiết tờ khai";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // BK01XGCForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(740, 423);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BK01XGCForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Bảng kê tờ khai nhập đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.BK01WizardForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIButton btnBack;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnFinish;
        private Janus.Windows.EditControls.UIButton btnNext;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.GridEX dgTKNTL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
    }
}