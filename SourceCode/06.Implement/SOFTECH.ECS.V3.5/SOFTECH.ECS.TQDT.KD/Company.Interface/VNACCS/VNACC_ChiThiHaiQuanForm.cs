﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ChiThiHaiQuanForm : BaseFormHaveGuidPanel
    {
        // public ELoaiThongTin LoaiThongTin = ELoaiThongTin.TK;
        public long Master_ID;
        public ELoaiThongTin LoaiThongTin;
        public KDT_VNACC_ToKhaiMauDich TKMD = null;
        private List<KDT_VNACC_ChiThiHaiQuan> ListChiThiHQ = new List<KDT_VNACC_ChiThiHaiQuan>();
        private KDT_VNACC_ChiThiHaiQuan CTHQ = null;

        public VNACC_ChiThiHaiQuanForm()
        {
            InitializeComponent();
        }
       
        private void btnGhi_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                if (CTHQ == null || CTHQ.ID == 0)
                {
                    CTHQ = new KDT_VNACC_ChiThiHaiQuan();
                }
                GetChiThi();
                CTHQ.Master_ID = Master_ID;
                if (CTHQ.Ten == "")
                    ShowMessage("Chưa nhập thông tin chỉ thị", false);
                else
                    CTHQ.InsertUpdate();
                CTHQ = new KDT_VNACC_ChiThiHaiQuan();
                SetChiThi();
                CTHQ = null;

                LoadData(Master_ID, LoaiThongTin);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            
        }

        private void GetChiThi()
        {
            try
            {
                CTHQ.PhanLoai = "";//ctrPhanLoai.Code;
                CTHQ.Ngay = Convert.ToDateTime(cclNgayChiThi.Value);
                CTHQ.Ten = txtTenChiThi.Text;
                CTHQ.NoiDung = txtNoiDung.Text;
                CTHQ.LoaiThongTin = "TK";
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SetChiThi()
        {
            try
            {
                if (CTHQ != null)
                {
                    //ctrPhanLoai.Code = CTHQ.PhanLoai;
                    cclNgayChiThi.Value = CTHQ.Ngay;
                    txtTenChiThi.Text = CTHQ.Ten;
                    txtNoiDung.Text = CTHQ.NoiDung;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void VNACC_ChiThiHaiQuanForm_Load(object sender, EventArgs e)
        {
            btnGhi.Visible = false;
            btnXoa.Visible = false;
            try
            {
                Cursor = Cursors.WaitCursor;
                
                LoadData(Master_ID, LoaiThongTin);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

        }

        private void LoadData(long master_id, ELoaiThongTin loaithongtin)
        {
            try
            {
                string where = string.Format("Master_id = {0} and LoaiThongTin = '{1}'", master_id, loaithongtin.ToString());
                ListChiThiHQ = KDT_VNACC_ChiThiHaiQuan.SelectCollectionDynamic(where, "");
                grList.DataSource = ListChiThiHQ;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            
           /* try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                //List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa chỉ thị này này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ListChiThiHQ.Add((KDT_VNACC_ChiThiHaiQuan)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_ChiThiHaiQuan item in ListChiThiHQ)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.ChiThiHQCollection.Remove(item);
                    }

                    grList.DataSource = TKMD.ChiThiHQCollection;
                    grList.Refetch();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            */
            
        }

        private void grList_SelectionChanged(object sender, EventArgs e)
        {
            /*try
            {
                Cursor = Cursors.WaitCursor;

                CTHQ = grList.CurrentRow.DataRow as KDT_VNACC_ChiThiHaiQuan;

                SetChiThi();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
             */
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    CTHQ = (KDT_VNACC_ChiThiHaiQuan)grList.CurrentRow.DataRow;
                    SetChiThi();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            

        }
    }
}
