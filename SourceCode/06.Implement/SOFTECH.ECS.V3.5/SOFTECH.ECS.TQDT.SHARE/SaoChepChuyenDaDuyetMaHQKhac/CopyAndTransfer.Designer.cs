﻿namespace SaoChepChuyenDaDuyetMaHQKhac
{
    partial class CopyAndTransfer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CopyAndTransfer));
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.grbMain = new Janus.Windows.EditControls.UIGroupBox();
            this.numNamDangKy = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.filterEditor1 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.donViHaiQuanControl1 = new SaoChepChuyenDaDuyetMaHQKhac.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.btnHuybo = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnReLoad = new Janus.Windows.EditControls.UIButton();
            this.btnThuchien = new Janus.Windows.EditControls.UIButton();
            this.officeFormAdorner1 = new Janus.Windows.Ribbon.OfficeFormAdorner(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            this.visualStyleManager1.DefaultColorScheme = "Office2007";
            // 
            // grbMain
            // 
            this.grbMain.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.grbMain.Controls.Add(this.numNamDangKy);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.filterEditor1);
            this.grbMain.Controls.Add(this.donViHaiQuanControl1);
            this.grbMain.Controls.Add(this.label7);
            this.grbMain.Controls.Add(this.lblInfo);
            this.grbMain.Controls.Add(this.btnHuybo);
            this.grbMain.Controls.Add(this.btnReLoad);
            this.grbMain.Controls.Add(this.btnThuchien);
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbMain.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.grbMain.Location = new System.Drawing.Point(0, 0);
            this.grbMain.Name = "grbMain";
            this.grbMain.Size = new System.Drawing.Size(677, 354);
            this.grbMain.TabIndex = 0;
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbMain.VisualStyleManager = this.visualStyleManager1;
            // 
            // numNamDangKy
            // 
            this.numNamDangKy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.numNamDangKy.Location = new System.Drawing.Point(539, 54);
            this.numNamDangKy.Name = "numNamDangKy";
            this.numNamDangKy.Size = new System.Drawing.Size(41, 20);
            this.numNamDangKy.TabIndex = 3;
            this.numNamDangKy.Text = "2,013";
            this.numNamDangKy.Value = 2013;
            this.numNamDangKy.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.numNamDangKy.VisualStyleManager = this.visualStyleManager1;
            this.numNamDangKy.Leave += new System.EventHandler(this.numNamDangKy_Leave);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(501, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Năm:";
            // 
            // filterEditor1
            // 
            this.filterEditor1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.filterEditor1.AutoApply = true;
            this.filterEditor1.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor1.DefaultConditionOperator = Janus.Data.ConditionOperator.BeginsWith;
            this.filterEditor1.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor1.Location = new System.Drawing.Point(3, 40);
            this.filterEditor1.MinSize = new System.Drawing.Size(16, 45);
            this.filterEditor1.Name = "filterEditor1";
            this.filterEditor1.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor1.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor1.ScrollStep = 15;
            this.filterEditor1.Size = new System.Drawing.Size(501, 45);
            this.filterEditor1.SourceControl = this.dgList;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(12, 91);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(653, 224);
            this.dgList.TabIndex = 5;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(227, 12);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(270, 22);
            this.donViHaiQuanControl1.TabIndex = 1;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(12, 18);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(209, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chọn hải quan khai báo khác cần chuyển:";
            // 
            // lblInfo
            // 
            this.lblInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblInfo.AutoSize = true;
            this.lblInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lblInfo.Location = new System.Drawing.Point(9, 327);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(22, 13);
            this.lblInfo.TabIndex = 5;
            this.lblInfo.Text = "[?]";
            // 
            // btnHuybo
            // 
            this.btnHuybo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHuybo.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnHuybo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHuybo.ImageIndex = 1;
            this.btnHuybo.ImageList = this.imageList1;
            this.btnHuybo.Location = new System.Drawing.Point(586, 321);
            this.btnHuybo.Name = "btnHuybo";
            this.btnHuybo.Size = new System.Drawing.Size(79, 24);
            this.btnHuybo.TabIndex = 7;
            this.btnHuybo.Text = "Hủy bỏ";
            this.btnHuybo.VisualStyleManager = this.visualStyleManager1;
            this.btnHuybo.Click += new System.EventHandler(this.btnHuybo_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "disk_multiple.png");
            this.imageList1.Images.SetKeyName(1, "clear.png");
            this.imageList1.Images.SetKeyName(2, "refresh_16.png");
            // 
            // btnReLoad
            // 
            this.btnReLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReLoad.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReLoad.ImageIndex = 2;
            this.btnReLoad.ImageList = this.imageList1;
            this.btnReLoad.Location = new System.Drawing.Point(586, 51);
            this.btnReLoad.Name = "btnReLoad";
            this.btnReLoad.Size = new System.Drawing.Size(79, 24);
            this.btnReLoad.TabIndex = 4;
            this.btnReLoad.Text = "Làm mới";
            this.btnReLoad.VisualStyleManager = this.visualStyleManager1;
            this.btnReLoad.Click += new System.EventHandler(this.btnReLoad_Click);
            // 
            // btnThuchien
            // 
            this.btnThuchien.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThuchien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThuchien.ImageIndex = 0;
            this.btnThuchien.ImageList = this.imageList1;
            this.btnThuchien.Location = new System.Drawing.Point(491, 321);
            this.btnThuchien.Name = "btnThuchien";
            this.btnThuchien.Size = new System.Drawing.Size(89, 24);
            this.btnThuchien.TabIndex = 6;
            this.btnThuchien.Text = "Thực hiện";
            this.btnThuchien.VisualStyleManager = this.visualStyleManager1;
            this.btnThuchien.Click += new System.EventHandler(this.btnThuchien_Click);
            // 
            // officeFormAdorner1
            // 
            this.officeFormAdorner1.Form = this;
            this.officeFormAdorner1.Office2007CustomColor = System.Drawing.Color.Empty;
            this.officeFormAdorner1.TitleBarFont = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // CopyAndTransfer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnHuybo;
            this.ClientSize = new System.Drawing.Size(677, 354);
            this.Controls.Add(this.grbMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CopyAndTransfer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sao chép và chuyển trạng thái tờ khai sang mã HẢI QUAN khác";
            this.Load += new System.EventHandler(this.CopyAndTransfer_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox grbMain;
        private Janus.Windows.Ribbon.OfficeFormAdorner officeFormAdorner1;
        public Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.EditControls.UIButton btnHuybo;
        private Janus.Windows.EditControls.UIButton btnThuchien;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.Label lblInfo;
        private System.Windows.Forms.Label label7;
        private DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor1;
        private Janus.Windows.EditControls.UIButton btnReLoad;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numNamDangKy;
        private System.Windows.Forms.Label label1;
    }
}

