﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.KDT.SXXK;

namespace Company.Interface
{
    public partial class TheoDoiThanhKhoanToKhaiForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public TheoDoiThanhKhoanToKhaiForm()
        {
            InitializeComponent();
        }

        private void TheoDoiThanhKhoanToKhaiForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["TonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["TonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            this.Text = "Theo dõi thanh khoản tờ khai số " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh.Trim() + "/" + TKMD.NgayDangKy.ToString("dd-MM-yyyy");
            NPLNhapTonCollection col = new NPLNhapTon().SelectCollectionDynamic("MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND SoToKhai =" + this.TKMD.SoToKhai + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "' AND NamDangKy =" + TKMD.NamDangKy + " AND TonCuoi < TonDau AND LanThanhLy IN (SELECT LanThanhLy FROM t_KDT_SXXK_HoSoThanhLyDangKy)", "");
            dgList.DataSource = col;
        }

        private void ExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Theo dõi thanh khoản tờ khai số " + TKMD.SoToKhai + "" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}