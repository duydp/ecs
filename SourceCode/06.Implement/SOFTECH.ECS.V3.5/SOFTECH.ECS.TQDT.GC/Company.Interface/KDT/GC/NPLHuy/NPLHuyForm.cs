﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.GC.BLL.GC;
namespace Company.Interface.KDT.GC
{
    public partial class NPLHuyForm : BaseForm
    {
        public NPLHuyDangKy NPLHuyDK = new NPLHuyDangKy();
        private NPLHuy NPLHuy = new NPLHuy();
        //private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        //bool isEdit = false;
        public bool isAuto = false;
        public DataTable tbNPLHuy = new DataTable();
        public HopDong HD = new HopDong();
        public NPLHuyForm()
        {
            InitializeComponent();
        }



        private void CungUngForm_Load(object sender, EventArgs e)
        {
            LoadData();
            //SetCommand();

        }
        private void setNPLHuyAuTo()
        {
            if (NPLHuyDK.ID == 0)
                NPLHuyDK.TrangThaiXuLy = -1;
            txtSoHopDong.Text = HD.SoHopDong;
            clcNgayTiepNhan.Value = NPLHuyDK.NgayTiepNhan;
            clcNgayHD.Value = HD.NgayKy;
            clcNgayHetHan.Value = HD.NgayHetHan;
            txtSoTiepNhan.Text = NPLHuyDK.SoTiepNhan.ToString();
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            foreach (DataRow dr in tbNPLHuy.Rows)
            {
                NPLHuy CU = new NPLHuy();
                CU.MaNPL = dr["Ma"].ToString();
                CU.TenNPL = dr["Ten"].ToString();
                CU.LuongNPLHuy = Convert.ToDecimal(dr["LuongTonCuoi"].ToString());
                //CU.ThanhTien = 0;
                CU.DVT_ID = dr["DVT"].ToString();
                NPLHuyDK.NPLHuy_List.Add(CU);
            }
            dgList.DataSource = NPLHuyDK.NPLHuy_List;
            //dgList.DataSource = KDT_GC_CungUng.SelectDynamic("Master_ID = " + CungUngDK.ID, "").Tables[0];
            dgList.Refetch();
        }
        private void LoadData()
        {
            if (isAuto)
            {
                setNPLHuyAuTo();
            }
            else
            {
                if (NPLHuyDK.ID == 0)
                    NPLHuyDK.TrangThaiXuLy = -1;
                txtSoHopDong.Text = HD.SoHopDong;
                clcNgayTiepNhan.Value = NPLHuyDK.NgayTiepNhan;
                clcNgayHD.Value = HD.NgayKy;
                clcNgayHetHan.Value = HD.NgayHetHan;
                txtSoTiepNhan.Text = NPLHuyDK.SoTiepNhan.ToString();
                if (NPLHuyDK.MaHQ == null || NPLHuyDK.MaHQ.Trim() == "")
                    ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                else
                    ctrMaHaiQuan.Code = NPLHuyDK.MaHQ;// CungUngDK.MaHQ;
                dgList.DataSource = NPLHuy.SelectDynamic("Master_ID = " + NPLHuyDK.ID, "").Tables[0];
                dgList.Refetch();
            }

        }
        private void SetCommand()
        {
            int TrangThai = NPLHuyDK.TrangThaiXuLy;
            if (NPLHuyDK.TrangThaiXuLy == -1 || NPLHuyDK.TrangThaiXuLy == 2)
            {
                cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (TrangThai == 2)
                    cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;
            }
            else
            {
                cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                if (TrangThai == 1)
                    cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                else
                    cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }


        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (isAuto)
            {
                int i = dgList.CurrentRow.Position;
                NPLHuyDK.NPLHuy_List.RemoveAt(i);
                dgList.DataSource = NPLHuyDK.NPLHuy_List;
            }
            else 
            {
                try
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    List<NPLHuy> ItemColl = new List<NPLHuy>();
                    if (dgList.GetRows().Length < 0) return;
                    if (items.Count <= 0) return;
                    if (ShowMessage("Bạn có muốn xóa NPL này không?", true) == "Yes")
                    {
                        foreach (GridEXSelectedItem i in items)
                        {
                            GridEXRow dr = i.GetRow();
                            long id = Int64.Parse(dr.Cells[1].Value.ToString());
                            NPLHuy.DeleteNPLHuy(id);

                        }
                        LoadData();
                        dgList.Refetch();
                    }

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            

        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdNPLCungUng":
                    ThemNPL();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdSend":
                    SendV5();
                    break;
                case "cmdFeedBack":
                    FeedBackV5();
                    break;
                case "cmdKetQuaXuLy":
                    KetQuaXuLyPK();
                    break;
                case "cmdHuyKhaiBao":
                    SendHuyV5();
                    break;
            }
        }
        private void SendHuyV5()
        {
            //if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tự cung ứng này ?", true) == "Yes")
            //{
            //    //   bool isKhaiSua = false;
            //    if (NPLHuyDK.ID == 0)
            //    {
            //        this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
            //        return;
            //    }
            //    else
            //    {
            //        NPLHuyDK = KDT_GC_CungUngDangKy.LoadFull(NPLHuyDK.ID);
            //        NPLHuyDK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
            //        //if (BKCungUng.TrangThaiXuLy == 5)
            //        //    isKhaiSua = true;
            //        //else
            //        //    isKhaiSua = false;
            //    }
            //    MsgSend sendXML = new MsgSend();
            //    sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
            //    sendXML.master_id = NPLHuyDK.ID;
            //    if (sendXML.Load())
            //    {
            //        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
            //        cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

            //        return;
            //    }
            //    try
            //    {
            //        if (this.NPLHuyDK.NPLCungUng_List.Count > 0)
            //        {
            //            string returnMessage = string.Empty;
            //            NPLHuyDK.GuidStr = Guid.NewGuid().ToString();
            //            //SXXK_TuCungUng TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferTuCungUng(BKCungUng, GlobalSettings.TEN_DON_VI, isKhaiSua);
            //            //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(BKCungUng, GlobalSettings.TEN_DON_VI);

            //            ObjectSend msgSend = CancelMessageV5(NPLHuyDK);
            //            //BKCungUng.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            //            SendMessageForm sendForm = new SendMessageForm();
            //            sendForm.Send += SendMessage;
            //            bool isSend = sendForm.DoSend(msgSend);
            //            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
            //            {
            //                sendForm.Message.XmlSaveMessage(NPLHuyDK.ID, "Hủy khai báo bảng kê");
            //                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //                cmdSave.Enabled = cmdSave1.Enabled = cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //                //btnDelete.Enabled = false;
            //                sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
            //                sendXML.master_id = NPLHuyDK.ID;
            //                sendXML.msg = msgSend.ToString();
            //                sendXML.func = 1;
            //                sendXML.InsertUpdate();

            //                NPLHuyDK.Update();
            //                FeedBackV5();
            //            }
            //            else if (!string.IsNullOrEmpty(msgInfor))
            //                ShowMessageTQDT(msgInfor, false);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.LocalLogger.Instance().WriteMessage(ex);
            //        
            //    }
            //}
        }
        public static ObjectSend CancelMessageV5(KDT_GC_CungUngDangKy BK)
        {
            string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
            string sfmtDate = "yyyy-MM-dd";
            //string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
            //bool IsToKhaiNhap = BK.MaLoaiHinh.StartsWith("N");
            BK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
            //bool IsHuyToKhai = (BK.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            bool IsHuyToKhai = true;
            string issuer = string.Empty;
            issuer = DeclarationIssuer.GC_TUCUNGUNG;

            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = BK.GuidStr,
                IssueLocation = string.Empty,
                Agents = new List<Agent>(),

                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(BK.SoTiepNhan, 0),
                Acceptance = BK.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = BK.MaHQ,
                Importer = new NameBase()
                {
                    Name = GlobalSettings.TEN_DON_VI,
                    Identity = BK.MaDoanhNghiep
                },
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.Agents.Add(new Agent()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "Xin hủy bảng kê NPL cung ứng do sai số liệu" }
                }
                );
            if (IsHuyToKhai)
            {
                //List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_BK_ID(BK.ID);
                //if (cancelContent.Count > 0)
                //{
                //    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
                //    declaredCancel.Reference = guidHuyTK;
                //}
            }
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN),
                Identity = BK.MaHQ.Trim()
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = declaredCancel.Reference //BK.GUIDSTR
            }, declaredCancel);
            return objSend;
        }
        private void ThemNPL()
        {
            NguyenPhuLieuRegistedForm NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            NPLRegistedForm.isBrower = true;
            NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = NPLHuyDK.HopDong_ID;
            NPLRegistedForm.ShowDialog();
            if (!string.IsNullOrEmpty(NPLRegistedForm.NguyenPhuLieuSelected.Ma))
            {

                NPLHuy.MaNPL = NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                NPLHuy.TenNPL = NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                NPLHuy.MaHS = NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                NPLHuy.DVT_ID = VNACCS_Mapper.GetCodeVNACC(NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                NPLHuyDetailsForm frm = new NPLHuyDetailsForm();
                frm.HuyNPL = NPLHuy;
                frm.NPLHuyDK = NPLHuyDK;
                frm.ShowDialog();
                LoadData();
                NPLHuy = new NPLHuy();
            }
            LoadData();
        }
        private void Save()
        {
            try
            {
                NPLHuyDK.NgayTiepNhan = clcNgayTiepNhan.Value;
                NPLHuyDK.SoHopDong = HD.SoHopDong;
                NPLHuyDK.HopDong_ID = HD.ID;
                NPLHuyDK.NgayHopDong = HD.NgayKy;
                NPLHuyDK.NgayHetHan = HD.NgayHetHan;
                NPLHuyDK.MaHQ = ctrMaHaiQuan.Code;
                NPLHuyDK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (NPLHuyDK.ID == 0)
                {
                    NPLHuyDK.TrangThaiXuLy = 1;
                    NPLHuyDK.Insert();
                }
                else
                {
                    NPLHuyDK.Update();
                }
                if (isAuto)
                    if (NPLHuyDK.NPLHuy_List.Count > 0)
                    {
                        foreach (var item in NPLHuyDK.NPLHuy_List)
                        {
                            item.Master_ID = NPLHuyDK.ID;
                            item.InsertUpdate();
                        }
                    }

                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void KetQuaXuLyPK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.NPLHuyDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_TUCUNGUNG;
            form.ShowDialog(this);
        }

        private void FeedBackV5()
        {
            //bool isFeedBack = true;
            //int count = Company.KDT.SHARE.Components.Globals.CountSend;
            ////MsgSend sendXML = new MsgSend();
            ////sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            ////sendXML.master_id = CungUngDK.ID;
            ////if (!sendXML.Load())
            ////{
            ////    ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
            ////    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ////    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            ////    return;
            ////}
            //while (isFeedBack)
            //{
            //    string reference = NPLHuyDK.GuidStr;

            //    SubjectBase subjectBase = new SubjectBase()
            //    {
            //        Issuer = DeclarationIssuer.GC_TUCUNGUNG,
            //        Reference = reference,
            //        Function = DeclarationFunction.HOI_TRANG_THAI,
            //        Type = DeclarationIssuer.GC_TUCUNGUNG,

            //    };

            //    ObjectSend msgSend = new ObjectSend(
            //                                new NameBase()
            //                                {
            //                                    Name = GlobalSettings.TEN_DON_VI,
            //                                    Identity = NPLHuyDK.MaDoanhNghiep
            //                                },
            //                                  new NameBase()
            //                                  {
            //                                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(NPLHuyDK.MaHQ.Trim())),
            //                                      //Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(CungUngDK.MaHQ).Trim() : CungUngDK.MaHQ
            //                                      Identity = NPLHuyDK.MaHQ
            //                                  }, subjectBase, null);
            //    SendMessageForm sendForm = new SendMessageForm();
            //    sendForm.Send += SendMessage;
            //    isFeedBack = sendForm.DoSend(msgSend);
            //    if (isFeedBack)
            //    {
            //        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
            //    }
            //}
            //NPLHuyDK = KDT_GC_CungUngDangKy.LoadFull(NPLHuyDK.ID);
            //LoadData();
        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            //feedbackContent = SingleMessage.NPLCungUngSendHandler(NPLHuyDK, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            //if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            //{
            //    if (NPLHuyDK.ID == 0)
            //    {
            //        this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
            //        return;
            //    }
            //    else
            //    {
            //        NPLHuyDK = KDT_GC_CungUngDangKy.LoadFull(NPLHuyDK.ID);
            //    }
            //    MsgSend sendXML = new MsgSend();
            //    sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
            //    sendXML.master_id = NPLHuyDK.ID;
            //    if (sendXML.Load())
            //    {
            //        ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
            //        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //        return;
            //    }
            //    try
            //    {
            //        if (this.NPLHuyDK.NPLCungUng_List.Count > 0)
            //        {

            //            string returnMessage = string.Empty;
            //            NPLHuyDK.GuidStr = Guid.NewGuid().ToString();
            //            GC_NPLCungUng nplCungUng = Mapper_V4.ToDataTransferTuCungUng(NPLHuyDK, GlobalSettings.TEN_DON_VI);

            //            ObjectSend msgSend = new ObjectSend(
            //                 new NameBase()
            //                 {
            //                     Name = GlobalSettings.TEN_DON_VI,
            //                     Identity = NPLHuyDK.MaDoanhNghiep
            //                 },
            //                  new NameBase()
            //                  {
            //                      Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(NPLHuyDK.MaHQ)),
            //                      // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(CungUngDK.MaHQ).Trim() : CungUngDK.MaHQ
            //                      Identity = NPLHuyDK.MaHQ
            //                  },
            //                  new SubjectBase()
            //                  {

            //                      Type = nplCungUng.Issuer,
            //                      Function = DeclarationFunction.KHAI_BAO,
            //                      Reference = NPLHuyDK.GuidStr,
            //                  },
            //                  nplCungUng
            //                );
            //            NPLHuyDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            //            SendMessageForm sendForm = new SendMessageForm();
            //            sendForm.Send += SendMessage;
            //            bool isSend = sendForm.DoSend(msgSend);
            //            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
            //            {
            //                sendForm.Message.XmlSaveMessage(NPLHuyDK.ID, MessageTitle.KhaiBaoNPLTuCungUng);
            //                cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //                cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //                btnXoa.Enabled = false;
            //                sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
            //                sendXML.master_id = NPLHuyDK.ID;
            //                //sendXML.msg = msgSend.
            //                sendXML.func = 1;
            //                sendXML.InsertUpdate();

            //                NPLHuyDK.Update();
            //                FeedBackV5();
            //            }
            //            else if (!string.IsNullOrEmpty(msgInfor))
            //                ShowMessageTQDT(msgInfor, false);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        Logger.LocalLogger.Instance().WriteMessage(ex);
            //        
            //    }
            //}
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {

            if (isAuto)
            {
                if (e.Row.RowType == RowType.Record)
                {

                    int id = dgList.CurrentRow.Position;
                    NPLHuyDetailsForm frm = new NPLHuyDetailsForm();

                    frm.HuyNPL = NPLHuyDK.NPLHuy_List[id];
                    frm.NPLHuyDK = NPLHuyDK;
                    frm.HopDong_ID = NPLHuyDK.HopDong_ID;
                    frm.ShowDialog();
                    LoadData();
                }
            }
            else
                if (e.Row.RowType == RowType.Record)
                {

                    int id = Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value);
                    NPLHuyDetailsForm frm = new NPLHuyDetailsForm();

                    frm.HuyNPL = NPLHuy.Load(id);
                    frm.NPLHuyDK = NPLHuyDK;
                    frm.HopDong_ID = NPLHuyDK.HopDong_ID;
                    frm.ShowDialog();
                    LoadData();
                }

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //{
            //    KDT_GC_CungUng CU = new KDT_GC_CungUng();
            //    CU = (KDT_GC_CungUng)e.Row.DataRow;
            //    decimal tongluog = 0;
            //    foreach (KDT_GC_CungUng_Detail CU_DeTail in CU.CungUngDetail_List)
            //    {
            //        foreach (KDT_GC_CungUng_ChungTu item in CU_DeTail.CungUngCT_List)
            //        {
            //            tongluog += (System.Decimal)item.SoLuong;
            //        }   
            //    }
            //    e.Row.Cells["LuongCungUng"].Text = tongluog.ToString();
            //}
        }

        private void btn_CapNhatPhanBo_Click(object sender, EventArgs e)
        {
            //List<NPLNhapTonThucTe> list_npl = new List<NPLNhapTonThucTe>();

            //foreach (KDT_GC_CungUng npl in NPLHuyDK.NPLCungUng_List)
            //{
            //    NPLNhapTonThucTe tt = new NPLNhapTonThucTe();
            //    tt.ID_HopDong = NPLHuyDK.HopDong_ID;
            //    tt.MaDoanhNghiep = NPLHuyDK.MaDoanhNghiep;
            //    tt.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    tt.MaHS = npl.MaHS;
            //    tt.MaLoaiHinh = "NPLCU";
            //    tt.MaNPL = npl.MaNPL;
            //    tt.NamDangKy = Int16.Parse(NPLHuyDK.NgayTiepNhan.Year.ToString());
            //    tt.SoToKhai = int.Parse(NPLHuyDK.ID.ToString());
            //    tt.TenNPL = npl.MaNPL;
            //    tt.Luong = npl.LuongCungUng;
            //    tt.Ton = npl.LuongCungUng;
            //    tt.TonTriGiaKB = 0;
            //    tt.TriGiaKB = 0;
            //    tt.XuatXu = "VN";
            //    tt.NgayDangKy = NPLHuyDK.NgayTiepNhan;
            //    tt.SoToKhaiVNACCS = 0;
            //    tt.DVT = npl.DVT_ID;
            //    list_npl.Add(tt);
            //}
            //try
            //{
            //    if (NPLNhapTonThucTe.InsertUpdateCollection(list_npl))
            //    {
            //        MessageBox.Show("Cập nhật thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    }else
            //        MessageBox.Show("Lỗi cập nhật phân bổ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }


    }
}
