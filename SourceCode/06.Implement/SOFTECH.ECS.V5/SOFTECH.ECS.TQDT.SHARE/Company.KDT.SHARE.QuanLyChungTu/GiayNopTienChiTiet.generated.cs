using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class GiayNopTienChiTiet
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GiayNopTien_ID { set; get; }
		public double SoTien { set; get; }
		public double DieuChinhGiam { set; get; }
		public int SacThue { set; get; }
		public string MaChuong { set; get; }
		public string Loai { set; get; }
		public string Khoan { set; get; }
		public string Muc { set; get; }
		public string TieuMuc { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static GiayNopTienChiTiet Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			GiayNopTienChiTiet entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new GiayNopTienChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayNopTien_ID"))) entity.GiayNopTien_ID = reader.GetInt64(reader.GetOrdinal("GiayNopTien_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTien"))) entity.SoTien = reader.GetDouble(reader.GetOrdinal("SoTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhGiam"))) entity.DieuChinhGiam = reader.GetDouble(reader.GetOrdinal("DieuChinhGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetInt32(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaChuong"))) entity.MaChuong = reader.GetString(reader.GetOrdinal("MaChuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetString(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetString(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetString(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetString(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<GiayNopTienChiTiet> SelectCollectionAll()
		{
			List<GiayNopTienChiTiet> collection = new List<GiayNopTienChiTiet>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				GiayNopTienChiTiet entity = new GiayNopTienChiTiet();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayNopTien_ID"))) entity.GiayNopTien_ID = reader.GetInt64(reader.GetOrdinal("GiayNopTien_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTien"))) entity.SoTien = reader.GetDouble(reader.GetOrdinal("SoTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhGiam"))) entity.DieuChinhGiam = reader.GetDouble(reader.GetOrdinal("DieuChinhGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetInt32(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaChuong"))) entity.MaChuong = reader.GetString(reader.GetOrdinal("MaChuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetString(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetString(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetString(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetString(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<GiayNopTienChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<GiayNopTienChiTiet> collection = new List<GiayNopTienChiTiet>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				GiayNopTienChiTiet entity = new GiayNopTienChiTiet();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayNopTien_ID"))) entity.GiayNopTien_ID = reader.GetInt64(reader.GetOrdinal("GiayNopTien_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTien"))) entity.SoTien = reader.GetDouble(reader.GetOrdinal("SoTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DieuChinhGiam"))) entity.DieuChinhGiam = reader.GetDouble(reader.GetOrdinal("DieuChinhGiam"));
				if (!reader.IsDBNull(reader.GetOrdinal("SacThue"))) entity.SacThue = reader.GetInt32(reader.GetOrdinal("SacThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaChuong"))) entity.MaChuong = reader.GetString(reader.GetOrdinal("MaChuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetString(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("Khoan"))) entity.Khoan = reader.GetString(reader.GetOrdinal("Khoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("Muc"))) entity.Muc = reader.GetString(reader.GetOrdinal("Muc"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuMuc"))) entity.TieuMuc = reader.GetString(reader.GetOrdinal("TieuMuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertGiayNopTienChiTiet(long giayNopTien_ID, double soTien, double dieuChinhGiam, int sacThue, string maChuong, string loai, string khoan, string muc, string tieuMuc, string ghiChu)
		{
			GiayNopTienChiTiet entity = new GiayNopTienChiTiet();	
			entity.GiayNopTien_ID = giayNopTien_ID;
			entity.SoTien = soTien;
			entity.DieuChinhGiam = dieuChinhGiam;
			entity.SacThue = sacThue;
			entity.MaChuong = maChuong;
			entity.Loai = loai;
			entity.Khoan = khoan;
			entity.Muc = muc;
			entity.TieuMuc = tieuMuc;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayNopTien_ID", SqlDbType.BigInt, GiayNopTien_ID);
			db.AddInParameter(dbCommand, "@SoTien", SqlDbType.Float, SoTien);
			db.AddInParameter(dbCommand, "@DieuChinhGiam", SqlDbType.Float, DieuChinhGiam);
			db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Int, SacThue);
			db.AddInParameter(dbCommand, "@MaChuong", SqlDbType.NVarChar, MaChuong);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.NVarChar, Loai);
			db.AddInParameter(dbCommand, "@Khoan", SqlDbType.NVarChar, Khoan);
			db.AddInParameter(dbCommand, "@Muc", SqlDbType.NVarChar, Muc);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.NVarChar, TieuMuc);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<GiayNopTienChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTienChiTiet item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateGiayNopTienChiTiet(long id, long giayNopTien_ID, double soTien, double dieuChinhGiam, int sacThue, string maChuong, string loai, string khoan, string muc, string tieuMuc, string ghiChu)
		{
			GiayNopTienChiTiet entity = new GiayNopTienChiTiet();			
			entity.ID = id;
			entity.GiayNopTien_ID = giayNopTien_ID;
			entity.SoTien = soTien;
			entity.DieuChinhGiam = dieuChinhGiam;
			entity.SacThue = sacThue;
			entity.MaChuong = maChuong;
			entity.Loai = loai;
			entity.Khoan = khoan;
			entity.Muc = muc;
			entity.TieuMuc = tieuMuc;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GiayNopTienChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayNopTien_ID", SqlDbType.BigInt, GiayNopTien_ID);
			db.AddInParameter(dbCommand, "@SoTien", SqlDbType.Float, SoTien);
			db.AddInParameter(dbCommand, "@DieuChinhGiam", SqlDbType.Float, DieuChinhGiam);
			db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Int, SacThue);
			db.AddInParameter(dbCommand, "@MaChuong", SqlDbType.NVarChar, MaChuong);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.NVarChar, Loai);
			db.AddInParameter(dbCommand, "@Khoan", SqlDbType.NVarChar, Khoan);
			db.AddInParameter(dbCommand, "@Muc", SqlDbType.NVarChar, Muc);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.NVarChar, TieuMuc);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<GiayNopTienChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTienChiTiet item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateGiayNopTienChiTiet(long id, long giayNopTien_ID, double soTien, double dieuChinhGiam, int sacThue, string maChuong, string loai, string khoan, string muc, string tieuMuc, string ghiChu)
		{
			GiayNopTienChiTiet entity = new GiayNopTienChiTiet();			
			entity.ID = id;
			entity.GiayNopTien_ID = giayNopTien_ID;
			entity.SoTien = soTien;
			entity.DieuChinhGiam = dieuChinhGiam;
			entity.SacThue = sacThue;
			entity.MaChuong = maChuong;
			entity.Loai = loai;
			entity.Khoan = khoan;
			entity.Muc = muc;
			entity.TieuMuc = tieuMuc;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayNopTien_ID", SqlDbType.BigInt, GiayNopTien_ID);
			db.AddInParameter(dbCommand, "@SoTien", SqlDbType.Float, SoTien);
			db.AddInParameter(dbCommand, "@DieuChinhGiam", SqlDbType.Float, DieuChinhGiam);
			db.AddInParameter(dbCommand, "@SacThue", SqlDbType.Int, SacThue);
			db.AddInParameter(dbCommand, "@MaChuong", SqlDbType.NVarChar, MaChuong);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.NVarChar, Loai);
			db.AddInParameter(dbCommand, "@Khoan", SqlDbType.NVarChar, Khoan);
			db.AddInParameter(dbCommand, "@Muc", SqlDbType.NVarChar, Muc);
			db.AddInParameter(dbCommand, "@TieuMuc", SqlDbType.NVarChar, TieuMuc);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<GiayNopTienChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTienChiTiet item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteGiayNopTienChiTiet(long id)
		{
			GiayNopTienChiTiet entity = new GiayNopTienChiTiet();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GiayNopTienChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<GiayNopTienChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiayNopTienChiTiet item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}