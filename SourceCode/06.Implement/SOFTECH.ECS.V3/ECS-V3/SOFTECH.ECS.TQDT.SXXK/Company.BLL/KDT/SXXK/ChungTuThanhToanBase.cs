using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SXXK
{
	public partial class ChungTuThanhToan 
	{
		#region Private members.
		
		protected long _Id;
		protected int _LanThanhLy;
		protected string _SoNgayChungTu = String.Empty;
		protected decimal _TriGiaCT;
		protected string _ToKhaiXuat = String.Empty;
		protected DateTime _NgayDangKyXuat = new DateTime(1900, 01, 01);
		protected string _SoNgayHopDong = String.Empty;
		protected string _MaHangXuat = String.Empty;
		protected decimal _TriGiaHopDong;
		protected decimal _TriGiaHangThucXuat;
		protected string _HinhThucThanhToan = String.Empty;
		protected string _GhiChu = String.Empty;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long Id
		{
			set {this._Id = value;}
			get {return this._Id;}
		}
		public int LanThanhLy
		{
			set {this._LanThanhLy = value;}
			get {return this._LanThanhLy;}
		}
		public string SoNgayChungTu
		{
			set {this._SoNgayChungTu = value;}
			get {return this._SoNgayChungTu;}
		}
		public decimal TriGiaCT
		{
			set {this._TriGiaCT = value;}
			get {return this._TriGiaCT;}
		}
		public string ToKhaiXuat
		{
			set {this._ToKhaiXuat = value;}
			get {return this._ToKhaiXuat;}
		}
		public DateTime NgayDangKyXuat
		{
			set {this._NgayDangKyXuat = value;}
			get {return this._NgayDangKyXuat;}
		}
		public string SoNgayHopDong
		{
			set {this._SoNgayHopDong = value;}
			get {return this._SoNgayHopDong;}
		}
		public string MaHangXuat
		{
			set {this._MaHangXuat = value;}
			get {return this._MaHangXuat;}
		}
		public decimal TriGiaHopDong
		{
			set {this._TriGiaHopDong = value;}
			get {return this._TriGiaHopDong;}
		}
		public decimal TriGiaHangThucXuat
		{
			set {this._TriGiaHangThucXuat = value;}
			get {return this._TriGiaHangThucXuat;}
		}
		public string HinhThucThanhToan
		{
			set {this._HinhThucThanhToan = value;}
			get {return this._HinhThucThanhToan;}
		}
		public string GhiChu
		{
			set {this._GhiChu = value;}
			get {return this._GhiChu;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_KDT_SXXK_ChungTuThanhToan_Load";
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, this._Id);
			
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) this._Id = reader.GetInt64(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayChungTu"))) this._SoNgayChungTu = reader.GetString(reader.GetOrdinal("SoNgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaCT"))) this._TriGiaCT = reader.GetDecimal(reader.GetOrdinal("TriGiaCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToKhaiXuat"))) this._ToKhaiXuat = reader.GetString(reader.GetOrdinal("ToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) this._NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayHopDong"))) this._SoNgayHopDong = reader.GetString(reader.GetOrdinal("SoNgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangXuat"))) this._MaHangXuat = reader.GetString(reader.GetOrdinal("MaHangXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHopDong"))) this._TriGiaHopDong = reader.GetDecimal(reader.GetOrdinal("TriGiaHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHangThucXuat"))) this._TriGiaHangThucXuat = reader.GetDecimal(reader.GetOrdinal("TriGiaHangThucXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThucThanhToan"))) this._HinhThucThanhToan = reader.GetString(reader.GetOrdinal("HinhThucThanhToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				reader.Close();
				return true;
			}
			reader.Close();
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		


		public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_ChungTuThanhToan_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_ChungTuThanhToan_SelectAll";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_ChungTuThanhToan_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_KDT_SXXK_ChungTuThanhToan_SelectDynamic";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public ChungTuThanhToanCollection SelectCollectionAll()
		{
			ChungTuThanhToanCollection collection = new ChungTuThanhToanCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				ChungTuThanhToan entity = new ChungTuThanhToan();
				
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt64(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayChungTu"))) entity.SoNgayChungTu = reader.GetString(reader.GetOrdinal("SoNgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaCT"))) entity.TriGiaCT = reader.GetDecimal(reader.GetOrdinal("TriGiaCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToKhaiXuat"))) entity.ToKhaiXuat = reader.GetString(reader.GetOrdinal("ToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayHopDong"))) entity.SoNgayHopDong = reader.GetString(reader.GetOrdinal("SoNgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangXuat"))) entity.MaHangXuat = reader.GetString(reader.GetOrdinal("MaHangXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHopDong"))) entity.TriGiaHopDong = reader.GetDecimal(reader.GetOrdinal("TriGiaHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHangThucXuat"))) entity.TriGiaHangThucXuat = reader.GetDecimal(reader.GetOrdinal("TriGiaHangThucXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThucThanhToan"))) entity.HinhThucThanhToan = reader.GetString(reader.GetOrdinal("HinhThucThanhToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public ChungTuThanhToanCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			ChungTuThanhToanCollection collection = new ChungTuThanhToanCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ChungTuThanhToan entity = new ChungTuThanhToan();
				
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt64(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayChungTu"))) entity.SoNgayChungTu = reader.GetString(reader.GetOrdinal("SoNgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaCT"))) entity.TriGiaCT = reader.GetDecimal(reader.GetOrdinal("TriGiaCT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ToKhaiXuat"))) entity.ToKhaiXuat = reader.GetString(reader.GetOrdinal("ToKhaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNgayHopDong"))) entity.SoNgayHopDong = reader.GetString(reader.GetOrdinal("SoNgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangXuat"))) entity.MaHangXuat = reader.GetString(reader.GetOrdinal("MaHangXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHopDong"))) entity.TriGiaHopDong = reader.GetDecimal(reader.GetOrdinal("TriGiaHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaHangThucXuat"))) entity.TriGiaHangThucXuat = reader.GetDecimal(reader.GetOrdinal("TriGiaHangThucXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThucThanhToan"))) entity.HinhThucThanhToan = reader.GetString(reader.GetOrdinal("HinhThucThanhToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_ChungTuThanhToan_Insert";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, this._Id);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			db.AddInParameter(dbCommand, "@SoNgayChungTu", SqlDbType.NVarChar, this._SoNgayChungTu);
			db.AddInParameter(dbCommand, "@TriGiaCT", SqlDbType.Money, this._TriGiaCT);
			db.AddInParameter(dbCommand, "@ToKhaiXuat", SqlDbType.VarChar, this._ToKhaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@SoNgayHopDong", SqlDbType.NVarChar, this._SoNgayHopDong);
			db.AddInParameter(dbCommand, "@MaHangXuat", SqlDbType.NVarChar, this._MaHangXuat);
			db.AddInParameter(dbCommand, "@TriGiaHopDong", SqlDbType.Money, this._TriGiaHopDong);
			db.AddInParameter(dbCommand, "@TriGiaHangThucXuat", SqlDbType.Money, this._TriGiaHangThucXuat);
			db.AddInParameter(dbCommand, "@HinhThucThanhToan", SqlDbType.VarChar, this._HinhThucThanhToan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(ChungTuThanhToanCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ChungTuThanhToan item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
        public void InsertFull(ChungTuThanhToanCollection collection, int LanThanhLy)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    DeleteDynamicTransaction("LanThanhLy = " + LanThanhLy, transaction);
                    InsertTransaction(transaction, collection);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
        }
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, ChungTuThanhToanCollection collection)
        {
            foreach (ChungTuThanhToan item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_KDT_SXXK_ChungTuThanhToan_InsertUpdate";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, this._Id);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			db.AddInParameter(dbCommand, "@SoNgayChungTu", SqlDbType.NVarChar, this._SoNgayChungTu);
			db.AddInParameter(dbCommand, "@TriGiaCT", SqlDbType.Money, this._TriGiaCT);
			db.AddInParameter(dbCommand, "@ToKhaiXuat", SqlDbType.VarChar, this._ToKhaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@SoNgayHopDong", SqlDbType.NVarChar, this._SoNgayHopDong);
			db.AddInParameter(dbCommand, "@MaHangXuat", SqlDbType.NVarChar, this._MaHangXuat);
			db.AddInParameter(dbCommand, "@TriGiaHopDong", SqlDbType.Money, this._TriGiaHopDong);
			db.AddInParameter(dbCommand, "@TriGiaHangThucXuat", SqlDbType.Money, this._TriGiaHangThucXuat);
			db.AddInParameter(dbCommand, "@HinhThucThanhToan", SqlDbType.VarChar, this._HinhThucThanhToan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(ChungTuThanhToanCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ChungTuThanhToan item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_ChungTuThanhToan_Update";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, this._Id);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			db.AddInParameter(dbCommand, "@SoNgayChungTu", SqlDbType.NVarChar, this._SoNgayChungTu);
			db.AddInParameter(dbCommand, "@TriGiaCT", SqlDbType.Money, this._TriGiaCT);
			db.AddInParameter(dbCommand, "@ToKhaiXuat", SqlDbType.VarChar, this._ToKhaiXuat);
			db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
			db.AddInParameter(dbCommand, "@SoNgayHopDong", SqlDbType.NVarChar, this._SoNgayHopDong);
			db.AddInParameter(dbCommand, "@MaHangXuat", SqlDbType.NVarChar, this._MaHangXuat);
			db.AddInParameter(dbCommand, "@TriGiaHopDong", SqlDbType.Money, this._TriGiaHopDong);
			db.AddInParameter(dbCommand, "@TriGiaHangThucXuat", SqlDbType.Money, this._TriGiaHangThucXuat);
			db.AddInParameter(dbCommand, "@HinhThucThanhToan", SqlDbType.VarChar, this._HinhThucThanhToan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(ChungTuThanhToanCollection collection, SqlTransaction transaction)
        {
            foreach (ChungTuThanhToan item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_KDT_SXXK_ChungTuThanhToan_Delete";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.BigInt, this._Id);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}

        public int DeleteDynamicTransaction(string where,SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_ChungTuThanhToan_DeleteDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, where);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(ChungTuThanhToanCollection collection, SqlTransaction transaction)
        {
            foreach (ChungTuThanhToan item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(ChungTuThanhToanCollection collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ChungTuThanhToan item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
	}	
}