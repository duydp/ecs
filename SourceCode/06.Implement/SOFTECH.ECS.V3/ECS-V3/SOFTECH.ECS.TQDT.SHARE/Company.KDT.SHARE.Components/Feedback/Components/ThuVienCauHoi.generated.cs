using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.Feedback.Components
{
	public partial class ThuVienCauHoi
	{
		#region Properties.
		
		public int MaCauHoi { set; get; }
		public int LinhVuc_ID { set; get; }
		public string Phanloai { set; get; }
		public string TieuDe { set; get; }
		public string NoiDungCauHoi { set; get; }
		public string TraLoi { set; get; }
		public DateTime NgayTao { set; get; }
		public DateTime NgayCapNhat { set; get; }
		public bool HienThi { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ThuVienCauHoi Load(int maCauHoi)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.Int, maCauHoi);
			ThuVienCauHoi entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new ThuVienCauHoi();
				if (!reader.IsDBNull(reader.GetOrdinal("MaCauHoi"))) entity.MaCauHoi = reader.GetInt32(reader.GetOrdinal("MaCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LinhVuc_ID"))) entity.LinhVuc_ID = reader.GetInt32(reader.GetOrdinal("LinhVuc_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanloai"))) entity.Phanloai = reader.GetString(reader.GetOrdinal("Phanloai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungCauHoi"))) entity.NoiDungCauHoi = reader.GetString(reader.GetOrdinal("NoiDungCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TraLoi"))) entity.TraLoi = reader.GetString(reader.GetOrdinal("TraLoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTao"))) entity.NgayTao = reader.GetDateTime(reader.GetOrdinal("NgayTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapNhat"))) entity.NgayCapNhat = reader.GetDateTime(reader.GetOrdinal("NgayCapNhat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<ThuVienCauHoi> SelectCollectionAll()
		{
			List<ThuVienCauHoi> collection = new List<ThuVienCauHoi>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				ThuVienCauHoi entity = new ThuVienCauHoi();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaCauHoi"))) entity.MaCauHoi = reader.GetInt32(reader.GetOrdinal("MaCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LinhVuc_ID"))) entity.LinhVuc_ID = reader.GetInt32(reader.GetOrdinal("LinhVuc_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanloai"))) entity.Phanloai = reader.GetString(reader.GetOrdinal("Phanloai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungCauHoi"))) entity.NoiDungCauHoi = reader.GetString(reader.GetOrdinal("NoiDungCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TraLoi"))) entity.TraLoi = reader.GetString(reader.GetOrdinal("TraLoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTao"))) entity.NgayTao = reader.GetDateTime(reader.GetOrdinal("NgayTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapNhat"))) entity.NgayCapNhat = reader.GetDateTime(reader.GetOrdinal("NgayCapNhat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<ThuVienCauHoi> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<ThuVienCauHoi> collection = new List<ThuVienCauHoi>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ThuVienCauHoi entity = new ThuVienCauHoi();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaCauHoi"))) entity.MaCauHoi = reader.GetInt32(reader.GetOrdinal("MaCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LinhVuc_ID"))) entity.LinhVuc_ID = reader.GetInt32(reader.GetOrdinal("LinhVuc_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanloai"))) entity.Phanloai = reader.GetString(reader.GetOrdinal("Phanloai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungCauHoi"))) entity.NoiDungCauHoi = reader.GetString(reader.GetOrdinal("NoiDungCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TraLoi"))) entity.TraLoi = reader.GetString(reader.GetOrdinal("TraLoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTao"))) entity.NgayTao = reader.GetDateTime(reader.GetOrdinal("NgayTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapNhat"))) entity.NgayCapNhat = reader.GetDateTime(reader.GetOrdinal("NgayCapNhat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ThuVienCauHoi> SelectCollectionBy_LinhVuc_ID(int linhVuc_ID)
		{
			List<ThuVienCauHoi> collection = new List<ThuVienCauHoi>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_LinhVuc_ID(linhVuc_ID);
			while (reader.Read())
			{
				ThuVienCauHoi entity = new ThuVienCauHoi();
				if (!reader.IsDBNull(reader.GetOrdinal("MaCauHoi"))) entity.MaCauHoi = reader.GetInt32(reader.GetOrdinal("MaCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("LinhVuc_ID"))) entity.LinhVuc_ID = reader.GetInt32(reader.GetOrdinal("LinhVuc_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Phanloai"))) entity.Phanloai = reader.GetString(reader.GetOrdinal("Phanloai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TieuDe"))) entity.TieuDe = reader.GetString(reader.GetOrdinal("TieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungCauHoi"))) entity.NoiDungCauHoi = reader.GetString(reader.GetOrdinal("NoiDungCauHoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TraLoi"))) entity.TraLoi = reader.GetString(reader.GetOrdinal("TraLoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTao"))) entity.NgayTao = reader.GetDateTime(reader.GetOrdinal("NgayTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCapNhat"))) entity.NgayCapNhat = reader.GetDateTime(reader.GetOrdinal("NgayCapNhat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThi"))) entity.HienThi = reader.GetBoolean(reader.GetOrdinal("HienThi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LinhVuc_ID(int linhVuc_ID)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectBy_LinhVuc_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc_ID", SqlDbType.Int, linhVuc_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_GopY_ThuVienCauHoi_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LinhVuc_ID(int linhVuc_ID)
		{
			const string spName = "p_GopY_ThuVienCauHoi_SelectBy_LinhVuc_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc_ID", SqlDbType.Int, linhVuc_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertThuVienCauHoi(int linhVuc_ID, string phanloai, string tieuDe, string noiDungCauHoi, string traLoi, DateTime ngayTao, DateTime ngayCapNhat, bool hienThi, string ghiChu)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();	
			entity.LinhVuc_ID = linhVuc_ID;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.TraLoi = traLoi;
			entity.NgayTao = ngayTao;
			entity.NgayCapNhat = ngayCapNhat;
			entity.HienThi = hienThi;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@MaCauHoi", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@LinhVuc_ID", SqlDbType.Int, LinhVuc_ID);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NgayCapNhat", SqlDbType.DateTime, NgayCapNhat.Year <= 1753 ? DBNull.Value : (object) NgayCapNhat);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				MaCauHoi = (int) db.GetParameterValue(dbCommand, "@MaCauHoi");
				return MaCauHoi;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				MaCauHoi = (int) db.GetParameterValue(dbCommand, "@MaCauHoi");
				return MaCauHoi;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateThuVienCauHoi(int maCauHoi, int linhVuc_ID, string phanloai, string tieuDe, string noiDungCauHoi, string traLoi, DateTime ngayTao, DateTime ngayCapNhat, bool hienThi, string ghiChu)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();			
			entity.MaCauHoi = maCauHoi;
			entity.LinhVuc_ID = linhVuc_ID;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.TraLoi = traLoi;
			entity.NgayTao = ngayTao;
			entity.NgayCapNhat = ngayCapNhat;
			entity.HienThi = hienThi;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_GopY_ThuVienCauHoi_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.Int, MaCauHoi);
			db.AddInParameter(dbCommand, "@LinhVuc_ID", SqlDbType.Int, LinhVuc_ID);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year == 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NgayCapNhat", SqlDbType.DateTime, NgayCapNhat.Year == 1753 ? DBNull.Value : (object) NgayCapNhat);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateThuVienCauHoi(int maCauHoi, int linhVuc_ID, string phanloai, string tieuDe, string noiDungCauHoi, string traLoi, DateTime ngayTao, DateTime ngayCapNhat, bool hienThi, string ghiChu)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();			
			entity.MaCauHoi = maCauHoi;
			entity.LinhVuc_ID = linhVuc_ID;
			entity.Phanloai = phanloai;
			entity.TieuDe = tieuDe;
			entity.NoiDungCauHoi = noiDungCauHoi;
			entity.TraLoi = traLoi;
			entity.NgayTao = ngayTao;
			entity.NgayCapNhat = ngayCapNhat;
			entity.HienThi = hienThi;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.Int, MaCauHoi);
			db.AddInParameter(dbCommand, "@LinhVuc_ID", SqlDbType.Int, LinhVuc_ID);
			db.AddInParameter(dbCommand, "@Phanloai", SqlDbType.VarChar, Phanloai);
			db.AddInParameter(dbCommand, "@TieuDe", SqlDbType.NVarChar, TieuDe);
			db.AddInParameter(dbCommand, "@NoiDungCauHoi", SqlDbType.NVarChar, NoiDungCauHoi);
			db.AddInParameter(dbCommand, "@TraLoi", SqlDbType.NVarChar, TraLoi);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year == 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NgayCapNhat", SqlDbType.DateTime, NgayCapNhat.Year == 1753 ? DBNull.Value : (object) NgayCapNhat);
			db.AddInParameter(dbCommand, "@HienThi", SqlDbType.Bit, HienThi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteThuVienCauHoi(int maCauHoi)
		{
			ThuVienCauHoi entity = new ThuVienCauHoi();
			entity.MaCauHoi = maCauHoi;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaCauHoi", SqlDbType.Int, MaCauHoi);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LinhVuc_ID(int linhVuc_ID)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_DeleteBy_LinhVuc_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LinhVuc_ID", SqlDbType.Int, linhVuc_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_GopY_ThuVienCauHoi_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ThuVienCauHoi> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ThuVienCauHoi item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}