using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.SXXK.BangKeThanhLy
{
    public partial class frmDispInfo : Form
    {
        BKToKhaiXuatCollection BKTKXColl = new BKToKhaiXuatCollection();
        BangKeHoSoThanhLyCollection BKHSTLC = new BangKeHoSoThanhLyCollection();
        DataSet dsDanhSachNPLTon = new DataSet();

        public ToKhaiMauDichCollection DSTKNTL = new ToKhaiMauDichCollection();

        public delegate void setLable(string txt);
        public delegate void settProgess();
        public frmDispInfo(BangKeHoSoThanhLyCollection BKHSTLC, DataSet dsDanhSachTon)
        {
            InitializeComponent();
            this.dsDanhSachNPLTon = dsDanhSachTon;
            foreach (BangKeHoSoThanhLy bkhs in BKHSTLC)
            {
                if (bkhs.MaBangKe == "DTLTKX")
                {
                    BKTKXColl = bkhs.bkTKXColletion;
                }
            }
            pBar.Maximum = BKTKXColl.Count;
        }

        public void dispTK(string str)
        {
            if (this.label1.InvokeRequired)
            {
                // It's on a different thread, so use Invoke.
                this.BeginInvoke(new setLable(dispTK), new object[] { str });
            }
            else
            {
                // It's on the same thread, no need for Invoke
                label1.Text = str;
            }
            
        }

        private void dispProcess()
        {
            if (this.pBar.InvokeRequired)
            {

                this.BeginInvoke(new settProgess(dispProcess));
            }
            else
            {
                // It's on the same thread, no need for Invoke
                pBar.Value += 1;
            }

            
        }

        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int i1 = 0;
            int chenhLech = Convert.ToInt32(GlobalSettings.CHENHLECH_THN_THX);                                            
            try
            {
                List<HangMauDich> HMDColl = new List<HangMauDich>();
                foreach (BKToKhaiXuat bk in BKTKXColl)
                {
                    dispProcess();
                    ToKhaiMauDich TKX = new ToKhaiMauDich();
                    TKX.SoToKhai = bk.SoToKhai;
                    TKX.MaHaiQuan = bk.MaHaiQuan;
                    TKX.MaLoaiHinh = bk.MaLoaiHinh;
                    TKX.NamDangKy = bk.NamDangKy;
                    if (TKX.Load())
                    {
                        this.dispTK(TKX.SoToKhai.ToString() + " (" + TKX.NgayDangKy.ToString("dd/MM/yyyy") + ")");
                        TKX.LoadHMDCollection();
                        List<HangMauDich> HMDCollTemp = TKX.HMDCollection;
                        /**
                         * Nguyen Phu lieu
                         * */
                        if (TKX.Xuat_NPL_SP == "N")
                        {
                            #region TKTai Xuat
                            
                            DateTime ngayThucXuat = TKX.NgayDangKy;
                            if (TKX.NGAY_THN_THX.Year > 1900)
                                ngayThucXuat = TKX.NGAY_THN_THX;

                            ngayThucXuat = ngayThucXuat.AddDays((-1) * chenhLech);
                            foreach (HangMauDich hmd in HMDCollTemp)
                            {
                                decimal nhuCau = hmd.SoLuong;
                                string maNPL = hmd.MaPhu.Trim();
                                DataRow[] rowTon = dsDanhSachNPLTon.Tables[0].Select("MaNPL='" + maNPL + "' and NgayThucNhap<='" + ngayThucXuat.ToShortDateString() + "' and Ton>0", "NgayThucNhap,SoToKhai,MaLoaiHinh");

                                foreach (DataRow row in rowTon)
                                {
                                    decimal tonDau = Convert.ToDecimal(row["Ton"]);
                                    decimal tonCuoi = tonDau - nhuCau;
                                    if (tonCuoi >= 0)
                                    {
                                        row["Ton"] = tonCuoi;
                                        ToKhaiMauDich TKMDTMP = new ToKhaiMauDich();

                                        TKMDTMP.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                                        TKMDTMP.MaLoaiHinh = (row["MaLoaiHinh"]).ToString();
                                        TKMDTMP.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                                        if (!checkExist(TKMDTMP, DSTKNTL))
                                        {
                                            TKMDTMP.Load();
                                            DSTKNTL.Add(TKMDTMP);
                                        }
                                        break;
                                    }
                                    else
                                    {
                                        nhuCau = nhuCau - tonDau;
                                        row["Ton"] = 0;
                                    }
                                }
                            }
                            #endregion TKTai Xuat
                        }
                            
                        /**
                         * San pham
                         * */
                        else
                        {
                            #region SP
                            DateTime ngayThucXuat = TKX.NgayDangKy;
                            if (TKX.NGAY_THN_THX.Year > 1900)
                                ngayThucXuat = TKX.NGAY_THN_THX;

                            ngayThucXuat = ngayThucXuat.AddDays((-1) * chenhLech);
                            foreach (HangMauDich hmd in HMDCollTemp)
                            {
                                DataSet dsDinhMuc = Company.BLL.SXXK.DinhMuc.getDinhMucOfSanPham(hmd.MaPhu, hmd.MaHaiQuan, GlobalSettings.MA_DON_VI, hmd.SoLuong, GlobalSettings.SoThapPhan.LuongNPL);
                                for (int i0 = 0; i0 < dsDinhMuc.Tables[0].Rows.Count; i0++)
                                {
                                    DataRow dr = dsDinhMuc.Tables[0].Rows[i0];
                                    decimal nhuCau = Convert.ToDecimal(dr["SoLuong"]);
                                    string maNPL = dr["MaNguyenPhuLieu"].ToString().Trim();
                                    DataRow[] rowTon = dsDanhSachNPLTon.Tables[0].Select("MaNPL='"+maNPL+"' and NgayThucNhap<='"+ngayThucXuat.ToShortDateString()+"' and Ton>0","NgayThucNhap,SoToKhai,MaLoaiHinh");
                                    foreach (DataRow row in rowTon)
                                    {
                                        decimal tonDau = Convert.ToDecimal(row["Ton"]);
                                        decimal tonCuoi = tonDau - nhuCau;
                                        if (tonCuoi >= 0)
                                        {
                                            row["Ton"] = tonCuoi;
                                            ToKhaiMauDich TKMDTMP = new ToKhaiMauDich();

                                            TKMDTMP.SoToKhai = Convert.ToInt32(row["SoToKhai"]);
                                            TKMDTMP.MaLoaiHinh= (row["MaLoaiHinh"]).ToString();
                                            TKMDTMP.NamDangKy = Convert.ToInt16(row["NamDangKy"]);
                                            TKMDTMP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                                            if (!checkExist(TKMDTMP, DSTKNTL))
                                            {
                                                TKMDTMP.Load();
                                                DSTKNTL.Add(TKMDTMP);
                                            }
                                            break;
                                        }
                                        else
                                        {
                                            nhuCau = nhuCau - tonDau;
                                            row["Ton"] = 0;
                                        }
                                    }

                                    
                                }
                            }
                            #endregion SP
                        }
                    }
                }

            }
            catch
            {
            }
        }

        private bool checkExist(ToKhaiMauDich tk, ToKhaiMauDichCollection tkCol)
        {
            for (int i = 0; i < tkCol.Count; i++)
            {
                if (tk.MaLoaiHinh == tkCol[i].MaLoaiHinh
                    && tk.NamDangKy == tkCol[i].NamDangKy && tk.SoToKhai == tkCol[i].SoToKhai)
                    return true;
            }
            return false;
        }

        private void frmDispInfo_Load(object sender, EventArgs e)
        {
            bgWorker.RunWorkerAsync(pBar);
        }

        private void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }

    }
}