﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Xml.Serialization;

namespace SignRemote
{
    public struct SendStatusInfo
    {
        public const int Send = 0;
        public const int Request = 1;
        public const int NoSign = 100;
        public const int Sign = 101;
        public const int Wait = 200;
        public const int Error = 300;
        public const int ServerLogout = 400;
        public const int ServerLogin = 401;

        // Số 5 đứng trước dành cho VNACCS
        public const int NoSignVNACCS = 500;
        public const int SignVNACCS = 501;
        public const int SendVNACCS = 50;
        public const int RequestVNACCS = 51;

    }
    public class Message
    {
        public string From { get; set; }
        /// <summary>
        /// Thông tin máy gửi đi
        /// </summary>
        public string GuidStr { get; set; }
        /// <summary>
        /// Thông tin gửi đi
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Thông tin chữ ký số
        /// </summary>
        public string SignCert { get; set; }
        /// <summary>
        /// Thông tin đã ký
        /// </summary>
        public string SignData { get; set; }

    }
    public class MessageFile
    {
        public string From { get; set; }
        /// <summary>
        /// Thông tin máy gửi đi
        public string GuidStr { get; set; }
        /// <summary>
        /// Thông tin gửi đi
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Thông tin File gửi đi
        /// </summary>
        public string FileData { get; set; }

    }
    [XmlRoot("Root")]
    public class MessageSend
    {

        /// <summary>
        /// Mật khẩu
        /// </summary>
        public string Password { get; set; }
        public int SendStatus { get; set; }
        public Message Message { get; set; }
        /// <summary>
        /// Nơi đến
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// Nơi gửi
        /// </summary>
        public string To { get; set; }

        public string Warrning { get; set; }
        /// <summary>
        /// = "VNACCS" nếu là messages của VNACCS
        /// </summary>
        public string TypeMessages { get; set; }
    }
    [XmlRoot("Root")]
    public class MessageSendFile
    {

        /// <summary>
        /// Mật khẩu
        /// </summary>
        public string Password { get; set; }
        public int SendStatus { get; set; }
        public MessageFile Message { get; set; }
        /// <summary>
        /// Nơi đến
        /// </summary>
        public string From { get; set; }
        /// <summary>
        /// Nơi gửi
        /// </summary>
        public string To { get; set; }

        public string Warrning { get; set; }
        /// <summary>
        /// = "VNACCS" nếu là messages của VNACCS
        /// </summary>
        public string TypeMessages { get; set; }
    }
}
