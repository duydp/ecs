﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Text;
using System.Security.Cryptography;

namespace Company.Interface
{
    public partial class WSForm : BaseForm
    {
        public bool IsReady = false;
        public WSForm()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI.Trim();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            error.Clear();
            bool isValid = ValidateControl.ValidateNull(txtMaDoanhNghiep, error, "Mã doanh nghiệp");
            isValid &= ValidateControl.ValidateNull(txtMatKhau, error, "Mật khẩu");
            if (!isValid)
                return;
            IsReady = true;
            GlobalSettings.IsRemember = ckLuu.Checked;
            GlobalSettings.UserId = txtMaDoanhNghiep.Text.Trim();
            //  if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00")
            // {
            txtMatKhau.Text = GetMD5Value(txtMatKhau.Text.Trim());
            //}
            //else
            //{
            //    txtMatKhau.Text = GetMD5Value(txtMaDoanhNghiep.Text.Trim() + "+" + txtMatKhau.Text.Trim());
            //}
            if (GlobalSettings.IsRemember)
            {
                GlobalSettings.PassWordDT = txtMatKhau.Text;
            }
            this.Close();

        }
        public string GetMD5Value(string data)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            //return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper(); //TQDT Version
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper().ToLower(); //TNTT Version
        }

    }
}