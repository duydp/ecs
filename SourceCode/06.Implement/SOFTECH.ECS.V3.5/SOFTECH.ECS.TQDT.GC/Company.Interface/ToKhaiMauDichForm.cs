﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Company.GC.BLL.KDT.SXXK;
using System.Net.Mail;
using System.Data;
using Company.Interface.GC;
using System.Collections.Generic;
using System.Xml;
using System.Diagnostics;
using GemBox.Spreadsheet;
using Infragistics.Excel;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Text.RegularExpressions;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KD.BLL.KDT;
using System.Drawing;
namespace Company.Interface
{
    public partial class ToKhaiMauDichForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiTriGia TKTGG = new ToKhaiTriGia();
        private string xmlCurrent = "";
        public Company.GC.BLL.KDT.GC.HopDong Hdgc = new Company.GC.BLL.KDT.GC.HopDong();
        public string NhomLoaiHinh = string.Empty;
        private string _xmlCurrent = "";
        public long PtkmdId;
        public bool BNew = true;
        public static string tenPTVT = "";
        private static int i;
        public bool luu = false;
        public bool isEdited = false;
        public static int soDongHang;
        public bool isDeny = false;
        bool isDeleted = false;
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;

        #region Biến phục vụ cho việc In tờ khai sửa đổi bổ sung
        //BEGIN: DATLMQ bổ sung các biến dùng để lưu giá trị TK cũ 16/02/2011
        private string nguoiNhapKhau = string.Empty;
        private string nguoiXuatKhau = string.Empty;
        private string nguoiUyThac = string.Empty;
        private string PTVT = string.Empty;
        private string soHieuPTVT = string.Empty;
        private DateTime ngayDenPTVT = new DateTime(1900, 1, 1);
        private string nuocXuatKhau = string.Empty;
        private string nuocNhapKhau = string.Empty;
        private string dieuKienGiaoHang = string.Empty;
        private string phuongThucThanhToan = string.Empty;
        private string soGiayPhep = string.Empty;
        private DateTime ngayGiayPhep = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanGiayPhep = new DateTime(1900, 1, 1);
        private string hoaDonTM = string.Empty;
        private DateTime ngayHoaDonTM = new DateTime(1900, 1, 1);
        private string diaDiemDoHang = string.Empty;
        private string nguyenTe = string.Empty;
        private decimal tyGiaTinhThue = 0;
        private decimal tyGiaUSD = 0;
        private string soHopDong = string.Empty;
        private DateTime ngayHopDong = new DateTime(1900, 1, 1);
        private DateTime ngayHetHanHopDong = new DateTime(1900, 1, 1);
        private string soVanDon = string.Empty;
        private DateTime ngayVanDon = new DateTime(1900, 1, 1);
        private string diaDiemXepHang = string.Empty;
        private decimal soContainer20;
        private decimal soContainer40;
        private decimal soKienHang;
        private decimal trongLuong;
        private double trongLuongNet;
        private decimal lePhiHaiQuan;
        private decimal phiBaoHiem;
        private decimal phiVanChuyen;
        private decimal phiKhac;
        //END

        //DATLMQ bổ sung biến phục vụ việc tự động lưu nội dung sửa đổi bổ sung 04/03/2011
        //public static List<Company.KD.BLL.KDT.HangMauDich> ListHangMauDichEdit = new List<Company.KD.BLL.KDT.HangMauDich>();
        //private static Company.KD.BLL.KDT.HangMauDich hangMDTK = new Company.KD.BLL.KDT.HangMauDich();
        public static string maHS_Edit = string.Empty;
        public static string tenHang_Edit = string.Empty;
        public static string maHang_Edit = string.Empty;
        public static string xuatXu_Edit = string.Empty;
        public static decimal soLuong_Edit;
        public static string dvt_Edit = string.Empty;
        public static decimal dongiaNT_Edit;
        public static decimal trigiaNT_Edit;
        public bool isCopy = false;
        //END
        #endregion

        //public NoiDungDieuChinhTK noiDungDieuChinhTK = new NoiDungDieuChinhTK();

        public ToKhaiMauDichForm()
        {
            InitializeComponent();

            CreateCommandBosung();

            //SetEvent_TextBox_GiayPhep();
            //SetEvent_TextBox_HoaDon();

            SetEvent_TextBox_VanDon();
            SetEvent_TextBox_GiayPhep();
            SetEvent_TextBox_HopDong();
            SetEvent_TextBox_HoaDon();
        }

        private void SetEvent_TextBox_VanDon()
        {
            //txtSoHieuPTVT.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            //txtSoHieuPTVT.ButtonClick += new EventHandler(txtSoHieuPTVT_ButtonClick2);

            txtSoVanTaiDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_HopDong()
        {
            txtSoHopDong.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_GiayPhep()
        {
            txtSoGiayPhep.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void SetEvent_TextBox_HoaDon()
        {
            txtSoHoaDonThuongMai.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
        }

        private void txtSoHieuPTVT_ButtonClick2(object sender, EventArgs e)
        {
            Company.Interface.VanTaiDonForm frmVanDon = new VanTaiDonForm();

            if (TKMD != null)
            {
                if (TKMD.MaLoaiHinh.Substring(0, 1) == "N" && TKMD.ID == 0)
                {
                    ShowMessage("Bạn vui lòng nhập thông tin sau trên tờ khai chính, và lưu tờ khai:\r\n- Số hiệu phương tiện vận tải\r\n- Ngày đến\r\n\nLưu ý: Thông tin 'Vận tải đơn chi tiết' chỉ cho phép nhập khi đã Lưu thông tin tờ khai thành công.", false);
                    return;
                }

                TKMD.SoVanDon = txtSoVanTaiDon.Text;
                TKMD.NgayVanDon = ccNgayVanTaiDon.Value;

                TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                TKMD.NgayDenPTVT = ccNgayDen.Value;

                frmVanDon.TKMD = TKMD;

                frmVanDon.ShowDialog(this);

                if (TKMD.VanTaiDon != null)
                {
                    txtSoHieuPTVT.Text = TKMD.VanTaiDon.SoHieuPTVT;
                    ccNgayDen.Text = TKMD.VanTaiDon.NgayDenPTVT.ToShortDateString();

                    if (txtSoVanTaiDon.Text != "")
                    {
                        if (Globals.ShowMessage("Bạn có muốn cập nhật lại thông tin 'Số vận đơn' không?.", true) == "Yes")
                        {
                            txtSoVanTaiDon.Text = TKMD.VanTaiDon.SoVanDon;
                            ccNgayVanTaiDon.Text = TKMD.NgayVanDon.ToShortDateString();
                        }
                    }
                }

            }
        }

        private void txtSoHopDong_Leave(object sender, EventArgs e)
        {
            if (txtSoHopDong.Text.Trim().Length > 0)
                ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = false;
            else
            {
                ccNgayHopDong.ReadOnly = ccNgayHHHopDong.ReadOnly = true;
                ccNgayHopDong.Text = ccNgayHHHopDong.Text = "";
            }
        }

        private void txtSoHoaDonThuongMai_Leave(object sender, EventArgs e)
        {
            if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                ccNgayHDTM.ReadOnly = false;
            else
            {
                ccNgayHDTM.ReadOnly = true;
                ccNgayHDTM.Text = "";
            }
        }

        private void txtSoVanTaiDon_Leave(object sender, EventArgs e)
        {
            if (txtSoVanTaiDon.Text.Trim().Length > 0)
                ccNgayVanTaiDon.ReadOnly = false;
            else
            {
                ccNgayVanTaiDon.ReadOnly = true;
                ccNgayVanTaiDon.Text = "";

            }
        }

        private void txtSoGiayPhep_Leave(object sender, System.EventArgs e)
        {
            if (txtSoGiayPhep.Text.Trim().Length > 0)
                ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = false;
            else
            {
                ccNgayGiayPhep.ReadOnly = ccNgayHHGiayPhep.ReadOnly = true;
                ccNgayGiayPhep.Text = ccNgayHHGiayPhep.Text = "";
            }
        }

        #region TẠO COMMAND TOOLBAR

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVanDonBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdGiayPhepBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHopDongBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHoaDonThuongMaiBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCOBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenCuaKhauBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuDangAnhBS1;
        private Janus.Windows.UI.CommandBars.UICommand cmdBoSungGiayKiemTra;
        private Janus.Windows.UI.CommandBars.UICommand cmdBoSungChungThuGiamDinh;


        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung()
        {
            #region Initialize
            //Tao nut command tren toolbar.
            this.cmdChungTuBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuBoSung");
            this.cmdVanDonBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDonBoSung");
            this.cmdGiayPhepBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhepBoSung");
            this.cmdHopDongBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongBoSung");
            this.cmdHoaDonThuongMaiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMaiBoSung");
            this.cmdCOBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCOBoSung");
            this.cmdChuyenCuaKhauBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhauBoSung");
            this.cmdChungTuDangAnhBS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnhBS");
            this.cmdBoSungGiayKiemTra = new Janus.Windows.UI.CommandBars.UICommand("BoSungGiayKiemTra");
            this.cmdBoSungChungThuGiamDinh = new Janus.Windows.UI.CommandBars.UICommand("BoSungChungThuGiamDinh");

            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                this.cmdChungTuBoSung1
            });

            if (!this.cmMain.Commands.Contains("cmdChungTuBoSung"))
                this.cmMain.Commands.Add(this.cmdChungTuBoSung1);
            if (!this.cmMain.Commands.Contains("cmdGiayPhepBoSung"))
                this.cmMain.Commands.Add(this.cmdGiayPhepBoSung1);
            if (!this.cmMain.Commands.Contains("cmdHopDongBoSung"))
                this.cmMain.Commands.Add(this.cmdHopDongBoSung1);
            if (!this.cmMain.Commands.Contains("cmdHoaDonThuongMaiBoSung"))
                this.cmMain.Commands.Add(this.cmdHoaDonThuongMaiBoSung1);
            if (!this.cmMain.Commands.Contains("cmdCOBoSung"))
                this.cmMain.Commands.Add(this.cmdCOBoSung1);
            if (!this.cmMain.Commands.Contains("cmdChuyenCuaKhauBoSung"))
                this.cmMain.Commands.Add(this.cmdChuyenCuaKhauBoSung1);
            if (!this.cmMain.Commands.Contains("cmdChungTuDangAnhBS"))
                this.cmMain.Commands.Add(this.cmdChungTuDangAnhBS1);
            if (!this.cmMain.Commands.Contains("cmdGiayNopTien"))
                this.cmMain.Commands.Add(this.cmdGiayNopTien);
            if (!this.cmMain.Commands.Contains("BoSungGiayKiemTra"))
                this.cmMain.Commands.Add(this.cmdBoSungGiayKiemTra);
            if (!this.cmMain.Commands.Contains("BoSungChungThuGiamDinh"))
                this.cmMain.Commands.Add(this.cmdBoSungChungThuGiamDinh);

            //this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            //    this.cmdChungTuBoSung1,                
            //    this.cmdGiayPhepBoSung1,
            //    this.cmdHopDongBoSung1,
            //    this.cmdHoaDonThuongMaiBoSung1,
            //    this.cmdCOBoSung1,
            //    this.cmdChuyenCuaKhauBoSung1,
            //    this.cmdChungTuDangAnhBS1
            //});

            // 
            // cmdChungTuBoSung
            // 
            this.cmdChungTuBoSung1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGiayPhepBoSung1,
            this.cmdHopDongBoSung1,
            this.cmdHoaDonThuongMaiBoSung1,
            this.cmdCOBoSung1,
            this.cmdChuyenCuaKhauBoSung1,
            this.cmdChungTuDangAnhBS1,
            cmdGiayNopTien, this.cmdBoSungChungThuGiamDinh, this.cmdBoSungGiayKiemTra});

            this.cmdChungTuBoSung1.Key = "cmdChungTuBoSung";
            this.cmdChungTuBoSung1.Name = "cmdChungTuBoSung1";
            this.cmdChungTuBoSung1.Text = "Chứng từ bổ sung";
            this.cmdChungTuBoSung1.ImageIndex = 4;
            // 
            // cmdHopDongBoSung1
            // 
            this.cmdHopDongBoSung1.Key = "cmdHopDongBoSung";
            this.cmdHopDongBoSung1.Name = "cmdHopDongBoSung1";
            this.cmdHopDongBoSung1.Text = "Hợp đồng ";
            this.cmdHopDongBoSung1.ImageIndex = 0;
            // 
            // cmdVanDonBoSung1
            // 
            this.cmdVanDonBoSung1.Key = "cmdVanDonBoSung";
            this.cmdVanDonBoSung1.Name = "cmdVanDonBoSung1";
            this.cmdVanDonBoSung1.Text = "Vận đơn ";
            this.cmdVanDonBoSung1.ImageIndex = 0;
            // 
            // CO1
            // 
            this.cmdCOBoSung1.Key = "cmdCOBoSung";
            this.cmdCOBoSung1.Name = "cmdCOBoSung1";
            this.cmdCOBoSung1.Text = "CO";
            this.cmdCOBoSung1.ImageIndex = 0;
            // 
            // cmdChuyenCuaKhauBoSung1
            // 
            this.cmdChuyenCuaKhauBoSung1.Key = "cmdChuyenCuaKhauBoSung";
            this.cmdChuyenCuaKhauBoSung1.Name = "cmdChuyenCuaKhauBoSung1";
            this.cmdChuyenCuaKhauBoSung1.Text = "Đề nghị chuyển cửa khẩu";
            this.cmdChuyenCuaKhauBoSung1.ImageIndex = 0;
            // 
            // cmdGiayPhepBoSung1
            // 
            this.cmdGiayPhepBoSung1.Key = "cmdGiayPhepBoSung";
            this.cmdGiayPhepBoSung1.Name = "cmdGiayPhepBoSung1";
            this.cmdGiayPhepBoSung1.Text = "Giấy phép";
            this.cmdGiayPhepBoSung1.ImageIndex = 0;
            // 
            // cmdHoaDonThuongMaiBoSung1
            // 
            this.cmdHoaDonThuongMaiBoSung1.Key = "cmdHoaDonThuongMaiBoSung";
            this.cmdHoaDonThuongMaiBoSung1.Name = "cmdHoaDonThuongMaiBoSung1";
            this.cmdHoaDonThuongMaiBoSung1.Text = "Hóa đơn thương mại";
            this.cmdHoaDonThuongMaiBoSung1.ImageIndex = 0;
            // 
            // cmdChungTuDangAnhBS1
            // 
            this.cmdChungTuDangAnhBS1.Key = "cmdChungTuDangAnhBS";
            this.cmdChungTuDangAnhBS1.Name = "cmdChungTuDangAnhBS1";
            this.cmdChungTuDangAnhBS1.Text = "Chứng từ dạng ảnh";
            this.cmdChungTuDangAnhBS1.ImageIndex = 0;
            // 
            // Giấy kiểm tra
            // 
            this.cmdBoSungGiayKiemTra.Key = "BoSungGiayKiemTra";
            this.cmdBoSungGiayKiemTra.Name = "BoSungGiayKiemTra";
            this.cmdBoSungGiayKiemTra.Text = "Giấy kiểm tra";
            this.cmdBoSungGiayKiemTra.ImageIndex = 0;
            // 
            // Chứng thư giám định
            // 
            this.cmdBoSungChungThuGiamDinh.Key = "BoSungChungThuGiamDinh";
            this.cmdBoSungChungThuGiamDinh.Name = "BoSungChungThuGiamDinh";
            this.cmdBoSungChungThuGiamDinh.Text = "Chứng thư giám định";
            this.cmdBoSungChungThuGiamDinh.ImageIndex = 0;

            #endregion

            cmdChungTuBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);

        }

        private void cmdChungTuBoSung1_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdVanDonBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoVanTaiDon_ButtonClick("1", null);
                        break;
                    case "cmdHoaDonThuongMaiBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                        break;
                    case "CO":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.QuanLyCo("1");
                        break;
                    case "cmdHopDongBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoHopDong_ButtonClick("1", null);
                        break;
                    case "cmdGiayPhepBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.txtSoGiayPhep_ButtonClick("1", null);
                        break;
                    case "cmdChuyenCuaKhauBoSung":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.DeNghiChuyenCuaKhau("1");
                        break;

                    case "cmdChungTuDangAnhBS":
                        if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                        this.AddChungTuAnh(true);
                        break;

                }
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        #region Validate Khai bo sung

        /// <summary>
        /// Kiem tra thong tin truoc khi khai bo sung chung tu.
        /// </summary>
        /// <param name="soToKhai"></param>
        /// <returns></returns>
        /// HUNGTQ, Update 07/06/2010.
        private bool ValidateKhaiBoSung(int soToKhai)
        {
            if (soToKhai == 0)
            {
                string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                Globals.ShowMessageTQDT(msg, false);

                return false;
            }

            return true;
        }

        #endregion

        #region Validate ToKhaiMauDich
        /// <summary>
        /// Kiem tra do dai cua truong nguoiNK
        /// </summary>
        /// DATLMQ, Update 30072010
        ErrorProvider err = new ErrorProvider();

        private bool ValidateNguoiNK()
        {
            bool isValid = false;
            try
            {
                if (NhomLoaiHinh.Substring(0, 1) == "N")
                    isValid = ValidateControl.ValidateLength(txtTenDonViDoiTac, 130, err, "Người xuất khẩu");
                else
                    isValid = ValidateControl.ValidateLength(txtTenDonViDoiTac, 130, err, "Người nhập khẩu");
            }
            catch (Exception ex) 
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        #endregion

        #endregion

        private void TinhLaiThue()
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
            {
                ShowMessage("Chức năng tự động tính thuế đã tắt. Vui lòng kiểm tra chính xác thông tin trước khi gửi cho Hải Quan", false);
                return;
            }
            TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            TKMD.TongTriGiaKhaiBao = 0;
            decimal phi = Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            decimal tongTriGiaHang = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
            {
                tongTriGiaHang += hmd.TriGiaKB;
                TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
            }
            if (tongTriGiaHang == 0) return;

            decimal triGiaTtMotDong = (phi == 0 || GlobalSettings.TuDongTinhThue == "0") ? TKMD.TyGiaTinhThue : (1 + phi / tongTriGiaHang) * Convert.ToDecimal(TKMD.TyGiaTinhThue);
            TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
            {
                //                 hmd.DonGiaTT = triGiaTtMotDong * hmd.DonGiaKB;
                //                 hmd.TriGiaTT = Math.Round(hmd.DonGiaTT * (hmd.SoLuong), MidpointRounding.AwayFromZero);
                //                 TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                //                 hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                //                 hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                //                 hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                //                 hmd.TriGiaThuKhac = Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero);

                hmd.DonGiaTT = triGiaTtMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = Math.Round(hmd.DonGiaTT * hmd.SoLuong, MidpointRounding.AwayFromZero);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                if (!hmd.ThueTuyetDoi)
                    hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                else
                {
                    hmd.ThueXNK = Math.Round(Convert.ToDecimal(hmd.DonGiaTuyetDoi) * TKMD.TyGiaTinhThue * hmd.SoLuong);
                }
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueBVMT = hmd.ThueSuatBVMT > 0 ? Math.Round((hmd.TriGiaTT) * Convert.ToDecimal(hmd.ThueSuatBVMT) / 100) : hmd.ThueBVMT;
                hmd.ThueChongPhaGia = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * Convert.ToDecimal(hmd.ThueSuatChongPhaGia));
                hmd.TriGiaThuKhac = hmd.TyLeThuKhac > 0 ? Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero) : hmd.TriGiaThuKhac;
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueChongPhaGia + hmd.ThueTTDB + hmd.TriGiaThuKhac + hmd.ThueBVMT) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = TKMD.HMDCollection;
            dgList.Refetch();
        }

        private void TinhTongTriGiaKhaiBao()
        {
            TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            TKMD.TongTriGiaKhaiBao = 0;
            TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in TKMD.HMDCollection)
            {
                TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
                TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
            }
        }
        private bool KiemTraLuongTon()
        {
            if (radSP.Checked)
            {
                DataSet dsLuongTon = NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.IDHopDong);
                foreach (Company.GC.BLL.KDT.HangMauDich hangMd in TKMD.HMDCollection)
                {
                    if (hangMd.ID > 0)
                    {
                        DataTable dtLuongNplTheoDm = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaPhu, hangMd.SoLuong, TKMD.IDHopDong).Tables[0];
                        foreach (DataRow row in dtLuongNplTheoDm.Rows)
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                        }
                    }
                }
                //LanNT Sort by maphu
                TKMD.HMDCollection.Sort(
                    delegate(HangMauDich hang1, HangMauDich hang2)
                    {
                        return hang1.MaPhu.CompareTo(hang2.MaPhu);
                    }
                    );
                string tenHangBiAm = "";
                foreach (Company.GC.BLL.KDT.HangMauDich hangMd in TKMD.HMDCollection)
                {
                    DataSet dsLuongNplTheoDm = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaPhu, hangMd.SoLuong, TKMD.IDHopDong);
                    foreach (DataRow rowNplTon in dsLuongTon.Tables[0].Rows)
                    {
                        bool ok = false;
                        foreach (DataRow rowNplTheoDm in dsLuongNplTheoDm.Tables[0].Rows)
                        {
                            if (rowNplTon["MaNguyenPhuLieu"].ToString().Trim() == rowNplTheoDm["MaNguyenPhuLieu"].ToString().Trim())
                            {
                                rowNplTon["LuongTon"] = Convert.ToDecimal(rowNplTon["LuongTon"]) - Convert.ToDecimal(rowNplTheoDm["LuongCanDung"]);
                                if (Convert.ToDecimal(rowNplTon["LuongTon"]) < 0)
                                {
                                    if (tenHangBiAm.IndexOf(hangMd.TenHang) < 0)
                                    {
                                        if (tenHangBiAm == "")
                                            tenHangBiAm += hangMd.TenHang;
                                        else
                                            tenHangBiAm += " ," + hangMd.TenHang;
                                    }
                                    ok = true;
                                    break;
                                }
                            }
                        }
                        if (ok)
                            break;
                    }
                }
                if (tenHangBiAm != "")
                {
                    //if (MLMessages("Nguyên phụ liệu tồn trong hệ thống không đủ để sản xuất các hàng sau : " + TenHangBiAm + ". Bạn có muốn lưu không?", "MSG_WRN01", TenHangBiAm, true) != "Yes")

                    //if (showMsg("MSG_WRN01", tenHangBiAm, true) != "Yes")
                    if (Globals.ShowMessageTQDT("Nguyên phụ liệu tồn trong hệ thống không đủ để sản xuất các hàng sau :\r\n " + tenHangBiAm + "\r\n \nBạn có muốn lưu không?", true) != "Yes")
                    {
                        return false;
                    }
                    return true;
                }
            }
            return true;

        }
        private void LoadTKMDData()
        {
            txtSoLuongPLTK.Value = TKMD.SoLuongPLTK;

            ccNgayDK.Text = TKMD.NgayDangKy.Year > 1900 ? TKMD.NgayDangKy.ToShortDateString() : "";

            // Doanh nghiệp.
            txtMaDonVi.Text = TKMD.MaDoanhNghiep;
            txtTenDonVi.Text = TKMD.TenDoanhNghiep;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = TKMD.TenDonViDoiTac;

            #region

            txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

            txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
            txtTenDonViUyThac.Text = this.TKMD.TenDonViUT;

            #endregion
            // To khai thu cong
            txtSoToKhaiGiay.Text = TKMD.ChiTietDonViDoiTac;
            clcNgayDuaVaoTK.Value = TKMD.Ngay_THN_THX;
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = TKMD.MaLoaiHinh;

            // Phương tiện vận tải.
            cbPTVT.SelectedValue = TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = TKMD.SoHieuPTVT;
            if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                ccNgayDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
            else
                ccNgayDen.Text = "";
            // Nước.
            ctrNuocXuatKhau.Ma = TKMD.MaLoaiHinh.Substring(0, 1) == "N" ? TKMD.NuocXK_ID : TKMD.NuocNK_ID;

            // ĐKGH.
            cbDKGH.SelectedValue = TKMD.DKGH_ID;

            // PTTT.
            cbPTTT.SelectedValue = TKMD.PTTT_ID;

            // Giấy phép.
            txtSoGiayPhep.Text = TKMD.SoGiayPhep;
            ccNgayGiayPhep.Text = TKMD.NgayGiayPhep.Year > 1900 ? TKMD.NgayGiayPhep.ToShortDateString() : "";
            ccNgayHHGiayPhep.Text = TKMD.NgayHetHanGiayPhep.Year > 1900 ? TKMD.NgayHetHanGiayPhep.ToShortDateString() : "";
            //if ( TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
            ccNgayHDTM.Text = TKMD.NgayHoaDonThuongMai.Year > 1900 ? TKMD.NgayHoaDonThuongMai.ToShortDateString() : "";
            //if ( TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = TKMD.CuaKhau_ID;

            // Nguyên tệ.
            ctrNguyenTe.Ma = TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = TKMD.TyGiaUSD;

            // Hợp đồng.
            txtSoHopDong.Text = TKMD.SoHopDong;
            ccNgayHopDong.Text = TKMD.NgayHopDong.Year > 1900 ? TKMD.NgayHopDong.ToShortDateString() : "";
            ccNgayHHHopDong.Text = TKMD.NgayHetHanHopDong.Year > 1900 ? TKMD.NgayHetHanHopDong.ToShortDateString() : "";
            //if ( TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

            // Vận tải đơn.
            txtSoVanTaiDon.Text = TKMD.SoVanDon;
            if (TKMD.NgayVanDon.Year > 1900)
            {
                ccNgayVanTaiDon.Value = TKMD.NgayVanDon;
                ccNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString();
            }
            else ccNgayVanTaiDon.Text = "";
            // if ( TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;
            //if ( TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

            // Tên chủ hàng.
            txtTenChuHang.Text = TKMD.TenChuHang;
            txtChucVu.Text = TKMD.ChucVu;
            // Container 20.


            //             // Container 40.
            //             txtSoContainer40.Value = TKMD.SoContainer40;

            // Số kiện hàng.
            txtSoKien.Value = TKMD.SoKien;

            // Trọng lượng.
            txtTrongLuong.Value = TKMD.TrongLuong;

            // Lệ phí HQ.
            txtLePhiHQ.Value = TKMD.LePhiHaiQuan;

            // Phí BH.
            txtPhiBaoHiem.Value = TKMD.PhiBaoHiem;

            // Phí VC.
            txtPhiVanChuyen.Value = TKMD.PhiVanChuyen;

            //Phí ngân hàng
            txtPhiNganHang.Value = TKMD.PhiKhac;

            txtTrongLuongTinh.Value = TKMD.TrongLuongNet;
            //MaMid
            txtMaMid.Text = TKMD.MaMid;

            if (TKMD.ID > 0)
            {
                if (TKMD.HMDCollection == null || TKMD.HMDCollection.Count == 0)
                    TKMD.LoadHMDCollection();
                if (TKMD.ChungTuTKCollection == null || TKMD.ChungTuTKCollection.Count == 0)
                    TKMD.LoadChungTuTKCollection();
                TKMD.LoadChungTuHaiQuan();
                if (TKMD.ListCO == null || TKMD.ListCO.Count == 0)
                    TKMD.LoadCO();

                Hdgc.ID = TKMD.IDHopDong;
                Hdgc = Company.GC.BLL.KDT.GC.HopDong.Load(Hdgc.ID);


                if (this.TKMD.TKTGCollection == null || this.TKMD.TKTGCollection.Count == 0)
                    this.TKMD.LoadTKTGCollection();

                if (this.TKMD.TKTGPP23Collection == null || this.TKMD.TKTGPP23Collection.Count == 0)
                    this.TKMD.LoadToKhaiTriGiaPP23();

                #region Ân hạn thuế

                if (TKMD.AnHanThue.IsAnHan)
                {
                    chkAnHangThue.Checked = TKMD.AnHanThue.IsAnHan;
                    txtAnHanThue_LyDo.Text = TKMD.AnHanThue.LyDoAnHan;
                    txtAnHanThue_ThoiGian.Value = TKMD.AnHanThue.SoNgay;
                }
                #endregion

                #region Đảm bảo nghĩa vụ nộp thuế
                if (TKMD.DamBaoNghiaVuNopThue.IsDamBao)
                {
                    chkDamBaoNopThue_isValue.Checked = TKMD.DamBaoNghiaVuNopThue.IsDamBao;
                    txtDamBaoNopThue_HinhThuc.Text = TKMD.DamBaoNghiaVuNopThue.HinhThuc;
                    txtDamBaoNopThue_TriGia.Value = TKMD.DamBaoNghiaVuNopThue.TriGiaDB;
                    ccDamBaoNopThue_NgayBatDau.Value = TKMD.DamBaoNghiaVuNopThue.NgayBatDau;
                    ccDamBaoNopThue_NgayKetThuc.Value = TKMD.DamBaoNghiaVuNopThue.NgayKetThuc;
                    ccDamBaoNopThue_NgayKetThuc.Text = ccDamBaoNopThue_NgayKetThuc.Value.ToString();
                    ccDamBaoNopThue_NgayBatDau.Text = ccDamBaoNopThue_NgayBatDau.Value.ToString();
                }

                #endregion

                txtTongSoContainer.Value = TKMD.SoLuongContainer.TongSoCont();

                #region Load dữ liệu header Ngonnt 24/02
                txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                ccNgayTN.Value = this.TKMD.NgayTiepNhan;
                txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
                ccNgayDK.Value = this.TKMD.NgayDangKy;
                txtHDPL.Text = this.TKMD.HUONGDAN;
                txtSoTiepNhan.ReadOnly = ccNgayTN.ReadOnly = txtSoToKhai.ReadOnly = ccNgayDK.ReadOnly = txtHDPL.ReadOnly = true;
                #endregion
            }
            dgList.DataSource = TKMD.HMDCollection;
            dgList.Refetch();
            gridEX1.DataSource = TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            if (TKMD.LoaiHangHoa == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
                radHangMau.Checked = false;
            }
            else if (TKMD.LoaiHangHoa == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
                radHangMau.Checked = false;
            }
            else if (TKMD.LoaiHangHoa == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
                radHangMau.Checked = false;
            }
            else if (TKMD.LoaiHangHoa == "H")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = false;
                radHangMau.Checked = true;
            }
            radTB.CheckedChanged += radSP_CheckedChanged;
            radNPL.CheckedChanged += radSP_CheckedChanged;
            radSP.CheckedChanged += radSP_CheckedChanged;
            radHangMau.CheckedChanged += radSP_CheckedChanged;
            //chung tu kem
            txtChungTu.Text = TKMD.GiayTo;
            //so to khai 
            if (TKMD.SoToKhai > 0)
                txtSoToKhai.Text = TKMD.SoToKhai.ToString();

            if (TKMD.SoTiepNhan > 0)
                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();

            txtDeXuatKhac.Text = TKMD.DeXuatKhac;
            txtLyDoSua.Text = TKMD.LyDoSua;

            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = "Chờ duyệt";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Wait for approval"; }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                lblTrangThai.Text = "Chưa khai báo";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not declared yet"; }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                lblTrangThai.Text = "Không được phê duyệt";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not approved"; }
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                lblTrangThai.Text = "Đã duyệt";
                if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                if (TKMD.PhanLuong == "")
                {
                    lblTrangThai.Text = "Đã duyệt";
                    if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                }
                else
                {
                    //xanh
                    if (TKMD.PhanLuong == "1" || TKMD.PhanLuong == "3" || TKMD.PhanLuong == "5")
                    {
                        lblTrangThai.Text = "Tờ khai đã được phân luồng xanh";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Declaration sheet has been integrated into green group  "; }
                    }
                    else if (TKMD.PhanLuong == "2" || TKMD.PhanLuong == "4" || TKMD.PhanLuong == "6" || TKMD.PhanLuong == "10")
                    {
                        lblTrangThai.Text = "Tờ khai đã được phân luồng đỏ";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Declaration sheet has been integrated into red group  "; }
                    }
                    else if (TKMD.PhanLuong == "8" || TKMD.PhanLuong == "12")
                    {
                        lblTrangThai.Text = "Tờ khai đã được phân luồng vàng";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Declaration sheet has been integrated into yellow group  "; }
                    }
                }

            }


            dgToKhaiTriGia.DataSource = this.TKMD.TKTGCollection;
            dglistTKTK23.DataSource = this.TKMD.TKTGPP23Collection;
        }

        #region Khởi tạo dữ liệu trước khi hiển thị

        private void KhoitaoDuLieuChuan()
        {
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = NhomLoaiHinh + "01";

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll().Tables[0];
            cbPTVT.DisplayMember = "Ten";
            cbPTVT.ValueMember = "ID";
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            ctrCuaKhau.Ma = NhomLoaiHinh.StartsWith("N") ? GlobalSettings.DIA_DIEM_DO_HANG : GlobalSettings.CUA_KHAU;
            //txtMaDaiLy.Text = GlobalSettings.MA_DON_VI;
            //txtTenDaiLy.Text = GlobalSettings.TEN_DON_VI;
            //txtMaDonViUyThac.Text = GlobalSettings.MA_DON_VI;
            //txtTenDonViUyThac.Text = GlobalSettings.TEN_DON_VI;

        }

        private void KhoitaoGiaoDienToKhai()
        {
            #region giao diện V4

            txtLePhiHQ.Enabled = GlobalSettings.SendV4;
            //txtTongSoContainer.Enabled = GlobalSettings.SendV4;
            uiDamBaoNghiaVuNT.Visible = GlobalSettings.SendV4;
            uiAnHanThue.Visible = GlobalSettings.SendV4;
            GiayKiemTra.Enabled = GiayKiemTra1.Enabled = GlobalSettings.SendV4 ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo2.Enabled = ChungTuKemTheo.Enabled = !GlobalSettings.SendV4 ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

            #endregion

            txtTenChuHang.Text = GlobalSettings.NguoiLienHe;
            txtChucVu.Text = GlobalSettings.ChucVu;
            txtMaMid.Text = GlobalSettings.MaMID;
            NhomLoaiHinh = NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = NhomLoaiHinh.Substring(0, 1);
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                Text = setText("Tờ khai nhập khẩu", "Import declaration") + "(" + NhomLoaiHinh + ")";
                uiGroupBox11.Visible = true;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = true;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = true;
                //chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = true;
                // chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = true;
                // chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = true;                
                txtMaMid.Visible = lblMaMid.Visible = false;
                CanDoiNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;
                PhanBo.Visible = Janus.Windows.UI.InheritableBoolean.False;
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                grbDiaDiemXepHang.Text = "Địa điểm xếp hàng";
            }
            else
            {
                radHangMau.Visible = false;
                cmdNPLCungUng.Visible = Janus.Windows.UI.InheritableBoolean.True;

                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                // Tiêu đề.
                Text = setText("Tờ khai xuất khẩu", "Export declaration") + "(" + NhomLoaiHinh + ")";
                uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                grbNguoiNK.Text = "Người xuất khẩu";
                grbNguoiXK.Text = "Người nhập khẩu";
                if (GlobalSettings.NGON_NGU == "1")
                {
                    grbNguoiNK.Text = "Exporter";
                    grbNguoiXK.Text = "Importer";
                }

                // Phương tiện vận tải.              
                //uiGroupBox11.Enabled = false;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                //chkHoaDonThuongMai.Visible =
                grbHoaDonThuongMai.Enabled = true;

                // Vận tải đơn.
                //chkVanTaiDon.Visible = 
                grbVanTaiDon.Enabled = true;


                // Nước XK.
                grbNuocXK.Text = "Nước nhập khẩu";
                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                grbDiaDiemDoHang.Text = "Cửa khẩu xuất hàng";
                if (GlobalSettings.NGON_NGU == "1")
                {
                    grbNuocXK.Text = "Import national";
                    grbDiaDiemDoHang.Text = "Export gate";
                }

                //chkDiaDiemXepHang.Enabled = 
                grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = 
                // chkDiaDiemXepHang.Visible = false;
                PhanBo.Visible = Janus.Windows.UI.InheritableBoolean.True;
                CanDoiNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }

            // Loại hình gia công.
            if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
            {
                //chkHopDong.Checked = true;
                //chkHopDong.Visible = false;
                txtSoHopDong.BackColor = ccNgayHopDong.BackColor = ccNgayHHHopDong.BackColor = System.Drawing.Color.FromArgb(255, 255, 192);
                txtSoHopDong.ButtonStyle = EditButtonStyle.Ellipsis;
                if (NhomLoaiHinh == "NGC")
                {
                    radNPL.Checked = true;
                    radSP.Visible = false;
                    radTB.Checked = false;
                }

            }
            //BO SUNG LOAI HINH TAM NHAP - TAI XUAT, 14/10/2011
            else if (this.NhomLoaiHinh.Contains("NTA01") || this.NhomLoaiHinh.Contains("NTA25"))
            {
                radSP.Visible = true;
            }
        }
        #endregion

        private DataTable GetTableCanDoi()
        {
            DataTable tableResult = new DataTable();
            tableResult.Columns.Add("MaSP");
            tableResult.Columns.Add("MaNPL");
            tableResult.Columns.Add("DinhMucChung", typeof(decimal));
            tableResult.Columns.Add("LuongTonDau", typeof(decimal));
            tableResult.Columns.Add("LuongSuDung", typeof(decimal));
            tableResult.Columns.Add("LuongTonCuoi", typeof(decimal));

            if (radSP.Checked)
            {
                DataSet dsLuongTon = NguyenPhuLieu.GetLuongNguyenPhuLieuTon(TKMD.IDHopDong);
                foreach (Company.GC.BLL.KDT.HangMauDich hangMd in TKMD.HMDCollection)
                {
                    if (hangMd.ID > 0)
                    {
                        DataTable dtLuongNPLTheoDM = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMd.MaPhu, hangMd.SoLuong, TKMD.IDHopDong).Tables[0];
                        foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                        }
                    }
                }
                TKMD.HMDCollection.Sort(
                    delegate(HangMauDich hang1, HangMauDich hang2)
                    {
                        return hang1.MaPhu.CompareTo(hang2.MaPhu);
                    }
                    );
                foreach (Company.GC.BLL.KDT.HangMauDich hangMD in TKMD.HMDCollection)
                {
                    DataTable dtLuongNPLTheoDM = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMD.MaPhu, hangMD.SoLuong, TKMD.IDHopDong).Tables[0];
                    foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                    {
                        try
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];
                            if (rowTon != null)
                            {
                                DataRow rowNew = tableResult.NewRow();
                                rowNew["MaSP"] = hangMD.MaPhu.Trim();
                                rowNew["MaNPL"] = row["MaNguyenPhuLieu"].ToString().Trim();
                                rowNew["DinhMucChung"] = row["dinhMucChung"];
                                rowNew["LuongTonDau"] = rowTon["LuongTon"];
                                rowNew["LuongSuDung"] = row["LuongCanDung"];
                                rowNew["LuongTonCuoi"] = Convert.ToDecimal(rowTon["LuongTon"]) - Convert.ToDecimal(row["LuongCanDung"]);
                                rowTon["LuongTon"] = rowNew["LuongTonCuoi"];
                                tableResult.Rows.Add(rowNew);
                            }
                        }
                        catch
                        {
                            //MLMessages("Nguyên phụ liệu có mã : " + row["MaNguyenPhuLieu"] + " không có trong hệ thống.Hãy cập nhật lại mã này.", "MSG_240247", row["MaNguyenPhuLieu"].ToString(), false);
                            //showMsg("MSG_240247", row["MaNguyenPhuLieu"].ToString());
                            Globals.ShowMessageTQDT(row["MaNguyenPhuLieu"].ToString(), false);
                            return null;
                        }
                    }
                }
            }
            return tableResult;

        }

        private void ToKhaiNhapKhauSxxkFormLoad(object sender, EventArgs e)
        {
            try
            {
                if (isCopy && TKMD.ID == 0 && TKMD.NPLCungUngs != null && TKMD.NPLCungUngs.Count > 0)
                {
                    foreach (Company.GC.BLL.KDT.GC.NPLCungUng item in TKMD.NPLCungUngs)
                    {
                        item.NPLCungUngDetails.Clear();
                    }
                    TKMD.NPLCungUngs.Clear();
                    isCopy = false;
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

            if (TKMD.ID == 0) TKMD.PhanLuong = "-1";
            ctrLoaiHinhMauDich.ValueChanged += new Company.Interface.Controls.LoaiHinhMauDichVControl.ValueChangedEventHandler(ctrLoaiHinhMauDich_ValueChanged);

            if (this.NhomLoaiHinh.Substring(0, 1).Equals("N"))
            {
                cmdVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (this.NhomLoaiHinh.Substring(0, 1).Equals("X"))
            {
                cmdVanDon.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

            GlobalSettings.LOAI_HINH = TKMD.MaLoaiHinh;

            txtDeXuatKhac.Enabled = true;

            radTB.CheckedChanged -= radSP_CheckedChanged;
            radNPL.CheckedChanged -= radSP_CheckedChanged;
            radSP.CheckedChanged -= radSP_CheckedChanged;
            radHangMau.CheckedChanged -= radSP_CheckedChanged;

            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
            txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
            ccNgayDen.Value = DateTime.Today;
            KhoitaoDuLieuChuan();
            KhoitaoGiaoDienToKhai();
            switch (this.OpenType)
            {
                case OpenFormType.View:
                    LoadTKMDData();
                    //ViewTKMD();
                    this.NhomLoaiHinh = TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case OpenFormType.Edit:
                    LoadTKMDData();
                    this.NhomLoaiHinh = TKMD.MaLoaiHinh.Substring(0, 3);
                    break;
                case OpenFormType.Insert:
                    txtTyGiaUSD.Text = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.GetTyGia("USD").ToString();
                    break;
            }

            SetCommandStatus();

            radTB.CheckedChanged += new System.EventHandler(radSP_CheckedChanged);
            radNPL.CheckedChanged += new System.EventHandler(radSP_CheckedChanged);
            radSP.CheckedChanged += new System.EventHandler(radSP_CheckedChanged);
            if (TKMD.ID > 0)
            {
                var sendXml = new MsgSend { LoaiHS = LoaiKhaiBao.ToKhai, master_id = TKMD.ID };
                if (sendXml.Load())
                {
                    lblTrangThai.Text = "Đang chờ xác nhận hải quan";
                    if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Waiting for confirmation";
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = XacNhan1.Enabled = XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
            }
            else
            {
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            if (GlobalSettings.TuDongTinhThue == "0")
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleToKhai.KhaiDienTu)))
            //{
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //    TopRebar1.Visible = false;
            //}    

            TinhTongTriGiaKhaiBao();
            if (string.IsNullOrEmpty(ctrNuocXuatKhau.Ma)) ctrNuocXuatKhau.Ma = "VN";

            //TODO: Hungtq updated 6/2/2013. Form_Load()
            //if (TKMD.ID == 0) //La to khai moi
            //{
            //    if (Company.KDT.SHARE.Components.Globals.IsKTX)
            //        tkmdVersionControlV2.Version = 1;
            //    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
            //        tkmdVersionControlV2.Version = 2;
            //    else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && !GlobalSettings.SendV4)
            //        tkmdVersionControlV2.Version = 3;
            //    else if (GlobalSettings.SendV4) //Mac dinh V4
            //        tkmdVersionControlV2.Version = 4;
            //    else
            //        tkmdVersionControlV2.Version = 0;

            //    tkmdVersionControlV2.IsCKS = (Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature);
            //    tkmdVersionControlV2.LoadInfo(TKMD.ID);
            //}
            //else
            //{
            //    tkmdVersionControlV2 = new Company.KDT.SHARE.Components.Controls.TKMDVersionControlV();
            //    tkmdVersionControlV2.LoadInfo(TKMD.ID);
            //}
            if (TKMD.MaLoaiHinh.Contains("V"))
            {
                try
                {
                    TKMD.SoHopDong = Company.GC.BLL.KDT.GC.HopDong.Load(TKMD.IDHopDong).SoHopDong;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }

            }
        }

        private void ctrLoaiHinhMauDich_ValueChanged(object sender, EventArgs e)
        {
            //BO SUNG LOAI HINH TAM NHAP - TAI XUAT, 14/10/2011
            if (this.ctrLoaiHinhMauDich.Ma == "NTA01" || this.ctrLoaiHinhMauDich.Ma == "NTA25")
            {
                radSP.Visible = true;
            }
        }

        private void CtrNguyenTeValueChanged(object sender, EventArgs e)
        {
            txtTyGiaTinhThue.Text = ctrNguyenTe.TyGia.ToString();
        }

        #region Xem tờ khai mậu dịch
        private void ViewTKMD()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            txtSoLuongPLTK.ReadOnly = true;
            cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;
            ccNgayDK.ReadOnly = true;
            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;
            txtChucVu.ReadOnly = true;

            // Container 20.
            txtTongSoContainer.ReadOnly = true;

            //             // Container 40.
            //             txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }
        #endregion

        #region Đọc file Excell
        private void ReadExcel()
        {
            if (NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                bool isValid = cvError.IsValid;
            }
            else
            {
                cvError.ContainerToValidate = txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                bool b = valid1 && valid2;
            }
            if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
            {
                epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid tax assessment rate"));
                epError.SetIconPadding(txtTyGiaTinhThue, -8);
                return;

            }
            if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
            {
                if (txtSoHopDong.Text == "")
                {
                    epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "Contract no. must be filled"));
                    epError.SetIconPadding(txtSoHopDong, -8);
                    return;
                }
                epError.Clear();
            }
            if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) TKMD.LoaiHangHoa = "T";
            if (radHangMau.Checked) TKMD.LoaiHangHoa = "H";
            var reform = new ReadExcelForm
                             {
                                 TiGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value),
                                 TKMD = TKMD
                             };
            reform.ShowDialog();
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            SetCommandStatus();
        }
        #endregion

        #region Thêm hàng
        private void ThemHang()
        {
            try
            {
                AddDataToToKhai();

                if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
                {
                    if (txtSoHopDong.Text == "")
                    {
                        epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "Contract no. must be filled"));
                        epError.SetIconPadding(txtSoHopDong, -8);
                        return;
                    }
                    epError.Clear();
                }
                cvError.ContainerToValidate = grbNguyenTe;
                cvError.Validate();
                if (!cvError.IsValid) return;
                if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
                if (radSP.Checked) TKMD.LoaiHangHoa = "S";
                if (radTB.Checked) TKMD.LoaiHangHoa = "T";
                if (radHangMau.Checked) TKMD.LoaiHangHoa = "H";
                HangMauDichForm f = new HangMauDichForm();
                f.OpenType = OpenFormType.Edit;
                f.TKMD = this.TKMD;
                f.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                f.NhomLoaiHinh = this.NhomLoaiHinh;
                if (radNPL.Checked) f.LoaiHangHoa = "N";
                if (radSP.Checked) f.LoaiHangHoa = "S";
                if (radTB.Checked) f.LoaiHangHoa = "T";
                if (radHangMau.Checked) f.LoaiHangHoa = "H";
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //f.HMDCollection = this.TKMD.HMDCollection;
                f.MaNguyenTe = ctrNguyenTe.Ma;
                f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
                this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
                f.TKMD = this.TKMD;
                f.ShowDialog();
                try
                {
                    dgList.DataSource = TKMD.HMDCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
                txtSoLuongPLTK.Text = TKMD.SoLuongPLTK.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new SendEventArgs(ex));
            }
        }
        #endregion

        #region Đưa dữ liệu vào tờ khai
        private void AddDataToToKhai()
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
            {
                epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid tax assessment rate"));
                epError.SetIconPadding(txtTyGiaTinhThue, -8);
                return;

            }
            TKMD.SoTiepNhan = TKMD.SoTiepNhan;
            TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
            }
            TKMD.SoHang = (short)TKMD.HMDCollection.Count;
            this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);

            // Doanh nghiệp.
            TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

            // Đơn vị đối tác.
            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


            TKMD.MaDonViUT = txtMaDonViUyThac.Text;
            TKMD.TenDonViUT = txtTenDonViUyThac.Text;

            // Đại lý TTHQ.
            TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
            TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

            // Loại hình mậu dịch.
            TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

            // Giấy phép.
            if (txtSoGiayPhep.Text.Trim().Length > 0)
            {
                TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                TKMD.NgayGiayPhep = !ccNgayGiayPhep.IsNullDate ? ccNgayGiayPhep.Value : DateTime.Parse("01/01/1900");
                TKMD.NgayHetHanGiayPhep = !ccNgayHHGiayPhep.IsNullDate ? ccNgayHHGiayPhep.Value : DateTime.Parse("01/01/1900");
            }
            else
            {
                TKMD.SoGiayPhep = "";
                TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
            }

            // 7. Hợp đồng.
            if (txtSoHopDong.Text.Trim().Length > 0)
            {
                TKMD.SoHopDong = txtSoHopDong.Text;
                // Ngày HD.
                TKMD.NgayHopDong = ccNgayHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHopDong.Value;
                // Ngày hết hạn HD.
                TKMD.NgayHetHanHopDong = ccNgayHHHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHHHopDong.Value;
            }
            else
            {
                TKMD.SoHopDong = "";
                TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
            }

            // Hóa đơn thương mại.
            if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
            {
                TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                TKMD.NgayHoaDonThuongMai = ccNgayHDTM.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHDTM.Value;
            }
            else
            {
                TKMD.SoHoaDonThuongMai = "";
                TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
            }
            // Phương tiện vận tải.
            TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
            TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
            TKMD.NgayDenPTVT = ccNgayDen.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayDen.Value;

            // Vận tải đơn.
            if (txtSoVanTaiDon.Text.Trim().Length > 0)
            {
                TKMD.SoVanDon = txtSoVanTaiDon.Text;
                TKMD.NgayVanDon = ccNgayVanTaiDon.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayVanTaiDon.Value;
            }
            else
            {
                TKMD.SoVanDon = "";
                TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
            }
            // Nước.
            if (NhomLoaiHinh.Substring(0, 1) == "N")
            {
                TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                TKMD.NuocNK_ID = "VN".PadRight(3);
            }
            else
            {
                TKMD.NuocXK_ID = "VN".PadRight(3);
                TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
            }

            // Địa điểm xếp hàng.
            //if (chkDiaDiemXepHang.Checked)
            TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
            // else
            //     TKMD.DiaDiemXepHang = "";

            // Địa điểm dỡ hàng.
            TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

            // Điều kiện giao hàng.
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            // Đồng tiền thanh toán.
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

            // Phương thức thanh toán.
            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

            //
            TKMD.TenChuHang = txtTenChuHang.Text;
            TKMD.ChucVu = txtChucVu.Text;
            TKMD.SoContainer20 = Convert.ToDecimal(txtTongSoContainer.Text);
            /*  TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);*/
            TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
            TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

            // Tổng trị giá khai báo (Chưa tính).
            TKMD.TongTriGiaKhaiBao = 0;
            //
            if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                TKMD.TrangThaiXuLy = -1;
            }
            TKMD.LoaiToKhaiGiaCong = "NPL";
            //
            TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
            TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
            TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
            TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

            // tkmd.NgayGui = DateTime.Parse("01/01/1900");
            if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) TKMD.LoaiHangHoa = "T";

            TKMD.GiayTo = txtChungTu.Text;
            TKMD.MaMid = txtMaMid.Text.Trim();
        }
        #endregion

        #region Lưu thông tin tờ khai

        private Company.GC.BLL.KDT.HangMauDich FindHangMauDichBy(List<HangMauDich> list, long idTKMD, string maPhu)
        {
            foreach (Company.GC.BLL.KDT.HangMauDich item in list)
            {
                if (item.TKMD_ID == idTKMD && item.MaPhu == maPhu)
                {
                    return item;
                }
            }

            return null;
        }

        //-----------------------------------------------------------------------------------------
        //DATLMQ bổ sung 2 Collection phục vụ việc Thêm, Xóa hàng khi sửa TKMD ngày 12/08/2011
        public static List<HangMauDich> deleteHMDCollection = new List<HangMauDich>();
        public static List<HangMauDich> insertHMDCollection = new List<HangMauDich>();
        //-----------------------------------------------------------------------------------------
        private void AutoInsertHMDToTKEdit(string _maHS_Old, string _tenHang_Old, string _maHang_Old, string _xuatXu_Old, decimal _soLuong_Old, string _dvt_Old, decimal _donGiaNT_Old, decimal _triGiaNT_Old, ToKhaiMauDich toKhaiMauDich)
        {
            try
            {
                //Company.KD.BLL.KDT.HangMauDich HMD_Edit = new Company.KD.BLL.KDT.HangMauDich();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
                List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();
                Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail noiDungEditDetail = new Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail();
                HangMauDichEditForm hmdEditForm = new HangMauDichEditForm();
                if (toKhaiMauDich.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    #region 21. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "21. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHS_Edit = _maHS_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHS_Edit = _maHS_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "20. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            tenHang_Edit = _tenHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                tenHang_Edit = _tenHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 20. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "20. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                            maHang_Edit = _maHang_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                                maHang_Edit = _maHang_Old;
                            }
                        }
                    }
                    #endregion

                    #region 22. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "22. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                            xuatXu_Edit = _xuatXu_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                                xuatXu_Edit = _xuatXu_Old;
                            }
                        }
                    }
                    #endregion

                    #region 23. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "23. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            soLuong_Edit = _soLuong_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                soLuong_Edit = _soLuong_Old;
                            }
                        }
                    }
                    #endregion

                    #region 24. Đơn vị tính
                    if (dvt_Edit != _dvt_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "24. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                            dvt_Edit = _dvt_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                                dvt_Edit = _dvt_Old;
                            }
                        }
                    }
                    #endregion

                    #region 25. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "25. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            dongiaNT_Edit = _donGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                dongiaNT_Edit = _donGiaNT_Old;
                            }
                        }
                    }
                    #endregion

                    #region 26. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "26. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                            trigiaNT_Edit = _triGiaNT_Old;
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                                trigiaNT_Edit = _triGiaNT_Old;
                            }
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 18. Mã HS
                    if (maHS_Edit != _maHS_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHSHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHSHMD";
                            noiDungTK.Ten = "18. Mã số hàng hóa";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHS_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHS_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Tên hàng, Quy cách phẩm chất
                    if (tenHang_Edit != _tenHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TenHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TenHang";
                            noiDungTK.Ten = "17. Tên hàng, quy cách phẩm chất";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _tenHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + tenHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 17. Mã hàng
                    if (maHang_Edit != _maHang_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "MaHang";
                            noiDungTK.Ten = "17. Mã hàng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _maHang_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + maHang_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 19. Xuất xứ
                    if (xuatXu_Edit.Trim() != _xuatXu_Old.Trim())
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "NuocXuatXuHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "NuocXuatXuHMD";
                            noiDungTK.Ten = "19. Xuất xứ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _xuatXu_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + xuatXu_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 20. Số lượng
                    if (soLuong_Edit != _soLuong_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "SoLuongHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "SoLuongHMD";
                            noiDungTK.Ten = "20. Số lượng";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _soLuong_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + soLuong_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 21. Đơn vị tính
                    if (dvt_Edit != _dvt_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonViTinhHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonViTinhHMD";
                            noiDungTK.Ten = "21. Đơn vị tính";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _dvt_Old;
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dvt_Edit;
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 22. Đơn giá nguyên tệ
                    if (dongiaNT_Edit != _donGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "DonGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "DonGiaNTHMD";
                            noiDungTK.Ten = "22. Đơn giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _donGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + dongiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion

                    #region 23. Trị giá nguyên tệ
                    if (trigiaNT_Edit != _triGiaNT_Old)
                    {
                        //isEdited = true;
                        listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "TriGiaNTHMD" + "' " + " AND MaLoaiHinh = '" + toKhaiMauDich.MaLoaiHinh + "'", null);
                        if (listNoiDungTK.Count == 0)
                        {
                            noiDungTK.Ma = "TriGiaNTHMD";
                            noiDungTK.Ten = "23. Trị giá nguyên tệ";
                            noiDungTK.MaLoaiHinh = toKhaiMauDich.MaLoaiHinh;
                            noiDungTK.InsertUpdate();

                            noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungEditDetail.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                            noiDungEditDetail.NoiDungTKSua = noiDungTK.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                            noiDungEditDetail.InsertUpdate();
                        }
                        else
                        {
                            foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai ndtk in listNoiDungTK)
                            {
                                noiDungEditDetail.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                                noiDungEditDetail.NoiDungTKChinh = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + _triGiaNT_Old.ToString();
                                noiDungEditDetail.NoiDungTKSua = ndtk.Ten + " của dòng hàng số " + soDongHang.ToString() + ": " + trigiaNT_Edit.ToString();
                                noiDungEditDetail.InsertUpdate();
                            }
                        }
                    }
                    #endregion
                }
            }
            catch
            {
                ShowMessage("Có lỗi trong quá trình ghi dữ liệu vào tờ khai sửa đổi, bổ sung", false);
            }
        }

        private bool checkHMDEsistsInTKMD(long _TKMD_ID, string _maHang)
        {
            Company.GC.BLL.KDT.HangMauDich hmd = new Company.GC.BLL.KDT.HangMauDich();
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _maHang);
            List<HangMauDich> hmdColl = (List<HangMauDich>)Company.GC.BLL.KDT.HangMauDich.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }

        //DATLMQ bổ sung AutoInsertAddHMD ngày 12/08/2011
        private void AutoInsertAddHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            foreach (Company.GC.BLL.KDT.HangMauDich hangMD in TKMD.HMDCollection)
            {
                if (!checkHMDEsistsInTKMD(TKMD.ID, hangMD.MaPhu))
                {
                    insertHMDCollection.Add(hangMD);
                    listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                    if (listNoiDungTK.Count == 0)
                    {
                        noiDungTK.Ma = "MaHang";
                        noiDungTK.Ten = "20. Mã hàng";
                        noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                        noiDungTK.InsertUpdate();

                        foreach (Company.GC.BLL.KDT.HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                    else
                    {
                        foreach (HangMauDich hang in insertHMDCollection)
                        {
                            noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                            noiDungDetails.NoiDungTKChinh = "";
                            noiDungDetails.NoiDungTKSua = "Thêm mới Mã hàng " + hang.MaPhu + " vào Tờ khai số " + TKMD.SoToKhai + " - Mã loại hình: " + TKMD.MaLoaiHinh + " - Ngày đăng ký: " + TKMD.NgayDangKy.ToShortDateString();
                            noiDungDetails.InsertUpdate();
                        }
                    }
                }
            }
        }

        //DATLMQ bổ sung AutoInsertDeleteHMD ngày 12/08/2011
        private void AutoInsertDeleteHMD()
        {
            Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai noiDungTK = new Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungDetails = new NoiDungDieuChinhTKDetail();
            List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai> listNoiDungTK = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai>();

            if (isDeleted)
            {
                listNoiDungTK = Company.KDT.SHARE.QuanLyChungTu.NoiDungToKhai.SelectCollectionDynamic("Ma = '" + "MaHang" + "' " + " AND MaLoaiHinh = '" + TKMD.MaLoaiHinh + "'", null);
                if (listNoiDungTK.Count == 0)
                {
                    noiDungTK.Ma = "MaHang";
                    noiDungTK.Ten = "20. Mã hàng";
                    noiDungTK.MaLoaiHinh = TKMD.MaLoaiHinh;
                    noiDungTK.InsertUpdate();

                    foreach (Company.GC.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin " + noiDungTK.Ten + " của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
                else
                {
                    foreach (Company.GC.BLL.KDT.HangMauDich hang in deleteHMDCollection)
                    {
                        noiDungDetails.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungDetails.NoiDungTKChinh = "Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.NoiDungTKSua = "Xóa thông tin Mã hàng của dòng hàng số " + hang.SoThuTuHang.ToString() + ": " + hang.MaPhu;
                        noiDungDetails.InsertUpdate();
                    }
                }
            }
        }

        //-----------------------------------------------------------------------------------------       
        public void Save()
        {
            bool valid;
            bool isKhaibaoSua = TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ? true : false;
            #region Dung cho to khai thu cong tơ khai dua vao thanh khoan
            if (Company.KDT.SHARE.Components.Globals.IsKTX)
            {
                TKMD.ChiTietDonViDoiTac = txtSoToKhaiGiay.Text;// luu so to khai giấy
                TKMD.Ngay_THN_THX = clcNgayDuaVaoTK.Value;// Luu ngay dua vao thanh khoan
            }
            #endregion
            if (NhomLoaiHinh.Substring(0, 1) == "N")
            {
                //Neu to khai da luu thi kiem tra chung tu Van tai don
                if (TKMD != null)
                {
                    valid = ValidateControl.ValidateNull(txtSoVanTaiDon, epError, "Số vận đơn");
                    if (!valid)
                    {
                        txtSoVanTaiDon.Focus();
                        ShowMessage("Bạn chưa nhập thông tin 'Vận tải đơn' trên tờ khai chính", false);
                        return;
                    }

                    valid = ValidateControl.ValidateNull(txtSoHieuPTVT, epError, "Số hiệu phương tiện vận tải");
                    if (!valid)
                    {
                        txtSoVanTaiDon.Focus();
                        ShowMessage("Bạn chưa nhập thông tin 'Số hiệu phương tiện vận tải' trên tờ khai chính", false);
                        return;
                    }

                    valid = ValidateControl.ValidateNull(ccNgayDen, epError, "Ngày đến phương tiện vận tải");
                    if (!valid)
                    {
                        txtSoVanTaiDon.Focus();
                        ShowMessage("Bạn chưa nhập thông tin 'Ngày đến phương tiện vận tải' trên tờ khai chính", false);
                        return;
                    }
                }

                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                //cvError.ContainerToValidate = txtTenDonViDoiTac;
                //cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }
            valid &= ValidateControl.ValidateNull(txtTenChuHang, epError, "Đại diện doanh nghiệp");

            #region Ân hạng thuế / Đảm bảo nghĩa vụ nộp thuế

            if (chkDamBaoNopThue_isValue.Checked)
            {
                valid &= ValidateControl.ValidateNull(ccDamBaoNopThue_NgayBatDau, epError, "Ngày bắt đầu được đảm bảo phải nhập");
                valid &= ValidateControl.ValidateNull(ccDamBaoNopThue_NgayKetThuc, epError, "Ngày kết thúc được đảm bảo phải nhập");
            }
            #endregion

            if (valid)
            {

                #region Ân hạng thuế / Đảm bảo nghĩa vụ nộp thuế
                TKMD.AnHanThue.IsAnHan = chkAnHangThue.Checked;
                if (chkAnHangThue.Checked)
                {
                    TKMD.AnHanThue.LyDoAnHan = txtAnHanThue_LyDo.Text.Trim();
                    TKMD.AnHanThue.SoNgay = Convert.ToInt32(txtAnHanThue_ThoiGian.Value);
                }
                TKMD.DamBaoNghiaVuNopThue.IsDamBao = chkDamBaoNopThue_isValue.Checked;
                if (chkDamBaoNopThue_isValue.Checked)
                {
                    TKMD.DamBaoNghiaVuNopThue.HinhThuc = txtDamBaoNopThue_HinhThuc.Text.Trim();
                    TKMD.DamBaoNghiaVuNopThue.NgayBatDau = ccDamBaoNopThue_NgayBatDau.Value;
                    TKMD.DamBaoNghiaVuNopThue.NgayKetThuc = ccDamBaoNopThue_NgayKetThuc.Value;
                    TKMD.DamBaoNghiaVuNopThue.TriGiaDB = Convert.ToDecimal(txtDamBaoNopThue_TriGia.Value);
                }
                #endregion

                if (TKMD.HMDCollection.Count == 0)
                {
                    Globals.ShowMessageTQDT("Thông báo", "Chưa nhập thông tin hàng của tờ khai", false);
                    return;
                }
                if (Hdgc == null)
                {
                    Globals.ShowMessageTQDT("Thông báo", "Hợp đồng không hợp lệ", false);
                    return;
                }
                if (TKMD.ID > 0)
                {
                    if (TKMD.MaLoaiHinh.Contains("X"))
                    {
                        if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                        {
                            Globals.ShowMessageTQDT("Thông báo", "Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                            return;
                        }
                    }
                }
                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "Invalid tax assessment rate"));
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;

                }
                if (!KiemTraLuongTon())
                    return;
                Hdgc = Company.GC.BLL.KDT.GC.HopDong.Load(Hdgc.ID);

                #region Gan gia tri to khai

                TKMD.SoTiepNhan = TKMD.SoTiepNhan;
                TKMD.SoToKhai = TKMD.SoToKhai;
                //TKMD.ActionStatus = 0;
                if (!isKhaibaoSua)
                {
                    if (TKMD.SoTiepNhan > 0)
                    {
                        TKMD.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                        TKMD.ActionStatus = 0;
                    }
                    else
                    {
                        TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        TKMD.ActionStatus = -1;
                        TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                    }

                    if (TKMD.SoToKhai > 0)
                        TKMD.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    else
                    {
                        TKMD.PhanLuong = "";
                        TKMD.HUONGDAN = "";
                        TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                    }
                }
                // Đề xuất khác
                TKMD.DeXuatKhac = txtDeXuatKhac.Text;
                //Lý do sửa
                if (isKhaibaoSua)
                    TKMD.LyDoSua = txtLyDoSua.Text;
                else
                    TKMD.LyDoSua = "";
                // TKMD.MaHaiQuan = HDGC.MaHaiQuan;
                TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                TKMD.SoHang = (short)TKMD.HMDCollection.Count;
                this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);

                // Doanh nghiệp.
                TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                // Đại lý TTHQ.
                TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                // Loại hình mậu dịch.
                TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                // Giấy phép.
                if (txtSoGiayPhep.Text.Trim().Length > 0)
                {
                    TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    TKMD.NgayGiayPhep = !ccNgayGiayPhep.IsNullDate ? ccNgayGiayPhep.Value : DateTime.Parse("01/01/1900");
                    TKMD.NgayHetHanGiayPhep = !ccNgayHHGiayPhep.IsNullDate ? ccNgayHHGiayPhep.Value : DateTime.Parse("01/01/1900");
                }
                else
                {
                    TKMD.SoGiayPhep = "";
                    TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    TKMD.NgayHopDong = ccNgayHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    TKMD.NgayHetHanHopDong = ccNgayHHHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHHHopDong.Value;
                }
                else
                {
                    TKMD.SoHopDong = "";
                    TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                {
                    TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    TKMD.NgayHoaDonThuongMai = ccNgayHDTM.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHDTM.Value;
                }
                else
                {
                    TKMD.SoHoaDonThuongMai = "";
                    TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }
                // Phương tiện vận tải.
                TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                TKMD.NgayDenPTVT = ccNgayDen.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayDen.Value;

                // Vận tải đơn.
                if (txtSoVanTaiDon.Text.Trim().Length > 0)
                {
                    TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    TKMD.NgayVanDon = ccNgayVanTaiDon.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayVanTaiDon.Value;
                }
                else
                {
                    TKMD.SoVanDon = "";
                    TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }
                // Nước.
                if (NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma.Trim();
                    TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    TKMD.NuocXK_ID = "VN".PadRight(3);
                    TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma.Trim();
                }

                // Địa điểm xếp hàng.
                //if (chkDiaDiemXepHang.Checked)
                TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                // else
                //     TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                TKMD.TenChuHang = txtTenChuHang.Text;
                TKMD.ChucVu = txtChucVu.Text;
                TKMD.SoContainer20 = Convert.ToDecimal(txtTongSoContainer.Text);
                //TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                if (GlobalSettings.TuDongTinhThue == "1" && TKMD.MaLoaiHinh.Contains("N"))
                    TinhLaiThue();
                else
                {
                    TinhTongTriGiaKhaiBao();
                }
                //
                if (!isKhaibaoSua)
                    TKMD.TrangThaiXuLy = -1;
                TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                // tkmd.NgayGui = DateTime.Parse("01/01/1900");
                if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
                if (radSP.Checked) TKMD.LoaiHangHoa = "S";
                if (radTB.Checked) TKMD.LoaiHangHoa = "T";

                TKMD.GiayTo = txtChungTu.Text;

                TKMD.MaMid = txtMaMid.Text.Trim();

                #endregion

                #region Luu to khai

                try
                {
                    #region Kiem tra khai bao sua to khai

                    if (isKhaibaoSua)
                    {
                        if (txtLyDoSua.Text.Trim() == "")
                        {
                            MLMessages("Chưa nhập lý do sửa tờ khai", "MSG_SAV06", "", false);
                            return;
                        }
                        try
                        {
                            List<Company.KDT.SHARE.Components.Message> tokhaiGocTruocKhiSuas = Company.KDT.SHARE.Components.Message.LayToKhaiGocTruocKhiSua_ItemID(TKMD.ID);

                            if (tokhaiGocTruocKhiSuas.Count == 0)
                            {
                                MLMessages("*** LƯU Ý ***:\r\nKhông tự động tạo được nội dung thông tin thay đổi mới cho tờ khai sửa này, do chưa lưu thông tin tờ khai gốc trước khi sửa tờ khai.", "MSG_SAV06", "", false);
                            }
                            else
                            {
                                NoiDungToKhai noiDungTK = new NoiDungToKhai();
                                List<NoiDungToKhai> listNoiDungTK = new List<NoiDungToKhai>();
                                NoiDungDieuChinhTKDetail noiDungEditDetail = new NoiDungDieuChinhTKDetail();
                                NoiDungDieuChinhTK noiDungEdit = new NoiDungDieuChinhTK();
                                noiDungEdit.TKMD_ID = TKMD.ID;
                                noiDungEdit.SoTK = TKMD.SoToKhai;
                                noiDungEdit.NgayDK = TKMD.NgayDangKy;
                                noiDungEdit.MaLoaiHinh = TKMD.MaLoaiHinh;
                                noiDungEdit.NgaySua = DateTime.Now;
                                i = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKMD.ID, TKMD.MaLoaiHinh);
                                if (i == 0)
                                {
                                    i++;
                                    noiDungEdit.SoDieuChinh = i;
                                    noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                    noiDungEdit.InsertUpdate();
                                    luu = true;
                                }
                                else
                                {
                                    if (luu == false)
                                    {
                                        i++;
                                        noiDungEdit.SoDieuChinh = i;
                                        noiDungEdit.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                        noiDungEdit.InsertUpdate();
                                        luu = true;
                                    }
                                    else
                                    {
                                        //Xoa thong tin cu cua lan dieu chinh
                                        NoiDungChinhSuaTKForm ndDieuChinhTKForm = new NoiDungChinhSuaTKForm();
                                        ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
                                        //foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
                                        //{
                                        //    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET && ndDieuChinhTK.SoDieuChinh == i)
                                        //    {
                                        //        Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTKDetail.DeleteBy_Id_DieuChinh(ndDieuChinhTK.ID);
                                        //    }
                                        //}
                                    }
                                }


                                //Lay thong tin to khai goc da luu truoc khi sua
                                ToKhaiMauDich TKMD_Goc = Helpers.Deserialize<ToKhaiMauDich>(tokhaiGocTruocKhiSuas[0].MessageContent);

                                //So sanh noi dung cua to khai goc va to khai sua
                                bool ok = SoSanhNoiDungSuaDoiToKhai(TKMD_Goc, TKMD);

                                if (!ok)
                                {
                                    ShowMessage("Lỗi trong quá trình lưu tờ khai sửa.", false);
                                }

                            }
                            if (TKMD.MaLoaiHinh.Contains("V"))
                            {
                                TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                                TKMD.ActionStatus = 5000;
                                Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiNhap;
                                if (TKMD.MaLoaiHinh.Substring(0, 1) == "X")
                                    msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiXuat;
                                Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty, TKMD.ID, TKMD.GUIDSTR,
                           msgType,
                           Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                           Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay + string.Format(" từ trạng thái {0} -> {1} để cập nhật lại dữ liệu tờ khai sửa", 5, Company.KDT.SHARE.Components.Globals.GetTrangThaiXuLyTK(TKMD.TrangThaiXuLy)),
                           string.Format("Số tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}\r\nPhân luồng: {4}", TKMD.SoToKhai, TKMD.NgayDangKy.ToShortDateString(), TKMD.MaLoaiHinh.Trim(), TKMD.MaHaiQuan.Trim(), Company.KDT.SHARE.Components.Globals.GetPhanLuong(TKMD.PhanLuong)));

                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi trong quá trình lưu tờ khai sửa: " + ex.Message, false);
                        }
                    }

                    #endregion

                    //Khanh - 23/08/2012, Không copy NPL cung ứng

                    //HungTQ, Update Nam dang ky.
                    TKMD.NamDK = DateTime.Now.Year;
                    if (string.IsNullOrEmpty(TKMD.GUIDSTR)) TKMD.GUIDSTR = Guid.NewGuid().ToString();

                    #region Kiêm tra thông tin trùng tờ khai
                    //try
                    //{
                    //    decimal SoLuongHang = 0;
                    //    foreach (HangMauDich item in TKMD.HMDCollection)
                    //    {
                    //        SoLuongHang = SoLuongHang + item.SoLuong;
                    //    }
                    //    long IDTrung = TrungToKhai.CheckToKhaiTrung(TKMD.ID, TKMD.MaLoaiHinh, TKMD.SoGiayPhep, TKMD.SoVanDon, TKMD.SoHoaDonThuongMai, TKMD.SoHopDong, TKMD.SoKien, TKMD.TrongLuong, SoLuongHang, TKMD.HMDCollection.Count);
                    //    if (IDTrung > 0)
                    //    {
                    //        TKMD.QuanLyMay = true;
                    //        if (ShowMessage("Tờ khai này trùng thông tin với tờ khai có ID = " + IDTrung + ". Bạn có muốn lưu lại ?", true) != "Yes")
                    //            return;

                    //    }
                    //    else
                    //        TKMD.QuanLyMay = false;
                    //}
                    //catch (System.Exception ex)
                    //{
                    //    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //    if (ShowMessage("Lỗi kiểm tra trùng tờ khai : " + ex + ". Bạn có muốn tiếp tục lưu tờ khai ?", true) != "Yes")
                    //        return;
                    //}


                    #endregion

                    TKMD.InsertUpdateFull();

                    //Neu la chinh sua, cap nhat lai ID luu tam la ID cua TKMD dang mo: tranh bi copy trung du lieu cua TK cu.
                    if (BNew == false)
                        PtkmdId = TKMD.ID;

                    //Lypt update date 22/01/2010
                    if (BNew)
                    {
                        //Cap nhat lai trang thai sau khi them moi
                        BNew = false;

                        //Copy Van don                        
                        List<VanDon> VanDoncollection = new List<VanDon>();
                        VanDoncollection = (List<VanDon>)VanDon.SelectCollectionBy_TKMD_ID(PtkmdId);
                        foreach (VanDon vd in VanDoncollection)
                        {
                            long vdResult = VanDon.InsertVanDon(vd.SoVanDon, vd.NgayVanDon, vd.HangRoi, vd.SoHieuPTVT, vd.NgayDenPTVT, vd.MaHangVT, vd.TenHangVT, vd.TenPTVT, vd.QuocTichPTVT, vd.NuocXuat_ID, vd.MaNguoiNhanHang,
                                vd.TenNguoiNhanHang, vd.MaNguoiGiaoHang, vd.TenNguoiNhanHang, vd.CuaKhauNhap_ID, vd.CuaKhauXuat, vd.MaNguoiNhanHangTrungGian, vd.TenNguoiNhanHangTrungGian, vd.MaCangXepHang, vd.TenCangXepHang,
                                vd.MaCangDoHang, vd.TenCangDoHang, TKMD.ID, vd.DKGH_ID, vd.DiaDiemGiaoHang, vd.NoiDi, vd.SoHieuChuyenDi, vd.NgayKhoiHanh, vd.TongSoKien, vd.LoaiKien, vd.SoHieuKien, vd.DiaDiemChuyenTai, vd.LoaiVanDon, vd.ID_NuocPhatHanh, vd.ID_NuocPhatHanh);
                            List<Container> ContColl = (List<Container>)Company.KDT.SHARE.QuanLyChungTu.Container.SelectCollectionBy_VanDon_ID(vd.ID);
                            for (int v = 0; v < ContColl.Count; v++)
                            {
                                ContColl[v].VanDon_ID = vdResult;
                            }
                            Company.KDT.SHARE.QuanLyChungTu.Container.InsertCollection(ContColl);
                        }
                        //copy Hop dong
                        List<HopDongThuongMai> hdThuongmais = new List<HopDongThuongMai>();
                        hdThuongmais = (List<HopDongThuongMai>)HopDongThuongMai.SelectCollectionBy_TKMD_ID(PtkmdId);

                        foreach (HopDongThuongMai hd in hdThuongmais)
                        {
                            //Copy hop dong
                            hd.LoaiKB = 0;
                            hd.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            hd.NgayTiepNhan = new DateTime(1900, 1, 1);
                            hd.SoTiepNhan = 0;
                            hd.GuidStr = Guid.NewGuid().ToString();

                            long hdResult = HopDongThuongMai.InsertHopDongThuongMai(hd.SoHopDongTM, hd.NgayHopDongTM, hd.ThoiHanThanhToan, hd.NguyenTe_ID, hd.PTTT_ID, hd.DKGH_ID, hd.DiaDiemGiaoHang, hd.MaDonViMua, hd.TenDonViMua,
                                hd.MaDonViBan, hd.TenDonViBan, hd.TongTriGia, hd.ThongTinKhac, TKMD.ID, hd.GuidStr, hd.LoaiKB, hd.SoTiepNhan, hd.NgayTiepNhan, hd.TrangThai, hd.NamTiepNhan, hd.MaDoanhNghiep);

                            //---Hang hop dong   
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hop dong
                            List<HopDongThuongMaiDetail> HopDongDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HopDongTM_ID(hd.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HopDongThuongMaiDetail> HopDongDTMColl = new List<HopDongThuongMaiDetail>();

                            for (int l = 0; l < HopDongDetailUpdateCollection.Count; l++)
                            {
                                HopDongThuongMaiDetail hopDongCopy = HopDongDetailUpdateCollection[l];

                                if (hopDongCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.GC.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hopDongCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HopDongThuongMaiDetail hdtmDetail = new HopDongThuongMaiDetail();
                                    hdtmDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdtmDetail.HopDongTM_ID = hdResult;

                                    hdtmDetail.SoThuTuHang = hopDongCopy.SoThuTuHang;
                                    hdtmDetail.MaHS = hopDongCopy.MaHS;
                                    hdtmDetail.MaPhu = hopDongCopy.MaPhu;
                                    hdtmDetail.TenHang = hopDongCopy.TenHang;
                                    hdtmDetail.NuocXX_ID = hopDongCopy.NuocXX_ID;
                                    hdtmDetail.DVT_ID = hopDongCopy.DVT_ID;
                                    hdtmDetail.SoLuong = hopDongCopy.SoLuong;
                                    hdtmDetail.DonGiaKB = hopDongCopy.DonGiaKB;
                                    hdtmDetail.TriGiaKB = hopDongCopy.TriGiaKB;

                                    HopDongDTMColl.Add(hdtmDetail);
                                }
                            }

                            HopDongThuongMaiDetail.InsertCollection(HopDongDTMColl);
                        }

                        //copy Giay phep
                        List<GiayPhep> GPColl = new List<GiayPhep>();
                        GPColl = (List<GiayPhep>)GiayPhep.SelectCollectionBy_TKMD_ID(PtkmdId);

                        foreach (GiayPhep gp in GPColl)
                        {
                            gp.LoaiKB = 0;
                            gp.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            gp.NgayTiepNhan = new DateTime(1900, 1, 1);
                            gp.SoTiepNhan = 0;
                            gp.GuidStr = Guid.NewGuid().ToString();

                            long gpResult = GiayPhep.InsertGiayPhep(gp.SoGiayPhep, gp.NgayGiayPhep, gp.NgayHetHan, gp.NguoiCap, gp.NoiCap, gp.MaDonViDuocCap, gp.TenDonViDuocCap, gp.MaCoQuanCap, gp.TenQuanCap, gp.ThongTinKhac, gp.MaDoanhNghiep,
                                TKMD.ID, gp.GuidStr, gp.LoaiKB, gp.SoTiepNhan, gp.NgayTiepNhan, gp.TrangThai, gp.NamTiepNhan, gp.LoaiGiayPhep, gp.HinhThucTruLui);

                            //--Hang Giay phep
                            //Lay hang mau dich co cap nhat thong tin rieng cua Giay phep
                            List<HangGiayPhepDetail> GiayPhepDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HangGiayPhepDetail.SelectCollectionBy_GiayPhep_ID(gp.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HangGiayPhepDetail> HangGP = new List<HangGiayPhepDetail>();

                            for (int k = 0; k < GiayPhepDetailUpdateCollection.Count; k++)
                            {
                                HangGiayPhepDetail giayPhepCopy = GiayPhepDetailUpdateCollection[k];

                                if (giayPhepCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.GC.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, giayPhepCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HangGiayPhepDetail gpDetail = new HangGiayPhepDetail();
                                    gpDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    gpDetail.GiayPhep_ID = gpResult;

                                    gpDetail.SoThuTuHang = giayPhepCopy.SoThuTuHang;
                                    gpDetail.MaHS = giayPhepCopy.MaHS;
                                    gpDetail.MaPhu = giayPhepCopy.MaPhu;
                                    gpDetail.TenHang = giayPhepCopy.TenHang;
                                    gpDetail.NuocXX_ID = giayPhepCopy.NuocXX_ID;
                                    gpDetail.DVT_ID = giayPhepCopy.DVT_ID;
                                    gpDetail.SoLuong = giayPhepCopy.SoLuong;
                                    gpDetail.DonGiaKB = giayPhepCopy.DonGiaKB;
                                    gpDetail.TriGiaKB = giayPhepCopy.TriGiaKB;

                                    gpDetail.MaChuyenNganh = giayPhepCopy.MaChuyenNganh;
                                    gpDetail.MaNguyenTe = giayPhepCopy.MaNguyenTe;

                                    HangGP.Add(gpDetail);
                                }
                            }
                            HangGiayPhepDetail.InsertCollection(HangGP);
                        }

                        // copy Hoa don thuong mai
                        List<HoaDonThuongMai> HDTMColl = new List<HoaDonThuongMai>();
                        HDTMColl = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(PtkmdId);

                        foreach (HoaDonThuongMai hdtm in HDTMColl)
                        {
                            hdtm.LoaiKB = 0;
                            hdtm.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            hdtm.NgayTiepNhan = new DateTime(1900, 1, 1);
                            hdtm.SoTiepNhan = 0;
                            hdtm.GuidStr = Guid.NewGuid().ToString();

                            long hdonResult = HoaDonThuongMai.InsertHoaDonThuongMai(hdtm.SoHoaDon, hdtm.NgayHoaDon, hdtm.NguyenTe_ID, hdtm.PTTT_ID, hdtm.DKGH_ID, hdtm.MaDonViMua, hdtm.TenDonViMua, hdtm.MaDonViBan, hdtm.TenDonViBan,
                                hdtm.ThongTinKhac, TKMD.ID, hdtm.GuidStr, hdtm.LoaiKB, hdtm.SoTiepNhan, hdtm.NgayTiepNhan, hdtm.TrangThai, hdtm.NamTiepNhan, hdtm.MaDoanhNghiep);

                            //--Hang hoa don thuong mai
                            //Lay hang mau dich co cap nhat thong tin rieng cua Hoa don
                            List<HoaDonThuongMaiDetail> HoaDonDetailUpdateCollection = Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(hdtm.ID);

                            //Copy hang mau dich cho to khai moi
                            List<HoaDonThuongMaiDetail> HoaDonTMColl = new List<HoaDonThuongMaiDetail>();

                            for (int l = 0; l < HoaDonDetailUpdateCollection.Count; l++)
                            {
                                HoaDonThuongMaiDetail hoaDonCopy = HoaDonDetailUpdateCollection[l];

                                if (hoaDonCopy != null)
                                {
                                    //Lay ID HMD cua to khai moi
                                    Company.GC.BLL.KDT.HangMauDich hangMauDich = FindHangMauDichBy(TKMD.HMDCollection, TKMD.ID, hoaDonCopy.MaPhu);

                                    //TODO: hungtq 26/05/2010, UPDATE
                                    HoaDonThuongMaiDetail hdonDetail = new HoaDonThuongMaiDetail();
                                    hdonDetail.HMD_ID = hangMauDich != null ? hangMauDich.ID : 0;
                                    hdonDetail.HoaDonTM_ID = hdonResult;

                                    hdonDetail.SoThuTuHang = hoaDonCopy.SoThuTuHang;
                                    hdonDetail.MaHS = hoaDonCopy.MaHS;
                                    hdonDetail.MaPhu = hoaDonCopy.MaPhu;
                                    hdonDetail.TenHang = hoaDonCopy.TenHang;
                                    hdonDetail.NuocXX_ID = hoaDonCopy.NuocXX_ID;
                                    hdonDetail.DVT_ID = hoaDonCopy.DVT_ID;
                                    hdonDetail.SoLuong = hoaDonCopy.SoLuong;
                                    hdonDetail.DonGiaKB = hoaDonCopy.DonGiaKB;
                                    hdonDetail.TriGiaKB = hoaDonCopy.TriGiaKB;

                                    HoaDonTMColl.Add(hdonDetail);
                                }
                            }
                            HoaDonThuongMaiDetail.InsertCollection(HoaDonTMColl);
                        }                        // copy CO
                        List<CO> COColl = new List<CO>();
                        COColl = (List<CO>)Company.KDT.SHARE.QuanLyChungTu.CO.SelectCollectionBy_TKMD_ID(PtkmdId);
                        foreach (CO co in COColl)
                        {
                            co.LoaiKB = 0;
                            co.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            co.NgayTiepNhan = new DateTime(1900, 1, 1);
                            co.SoTiepNhan = 0;
                            co.GuidStr = Guid.NewGuid().ToString();

                            Company.KDT.SHARE.QuanLyChungTu.CO.InsertCO(co.SoCO, co.NgayCO, co.ToChucCap, co.NuocCapCO, co.MaNuocXKTrenCO, co.MaNuocNKTrenCO, co.TenDiaChiNguoiXK, co.TenDiaChiNguoiNK, co.LoaiCO,
                                co.ThongTinMoTaChiTiet, co.MaDoanhNghiep, co.NguoiKy, co.NoCo, co.ThoiHanNop, TKMD.ID, co.GuidStr, co.LoaiKB, co.SoTiepNhan, co.NgayTiepNhan, co.TrangThai, co.NamTiepNhan, co.NgayHetHan, co.NgayKhoiHanh, co.CangXepHang, co.CangDoHang, co.MaNguoiXK, co.MaNguoiNK, co.HamLuongXuatXu, co.MoTaHangHoa);
                        }
                        //Copy De nghi chuyen cua khau
                        List<DeNghiChuyenCuaKhau> chuyenCK = new List<DeNghiChuyenCuaKhau>();
                        chuyenCK = (List<DeNghiChuyenCuaKhau>)Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(PtkmdId);
                        foreach (DeNghiChuyenCuaKhau ck in chuyenCK)
                        {
                            ck.LoaiKB = 0;
                            ck.TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO;
                            ck.NgayTiepNhan = new DateTime(1900, 1, 1);
                            ck.SoTiepNhan = 0;
                            ck.GuidStr = Guid.NewGuid().ToString();


                            Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.InsertDeNghiChuyenCuaKhau(ck.ThongTinKhac, ck.MaDoanhNghiep, TKMD.ID, ck.GuidStr, ck.LoaiKB, ck.SoTiepNhan, ck.NgayTiepNhan, ck.TrangThai,
                                ck.NamTiepNhan, ck.SoVanDon, ck.NgayVanDon, ck.ThoiGianDen, ck.DiaDiemKiemTra, ck.TuyenDuong, ck.PTVT_ID, ck.DiaDiemKiemTraCucHQThanhPho);
                        }

                        //Copy Chung tu kem
                        List<ChungTuKem> chungTuKem = new List<ChungTuKem>();
                        chungTuKem = ChungTuKem.SelectCollectionBy_TKMDID(PtkmdId);
                        long ctkID = 0;
                        foreach (ChungTuKem ctk in chungTuKem)
                        {
                            ctk.LoaiKB = "0";
                            ctk.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO.ToString();
                            ctk.NGAYTN = new DateTime(1900, 1, 1);
                            ctk.SOTN = 0;
                            ctk.GUIDSTR = Guid.NewGuid().ToString();

                            ctkID = ChungTuKem.InsertChungTuKem(ctk.SO_CT, ctk.NGAY_CT, ctk.MA_LOAI_CT, ctk.DIENGIAI, ctk.LoaiKB, ctk.TrangThaiXuLy, ctk.Tempt, ctk.MessageID, ctk.GUIDSTR, ctk.KDT_WAITING, ctk.KDT_LASTINFO, ctk.SOTN, ctk.NGAYTN, ctk.TotalSize, ctk.Phanluong, ctk.HuongDan, TKMD.ID);

                            ctk.LoadListCTChiTiet();
                            foreach (ChungTuKemChiTiet chiTiet in ctk.listCTChiTiet)
                            {
                                ChungTuKemChiTiet.InsertChungTuKemChiTiet(ctkID, chiTiet.FileName, chiTiet.FileSize, chiTiet.NoiDung);
                            }
                        }

                        if (TKMD.HMDCollection == null || TKMD.HMDCollection.Count == 0)
                            TKMD.LoadHMDCollection();
                        if (TKMD.ChungTuTKCollection == null || TKMD.ChungTuTKCollection.Count == 0)
                            TKMD.LoadChungTuTKCollection();
                        TKMD.LoadChungTuHaiQuan();
                        if (TKMD.ListCO == null || TKMD.ListCO.Count == 0)
                            TKMD.LoadCO();




                    }
                    //Het Lypt Update

                    TKMD.LoadChungTuTKCollection();
                    TKMD.LoadHMDCollection();
                    dgList.DataSource = TKMD.HMDCollection;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch
                    {
                        dgList.Refresh();
                    }
                    gridEX1.DataSource = TKMD.ChungTuTKCollection;
                    gridEX1.Refetch();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai;
                            log.ID_DK = TKMD.ID;
                            log.GUIDSTR_DK = "";
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                        try
                        {
                            string where = "1 = 1";
                            where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                            List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                            if (listLog.Count > 0)
                            {
                                long idLog = listLog[0].IDLog;
                                string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                long idDK = listLog[0].ID_DK;
                                string guidstr = listLog[0].GUIDSTR_DK;
                                string userKhaiBao = listLog[0].UserNameKhaiBao;
                                DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                string userSuaDoi = GlobalSettings.UserLog;
                                DateTime ngaySuaDoi = DateTime.Now;
                                string ghiChu = listLog[0].GhiChu;
                                bool isDelete = listLog[0].IsDelete;
                                Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                            userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                            }
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                            return;
                        }
                    }
                    #endregion

                    MLMessages("Lưu tờ khai thành công.", "MSG_SAV02", "", false);
                    SetCommandStatus();

                    //TODO: updated Hungtq 6/2/2013. Save()
                    SaveVersion();

                }
                catch (Exception ex)
                {
                    SingleMessage.SendMail(setText("Dữ liệu không hợp lệ.", "Invalid value. Error"), TKMD.MaHaiQuan, new SendEventArgs(ex));
                }
                #endregion
            }
        }

        private bool SoSanhNoiDungSuaDoiToKhai(ToKhaiMauDich TKMD_Goc, ToKhaiMauDich TKMD)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string loaiHinh = TKMD.MaLoaiHinh.Substring(0, 1);

                if (loaiHinh.Equals("N"))
                {
                    #region To khai NHAP

                    ////Người nhập khẩu & Người xuất khẩu
                    //if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    //{
                    //    nguoiNhapKhau = TKMD.TenDoanhNghiep;
                    //    nguoiXuatKhau = TKMD.TenDonViDoiTac;
                    //}
                    //else
                    //{
                    //    nguoiNhapKhau = TKMD.TenDonViDoiTac;
                    //    nguoiXuatKhau = TKMD.TenDoanhNghiep;
                    //}
                    //1. Người xuất khẩu
                    //Tên Đơn vị đối tác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiXuatKhau", "1. Người xuất khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac);

                    //3. Đơn vị ủy thác
                    //Tên đơn vị Ủy thác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViUT, TKMD.TenDonViUT))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiUyThac", "3. Người ủy thác:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViUT, TKMD.TenDonViUT);

                    //6. Hóa đơn Thương mại
                    //Số Hóa đơn Thương mại
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HoaDonThuongMai", "6. Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai);
                    //Ngày Hóa đơn Thương mại
                    if (TKMD_Goc.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHoaDonThuongMai", "6. Ngày Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //7. Giấy phép
                    //Số Giấy phép
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GiayPhep", "7. Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep);
                    //Ngày Giấy phép
                    if (TKMD_Goc.NgayGiayPhep != TKMD.NgayGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayGiayPhep", "7. Ngày Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày Hết hạn Giấy phép
                    if (TKMD_Goc.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanGiayPhep", "7. Ngày hết hạn Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //8. Hợp đồng
                    //Số Hợp đồng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHopDong, TKMD.SoHopDong))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HopDong", "8. Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.SoHopDong, TKMD.SoHopDong);
                    //Ngày Hợp đồng
                    if (TKMD_Goc.NgayHopDong != TKMD.NgayHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHopDong", "8. Ngày Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày hết hạn Hợp đồng
                    if (TKMD_Goc.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanHopDong", "8. Ngày hết hạn Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //9. Vận tải đơn
                    //Số vận tải đơn
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoVanDon, TKMD.SoVanDon))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "VanDon", "9. Vận tải đơn:", TKMD.MaLoaiHinh, TKMD_Goc.SoVanDon, TKMD.SoVanDon);
                    //Ngày vận tải đơn
                    if (TKMD_Goc.NgayVanDon != TKMD.NgayVanDon)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayVanDon", "9. Ngày vận tải đơn:", TKMD.MaLoaiHinh, TKMD_Goc.NgayVanDon.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayVanDon.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    ////Địa điểm dở hàng & Địa điểm xếp hàng
                    //if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    //{
                    //    diaDiemDoHang = TKMD.CuaKhau_ID;
                    //    diaDiemXepHang = TKMD.DiaDiemXepHang;
                    //}
                    //else
                    //{
                    //    diaDiemXepHang = TKMD.CuaKhau_ID;
                    //    diaDiemDoHang = TKMD.DiaDiemXepHang;
                    //}
                    //10. Cảng xếp hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DiaDiemXepHang, TKMD.DiaDiemXepHang))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CangXepHang", "10. Cảng xếp hàng:", TKMD.MaLoaiHinh, TKMD_Goc.DiaDiemXepHang, TKMD.DiaDiemXepHang);

                    //11. Cảng dở hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.CuaKhau_ID, TKMD.CuaKhau_ID.Trim()))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CangDohang", "11. Cảng dở hàng:", TKMD.MaLoaiHinh, TKMD_Goc.CuaKhau_ID, TKMD.CuaKhau_ID);

                    //Phương tiện vận tải
                    //Loại Phương tiện vận tải
                    if (SoSanhStringNoiDungSua(TKMD_Goc.PTVT_ID, TKMD.PTVT_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PTVT", "12. Phương tiện vận tải:", TKMD.MaLoaiHinh, TKMD_Goc.PTVT_ID, TKMD.PTVT_ID);
                    //Số hiệu Phương tiện vận tải
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHieuPTVT, TKMD.SoHieuPTVT))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoHieuPTVT", "12. Số hiệu Phương tiện vận tải:", TKMD.MaLoaiHinh, TKMD_Goc.SoHieuPTVT, TKMD.SoHieuPTVT);
                    //Ngày đến Phương tiện vận tải
                    if (TKMD_Goc.NgayDenPTVT != TKMD.NgayDenPTVT)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayDenPTVT", "12. Ngày đến Phương tiện vận tải:", TKMD.MaLoaiHinh, TKMD_Goc.NgayDenPTVT.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayDenPTVT.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    ////Nước xuất khẩu & Nước nhập khẩu
                    //if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                    //    nuocXuatKhau = TKMD.NuocXK_ID;
                    //else
                    //    nuocNhapKhau = TKMD.NuocNK_ID;
                    //13. Nước xuất khẩu
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NuocXK_ID, TKMD.NuocXK_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NuocXK", "13. Nước xuất khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.NuocXK_ID, TKMD.NuocXK_ID);

                    //14. Điều kiện giao hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DKGH_ID, TKMD.DKGH_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DKGH", "14. Điều kiện giao hàng:", TKMD.MaLoaiHinh, TKMD_Goc.DKGH_ID, TKMD.DKGH_ID);

                    //15. Phương thức thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.PTTT_ID, TKMD.PTTT_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PTTT", "15. Phương thức thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.PTTT_ID, TKMD.PTTT_ID);

                    //16. Đồng tiền thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DongTienThanhToan", "16. Đồng tiền thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //17. Tỷ giá tính thuế
                    if (TKMD_Goc.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TyGiaTinhThue", "17. Tỷ giá tính thuế:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //31.b Số kiện hàng
                    if (TKMD_Goc.SoKien != TKMD.SoKien)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoKien", "31.b Tổng số kiện:", TKMD.MaLoaiHinh, TKMD_Goc.SoKien.ToString(), TKMD.SoKien.ToString());

                    //31.c Trọng lượng
                    if (TKMD_Goc.TrongLuong != TKMD.TrongLuong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuong", "31.c Tổng trọng lượng:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuong.ToString(), TKMD.TrongLuong.ToString());

                    //31.c Trọng lượng tịnh
                    if (TKMD_Goc.TrongLuongNet != TKMD.TrongLuongNet)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuongTinh", "31.c Trọng lượng tịnh:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuongNet.ToString(), TKMD.TrongLuongNet.ToString());

                    //35. Ghi chep khac
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GhiChepKhac", "35. Ghi chép khác:", TKMD.MaLoaiHinh, TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac);

                    #endregion
                }
                else
                {
                    #region To khai XUAT

                    //2. Người nhập khẩu
                    //Tên Đơn vị đối tác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiNhapKhau", "2. Người nhập khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViDoiTac, TKMD.TenDonViDoiTac);

                    //3. Đơn vị ủy thác
                    //Tên đơn vị Ủy thác
                    if (SoSanhStringNoiDungSua(TKMD_Goc.TenDonViUT, TKMD.TenDonViUT))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NguoiUyThac", "3. Người ủy thác:", TKMD.MaLoaiHinh, TKMD_Goc.TenDonViUT, TKMD.TenDonViUT);

                    //6. Giấy phép
                    //Số Giấy phép
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GiayPhep", "6. Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.SoGiayPhep, TKMD.SoGiayPhep);
                    //Ngày Giấy phép
                    if (TKMD_Goc.NgayGiayPhep != TKMD.NgayGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayGiayPhep", "6. Ngày Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày Hết hạn Giấy phép
                    if (TKMD_Goc.NgayHetHanGiayPhep != TKMD.NgayHetHanGiayPhep)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanGiayPhep", "6. Ngày hết hạn Giấy phép:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanGiayPhep.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //7. Hợp đồng
                    //Số Hợp đồng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHopDong, TKMD.SoHopDong))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HopDong", "7. Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.SoHopDong, TKMD.SoHopDong);
                    //Ngày Hợp đồng
                    if (TKMD_Goc.NgayHopDong != TKMD.NgayHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHopDong", "7. Ngày Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));
                    //Ngày hết hạn Hợp đồng
                    if (TKMD_Goc.NgayHetHanHopDong != TKMD.NgayHetHanHopDong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHetHanHopDong", "7. Ngày hết hạn Hợp đồng:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHetHanHopDong.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //8. Hóa đơn Thương mại
                    //Số Hóa đơn Thương mại
                    if (SoSanhStringNoiDungSua(TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "HoaDonThuongMai", "8. Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.SoHoaDonThuongMai, TKMD.SoHoaDonThuongMai);
                    //Ngày Hóa đơn Thương mại
                    if (TKMD_Goc.NgayHoaDonThuongMai != TKMD.NgayHoaDonThuongMai)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NgayHoaDonThuongMai", "8. Ngày Hóa đơn thương mại:", TKMD.MaLoaiHinh, TKMD_Goc.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN), TKMD.NgayHoaDonThuongMai.ToString(GlobalSettings.DATETIME_FORMAT_VN));

                    //9. Cảng xếp hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DiaDiemXepHang, TKMD.DiaDiemXepHang))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CangXepHang", "9. Cảng xếp hàng:", TKMD.MaLoaiHinh, TKMD_Goc.CuaKhau_ID, TKMD.CuaKhau_ID);

                    //10. Nước xuất khẩu
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NuocXK_ID, TKMD.NuocXK_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NuocXK", "10. Nước nhập khẩu:", TKMD.MaLoaiHinh, TKMD_Goc.NuocNK_ID, TKMD.NuocNK_ID);

                    //11. Điều kiện giao hàng
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DKGH_ID, TKMD.DKGH_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DKGH", "11. Điều kiện giao hàng:", TKMD.MaLoaiHinh, TKMD_Goc.DKGH_ID, TKMD.DKGH_ID);

                    //12. Phương thức thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.PTTT_ID, TKMD.PTTT_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PTTT", "12. Phương thức thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.PTTT_ID, TKMD.PTTT_ID);

                    //13. Đồng tiền thanh toán
                    if (SoSanhStringNoiDungSua(TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DongTienThanhToan", "13. Đồng tiền thanh toán:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //14. Tỷ giá tính thuế
                    if (TKMD_Goc.TyGiaTinhThue != TKMD.TyGiaTinhThue)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TyGiaTinhThue", "14. Tỷ giá tính thuế:", TKMD.MaLoaiHinh, TKMD_Goc.NguyenTe_ID, TKMD.NguyenTe_ID);

                    //25.b Số kiện hàng
                    if (TKMD_Goc.SoKien != TKMD.SoKien)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoKien", "25.b Tổng số kiện:", TKMD.MaLoaiHinh, TKMD_Goc.SoKien.ToString(), TKMD.SoKien.ToString());

                    //25.c Trọng lượng
                    if (TKMD_Goc.TrongLuong != TKMD.TrongLuong)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuong", "25.c Tổng trọng lượng:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuong.ToString(), TKMD.TrongLuong.ToString());

                    //25.c Trọng lượng tịnh
                    if (TKMD_Goc.TrongLuongNet != TKMD.TrongLuongNet)
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuongTinh", "25.c Trọng lượng tịnh:", TKMD.MaLoaiHinh, TKMD_Goc.TrongLuongNet.ToString(), TKMD.TrongLuongNet.ToString());

                    //29. Ghi chep khac
                    if (SoSanhStringNoiDungSua(TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac))
                        CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "GhiChepKhac", "29. Ghi chép khác:", TKMD.MaLoaiHinh, TKMD_Goc.DeXuatKhac, TKMD.DeXuatKhac);

                    #endregion
                }

                //Lệ phí hải quan
                if (TKMD_Goc.LePhiHaiQuan != TKMD.LePhiHaiQuan)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtLePhiHQ.Tag.ToString(), "Phí hải quan:", TKMD.MaLoaiHinh, TKMD_Goc.LePhiHaiQuan.ToString(), TKMD.LePhiHaiQuan.ToString());

                //Phí bảo hiểm
                if (TKMD_Goc.PhiBaoHiem != TKMD.PhiBaoHiem)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtPhiBaoHiem.Tag.ToString(), "Phí bảo hiểm:", TKMD.MaLoaiHinh, TKMD_Goc.PhiBaoHiem.ToString(), TKMD.PhiBaoHiem.ToString());

                //Phí vận chuyển
                if (TKMD_Goc.PhiVanChuyen != TKMD.PhiVanChuyen)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtPhiVanChuyen.Tag.ToString(), "Phí vận chuyển:", TKMD.MaLoaiHinh, TKMD_Goc.PhiVanChuyen.ToString(), TKMD.PhiVanChuyen.ToString());

                //Phí khác
                if (TKMD_Goc.PhiKhac != TKMD.PhiKhac)
                    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, txtPhiNganHang.Tag.ToString(), "Phí khác:", TKMD.MaLoaiHinh, TKMD_Goc.PhiKhac.ToString(), TKMD.PhiKhac.ToString());

                #region Thong tin CONTAINER
                //Kiem tra thong tin bi Xoa/ Them moi/ Sua doi
                if (TKMD.VanTaiDon != null && TKMD_Goc.VanTaiDon != null)
                {
                    //1. Container: Tim thong tin cu so voi thong tin moi -> Kiem tra thong tin bi XOA/ SUA DOI
                    Company.KDT.SHARE.QuanLyChungTu.Container filter = null;
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in TKMD_Goc.VanTaiDon.ContainerCollection) //To khai cu
                    {
                        filter = TKMD.VanTaiDon.ContainerCollection.Find(delegate(Company.KDT.SHARE.QuanLyChungTu.Container o) { return o.SoHieu == item.SoHieu; });

                        //Khong co thong tin -> Da xoa
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoHieuContainer", (loaiHinh == "N" ? "31" : "25") + ".a Số hiệu container:", TKMD.MaLoaiHinh, item.SoHieu, string.Format("Xóa thông tin container '{0}'", item.SoHieu));
                        }
                        //Co thong tin -> Sua doi
                        else
                        {
                            if (item.SoKien != filter.SoKien)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoKienContainer", (loaiHinh == "N" ? "31" : "25") + ".b Số lượng kiện trong container:", TKMD.MaLoaiHinh, item.SoKien.ToString(), filter.SoKien.ToString());

                            if (item.TrongLuong != filter.TrongLuong)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TrongLuongContainer", (loaiHinh == "N" ? "31" : "25") + ".c Trọng lượng hàng trong container:", TKMD.MaLoaiHinh, item.TrongLuong.ToString(), filter.TrongLuong.ToString());

                            if (SoSanhStringNoiDungSua(item.DiaDiemDongHang, filter.DiaDiemDongHang))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DiaDiemDongHangContainer", (loaiHinh == "N" ? "31" : "25") + ".d Địa điểm đóng hàng:", TKMD.MaLoaiHinh, item.DiaDiemDongHang, filter.DiaDiemDongHang);
                        }
                    }

                    //2. Container: Tim thong tin moi so voi thong tin cu -> Kiem tra thong tin THEM MOI
                    filter = null;
                    foreach (Company.KDT.SHARE.QuanLyChungTu.Container item in TKMD.VanTaiDon.ContainerCollection) //To khai moi
                    {
                        filter = TKMD_Goc.VanTaiDon.ContainerCollection.Find(delegate(Company.KDT.SHARE.QuanLyChungTu.Container o) { return o.SoHieu == item.SoHieu; });

                        //Khong co thong tin -> Them moi
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoHieuContainer", (loaiHinh == "N" ? "31" : "25") + ".a Số hiệu container:", TKMD.MaLoaiHinh, item.SoHieu, string.Format("Thêm mới thông tin container '{0}'", item.SoHieu));
                        }
                    }
                }
                #endregion

                #region Thong tin HANG
                //Kiem tra thong tin bi Xoa/ Them moi/ Sua doi
                if (TKMD.HMDCollection != null && TKMD_Goc.HMDCollection != null)
                {
                    //1. Hang: Tim thong tin cu so voi thong tin moi -> Kiem tra thong tin bi XOA/ SUA DOI
                    Company.GC.BLL.KDT.HangMauDich filter = null;
                    foreach (Company.GC.BLL.KDT.HangMauDich item in TKMD_Goc.HMDCollection) //To khai cu
                    {
                        //Doi voi Kinh doanh: kiem tra ten hang, vi khong co ma.
                        //Doi voi SXXK, GC: kiem tra ma hang.
                        if (item.MaPhu != null && item.MaPhu != "")
                        {
                            filter = TKMD.HMDCollection.Find(delegate(Company.GC.BLL.KDT.HangMauDich o) { return o.MaPhu == item.MaPhu; });
                        }
                        else
                        {
                            filter = TKMD.HMDCollection.Find(delegate(Company.GC.BLL.KDT.HangMauDich o) { return o.TenHang == item.TenHang; });
                        }

                        //Khong co thong tin -> Da xoa/Sua doi
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TenHMD", (loaiHinh == "N" ? "18" : "15") + ". Mô tả hàng hóa:", TKMD.MaLoaiHinh, item.TenHang, string.Format("Xóa thông tin hàng '{0}/ {1}' tại dòng số {2}", item.TenHang, item.MaPhu, item.SoThuTuHang));
                        }
                        //Co thong tin -> Sua doi
                        else
                        {
                            if (item.SoThuTuHang != filter.SoThuTuHang)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoThuTuHMD", string.Format("Số thứ tự hàng của dòng hàng số {0}:", item.SoThuTuHang), TKMD.MaLoaiHinh, item.SoThuTuHang.ToString(), string.Format("Thay đổi số thứ tự dòng hàng số {0} -> dòng hàng số {1}", item.SoThuTuHang, filter.SoThuTuHang));

                            if (SoSanhStringNoiDungSua(item.TenHang, filter.TenHang))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TenHMD", string.Format("{0} Mô tả hàng hóa (Tên hàng) của dòng hàng số {1}:", (loaiHinh == "N" ? "18." : "15."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.TenHang, filter.TenHang);

                            if (SoSanhStringNoiDungSua(item.MaPhu, filter.MaPhu))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "MaHMD", string.Format("{0} Mô tả hàng hóa (Mã hàng) của dòng hàng số {1}:", (loaiHinh == "N" ? "18." : "15."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.MaPhu, filter.MaPhu);

                            if (SoSanhStringNoiDungSua(item.MaHS, filter.MaHS))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "MaHSHMD", string.Format("{0} Mã số hàng hóa của dòng hàng số {1}:", (loaiHinh == "N" ? "19." : "16."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.MaHS, filter.MaHS);

                            if (SoSanhStringNoiDungSua(item.NuocXX_ID, filter.NuocXX_ID))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "NuocXuatXuHMD", string.Format("{0} Xuất xứ của dòng hàng số {1}:", (loaiHinh == "N" ? "20." : "17."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.NuocXX_ID.ToString(), filter.NuocXX_ID.ToString());

                            //if (SoSanhStringNoiDungSua(item.CheDoUuDai, filter.CheDoUuDai) & loaiHinh == "N")
                            //    CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "CheDoUuDaiHMD", string.Format("{0} Chế độ ưu đãi của dòng hàng số {1}:", (loaiHinh == "N" ? "21." : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.CheDoUuDai, filter.CheDoUuDai);

                            if (item.SoLuong != filter.SoLuong)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "SoLuongHMD", string.Format("{0} Lượng hàng của dòng hàng số {1}:", (loaiHinh == "N" ? "22." : "18."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.SoLuong.ToString(), filter.SoLuong.ToString());

                            if (SoSanhStringNoiDungSua(item.DVT_ID, filter.DVT_ID))
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DonViTinhHMD", string.Format("{0} Đơn vị tính của dòng hàng số {1}:", (loaiHinh == "N" ? "23." : "19."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.DVT_ID.ToString(), filter.DVT_ID.ToString());

                            if (item.DonGiaKB != filter.DonGiaKB)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "DonGiaHMD", string.Format("{0} Đơn giá của dòng hàng số {1}:", (loaiHinh == "N" ? "24." : "20."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.DonGiaKB.ToString(), filter.DonGiaKB.ToString());

                            if (item.TriGiaKB != filter.TriGiaKB)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TriGiaNguyenTeHMD", string.Format("{0} Trị giá nguyên tệ của dòng hàng số {1}:", (loaiHinh == "N" ? "25." : "21."), item.SoThuTuHang), TKMD.MaLoaiHinh, item.TriGiaKB.ToString(), filter.TriGiaKB.ToString());

                            //Thue XNK 
                            if (item.ThueSuatXNK != filter.ThueSuatXNK)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatXNKHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "26. Thuế nhập khẩu:" : "22.b Thuế xuât khẩu:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatXNK.ToString(), filter.ThueSuatXNK.ToString());

                            if (item.ThueXNK != filter.ThueXNK)
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueXNKHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "26. Thuế nhập khẩu:" : "22.c Thuế xuât khẩu:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueXNK.ToString(), filter.ThueXNK.ToString());

                            //Thue TTDB
                            if (item.ThueSuatTTDB != filter.ThueSuatTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatTTDBHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế TTĐB:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatTTDB.ToString(), filter.ThueSuatTTDB.ToString());

                            if (item.ThueTTDB != filter.ThueTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueTTDBHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế TTĐB:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueTTDB.ToString(), filter.ThueTTDB.ToString());

                            //Thue Tu ve chong ban pha gia
                            if (item.ThueSuatTTDB != filter.ThueSuatTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatTVCBPGHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế Tự vệ chống bán phá giá:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatChongPhaGia.ToString(), filter.ThueSuatChongPhaGia.ToString());

                            if (item.ThueTTDB != filter.ThueTTDB && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueTVCBPGHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế Tự vệ chống bán phá giá:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueChongPhaGia.ToString(), filter.ThueChongPhaGia.ToString());

                            //Thue BVMT
                            if (item.ThueSuatBVMT != filter.ThueSuatBVMT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatBVMTHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "28. Thuế BVMT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatBVMT.ToString(), filter.ThueSuatBVMT.ToString());

                            if (item.ThueBVMT != filter.ThueBVMT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueBVMTHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "27. Thuế BVMT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueBVMT.ToString(), filter.ThueBVMT.ToString());

                            //Thue GTGT
                            if (item.ThueSuatGTGT != filter.ThueSuatGTGT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "ThueSuatGTGTHMD", string.Format("{0} Thuế suất(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "29. Thuế GTGT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueSuatGTGT.ToString(), filter.ThueSuatGTGT.ToString());

                            if (item.ThueGTGT != filter.ThueGTGT && loaiHinh == "N") //Chi co to khai nhap
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TienThueGTGTHMD", string.Format("{0} Tiền thuế của dòng hàng số {1}:", (loaiHinh == "N" ? "28. Thuế GTGT:" : ""), item.SoThuTuHang), TKMD.MaLoaiHinh, item.ThueGTGT.ToString(), filter.ThueGTGT.ToString());

                            //Thue thu khac
                            if (item.TyLeThuKhac != filter.TyLeThuKhac && loaiHinh == "X") //Chi co to khai xuat
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TyLeThuKhacHMD", string.Format("{0} Tỷ lệ(%) của dòng hàng số {1}:", (loaiHinh == "N" ? "" : "23.b Thu khác:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.TyLeThuKhac.ToString(), filter.TyLeThuKhac.ToString());

                            if (item.PhuThu != filter.PhuThu && loaiHinh == "X") //Chi co to khai xuat
                                CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "PhuThuHMD", string.Format("{0} Số tiền của dòng hàng số {1}:", (loaiHinh == "N" ? "" : "23.c Thu khác:"), item.SoThuTuHang), TKMD.MaLoaiHinh, item.PhuThu.ToString(), filter.PhuThu.ToString());
                        }
                    }

                    //2. Hang: Tim thong tin moi so voi thong tin cu -> Kiem tra thong tin THEM MOI
                    filter = null;
                    foreach (Company.GC.BLL.KDT.HangMauDich item in TKMD.HMDCollection) //To khai moi
                    {
                        //Doi voi Kinh doanh: kiem tra ten hang, vi khong co ma.
                        //Doi voi SXXK, GC: kiem tra ma hang.
                        if (item.MaPhu != null && item.MaPhu != "")
                        {
                            filter = TKMD_Goc.HMDCollection.Find(delegate(Company.GC.BLL.KDT.HangMauDich o) { return o.MaPhu == item.MaPhu; });
                        }
                        else
                        {
                            filter = TKMD_Goc.HMDCollection.Find(delegate(Company.GC.BLL.KDT.HangMauDich o) { return o.TenHang == item.TenHang; });
                        }

                        //Khong co thong tin -> Them moi
                        if (filter == null)
                        {
                            CapNhatNoiDungSuaToKhai(TKMD_Goc, TKMD, "TenHMD", (loaiHinh == "N" ? "18" : "15") + ". Mô tả hàng hóa:", TKMD.MaLoaiHinh, item.TenHang, string.Format("Thêm mới thông tin hàng '{0}/ {1}' tại dòng số {2}", item.TenHang, item.MaPhu, item.SoThuTuHang));
                        }
                    }
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return false;
        }

        private void CapNhatNoiDungSuaToKhai(ToKhaiMauDich TKMD_Goc, ToKhaiMauDich TKMD, string maThongTin, string tenThongTin, string maLoaiHinh, string noiDungChinh, string noiDungSua)
        {
            NoiDungToKhai noiDungSuaTK = new NoiDungToKhai();
            NoiDungDieuChinhTKDetail noiDungSuaChiTiet = new NoiDungDieuChinhTKDetail();

            List<NoiDungToKhai> listNoiDungSuaTK = NoiDungToKhai.SelectCollectionDynamic("Ma = '" + maThongTin + "' " + " AND MaLoaiHinh = '" + maLoaiHinh + "'", null);

            if (listNoiDungSuaTK.Count == 0)
            {
                noiDungSuaTK.Ma = maThongTin;
                noiDungSuaTK.Ten = tenThongTin;
                noiDungSuaTK.MaLoaiHinh = maLoaiHinh;
                noiDungSuaTK.InsertUpdate();

                List<NoiDungDieuChinhTKDetail> noiDungDieuChinhTKDetailCollections = NoiDungDieuChinhTKDetail.SelectCollectionDynamic(string.Format("NoiDungTKChinh = N'{0}'", noiDungSuaTK.Ten + " " + noiDungChinh), "");

                if ((noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count > 0 && noiDungDieuChinhTKDetailCollections[0].NguoiTao == ENoiDungDieuChinhTK.Auto.ToString())
                    || (noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count == 0))
                {
                    noiDungSuaChiTiet.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                    noiDungSuaChiTiet.NoiDungTKChinh = noiDungSuaTK.Ten + " " + noiDungChinh;
                    noiDungSuaChiTiet.NoiDungTKSua = noiDungSuaTK.Ten + " " + noiDungSua;
                    noiDungSuaChiTiet.NguoiTao = ENoiDungDieuChinhTK.Auto.ToString();
                    noiDungSuaChiTiet.InsertUpdateByThongTin(null);
                }
            }
            else
            {
                foreach (NoiDungToKhai ndtk in listNoiDungSuaTK)
                {
                    if (ndtk.Ten != tenThongTin)
                    {
                        ndtk.Ten = tenThongTin;
                        ndtk.Update();
                    }

                    List<NoiDungDieuChinhTKDetail> noiDungDieuChinhTKDetailCollections = NoiDungDieuChinhTKDetail.SelectCollectionDynamic(string.Format("NoiDungTKChinh = N'{0}'", ndtk.Ten + " " + noiDungChinh), "");

                    if ((noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count > 0 && noiDungDieuChinhTKDetailCollections[0].NguoiTao == ENoiDungDieuChinhTK.Auto.ToString())
                        || (noiDungDieuChinhTKDetailCollections != null && noiDungDieuChinhTKDetailCollections.Count == 0))
                    {
                        noiDungSuaChiTiet.Id_DieuChinh = Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK.SelectMaxId_DieuChinh();
                        noiDungSuaChiTiet.NoiDungTKChinh = ndtk.Ten + " " + noiDungChinh;
                        noiDungSuaChiTiet.NoiDungTKSua = ndtk.Ten + " " + noiDungSua;
                        noiDungSuaChiTiet.NguoiTao = ENoiDungDieuChinhTK.Auto.ToString();
                        noiDungSuaChiTiet.InsertUpdateByThongTin(null);
                    }
                }
            }
        }
        private bool SoSanhStringNoiDungSua(string goc, string sua)
        {
            return (string.IsNullOrEmpty(goc) ? "" : goc).Trim() != (string.IsNullOrEmpty(sua) ? "" : sua).Trim();
        }

        //Hungtq updated 6/2/2013
        public void SaveVersion()
        {
            try
            {
                bool cks = Company.KDT.SHARE.Components.Globals.IsSignOnLan || Company.KDT.SHARE.Components.Globals.IsSignRemote || Company.KDT.SHARE.Components.Globals.IsSignature;

                if (Company.KDT.SHARE.Components.Globals.IsKTX)
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 1, cks, "", null);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "2.00")
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 2, cks, "", null);
                else if (Company.KDT.SHARE.Components.Globals.VersionSend == "3.00" && GlobalSettings.SendV4 == false)
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 3, cks, "", null);
                else if (GlobalSettings.SendV4) //Mac dinh V4
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 4, cks, "", null);
                else
                    Company.KDT.SHARE.Components.Common.ToKhaiMauDichBoSung.InsertUpdateByTKMD(TKMD.ID, 0, cks, "", null);

                //Reload
                tkmdVersionControlV2.LoadInfo(TKMD.ID);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion

        #region In Tờ Khai

        private void inToKhai()
        {
            if (TKMD.HMDCollection.Count == 0) return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = TKMD;
                    f.ShowDialog();
                    break;

                case "X":
                    var f1 = new ReportViewTKXForm();
                    f1.TKMD = TKMD;
                    f1.ShowDialog();
                    break;
            }

        }
        #endregion
        #region In tờ khai thông quan điện tử theo TT 15

        private void InToKhaiTQDT_TT15()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);
                return;
            }
            if (this.TKMD.TrangThaiXuLy != 1)
            {
                if (MLMessages("Tờ khai chưa được duyệt, bạn có muốn in hay không?", "MSG_PRI02", "", true) != "Yes") return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNTQDTFormTT196New f = new ReportViewTKNTQDTFormTT196New(false);
                    f.TKMD = this.TKMD;
                    f.ShowDialog();
                    break;
                case "X":
                    ReportViewTKXTQDTFormTT196New f1 = new ReportViewTKXTQDTFormTT196New(false);
                    f1.TKMD = this.TKMD;
                    f1.ShowDialog();
                    break;
            }
        }
        #endregion
        #region In to Khai sua doi bo sung
        private void InToKhaiDienTuSuaDoiBoSung()
        {
            InToKhaiSuaDoiBoSungForm frmInToKhaiSuaDoiBoSung = new InToKhaiSuaDoiBoSungForm();
            InToKhaiSuaDoiBoSungForm.TKMD = TKMD;
            frmInToKhaiSuaDoiBoSung.ShowDialog();
        }
        #endregion

        #region In bang ke Container
        private void InContainer()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.BangkeSoContainer f = new Company.Interface.Report.SXXK.BangkeSoContainer();
            f.tkmd = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        #endregion
        #region In to khai tai cho
        private void InToKhaiTaiChoTT196()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (this.TKMD.HMDCollection.Count == 0) return;
                if (this.TKMD.ID == 0)
                {
                    //ShowMessage("Bạn hãy lưu trước khi in.",false);
                    MLMessages("Bạn hãy lưu trước khi in.", "MSG_PRI01", "", false);

                    return;
                }
                switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
                {
                    //Nhap
                    case "N":
                        ReportViewTKNTQDTFormTT196New f = new ReportViewTKNTQDTFormTT196New(true);
                        f.TKMD = this.TKMD;
                        f.Show();
                        break;
                    //Xuat
                    case "X":
                        ReportViewTKXTQDTFormTT196New f1 = new ReportViewTKXTQDTFormTT196New(true);
                        f1.TKMD = this.TKMD;
                        f1.Show();
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        #endregion
        #region In an dinh thue
        private void InAnDinhThue(long tkmdID, string guidstr)
        {
            try
            {
                AnDinhThue anDinhThue = new AnDinhThue();
                List<AnDinhThue> dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                if (dsADT.Count > 0)
                {
                    anDinhThue = dsADT[0];
                    anDinhThue.LoadChiTiet();

                    Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                    anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                    anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                    anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                    anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                }
                else
                {
                    //Hungtq updated 21/02/2012.
                    //Không có thông tin Ấn định thuế hoặc Có thông tin ấn định thuế từ kết quả Hải quan trả về nhưng chưa lưu được xuống database.
                    Company.Interface.Globals.AnDinhThue_InsertFromXML(this.TKMD);

                    //InAnDinhThue
                    dsADT = (List<AnDinhThue>)Company.KDT.SHARE.Components.AnDinhThue.AnDinhThue.SelectCollectionBy_TKMD_ID(TKMD.ID);
                    if (dsADT.Count > 0)
                    {
                        anDinhThue = dsADT[0];
                        anDinhThue.LoadChiTiet();

                        Globals.ExportExcelThongBaoThue(TKMD, anDinhThue.SoQuyetDinh, anDinhThue.NgayQuyetDinh, anDinhThue.NgayHetHan, anDinhThue.TaiKhoanKhoBac, anDinhThue.TenKhoBac,
                        anDinhThue.ThueXNK.Chuong, anDinhThue.ThueXNK.Loai.ToString(), anDinhThue.ThueXNK.Khoan.ToString(), anDinhThue.ThueXNK.Muc.ToString(), anDinhThue.ThueXNK.TieuMuc.ToString(), (double)anDinhThue.ThueXNK.TienThue,
                        anDinhThue.ThueVAT.Chuong, anDinhThue.ThueVAT.Loai.ToString(), anDinhThue.ThueVAT.Khoan.ToString(), anDinhThue.ThueVAT.Muc.ToString(), anDinhThue.ThueVAT.TieuMuc.ToString(), (double)anDinhThue.ThueVAT.TienThue,
                        anDinhThue.ThueTTDB.Chuong, anDinhThue.ThueTTDB.Loai.ToString(), anDinhThue.ThueTTDB.Khoan.ToString(), anDinhThue.ThueTTDB.Muc.ToString(), anDinhThue.ThueTTDB.TieuMuc.ToString(), (double)anDinhThue.ThueTTDB.TienThue,
                        anDinhThue.ThueTVCBPG.Chuong, anDinhThue.ThueTVCBPG.Loai.ToString(), anDinhThue.ThueTVCBPG.Khoan.ToString(), anDinhThue.ThueTVCBPG.Muc.ToString(), anDinhThue.ThueTVCBPG.TieuMuc.ToString(), (double)anDinhThue.ThueTVCBPG.TienThue, 0);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        #endregion
        #region In Bang ke hang hoa theo HD
        private void BangKe_HD()
        {
            //this.TKMD = GetTKMDSelected();
            if (TKMD.ID == 0)
                return;

            if (TKMD != null && TKMD.HopDongThuongMaiCollection != null && TKMD.HopDongThuongMaiCollection.Count > 0)
            {
                Company.Interface.Report.SXXK.BangKe_HD_TheoTK f = new Company.Interface.Report.SXXK.BangKe_HD_TheoTK();
                f.tkmd = TKMD;
                f.BindReport_HopDong();
                f.ShowPreview();
            }
            else
                ShowMessage("Không tồn tại hợp đồng trong tờ khai này", false);
        }
        #endregion
        #region In Phieu kiem tra chung tu giay
        private void InPhieuKiemTra_CT()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.KetQuaKiemTraChungTuGiay_TT196 f = new Company.Interface.Report.SXXK.KetQuaKiemTraChungTuGiay_TT196();
            f.TKMD = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        #endregion
        #region In Phieu kiem tra hang hoa
        private void InPhieuKiemTra_HH()
        {
            if (TKMD.ID == 0)
                return;
            Company.Interface.Report.SXXK.KetQuaKiemTraHangHoa_TT196 f = new Company.Interface.Report.SXXK.KetQuaKiemTraHangHoa_TT196();
            f.TKMD = TKMD;
            f.BindReport();
            f.ShowPreview();
        }
        #endregion

        #region Set  trạng thái thanh Command
        private void SetCommandStatus()
        {

            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;

            txtLyDoSua.Enabled = false;
            cmdNPLCungUng.Visible = Janus.Windows.UI.InheritableBoolean.True;
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    lblTrangThai.Text = "Chưa khai báo";
                    lblPhanLuong.Text = "Chưa phân luồng";
                }
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = txtSoToKhai.Text = "";
                cmdNPLCungUng.Visible = NhomLoaiHinh.Substring(0, 1) != "N" ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = "Không phê duyệt";

                cmdNPLCungUng.Visible = TKMD.MaLoaiHinh.Substring(0, 1) != "N" ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdNPLCungUng.Visible = TKMD.MaLoaiHinh.Substring(0, 1) != "N" ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;

                //Hungtq, Update 20/07/2010
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                if (TKMD.PhanLuong != "")
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }

                lblTrangThai.Text = "Đã duyệt";

                txtSoToKhai.Text = TKMD.SoToKhai.ToString();
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chờ duyệt";
                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }

            if (TKMD.PhanLuong == "1")
            {
                lblPhanLuong.ForeColor = Color.Green;
                lblPhanLuong.Text = "Luồng Xanh";
            }
            else if (TKMD.PhanLuong == "2")
            {
                lblPhanLuong.Text = "Luồng Vàng";
                lblPhanLuong.ForeColor = Color.Yellow;

            }
            else if (TKMD.PhanLuong == "3")
            {
                lblPhanLuong.Text = "Luồng Đỏ";
                lblPhanLuong.ForeColor = Color.Red;

            }
            else
            {
                lblPhanLuong.Text = "Chưa phân luồng";
                lblPhanLuong.ForeColor = Color.Black;
            }
            cmdToKhaiTriGia.Enabled = cmdChungTuDinhKem.Enabled;

            SetCommandStatusSuaHuyToKhai();

            //Hien thi thong tin so luong chung tu dinh kem tren menu 'Chung tu dinh kem'
            SetStatusChungTuDinhKem(this.TKMD);
            #region set trùng tờ khai
            timer1.Stop();
            timer1.Interval = 500;
            if (TKMD.QuanLyMay)
            {

                pictureBox1.Visible = true;

                timer1.Start();
            }
            else
            {
                pictureBox1.Visible = false;
                timer1.Stop();
            }
            #endregion
            if (Company.KDT.SHARE.Components.Globals.IsKTX && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdSendTK.Visible = cmdSendTK1.Visible = Janus.Windows.UI.InheritableBoolean.True;
            }
            else
                cmdSendTK.Visible = cmdSendTK1.Visible = Janus.Windows.UI.InheritableBoolean.False;

        }

        private void SetCommandStatusSuaHuyToKhai()
        {
            txtLyDoSua.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.HUYTKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chưa khai báo";
                txtLyDoSua.Enabled = true;

            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET_DASUACHUA)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Chờ duyệt";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
            {
                //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdTinhLaiThue.Enabled = cmdChungTuDinhKem.Enabled = ThemHang2.Enabled = ThemHang1.Enabled = cmdSave.Enabled = cmdSend.Enabled
                // = Janus.Windows.UI.InheritableBoolean.False;

                //dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                //cmdHuyToKhaiDaDuyet.Enabled = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Đã hủy";

                txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
            }

            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //Nếu tờ khai chờ duyệt có số tờ khai thì không cho phép hủy khai báo
                if (TKMD.SoToKhai > 0)
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lblTrangThai.Text = "Không phê duyệt";

                if (TKMD.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
            }

            //Hungtq updated 24/02/2012.
            #region Cho phép xem thông tin Chứng từ kèm khi Tờ khai ở trạng thái: Chờ duyệt, Đã duyệt, Sửa tờ khai, Hủy tờ khai, Không phê duyệt, Chờ hủy tờ khai, Hủy tờ khai.

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                || TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                )
            {
                cmdChungTuDinhKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }

            #endregion
        }

        /// <summary>
        /// Hien thi thong tin so luong chung tu dinh kem tren menu 'Chung tu dinh kem'. 
        /// Updaetd by Hungtq, 24/09/2012.
        /// </summary>
        /// <param name="tkmd"></param>
        private void SetStatusChungTuDinhKem(Company.GC.BLL.KDT.ToKhaiMauDich tkmd)
        {
            try
            {
                this.cmdVanDon1.ImageIndex = (tkmd.VanTaiDon != null ? 10 : 0);

                //Kiem tra thong tin co phai la Bo sung khong?
                this.cmdHopDong1.ImageIndex = (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai.KiemTraChungTuCoBoSung(tkmd.HopDongThuongMaiCollection) == false) == true ? 10 : 0;

                this.cmdGiayPhep1.ImageIndex = (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayPhep.KiemTraChungTuCoBoSung(tkmd.GiayPhepCollection) == false) == true ? 10 : 0;

                this.cmdHoaDonThuongMai1.ImageIndex = (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai.KiemTraChungTuCoBoSung(tkmd.HoaDonThuongMaiCollection) == false) == true ? 10 : 0;

                this.cmdCO1.ImageIndex = (tkmd.COCollection != null && tkmd.COCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.CO.KiemTraChungTuCoBoSung(tkmd.COCollection) == false) == true ? 10 : 0;

                this.cmDeNghiChuyenCuaKhau1.ImageIndex = (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.KiemTraChungTuCoBoSung(tkmd.listChuyenCuaKhau) == false) == true ? 10 : 0;

                this.cmdChungTuDangAnh1.ImageIndex = (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.KiemTraChungTuCoBoSung(tkmd.ChungTuKemCollection) == false) == true ? 10 : 0;
                this.cmdChungTuDangAnh1.Visible = Janus.Windows.UI.InheritableBoolean.True;

                this.cmdChungTuNo1.ImageIndex = (tkmd.ChungTuNoCollection != null && tkmd.ChungTuNoCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungTuNo.KiemTraChungTuCoBoSung(tkmd.ChungTuNoCollection) == false) == true ? 10 : 0;

                this.ChungThuGiamDinh1.ImageIndex = (tkmd.ChungThuGD != null && tkmd.ChungThuGD.ID > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh.KiemTraChungTuCoBoSung(tkmd.ChungThuGD) == false) == true ? 10 : 0;

                this.GiayKiemTra1.ImageIndex = (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra.KiemTraChungTuCoBoSung(tkmd.GiayKiemTraCollection) == false) == true ? 10 : 0;

                //Chung tu dinh kem Bo sung
                this.cmdCOBoSung1.ImageIndex = (tkmd.COCollection != null && tkmd.COCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.CO.KiemTraChungTuCoBoSung(tkmd.COCollection) == true) == true ? 10 : 0;

                this.cmdGiayPhepBoSung1.ImageIndex = (tkmd.GiayPhepCollection != null && tkmd.GiayPhepCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayPhep.KiemTraChungTuCoBoSung(tkmd.GiayPhepCollection) == true) == true ? 10 : 0;

                this.cmdHopDongBoSung1.ImageIndex = (tkmd.HopDongThuongMaiCollection != null && tkmd.HopDongThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai.KiemTraChungTuCoBoSung(tkmd.HopDongThuongMaiCollection) == true) == true ? 10 : 0;

                this.cmdHoaDonThuongMaiBoSung1.ImageIndex = (tkmd.HoaDonThuongMaiCollection != null && tkmd.HoaDonThuongMaiCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMai.KiemTraChungTuCoBoSung(tkmd.HoaDonThuongMaiCollection) == true) == true ? 10 : 0;

                this.cmdChuyenCuaKhauBoSung1.ImageIndex = (tkmd.listChuyenCuaKhau != null && tkmd.listChuyenCuaKhau.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.DeNghiChuyenCuaKhau.KiemTraChungTuCoBoSung(tkmd.listChuyenCuaKhau) == true) == true ? 10 : 0;

                this.cmdChungTuDangAnhBS1.ImageIndex = (tkmd.ChungTuKemCollection != null && tkmd.ChungTuKemCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungTuKem.KiemTraChungTuCoBoSung(tkmd.ChungTuKemCollection) == true) == true ? 10 : 0;
                this.cmdChungTuDangAnhBS1.Visible = Janus.Windows.UI.InheritableBoolean.True;

                //this.cmdbosGiayNopTien.ImageIndex = (tkmd.GiayNopTiens != null && tkmd.GiayNopTiens.Count > 0
                //    && Company.KDT.SHARE.QuanLyChungTu.GiayNopTien.KiemTraChungTuCoBoSung(tkmd.GiayNopTiens) == true) == true ? 10 : 0;

                this.cmdBoSungGiayKiemTra.ImageIndex = (tkmd.GiayKiemTraCollection != null && tkmd.GiayKiemTraCollection.Count > 0
                    && Company.KDT.SHARE.QuanLyChungTu.GiayKiemTra.KiemTraChungTuCoBoSung(tkmd.GiayKiemTraCollection) == true) == true ? 10 : 0;

                this.cmdBoSungChungThuGiamDinh.ImageIndex = (tkmd.ChungThuGD != null && tkmd.ChungThuGD.ID > 0
                    && Company.KDT.SHARE.QuanLyChungTu.ChungThuGiamDinh.KiemTraChungTuCoBoSung(tkmd.ChungThuGD) == true) == true ? 10 : 0;

                //To khai tri gia PP1, PP2, PP3

                this.ToKhaiTriGia.ImageIndex = (tkmd.TKTGCollection != null && tkmd.TKTGCollection.Count > 0) == true ? 10 : 0;

                this.cmdToKhaiTGPP2.ImageIndex = (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0) == true ? 10 : 0;

                this.cmdToKhaiTGPP3.ImageIndex = (tkmd.TKTGPP23Collection != null && tkmd.TKTGPP23Collection.Count > 0) == true ? 10 : 0;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion

        #region sự kiện kích chuột trên thanh công cụ
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                //Hungtq, Update 06/11/2011
                case "cmdSave":
                    Save();
                    break;
                case "cmdTinhLaiThue":
                    TinhLaiThue();
                    //showMsg("MSG_WRN36", TKMD.TongTriGiaKhaiBao);
                    Globals.ShowMessageTQDT("Tổng trị giá khai báo là : " + TKMD.TongTriGiaKhaiBao, false);
                    break;
                case "cmdReadExcel":
                    ReadExcel();
                    break;
                case "cmdThemHang":
                    ThemHang();
                    break;
                case "ThemHang":
                    ThemHang();
                    break;
                case "ToKhaiViet":
                    inToKhai();
                    break;
                case "cmdSendTK":
                    string h = "Bạn đang khai báo thông tin tờ khai đưa vào thanh khoan đến:\r\n";
                    h += string.Format("Chi cục Hải quan:  {0}-{1}\r\n", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    h += string.Format("Service khai báo: {0}\r\n\n", GlobalSettings.DiaChiWS);
                    h += "Bạn có muốn khai báo thông tin tờ khai này không?";
                    Company.KDT.SHARE.Components.Globals.IsKhaiTK = true;
                    if (this.ShowMessage(h, true) == "Yes")
                    {
                        SendV3();
                        // Send();

                    }
                    Company.KDT.SHARE.Components.Globals.IsKhaiTK = false;
                    break;
                case "cmdSend":
                    string hoi = "Bạn đang khai báo thông tin tờ khai đến:\r\n";
                    hoi += string.Format("Chi cục Hải quan:  {0}-{1}\r\n", GlobalSettings.MA_HAI_QUAN, GlobalSettings.TEN_HAI_QUAN);
                    hoi += string.Format("Service khai báo: {0}\r\n\n", GlobalSettings.DiaChiWS);
                    hoi += "Bạn có muốn khai báo thông tin tờ khai này không?";

                    if (this.ShowMessage(hoi, true) == "Yes")
                    {
                        SendV3();
                        // Send();
                        checkTrangThaiXuLyTK();
                    }
                    break;
                case "ChungTuKemTheo":
                    AddChungTu();
                    break;
                case "NhanDuLieu":
                    //NhanPhanLuongToKhai();
                    FeedBackV3();
                    checkTrangThaiXuLyTK();
                    break;
                case "XacNhan":
                    // LaySoTiepNhanDT();
                    FeedBackV3();
                    checkTrangThaiXuLyTK();
                    break;
                case "FileDinhKem":
                    GetFileDinhKem();
                    break;
                case "Huy":
                    //HuyToKhai();
                    CancelV3();
                    break;
                case "CanDoiNhapXuat":
                    ShowCanDoiNhapXuat();
                    break;
                case "cmdNhieuHang":
                    ShowThemNhieuHang();
                    break;
                case "PhanBo":
                    ThucHienPhanBoToKhai();
                    break;
                case "cmdXuatExcelHang":
                    XuatExcelHang();
                    break;
                case "cmdVanDon":
                    txtSoVanTaiDon_ButtonClick(null, null);
                    break;
                case "cmdHopDong":
                    ViewHopDongTM(false);
                    break;
                case "cmdGiayPhep":
                    txtSoGiayPhep_ButtonClick(null, null);
                    break;
                case "cmdHoaDonThuongMai":
                    txtSoHoaDonThuongMai_ButtonClick(null, null);
                    break;
                case "cmdCO":
                    QuanLyCo("");
                    break;
                case "GiayKiemTra":
                    this.QLGiayKiemTra(false);
                    break;
                case "BoSungGiayKiemTra":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.QLGiayKiemTra(true);
                    break;
                case "ChungThuGiamDinh":
                    this.QLChungThuGiamDinh(false);
                    break;
                case "BoSungChungThuGiamDinh":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.QLChungThuGiamDinh(true);
                    break;
                case "cmDeNghiChuyenCuaKhau":
                    DeNghiChuyenCuaKhau("");
                    break;
                //
                case "cmdHuyTokhaiDaDuyet":
                    CancelV3();
                    // HuyToKhaiDaDuyet();
                    break;
                case "cmdSuaToKhaiDaDuyet":
                    SuaToKhaiDaDuyet();
                    break;
                case "cmdFileAnh":
                    AddChungTuAnh(false);
                    break;
                case "cmdKetQuaXuLy":
                    XemKetQuaXuLy();
                    break;
                case "cmdChungTuDangAnh":
                    AddChungTuAnh(false);
                    break;
                //Chung tu bo sung
                case "cmdVanDonBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoVanTaiDon_ButtonClick("1", null);
                    break;
                case "cmdHoaDonThuongMaiBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                    break;
                case "cmdCOBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.QuanLyCo("1");
                    break;
                case "cmdHopDongBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    //this.txtSoHopDong_ButtonClick("1", null);
                    this.ViewHopDongTM(true);
                    break;
                case "cmdGiayPhepBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.txtSoGiayPhep_ButtonClick("1", null);
                    break;
                case "cmdChuyenCuaKhauBoSung":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.DeNghiChuyenCuaKhau("1");
                    break;
                case "cmdChungTuDangAnhBS":
                    if (!ValidateKhaiBoSung(TKMD.SoToKhai)) return;
                    this.AddChungTuAnh(true);
                    break;
                case "cmdChungTuNo":
                    AddChungTuNo();
                    break;
                case "cmdNPLCungUng":
                    AddNPLCungUng();
                    break;
                //Update by KhanhHN 28-03-2012

                case "ToKhaiTriGia":
                    this.AddToKhaiTriGia();
                    ReViewData();
                    break;
                case "cmdToKhaiTGPP2":
                    this.AddToKhaiTriGia2();
                    ReViewData();
                    break;
                case "cmdToKhaiTGPP3":
                    this.AddToKhaiTriGia3();
                    ReViewData();
                    break;
                case "cmdGiayNopTien":
                    ShowGiayNopTien();
                    break;
                case "cmdHuyToKhaiDaDuyet":
                    ShowHuyToKhaiDaDuyet();
                    break;
                //Chuc nang in
                case "cmdInTKTT15":
                    this.InToKhaiTQDT_TT15();
                    break;
                case "cmdInTKTCTT196":
                    this.InToKhaiTaiChoTT196();
                    break;
                case "cmdInTKDTSuaDoiBoSung":
                    InToKhaiDienTuSuaDoiBoSung();
                    break;
                case "cmdContainer":
                    InContainer();
                    break;
                case "cmdInAnDinhThue":
                    InAnDinhThue(this.TKMD.ID, this.TKMD.GUIDSTR);
                    break;
                case "cmdBangKeHangHoa_HD":
                    BangKe_HD();
                    break;
                case "cmdInPhieuKetQuaKiemTra":
                    InPhieuKiemTra_CT();
                    break;
                case "cmdInPhieuKiemTraHangHoa":
                    InPhieuKiemTra_HH();
                    break;
                case "cmdPhanHoiPhanLuong":
                    FeedBackV3("31");
                    break;
                case "cmdPhanHoiSoTiepNhan":
                    FeedBackV3("29");
                    break;
                case "cmdPhanHoiSoToKhai":
                    FeedBackV3("30");
                    break;
                case "cmdPhuKienToKhai":
                    ShowPhuKien();
                    break;
            }
        }


        #endregion
        private void ShowPhuKien()
        {
            PhuKienManagerFrm f = new PhuKienManagerFrm();
            f.TKMD = TKMD;
            f.ShowDialog();
        }
        private void ShowHuyToKhaiDaDuyet()
        {
            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + TKMD.SoToKhai.ToString();
            List<Company.KDT.SHARE.QuanLyChungTu.HuyToKhai> items = Company.KDT.SHARE.QuanLyChungTu.HuyToKhai.SelectCollectionDynamic(string.Format("TKMD_ID={0}", TKMD.ID), "");
            if (items.Count == 0 && MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {

                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = TKMD;
                f.ShowDialog();

            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = TKMD;
                f.ShowDialog();
            }
        }
        private void ShowGiayNopTien()
        {
            ListGiayNopTienForm giayNopTienList = new ListGiayNopTienForm();
            giayNopTienList.TKMD = this.TKMD;
            giayNopTienList.ShowDialog();

        }
        private void ReViewData()
        {
            dgToKhaiTriGia.DataSource = this.TKMD.TKTGCollection;
            dglistTKTK23.DataSource = this.TKMD.TKTGPP23Collection;
        }
        private void AddToKhaiTriGia()
        {
            if (this.TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin tờ khai trước khi nhập tờ khai trị giá.", "MSG_SAV09", "", false);
                return;
            }
            ToKhaiTriGiaForm f = new ToKhaiTriGiaForm();
            f.TKMD = this.TKMD;

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog();
            try
            {
                dgToKhaiTriGia.Refetch();
            }
            catch
            {
                dgToKhaiTriGia.Refresh();
            }
        }

        private void AddToKhaiTriGia2()
        {
            if (this.TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin tờ khai trước khi nhập tờ khai trị giá.", "MSG_SAV09", "", false);
                return;
            }
            HangTKTGPP23Form f = new HangTKTGPP23Form();
            f.TKTG23 = new ToKhaiTriGiaPP23();
            f.TKTG23.MaToKhaiTriGia = 1;
            f.TKMD = this.TKMD;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog();
            if (f.TKTG23.ID > 0)
            {
                TKMD.TKTGPP23Collection.Add(f.TKTG23);
                try
                {
                    dglistTKTK23.DataSource = TKMD.TKTGPP23Collection;
                    dglistTKTK23.Refetch();
                }
                catch
                {
                    dglistTKTK23.Refresh();
                }
            }

        }
        private void AddToKhaiTriGia3()
        {
            if (this.TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin tờ khai trước khi nhập tờ khai trị giá.", "MSG_SAV09", "", false);
                return;
            }
            HangTKTGPP23Form f = new HangTKTGPP23Form();
            f.TKTG23 = new ToKhaiTriGiaPP23();
            f.TKTG23.MaToKhaiTriGia = 2;
            f.TKMD = this.TKMD;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog();
            if (f.TKTG23.ID > 0)
            {
                TKMD.TKTGPP23Collection.Add(f.TKTG23);
                try
                {
                    dglistTKTK23.DataSource = TKMD.TKTGPP23Collection;
                    dglistTKTK23.Refetch();
                }
                catch
                {
                    dglistTKTK23.Refresh();
                }
            }

        }
        //datlmq bo sung ham KiemTraTrangThaiToKhai 07/09/2010
        private void checkTrangThaiXuLyTK()
        {
            NoiDungChinhSuaTKForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKForm();
            noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(TKMD.ID);
            //Nếu là tờ khai sửa đã được duyệt
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa đang chờ duyệt
            else if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa mới
            else
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        ndDieuChinhTK.Update();
                    }
                }
            }
        }

        //datlmq update InToKhaiDienTuSuaDoiBoSung 30/08/2010

        private void HuyToKhaiDaDuyet()
        {
            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + TKMD.SoToKhai.ToString();
            msg += "\n----------------------";
            //msg += "\nCó " + TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    HuyToKhaiForm f = new HuyToKhaiForm();
                    f.TKMD = TKMD;
                    f.ShowDialog();
                }
            }
            else
            {
                HuyToKhaiForm f = new HuyToKhaiForm();
                f.TKMD = TKMD;
                f.ShowDialog();
            }
        }

        #region Bổ sung Khai báo sửa tờ khai
        private void NhanPhanHoiSuaTK()
        {
            string password = "";
            try
            {
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;

                for (int i = 0; i < 3; i++)
                {
                    LayPhanHoiSuaTK(password);
                }
            }
            catch (Exception ex)
            {
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (MLMessages("Thông tin khai báo không thành công. Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.NHAN_THONG_TIN;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            ShowMessage("Không thể kết nối đến hệ thống Hải quan:" + msg[0], false, true, ex.ToString());
                        }
                        else
                        {
                            //if (MLMessages("Có lỗi trong nhận thông tin : " + msg[0], "MSG_SEN20", "", true) == "Yes")
                            ShowMessage("Có lỗi trong nhận thông tin: " + msg[0], false, true, ex.ToString());
                            if (msg[0] == "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        //MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        ShowMessage("Xảy ra lỗi không xác định.", false, true, ex.ToString());
                        GlobalSettings.PassWordDT = "";
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void LayPhanHoiSuaTK(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 25/11/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, TKMD.GUIDSTR));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = Company.GC.BLL.EntityBase.GetPathProgram();
                docNPL.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = TKMD.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN;
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Company.KDT.SHARE.Components.Utils.FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = TKMD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = TKMD.WSRequestPhanLuong(pass);
                xmlCurrent = TKMD.WSRequestPhanLuong(pass);

                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    //LayPhanHoiSuaTK(pass);
                    return;
                }

                //}
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối";
                        this.TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        this.TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI
                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Tờ khai bị từ chối: " + Company.GC.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.GC.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                this.TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.GC.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(this.TKMD.ID, Company.GC.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void SuaToKhaiDaDuyet()
        {
            try
            {

                //DATLMQ bổ sung Request thông tin trả về từ Hải quan trước khi chuyển sang trạng thái Sửa TK 25/05/2011
                if ((!TKMD.GUIDSTR.Equals("")) && TKMD.SoTiepNhan > 0)
                    FeedBackV3();

                string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
                msg += "\n\nSố tờ khai: " + TKMD.SoToKhai.ToString();
                msg += "\n----------------------";
                //msg += "\nCó " + TKMD.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
                if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
                {
                    long id = TKMD.ID;
                    TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                    //Cap nhat trang thai cho to khai
                    if (TKMD.MaLoaiHinh.Contains("V"))
                    {
                        this.TKMD.GUIDSTR = Guid.NewGuid().ToString();
                    }
                    else
                    {
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;
                    }
                    string msgOginal = Helpers.Serializer(TKMD);

                    if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                        Company.KDT.SHARE.Components.Globals.SaveMessage(msgOginal, TKMD.ID, TKMD.GUIDSTR, MessageTypes.ToKhaiNhap, MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");
                    else
                        Company.KDT.SHARE.Components.Globals.SaveMessage(msgOginal, TKMD.ID, TKMD.GUIDSTR, MessageTypes.ToKhaiXuat, MessageFunctions.SuaToKhai, "Thay đổi trạng thái", "Thay đổi sang trạng thái sửa tờ khai");


                    TKMD.Update();
                    #region log cũ
                    ////Cap nhat trang thai sua to khai
                    //Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                    //kqxl.ItemID = TKMD.ID;
                    //kqxl.ReferenceID = new Guid(TKMD.GUIDSTR);
                    //kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                    //kqxl.LoaiThongDiep = "Chuyển trạng thái sửa tờ khai"; // Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                    //kqxl.NoiDung = msgOginal;
                    //kqxl.Ngay = DateTime.Now;
                    //kqxl.Insert();

                    ////DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                    //try
                    //{
                    //    string where = "1 = 1";
                    //    where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", TKMD.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.ToKhai);
                    //    List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                    //    if (listLog.Count > 0)
                    //    {
                    //        long idLog = listLog[0].IDLog;
                    //        string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                    //        long idDK = listLog[0].ID_DK;
                    //        string guidstr = TKMD.GUIDSTR;
                    //        string userKhaiBao = listLog[0].UserNameKhaiBao;
                    //        DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                    //        string userSuaDoi = GlobalSettings.UserLog;
                    //        DateTime ngaySuaDoi = DateTime.Now;
                    //        string ghiChu = listLog[0].GhiChu;
                    //        bool isDelete = listLog[0].IsDelete;
                    //        Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                    //                                                                    userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                    //    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail("Không thể cập nhật user sửa đổi", TKMD.MaHaiQuan, new SendEventArgs(ex));
                return;
            }

            SetCommandStatusSuaHuyToKhai();
        }
        private void XemKetQuaXuLy()
        {

            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.TKMD.ID;
            bool isToKhaiNhap = TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N";
            form.DeclarationIssuer = isToKhaiNhap ? DeclarationIssuer.GC_TOKHAI_NHAP : DeclarationIssuer.GC_TOKHAI_XUAT;
            form.ShowDialog(this);

        }

        private void AddChungTuAnh(bool isKhaiBoSung)
        {
            var ctForm = new ListChungTuKemForm { TKMD = TKMD };
            ctForm.isKhaiBoSung = isKhaiBoSung;
            ctForm.ShowDialog();
        }

        //private void HuyToKhaiMd()
        //{
        //    if (ShowMessage("Tờ khai đã được cấp số, bạn có chắc chắn muốn hủy không ?", true) == "Yes")
        //    {
        //        var f = new HuyToKhaiForm { TKMD = TKMD };
        //        f.ShowDialog();
        //    }
        //}
        //private void SuaToKhaiDaDuyet()
        //{
        //    if (ShowMessage("Tờ khai đã được cấp số, bạn có chắc chắn muốn sửa không ?", true) == "Yes")
        //    {
        //        var f = new SuaToKhaiForm { TKMD = TKMD };
        //        f.ShowDialog();
        //    }

        //}

        private void DeNghiChuyenCuaKhau(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            var f = new ListDeNghiChuyenCuaKhauTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();
        }
        private void QLChungThuGiamDinh(bool isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ChungThuGiamDinhForm f = new ChungThuGiamDinhForm();
            f.isKhaiBoSung = isKhaiBoSung;
            f.TKMD = TKMD;
            f.ChungTuGiamDinh = TKMD.ChungThuGD;
            f.ShowDialog(this);
        }
        private void QLGiayKiemTra(bool isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayKiemTraForm f = new ListGiayKiemTraForm();
            f.isKhaiBoSung = isKhaiBoSung;
            f.TKMD = TKMD;
            f.ShowDialog(this);
        }
        private void QuanLyCo(string isKhaiBoSung)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            var f = new ListCOTKMDForm();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;
            f.ShowDialog();
        }

        private void InTrangA4()
        {
            if (TKMD.HMDCollection.Count == 0) return;
            switch (TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    //ReportViewTKNA4Form f = new ReportViewTKNA4Form();
                    var f = new ReportViewTKNTQDTForm { TKMD = TKMD };
                    f.ShowDialog();
                    break;

                case "X":
                    //ReportViewTKXA4Form f1 = new ReportViewTKXA4Form();
                    var f1 = new ReportViewTKXTQDTForm { TKMD = TKMD };
                    f1.ShowDialog();
                    break;
            }
        }

        private void XuatExcelHang()
        {
            var sfNpl = new SaveFileDialog
                            {
                                RestoreDirectory = true,
                                InitialDirectory = Application.StartupPath,
                                Filter = "Excel files| *.xls"
                            };
            sfNpl.ShowDialog();
            if (sfNpl.FileName != "")
            {
                var str = sfNpl.OpenFile();
                gridEXExporterHangToKhai.Export(str);
                str.Close();
                //if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                if (Globals.ShowMessageTQDT("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNpl.FileName);
                }
            }
        }

        #region Phân bổ tờ khai
        public void ThucHienPhanBoToKhai()
        {
            try
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    //showMsg("MSG_ALL03");
                    //MLMessages("Tờ khai này đã được phân bổ.", "MSG_ALL03", "", false);
                    Globals.ShowMessageTQDT("Tờ khai này đã được phân bổ.", false);
                    return;
                }
                if (TKMD.LoaiHangHoa == "S")
                {
                    int cnt = 0;
                    PhanBoToKhaiXuat.PhanBoChoToKhaiXuatGC(TKMD, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc, ref cnt);
                    //showMsg("MSG_ALL04");
                    //MLMessages("Thực hiện phân bổ thành công.","MSG_ALL04","", false);
                    Globals.ShowMessageTQDT("Thực hiện phân bổ thành công.", false);
                }
                else if (TKMD.LoaiHangHoa == "N")
                {

                    var f = new SelectNPLTaiXuatForm { TKMD = TKMD };
                    TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                    foreach (var hmd in TKMD.HMDCollection)
                    {
                        var pbToKhaiTaiXuat = new PhanBoToKhaiXuat
                                                  {
                                                      ID_TKMD = TKMD.ID,
                                                      MaDoanhNghiep = GlobalSettings.MA_DON_VI,
                                                      MaSP = hmd.MaPhu,
                                                      SoLuongXuat = hmd.SoLuong
                                                  };
                        TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                    }
                    f.pbToKhaiXuatCollection = TKMD.PhanBoToKhaiXuatCollection;
                    f.ShowDialog();
                    if (f.isPhanBo)
                        //showMsg("MSG_ALL04");
                        Globals.ShowMessageTQDT("Thực hiện phân bổ thành công.", false);
                }
                // Message("MSG_ALL04", "", false);
                //ShowMessage("Thực hiện phân bổ thành công.", false);
            }
            catch (Exception ex)
            {
                //showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Có lỗi khi phân bổ : "+ex.Message, false);
                Globals.ShowMessageTQDT("Có lỗi khi phân bổ : " + ex.Message, false);
            }
        }
        #endregion

        private void ShowThemNhieuHang()
        {
            try
            {
                AddDataToToKhai();
                if (NhomLoaiHinh == "NGC" || NhomLoaiHinh == "XGC")
                {
                    if (txtSoHopDong.Text == "")
                    {
                        epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "Contract no. must be filled"));
                        epError.SetIconPadding(txtSoHopDong, -8);
                        return;
                    }
                    epError.Clear();
                }
                cvError.ContainerToValidate = grbNguyenTe;
                cvError.Validate();
                if (!cvError.IsValid) return;
                var f = new KDT.ToKhai.SelectNhieuHangMauDichForm { HD = Hdgc, HMDCollection = new List<HangMauDich>() };
                foreach (var hmd in TKMD.HMDCollection)
                {
                    var hmdNew = hmd.Copy();
                    f.HMDCollection.Add(hmdNew);
                }
                f.TKMD = TKMD;
                f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                f.ShowDialog();
                txtSoLuongPLTK.Text = TKMD.SoLuongPLTK.ToString();
                try
                {
                    dgList.DataSource = TKMD.HMDCollection;
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new SendEventArgs(ex));
            }
        }
        private void ShowCanDoiNhapXuat()
        {
            var f = new CanDoiNhapXuatForm { TKMD = TKMD, tableCanDoi = GetTableCanDoi() };
            if (f.tableCanDoi == null)
                return;
            f.ShowDialog();
        }
        private void GetFileDinhKem()
        {
            var f = new SendMailChungTuForm { TKMD = TKMD };
            f.ShowDialog();
        }

        #region Bổ sung Hủy khai báo Tờ khai: DATLMQ 26/05/2011
        private void LayPhanHoiHuyKhaiBao(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    this.Refresh();
                    return;
                }
                string mess = "";
                try
                {
                    //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                    if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                    }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "Tờ khai sửa bị từ chối:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                        msg += " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                        TKMD.Update();
                        this.ShowMessageTQDT(msg, false);
                    }
                    else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string msg = "Tờ khai sửa chờ duyệt:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                        msg += " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                        this.ShowMessageTQDT(msg, false);
                    }
                    else
                    {// LÀ TỜ KHAI MỚI

                        if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + " Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Xanh";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                        {
                            mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Vàng";

                        }
                        else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                        {
                            mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                            this.ShowMessageTQDT("Tờ khai đã được cấp số\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n" + mess, false);
                            lblPhanLuong.Text = "Luồng Đỏ";

                        }
                        else
                        {
                            if (TKMD.ActionStatus == -1)
                            {
                                this.ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa nhận được phản hồi từ hệ thống Hải quan. ", false);
                            }
                            else
                            {
                                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                                {
                                    if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                        this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                                    else
                                        this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                                }
                                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                                {
                                    string msg = "Phản hồi từ hải quan: " + Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                    if (TKMD.ActionStatus != (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                    {
                                        TKMD.SoTiepNhan = 0;
                                        TKMD.SoToKhai = 0;
                                        TKMD.NgayTiepNhan = new DateTime(1900, 1, 1);
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                        TKMD.PhanLuong = "";
                                        TKMD.HUONGDAN = "";
                                    }
                                    else
                                        TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                    TKMD.Update();
                                    isDeny = true;
                                    this.ShowMessageTQDT(msg, false);
                                }
                                else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                                {
                                    string msg = Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                    this.ShowMessageTQDT(msg, false);
                                }
                                else
                                {
                                    this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                                }

                            }
                        }
                    }
                }
                catch (Exception exx) { ShowMessage(exx.Message, false); }


                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        #endregion

        private void HuyToKhai()
        {
            ToKhaiMauDich tkmd = TKMD;
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();

            if (tkmd != null)
            {
                tkmd = TKMD;
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.Load();
            }
            else
            {
                MLMessages("Chưa chọn thông tin để hủy.", "MSG_CNL01", "", false);
                return;
            }
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
            this.Cursor = Cursors.WaitCursor;
            //DATLMQ bổ sung kiểm tra TK đã được HQ duyệt chưa 25/05/2011
            for (int i = 0; i < 2; i++)
            {
                LayPhanHoiHuyKhaiBao(password);
            }
            if (TKMD.SoToKhai > 0)
            {
                ShowMessage("Tờ khai đã có số tờ khai. Không thể hủy khai báo", false);
                return;
            }
            else if (ToKhaiMauDich.tuChoi)
            {
                ShowMessage("Tờ khai đã bị từ chối. Không thể hủy khai báo", false);
                isDeny = false;
                return;
            }
            try
            {
                xmlCurrent = tkmd.WSCancelXMLGC(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = tkmd.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();

                #region Nhận thông tin phản hồi cũ
                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(password, xmlCurrent);


                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    return;
                }

                string mess = "";
                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }

                    mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();

                #endregion
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            //showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //showMsg("MSG_WRN13", ex.Message);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);

                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", "", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now);
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void NhanDuLieuToKhai()
        {
            var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
            string password = "";
            if (sendXml.Load())
            {
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN01", "", false);
                //showMsg("MSG_SEN01");
                Globals.ShowMessageTQDT("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                var wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                Cursor = Cursors.WaitCursor;
                _xmlCurrent = TKMD.WSRequestXMLGC(password);
                sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID, msg = _xmlCurrent, func = 2 };
                _xmlCurrent = "";

                sendXml.InsertUpdate();

                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            //showMsg("MSG_WRN12");
                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            //showMsg("MSG_2702016", msg[0]);
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //showMsg("MSG_WRN13", ex.Message);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private bool ProcessMessage(string sfmtMessage)
        {
            //LanNT: pattern format header is ERROR:Content=
            string pattern = @"(?<error>\w+):Content=(?<content>(\w|\.)+)";
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex(pattern);
            string msg = string.Empty;
            string msgType = string.Empty;
            foreach (Match item in regex.Matches(sfmtMessage))
            {
                msg += item.Groups["content"].Value;
                msgType = item.Groups["error"].Value;
            }

            // Thực hiện kiểm tra. 

            if (msgType.Equals("NoInfo"))
            {
                string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                if (kq == "Yes")
                {
                    return true;
                }

            }
            else if (msgType.Equals("TuChoi"))
            {
                msg = "Hải quan từ chối với thông báo:\r\n " + msg;
                ShowMessage(msg, false);
            }
            else if (msgType.Equals("ERROR"))
            {
                msg = "Lỗi từ hệ thống hải quan trả về :\r\n" + msg;
                ShowMessage(msg, false);
            }
            else
            {
                string kq = MLMessages(msg + "\r\nnBạn có muốn tiếp tục lấy phân luồng thông tin không?", "MSG_STN02", "", true);
                if (kq == "Yes")
                {
                    return true;
                }
            }
            return false;
        }

        #region Nhận phân luồng tờ khai
        public bool isDisplay = true;
        private void LayPhanHoiPhanLuong(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                sendXML.func = 2;
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);
                this.Cursor = Cursors.Default;

                //if (ProcessMessage(xmlCurrent))
                //{
                //    this.Refresh();
                //    LayPhanHoiPhanLuong(pass);
                //}
                #region Tam thoi
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                string mess = "";



                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                        mess = "Khai báo thông tin sửa tờ khai thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    else
                        mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        InAnDinhThue();
                    }
                }


                #endregion
                //Không dùng LanNT
                //Lý do đã cập nhật thông tin 1 lần. từ lấy phản hồi
                //Không được cập nhật lần 2 khi phân tích message thông báo;
                //Lấy thông báo chưa chính xác
                //if (isDisplay)
                //showThongBao();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false); 
                        MLMessages("Xảy ra lỗi không xác định. ", "MSG_SEN20", "", false);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void InAnDinhThue()
        {
            Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG, 0);
        }



        private void showThongBao()
        {
            string mess = "";
            try
            {
                isDisplay = true;
                //NẾU LÀ TK SỬA ĐÃ ĐƯỢC DUYỆT
                if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                {
                    this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                }// NẾU LÀ TK SỬA BỊ TỪ CHỐI
                else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    string msg = "Tờ khai sửa bị từ chối:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                    msg += " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                    TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                    TKMD.Update();
                    this.ShowMessageTQDT(msg, false);
                }
                else if (TKMD.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    string msg = "Tờ khai sửa chờ duyệt:\r\nSố tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n" + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n";
                    msg += "Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n";
                    this.ShowMessageTQDT(msg, false);
                }
                else
                {// LÀ TỜ KHAI MỚI

                    if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_XANH)
                    {
                        mess = "Phân luồng tờ khai : " + "LUỒNG XANH" + "\r\n" + "Hướng dẫn của cơ quan Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessageTQDT(mess, false);
                        lblPhanLuong.Text = "Luồng Xanh";
                        isDisplay = false;
                    }
                    else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                    {
                        mess = "Phân luồng tờ khai : " + "LUỒNG VÀNG" + "\r\n" + "Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessageTQDT(mess, false);
                        lblPhanLuong.Text = "Luồng Vàng";
                        isDisplay = false;
                    }
                    else if (TKMD.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                    {
                        mess = "Phân luồng tờ khai: " + "LUỒNG ĐỎ" + "\r\n" + " Hướng dẫn của Hải quan : " + TKMD.HUONGDAN.ToString() + "\r\n";
                        this.ShowMessageTQDT(mess, false);
                        lblPhanLuong.Text = "Luồng Đỏ";
                        isDisplay = false;
                    }
                    else
                    {
                        if (TKMD.ActionStatus == -1)
                        {
                            this.ShowMessageTQDT("Thông báo từ hệ thống ECS", "Chưa nhận được phản hồi từ hệ thống Hải quan. ", false);
                        }
                        else
                        {
                            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                            {
                                if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                    this.ShowMessageTQDT("Tờ khai sửa đã được duyệt :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                                else
                                    this.ShowMessageTQDT("Tờ khai đã được cấp số :\r\n " + "Số tờ khai : " + TKMD.SoToKhai.ToString() + "\r\n" + " Loại hình đăng ký : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm đăng ký :" + TKMD.NgayDangKy.Year.ToString() + "\r\n", false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                            {
                                string msg = "Phản hồi từ hải quan: " + Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                                if (TKMD.ActionStatus != (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                                {
                                    TKMD.SoTiepNhan = 0;
                                    TKMD.SoToKhai = 0;
                                    TKMD.NgayTiepNhan = new DateTime(1900, 1, 1);
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                    TKMD.PhanLuong = "";
                                    TKMD.HUONGDAN = "";
                                }
                                else
                                    TKMD.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                                TKMD.Update();
                                this.ShowMessageTQDT(msg, false);
                            }
                            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                            {
                                string msg = Company.KDT.SHARE.Components.KetQuaXuLy.LayKetQuaXuLy(TKMD.ID, Company.KDT.SHARE.Components.KetQuaXuLy.LoaiThongDiep_KhaiBaoThanhCong);
                                this.ShowMessageTQDT(msg, false);
                            }
                            else
                            {
                                this.ShowMessageTQDT("Hải quan chưa xử lý tờ khai này.", false);
                            }

                        }
                    }
                }
            }
            catch (Exception exx) { ShowMessage(exx.Message, false); }

        }

        private void NhanPhanLuongToKhai()
        {
            var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
            string password = "";
            if (sendXml.Load())
            {
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_SEN01", "", false);
                //showMsg("MSG_SEN01");
                //Globals.ShowMessageTQDT("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                //XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                Cursor = Cursors.WaitCursor;
                LayPhanHoiPhanLuong(password);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {

                            //showMsg("MSG_WRN12");
                            Globals.ShowMessageTQDT("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.", false);
                            return;
                        }
                        else
                        {
                            //showMsg("MSG_2702016", msg[0]);
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            //showMsg("MSG_WRN13", ex.Message);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        #endregion Phan Luông

        private void AddChungTu()
        {
            ChungTuForm f = new ChungTuForm();
            f.OpenType = OpenFormType.Insert;
            f.collection = TKMD.ChungTuTKCollection;
            f.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
            f.ShowDialog();
            TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            SetCommandStatus();

        }
        private void AddChungTuNo()
        {
            ListChungTuNoForm ctForm = new ListChungTuNoForm();
            ctForm.TKMD = this.TKMD;
            ctForm.ShowDialog();
        }
        private void AddNPLCungUng()
        {
            Company.Interface.KDT.GC.NPLCungUngForm ctForm = new Company.Interface.KDT.GC.NPLCungUngForm();
            ctForm.TKMD = this.TKMD;
            ctForm.ShowDialog();
        }
        private void Send()
        {
            if (TKMD.ID == 0)
            {
                MLMessages("Lưu thông tin trước khi gửi!", "MSG_SEN14", "", false);
                return;
            }
            string password = "";
            Cursor = Cursors.Default;
            var sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID };
            if (sendXml.Load())
            {
                ShowMessageTQDT("Hệ thống ECS thông báo", "Tờ khai đã gửi  tới hệ thống hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin.", false);

                //Hien thi nut Nhan du lieu
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                return;
            }
            var wsForm = new WSForm();
            try
            {

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                Cursor = Cursors.WaitCursor;
                if (MainForm.versionHD != 2)
                {
                    try
                    {
                        string st = WebServiceConnection.checkKhaiBao(GlobalSettings.MA_DON_VI.Trim(), GlobalSettings.TEN_DON_VI, GlobalSettings.DIA_CHI, GlobalSettings.MA_HAI_QUAN);
                        if (st != "")
                        {
                            ShowMessage(st, false);
                            Cursor = Cursors.Default;
                            return;
                        }

                    }
                    catch { }
                }

                if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    _xmlCurrent = TKMD.WSKhaiBaoToKhaiSua(password, GlobalSettings.MaMID);
                }
                else
                {
                    if (TKMD.MaLoaiHinh.StartsWith("NGC") || TKMD.MaLoaiHinh.StartsWith("NTA"))
                    {
                        _xmlCurrent = TKMD.WSSendXMLNHAP(password);
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("XGC"))
                    {
                        _xmlCurrent = TKMD.WSSendXMLXuat(password, GlobalSettings.MaMID);
                    }
                }
                Cursor = Cursors.Default;

                sendXml = new MsgSend { LoaiHS = "TK", master_id = TKMD.ID, msg = _xmlCurrent, func = 1 };

                _xmlCurrent = "";

                sendXml.InsertUpdate();
                dgList.AllowDelete = InheritableBoolean.False;
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            ShowMessage("Không kết nối được với Hệ thống Hải quan.\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                            return;
                        }
                        else
                        {
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() == "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + "Chi tiết lỗi:\r\n" + ex.Message, false, true, string.Empty);
                        }
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void SoTiepNhanDTTQDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    //MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
                    //  showMsg("MSG_SEN01");
                    //// return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                Cursor = Cursors.WaitCursor;
                {
                    _xmlCurrent = TKMD.LayPhanHoiGC(password, sendXML.msg);
                }
                Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (_xmlCurrent != "")
                {
                    doc.LoadXml(_xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        string kq = Globals.ShowMessageTQDT("Chưa nhận được phản hồi từ hệ thống hải quan", true);
                        if (kq == "Yes")
                        {
                            Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            lblTrangThai.Text = "Chờ xác nhận";
                            // if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait for confirmation";
                        }
                        return;
                    }
                }
                if (sendXML.func == 1)
                {
                    //showMsg("MSG_SEN02", TKMD.SoTiepNhan);
                    ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Đăng ký thành công ! \r\n Số TNĐKĐT : " + TKMD.SoTiepNhan.ToString() + "\r\n Năm đăng ký : " + TKMD.NamDK.ToString(), false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = "Chờ duyệt chính thức";
                    // if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Wait for approval"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    //MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);
                    //showMsg("MSG_2702024");
                    Globals.ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not declared yet"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {

                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }

                        //showMsg("MSG_SEN11", new string[] { TKMD.SoToKhai.ToString(), mess });
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN11", TKMD.SoToKhai.ToString(), false);
                        Globals.ShowMessageTQDT("Số tờ khai : " + TKMD.SoToKhai.ToString() + mess, false);

                        lblTrangThai.Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }

                        Globals.ShowMessageTQDT("Hải quan chưa duyệt danh sách này", false);
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa duyệt danh sách này", "MSG_SEN04", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        Globals.ShowMessageTQDT("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        //showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN05", "", false);
                        lblTrangThai.Text = "Hải quan không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not approved"; }
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}

                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            //showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                SetCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                            //showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message,false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            var doc = new XmlDocument();
            XmlNode node = null;
            var wsForm = new WSForm();
            var sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    // MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_STN01", "", false);
                    //showMsg("MSG_SEN01");
                    Globals.ShowMessageTQDT("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                Cursor = Cursors.WaitCursor;
                {
                    _xmlCurrent = TKMD.LayPhanHoiGC(password, sendXML.msg);
                }
                Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (_xmlCurrent != "")
                {
                    doc.LoadXml(_xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        string kq = Globals.ShowMessageTQDT("Chưa nhận được phản hồi từ hệ thống hải quan. \nBạn có muốn tiếp tục lấy phản hồi không?.", true);
                        if (kq == "Yes")
                        {
                            Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            lblTrangThai.Text = "Chờ xác nhận";
                            // if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Wait for confirmation";
                        }
                        return;
                    }
                }
                if (sendXML.func == 1)
                {
                    //showMsg("MSG_SEN02", TKMD.SoTiepNhan);
                    ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Khai báo thành công ! \r\n Số tiếp nhận : " + TKMD.SoTiepNhan.ToString() + "\r\n Năm đăng ký : " + TKMD.NamDK.ToString(), false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = "Chờ duyệt chính thức";
                    // if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Wait for approval"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    Globals.ShowMessageTQDT("Hủy khai báo thành công", false);
                    //MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);
                    //showMsg("MSG_2702024");
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = "Chưa khai báo";
                    if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not declared yet"; }
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        //showMsg("MSG_SEN11", new string[] { TKMD.SoToKhai.ToString(), mess });

                        Globals.ShowMessageTQDT("Số tờ khai : " + TKMD.SoToKhai.ToString() + mess, false);
                        lblTrangThai.Text = "Đã duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Approved"; }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        if (string.IsNullOrEmpty(mess))
                            mess = "Hải quan chưa duyệt tờ khai này";
                        Globals.ShowMessageTQDT(mess, false);
                        //showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa duyệt danh sách này", "MSG_SEN04", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        Globals.ShowMessageTQDT("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        //showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN05", "", false);
                        lblTrangThai.Text = "Hải quan không phê duyệt";
                        if (GlobalSettings.NGON_NGU == "1") { lblTrangThai.Text = "Not approved"; }
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}

                            Globals.ShowMessageTQDT("Không kết nối được với hệ thống hải quan.", false);
                            //showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            Globals.ShowMessageTQDT("Có lỗi trong khai báo : " + msg[0], false);
                            //showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                SetCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            //showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message,false);
                            Globals.ShowMessageTQDT("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            //showMsg("MSG_2702004", ex.Message);
                            Globals.ShowMessageTQDT(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void sendMailChungTu()
        {
            Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    //showMsg("MSG_MAL01");
                    Globals.ShowMessageTQDT("Chưa cấu hình mail của doanh nghiệp", false);

                if (GlobalSettings.MailHaiQuan == "")
                    //showMsg("MSG_MAL02");
                    Globals.ShowMessageTQDT("Chưa cấu hình mail của hải quan tiếp nhận", false);

                return;
            }
            var mail = new MailMessage
                           {
                               From = new MailAddress(GlobalSettings.MailDoanhNghiep, GlobalSettings.TEN_DON_VI, System.Text.Encoding.UTF8)
                           };
            string[] mailArray = GlobalSettings.MailHaiQuan.Split(';');
            for (int k = 0; k < mailArray.Length; ++k)
                if (mailArray[k] != "")
                    mail.To.Add(mailArray[k]);
            mail.Subject = "Khai báo điện tử - " + GlobalSettings.MA_DON_VI + "- Tờ khai có số tiếp nhận : " + TKMD.SoTiepNhan.ToString();
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Hải quan điện tử - Duyệt tờ khai - Chứng từ ";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            mail.Priority = MailPriority.High;

            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                try
                {
                    if (ct.FileUpLoad != "")
                    {
                        //FileStream file = File.Open(ct.FileUpLoad, FileMode.Open);
                        var att = new Attachment(ct.FileUpLoad);
                        mail.Attachments.Add(att);
                        mail.Body += "\n" + ct.TenChungTu + " ";
                        if (ct.SoBanChinh > 0)
                            mail.Body += "Số bản chính : " + ct.SoBanChinh;
                        if (ct.SoBanSao > 0)
                            mail.Body += "Số bản sao : " + ct.SoBanSao;
                        ++i;
                    }
                }
                catch
                {
                }
            }
            try
            {
                if (i > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    var smtp = new SmtpClient
                                   {
                                       Port = 587,
                                       Host = "smtp.gmail.com",
                                       EnableSsl = true,
                                       Credentials =
                                           new System.Net.NetworkCredential("haiquandientu@gmail.com",
                                                                            "haiquandientudanang")
                                   };
                    smtp.Send(mail);
                    //showMsg("MSG_MAL05");
                    //MLMessages("Gửi mail thành công.","MSG_MAL05","", false);
                    Globals.ShowMessageTQDT("Gửi mail thành công.", false);
                    Cursor = Cursors.Default;
                    Close();
                }
                Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                //showMsg("MSG_MAL06");
                Globals.ShowMessageTQDT("Không gửi chứng từ tới hải quan được.", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //HUNGTQ, Update 25/05/2010
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    return;

                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //xmlCurrent = TKMD.LayPhanHoi(pass, sendXML.msg);
                xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXML.msg);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;
                }
                string mess = "";



                if (sendXML.func == 1)
                {
                    if (TKMD.SoTiepNhan == 0)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    if (TKMD.ActionStatus == (short)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                        mess = "Khai báo thông tin sửa tờ khai thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    else
                        mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                    ShowMessageTQDT(mess, false);
                    txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");
                }
                //Nhận thông tin hủy tờ khai
                else if (sendXML.func == 3)
                {
                    ShowMessageTQDT("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), "MSG_SEN18", "" + TKMD.SoToKhai.ToString(), false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Đã duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Approved";
                        }
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", "MSG_SEN09", "", false);
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            lblTrangThai.Text = "Hải quan không phê duyệt";
                        }
                        else
                        {
                            lblTrangThai.Text = "Customs not yet Approved";
                        }

                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }

                if (TKMD.IsThongBaoThue)
                {
                    if (ShowMessage("Hải quan có trả về Thông báo Thuế.\nBạn có muốn in không?", true).ToUpper().Equals("YES"))
                    {
                        Globals.ExportExcelThongBaoThue(TKMD, TKMD.SoQD, TKMD.NgayQD, TKMD.NgayHetHan, TKMD.TaiKhoanKhoBac, TKMD.TenKhoBac,
                            TKMD.ChuongThueXNK, TKMD.LoaiThueXNK, TKMD.KhoanThueXNK, TKMD.MucThueXNK, TKMD.TieuMucThueXNK, TKMD.SoTienThueXNK,
                            TKMD.ChuongThueVAT, TKMD.LoaiThueVAT, TKMD.KhoanThueVAT, TKMD.MucThueVAT, TKMD.TieuMucThueVAT, TKMD.SoTienThueVAT,
                            TKMD.ChuongThueTTDB, TKMD.LoaiThueTTDB, TKMD.KhoanThueTTDB, TKMD.MucThueTTDB, TKMD.TieuMucThueTTDB, TKMD.SoTienThueTTDB,
                            TKMD.ChuongThueTVCBPG, TKMD.LoaiThueTVCBPG, TKMD.KhoanThueTVCBPG, TKMD.MucThueTVCBPG, TKMD.TieuMucThueTVCBPG, TKMD.SoTienThueTVCBPG, 0);
                    }
                }

                //xoa thông tin msg nay trong database
                sendXML.Delete();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            // if (ShowMessage("Không thể thực hiện khai báo thông tin đến Hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            if (MLMessages("Thông tin khai báo không thành công.Không kết nối được tới  hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", "MSG_SEN19", "", true) == "Yes")
                            {
                                HangDoi hd = new HangDoi();
                                hd.ID = TKMD.ID;
                                hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                hd.TrangThai = TKMD.TrangThaiXuLy;
                                hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                                hd.PassWord = pass;
                                MainForm.AddToQueueForm(hd);
                                MainForm.ShowQueueForm();
                            }
                        }
                        else
                        {
                            // ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            //DATLMQ comment ngày 29/03/2011
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN20", "", false);
                            ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n" + msg[0], false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                            sendXML.Delete();
                            SetCommandStatus();
                        }
                    }
                    else
                    {
                        //  ShowMessage("Xảy ra lỗi không xác định.", false);
                        //DATLMQ comment ngày 29/03/2011
                        //MLMessages("Xảy ra lỗi : " + ex.Message.ToString(), "MSG_SEN20", "", false);
                        ShowMessage("Thông tin trả về từ Hệ thống Hải quan:\r\n", false, true, "Chi tiết lỗi:\r\n" + ex.ToString() + " " + ex.StackTrace);
                        SetCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }


            /* 
             var sendXml = new MsgSend();
             var doc = new XmlDocument();
             XmlNode node = null;
             try
             {
                 if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                     return;


                 sendXml.LoaiHS = "TK";
                 sendXml.master_id = TKMD.ID;
                 sendXml.Load();
                 Cursor = Cursors.WaitCursor;

                 _xmlCurrent = TKMD.LayPhanHoiTQDTKhaiBao(pass, sendXml.msg);

                 Cursor = Cursors.Default;
                 // Thực hiện kiểm tra.                  
                 // Thực hiện kiểm tra.  
                 if (xmlCurrent != "")
                 {
                     string kq = MLMessages("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", "MSG_STN02", "", true);
                     if (kq == "Yes")
                     {
                         this.Refresh();
                         LayPhanHoiPhanLuong(pass);
                     }
                     return;
                 }
                 string mess = "";
                 if (sendXml.func == 1)
                 {
                     if (TKMD.SoTiepNhan == 0)
                     {
                         MLMessages("Hải quan chưa duyệt danh sách này", "MSG_STN08", "", false);
                         cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                         dgList.AllowDelete = InheritableBoolean.False;
                         return;
                     }

                     mess = "Khai báo thông tin thành công ! \r\n" + "Số tiếp nhận đăng ký điện tử : " + TKMD.SoTiepNhan.ToString() + "\r\n" + " Loại hình khai báo : " + TKMD.MaLoaiHinh.ToString() + "\r\n Năm tiếp nhận : " + TKMD.NamDK.ToString();
                     ShowMessageTQDT(mess, false);
                     txtSoTiepNhan.Text = TKMD.SoTiepNhan.ToString();
                     lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wait to approve");

                 }
                 //Nhận thông tin hủy tờ khai
                 else if (sendXml.func == 3)
                 {
                     ShowMessageTQDT("Hủy tờ khai thành công !", false);
                     txtSoTiepNhan.Text = "";
                     lblTrangThai.Text = "Chưa khai báo";
                     if (GlobalSettings.NGON_NGU == "1") lblTrangThai.Text = "Not declared yet";
                 }
                 else if (sendXml.func == 2)
                 {
                     if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                     {
                         ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Tờ khai đã được duyệt chính thức\r\n Số tờ khai : " + TKMD.SoToKhai.ToString(), false);
                         if (GlobalSettings.NGON_NGU == "0")
                         {
                             lblTrangThai.Text = "Đã duyệt";
                         }
                         else
                         {
                             lblTrangThai.Text = "Approved";
                         }
                         txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                     }
                     else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                     {
                         ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Tờ khai chưa được duyệt", false);

                         cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                         dgList.AllowDelete = InheritableBoolean.False;
                     }
                     else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                     {
                         ShowMessageTQDT("Thông báo trả về từ hệ thống Hải quan", "Tờ khai không phê duyệt duyệt", false);

                         if (GlobalSettings.NGON_NGU == "0")
                         {
                             lblTrangThai.Text = "Hải quan không phê duyệt";
                         }
                         else
                         {
                             lblTrangThai.Text = "Customs not yet Approved";
                         }

                         cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                         NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                         dgList.AllowDelete = InheritableBoolean.False;
                     }
                 }
                 //xoa thông tin msg nay trong database
                 sendXml.Delete();
                 SetCommandStatus();
             }
             catch (Exception ex)
             {
                 Cursor = Cursors.Default;
                 //if (loaiWS == "1")
                 {
                     #region FPTService
                     string[] msg = ex.Message.Split('|');
                     if (msg.Length == 2)
                     {
                         if (msg[1] == "DOTNET_LEVEL")
                         {
                             //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                             //{
                             //    HangDoi hd = new HangDoi();
                             //    hd.ID = TKMD.ID;
                             //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                             //    hd.TrangThai = TKMD.TrangThaiXuLy;
                             //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                             //    hd.PassWord = pass;
                             //    MainForm.AddToQueueForm(hd);
                             //    MainForm.ShowQueueForm();
                             //}
                             showMsg("MSG_WRN12");
                             //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                             return;
                         }
                         else
                         {
                             showMsg("MSG_2702016", msg[0]);
                             //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                             sendXml.Delete();
                             SetCommandStatus();
                         }
                     }
                     else
                     {
                         if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                         {
                             showMsg("MSG_WRN13", ex.Message);
                             //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                             sendXml.Delete();
                             SetCommandStatus();
                         }
                         else
                         {
                             GlobalSettings.PassWordDT = "";
                             showMsg("MSG_2702004", ex.Message);
                             //ShowMessage(ex.Message, false);
                         }
                     }
                     #endregion FPTService
                 }

                 StreamWriter write = File.AppendText("Error.txt");
                 write.WriteLine("--------------------------------");
                 write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                 write.WriteLine(ex.StackTrace);
                 write.WriteLine("Lỗi là : ");
                 write.WriteLine(ex.Message);
                 write.WriteLine("--------------------------------");
                 write.Flush();
                 write.Close();
             }
             finally
             {
                 Cursor = Cursors.Default;
             }

             */
        }
        //------------------------------------------------k-----------------------------------------
        private void ViewHopDongTM(bool loaikhai)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHopDongTKMDForm f = new ListHopDongTKMDForm();
            if (loaikhai == true)
            {
                f.isKhaiBoSung = true;
            }
            f.TKMD = TKMD;
            f.ShowDialog();

            //if (f.TKMD.SoHopDong != "")
            //{
            //    txtSoHopDong.Text = f.TKMD.SoHopDong;
            //    ccNgayHopDong.Text = f.TKMD.NgayHopDong.ToShortDateString();
            //    ccNgayHHHopDong.Text = f.TKMD.NgayHetHanHopDong.ToShortDateString();
            //}
        }

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.TrangThaiXuLy == 0)
            {
                Globals.ShowMessageTQDT("Tờ khai đã dược khai báo tới hải quan nên không chỉnh sửa được.Hãy hủy khai báo trước khi chỉnh sửa dữ liệu.", false);
                //showMsg("MSG_0203066");
                return;
            }
            if (TKMD.TrangThaiXuLy == 1)
            {
                //showMsg("MSG_STN10");
                Globals.ShowMessageTQDT("Tờ khai đã dược duyệt nên không thể chỉnh sửa dữ liệu !", false);
                return;
            }
            switch (NhomLoaiHinh)
            {
                case "NGC":
                    var f = new HopDongManageForm { IsBrowseForm = true };
                    if (sender != null)
                    {
                        f.isKhaiBoSung = true;
                    }
                    f.ShowDialog();
                    if (f.HopDongSelected.ID > 0)
                    {
                        Hdgc = new Company.GC.BLL.KDT.GC.HopDong();
                        if (Hdgc.ID != f.HopDongSelected.ID)
                        {
                            if (TKMD.HMDCollection.Count > 0)
                            {
                                //if (showMsg("MSG_SAV05", true) == "Yes")
                                //if (ShowMessage("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                if (Globals.ShowMessageTQDT("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                {
                                    foreach (Company.GC.BLL.KDT.HangMauDich HangDelete in TKMD.HMDCollection)
                                    {
                                        if (HangDelete.ID > 0)
                                            HangDelete.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                                    }
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            Hdgc = f.HopDongSelected;
                            txtSoHopDong.Text = f.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Text = f.HopDongSelected.NgayKy.ToShortDateString();
                            ccNgayHHHopDong.Text = f.HopDongSelected.NgayHetHan.ToShortDateString();
                            txtTenDonViDoiTac.Text = f.HopDongSelected.DonViDoiTac + "\r\n" + f.HopDongSelected.DiaChiDoiTac;
                            TKMD.IDHopDong = Hdgc.ID;
                        }
                    }
                    break;
                case "NVE":
                    var fv = new HopDongManageForm { IsBrowseForm = true };
                    if (sender != null)
                    {
                        fv.isKhaiBoSung = true;
                    }
                    fv.ShowDialog();
                    if (fv.HopDongSelected.ID > 0)
                    {
                        Hdgc = new Company.GC.BLL.KDT.GC.HopDong();
                        if (Hdgc.ID != fv.HopDongSelected.ID)
                        {
                            if (TKMD.HMDCollection.Count > 0)
                            {
                                //if (showMsg("MSG_SAV05", true) == "Yes")
                                //if (ShowMessage("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                if (Globals.ShowMessageTQDT("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                {
                                    foreach (Company.GC.BLL.KDT.HangMauDich HangDelete in TKMD.HMDCollection)
                                    {
                                        if (HangDelete.ID > 0)
                                            HangDelete.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                                    }
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            Hdgc = fv.HopDongSelected;
                            txtSoHopDong.Text = fv.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Text = fv.HopDongSelected.NgayKy.ToShortDateString();
                            ccNgayHHHopDong.Text = fv.HopDongSelected.NgayHetHan.ToShortDateString();
                            txtTenDonViDoiTac.Text = fv.HopDongSelected.DonViDoiTac + "\r\n" + fv.HopDongSelected.DiaChiDoiTac;
                            TKMD.IDHopDong = Hdgc.ID;
                        }
                    }
                    break;
                case "XGC":
                    var f1 = new HopDongManageForm { IsBrowseForm = true };
                    if (sender != null)
                    {
                        f1.isKhaiBoSung = true;
                    }
                    f1.ShowDialog();
                    if (!string.IsNullOrEmpty(f1.HopDongSelected.SoHopDong))
                    {
                        if (Hdgc != null && Hdgc.ID != f1.HopDongSelected.ID)
                        {
                            if (TKMD.HMDCollection.Count > 0)
                            {
                                //if (showMsg("MSG_SAV05", true) == "Yes")
                                if (Globals.ShowMessageTQDT("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                {
                                    foreach (Company.GC.BLL.KDT.HangMauDich HangDelete in TKMD.HMDCollection)
                                    {
                                        if (HangDelete.ID > 0)
                                            HangDelete.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                                    }
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            Hdgc = f1.HopDongSelected;
                            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Text = f1.HopDongSelected.NgayKy.ToShortDateString();
                            ccNgayHHHopDong.Text = f1.HopDongSelected.NgayHetHan.ToShortDateString();
                            TKMD.IDHopDong = Hdgc.ID;
                            txtTenDonViDoiTac.Text = f1.HopDongSelected.DonViDoiTac + "\r\n" + f1.HopDongSelected.DiaChiDoiTac;
                        }
                        else
                        {
                            Hdgc = f1.HopDongSelected;
                            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Text = f1.HopDongSelected.NgayKy.ToShortDateString();
                            ccNgayHHHopDong.Text = f1.HopDongSelected.NgayHetHan.ToShortDateString();
                            TKMD.IDHopDong = Hdgc.ID;
                            txtTenDonViDoiTac.Text = f1.HopDongSelected.DonViDoiTac + "\r\n" + f1.HopDongSelected.DiaChiDoiTac;
                        }
                    }
                    break;
            }
        }

        private void DgListRowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            int pos = items[0].Position;
            Company.GC.BLL.KDT.HangMauDich hangmaudich = this.TKMD.HMDCollection[pos];

            HangMauDichForm frmHangMD = new HangMauDichForm(hangmaudich);
            frmHangMD.TKMD = this.TKMD;
            frmHangMD.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            frmHangMD.NhomLoaiHinh = this.NhomLoaiHinh;
            frmHangMD.MaHaiQuan = this.TKMD.MaHaiQuan;
            frmHangMD.MaNguyenTe = ctrNguyenTe.Ma;
            frmHangMD.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            frmHangMD.ShowDialog();
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            txtSoLuongPLTK.Text = TKMD.SoLuongPLTK.ToString();
        }

        private void DgListLoadingRow(object sender, RowLoadEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Row.Cells["NuocXX_ID"].Text.Trim()))
                e.Row.Cells["NuocXX_ID"].Text = Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }

        private void GridEx1RowDoubleClick(object sender, RowActionEventArgs e)
        {
            var f = new ChungTuForm
                        {
                            OpenType =
                                TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET ? OpenFormType.View : OpenFormType.Edit
                        };
            var ctDetail = new ChungTu
                               {
                                   ID = Convert.ToInt64(e.Row.Cells["ID"].Text),
                                   TenChungTu = e.Row.Cells["TenChungTu"].Text,
                                   SoBanChinh = Convert.ToInt16(e.Row.Cells["SoBanChinh"].Text),
                                   SoBanSao = Convert.ToInt16(e.Row.Cells["SoBanSao"].Text),
                                   STTHang = Convert.ToInt16(e.Row.Cells["STTHang"].Text),
                                   Master_ID = Convert.ToInt64(e.Row.Cells["Master_ID"].Text),
                                   FileUpLoad = e.Row.Cells["FileUpLoad"].Text
                               };
            f.ctDetail = ctDetail;
            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                if (ct.TenChungTu.ToUpper() == ctDetail.TenChungTu.ToUpper())
                {
                    TKMD.ChungTuTKCollection.RemoveAt(i);
                    break;
                }
                ++i;
            }
            f.MaLoaiHinh = TKMD.MaLoaiHinh;
            f.collection = TKMD.ChungTuTKCollection;
            f.ShowDialog();
            TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            SetCommandStatus();
        }

        private void GridEx1DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                Globals.ShowMessageTQDT("Tờ khai đã được duyệt không được sửa", false);
                //showMsg("MSG_2702027");
                e.Cancel = true;
                return;
            }
            var ctDetail = new ChungTu { ID = Convert.ToInt64(e.Row.Cells["ID"].Text) };
            if (Globals.ShowMessageTQDT("Bạn có muốn xóa các thông tin đã chọn không ?", true) == "Yes")
            //if (showMsg("MSG_DEL01", true) == "Yes")
            {
                if (ctDetail.ID > 0)
                    ctDetail.Delete();
                TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
                e.Cancel = true;
        }
        public bool checkExistHMD(long _TKMD_ID, string _mahang)
        {
            string where = "1 = 1";
            where += string.Format(" AND TKMD_ID = {0} AND MaPhu = '{1}'", _TKMD_ID, _mahang);
            List<Company.GC.BLL.KDT.HangMauDich> hmdColl = (List<Company.GC.BLL.KDT.HangMauDich>)Company.GC.BLL.KDT.HangMauDich.SelectCollectionDynamic(where, "");
            if (hmdColl.Count > 0)
            {
                return true;
            }
            return false;
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKMD.ID > 0)
            {
                if (TKMD.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                    {
                        //showMsg("MSG_ALL01");
                        Globals.ShowMessageTQDT("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        e.Cancel = true;
                        return;
                    }
                }


            }
            if (Globals.ShowMessageTQDT("Bạn có muốn xóa các thông tin đã chọn không?", true) == "Yes")
            //if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        var hmd = (Company.GC.BLL.KDT.HangMauDich)i.GetRow().DataRow;

                        //DATLMQ bổ sung Lưu tạm giá trị Hàng mậu dịch vào DeleteHMDCollection ngày 11/08/2011
                        if (checkExistHMD(TKMD.ID, hmd.MaPhu))
                        {
                            ToKhaiMauDichForm.deleteHMDCollection.Add(hmd);
                            isDeleted = true;
                        }

                        if (hmd.ID > 0)
                        {
                            hmd.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
            SetCommandStatus();
        }

        private void editBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            bool isDelete = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO
                            || TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY
                            || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET;
            if (!isDelete) return;
            try
            {
                foreach (Company.GC.BLL.KDT.HangMauDich HMD in TKMD.HMDCollection)
                {
                    HMD.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, Hdgc.ID);
                }
            }
            catch { }
            TKMD.HMDCollection.Clear();
            dgList.DataSource = TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtChungTu_ButtonClick(object sender, EventArgs e)
        {
            GiayToForm f = new GiayToForm();
            f.collection = TKMD.ChungTuTKCollection;

            //Ngonnt 22/02
            f.countHoaDon = TKMD.HoaDonThuongMaiCollection.Count;
            f.countHopDong = TKMD.HopDongThuongMaiCollection.Count;
            f.countVanTai = (TKMD.VanTaiDon != null) ? 1 : 0;
            f.countGiayPhep = TKMD.GiayPhepCollection.Count;
            f.countCO = TKMD.COCollection.Count;
            f.countChuyenCK = TKMD.listChuyenCuaKhau.Count;
            f.countGKT = TKMD.GiayKiemTraCollection.Count;
            //f.countCTNo = TKMD.Ch.Count;
            f.countCTGD = (TKMD.ChungThuGD != null && TKMD.ChungThuGD.ID > 0) ? 1 : 0;
            //Ngonnt 22/02

            if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();
            TKMD.ChungTuTKCollection = f.collection;
            try
            {
                gridEX1.DataSource = TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }
            txtChungTu.Text = f.GiayTo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ToKhaiMauDichForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool ok = false;
            //if (TKMD.MaLoaiHinh.StartsWith("N"))
            //{
            //    //if (ToKhaiMauDichForm.isTKNhap)
            //    //    ok = true;
            //}
            //else
            //{
            //    //if (ToKhaiMauDichForm.isTKXuat)
            //    //    ok = true;
            //}
            ok = false;
            try
            {
                if (ok)
                {
                    if (Globals.ShowMessageTQDT("Dữ liệu có sự thay đổi. Bạn có muốn cập nhật lại không?", true) == "Yes")
                    //if (showMsg("MSG_0203067", true) == "Yes")
                    {
                        TKMD.SoTiepNhan = TKMD.SoTiepNhan;
                        TKMD.MaHaiQuan = Hdgc.MaHaiQuan;
                        TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                        TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                        TKMD.SoHang = (short)TKMD.HMDCollection.Count;
                        this.TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);

                        // Doanh nghiệp.
                        TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                        // Đơn vị đối tác.
                        TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                        TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                        TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                        // Đại lý TTHQ.
                        TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                        TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                        // Loại hình mậu dịch.
                        TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                        // Giấy phép.
                        if (txtSoGiayPhep.Text.Trim().Length > 0)
                        {
                            TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                            TKMD.NgayGiayPhep = !ccNgayGiayPhep.IsNullDate ? ccNgayGiayPhep.Value : DateTime.Parse("01/01/1900");
                            TKMD.NgayHetHanGiayPhep = !ccNgayHHGiayPhep.IsNullDate ? ccNgayHHGiayPhep.Value : DateTime.Parse("01/01/1900");
                        }
                        else
                        {
                            TKMD.SoGiayPhep = "";
                            TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                            TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                        }

                        // 7. Hợp đồng.
                        if (txtSoHopDong.Text.Trim().Length > 0)
                        {
                            TKMD.SoHopDong = txtSoHopDong.Text;
                            // Ngày HD.
                            TKMD.NgayHopDong = ccNgayHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHopDong.Value;
                            // Ngày hết hạn HD.
                            TKMD.NgayHetHanHopDong = ccNgayHHHopDong.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHHHopDong.Value;
                        }
                        else
                        {
                            TKMD.SoHopDong = "";
                            TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                            TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                        }

                        // Hóa đơn thương mại.
                        if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                        {
                            TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                            TKMD.NgayHoaDonThuongMai = ccNgayHDTM.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayHDTM.Value;
                        }
                        else
                        {
                            TKMD.SoHoaDonThuongMai = "";
                            TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                        }
                        // Phương tiện vận tải.
                        TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                        TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                        TKMD.NgayDenPTVT = ccNgayDen.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayDen.Value;

                        // Vận tải đơn.
                        if (txtSoVanTaiDon.Text.Trim().Length > 0)
                        {
                            TKMD.SoVanDon = txtSoVanTaiDon.Text;
                            TKMD.NgayVanDon = ccNgayVanTaiDon.IsNullDate ? DateTime.Parse("01/01/1900") : ccNgayVanTaiDon.Value;
                        }
                        else
                        {
                            TKMD.SoVanDon = "";
                            TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                        }
                        // Nước.
                        if (NhomLoaiHinh.Substring(0, 1) == "N")
                        {
                            TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                            TKMD.NuocNK_ID = "VN".PadRight(3);
                        }
                        else
                        {
                            TKMD.NuocXK_ID = "VN".PadRight(3);
                            TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                        }

                        // Địa điểm xếp hàng.
                        //if (chkDiaDiemXepHang.Checked)
                        TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                        // else
                        //     TKMD.DiaDiemXepHang = "";

                        // Địa điểm dỡ hàng.
                        TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                        // Điều kiện giao hàng.
                        TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                        // Đồng tiền thanh toán.
                        TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                        TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                        // Phương thức thanh toán.
                        TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                        //
                        TKMD.TenChuHang = txtTenChuHang.Text;
                        TKMD.ChucVu = txtChucVu.Text;
                        TKMD.SoContainer20 = Convert.ToDecimal(txtTongSoContainer.Text);
                        //TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                        // Tổng trị giá khai báo (Chưa tính).

                        TKMD.TongTriGiaKhaiBao = 0;
                        TKMD.TongTriGiaTinhThue = 0;
                        foreach (Company.GC.BLL.KDT.HangMauDich hang in TKMD.HMDCollection)
                        {
                            TKMD.TongTriGiaKhaiBao += hang.TriGiaKB;
                            TKMD.TongTriGiaTinhThue += hang.TriGiaTT;
                        }
                        TKMD.TongTriGiaKhaiBao = 0;
                        //
                        TKMD.TrangThaiXuLy = -1;
                        TKMD.LoaiToKhaiGiaCong = "NPL";
                        //
                        TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                        TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                        // tkmd.NgayGui = DateTime.Parse("01/01/1900");
                        if (radNPL.Checked) TKMD.LoaiHangHoa = "N";
                        if (radSP.Checked) TKMD.LoaiHangHoa = "S";
                        if (radTB.Checked) TKMD.LoaiHangHoa = "T";

                        TKMD.GiayTo = txtChungTu.Text;

                        TKMD.MaMid = txtMaMid.Text.Trim();
                        TKMD.InsertUpdateFull();
                    }
                }
                //if (TKMD.MaLoaiHinh.StartsWith("N"))
                //    isTKNhap = false;
                //else
                //    isTKXuat = false;
            }
            catch (Exception ex)
            {
                //if (showMsg("MSG_2702028", ex.Message, true) != "Yes")
                if (Globals.ShowMessageTQDT("Dữ liệu cập nhật có lỗi :" + ex.Message + ". Bạn có muốn thoát không?", true) != "Yes")
                    e.Cancel = true;
                else
                {
                    //if (TKMD.MaLoaiHinh.StartsWith("N"))
                    //    isTKNhap = false;
                    //else
                    //    isTKXuat = false;
                }
            }
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtSoVanTaiDon_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.MaLoaiHinh.Substring(0, 1) == "N" && TKMD.ID == 0)
            {
                ShowMessage("Bạn vui lòng nhập thông tin sau trên tờ khai chính, và lưu tờ khai:\r\n- Số vận đơn\n- Ngày vận đơn.\r\n\nLưu ý: Thông tin 'Vận tải đơn chi tiết' chỉ cho phép nhập khi đã Lưu thông tin tờ khai thành công.", false);
                return;
            }

            VanTaiDonForm f = new VanTaiDonForm();
            f.TKMD = TKMD;
            f.IDPhuongThucVanTai = cbPTVT.SelectedValue.ToString();
            f.NhomLoaiHinh = this.NhomLoaiHinh;

            //Start Ngonnt 25/01
            TKMD.SoVanDon = txtSoVanTaiDon.Text;
            TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
            TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
            TKMD.NgayDenPTVT = ccNgayDen.Value;
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            TKMD.CuaKhau_ID = ctrCuaKhau.Ma;
            TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
            //End

            f.ShowDialog();
            if (f.TKMD.VanTaiDon != null && f.TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
            {
                if (TKMD.VanTaiDon.LoaiVanDon == LoaiVanDon.DUONG_KHONG)
                {
                    txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
                }
                if (TKMD.NgayDenPTVT != null && TKMD.NgayDenPTVT.Year > 1900)
                    ccNgayDen.Text = TKMD.NgayDenPTVT.ToShortDateString();
                else
                    ccNgayDen.Text = "";

                // ĐKGH.
                cbDKGH.SelectedValue = TKMD.DKGH_ID;

                // Địa điểm dỡ hàng.
                ctrCuaKhau.Ma = TKMD.CuaKhau_ID;

                // Vận tải đơn.
                txtSoVanTaiDon.Text = TKMD.SoVanDon;
                ccNgayVanTaiDon.Text = TKMD.NgayVanDon.Year > 1900 ? TKMD.NgayVanDon.ToShortDateString() : "";
                // if ( TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

                // Địa điểm xếp hàng.
                txtDiaDiemXepHang.Text = TKMD.DiaDiemXepHang;
                //if ( TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

                //                 // Container 20.

                //                 txtTongSoContainer.Value = TKMD.SoContainer20;

                // Container 40.
                //txtSoContainer40.Value = TKMD.SoContainer40;
                TKMD.SoLuongContainer.LoadSoContFromVanDon(TKMD.VanTaiDon);
            }
            else
            {
                ccNgayVanTaiDon.Text = string.Empty;
                txtSoVanTaiDon.Text = string.Empty;
            }
            // txtTongSoContainer.Value = (TKMD.VanTaiDon != null && TKMD.VanTaiDon.ContainerCollection != null) ? TKMD.VanTaiDon.ContainerCollection.Count : 0;
            txtTongSoContainer.Value = TKMD.SoLuongContainer.TongSoCont();
        }

        private void txtSoGiayPhep_ButtonClick(object sender, EventArgs e)
        {

            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDForm f = new ListGiayPhepTKMDForm();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }

            TKMD.SoGiayPhep = txtSoGiayPhep.Text;
            TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
            TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;

            f.TKMD = TKMD;
            f.ShowDialog();

            if (f.TKMD.SoGiayPhep != "")
            {
                txtSoGiayPhep.Text = f.TKMD.SoGiayPhep;
                ccNgayGiayPhep.Text = f.TKMD.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = f.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            }

        }

        private void txtSoHoaDonThuongMai_ButtonClick(object sender, EventArgs e)
        {
            if (TKMD.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonThuongMaiTKMDForm f = new ListHoaDonThuongMaiTKMDForm();
            if (sender != null)
                f.isKhaiBoSung = true;
            f.TKMD = TKMD;

            //Start Ngonnt 25/01
            TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
            TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
            TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();
            TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            //End

            f.ShowDialog();

            if (f.TKMD.SoHoaDonThuongMai != "")
            {
                txtSoHoaDonThuongMai.Text = f.TKMD.SoHoaDonThuongMai;
                ccNgayHDTM.Text = f.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            }
        }

        private void txtTenDonViDoiTac_Leave(object sender, EventArgs e)
        {
            if (!ValidateNguoiNK())
                return;
        }

        private void btnNoiDungChinhSua_Click(object sender, EventArgs e)
        {
            NoiDungChinhSuaTKForm noidungchinhsuatk = new NoiDungChinhSuaTKForm();
            noidungchinhsuatk.TKMD = this.TKMD;
            noidungchinhsuatk.noiDungDCTK.TrangThai = TKMD.TrangThaiXuLy;
            noidungchinhsuatk.ShowDialog();
        }
        private void dgToKhaiTriGia_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (dgToKhaiTriGia.GetRows().Length < 1) return;
            if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgToKhaiTriGia.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiTriGia tktg = (ToKhaiTriGia)i.GetRow().DataRow;
                        if (tktg.ID > 0)
                        {
                            tktg.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void dgToKhaiTriGia_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ToKhaiTriGia TKTG = (ToKhaiTriGia)e.Row.DataRow;
            ToKhaiTriGiaForm f = new ToKhaiTriGiaForm();
            f.TKTG = TKTG;
            f.TKMD = this.TKMD;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog();
            try
            {
                dgToKhaiTriGia.Refetch();
            }
            catch
            {
                dgToKhaiTriGia.Refresh();
            }
        }

        private void dglistTKTK23_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["MaToKhaiTriGia"].Value.ToString() == "1")
                e.Row.Cells["MaToKhaiTriGia"].Text = setText("Tờ khai trị giá PP3", "Declaration under 3nd method");
            else
                e.Row.Cells["MaToKhaiTriGia"].Text = setText("Tờ khai trị giá PP2", "Declaration under 2nd method");
        }

        private void dglistTKTK23_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HangTKTGPP23Form f = new HangTKTGPP23Form();
            f.TKTG23 = (ToKhaiTriGiaPP23)e.Row.DataRow;
            f.TKMD = this.TKMD;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Insert;
            else
                f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            f.ShowDialog();
            try
            {
                dglistTKTK23.Refetch();
            }
            catch
            {
                dglistTKTK23.Refresh();
            }
        }

        private void dglistTKTK23_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (dglistTKTK23.GetRows().Length < 1) return;
            if (MLMessages("Bạn có muốn xóa thông tin đã chọn không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dglistTKTK23.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiTriGiaPP23 tktg = (ToKhaiTriGiaPP23)i.GetRow().DataRow;
                        if (tktg.ID > 0)
                        {
                            tktg.Delete();
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }
        private void chkAnHangThue_CheckedChanged(object sender, EventArgs e)
        {
            txtAnHanThue_ThoiGian.Enabled = txtAnHanThue_LyDo.Enabled = chkAnHangThue.Checked;
        }

        private void chkDamBaoNopThue_isValue_CheckedChanged(object sender, EventArgs e)
        {
            txtDamBaoNopThue_HinhThuc.Enabled = txtDamBaoNopThue_TriGia.Enabled = ccDamBaoNopThue_NgayBatDau.Enabled = ccDamBaoNopThue_NgayKetThuc.Enabled = chkDamBaoNopThue_isValue.Checked;
        }

        private void chkGiamThue_isValue_CheckedChanged(object sender, EventArgs e)
        {
            // txtGiamThue_SoVanBan.Enabled = txtGiamThue_ThueSuatGoc.Enabled = txtGiamThue_TyLeGiam.Enabled = chkGiamThue_isValue.Checked;
        }
        private void txtLePhiHQ_Click(object sender, EventArgs e)
        {
            LePhiHQ f = new LePhiHQ();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            this.TKMD.LePhiHQCollection = f.TKMD.LePhiHQCollection;
            decimal tongLePhi = 0;
            if (TKMD.LePhiHQCollection != null && TKMD.LePhiHQCollection.Count > 0)
            {
                foreach (Company.KDT.SHARE.QuanLyChungTu.LePhiHQ lephi in TKMD.LePhiHQCollection)
                {
                    tongLePhi += lephi.SoTienLePhi;
                }
            }
            txtLePhiHQ.Value = tongLePhi;
        }

        #region Send V3 Create by LANNT
        private void FeedBackV3()
        {
            FeedBackV3(string.Empty);
        }
        private void FeedBackV3(string DieuKienPhanHoi)
        {
            try
            {

                bool isFeedBack = true;
                int count = Company.KDT.SHARE.Components.Globals.CountSend;
                while (isFeedBack)
                {
                    ObjectSend msgSend;
                    if (string.IsNullOrEmpty(DieuKienPhanHoi))
                        msgSend = SingleMessage.FeedBackMessageV3(TKMD);
                    else
                        msgSend = SingleMessage.FeedBack(TKMD, TKMD.GUIDSTR, DieuKienPhanHoi);

                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    if (isFeedBack)
                    {
                        if (
                            feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                            feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                            )
                        {
                            if (string.IsNullOrEmpty(DieuKienPhanHoi))
                                isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                            else
                            {
                                isFeedBack = false;
                                ShowMessageTQDT(msgInfor, false);
                            }
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                        {
                            if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                                feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                            {
                                isFeedBack = false;
                                ShowMessageTQDT(msgInfor, false);
                            }
                            else
                            {
                                isFeedBack = true;
                                ShowMessageTQDT(msgInfor, false);
                            }
                            count--;

                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                            feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(DieuKienPhanHoi))
                                isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                            else
                            {
                                isFeedBack = false;
                                ShowMessageTQDT(msgInfor, false);
                            }
                        }
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetCommandStatus();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void CancelV3()
        {

            ObjectSend msgSend = SingleMessage.CancelMessageV3(TKMD);
            SendMessageForm dlgSendForm = new SendMessageForm();

            dlgSendForm.Send += SendMessage;
            bool isSend = dlgSendForm.DoSend(msgSend);

            if (isSend)
            {
                FeedBackV3();
                dlgSendForm.Message.XmlSaveMessage(TKMD.ID, MessageTitle.HuyKhaiBaoToKhai);
            }

        }
        private void SendV3()
        {
            try
            {
                //string s = Helpers.Serializer(TKMD);
                //ToKhaiMauDich tktmp = Helpers.Deserialize<ToKhaiMauDich>(s);
                if (TKMD.ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    TKMD.Load();
                    TKMD.LoadChungTuHaiQuan();
                }
                //if (GlobalSettings.IsDaiLy && !GlobalSettings.ISKHAIBAO && TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                //    this.ShowMessage("Đại lý chưa đồng bộ tờ khai cho doanh nghiệp. Số tờ khai tối đa đại lý được khai là: " + GlobalSettings.SOTOKHAI_DONGBO.ToString(), false);
                //    return;
                //}

                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                sendXML.master_id = TKMD.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }

                if (TKMD.SoToKhai != 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET && !Company.KDT.SHARE.Components.Globals.IsKhaiTK)
                {

                    string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                    ShowMessage(msg, false);
                    return;

                }
                else
                {
                    #region  Kiêm tra trùng tờ khai
                    try
                    {
                        decimal _soLuongHang = 0;
                        foreach (HangMauDich item in TKMD.HMDCollection)
                        {
                            _soLuongHang = _soLuongHang + item.SoLuong;
                        }
                        long soToKhai = TrungToKhai.CheckSoToKhaiTrung(TKMD.SoToKhai, TKMD.MaHaiQuan, TKMD.MaLoaiHinh, TKMD.SoGiayPhep, TKMD.SoVanDon, TKMD.SoHoaDonThuongMai, TKMD.SoHopDong, TKMD.SoKien, TKMD.TrongLuong, _soLuongHang, TKMD.HMDCollection.Count);
                        if (soToKhai > 0)
                        {
                            string notice = string.Format("Tờ khai này đang trùng với tờ khai {0}/{1}/{2}", soToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan);
                            string warning = string.Format("\r\nChú ý: Người dùng cố tình khai báo lặp lại tờ khai sẽ phải chịu xử phạt hành chính theo quy định hiện hành của HQ");
                            if (ShowMessageTQDT(notice + warning + "\r\nBạn có muốn tiếp tục khai báo ?", true) != "Yes")
                                return;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    #endregion

                    ObjectSend msgSend = SingleMessage.SendMessageV3(TKMD);

                    //TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;

                    string msgsend = Helpers.Serializer(msgSend.Content, false, false);

                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;

                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                    sendXML.master_id = TKMD.ID;
                    sendXML.func = 1;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.InsertUpdate();
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    bool isSend = dlgSendForm.DoSend(msgSend);
                    //Lưu ý phải để dòng code lưu message sau kết quả trả về.
                    dlgSendForm.Message.XmlSaveMessage(TKMD.ID, TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? MessageTitle.KhaiBaoToKhai : MessageTitle.KhaiBaoSuaTK);

                    //TODO: Hungtq updated 6/2/2013. SendV3()()
                    //Luu version, co su dung CKS tren to khai khi da gui duoc to khai den HQ.
                    if (isSend)
                        SaveVersion();

                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        //dlgSendForm.Message.XmlSaveMessage(TKMD.ID, MessageTitle.KhaiBaoToKhai);
                        //sendXML = new MsgSend();
                        //sendXML.LoaiHS = LoaiKhaiBao.ToKhai;
                        //sendXML.master_id = TKMD.ID;
                        //sendXML.func = 1;
                        //sendXML.InsertUpdate();
                        TKMD.Update();
                        FeedBackV3();

                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        ShowMessageTQDT(msgInfor, false);
                    }


                }



            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);

            this.Invoke(new MethodInvoker(
                delegate
                {
                    if (this.TKMD.SoTiepNhan > 0)
                        txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    if (this.TKMD.SoToKhai > 0)
                        txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();
                    SetCommandStatus();
                }));
        }
        /// <summary>
        /// Xử lý message tờ khai trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ToKhaiSendHandler(TKMD, ref msgInfor, sender, e);

        }

        #endregion

        private void txtTongSoContainer_Click(object sender, EventArgs e)
        {
            ListContainerTKMDForm f = new ListContainerTKMDForm();
            f.TKMD = this.TKMD;
            f.ShowDialog(this);
            txtTongSoContainer.Value = TKMD.SoLuongContainer.TongSoCont();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox1.Visible = !pictureBox1.Visible;
        }

        private void cmbToolBar_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }


    }
}
