using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;

namespace Company.Interface.Report.WareHouse
{
    public partial class ReportViewBCXNT_KhoKeToan : DevExpress.XtraReports.UI.XtraReport
    {
        public DataTable dt;
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public decimal TongNhap = 0;
        public decimal TongXuat = 0;
        public decimal TongTon = 0;
        public ReportViewBCXNT_KhoKeToan()
        {
            InitializeComponent();
        }
        public void BindReport()
       {
           try
           {
               if (dt.Rows.Count == 0)
                   return;
               lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
               lblDiaChi.Text = GlobalSettings.DIA_CHI;
               lblTuNgay.Text = DateTime.Now.ToString("dd/MM/yyyy");
               if (NPL.MANPL != String.Empty)
               {
                   lblTenHangHoa.Text = NPL.TENNPL;
                   lblMaHangHoa.Text = NPL.MANPL;
                   lblDVT.Text = NPL.DVT;
               }
               else
               {
                   lblTenHangHoa.Text = SP.TENSP;
                   lblMaHangHoa.Text = SP.MASP;
                   lblDVT.Text = SP.DVT;
               }
               dt.TableName = "CartReport";
               this.DataSource = dt;
               lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
               lblNgayThang.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NGAYCT", "{0:dd/MM/yyyy}");
               lblCTNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SOCTNHAP");
               lblCTXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SOCTXUAT");
               lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LUONGNHAP", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
               lblLuongXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LUONGXUAT", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
               lblLuongTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LUONGTON", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
               lblDienGiai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".GHICHU");
           }
           catch (Exception ex)
           {
                Logger.LocalLogger.Instance().WriteMessage(ex);
           }
        }
        private void lblLuongNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                TongNhap += Convert.ToDecimal(lblLuongNhap.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblLuongXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                TongXuat += Convert.ToDecimal(lblLuongXuat.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblLuongTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //TongTon += Convert.ToDecimal(lblLuongTon.Text);
        }

        private void lblTongNhap_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblTongNhap.Text = TongNhap.ToString("N5");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblTongXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblTongXuat.Text = TongXuat.ToString("N5");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lblTongTonCuoi_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            try
            {
                lblTongTonCuoi.Text = (TongNhap - TongXuat).ToString("N5");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

    }
}
