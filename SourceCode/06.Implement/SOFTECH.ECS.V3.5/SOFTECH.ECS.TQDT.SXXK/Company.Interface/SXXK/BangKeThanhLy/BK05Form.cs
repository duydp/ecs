using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK05Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLTaiXuatCollection bkCollection = new BKNPLTaiXuatCollection();
        private string MaLoaiHinhNhap;
        private string MaLoaiHinhXuat;
        private string MaHaiQuanNhap;
        private string MaHaiQuanXuat;
        private string ToKhaiXuat;
        private string ToKhaiNhap;
        private string DVT_ID;
        private DateTime NgayDangKy;
        private DateTime NgayDangKyXuat;
        public BK05Form()
        {
            InitializeComponent();
        }

        private void BK05Form_Load(object sender, EventArgs e)
        {
            gridEX2.Tables[0].Columns["LuongTaiXuat"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                cmdAdd.Enabled = cmdSave.Enabled = false;
                gridEX2.AllowDelete = InheritableBoolean.False;
                gridEX2.AllowEdit = InheritableBoolean.False;
            }
            gridEX2.DataSource = this.bkCollection;
        }

        private void txtToKhaiXuat_ButtonClick(object sender, EventArgs e)
        {
            txtToKhaiNhap.Text = "";
            txtLoaiHinhNhap.Text = "";
            txtNamDKNhap.Text = "";

            int id = this.HSTL.getBKToKhaiXuat();
            DataSet ds = new BKToKhaiXuat().getBKToKhaiXuatNPL(this.HSTL.BKCollection[id].ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DSToKhaiXuatNPLForm f = new DSToKhaiXuatNPLForm();
                f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
                f.ds = ds;
                f.ShowDialog(this);
                if (f.SoToKhai == 0) return;
                txtToKhaiXuat.Text = Convert.ToString(f.SoToKhaiVNACCS);
                ToKhaiXuat = Convert.ToString(f.SoToKhai);
                txtLoaiHinhXuat.Text = LoaiHinhMauDich_GetTenVT((object)f.MaLoaiHinh);
                //txtNamDKXuat.Text = Convert.ToString(f.NgayDangKy.Year);
                txtNamDKXuat.Text = f.NgayDangKy.ToString("dd/MM/yyyy");
                txtMaNPL.Text = f.MaNPL;
                txtTenNPL.Text = f.TenNPL;
                this.MaHaiQuanXuat = f.MaHaiQuan;
                this.MaLoaiHinhXuat = f.MaLoaiHinh;
                this.NgayDangKyXuat = f.NgayDangKy;
                this.DVT_ID = f.DVT_ID;
            }
            else
            {
                //ShowMessage("Không có tờ khai xuất NPL nào trong bảng kê danh sách tờ khai xuất!", false);
                MLMessages("Không có tờ khai xuất NPL nào trong bảng kê danh sách tờ khai xuất!", "MSG_THK79", "", false);
            }
        }

        private void txtToKhaiNhap_ButtonClick(object sender, EventArgs e)
        {
            if (txtToKhaiXuat.Text == "")
            {
                MLMessages("Bạn hãy nhập tờ khai xuất trước.", "MSG_THK80", "", false);
                return;
            }

            int id = this.HSTL.getBKToKhaiNhap();
            DataSet ds = new BKToKhaiNhap().getBKToKhaiNhapChuaNPL(this.HSTL.BKCollection[id].ID, txtMaNPL.Text);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DSToKhaiNhapChuaNPLForm f = new DSToKhaiNhapChuaNPLForm();
                f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
                f.ds = ds;
                f.ShowDialog(this);
                if (f.SoToKhai == 0) return;
                txtToKhaiNhap.Text = Convert.ToString(f.SoToKhaiVNACCS);
                ToKhaiNhap = Convert.ToString(f.SoToKhai);
                txtLoaiHinhNhap.Text = LoaiHinhMauDich_GetTenVT((object)f.MaLoaiHinh);
                //txtNamDKNhap.Text = Convert.ToString(f.NgayDangKy.Year);
                txtNamDKNhap.Text = f.NgayDangKy.ToString("dd/MM/yyyy");
                this.MaLoaiHinhNhap = f.MaLoaiHinh;
                this.MaHaiQuanNhap = f.MaHaiQuan;
                this.NgayDangKy = f.NgayDangKy;
            }
            else
            {
                //ShowMessage("Không có tờ khai nhập chứa NPL '" + txtMaNPL.Text +"' trong danh sách bảng kê tờ khai nhập!", false);
                MLMessages("Không có tờ khai nhập chứa NPL '" + txtMaNPL.Text + "' trong danh sách bảng kê tờ khai nhập!", "MSG_THK79", "", false);
            }
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            //if(CheckExit())
            BKNPLTaiXuat bk = new BKNPLTaiXuat();
            bk.SoToKhai = Convert.ToInt32(ToKhaiNhap);
            bk.MaLoaiHinh = this.MaLoaiHinhNhap;
            bk.NamDangKy = (Int16)Convert.ToDateTime(txtNamDKNhap.Text).Year;
            bk.MaHaiQuan = this.MaHaiQuanNhap;
            bk.NgayDangKy = this.NgayDangKy;
            bk.NgayDangKyXuat = this.NgayDangKyXuat;
            bk.SoToKhaiXuat = Convert.ToInt32(ToKhaiXuat);
            bk.MaLoaiHinhXuat = this.MaLoaiHinhXuat;
            bk.NamDangKyXuat = (Int16)Convert.ToDateTime(txtNamDKXuat.Text).Year;
            bk.MaHaiQuanXuat = this.MaHaiQuanXuat;
            bk.MaNPL = txtMaNPL.Text;
            bk.TenNPL = txtTenNPL.Text;
            bk.DVT_ID = this.DVT_ID;
            bk.TenDVT = DonViTinh_GetName(this.DVT_ID);
            bk.LuongTaiXuat = Convert.ToDecimal(txtSoLuongXuat.Value);
            if (donViTinhQuyDoiControl1.IsEnable)
                bk.DVT_QuyDoi = donViTinhQuyDoiControl1.DVT_QuyDoi;
            else
                bk.DVT_QuyDoi = null;
            this.bkCollection.Add(bk);
            gridEX2.Refetch();
            this.ClearData();
            donViTinhQuyDoiControl1.Reset();
        }
        private void ClearData()
        {
            txtToKhaiNhap.Text = "";
            txtLoaiHinhNhap.Text = "";
            txtNamDKNhap.Text = "";
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtToKhaiXuat.Text = "";
            txtLoaiHinhXuat.Text = "";
            txtNamDKXuat.Text = "";
            txtSoLuongXuat.Value = 0;
        }
        private void gridEX2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (e.Row.Cells["MaLoaiHinh"].Value.ToString().Contains("V"))
                {
                    decimal soToKhaiVNACCS= Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(e.Row.Cells["SoToKhai"].Value));
                    e.Row.Cells["SoToKhai"].Text = soToKhaiVNACCS.ToString() + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                }
                else
                    e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
                if (e.Row.Cells["MaLoaiHinhXuat"].Value.ToString().Contains("V"))
                {
                    decimal soToKhaiVNACCS = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(Convert.ToInt32(e.Row.Cells["SoToKhaiXuat"].Value));
                    e.Row.Cells["SoToKhaiXuat"].Text = soToKhaiVNACCS.ToString()+ "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhXuat"].Value) + "/" + e.Row.Cells["NamDangKyXuat"].Text;
                }
                else
                    e.Row.Cells["SoToKhaiXuat"].Text = e.Row.Cells["SoToKhaiXuat"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhXuat"].Value) + "/" + e.Row.Cells["NamDangKyXuat"].Text;

            }
        }
        private bool CheckExit(int soToKhaiNhap, string maLoaiHinhNhap, short namDangKyNhap, int soToKhaiXuat, string maLoaiHinhXuat, short namDangKyXuat, string maNPL)
        {
            foreach (BKNPLTaiXuat bk in this.bkCollection)
            {
                if (bk.SoToKhai == soToKhaiNhap && bk.MaLoaiHinh == maLoaiHinhNhap && bk.NamDangKy == namDangKyNhap &&
                  bk.SoToKhaiXuat == bk.SoToKhaiXuat && bk.MaLoaiHinhXuat == maLoaiHinhXuat && bk.NamDangKyXuat == namDangKyXuat && bk.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL tái xuất.", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLTaiXuat() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLNPLTX";
                    bk.TenBangKe = "DTLNPLTX";
                    bk.bkNPLTXCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLTaiXuat()].bkNPLTXCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLTaiXuat()].InsertUpdate_BKNPLTX(this.bkCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void txtNamDKXuat_TextChanged(object sender, EventArgs e)
        {

        }

        private void gridEX2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            BKNPLTaiXuat bk = (BKNPLTaiXuat)e.Row.DataRow;
            if(bk != null)
            {

                if (bk.MaLoaiHinh.Contains("V"))
                {
                    txtToKhaiNhap.Text = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(bk.SoToKhai).ToString();
                }
                else
                    txtToKhaiNhap.Text = bk.SoToKhai.ToString("N0");

                 txtLoaiHinhNhap.Text = bk.MaLoaiHinh;
                 txtNamDKNhap.Text =  bk.NgayDangKy.ToString("dd/MM/yyyy");
                 txtNamDKXuat.Text = bk.NgayDangKyXuat.ToString("dd/MM/yyyy");
                 
                 txtMaNPL.Text = bk.MaNPL;
                 txtTenNPL.Text = bk.TenNPL ;
                 txtSoLuongXuat.Value = (decimal)bk.LuongTaiXuat;
                 if (bk.MaLoaiHinhXuat.Contains("V"))
                 {
                     txtToKhaiXuat.Text = Company.KDT.SHARE.VNACCS.CapSoToKhai.GetSoTKVNACCS(bk.SoToKhaiXuat).ToString();
                 }
                else
                     txtToKhaiXuat.Text = bk.SoToKhaiXuat.ToString("N0");
                 txtLoaiHinhXuat.Text = bk.MaLoaiHinhXuat;
                 #region Đơn vị tính quy đổi
                 if (bk.DVT_QuyDoi != null)
                 {
                     donViTinhQuyDoiControl1.DVT_QuyDoi = bk.DVT_QuyDoi;
                     donViTinhQuyDoiControl1.Master_Id = 0;
                 }
                 else if (bk.ID > 0)
                 {
                     donViTinhQuyDoiControl1.Master_Id = bk.ID;
                     donViTinhQuyDoiControl1.Type = "BK_NPLTX";
                 }
                 else
                     donViTinhQuyDoiControl1.DVT_QuyDoi = null;
                 donViTinhQuyDoiControl1.DonViTinhQuyDoiControl_Load(null, null);
                //donViTinhQuyDoiControl1.DVT_QuyDoi = bk.DVT_QuyDoi;
                 #endregion
                 ToKhaiNhap = bk.SoToKhai.ToString();
                 ToKhaiXuat = bk.SoToKhaiXuat.ToString();
                
            }
        }

        private void cmdThuTucHQTruocDo_Click(object sender, EventArgs e)
        {
            if (this.HSTL.getBKNPLTaiXuat() < 0)
                ShowMessage("Vui lòng lưu thông tin bảng kê trước khi thêm chứng từ HQ trước đó", false);
            else
            {
                ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                f.Master_ID = this.HSTL.getBKNPLTaiXuat();
                f.Type = "BK_NPLTX";
                f.ShowDialog(this);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}