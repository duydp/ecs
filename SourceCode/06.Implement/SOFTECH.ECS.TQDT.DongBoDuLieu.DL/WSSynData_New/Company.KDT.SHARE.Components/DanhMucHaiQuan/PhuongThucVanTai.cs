﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class PhuongThucVanTai
    {
        public static List<PhuongThucVanTai> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<PhuongThucVanTai> collection = new List<PhuongThucVanTai>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                PhuongThucVanTai entity = new PhuongThucVanTai();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.Ten = dataSet.Tables[0].Rows[i]["Ten"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
