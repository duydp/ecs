/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD]    Script Date: 4/4/2022 3:13:56 PM ******/
-- Drop Existing Procedures
IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD]

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD]    Script Date: 4/4/2022 3:13:56 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD]      
-- Database: ECS_TQDT_KD_VNACCS      
-- Author: Ngo Thanh Tung      
-- Time created: Thursday, January 23, 2014      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD]      
 @WhereCondition NVARCHAR(500),      
 @OrderByExpression NVARCHAR(250) = NULL      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL NVARCHAR(MAX)      
      
SET @SQL =       
'SELECT * FROM dbo.t_KDT_VNACC_ToKhaiMauDich A 
INNER JOIN dbo.t_KDT_VNACC_HangMauDich B ON B.TKMD_ID = A.ID 
LEFT OUTER JOIN dbo.t_KDT_VNACC_TK_SoVanDon C ON C.TKMD_ID = A.ID 
LEFT OUTER JOIN dbo.t_KDT_VNACC_HangMauDich_ThueThuKhac D ON D.Master_id =B.ID 
LEFT OUTER JOIN dbo.t_KDT_VNACC_TK_KhoanDieuChinh E ON E.TKMD_ID = A.ID 
LEFT OUTER JOIN dbo.t_KDT_VNACC_TK_PhanHoi_TyGia F ON F.Master_ID = A.ID  
WHERE ' + @WhereCondition
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL 

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '42.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('42.4',GETDATE(), N'CẬP NHẬT PROC: p_KDT_VNACC_ToKhaiMauDich_SelectCollectionAndHMD')
END
