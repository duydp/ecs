using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace Company.Interface.Report.GC.ThongTu38
{
    public partial class ViewNPLXuat : DevExpress.XtraReports.UI.XtraReport
    {
        public ViewNPLXuat()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dtb)
        {
            DetailReport.DataSource = dtb;
            txtToKhaiXuat.DataBindings.Add("Text", DetailReport.DataSource, "SoTKXVNACCS");
            txtToKhaiNhap.DataBindings.Add("Text", DetailReport.DataSource, "SoTKNVNACCS");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "MANPL");
            txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TENNPL");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            txtLuongXuat.DataBindings.Add("Text", DetailReport.DataSource, "LUONGXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5));
            txtTriGiaXuat.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIAXUAT", Company.KDT.SHARE.Components.Globals.FormatNumber(5));
            txtLuongNhap.DataBindings.Add("Text", DetailReport.DataSource, "LUONGNHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5));
            txtDonGia.DataBindings.Add("Text", DetailReport.DataSource, "DONGIANHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5));
            txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TRIGIANHAP", Company.KDT.SHARE.Components.Globals.FormatNumber(5));
            txtTyGia.DataBindings.Add("Text", DetailReport.DataSource, "TyGiaTinhThue", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
        }
    }
}
