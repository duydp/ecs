-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Insert]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Update]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Delete]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Load]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_SelectAll]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_SelectBy_LinhVuc]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_SelectBy_LinhVuc]

IF OBJECT_ID(N'[dbo].[p_GopY_CauHoiDoanhNghiep_DeleteBy_LinhVuc]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_DeleteBy_LinhVuc]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Insert]
	@MaDonVi nvarchar(50),
	@TenDonVi nvarchar(250),
	@LinhVuc varchar(50),
	@Phanloai varchar(50),
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@NgayGui datetime,
	@CauHoiTraLoiID bigint,
	@NgayTraLoi datetime,
	@CauHoiID bigint OUTPUT
AS

INSERT INTO [dbo].[t_GopY_CauHoiDoanhNghiep]
(
	[MaDonVi],
	[TenDonVi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[NgayGui],
	[CauHoiTraLoiID],
	[NgayTraLoi]
)
VALUES 
(
	@MaDonVi,
	@TenDonVi,
	@LinhVuc,
	@Phanloai,
	@TieuDe,
	@NoiDungCauHoi,
	@NgayGui,
	@CauHoiTraLoiID,
	@NgayTraLoi
)

SET @CauHoiID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Update]
	@CauHoiID bigint,
	@MaDonVi nvarchar(50),
	@TenDonVi nvarchar(250),
	@LinhVuc varchar(50),
	@Phanloai varchar(50),
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@NgayGui datetime,
	@CauHoiTraLoiID bigint,
	@NgayTraLoi datetime
AS

UPDATE
	[dbo].[t_GopY_CauHoiDoanhNghiep]
SET
	[MaDonVi] = @MaDonVi,
	[TenDonVi] = @TenDonVi,
	[LinhVuc] = @LinhVuc,
	[Phanloai] = @Phanloai,
	[TieuDe] = @TieuDe,
	[NoiDungCauHoi] = @NoiDungCauHoi,
	[NgayGui] = @NgayGui,
	[CauHoiTraLoiID] = @CauHoiTraLoiID,
	[NgayTraLoi] = @NgayTraLoi
WHERE
	[CauHoiID] = @CauHoiID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_InsertUpdate]
	@CauHoiID bigint,
	@MaDonVi nvarchar(50),
	@TenDonVi nvarchar(250),
	@LinhVuc varchar(50),
	@Phanloai varchar(50),
	@TieuDe nvarchar(250),
	@NoiDungCauHoi nvarchar(max),
	@NgayGui datetime,
	@CauHoiTraLoiID bigint,
	@NgayTraLoi datetime
AS
IF EXISTS(SELECT [CauHoiID] FROM [dbo].[t_GopY_CauHoiDoanhNghiep] WHERE [CauHoiID] = @CauHoiID)
	BEGIN
		UPDATE
			[dbo].[t_GopY_CauHoiDoanhNghiep] 
		SET
			[MaDonVi] = @MaDonVi,
			[TenDonVi] = @TenDonVi,
			[LinhVuc] = @LinhVuc,
			[Phanloai] = @Phanloai,
			[TieuDe] = @TieuDe,
			[NoiDungCauHoi] = @NoiDungCauHoi,
			[NgayGui] = @NgayGui,
			[CauHoiTraLoiID] = @CauHoiTraLoiID,
			[NgayTraLoi] = @NgayTraLoi
		WHERE
			[CauHoiID] = @CauHoiID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_GopY_CauHoiDoanhNghiep]
		(
			[MaDonVi],
			[TenDonVi],
			[LinhVuc],
			[Phanloai],
			[TieuDe],
			[NoiDungCauHoi],
			[NgayGui],
			[CauHoiTraLoiID],
			[NgayTraLoi]
		)
		VALUES 
		(
			@MaDonVi,
			@TenDonVi,
			@LinhVuc,
			@Phanloai,
			@TieuDe,
			@NoiDungCauHoi,
			@NgayGui,
			@CauHoiTraLoiID,
			@NgayTraLoi
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Delete]
	@CauHoiID bigint
AS

DELETE FROM 
	[dbo].[t_GopY_CauHoiDoanhNghiep]
WHERE
	[CauHoiID] = @CauHoiID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_DeleteBy_LinhVuc]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_DeleteBy_LinhVuc]
	@LinhVuc varchar(50)
AS

DELETE FROM [dbo].[t_GopY_CauHoiDoanhNghiep]
WHERE
	[LinhVuc] = @LinhVuc

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_GopY_CauHoiDoanhNghiep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_Load]
	@CauHoiID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[CauHoiID],
	[MaDonVi],
	[TenDonVi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[NgayGui],
	[CauHoiTraLoiID],
	[NgayTraLoi]
FROM
	[dbo].[t_GopY_CauHoiDoanhNghiep]
WHERE
	[CauHoiID] = @CauHoiID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_SelectBy_LinhVuc]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_SelectBy_LinhVuc]
	@LinhVuc varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[CauHoiID],
	[MaDonVi],
	[TenDonVi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[NgayGui],
	[CauHoiTraLoiID],
	[NgayTraLoi]
FROM
	[dbo].[t_GopY_CauHoiDoanhNghiep]
WHERE
	[LinhVuc] = @LinhVuc

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[CauHoiID],
	[MaDonVi],
	[TenDonVi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[NgayGui],
	[CauHoiTraLoiID],
	[NgayTraLoi]
FROM [dbo].[t_GopY_CauHoiDoanhNghiep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_GopY_CauHoiDoanhNghiep_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 25, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_GopY_CauHoiDoanhNghiep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[CauHoiID],
	[MaDonVi],
	[TenDonVi],
	[LinhVuc],
	[Phanloai],
	[TieuDe],
	[NoiDungCauHoi],
	[NgayGui],
	[CauHoiTraLoiID],
	[NgayTraLoi]
FROM
	[dbo].[t_GopY_CauHoiDoanhNghiep]	

GO

