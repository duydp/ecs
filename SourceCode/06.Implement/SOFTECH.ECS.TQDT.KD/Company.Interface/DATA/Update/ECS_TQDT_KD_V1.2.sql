/*
Run this script on:

        ecsteam.ECS_TQDT_KD_VERSION    -  This database will be modified

to synchronize it with:

        ecsteam.ECS_TQDT_KD

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 11/15/2011 6:31:17 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_KDT_AnDinhThue]'
GO
CREATE TABLE [dbo].[t_KDT_AnDinhThue]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[TKMD_ID] [bigint] NOT NULL,
[TKMD_Ref] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SoQuyetDinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NgayQuyetDinh] [datetime] NOT NULL,
[NgayHetHan] [datetime] NOT NULL,
[TaiKhoanKhoBac] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenKhoBac] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GhiChu] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_AnHanThue] on [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ADD CONSTRAINT [PK_t_KDT_AnHanThue] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Load]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_SelectAll]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
CREATE TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[IDAnDinhThue] [int] NOT NULL,
[SacThue] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TienThue] [numeric] (18, 4) NOT NULL,
[Chuong] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Loai] [int] NOT NULL,
[Khoan] [int] NOT NULL,
[Muc] [int] NOT NULL,
[TieuMuc] [int] NOT NULL,
[GhiChu] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_AnDinhThue_ChiTiet] on [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ADD CONSTRAINT [PK_t_KDT_AnDinhThue_ChiTiet] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Insert]
	@IDAnDinhThue int,
	@SacThue char(10),
	@TienThue numeric(18, 4),
	@Chuong varchar(10),
	@Loai int,
	@Khoan int,
	@Muc int,
	@TieuMuc int,
	@GhiChu nvarchar(150),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_AnDinhThue_ChiTiet]
(
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
)
VALUES 
(
	@IDAnDinhThue,
	@SacThue,
	@TienThue,
	@Chuong,
	@Loai,
	@Khoan,
	@Muc,
	@TieuMuc,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Update]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Update]
	@ID int,
	@IDAnDinhThue int,
	@SacThue char(10),
	@TienThue numeric(18, 4),
	@Chuong varchar(10),
	@Loai int,
	@Khoan int,
	@Muc int,
	@TieuMuc int,
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
SET
	[IDAnDinhThue] = @IDAnDinhThue,
	[SacThue] = @SacThue,
	[TienThue] = @TienThue,
	[Chuong] = @Chuong,
	[Loai] = @Loai,
	[Khoan] = @Khoan,
	[Muc] = @Muc,
	[TieuMuc] = @TieuMuc,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]
	@ID int,
	@IDAnDinhThue int,
	@SacThue char(10),
	@TienThue numeric(18, 4),
	@Chuong varchar(10),
	@Loai int,
	@Khoan int,
	@Muc int,
	@TieuMuc int,
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_AnDinhThue_ChiTiet] 
		SET
			[IDAnDinhThue] = @IDAnDinhThue,
			[SacThue] = @SacThue,
			[TienThue] = @TienThue,
			[Chuong] = @Chuong,
			[Loai] = @Loai,
			[Khoan] = @Khoan,
			[Muc] = @Muc,
			[TieuMuc] = @TieuMuc,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_AnDinhThue_ChiTiet]
		(
			[IDAnDinhThue],
			[SacThue],
			[TienThue],
			[Chuong],
			[Loai],
			[Khoan],
			[Muc],
			[TieuMuc],
			[GhiChu]
		)
		VALUES 
		(
			@IDAnDinhThue,
			@SacThue,
			@TienThue,
			@Chuong,
			@Loai,
			@Khoan,
			@Muc,
			@TieuMuc,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteBy_IDAnDinhThue]
	@IDAnDinhThue int
AS

DELETE FROM [dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[IDAnDinhThue] = @IDAnDinhThue

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_Load]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectBy_IDAnDinhThue]
	@IDAnDinhThue int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue_ChiTiet]
WHERE
	[IDAnDinhThue] = @IDAnDinhThue

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM
	[dbo].[t_KDT_AnDinhThue_ChiTiet]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdateBy]'
GO
   
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdate]  
-- Database: ECS_TQ_SXXK_CTTT  
-- Author: Ngo Thanh Tung  
-- Time created: Sunday, November 06, 2011  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_InsertUpdateBy]  
 @ID int,  
 @IDAnDinhThue int,  
 @SacThue char(10),  
 @TienThue numeric(18, 4),  
 @Chuong varchar(10),  
 @Loai int,  
 @Khoan int,  
 @Muc int,  
 @TieuMuc int,  
 @GhiChu nvarchar(150)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] WHERE [IDAnDinhThue] = @IDAnDinhThue AND SacThue = @SacThue)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_AnDinhThue_ChiTiet]   
  SET  
   [IDAnDinhThue] = @IDAnDinhThue,  
   [SacThue] = @SacThue,  
   [TienThue] = @TienThue,  
   [Chuong] = @Chuong,  
   [Loai] = @Loai,  
   [Khoan] = @Khoan,  
   [Muc] = @Muc,  
   [TieuMuc] = @TieuMuc,  
   [GhiChu] = @GhiChu  
  WHERE  
   [IDAnDinhThue] = @IDAnDinhThue AND SacThue = @SacThue
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_KDT_AnDinhThue_ChiTiet]  
  (  
   [IDAnDinhThue],  
   [SacThue],  
   [TienThue],  
   [Chuong],  
   [Loai],  
   [Khoan],  
   [Muc],  
   [TieuMuc],  
   [GhiChu]  
  )  
  VALUES   
  (  
   @IDAnDinhThue,  
   @SacThue,  
   @TienThue,  
   @Chuong,  
   @Loai,  
   @Khoan,  
   @Muc,  
   @TieuMuc,  
   @GhiChu  
  )    
 END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Insert]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Insert]
	@TKMD_ID bigint,
	@TKMD_Ref nvarchar(150),
	@SoQuyetDinh nvarchar(50),
	@NgayQuyetDinh datetime,
	@NgayHetHan datetime,
	@TaiKhoanKhoBac varchar(50),
	@TenKhoBac nvarchar(150),
	@GhiChu nvarchar(250),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_AnDinhThue]
(
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
)
VALUES 
(
	@TKMD_ID,
	@TKMD_Ref,
	@SoQuyetDinh,
	@NgayQuyetDinh,
	@NgayHetHan,
	@TaiKhoanKhoBac,
	@TenKhoBac,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Update]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Update]
	@ID int,
	@TKMD_ID bigint,
	@TKMD_Ref nvarchar(150),
	@SoQuyetDinh nvarchar(50),
	@NgayQuyetDinh datetime,
	@NgayHetHan datetime,
	@TaiKhoanKhoBac varchar(50),
	@TenKhoBac nvarchar(150),
	@GhiChu nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_AnDinhThue]
SET
	[TKMD_ID] = @TKMD_ID,
	[TKMD_Ref] = @TKMD_Ref,
	[SoQuyetDinh] = @SoQuyetDinh,
	[NgayQuyetDinh] = @NgayQuyetDinh,
	[NgayHetHan] = @NgayHetHan,
	[TaiKhoanKhoBac] = @TaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_InsertUpdate]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_InsertUpdate]
	@ID int,
	@TKMD_ID bigint,
	@TKMD_Ref nvarchar(150),
	@SoQuyetDinh nvarchar(50),
	@NgayQuyetDinh datetime,
	@NgayHetHan datetime,
	@TaiKhoanKhoBac varchar(50),
	@TenKhoBac nvarchar(150),
	@GhiChu nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_AnDinhThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_AnDinhThue] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[TKMD_Ref] = @TKMD_Ref,
			[SoQuyetDinh] = @SoQuyetDinh,
			[NgayQuyetDinh] = @NgayQuyetDinh,
			[NgayHetHan] = @NgayHetHan,
			[TaiKhoanKhoBac] = @TaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_AnDinhThue]
		(
			[TKMD_ID],
			[TKMD_Ref],
			[SoQuyetDinh],
			[NgayQuyetDinh],
			[NgayHetHan],
			[TaiKhoanKhoBac],
			[TenKhoBac],
			[GhiChu]
		)
		VALUES 
		(
			@TKMD_ID,
			@TKMD_Ref,
			@SoQuyetDinh,
			@NgayQuyetDinh,
			@NgayHetHan,
			@TaiKhoanKhoBac,
			@TenKhoBac,
			@GhiChu
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_Delete]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_AnDinhThue]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_AnDinhThue]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_DeleteDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_AnDinhThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_SelectDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[TKMD_Ref],
	[SoQuyetDinh],
	[NgayQuyetDinh],
	[NgayHetHan],
	[TaiKhoanKhoBac],
	[TenKhoBac],
	[GhiChu]
FROM [dbo].[t_KDT_AnDinhThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]
-- Database: ECS_TQ_SXXK_CTTT
-- Author: Ngo Thanh Tung
-- Time created: Sunday, November 06, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_AnDinhThue_ChiTiet_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[IDAnDinhThue],
	[SacThue],
	[TienThue],
	[Chuong],
	[Loai],
	[Khoan],
	[Muc],
	[TieuMuc],
	[GhiChu]
FROM [dbo].[t_KDT_AnDinhThue_ChiTiet] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_AnDinhThue_ChiTiet]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue_ChiTiet] ADD
CONSTRAINT [FK_t_KDT_AnDinhThue_ChiTiet_t_KDT_AnDinhThue] FOREIGN KEY ([IDAnDinhThue]) REFERENCES [dbo].[t_KDT_AnDinhThue] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ADD
CONSTRAINT [FK_t_KDT_AnDinhThue_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '1.2', [Date] = GETDATE()