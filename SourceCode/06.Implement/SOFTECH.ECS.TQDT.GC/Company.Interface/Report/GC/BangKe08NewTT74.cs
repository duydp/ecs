﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe08NewTT74 : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        public Report.ReportViewBC08TT74Form report = new ReportViewBC08TT74Form();

        public BangKe08NewTT74()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            this.ReportHeader.Visible = First;
            this.lblNgayKi.Visible = this.Last;
            this.lblNgayKi1.Visible = this.Last;
            this.lblNgayKi2.Visible = this.Last;
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;

            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[0].ColumnName);
            lblTKXK.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[1].ColumnName);
            
            //if (lblTKXK.Text == "")
            //    lblTKXK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            //else
            //    lblTKXK.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom))); 
            lblTenSPNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoLuong.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[3].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTriGia.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N"+ GlobalSettings.SoThapPhan.LuongNPL +"}");
            lblHopDong.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblChiCucHQ.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblSoBL.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[8].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblNgayKi.Text = GlobalSettings.TieudeNgay;
        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.Label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }

    }
}
