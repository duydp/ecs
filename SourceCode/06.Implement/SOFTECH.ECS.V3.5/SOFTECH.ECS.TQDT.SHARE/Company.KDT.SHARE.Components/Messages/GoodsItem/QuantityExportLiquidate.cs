﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
  public  class QuantityExportLiquidate
    {
        /// <summary>
        /// Lượng tiêu hủy
        /// </summary>
      [XmlElement("quantityDestruction")]
      public decimal QuantityDestruction { get; set; }

      /// <summary>
      /// Lượng Lượng bán
      /// </summary>
      [XmlElement("quantitySell")]
      public decimal QuantitySell { get; set; }
    }
}
