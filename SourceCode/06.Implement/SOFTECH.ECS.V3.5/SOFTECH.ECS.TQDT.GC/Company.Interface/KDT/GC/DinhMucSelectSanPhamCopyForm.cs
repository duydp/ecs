using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using System.Windows.Forms;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucSelectSanPhamCopyForm : BaseForm
    {
        public Company.GC.BLL.GC.SanPham SPDetail;
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.DinhMuc DMDetail = new Company.GC.BLL.KDT.GC.DinhMuc();
        public DinhMucDangKy dmdk = new DinhMucDangKy();
        public bool isBrower = false;
        public DinhMucSelectSanPhamCopyForm()
        {
            InitializeComponent();
            SPDetail = new Company.GC.BLL.GC.SanPham();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void DinhMucSelectSanPhamCopyForm_Load(object sender, EventArgs e)
        {
            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            sp.HopDong_ID = HD.ID;
            dgList.DataSource = sp.SelectCollectionBy_HopDong_ID_ChuaCoDM();
            //dgList.DataSource = 
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            SPDetail = (Company.GC.BLL.GC.SanPham)e.Row.DataRow;
            Company.GC.BLL.GC.DinhMuc dm = new Company.GC.BLL.GC.DinhMuc();
            string where = "";
            where += string.Format(" MaSanPham = '{0}' ", SPDetail.Ma);
            //dmdk.DMCollection = DMDetail.copyDinhMuc;
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}