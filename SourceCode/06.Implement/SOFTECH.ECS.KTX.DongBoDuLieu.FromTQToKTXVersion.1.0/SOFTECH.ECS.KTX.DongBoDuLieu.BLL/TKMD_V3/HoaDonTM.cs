﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.BLL.KDT;

namespace DongBoDuLieu.BLL.TKMD_V3
{
    public class HoaDonTM
    {
        private static HoaDonThuongMai ConvertToHDTM(DataRow drHD)
        {
            string LoaiHinh = drHD["Ma_LH"].ToString().Trim().Substring(0, 1);
            HoaDonThuongMai hd = new HoaDonThuongMai();
            hd.SoHoaDon = drHD["So_CT"].ToString();
            hd.NgayHoaDon = Convert.ToDateTime(drHD["Ngay_CT"]);
            //hd.SoHopDongTM = drHD["HanThanhToan"].ToString();
            hd.DKGH_ID = drHD["Ma_GH"].ToString();
            hd.PTTT_ID = drHD["Ma_PTTT"].ToString();
            hd.NguyenTe_ID = drHD["Ma_NT"].ToString();

            //Trên database HQ - Mã doanh nghiệp luôn là mã người mua
            // Tùy chỉnh lại đối với tờ khai xuất Doanh nghiệp là người bán
            if (LoaiHinh.ToUpper() == "X")
            {
                hd.MaDonViBan = drHD["MaNguoiMua"].ToString();
                hd.TenDonViBan = drHD["TenNguoiMua"].ToString();
                hd.MaDonViMua = drHD["MaNguoiBan"].ToString();
                hd.TenDonViMua = drHD["TenNguoiBan"].ToString();

            }
            else
            {
                hd.MaDonViMua = drHD["MaNguoiMua"].ToString();
                hd.TenDonViMua = drHD["TenNguoiMua"].ToString();
                hd.MaDonViBan = drHD["MaNguoiBan"].ToString();
                hd.TenDonViBan = drHD["TenNguoiBan"].ToString();
            }

            hd.ThongTinKhac = drHD["GhiChu"].ToString();

            return hd;
        }
        public static long InsertUpdateHoaDonTM(long TKMD_ID, DataRow drHoaDon, SqlTransaction transaction, SqlDatabase db)
        {
            HoaDonThuongMai HoaDonTM = ConvertToHDTM(drHoaDon);
            List<HoaDonThuongMai> listHDTM = HoaDonThuongMai.SelectCollectionDynamic("TKMD_ID = " + TKMD_ID, null, transaction, db);
            if (listHDTM != null && listHDTM.Count > 0)
            {
                return 0;
            }
            else
            {
                HoaDonTM.TKMD_ID = TKMD_ID;
                long ID = HoaDonTM.InsertTransaction(transaction, db);
                if (ID > 0)
                {
                    ToKhaiMauDich tkmd = new ToKhaiMauDich();
                    tkmd.ID = TKMD_ID;
                    tkmd.Load(transaction, db);
                    tkmd.LoadHMDCollection(transaction, db);
                    foreach (Company.BLL.KDT.HangMauDich hmd in tkmd.HMDCollection)
                    {
                        HoaDonThuongMaiDetail detail = new HoaDonThuongMaiDetail();
                        detail.DonGiaKB = (double)hmd.DonGiaKB;
                        detail.DVT_ID = hmd.DVT_ID;
                        detail.NuocXX_ID = hmd.NuocXX_ID;
                        detail.MaHS = hmd.MaHS;
                        detail.MaPhu = hmd.MaPhu;
                        detail.SoLuong = hmd.SoLuong;
                        detail.SoThuTuHang = hmd.SoThuTuHang;
                        detail.TriGiaKB = (double)hmd.TriGiaKB;
                        detail.HMD_ID = hmd.ID;
                        detail.TenHang = hmd.TenHang;
                        detail.HoaDonTM_ID = ID;
                        detail.InsertUpdate(transaction, db);
                    }
                }
                return ID;
            }
        }

    }
}
