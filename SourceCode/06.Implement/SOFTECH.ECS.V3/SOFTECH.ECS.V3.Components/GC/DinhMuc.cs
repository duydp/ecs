﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class DinhMuc
    {   
        /// <summary>
        /// ConfigProductionNorm
        /// </summary>
        /// <param name="ProductionNorm">XElement</param>        
        /// <returns>XElement</returns>
        public static XElement ConfigProductionNorm(XElement ProductionNorm, DinhMuc DM)
        {
            if (ProductionNorm != null && ProductionNorm.Elements().Count() > 0 && DM != null)
            {
                DonViTinh DVT = DonViTinh.Load(DM.DVT_ID);
                NguyenPhuLieu NPL = NguyenPhuLieu.LoadByMaNguyenPhuLieu(DM.MaNguyenPhuLieu);
                XElement MeterialsNorm = new XElement(NodeDinhMuc.MeterialsNorm);
                foreach (XElement element in ProductionNorm.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeDinhMuc.Product.ToUpper()))
                    {
                        foreach (XElement elementChild in element.Elements())
                        {
                            if (elementChild.Name.ToString().ToUpper().Equals(NodeDinhMuc.Commodity.ToUpper()))
                            {
                                foreach (XElement Child in elementChild.Elements())
                                {
                                    if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.description.ToUpper()))
                                        Child.SetValue(DM.TenSanPham);
                                    if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.identification.ToUpper()))
                                        Child.SetValue(DM.MaSanPham);
                                    if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.tariffClassification.ToUpper()))
                                        Child.SetValue("");
                                }
                            }
                            if (DVT != null)
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeDinhMuc.GoodsMeasure.ToUpper()))
                                {
                                    foreach (XElement Child in elementChild.Elements())
                                    {
                                        if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.measureUnit.ToUpper()))
                                            Child.SetValue(DVT.Ten);
                                    }
                                }
                            }
                        }
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeDinhMuc.MeterialsNorm.ToUpper()))
                        MeterialsNorm = element;
                }
                if (MeterialsNorm != null&&NPL!=null)//1..n tuy nhiên ở đây CSDL chỉ cho phép 1 Định mức thì chỉ có 1 Nguyên Phụ Liệu
                {
                    DVT = DonViTinh.Load(NPL.DVT_ID);
                    foreach (XElement element in MeterialsNorm.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeDinhMuc.Material.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodeDinhMuc.Commodity.ToUpper()))
                                {
                                    foreach (XElement Child in elementChild.Elements())
                                    {
                                        if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.description.ToUpper()))
                                            Child.SetValue(NPL.Ma);
                                        if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.identification.ToUpper()))
                                            Child.SetValue(NPL.Ten);
                                        if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.tariffClassification.ToUpper()))
                                            Child.SetValue(NPL.MaHS);
                                    }
                                }
                                if (DVT != null)
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeDinhMuc.GoodsMeasure.ToUpper()))
                                    {
                                        foreach (XElement Child in elementChild.Elements())
                                        {
                                            if (Child.Name.ToString().ToUpper().Equals(NodeDinhMuc.measureUnit.ToUpper()))
                                                Child.SetValue(DVT.Ten);
                                        }
                                    }
                                }
                                
                            }
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodeDinhMuc.norm.ToUpper()))
                            element.SetValue(DM.DinhMucSuDung);
                        if (element.Name.ToString().ToUpper().Equals(NodeDinhMuc.loss.ToUpper()))
                            element.SetValue(DM.TyLeHaoHut);
                    }
                }
            }
            return ProductionNorm;
        }
        /// <summary>
        /// ConfigDinhMuc
        /// </summary> 
        /// <returns>string</returns>
        public static string ConfigDinhMuc(HopDong HD, int issuer, int function, int status, int declarationOffice)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/DinhMuc.xml");                
                IList<DinhMuc> lDM;
                if (HD != null && docXML != null)
                {
                    XElement Root = docXML.Root;
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        CuaKhau CK = CuaKhau.Load(HD.MaHaiQuan);
                        DonViHaiQuan DVHQ = DonViHaiQuan.Load(HD.MaHaiQuan);
                        XElement ProductionNorm = Root.Element(NodeDinhMuc.ProductionNorm);
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.issuer.ToUpper()))
                                element.SetValue(issuer.ToString());
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.reference.ToUpper()))
                                element.SetValue(HD.ID);
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.issue.ToUpper()))
                                element.SetValue("");
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.function.ToUpper()))
                                element.SetValue(function);
                            if (CK != null)
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeHopDong.issueLocation.ToUpper()))
                                    element.SetValue(CK.Ten);
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.status.ToUpper()))
                                element.SetValue(status);
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.customsReference.ToUpper()))
                                element.SetValue(HD.GUIDSTR);
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.acceptance.ToUpper()))
                                element.SetValue(HD.NgayDangKy);
                            if (DVHQ != null)
                            {
                                if (element.Name.ToString().ToUpper().Equals(NodeHopDong.declarationOffice.ToUpper()))
                                    element.SetValue(DVHQ.Ten);
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Agent.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                                        elementChild.SetValue("");
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                                        elementChild.SetValue(HD.MaDaiLy);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.status.ToUpper()))
                                        elementChild.SetValue("");
                                }
                            }
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.Importer.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.name.ToUpper()))
                                        elementChild.SetValue(HD.DonViDoiTac);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.identity.ToUpper()))
                                        elementChild.SetValue("");
                                }
                            }                     
                            if (element.Name.ToString().ToUpper().Equals(NodeHopDong.customsReference.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.reference.ToUpper()))
                                        elementChild.SetValue(HD.SoTiepNhan);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.issue.ToUpper()))
                                        elementChild.SetValue(HD.NgayDangKy);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodeHopDong.declarationOffice.ToUpper()))
                                        elementChild.SetValue("");
                                }
                            }       
                            if (element.Name.ToString().ToUpper().Equals(NodeDinhMuc.ProductionNorm.ToUpper()))
                                ProductionNorm = element;
                        }
                        if (HD != null)
                        {
                            IList<DinhMucDangKy> lDMDK = DinhMucDangKy.SelectCollectionBy_ID_HopDong(HD.ID);
                            if (lDMDK != null && lDMDK.Count > 0)
                            {                                
                                lDM = DinhMuc.SelectCollectionBy_Master_ID(lDMDK[0].ID);
                                if(lDM!=null)
                                {
                                    if (lDM.Count == 1)
                                    {
                                        ConfigProductionNorm(ProductionNorm, lDM[0]);
                                    }
                                    else if (lDM.Count > 1)
                                    {
                                        XElement TempProductionNorm = new XElement(ProductionNorm);
                                        ConfigProductionNorm(ProductionNorm, lDM[0]);
                                        // Add new ProductionNorm
                                        for (int i = 1; i < lDM.Count; i++)
                                        {
                                            XElement NewProductionNorm = new XElement(TempProductionNorm);
                                            Root.Add(ConfigProductionNorm(NewProductionNorm, lDM[i]));
                                        }
                                    }
                                }                                
                                
                            }
                        }
                        docXML.Save(@"D:\NewDinhMuc.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
