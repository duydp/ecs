﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
#if KD_V3 || KD_V4
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.Utils;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.Utils;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.Utils;
#endif
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using System.Diagnostics;

namespace Company.Interface.Report
{

    public partial class ReportViewTKNTQDT_TaiCho_FormTT196 : BaseForm
    {
        public Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196 ToKhaiChinhReport = new Company.Interface.Report.TQDTToKhaiNK_TaiCho_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196 PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK PhuLucReportThueXNK = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue PhuLucReportMienThue = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue();
        public Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB PhuLucReportThueTTDB = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB();
        public Company.Interface.Report.BangkeSoContainer_TT196_CV3742 PhuLucContainer = new Company.Interface.Report.BangkeSoContainer_TT196_CV3742();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public XRControl Cell = new XRControl();
        public int index = 0;
        public LoaiPhuLuc LoaiPLCuaTK;
        public int SoLuongPhuLuc;


        /// <summary>
        /// Mac dinh = 9 dong hang tren phu luc to khai.
        /// </summary>
        private int soDongHang = 3;

        public ReportViewTKNTQDT_TaiCho_FormTT196()
        {
            InitializeComponent();
        }

        private void ReportViewTKXTQDTFormTT15_Load(object sender, EventArgs e)
        {
            //DATLMQ bổ sung thiết lập margin: 25/02/2011
            this.ToKhaiChinhReport.Margins.Top = 0;
            this.ToKhaiChinhReport.Margins.Bottom = 0;
            this.PhuLucReport.Margins.Top = 0;
            this.PhuLucReport.Margins.Bottom = 0;
            // edit by KhanhHn
            // fix bug tờ khai không có vận đơn
            int soContainer;
            if (TKMD.VanTaiDon != null)
                soContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
            else
                soContainer = 0;
            if (!Company.KDT.SHARE.Components.Globals.InCV3742)
                LoaiPLCuaTK = LoaiPhuLuc.TT196;
            else
                LoaiPLCuaTK = this.KiemTraHang(TKMD);
            if (LoaiPLCuaTK == LoaiPhuLuc.TT196)
            {
                if (this.TKMD.HMDCollection.Count > 1 || soContainer > 3)
                {
                    int countHang = (this.TKMD.HMDCollection.Count - 1) / 3 + 1;
                    int countContainer = (soContainer - 1) / 4 + 1;
                    int count = countHang >= countContainer ? countHang : countContainer;
                    for (int i = 0; i < count; i++)
                        this.AddItemComboBox();
                }
            }
            else if (LoaiPLCuaTK == LoaiPhuLuc.MienThue_TatCa)
            {
                int countHang = (this.TKMD.HMDCollection.Count - 1) / 20 + 1;
                int count = 0;
                if (this.TKMD.HMDCollection.Count > 1)
                    count = countHang;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
                SoLuongPhuLuc = count;
            }
            else if (LoaiPLCuaTK == LoaiPhuLuc.MienThue_TTDB_Va_BVMT)
            {
                int countHang = (this.TKMD.HMDCollection.Count - 1) / 4 + 1;
                int count = 0;
                if (this.TKMD.HMDCollection.Count > 1)
                    count = countHang;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
                SoLuongPhuLuc = count;
            }
            else if (LoaiPLCuaTK == LoaiPhuLuc.Full)
            {
                int countHang = (this.TKMD.HMDCollection.Count - 1) / 4 + 1;
                int count = 0;
                if (this.TKMD.HMDCollection.Count > 1)
                    count = countHang;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
                SoLuongPhuLuc = count;
            }
            if (GlobalSettings.IsKhongDungBangKeCont && LoaiPLCuaTK != LoaiPhuLuc.TT196)
            {
                int countPhuLucContainer = 0;
                if (soContainer > 4)
                    countPhuLucContainer = (soContainer - 1) / 20 + 1;
                for (int i = 1; i <= countPhuLucContainer; i++)
                    cboToKhai.Items.Add("Phụ lục Container " + i.ToString(), "cont" + i.ToString());
            }
            cboToKhai.SelectedIndex = 0;
        }

        /// <summary>
        /// Thêm số phục lục vào comboBox Tờ khai
        /// </summary>
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {
                    this.ToKhaiChinhReport.TKMD = this.TKMD;
                    this.ToKhaiChinhReport.report = this;
                    this.ToKhaiChinhReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
                    this.ToKhaiChinhReport.inThueBVMT = chkInThueBVMT.Checked;
                    this.ToKhaiChinhReport.InMaHang = chkInMaHang.Checked;
                    this.ToKhaiChinhReport.BindReport();
                    printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                    this.ToKhaiChinhReport.CreateDocument();
                }
                else if (!cboToKhai.SelectedValue.ToString().Contains("cont"))
                {
                    try
                    {
                        index = cboToKhai.SelectedIndex;
                    }
                    catch { index = 1; }

                    txtDeXuatKhac.Text = "";
                    txtTenNhomHang.Text = "";
                    if (LoaiPLCuaTK == LoaiPhuLuc.TT196)
                    {
                        List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                        // chia hang theo phu luc
                        int begin = (cboToKhai.SelectedIndex - 1) * 3; //Comment by Hungtq
                        int end = cboToKhai.SelectedIndex * 3; //Comment by Hungtq

                        if (begin <= this.TKMD.HMDCollection.Count - 1)
                        {
                            if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                            for (int i = begin; i < end; i++)
                                HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                        }
                        //chia container theo phu luc
                        List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                        int beginContainer = (cboToKhai.SelectedIndex - 1) * 4;
                        int endContainer = cboToKhai.SelectedIndex * 4;
                        //edit by KhanhHn - fix lỗi vận đơn null
                        if (TKMD.VanTaiDon != null)
                        {
                            if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
                            {
                                if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
                                    endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
                                for (int j = beginContainer; j < endContainer; j++)
                                    ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
                            }
                        }
                        this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196();
                        this.PhuLucReport.report = this;
                        this.PhuLucReport.TKMD = this.TKMD;
                        if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                            this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                        this.PhuLucReport.HMDCollection = HMDReportCollection;
                        this.PhuLucReport.ContCollection = ContainerReportCo;
                        this.PhuLucReport.SoDongHang = soDongHang;
                        this.PhuLucReport.Phulucso = cboToKhai.SelectedIndex;
                        this.PhuLucReport.InMaHang = chkInMaHang.Checked;
                        this.PhuLucReport.inThueBVMT = chkInThueBVMT.Checked;
                        this.PhuLucReport.BindReport(index.ToString());
                        printControl1.PrintingSystem = PhuLucReport.PrintingSystem;

                        this.PhuLucReport.CreateDocument();
                    }
                    else
                    {
                        switch (LoaiPLCuaTK)
                        {
                            case LoaiPhuLuc.Full:
                                {
                                    InPhuLucCoThueTTDB_CV3742();
                                }
                                break;
                            case LoaiPhuLuc.MienThue_TatCa:
                                {
                                    InPhuLucMienThue_CV3742();
                                }
                                break;
                            case LoaiPhuLuc.MienThue_TTDB_Va_BVMT:
                                {
                                    InPhuLucCoThueXNK_CV3742();
                                }
                                break;
                            default:
                                break;
                        }
                    }

                }
                else if (cboToKhai.SelectedValue.ToString().Contains("cont"))
                {
                    int indexPLCont = 1;
                    try
                    {
                        indexPLCont = Convert.ToInt16(cboToKhai.SelectedValue.ToString().Substring(4));
                    }
                    catch (System.Exception ex)
                    {
                        indexPLCont = 1;
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    int beginContainer = (indexPLCont - 1) * 20 + 1;
                    int endContainer = indexPLCont * 20;
                    if (endContainer > TKMD.VanTaiDon.ContainerCollection.Count) endContainer = TKMD.VanTaiDon.ContainerCollection.Count;
                    List<Company.KDT.SHARE.QuanLyChungTu.Container> listCont = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
                    for (int i = beginContainer - 1; i < endContainer; i++)
                    {
                        
                        listCont.Add(TKMD.VanTaiDon.ContainerCollection[i]);
                    }
                    this.PhuLucContainer = new BangkeSoContainer_TT196_CV3742();
                    //this.PhuLucContainer.report = this;
                    this.PhuLucContainer.Phulucso = cboToKhai.SelectedIndex;
                    this.PhuLucContainer.indexPL = indexPLCont;
                    this.PhuLucContainer.TKMD = TKMD;
                    this.PhuLucContainer.ContainerCollection = listCont;
                    this.PhuLucContainer.ToKhaiTaiCho = true;
                    this.PhuLucContainer.BindReport();
                    printControl1.PrintingSystem = PhuLucContainer.PrintingSystem;
                    this.PhuLucContainer.CreateDocument();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                if (cboToKhai.SelectedIndex == 0)
                {

                    this.ToKhaiChinhReport.CreateDocument();
                    try
                    {
                        printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi in báo cáo tờ khai: " + ex.Message, false);
                    }
                    //this.ToKhaiChinhReport.CreateDocument();
                }
                else
                {
                    this.PhuLucReport.CreateDocument();
                    try
                    {
                        printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                    }
                    catch (Exception ex)
                    {
                        ShowMessage("Lỗi in báo cáo tờ khai: " + ex.Message, false);
                    }
                    //this.PhuLucReport.CreateDocument();
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnApDung_Click(object sender, EventArgs e)
        {
            try
            {
//                 if (cboToKhai.SelectedIndex == 0)
//                 {
//                     //datlmq update 29072010
//                     //if (txtTenNhomHang.Text != "")
//                     //    this.ToKhaiChinhReport.setThongTin(this.Cell, txtTenNhomHang.Text);
//                     this.ToKhaiChinhReport.CreateDocument();
//                 }
//                 else
//                 {
//                     //this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
//                     this.PhuLucReport.CreateDocument();
//                 }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkInMaHang_CheckedChanged(object sender, EventArgs e)
        {
            cboToKhai_SelectedIndexChanged(null, null);
//             try
//             {
//                 if (cboToKhai.SelectedIndex == 0)
//                 {
//                     if (chkInMaHang.Checked)
//                         this.ToKhaiChinhReport.InMaHang = true;
//                     else
//                         this.ToKhaiChinhReport.InMaHang = false;
//                     this.ToKhaiChinhReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
//                     this.ToKhaiChinhReport.inThueBVMT = chkInThueBVMT.Checked;
//                     this.ToKhaiChinhReport.BindReport();
//                     printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
//                     this.ToKhaiChinhReport.CreateDocument();
// 
//                 }
//                 else
//                 {
//                     if (chkInMaHang.Checked)
//                         this.PhuLucReport.InMaHang = true;
//                     else
//                         this.PhuLucReport.InMaHang = false;
//                     this.PhuLucReport.inTriGiaTT = chkInTriGiaTinhThue.Checked;
//                     this.PhuLucReport.inThueBVMT = chkInThueBVMT.Checked;
//                     this.PhuLucReport.BindReport(index.ToString());
//                     printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
//                     this.PhuLucReport.CreateDocument();
// 
//                 }
//             }
//             catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
//                 if (cboToKhai.SelectedIndex == 0)
//                 {
//                     this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
//                     this.ToKhaiChinhReport.BindReport();
//                     printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
//                     this.ToKhaiChinhReport.CreateDocument();
// 
//                 }
//                 else
//                 {
//                     this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
//                     this.PhuLucReport.BindReport(index.ToString());
//                     printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
//                     this.PhuLucReport.CreateDocument();
// 
//                 }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void chkMienThueNK_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                cboToKhai_SelectedIndexChanged(null, null);
//                 if (cboToKhai.SelectedIndex == 0)
//                 {
//                     this.ToKhaiChinhReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
//                     // this.ToKhaiChinhReport.MienThueNK = chkMienThueNK.Checked;
//                     //this.ToKhaiChinhReport.MienThueGTGT = chkMienThueGTGT.Checked;
// 
//                     this.ToKhaiChinhReport.BindReport();
//                     printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
//                     this.ToKhaiChinhReport.CreateDocument();
//                 }
//                 else
//                 {
//                     this.PhuLucReport.BanLuuHaiQuan = chkInBanLuuHaiQuan.Checked;
//                     //this.PhuLucReport.MienThueNK = chkMienThueNK.Checked;
//                     //this.PhuLucReport.MienThueGTGT = chkMienThueGTGT.Checked;
// 
//                     this.PhuLucReport.BindReport(cboToKhai.Items.Count.ToString());
//                     printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
//                     this.PhuLucReport.CreateDocument();
// 
//                 }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// In pdf Button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnPrintPDF_Click(object sender, EventArgs e)
        {

            try
            {
                ExportPDF();
                #region comment by Khanhhn 26/04/2013
//                 if (cboToKhai.SelectedIndex == 0)
//                 {
// 
//                     this.ToKhaiChinhReport.CreateDocument();
//                     this.ToKhaiChinhReport.Margins.Top = 0;
//                     this.ToKhaiChinhReport.Margins.Bottom = 0;
// 
//                     // BEGIN: SAVE FILE TO PDF.
//                     SaveFileDialog dlg = new SaveFileDialog();
//                     dlg.InitialDirectory = "C:\\";
//                     dlg.RestoreDirectory = true;
//                     dlg.Filter = "pdf files (*.pdf)|*.pdf";
// 
//                     if (dlg.ShowDialog(this) == DialogResult.OK)
//                     {
//                         if (string.IsNullOrEmpty(dlg.FileName) == false)
//                         {
//                             string filePath = dlg.FileName;
//                             this.ToKhaiChinhReport.PrintingSystem.ExportToPdf(filePath);
//                             try
//                             {
//                                 // Open file pdf.
//                                 Process.Start(filePath);
//                             }
//                             catch (Exception ex) { throw ex; }
//                         }
//                     }
//                     // END: SAVE FILE TO PDF.
//                     this.ToKhaiChinhReport.CreateDocument();
//                 }
//                 else
//                 {
//                     this.PhuLucReport.CreateDocument();
//                     this.PhuLucReport.Margins.Top = 0;
//                     this.PhuLucReport.Margins.Bottom = 0;
// 
//                     // BEGIN: SAVE FILE TO PDF.
//                     SaveFileDialog dlg = new SaveFileDialog();
//                     dlg.InitialDirectory = "C:\\";
//                     dlg.RestoreDirectory = true;
//                     dlg.Filter = "pdf files (*.pdf)|*.pdf";
// 
//                     if (dlg.ShowDialog(this) == DialogResult.OK)
//                     {
//                         if (string.IsNullOrEmpty(dlg.FileName) == false)
//                         {
//                             string filePath = dlg.FileName;
//                             this.PhuLucReport.PrintingSystem.ExportToPdf(filePath);
//                             try
//                             {
//                                 // Open file pdf.
//                                 Process.Start(filePath);
//                             }
//                             catch (Exception ex) { throw ex; }
//                         }
//                     }
//                     // END: SAVE FILE TO PDF.
//                     this.PhuLucReport.CreateDocument();
//                 }
#endregion

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            string thongBao = "Lưu ý: \r\n";
            thongBao += "          Doanh nghiệp chịu hoàn toàn trách nhiệm trước pháp luật khi thay đổi nội dung tờ khai trên file Excel.";
            if (ShowMessage(thongBao, true) == "Yes")
            {
                Company.KDT.SHARE.Components.Globals.SaveMessage("Xuat Excel", TKMD.ID, "Xuất tờ khai ra Excel", thongBao);
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "Excel 2003 |*.xls";
                save.CheckPathExists = true;
                save.ShowDialog(this);
                XlsExportOptions optionToKhai = new XlsExportOptions();
                if (cboToKhai.SelectedIndex == 0)
                {
//                     this.ToKhaiChinhReport.TKMD = this.TKMD;
//                     this.ToKhaiChinhReport.report = this;
//                     this.ToKhaiChinhReport.InMaHang = chkInMaHang.Checked;
//                     this.ToKhaiChinhReport.BindReport();
//                     printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
//                     this.ToKhaiChinhReport.CreateDocument();
                    optionToKhai.SheetName = "To khai chinh";
                    optionToKhai.TextExportMode = TextExportMode.Text;
                    this.ToKhaiChinhReport.ExportToXls(save.FileName, optionToKhai);

                }
                else if (!cboToKhai.SelectedValue.ToString().Contains("cont"))
                {
                    try
                    {
                        index = cboToKhai.SelectedIndex;
                    }
                    catch { index = 1; }
// 
//                     txtDeXuatKhac.Text = "";
//                     txtTenNhomHang.Text = "";
//                     List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
//                     // chia hang theo phu luc
//                     int begin = (cboToKhai.SelectedIndex - 1) * 3; //Comment by Hungtq
//                     int end = cboToKhai.SelectedIndex * 3; //Comment by Hungtq
// 
//                     if (begin <= this.TKMD.HMDCollection.Count - 1)
//                     {
//                         if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
//                         for (int i = begin; i < end; i++)
//                             HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
//                     }
//                     //chia container theo phu luc
//                     List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
//                     int beginContainer = (cboToKhai.SelectedIndex - 1) * 4;
//                     int endContainer = cboToKhai.SelectedIndex * 4;
//                     //edit by KhanhHn - fix lỗi vận đơn null
//                     if (TKMD.VanTaiDon != null)
//                     {
//                         if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count - 1)
//                         {
//                             if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
//                                 endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
//                             for (int j = beginContainer; j < endContainer; j++)
//                                 ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
//                         }
//                     }
//                     this.PhuLucReport = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TaiCho_TT196();
//                     this.PhuLucReport.report = this;
//                     this.PhuLucReport.TKMD = this.TKMD;
//                     if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
//                         this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
//                     this.PhuLucReport.HMDCollection = HMDReportCollection;
//                     this.PhuLucReport.ContCollection = ContainerReportCo;
//                     this.PhuLucReport.SoDongHang = soDongHang;
//                     this.PhuLucReport.Phulucso = cboToKhai.SelectedIndex;
//                     this.PhuLucReport.BindReport(index.ToString());
//                     printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
// 
//                     this.PhuLucReport.CreateDocument();
                    optionToKhai.SheetName = "Phu luc " + index.ToString();
                    optionToKhai.TextExportMode = TextExportMode.Text;

                    if (LoaiPLCuaTK == LoaiPhuLuc.TT196 && this.PhuLucReport != null)
                        this.PhuLucReport.ExportToXls(save.FileName, optionToKhai);
                    else if (LoaiPLCuaTK == LoaiPhuLuc.MienThue_TatCa && this.PhuLucReportMienThue != null)
                        this.PhuLucReportMienThue.ExportToXls(save.FileName, optionToKhai);
                    else if (LoaiPLCuaTK == LoaiPhuLuc.MienThue_TTDB_Va_BVMT && this.PhuLucReportThueXNK != null)
                        this.PhuLucReportThueXNK.ExportToXls(save.FileName, optionToKhai);
                    else if (LoaiPLCuaTK == LoaiPhuLuc.Full && this.PhuLucReportThueTTDB != null)
                        this.PhuLucReportThueTTDB.ExportToXls(save.FileName, optionToKhai);

                }
                else if (cboToKhai.SelectedValue.ToString().Contains("cont"))
                {
                    if(this.PhuLucContainer != null)
                        this.PhuLucContainer.ExportToXls(save.FileName, optionToKhai);
                }

                Process.Start(save.FileName);
            }

            // 
            //             if (this.cboToKhai.SelectedIndex == 0)
            //             {
            //                 Globals.ExportExcel(Globals.ToKhaiType.Nhap, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThueNK.Checked, chkMienThueGTGT.Checked, "");
            //             }
            //             else
            //             {
            //                 Globals.ExportExcel(Globals.ToKhaiType.PhucLucTKN, TKMD.ID, chkInBanLuuHaiQuan.Checked, chkInMaHang.Checked, chkMienThueNK.Checked, chkMienThueGTGT.Checked, cboToKhai.SelectedIndex.ToString());
            //             }
        }
        private void ExportPDF()
        {
             SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF |*.pdf";
                save.CheckPathExists = true;
                save.ShowDialog(this);
                PdfExportOptions optionToKhai = new PdfExportOptions();
                if (cboToKhai.SelectedIndex == 0)
                {
//                     this.ToKhaiChinhReport.TKMD = this.TKMD;
//                     this.ToKhaiChinhReport.report = this;
//                     this.ToKhaiChinhReport.InMaHang = chkInMaHang.Checked;
//                     this.ToKhaiChinhReport.BindReport();
//                     printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
//                     this.ToKhaiChinhReport.CreateDocument();
//                    optionToKhai.ImageQuality = "To khai chinh";
  //                  optionToKhai.TextExportMode = TextExportMode.Text;
                    this.ToKhaiChinhReport.ExportToPdf(save.FileName, optionToKhai);

                }
                else if (!cboToKhai.SelectedValue.ToString().Contains("cont"))
                {
                    try
                    {
                        index = cboToKhai.SelectedIndex;
                    }
                    catch { index = 1; }
    //                  optionToKhai.SheetName = "Phu luc " + index.ToString();
      //              optionToKhai.TextExportMode = TextExportMode.Text;

                    if (LoaiPLCuaTK == LoaiPhuLuc.TT196 && this.PhuLucReport != null)
                        this.PhuLucReport.ExportToPdf(save.FileName, optionToKhai);
                    else if (LoaiPLCuaTK == LoaiPhuLuc.MienThue_TatCa && this.PhuLucReportMienThue != null)
                        this.PhuLucReportMienThue.ExportToPdf(save.FileName, optionToKhai);
                    else if (LoaiPLCuaTK == LoaiPhuLuc.MienThue_TTDB_Va_BVMT && this.PhuLucReportThueXNK != null)
                        this.PhuLucReportThueXNK.ExportToPdf(save.FileName, optionToKhai);
                    else if (LoaiPLCuaTK == LoaiPhuLuc.Full && this.PhuLucReportThueTTDB != null)
                        this.PhuLucReportThueTTDB.ExportToPdf(save.FileName, optionToKhai);

                }
                else if (cboToKhai.SelectedValue.ToString().Contains("cont"))
                {
                    if(this.PhuLucContainer != null)
                        this.PhuLucContainer.ExportToPdf(save.FileName, optionToKhai);
                }

                Process.Start(save.FileName);
        }
        public enum LoaiPhuLuc
        {
            Full, // In theo CV 3742 nhưng một trong các hàng hóa có thuế TTDB hoặc BVMT
            MienThue_TatCa, // In theo CV 3742 nhưng tất cả các hàng được miễn thuế
            MienThue_TTDB_Va_BVMT, // In theo CV 3742 nhưng tất cả các hàng được miễn thuế TTDB và BVMT
            TT196 // In theo TT 196
        }
        public LoaiPhuLuc KiemTraHang(ToKhaiMauDich TKMD)
        {
            try
            {
                LoaiPhuLuc temp = LoaiPhuLuc.MienThue_TatCa;
                foreach (HangMauDich hmd in TKMD.HMDCollection)
                {
                    if (hmd.ThueBVMT > 0 || hmd.ThueTTDB > 0)
                        return LoaiPhuLuc.Full;
                    else if (hmd.ThueSuatXNK > 0 || hmd.ThueGTGT > 0)
                        temp = LoaiPhuLuc.MienThue_TTDB_Va_BVMT;
                }
                return temp;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return LoaiPhuLuc.Full;
            }
        }
        public void InPhuLucCoThueXNK_CV3742()
        {
            List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
            // chia hang theo phu luc


            int begin = (cboToKhai.SelectedIndex - 1) * 4;
            int end = cboToKhai.SelectedIndex * 4;

            if (begin <= this.TKMD.HMDCollection.Count - 1)
            {
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
            }
            if (TKMD.HMDCollection.Count == 1)
                HMDReportCollection = null;
            //chia container theo phu luc
            //             List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
            //             int beginContainer = (cboToKhai.SelectedIndex) * 4 + 1;
            //             int endContainer = (cboToKhai.SelectedIndex + 1) * 4;
            //edit by KhanhHn - fix lỗi vận đơn null
            //             if (TKMD.VanTaiDon != null)
            //             {
            //                 if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
            //                 {
            //                     if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
            //                         endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
            //                     for (int j = beginContainer - 1; j < endContainer; j++)
            //                         ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
            //                 }
            //             }
            this.PhuLucReportThueXNK = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueXNK();
            //this.PhuLucReportThueXNK.report = this;
            this.PhuLucReportThueXNK.TKMD = this.TKMD;
            if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                this.PhuLucReportThueXNK.NgayDangKy = this.TKMD.NgayDangKy;
            this.PhuLucReportThueXNK.HMDCollection = HMDReportCollection;
            /*this.PhuLucReportThueXNK.ContCollection = ContainerReportCo;*/
            this.PhuLucReportThueXNK.SoDongHang = soDongHang;
            this.PhuLucReportThueXNK.Phulucso = cboToKhai.SelectedIndex;
            this.PhuLucReportThueXNK.InMaHang = chkInMaHang.Checked;
            this.PhuLucReportThueXNK.inThueBVMT = chkInThueBVMT.Checked;
            this.PhuLucReportThueXNK.inTriGiaTT = chkInTriGiaTinhThue.Checked;
            this.PhuLucReportThueXNK.ToKhaiTaiCho = true;
            this.PhuLucReportThueXNK.BindReport(index.ToString());
            printControl1.PrintingSystem = PhuLucReportThueXNK.PrintingSystem;

            this.PhuLucReportThueXNK.CreateDocument();
        }
        public void InPhuLucMienThue_CV3742()
        {
            List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
            // chia hang theo phu luc


            int begin = (cboToKhai.SelectedIndex - 1) * 20;
            int end = cboToKhai.SelectedIndex * 20;

            if (begin <= this.TKMD.HMDCollection.Count - 1)
            {
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
            }
            if (TKMD.HMDCollection.Count == 1)
                HMDReportCollection = null;
            //chia container theo phu luc
            //             List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
            //             int beginContainer = (cboToKhai.SelectedIndex) * 4 + 1;
            //             int endContainer = (cboToKhai.SelectedIndex + 1) * 4;
            //edit by KhanhHn - fix lỗi vận đơn null
            //             if (TKMD.VanTaiDon != null)
            //             {
            //                 if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
            //                 {
            //                     if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
            //                         endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
            //                     for (int j = beginContainer - 1; j < endContainer; j++)
            //                         ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
            //                 }
            //             }
            this.PhuLucReportMienThue = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_MienThue();
            /*this.PhuLucReportMienThue.report = this;*/
            this.PhuLucReportMienThue.ToKhaiTaiCho = true;
            this.PhuLucReportMienThue.TKMD = this.TKMD;
            if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                this.PhuLucReportMienThue.NgayDangKy = this.TKMD.NgayDangKy;
            this.PhuLucReportMienThue.HMDCollection = HMDReportCollection;
            /*this.PhuLucReportMienThue.ContCollection = ContainerReportCo;*/
            this.PhuLucReportMienThue.SoDongHang = soDongHang;
            this.PhuLucReportMienThue.Phulucso = cboToKhai.SelectedIndex;
            this.PhuLucReportMienThue.InMaHang = chkInMaHang.Checked;
            this.PhuLucReportMienThue.inThueBVMT = chkInThueBVMT.Checked;
            this.PhuLucReportMienThue.inTriGiaTT = chkInTriGiaTinhThue.Checked;
            this.PhuLucReportMienThue.BindReport(index.ToString());
            printControl1.PrintingSystem = PhuLucReportMienThue.PrintingSystem;
            this.PhuLucReportMienThue.CreateDocument();
        }
        public void InPhuLucCoThueTTDB_CV3742()
        {
            List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
            // chia hang theo phu luc


            int begin = (cboToKhai.SelectedIndex - 1) * 4;
            int end = cboToKhai.SelectedIndex * 4;

            if (begin <= this.TKMD.HMDCollection.Count - 1)
            {
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
            }
            if (TKMD.HMDCollection.Count == 1)
                HMDReportCollection = null;
            //chia container theo phu luc
            //             List<Company.KDT.SHARE.QuanLyChungTu.Container> ContainerReportCo = new List<Company.KDT.SHARE.QuanLyChungTu.Container>();
            //             int beginContainer = (cboToKhai.SelectedIndex) * 4 + 1;
            //             int endContainer = (cboToKhai.SelectedIndex + 1) * 4;
            //edit by KhanhHn - fix lỗi vận đơn null
            //             if (TKMD.VanTaiDon != null)
            //             {
            //                 if (beginContainer <= this.TKMD.VanTaiDon.ContainerCollection.Count)
            //                 {
            //                     if (endContainer > this.TKMD.VanTaiDon.ContainerCollection.Count)
            //                         endContainer = this.TKMD.VanTaiDon.ContainerCollection.Count;
            //                     for (int j = beginContainer - 1; j < endContainer; j++)
            //                         ContainerReportCo.Add(this.TKMD.VanTaiDon.ContainerCollection[j]);
            //                 }
            //             }
            this.PhuLucReportThueTTDB = new Company.Interface.Report.TQDTPhuLucToKhaiNhap_TT196_CV3742_CoThueTTDB();
            /*this.PhuLucReportThueTTDB.report = this;*/
            this.PhuLucReportThueTTDB.ToKhaiTaiCho = true;
            this.PhuLucReportThueTTDB.TKMD = this.TKMD;
            if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                this.PhuLucReportThueTTDB.NgayDangKy = this.TKMD.NgayDangKy;
            this.PhuLucReportThueTTDB.HMDCollection = HMDReportCollection;
            /*this.PhuLucReportThueTTDB.ContCollection = ContainerReportCo;*/
            this.PhuLucReportThueTTDB.SoDongHang = soDongHang;
            this.PhuLucReportThueTTDB.Phulucso = cboToKhai.SelectedIndex;
            this.PhuLucReportThueTTDB.InMaHang = chkInMaHang.Checked;
            this.PhuLucReportThueTTDB.inThueBVMT = chkInThueBVMT.Checked;
            this.PhuLucReportThueTTDB.inTriGiaTT = chkInTriGiaTinhThue.Checked;
            this.PhuLucReportThueTTDB.BindReport(index.ToString());
            printControl1.PrintingSystem = PhuLucReportThueTTDB.PrintingSystem;

            this.PhuLucReportThueTTDB.CreateDocument();
        }


    }
}