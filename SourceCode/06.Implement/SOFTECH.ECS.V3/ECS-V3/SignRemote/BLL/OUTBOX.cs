using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.IO;

namespace SignRemote
{
    public partial class OUTBOX
    {
        public static void InsertDataSignLocal(string guidId, string signData, string signCert)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                OUTBOX.InsertOUTBOX(string.Empty, string.Empty, string.Empty, signData, signCert, DateTime.Now, -1, guidId);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public static void UpdateDataSignLocal(string guidId, string signData, string signCert)
        {
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                OUTBOX.UpdateOutBoxNew(signData, signCert, guidId);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //private static void UpdateOutBoxNew(string MSG_SIGN_DATA, string MSG_SIGN_CERT, string GUIDSTR,string Status)
        //{
        //    const string spName = "p_MSG_OUTBOX_Update_New";
        //    SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        //    SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
        //    db.AddInParameter(dbCommand, "@MSG_SIGN_DATA", SqlDbType.NVarChar, MSG_SIGN_DATA);
        //    db.AddInParameter(dbCommand, "@MSG_SIGN_CERT", SqlDbType.NVarChar, MSG_SIGN_CERT);
        //    db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
        //    db.ExecuteNonQuery(dbCommand);
        //}
        public static OUTBOX GetContentSignLocal(string cnnString, string guidId)
        {
            try
            {
                SqlConnection cnn = new SqlConnection(cnnString);
                cnn.Open();
                string cmdText = string.Format("Select top(1) * from t_MSG_OUTBOX where GUIDSTR='{0}' order by CREATE_TIME desc", guidId);
                SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);
                IDataReader reader = sqlCmd.ExecuteReader();
                OUTBOX outbox = null;
                if (reader.Read())
                {
                    outbox = new OUTBOX();
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) outbox.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) outbox.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
                }
                reader.Close();
                //cmdText = string.Format("Delete t_MSG_OUTBOX where  GUIDSTR='{0}'", guidId);
                //sqlCmd.CommandText = cmdText;
                //sqlCmd.ExecuteNonQuery();
                return outbox;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }
        public static List<OUTBOX> GetCollectionContentSignLocal(string cnnString, string guidId)
        {
            try
            {
                SqlConnection cnn = new SqlConnection(cnnString);
                cnn.Open();
                string cmdText = string.Format("Select * from t_MSG_OUTBOX where GUIDSTR='{0}' order by CREATE_TIME desc", guidId);
                SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);
                IDataReader reader = sqlCmd.ExecuteReader();
                List<OUTBOX> collection = new List<OUTBOX>();
                while (reader.Read())
                {
                    OUTBOX outbox = new OUTBOX();
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) outbox.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
                    if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) outbox.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
                    collection.Add(outbox);

                }
                reader.Close();
                //cmdText = string.Format("Delete t_MSG_OUTBOX where  GUIDSTR='{0}'", guidId);
                //sqlCmd.CommandText = cmdText;
                //sqlCmd.ExecuteNonQuery();
                return collection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }

        }
        //public void ProcessExcuteIfGetSignFailed()
        //{
        //    try
        //    {
        //        ProcessAPINew process = new ProcessAPINew(true);
        //        process.DoProcess();
        //    }
        //    catch (Exception ex)
        //    {
        //        StreamWriter write = File.AppendText("Error.txt");
        //        write.WriteLine("--------------------------------");
        //        write.WriteLine("Lỗi ProcessExcuteIfGetSignFailed. Thời gian thực hiện : " + DateTime.Now.ToString());
        //        write.WriteLine(ex.StackTrace);
        //        write.WriteLine("Lỗi là : ");
        //        write.WriteLine(ex.Message);
        //        write.WriteLine("--------------------------------");
        //        write.Flush();
        //        write.Close();

        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
    }

}