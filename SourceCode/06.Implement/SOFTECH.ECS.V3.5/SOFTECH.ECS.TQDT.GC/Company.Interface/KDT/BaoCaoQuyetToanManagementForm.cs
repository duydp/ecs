﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT
{
    public partial class BaoCaoQuyetToanManagementForm : BaseForm
    {
        public string where = "";
        public BaoCaoQuyetToanManagementForm()
        {
            InitializeComponent();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQT = new KDT_VNACCS_BaoCaoQuyetToan_TT39();
                BCQT = (KDT_VNACCS_BaoCaoQuyetToan_TT39)e.Row.DataRow;
                BaoCaoQuyetToanHopDongForm f = new BaoCaoQuyetToanHopDongForm();
                f.BCQT = BCQT;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void BaoCaoQuyetToanManagementForm_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            cbStatus.SelectedValue = 0;
            txtNamQT.Text = DateTime.Now.Year.ToString();
            GetSearchWhere();
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTN LIKE '%" + txtSoTiepNhan.Text + "%'";
                if (!String.IsNullOrEmpty(txtNamQT.Text))
                    where += " AND YEAR(NgayTN) = " + txtNamQT.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_TT39.SelectCollectionDynamic(GetSearchWhere(), "");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách báo cáo quyết toán " + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Doanh nghiệp có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdUpdateStatus":
                    UpdateStatus();
                    break;
            } 
        }

        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<KDT_VNACCS_BaoCaoQuyetToan_TT39> BCQTCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            BCQTCollection.Add((KDT_VNACCS_BaoCaoQuyetToan_TT39)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < BCQTCollection.Count; i++)
                        {
                            Company.Interface.KDT.GC.UpdateStatusForm f = new Company.Interface.KDT.GC.UpdateStatusForm();
                            f.BCQT = BCQTCollection[i];
                            f.formType = "BCQT";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                    else
                    {
                        showMsg("MSG_2702040", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                string LoaiBaoCao = e.Row.Cells["LoaiBaoCao"].Value.ToString();
                switch (LoaiBaoCao)
                {
                    case "1":
                        e.Row.Cells["LoaiBaoCao"].Text = "Nguyên liệu, vật liệu nhập khẩu ";
                        break;
                    case "2":
                        e.Row.Cells["LoaiBaoCao"].Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count < 0) return;
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA BÁO CÁO QUYẾT TOÁN NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        KDT_VNACCS_BaoCaoQuyetToan_TT39 goodItems = (KDT_VNACCS_BaoCaoQuyetToan_TT39)row.GetRow().DataRow;
                        goodItems.HopDongCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail.SelectCollectionBy_GoodItem_ID(goodItems.ID);
                        foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in goodItems.HopDongCollection)
                        {
                            item.HangHoaCollection = KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail.SelectCollectionBy_Contract_ID(item.ID);
                        }
                        if (goodItems.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || goodItems.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || goodItems.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || goodItems.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        {
                            ShowMessageTQDT("BÁO CÁO QUYẾT TOÁN ĐÃ GỬI LÊN HQ .DOANH NGHIỆP KHÔNG ĐƯỢC XOÁ BCQT NÀY",false);
                        }
                        else
                        {
                            goodItems.DeleteFull();
                        }
                    }
                    btnSearch_Click(null,null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
