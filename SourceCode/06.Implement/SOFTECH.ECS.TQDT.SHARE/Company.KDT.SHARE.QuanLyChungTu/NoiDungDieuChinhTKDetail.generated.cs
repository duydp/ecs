using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class NoiDungDieuChinhTKDetail
	{
		#region Properties.
		
		public int ID { set; get; }
		public string NoiDungTKChinh { set; get; }
		public string NoiDungTKSua { set; get; }
		public int Id_DieuChinh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static NoiDungDieuChinhTKDetail Load(int id)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
			NoiDungDieuChinhTKDetail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new NoiDungDieuChinhTKDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKChinh"))) entity.NoiDungTKChinh = reader.GetString(reader.GetOrdinal("NoiDungTKChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKSua"))) entity.NoiDungTKSua = reader.GetString(reader.GetOrdinal("NoiDungTKSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("Id_DieuChinh"))) entity.Id_DieuChinh = reader.GetInt32(reader.GetOrdinal("Id_DieuChinh"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<NoiDungDieuChinhTKDetail> SelectCollectionAll()
		{
			List<NoiDungDieuChinhTKDetail> collection = new List<NoiDungDieuChinhTKDetail>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKChinh"))) entity.NoiDungTKChinh = reader.GetString(reader.GetOrdinal("NoiDungTKChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKSua"))) entity.NoiDungTKSua = reader.GetString(reader.GetOrdinal("NoiDungTKSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("Id_DieuChinh"))) entity.Id_DieuChinh = reader.GetInt32(reader.GetOrdinal("Id_DieuChinh"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<NoiDungDieuChinhTKDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<NoiDungDieuChinhTKDetail> collection = new List<NoiDungDieuChinhTKDetail>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKChinh"))) entity.NoiDungTKChinh = reader.GetString(reader.GetOrdinal("NoiDungTKChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKSua"))) entity.NoiDungTKSua = reader.GetString(reader.GetOrdinal("NoiDungTKSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("Id_DieuChinh"))) entity.Id_DieuChinh = reader.GetInt32(reader.GetOrdinal("Id_DieuChinh"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<NoiDungDieuChinhTKDetail> SelectCollectionBy_Id_DieuChinh(int id_DieuChinh)
		{
			List<NoiDungDieuChinhTKDetail> collection = new List<NoiDungDieuChinhTKDetail>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_Id_DieuChinh(id_DieuChinh);
			while (reader.Read())
			{
				NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKChinh"))) entity.NoiDungTKChinh = reader.GetString(reader.GetOrdinal("NoiDungTKChinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDungTKSua"))) entity.NoiDungTKSua = reader.GetString(reader.GetOrdinal("NoiDungTKSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("Id_DieuChinh"))) entity.Id_DieuChinh = reader.GetInt32(reader.GetOrdinal("Id_DieuChinh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Id_DieuChinh(int id_DieuChinh)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, id_DieuChinh);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Id_DieuChinh(int id_DieuChinh)
		{
			const string spName = "p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, id_DieuChinh);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertNoiDungDieuChinhTKDetail(string noiDungTKChinh, string noiDungTKSua, int id_DieuChinh)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();	
			entity.NoiDungTKChinh = noiDungTKChinh;
			entity.NoiDungTKSua = noiDungTKSua;
			entity.Id_DieuChinh = id_DieuChinh;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
			db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateNoiDungDieuChinhTKDetail(int id, string noiDungTKChinh, string noiDungTKSua, int id_DieuChinh)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();			
			entity.ID = id;
			entity.NoiDungTKChinh = noiDungTKChinh;
			entity.NoiDungTKSua = noiDungTKSua;
			entity.Id_DieuChinh = id_DieuChinh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
			db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateNoiDungDieuChinhTKDetail(int id, string noiDungTKChinh, string noiDungTKSua, int id_DieuChinh)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();			
			entity.ID = id;
			entity.NoiDungTKChinh = noiDungTKChinh;
			entity.NoiDungTKSua = noiDungTKSua;
			entity.Id_DieuChinh = id_DieuChinh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@NoiDungTKChinh", SqlDbType.NVarChar, NoiDungTKChinh);
			db.AddInParameter(dbCommand, "@NoiDungTKSua", SqlDbType.NVarChar, NoiDungTKSua);
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, Id_DieuChinh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteNoiDungDieuChinhTKDetail(int id)
		{
			NoiDungDieuChinhTKDetail entity = new NoiDungDieuChinhTKDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Id_DieuChinh(int id_DieuChinh)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteBy_Id_DieuChinh]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id_DieuChinh", SqlDbType.Int, id_DieuChinh);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<NoiDungDieuChinhTKDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (NoiDungDieuChinhTKDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}