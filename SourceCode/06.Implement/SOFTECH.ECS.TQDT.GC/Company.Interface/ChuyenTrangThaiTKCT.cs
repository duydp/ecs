using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL;

namespace Company.Interface
{
    public partial class ChuyenTrangThaiTKCT : BaseForm
    {
        ToKhaiChuyenTiep tkct;
        public ChuyenTrangThaiTKCT(ToKhaiChuyenTiep tk)
        {
            InitializeComponent();
            this.tkct = tk;
            this.Text = this.Text + " " + this.tkct.ID.ToString();
            this.lblCaption.Text = this.lblCaption.Text + " " + this.tkct.ID.ToString();
            this.lblMaToKhai.Text = this.tkct.ID.ToString();
            this.lblMaLoaiHinh.Text = this.tkct.MaLoaiHinh;
            if (tkct.SoToKhai > 0)
            {
                txtSoToKhai.Text = tkct.SoToKhai.ToString();
                ccNgayDayKy.Text = tkct.NgayDangKy.ToShortDateString();
            }
            if (tkct.PhanLuong != "") {
                cbPL.SelectedValue = tkct.PhanLuong;
            }
        }

        private void btnThuchien_Click(object sender, EventArgs e)
        {
            if (this.validData())
            {
                this.getData();
                //this.tkmd.LoadHMDCollection();

                ToKhaiChuyenTiep tkDaDangKy = new ToKhaiChuyenTiep();
                tkDaDangKy.MaHaiQuanTiepNhan = tkct.MaHaiQuanTiepNhan;
                tkDaDangKy.NgayDangKy = tkct.NgayDangKy;
                tkDaDangKy.MaLoaiHinh = tkct.MaLoaiHinh;
                tkDaDangKy.SoToKhai = tkct.SoToKhai;
                tkDaDangKy.PhanLuong = tkct.PhanLuong;

                if (!tkDaDangKy.isHaveWith4Para())
                {
                    Company.KDT.SHARE.Components.MessageTypes msgT = Company.KDT.SHARE.Components.MessageTypes.ToKhaiChuyenTiepNhap;
                    if(tkct.MaLoaiHinh.Substring(0,1).Equals("X"))
                        msgT = Company.KDT.SHARE.Components.MessageTypes.ToKhaiChuyenTiepXuat;
                    Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                        tkct.ID,tkct.GUIDSTR,msgT,Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                          string.Format("Trước khi chuyển trạng thái ={0}", tkct.TrangThaiXuLy));

                    this.tkct.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                    this.tkct.HUONGDAN = txtHuongdan.Text;
                    this.tkct.Update();
                    this.tkct.TransferToNPLTon();
                         
                    if (showMsg("MSG_240241") == "Cancel")
                    //if (ShowMessage("Chuyển trạng thái thành công", false) == "Cancel")
                    {
                        this.Close();
                    }
                }
                else
                {
                    showMsg("MSG_240242");
                    //ShowMessage("Thông tin tờ khai này trùng trong danh sách đăng ký! \nVui lòng kiểm tra lại số tờ khai và ngày đăng ký", false);
                }
            }
            else
            {
                showMsg("MSG_240243");
                //ShowMessage("Dữ liệu nhập vào chưa hợp lệ! \nVui lòng nhập lại.", false);
            }
        }

        private bool validData()
        {
            bool value = true;
            if (txtSoToKhai.Text == "" || int.Parse(txtSoToKhai.Text) < 0) value = false;
            if (cbPL.SelectedValue == null)
            {
                cbPL.Focus();
                return false;
            }
            return value;
        }

        private void getData()
        {
            this.tkct.SoToKhai = int.Parse(txtSoToKhai.Text);
            this.tkct.NgayDangKy = ccNgayDayKy.Value;
            this.tkct.PhanLuong = cbPL.SelectedValue.ToString();
            if (this.tkct.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                this.tkct.NgayTiepNhan = this.tkct.NgayDangKy;
            }
        }

        private void btnHuybo_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}