﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Configuration;

namespace Company.KDT.SHARE.Components
{
    public class Version
    {
        protected static SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_Version where [Version] = (SELECT Max(cast([Version] as decimal(5,2))) FROM t_HaiQuan_Version)";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string GetVersion()
        {
            try
            {
                DataSet ds = SelectAll();
                string version = "", date = "";
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    version = ds.Tables[0].Rows[0]["Version"].ToString();
                    date = System.Convert.ToDateTime(ds.Tables[0].Rows[0]["Date"]).ToString("dd/MM/yyyy");
                    return version + ", " + date;
                }
                else
                    return "";
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }

        public static int UpdateVesion(string version)
        {
            //Xoa version moi bi loi so voi version truoc khi cap nhat
            string query = @"if (select COUNT(*) FROM t_HaiQuan_Version) = 1
                                begin
                                    update t_HaiQuan_Version set [version] = '" + version + "'" +
                            @"    end
                            else
                                begin
                                    DELETE FROM t_HaiQuan_Version WHERE cast([Version] as decimal(5,2)) > '" + version + "'" +
                                "end"; ; //TODO: Hungtq, 11/11/2013                
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteNonQuery(dbCommand);
        }

    }
}