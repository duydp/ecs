using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAE4000 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute FIC { get; set; }
public PropertiesAttribute BNO { get; set; }
public PropertiesAttribute DNO { get; set; }
public PropertiesAttribute TDN { get; set; }
public PropertiesAttribute ECB { get; set; }
public PropertiesAttribute CCC { get; set; }
public PropertiesAttribute MTC { get; set; }
public PropertiesAttribute RID { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute ECD { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EPP { get; set; }
public PropertiesAttribute EPA { get; set; }
public PropertiesAttribute EPT { get; set; }
public PropertiesAttribute EXC { get; set; }
public PropertiesAttribute EXN { get; set; }
public PropertiesAttribute CGC { get; set; }
public PropertiesAttribute CGN { get; set; }
public PropertiesAttribute CGP { get; set; }
public PropertiesAttribute CGA { get; set; }
public PropertiesAttribute CAT { get; set; }
public PropertiesAttribute CAC { get; set; }
public PropertiesAttribute CAS { get; set; }
public PropertiesAttribute CGK { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute EKN { get; set; }
public PropertiesAttribute NO { get; set; }
public PropertiesAttribute NOT { get; set; }
public PropertiesAttribute GW { get; set; }
public PropertiesAttribute GWT { get; set; }
public PropertiesAttribute ST { get; set; }
public PropertiesAttribute DSC { get; set; }
public PropertiesAttribute DSN { get; set; }
public PropertiesAttribute PSC { get; set; }
public PropertiesAttribute PSN { get; set; }
public PropertiesAttribute VSC { get; set; }
public PropertiesAttribute VSN { get; set; }
public PropertiesAttribute SYM { get; set; }
public PropertiesAttribute MRK { get; set; }
public GroupAttribute SS_ { get; set; }
public PropertiesAttribute IV1 { get; set; }
public PropertiesAttribute IV2 { get; set; }
public PropertiesAttribute IV3 { get; set; }
public PropertiesAttribute IVD { get; set; }
public PropertiesAttribute IVP { get; set; }
public PropertiesAttribute IP2 { get; set; }
public PropertiesAttribute IP3 { get; set; }
public PropertiesAttribute IP4 { get; set; }
public PropertiesAttribute IP1 { get; set; }
public PropertiesAttribute FCD { get; set; }
public PropertiesAttribute FKK { get; set; }
public PropertiesAttribute CNV { get; set; }
public PropertiesAttribute TP { get; set; }
public PropertiesAttribute TPM { get; set; }
public PropertiesAttribute BRC { get; set; }
public PropertiesAttribute BYA { get; set; }
public PropertiesAttribute BCM { get; set; }
public PropertiesAttribute BCN { get; set; }
public PropertiesAttribute ENC { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute SCM { get; set; }
public PropertiesAttribute SCN { get; set; }
public GroupAttribute EA_ { get; set; }
public PropertiesAttribute DPD { get; set; }
public GroupAttribute ST_ { get; set; }
public PropertiesAttribute ARP { get; set; }
public PropertiesAttribute ADT { get; set; }
public PropertiesAttribute NT2 { get; set; }
public PropertiesAttribute REF { get; set; }
public GroupAttribute VC_ { get; set; }
public PropertiesAttribute VN { get; set; }
public PropertiesAttribute VA { get; set; }
public GroupAttribute C__ { get; set; }
public PropertiesAttribute CCM { get; set; }
public GroupAttribute D__ { get; set; }

public VAE4000()
        {
ECN = new PropertiesAttribute(12, typeof(int));
FIC = new PropertiesAttribute(12, typeof(string));
BNO = new PropertiesAttribute(2, typeof(int));
DNO = new PropertiesAttribute(2, typeof(int));
TDN = new PropertiesAttribute(12, typeof(int));
ECB = new PropertiesAttribute(3, typeof(string));
CCC = new PropertiesAttribute(1, typeof(string));
MTC = new PropertiesAttribute(1, typeof(string));
RID = new PropertiesAttribute(8, typeof(DateTime));
CH = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
ECD = new PropertiesAttribute(8, typeof(DateTime));
EPC = new PropertiesAttribute(13, typeof(string));
EPN = new PropertiesAttribute(300, typeof(string));
EPP = new PropertiesAttribute(7, typeof(string));
EPA = new PropertiesAttribute(300, typeof(string));
EPT = new PropertiesAttribute(20, typeof(string));
EXC = new PropertiesAttribute(13, typeof(string));
EXN = new PropertiesAttribute(300, typeof(string));
CGC = new PropertiesAttribute(13, typeof(string));
CGN = new PropertiesAttribute(70, typeof(string));
CGP = new PropertiesAttribute(9, typeof(string));
CGA = new PropertiesAttribute(35, typeof(string));
CAT = new PropertiesAttribute(35, typeof(string));
CAC = new PropertiesAttribute(35, typeof(string));
CAS = new PropertiesAttribute(35, typeof(string));
CGK = new PropertiesAttribute(2, typeof(string));
ECC = new PropertiesAttribute(5, typeof(string));
EKN = new PropertiesAttribute(35, typeof(string));
NO = new PropertiesAttribute(8, typeof(int));
NOT = new PropertiesAttribute(3, typeof(string));
GW = new PropertiesAttribute(10, typeof(int));
GWT = new PropertiesAttribute(3, typeof(string));
ST = new PropertiesAttribute(7, typeof(string));
DSC = new PropertiesAttribute(5, typeof(string));
DSN = new PropertiesAttribute(35, typeof(string));
PSC = new PropertiesAttribute(6, typeof(string));
PSN = new PropertiesAttribute(35, typeof(string));
VSC = new PropertiesAttribute(9, typeof(string));
VSN = new PropertiesAttribute(35, typeof(string));
SYM = new PropertiesAttribute(8, typeof(DateTime));
MRK = new PropertiesAttribute(140, typeof(string));
IV1 = new PropertiesAttribute(1, typeof(string));
IV2 = new PropertiesAttribute(12, typeof(int));
IV3 = new PropertiesAttribute(35, typeof(string));
IVD = new PropertiesAttribute(8, typeof(DateTime));
IVP = new PropertiesAttribute(7, typeof(string));
IP2 = new PropertiesAttribute(3, typeof(string));
IP3 = new PropertiesAttribute(3, typeof(string));
IP4 = new PropertiesAttribute(20, typeof(int));
IP1 = new PropertiesAttribute(1, typeof(string));
FCD = new PropertiesAttribute(3, typeof(string));
FKK = new PropertiesAttribute(20, typeof(int));
CNV = new PropertiesAttribute(1, typeof(string));
TP = new PropertiesAttribute(20, typeof(int));
TPM = new PropertiesAttribute(1, typeof(string));
BRC = new PropertiesAttribute(11, typeof(string));
BYA = new PropertiesAttribute(4, typeof(int));
BCM = new PropertiesAttribute(10, typeof(string));
BCN = new PropertiesAttribute(10, typeof(string));
ENC = new PropertiesAttribute(1, typeof(string));
SBC = new PropertiesAttribute(11, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
SCM = new PropertiesAttribute(10, typeof(string));
SCN = new PropertiesAttribute(10, typeof(string));
DPD = new PropertiesAttribute(8, typeof(DateTime));
ARP = new PropertiesAttribute(7, typeof(string));
ADT = new PropertiesAttribute(8, typeof(DateTime));
NT2 = new PropertiesAttribute(300, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
VN = new PropertiesAttribute(70, typeof(string));
VA = new PropertiesAttribute(300, typeof(string));
CCM = new PropertiesAttribute(1, typeof(string));
#region SS_
List<PropertiesAttribute> listSS_ = new List<PropertiesAttribute>();
listSS_.Add(new PropertiesAttribute(4, 5, EnumGroupID.VAE4000_SS_, typeof(string)));
listSS_.Add(new PropertiesAttribute(20, 5, EnumGroupID.VAE4000_SN_, typeof(string)));
SS_ = new GroupAttribute("SS_", 5, listSS_);
#endregion SS_
#region EA_
List<PropertiesAttribute> listEA_ = new List<PropertiesAttribute>();
listEA_.Add(new PropertiesAttribute(3, 3, EnumGroupID.VAE4000_EA_, typeof(string)));
listEA_.Add(new PropertiesAttribute(12, 3, EnumGroupID.VAE4000_EB_, typeof(int)));
EA_ = new GroupAttribute("EA_", 3, listEA_);
#endregion EA_
#region ST_
List<PropertiesAttribute> listST_ = new List<PropertiesAttribute>();
listST_.Add(new PropertiesAttribute(7, 3, EnumGroupID.VAE4000_ST_, typeof(string)));
listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAE4000_AD_, typeof(DateTime)));
listST_.Add(new PropertiesAttribute(8, 3, EnumGroupID.VAE4000_SD_, typeof(DateTime)));
ST_ = new GroupAttribute("ST_", 3, listST_);
#endregion ST_
#region VC_
List<PropertiesAttribute> listVC_ = new List<PropertiesAttribute>();
listVC_.Add(new PropertiesAttribute(7, 5, EnumGroupID.VAE4000_VC_, typeof(string)));
VC_ = new GroupAttribute("VC_", 5, listVC_);
#endregion VC_
#region C__
List<PropertiesAttribute> listC__ = new List<PropertiesAttribute>();
listC__.Add(new PropertiesAttribute(12, 50, EnumGroupID.VAE4000_C__, typeof(string)));
C__ = new GroupAttribute("C__", 50, listC__);
#endregion C__
#region D__
List<PropertiesAttribute> listD__ = new List<PropertiesAttribute>();
listD__.Add(new PropertiesAttribute(8, 10, EnumGroupID.VAE4000_D__, typeof(DateTime)));
listD__.Add(new PropertiesAttribute(120, 10, EnumGroupID.VAE4000_T__, typeof(string)));
listD__.Add(new PropertiesAttribute(420, 10, EnumGroupID.VAE4000_I__, typeof(string)));
D__ = new GroupAttribute("D__", 10, listD__);
#endregion D__
TongSoByte = 9235;
}

}public partial class EnumGroupID
    {
public static readonly string VAE4000_SS_ = "VAE4000_SS_";
public static readonly string VAE4000_SN_ = "VAE4000_SN_";
public static readonly string VAE4000_EA_ = "VAE4000_EA_";
public static readonly string VAE4000_EB_ = "VAE4000_EB_";
public static readonly string VAE4000_ST_ = "VAE4000_ST_";
public static readonly string VAE4000_AD_ = "VAE4000_AD_";
public static readonly string VAE4000_SD_ = "VAE4000_SD_";
public static readonly string VAE4000_VC_ = "VAE4000_VC_";
public static readonly string VAE4000_C__ = "VAE4000_C__";
public static readonly string VAE4000_D__ = "VAE4000_D__";
public static readonly string VAE4000_T__ = "VAE4000_T__";
public static readonly string VAE4000_I__ = "VAE4000_I__";
}

}
