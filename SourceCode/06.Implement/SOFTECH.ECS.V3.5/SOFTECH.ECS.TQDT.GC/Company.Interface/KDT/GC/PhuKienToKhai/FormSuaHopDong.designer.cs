﻿namespace Company.Interface.KDT.GC
{
    partial class FormSuaHopDong
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSuaHopDong));
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.clcNgayHDMoi = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayHDCu = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSoHDCu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHDMoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnLuu);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Size = new System.Drawing.Size(405, 265);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(327, 240);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(248, 239);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(72, 23);
            this.btnLuu.TabIndex = 10;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLuu.VisualStyleManager = this.vsmMain;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(6, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Ngày hoàn hợp đồng";
            // 
            // clcNgayHDMoi
            // 
            this.clcNgayHDMoi.Location = new System.Drawing.Point(120, 58);
            this.clcNgayHDMoi.Name = "clcNgayHDMoi";
            this.clcNgayHDMoi.ReadOnly = false;
            this.clcNgayHDMoi.Size = new System.Drawing.Size(130, 21);
            this.clcNgayHDMoi.TabIndex = 11;
            this.clcNgayHDMoi.TagName = "";
            this.clcNgayHDMoi.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHDMoi.WhereCondition = "";
            // 
            // clcNgayHDCu
            // 
            this.clcNgayHDCu.Location = new System.Drawing.Point(120, 63);
            this.clcNgayHDCu.Name = "clcNgayHDCu";
            this.clcNgayHDCu.ReadOnly = false;
            this.clcNgayHDCu.Size = new System.Drawing.Size(130, 21);
            this.clcNgayHDCu.TabIndex = 11;
            this.clcNgayHDCu.TagName = "";
            this.clcNgayHDCu.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHDCu.WhereCondition = "";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtSoHDCu);
            this.uiGroupBox1.Controls.Add(this.clcNgayHDCu);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(387, 100);
            this.uiGroupBox1.TabIndex = 18;
            this.uiGroupBox1.Text = "Hợp đồng củ";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtSoHDMoi);
            this.uiGroupBox2.Controls.Add(this.clcNgayHDMoi);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 131);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(387, 94);
            this.uiGroupBox2.TabIndex = 18;
            this.uiGroupBox2.Text = "Hợp đồng mới";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(6, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Số hợp đồng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(6, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Số hợp đồng";
            // 
            // txtSoHDCu
            // 
            this.txtSoHDCu.BackColor = System.Drawing.Color.White;
            this.txtSoHDCu.Location = new System.Drawing.Point(120, 29);
            this.txtSoHDCu.Name = "txtSoHDCu";
            this.txtSoHDCu.Size = new System.Drawing.Size(249, 21);
            this.txtSoHDCu.TabIndex = 13;
            this.txtSoHDCu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHDCu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHDCu.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHDMoi
            // 
            this.txtSoHDMoi.BackColor = System.Drawing.Color.White;
            this.txtSoHDMoi.Location = new System.Drawing.Point(120, 31);
            this.txtSoHDMoi.Name = "txtSoHDMoi";
            this.txtSoHDMoi.Size = new System.Drawing.Size(249, 21);
            this.txtSoHDMoi.TabIndex = 13;
            this.txtSoHDMoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHDMoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(6, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ngày hoàn hợp đồng";
            // 
            // FormSuaHopDong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 265);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormSuaHopDong";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Phụ kiện sửa thông tin hợp đồng của tờ khai";
            this.Load += new System.EventHandler(this.FormNgayHoanThanh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHDMoi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHDCu;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHDMoi;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHDCu;

    }
}