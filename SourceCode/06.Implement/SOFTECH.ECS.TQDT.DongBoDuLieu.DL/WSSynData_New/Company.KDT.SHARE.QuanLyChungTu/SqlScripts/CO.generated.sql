-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_CO_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_CO_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CO_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Insert]
	@SoCO varchar(100),
	@NgayCO datetime,
	@ToChucCap nvarchar(500),
	@NuocCapCO varchar(3),
	@MaNuocXKTrenCO varchar(3),
	@MaNuocNKTrenCO varchar(3),
	@TenDiaChiNguoiXK nvarchar(500),
	@TenDiaChiNguoiNK nvarchar(500),
	@LoaiCO varchar(50),
	@ThongTinMoTaChiTiet nvarchar(max),
	@MaDoanhNghiep varchar(50),
	@NguoiKy nvarchar(500),
	@NoCo int,
	@ThoiHanNop datetime,
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@NgayHetHan datetime,
	@NgayKhoiHanh datetime,
	@CangXepHang nvarchar(10),
	@CangDoHang nvarchar(10),
	@MaNguoiXK varchar(50),
	@MaNguoiNK varchar(50),
	@HamLuongXuatXu float,
	@MoTaHangHoa nvarchar(max),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_CO]
(
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[NgayHetHan],
	[NgayKhoiHanh],
	[CangXepHang],
	[CangDoHang],
	[MaNguoiXK],
	[MaNguoiNK],
	[HamLuongXuatXu],
	[MoTaHangHoa]
)
VALUES 
(
	@SoCO,
	@NgayCO,
	@ToChucCap,
	@NuocCapCO,
	@MaNuocXKTrenCO,
	@MaNuocNKTrenCO,
	@TenDiaChiNguoiXK,
	@TenDiaChiNguoiNK,
	@LoaiCO,
	@ThongTinMoTaChiTiet,
	@MaDoanhNghiep,
	@NguoiKy,
	@NoCo,
	@ThoiHanNop,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@NgayHetHan,
	@NgayKhoiHanh,
	@CangXepHang,
	@CangDoHang,
	@MaNguoiXK,
	@MaNguoiNK,
	@HamLuongXuatXu,
	@MoTaHangHoa
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Update]
	@ID bigint,
	@SoCO varchar(100),
	@NgayCO datetime,
	@ToChucCap nvarchar(500),
	@NuocCapCO varchar(3),
	@MaNuocXKTrenCO varchar(3),
	@MaNuocNKTrenCO varchar(3),
	@TenDiaChiNguoiXK nvarchar(500),
	@TenDiaChiNguoiNK nvarchar(500),
	@LoaiCO varchar(50),
	@ThongTinMoTaChiTiet nvarchar(max),
	@MaDoanhNghiep varchar(50),
	@NguoiKy nvarchar(500),
	@NoCo int,
	@ThoiHanNop datetime,
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@NgayHetHan datetime,
	@NgayKhoiHanh datetime,
	@CangXepHang nvarchar(10),
	@CangDoHang nvarchar(10),
	@MaNguoiXK varchar(50),
	@MaNguoiNK varchar(50),
	@HamLuongXuatXu float,
	@MoTaHangHoa nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_CO]
SET
	[SoCO] = @SoCO,
	[NgayCO] = @NgayCO,
	[ToChucCap] = @ToChucCap,
	[NuocCapCO] = @NuocCapCO,
	[MaNuocXKTrenCO] = @MaNuocXKTrenCO,
	[MaNuocNKTrenCO] = @MaNuocNKTrenCO,
	[TenDiaChiNguoiXK] = @TenDiaChiNguoiXK,
	[TenDiaChiNguoiNK] = @TenDiaChiNguoiNK,
	[LoaiCO] = @LoaiCO,
	[ThongTinMoTaChiTiet] = @ThongTinMoTaChiTiet,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NguoiKy] = @NguoiKy,
	[NoCo] = @NoCo,
	[ThoiHanNop] = @ThoiHanNop,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[NgayHetHan] = @NgayHetHan,
	[NgayKhoiHanh] = @NgayKhoiHanh,
	[CangXepHang] = @CangXepHang,
	[CangDoHang] = @CangDoHang,
	[MaNguoiXK] = @MaNguoiXK,
	[MaNguoiNK] = @MaNguoiNK,
	[HamLuongXuatXu] = @HamLuongXuatXu,
	[MoTaHangHoa] = @MoTaHangHoa
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_InsertUpdate]
	@ID bigint,
	@SoCO varchar(100),
	@NgayCO datetime,
	@ToChucCap nvarchar(500),
	@NuocCapCO varchar(3),
	@MaNuocXKTrenCO varchar(3),
	@MaNuocNKTrenCO varchar(3),
	@TenDiaChiNguoiXK nvarchar(500),
	@TenDiaChiNguoiNK nvarchar(500),
	@LoaiCO varchar(50),
	@ThongTinMoTaChiTiet nvarchar(max),
	@MaDoanhNghiep varchar(50),
	@NguoiKy nvarchar(500),
	@NoCo int,
	@ThoiHanNop datetime,
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@NgayHetHan datetime,
	@NgayKhoiHanh datetime,
	@CangXepHang nvarchar(10),
	@CangDoHang nvarchar(10),
	@MaNguoiXK varchar(50),
	@MaNguoiNK varchar(50),
	@HamLuongXuatXu float,
	@MoTaHangHoa nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CO] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CO] 
		SET
			[SoCO] = @SoCO,
			[NgayCO] = @NgayCO,
			[ToChucCap] = @ToChucCap,
			[NuocCapCO] = @NuocCapCO,
			[MaNuocXKTrenCO] = @MaNuocXKTrenCO,
			[MaNuocNKTrenCO] = @MaNuocNKTrenCO,
			[TenDiaChiNguoiXK] = @TenDiaChiNguoiXK,
			[TenDiaChiNguoiNK] = @TenDiaChiNguoiNK,
			[LoaiCO] = @LoaiCO,
			[ThongTinMoTaChiTiet] = @ThongTinMoTaChiTiet,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NguoiKy] = @NguoiKy,
			[NoCo] = @NoCo,
			[ThoiHanNop] = @ThoiHanNop,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[NgayHetHan] = @NgayHetHan,
			[NgayKhoiHanh] = @NgayKhoiHanh,
			[CangXepHang] = @CangXepHang,
			[CangDoHang] = @CangDoHang,
			[MaNguoiXK] = @MaNguoiXK,
			[MaNguoiNK] = @MaNguoiNK,
			[HamLuongXuatXu] = @HamLuongXuatXu,
			[MoTaHangHoa] = @MoTaHangHoa
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CO]
		(
			[SoCO],
			[NgayCO],
			[ToChucCap],
			[NuocCapCO],
			[MaNuocXKTrenCO],
			[MaNuocNKTrenCO],
			[TenDiaChiNguoiXK],
			[TenDiaChiNguoiNK],
			[LoaiCO],
			[ThongTinMoTaChiTiet],
			[MaDoanhNghiep],
			[NguoiKy],
			[NoCo],
			[ThoiHanNop],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[NgayHetHan],
			[NgayKhoiHanh],
			[CangXepHang],
			[CangDoHang],
			[MaNguoiXK],
			[MaNguoiNK],
			[HamLuongXuatXu],
			[MoTaHangHoa]
		)
		VALUES 
		(
			@SoCO,
			@NgayCO,
			@ToChucCap,
			@NuocCapCO,
			@MaNuocXKTrenCO,
			@MaNuocNKTrenCO,
			@TenDiaChiNguoiXK,
			@TenDiaChiNguoiNK,
			@LoaiCO,
			@ThongTinMoTaChiTiet,
			@MaDoanhNghiep,
			@NguoiKy,
			@NoCo,
			@ThoiHanNop,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@NgayHetHan,
			@NgayKhoiHanh,
			@CangXepHang,
			@CangDoHang,
			@MaNguoiXK,
			@MaNguoiNK,
			@HamLuongXuatXu,
			@MoTaHangHoa
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CO]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_CO]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[NgayHetHan],
	[NgayKhoiHanh],
	[CangXepHang],
	[CangDoHang],
	[MaNguoiXK],
	[MaNguoiNK],
	[HamLuongXuatXu],
	[MoTaHangHoa]
FROM
	[dbo].[t_KDT_CO]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[NgayHetHan],
	[NgayKhoiHanh],
	[CangXepHang],
	[CangDoHang],
	[MaNguoiXK],
	[MaNguoiNK],
	[HamLuongXuatXu],
	[MoTaHangHoa]
FROM
	[dbo].[t_KDT_CO]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[NgayHetHan],
	[NgayKhoiHanh],
	[CangXepHang],
	[CangDoHang],
	[MaNguoiXK],
	[MaNguoiNK],
	[HamLuongXuatXu],
	[MoTaHangHoa]
FROM [dbo].[t_KDT_CO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CO_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 29, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CO_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoCO],
	[NgayCO],
	[ToChucCap],
	[NuocCapCO],
	[MaNuocXKTrenCO],
	[MaNuocNKTrenCO],
	[TenDiaChiNguoiXK],
	[TenDiaChiNguoiNK],
	[LoaiCO],
	[ThongTinMoTaChiTiet],
	[MaDoanhNghiep],
	[NguoiKy],
	[NoCo],
	[ThoiHanNop],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[NgayHetHan],
	[NgayKhoiHanh],
	[CangXepHang],
	[CangDoHang],
	[MaNguoiXK],
	[MaNguoiNK],
	[HamLuongXuatXu],
	[MoTaHangHoa]
FROM
	[dbo].[t_KDT_CO]	

GO

