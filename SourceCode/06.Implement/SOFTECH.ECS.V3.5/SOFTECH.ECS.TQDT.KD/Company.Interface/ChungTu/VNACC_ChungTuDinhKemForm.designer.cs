﻿namespace Company.Interface
{
    partial class VNACC_ChungTuDinhKemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChungTuDinhKemForm));
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTieuDe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.btnXemFile = new Janus.Windows.EditControls.UIButton();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.btnKetQuaXuLy = new Janus.Windows.EditControls.UIButton();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lbTrangThai = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.btnXoaChungTu = new Janus.Windows.EditControls.UIButton();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.lblLuuY = new DevExpress.XtraEditors.LabelControl();
            this.lblTongDungLuong = new DevExpress.XtraEditors.LabelControl();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageMSB = new Janus.Windows.UI.Tab.UITabPage();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNhomXuLyHS = new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy();
            this.ucCoQuanHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiTabPageHYS = new Janus.Windows.UI.Tab.UITabPage();
            this.ucPhanLoaiThuTucKhaiBao = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.txtGhiChu_HYS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoQuanLyTrongNoiBoDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiNguoiKhaiBao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucNhomXuLyHS_HYS = new Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy();
            this.ucCoQuanHaiQuan_HYS = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.uiTabPageFile = new Janus.Windows.UI.Tab.UITabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblLinkExcelTemplate = new System.Windows.Forms.LinkLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdSuaChungTu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaChungTu");
            this.cmdPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdPhanHoi");
            this.cmdSuaChungTu = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaChungTu");
            this.cmdTTHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdTTHQ");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenNguoiKhaiBao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucTrangThaiKhaiBao = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label7 = new System.Windows.Forms.Label();
            this.dtNgayHoanThanhKiemTraHoSo = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label16 = new System.Windows.Forms.Label();
            this.dtNgaySuaCuoiCung = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label18 = new System.Windows.Forms.Label();
            this.dtNgayKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtGhiChuHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDiaChiNguoiKhaiBao = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSoToKhai_HYS = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoDeLayTepDinhKem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageMSB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.uiTabPageHYS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.uiTabPageFile.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 674), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 674);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 650);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 650);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.panel2);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(784, 674);
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(119, 190);
            this.txtGhiChu.MaxLength = 996;
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChu.Size = new System.Drawing.Size(539, 69);
            this.txtGhiChu.TabIndex = 3;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Số tờ khai";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Ghi chú";
            // 
            // txtTieuDe
            // 
            this.txtTieuDe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTieuDe.Location = new System.Drawing.Point(119, 97);
            this.txtTieuDe.MaxLength = 210;
            this.txtTieuDe.Multiline = true;
            this.txtTieuDe.Name = "txtTieuDe";
            this.txtTieuDe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtTieuDe.Size = new System.Drawing.Size(539, 44);
            this.txtTieuDe.TabIndex = 1;
            this.txtTieuDe.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTieuDe.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(17, 113);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(42, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Tiêu đề";
            // 
            // dgList
            // 
            this.dgList.AlternatingColors = true;
            this.dgList.AutoEdit = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(782, 496);
            this.dgList.TabIndex = 0;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(591, 31);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 1;
            this.btnXoa.Text = "Xoá file";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(686, 8);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(86, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Image = ((System.Drawing.Image)(resources.GetObject("btnAddNew.Image")));
            this.btnAddNew.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddNew.Location = new System.Drawing.Point(476, 31);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(109, 23);
            this.btnAddNew.TabIndex = 0;
            this.btnAddNew.Text = "Thêm file";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Image = ((System.Drawing.Image)(resources.GetObject("btnGhi.Image")));
            this.btnGhi.ImageSize = new System.Drawing.Size(20, 20);
            this.btnGhi.Location = new System.Drawing.Point(477, 8);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(88, 23);
            this.btnGhi.TabIndex = 1;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // btnXemFile
            // 
            this.btnXemFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXemFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXemFile.Image = ((System.Drawing.Image)(resources.GetObject("btnXemFile.Image")));
            this.btnXemFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXemFile.Location = new System.Drawing.Point(685, 31);
            this.btnXemFile.Name = "btnXemFile";
            this.btnXemFile.Size = new System.Drawing.Size(86, 23);
            this.btnXemFile.TabIndex = 2;
            this.btnXemFile.Text = "Xem file";
            this.btnXemFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXemFile.Click += new System.EventHandler(this.btnXemFile_Click);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXuLy);
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.lbTrangThai);
            this.grpTiepNhan.Controls.Add(this.label10);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Dock = System.Windows.Forms.DockStyle.Top;
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(0, 0);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(784, 51);
            this.grpTiepNhan.TabIndex = 0;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnKetQuaXuLy
            // 
            this.btnKetQuaXuLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKetQuaXuLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXuLy.Image = ((System.Drawing.Image)(resources.GetObject("btnKetQuaXuLy.Image")));
            this.btnKetQuaXuLy.ImageSize = new System.Drawing.Size(20, 20);
            this.btnKetQuaXuLy.Location = new System.Drawing.Point(590, 17);
            this.btnKetQuaXuLy.Name = "btnKetQuaXuLy";
            this.btnKetQuaXuLy.Size = new System.Drawing.Size(109, 23);
            this.btnKetQuaXuLy.TabIndex = 2;
            this.btnKetQuaXuLy.Text = "Kết quả xử lý";
            this.btnKetQuaXuLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnKetQuaXuLy.Click += new System.EventHandler(this.btnKetQuaXuLy_Click);
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(350, 18);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(94, 21);
            this.ccNgayTiepNhan.TabIndex = 1;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 18);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(138, 21);
            this.txtSoTiepNhan.TabIndex = 0;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lbTrangThai
            // 
            this.lbTrangThai.AutoSize = true;
            this.lbTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTrangThai.Location = new System.Drawing.Point(463, 23);
            this.lbTrangThai.Name = "lbTrangThai";
            this.lbTrangThai.Size = new System.Drawing.Size(56, 13);
            this.lbTrangThai.TabIndex = 2;
            this.lbTrangThai.Text = "Trạng thái";
            this.lbTrangThai.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(265, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Ngày tiếp nhận";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(21, 22);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Số tiếp nhận";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // btnXoaChungTu
            // 
            this.btnXoaChungTu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoaChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaChungTu.Image = ((System.Drawing.Image)(resources.GetObject("btnXoaChungTu.Image")));
            this.btnXoaChungTu.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoaChungTu.Location = new System.Drawing.Point(571, 8);
            this.btnXoaChungTu.Name = "btnXoaChungTu";
            this.btnXoaChungTu.Size = new System.Drawing.Size(109, 23);
            this.btnXoaChungTu.TabIndex = 0;
            this.btnXoaChungTu.Text = "Xoá chứng từ";
            this.btnXoaChungTu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoaChungTu.Click += new System.EventHandler(this.btnXoaChungTu_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.labelControl1.Location = new System.Drawing.Point(5, 6);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(97, 13);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "Tổng dung lượng:";
            // 
            // labelControl2
            // 
            this.labelControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.labelControl2.Location = new System.Drawing.Point(5, 25);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(293, 13);
            this.labelControl2.TabIndex = 3;
            this.labelControl2.Text = "Lưu ý: Tổng dung lượng các file đính kèm không quá ";
            // 
            // lblLuuY
            // 
            this.lblLuuY.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLuuY.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lblLuuY.Appearance.ForeColor = System.Drawing.Color.Brown;
            this.lblLuuY.Location = new System.Drawing.Point(303, 25);
            this.lblLuuY.Name = "lblLuuY";
            this.lblLuuY.Size = new System.Drawing.Size(27, 13);
            this.lblLuuY.TabIndex = 4;
            this.lblLuuY.Text = "2 MB";
            // 
            // lblTongDungLuong
            // 
            this.lblTongDungLuong.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTongDungLuong.Appearance.ForeColor = System.Drawing.Color.Blue;
            this.lblTongDungLuong.Location = new System.Drawing.Point(117, 6);
            this.lblTongDungLuong.Name = "lblTongDungLuong";
            this.lblTongDungLuong.Size = new System.Drawing.Size(70, 13);
            this.lblTongDungLuong.TabIndex = 1;
            this.lblTongDungLuong.Text = "0 MB (0 Bytes)";
            // 
            // uiTab1
            // 
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 51);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(784, 584);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageMSB,
            this.uiTabPageHYS,
            this.uiTabPageFile});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            // 
            // uiTabPageMSB
            // 
            this.uiTabPageMSB.Controls.Add(this.txtSoToKhai);
            this.uiTabPageMSB.Controls.Add(this.uiGroupBox1);
            this.uiTabPageMSB.Controls.Add(this.txtGhiChu);
            this.uiTabPageMSB.Controls.Add(this.label8);
            this.uiTabPageMSB.Controls.Add(this.txtTieuDe);
            this.uiTabPageMSB.Controls.Add(this.label1);
            this.uiTabPageMSB.Controls.Add(this.label27);
            this.uiTabPageMSB.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageMSB.Name = "uiTabPageMSB";
            this.uiTabPageMSB.Size = new System.Drawing.Size(782, 562);
            this.uiTabPageMSB.TabStop = true;
            this.uiTabPageMSB.Text = "Thông tin chung (MSB)";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.DecimalDigits = 0;
            this.txtSoToKhai.Location = new System.Drawing.Point(119, 154);
            this.txtSoToKhai.MaxLength = 12;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(139, 21);
            this.txtSoToKhai.TabIndex = 2;
            this.txtSoToKhai.Text = "0";
            this.txtSoToKhai.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ctrNhomXuLyHS);
            this.uiGroupBox1.Controls.Add(this.ucCoQuanHaiQuan);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(782, 83);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Kiểm soát nơi đến";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ctrNhomXuLyHS
            // 
            this.ctrNhomXuLyHS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ctrNhomXuLyHS.Code = "";
            this.ctrNhomXuLyHS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrNhomXuLyHS.CustomsCode = null;
            this.ctrNhomXuLyHS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrNhomXuLyHS.IsOnlyWarning = true;
            this.ctrNhomXuLyHS.IsValidate = true;
            this.ctrNhomXuLyHS.Location = new System.Drawing.Point(111, 50);
            this.ctrNhomXuLyHS.Name = "ctrNhomXuLyHS";
            this.ctrNhomXuLyHS.Name_VN = "";
            this.ctrNhomXuLyHS.SetValidate = false;
            this.ctrNhomXuLyHS.ShowColumnCode = true;
            this.ctrNhomXuLyHS.ShowColumnName = false;
            this.ctrNhomXuLyHS.Size = new System.Drawing.Size(165, 21);
            this.ctrNhomXuLyHS.TabIndex = 9;
            this.ctrNhomXuLyHS.TagName = "";
            this.ctrNhomXuLyHS.WhereCondition = "";
            // 
            // ucCoQuanHaiQuan
            // 
            this.ucCoQuanHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ucCoQuanHaiQuan.Code = "";
            this.ucCoQuanHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCoQuanHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCoQuanHaiQuan.IsOnlyWarning = true;
            this.ucCoQuanHaiQuan.IsValidate = true;
            this.ucCoQuanHaiQuan.Location = new System.Drawing.Point(111, 18);
            this.ucCoQuanHaiQuan.Name = "ucCoQuanHaiQuan";
            this.ucCoQuanHaiQuan.Name_VN = "";
            this.ucCoQuanHaiQuan.SetValidate = false;
            this.ucCoQuanHaiQuan.ShowColumnCode = true;
            this.ucCoQuanHaiQuan.ShowColumnName = true;
            this.ucCoQuanHaiQuan.Size = new System.Drawing.Size(324, 26);
            this.ucCoQuanHaiQuan.TabIndex = 0;
            this.ucCoQuanHaiQuan.TagCode = "";
            this.ucCoQuanHaiQuan.TagName = "";
            this.ucCoQuanHaiQuan.Where = null;
            this.ucCoQuanHaiQuan.WhereCondition = "";
            this.ucCoQuanHaiQuan.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            this.ucCoQuanHaiQuan.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCoQuanHaiQuan_EditValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Nhóm xử lý hồ sơ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Cơ quan Hải quan";
            // 
            // uiTabPageHYS
            // 
            this.uiTabPageHYS.AutoScroll = true;
            this.uiTabPageHYS.Controls.Add(this.uiGroupBox3);
            this.uiTabPageHYS.Controls.Add(this.uiGroupBox4);
            this.uiTabPageHYS.Controls.Add(this.uiGroupBox2);
            this.uiTabPageHYS.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageHYS.Name = "uiTabPageHYS";
            this.uiTabPageHYS.Size = new System.Drawing.Size(782, 562);
            this.uiTabPageHYS.TabStop = true;
            this.uiTabPageHYS.Text = "Thông tin chung (HYS)";
            // 
            // ucPhanLoaiThuTucKhaiBao
            // 
            this.ucPhanLoaiThuTucKhaiBao.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhanLoaiThuTucKhaiBao.Appearance.Options.UseBackColor = true;
            this.ucPhanLoaiThuTucKhaiBao.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A546;
            this.ucPhanLoaiThuTucKhaiBao.Code = "";
            this.ucPhanLoaiThuTucKhaiBao.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhanLoaiThuTucKhaiBao.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhanLoaiThuTucKhaiBao.IsOnlyWarning = true;
            this.ucPhanLoaiThuTucKhaiBao.IsValidate = true;
            this.ucPhanLoaiThuTucKhaiBao.Location = new System.Drawing.Point(179, 27);
            this.ucPhanLoaiThuTucKhaiBao.Name = "ucPhanLoaiThuTucKhaiBao";
            this.ucPhanLoaiThuTucKhaiBao.Name_VN = "";
            this.ucPhanLoaiThuTucKhaiBao.SetValidate = false;
            this.ucPhanLoaiThuTucKhaiBao.ShowColumnCode = true;
            this.ucPhanLoaiThuTucKhaiBao.ShowColumnName = true;
            this.ucPhanLoaiThuTucKhaiBao.Size = new System.Drawing.Size(354, 26);
            this.ucPhanLoaiThuTucKhaiBao.TabIndex = 5;
            this.ucPhanLoaiThuTucKhaiBao.TagCode = "";
            this.ucPhanLoaiThuTucKhaiBao.TagName = "";
            this.ucPhanLoaiThuTucKhaiBao.Where = null;
            this.ucPhanLoaiThuTucKhaiBao.WhereCondition = "";
            this.ucPhanLoaiThuTucKhaiBao.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtGhiChu_HYS
            // 
            this.txtGhiChu_HYS.Location = new System.Drawing.Point(179, 138);
            this.txtGhiChu_HYS.MaxLength = 996;
            this.txtGhiChu_HYS.Multiline = true;
            this.txtGhiChu_HYS.Name = "txtGhiChu_HYS";
            this.txtGhiChu_HYS.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChu_HYS.Size = new System.Drawing.Size(511, 60);
            this.txtGhiChu_HYS.TabIndex = 8;
            this.txtGhiChu_HYS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoQuanLyTrongNoiBoDoanhNghiep
            // 
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.Location = new System.Drawing.Point(179, 103);
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.Name = "txtSoQuanLyTrongNoiBoDoanhNghiep";
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.Size = new System.Drawing.Size(356, 21);
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.TabIndex = 7;
            this.txtSoQuanLyTrongNoiBoDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDienThoaiNguoiKhaiBao
            // 
            this.txtSoDienThoaiNguoiKhaiBao.Location = new System.Drawing.Point(179, 65);
            this.txtSoDienThoaiNguoiKhaiBao.Name = "txtSoDienThoaiNguoiKhaiBao";
            this.txtSoDienThoaiNguoiKhaiBao.Size = new System.Drawing.Size(121, 21);
            this.txtSoDienThoaiNguoiKhaiBao.TabIndex = 6;
            this.txtSoDienThoaiNguoiKhaiBao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 108);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(118, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "Số quản lý trong nội bộ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 69);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(142, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Số điện thoại người khai báo";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(13, 162);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(42, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "Ghi chú";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ucNhomXuLyHS_HYS);
            this.uiGroupBox2.Controls.Add(this.ucCoQuanHaiQuan_HYS);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(782, 87);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Kiểm soát nơi đến";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // ucNhomXuLyHS_HYS
            // 
            this.ucNhomXuLyHS_HYS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ucNhomXuLyHS_HYS.Code = "";
            this.ucNhomXuLyHS_HYS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucNhomXuLyHS_HYS.CustomsCode = null;
            this.ucNhomXuLyHS_HYS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucNhomXuLyHS_HYS.IsOnlyWarning = true;
            this.ucNhomXuLyHS_HYS.IsValidate = true;
            this.ucNhomXuLyHS_HYS.Location = new System.Drawing.Point(179, 50);
            this.ucNhomXuLyHS_HYS.Name = "ucNhomXuLyHS_HYS";
            this.ucNhomXuLyHS_HYS.Name_VN = "";
            this.ucNhomXuLyHS_HYS.SetValidate = false;
            this.ucNhomXuLyHS_HYS.ShowColumnCode = true;
            this.ucNhomXuLyHS_HYS.ShowColumnName = true;
            this.ucNhomXuLyHS_HYS.Size = new System.Drawing.Size(165, 21);
            this.ucNhomXuLyHS_HYS.TabIndex = 9;
            this.ucNhomXuLyHS_HYS.TagName = "";
            this.ucNhomXuLyHS_HYS.WhereCondition = "";
            // 
            // ucCoQuanHaiQuan_HYS
            // 
            this.ucCoQuanHaiQuan_HYS.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ucCoQuanHaiQuan_HYS.Code = "";
            this.ucCoQuanHaiQuan_HYS.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCoQuanHaiQuan_HYS.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCoQuanHaiQuan_HYS.IsOnlyWarning = true;
            this.ucCoQuanHaiQuan_HYS.IsValidate = true;
            this.ucCoQuanHaiQuan_HYS.Location = new System.Drawing.Point(179, 19);
            this.ucCoQuanHaiQuan_HYS.Name = "ucCoQuanHaiQuan_HYS";
            this.ucCoQuanHaiQuan_HYS.Name_VN = "";
            this.ucCoQuanHaiQuan_HYS.SetValidate = false;
            this.ucCoQuanHaiQuan_HYS.ShowColumnCode = true;
            this.ucCoQuanHaiQuan_HYS.ShowColumnName = true;
            this.ucCoQuanHaiQuan_HYS.Size = new System.Drawing.Size(356, 26);
            this.ucCoQuanHaiQuan_HYS.TabIndex = 0;
            this.ucCoQuanHaiQuan_HYS.TagCode = "";
            this.ucCoQuanHaiQuan_HYS.TagName = "";
            this.ucCoQuanHaiQuan_HYS.Where = null;
            this.ucCoQuanHaiQuan_HYS.WhereCondition = "";
            this.ucCoQuanHaiQuan_HYS.Leave += new System.EventHandler(this.ctrCoQuanHaiQuan_Leave);
            this.ucCoQuanHaiQuan_HYS.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nhóm xử lý hồ sơ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Cơ quan Hải quan";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(130, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Phân loại thủ tục khai báo";
            // 
            // uiTabPageFile
            // 
            this.uiTabPageFile.Controls.Add(this.dgList);
            this.uiTabPageFile.Controls.Add(this.panel1);
            this.uiTabPageFile.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageFile.Name = "uiTabPageFile";
            this.uiTabPageFile.Size = new System.Drawing.Size(782, 562);
            this.uiTabPageFile.TabStop = true;
            this.uiTabPageFile.Text = "Danh sách tệp tin đính kèm";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.lblLinkExcelTemplate);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Controls.Add(this.lblTongDungLuong);
            this.panel1.Controls.Add(this.labelControl1);
            this.panel1.Controls.Add(this.lblLuuY);
            this.panel1.Controls.Add(this.btnXemFile);
            this.panel1.Controls.Add(this.btnAddNew);
            this.panel1.Controls.Add(this.btnXoa);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 496);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(782, 66);
            this.panel1.TabIndex = 1;
            // 
            // lblLinkExcelTemplate
            // 
            this.lblLinkExcelTemplate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblLinkExcelTemplate.AutoSize = true;
            this.lblLinkExcelTemplate.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkExcelTemplate.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkExcelTemplate.Location = new System.Drawing.Point(487, 3);
            this.lblLinkExcelTemplate.Name = "lblLinkExcelTemplate";
            this.lblLinkExcelTemplate.Size = new System.Drawing.Size(290, 15);
            this.lblLinkExcelTemplate.TabIndex = 29;
            this.lblLinkExcelTemplate.TabStop = true;
            this.lblLinkExcelTemplate.Text = "Mở tệp tin mẫu Excel : Danh sách container đính kèm";
            this.lblLinkExcelTemplate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLinkExcelTemplate_LinkClicked);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnGhi);
            this.panel2.Controls.Add(this.btnXoaChungTu);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 635);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(784, 39);
            this.panel2.TabIndex = 1;
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao,
            this.cmdPhanHoi,
            this.cmdSuaChungTu,
            this.cmdTTHQ});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("64392c4c-08e0-49a5-ab35-32992c7e6756");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao1,
            this.cmdSuaChungTu1,
            this.cmdPhanHoi1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(324, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdSuaChungTu1
            // 
            this.cmdSuaChungTu1.Key = "cmdSuaChungTu";
            this.cmdSuaChungTu1.Name = "cmdSuaChungTu1";
            // 
            // cmdPhanHoi1
            // 
            this.cmdPhanHoi1.Key = "cmdPhanHoi";
            this.cmdPhanHoi1.Name = "cmdPhanHoi1";
            this.cmdPhanHoi1.Text = "Thông tin Hải Quan";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdPhanHoi
            // 
            this.cmdPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdPhanHoi.Image")));
            this.cmdPhanHoi.Key = "cmdPhanHoi";
            this.cmdPhanHoi.Name = "cmdPhanHoi";
            this.cmdPhanHoi.Text = "Thông tin phản hồi";
            // 
            // cmdSuaChungTu
            // 
            this.cmdSuaChungTu.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuaChungTu.Image")));
            this.cmdSuaChungTu.Key = "cmdSuaChungTu";
            this.cmdSuaChungTu.Name = "cmdSuaChungTu";
            this.cmdSuaChungTu.Text = "Sửa chứng từ";
            // 
            // cmdTTHQ
            // 
            this.cmdTTHQ.Image = ((System.Drawing.Image)(resources.GetObject("cmdTTHQ.Image")));
            this.cmdTTHQ.Key = "cmdTTHQ";
            this.cmdTTHQ.Name = "cmdTTHQ";
            this.cmdTTHQ.Text = "Thông tin Hải Quan";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(990, 28);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtSoDienThoaiNguoiKhaiBao);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.ucPhanLoaiThuTucKhaiBao);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtGhiChu_HYS);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.txtSoQuanLyTrongNoiBoDoanhNghiep);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(0, 87);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(782, 213);
            this.uiGroupBox4.TabIndex = 15;
            this.uiGroupBox4.Text = "Thông tin chung";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtTenNguoiKhaiBao);
            this.uiGroupBox3.Controls.Add(this.ucTrangThaiKhaiBao);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.dtNgayHoanThanhKiemTraHoSo);
            this.uiGroupBox3.Controls.Add(this.label16);
            this.uiGroupBox3.Controls.Add(this.dtNgaySuaCuoiCung);
            this.uiGroupBox3.Controls.Add(this.label18);
            this.uiGroupBox3.Controls.Add(this.dtNgayKhaiBao);
            this.uiGroupBox3.Controls.Add(this.label17);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.txtGhiChuHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label14);
            this.uiGroupBox3.Controls.Add(this.label19);
            this.uiGroupBox3.Controls.Add(this.txtDiaChiNguoiKhaiBao);
            this.uiGroupBox3.Controls.Add(this.label22);
            this.uiGroupBox3.Controls.Add(this.txtSoToKhai_HYS);
            this.uiGroupBox3.Controls.Add(this.txtSoDeLayTepDinhKem);
            this.uiGroupBox3.Controls.Add(this.label20);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 300);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(782, 262);
            this.uiGroupBox3.TabIndex = 16;
            this.uiGroupBox3.Text = "Thông tin tra cứu";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtTenNguoiKhaiBao
            // 
            this.txtTenNguoiKhaiBao.Enabled = false;
            this.txtTenNguoiKhaiBao.Location = new System.Drawing.Point(192, 43);
            this.txtTenNguoiKhaiBao.Name = "txtTenNguoiKhaiBao";
            this.txtTenNguoiKhaiBao.Size = new System.Drawing.Size(356, 21);
            this.txtTenNguoiKhaiBao.TabIndex = 0;
            this.txtTenNguoiKhaiBao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ucTrangThaiKhaiBao
            // 
            this.ucTrangThaiKhaiBao.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucTrangThaiKhaiBao.Appearance.Options.UseBackColor = true;
            this.ucTrangThaiKhaiBao.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.B524;
            this.ucTrangThaiKhaiBao.Code = "";
            this.ucTrangThaiKhaiBao.ColorControl = System.Drawing.Color.Empty;
            this.ucTrangThaiKhaiBao.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucTrangThaiKhaiBao.Enabled = false;
            this.ucTrangThaiKhaiBao.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucTrangThaiKhaiBao.IsOnlyWarning = false;
            this.ucTrangThaiKhaiBao.IsValidate = true;
            this.ucTrangThaiKhaiBao.Location = new System.Drawing.Point(192, 194);
            this.ucTrangThaiKhaiBao.Name = "ucTrangThaiKhaiBao";
            this.ucTrangThaiKhaiBao.Name_VN = "";
            this.ucTrangThaiKhaiBao.SetOnlyWarning = false;
            this.ucTrangThaiKhaiBao.SetValidate = false;
            this.ucTrangThaiKhaiBao.ShowColumnCode = true;
            this.ucTrangThaiKhaiBao.ShowColumnName = true;
            this.ucTrangThaiKhaiBao.Size = new System.Drawing.Size(121, 24);
            this.ucTrangThaiKhaiBao.TabIndex = 7;
            this.ucTrangThaiKhaiBao.TagName = "";
            this.ucTrangThaiKhaiBao.Where = null;
            this.ucTrangThaiKhaiBao.WhereCondition = "";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(30, 174);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(54, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Số tờ khai";
            // 
            // dtNgayHoanThanhKiemTraHoSo
            // 
            // 
            // 
            // 
            this.dtNgayHoanThanhKiemTraHoSo.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtNgayHoanThanhKiemTraHoSo.DropDownCalendar.Name = "";
            this.dtNgayHoanThanhKiemTraHoSo.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayHoanThanhKiemTraHoSo.Enabled = false;
            this.dtNgayHoanThanhKiemTraHoSo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayHoanThanhKiemTraHoSo.IsNullDate = true;
            this.dtNgayHoanThanhKiemTraHoSo.Location = new System.Drawing.Point(192, 99);
            this.dtNgayHoanThanhKiemTraHoSo.Name = "dtNgayHoanThanhKiemTraHoSo";
            this.dtNgayHoanThanhKiemTraHoSo.Nullable = true;
            this.dtNgayHoanThanhKiemTraHoSo.NullButtonText = "Xóa";
            this.dtNgayHoanThanhKiemTraHoSo.ShowNullButton = true;
            this.dtNgayHoanThanhKiemTraHoSo.Size = new System.Drawing.Size(94, 21);
            this.dtNgayHoanThanhKiemTraHoSo.TabIndex = 2;
            this.dtNgayHoanThanhKiemTraHoSo.TodayButtonText = "Hôm nay";
            this.dtNgayHoanThanhKiemTraHoSo.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Enabled = false;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(373, 174);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(75, 13);
            this.label16.TabIndex = 6;
            this.label16.Text = "Ngày khai báo";
            // 
            // dtNgaySuaCuoiCung
            // 
            // 
            // 
            // 
            this.dtNgaySuaCuoiCung.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtNgaySuaCuoiCung.DropDownCalendar.Name = "";
            this.dtNgaySuaCuoiCung.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgaySuaCuoiCung.Enabled = false;
            this.dtNgaySuaCuoiCung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgaySuaCuoiCung.IsNullDate = true;
            this.dtNgaySuaCuoiCung.Location = new System.Drawing.Point(454, 197);
            this.dtNgaySuaCuoiCung.Name = "dtNgaySuaCuoiCung";
            this.dtNgaySuaCuoiCung.Nullable = true;
            this.dtNgaySuaCuoiCung.NullButtonText = "Xóa";
            this.dtNgaySuaCuoiCung.ShowNullButton = true;
            this.dtNgaySuaCuoiCung.Size = new System.Drawing.Size(94, 21);
            this.dtNgaySuaCuoiCung.TabIndex = 8;
            this.dtNgaySuaCuoiCung.TodayButtonText = "Hôm nay";
            this.dtNgaySuaCuoiCung.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Enabled = false;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(347, 203);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(101, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Ngày sửa cuối cùng";
            // 
            // dtNgayKhaiBao
            // 
            // 
            // 
            // 
            this.dtNgayKhaiBao.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.dtNgayKhaiBao.DropDownCalendar.Name = "";
            this.dtNgayKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKhaiBao.Enabled = false;
            this.dtNgayKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayKhaiBao.IsNullDate = true;
            this.dtNgayKhaiBao.Location = new System.Drawing.Point(454, 170);
            this.dtNgayKhaiBao.Name = "dtNgayKhaiBao";
            this.dtNgayKhaiBao.Nullable = true;
            this.dtNgayKhaiBao.NullButtonText = "Xóa";
            this.dtNgayKhaiBao.ShowNullButton = true;
            this.dtNgayKhaiBao.Size = new System.Drawing.Size(94, 21);
            this.dtNgayKhaiBao.TabIndex = 6;
            this.dtNgayKhaiBao.TodayButtonText = "Hôm nay";
            this.dtNgayKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(30, 203);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(99, 13);
            this.label17.TabIndex = 6;
            this.label17.Text = "Trạng thái khai báo";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(26, 104);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(160, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "Ngày hoàn thành kiểm tra hồ sơ";
            // 
            // txtGhiChuHaiQuan
            // 
            this.txtGhiChuHaiQuan.Enabled = false;
            this.txtGhiChuHaiQuan.Location = new System.Drawing.Point(192, 126);
            this.txtGhiChuHaiQuan.MaxLength = 996;
            this.txtGhiChuHaiQuan.Multiline = true;
            this.txtGhiChuHaiQuan.Name = "txtGhiChuHaiQuan";
            this.txtGhiChuHaiQuan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChuHaiQuan.Size = new System.Drawing.Size(511, 38);
            this.txtGhiChuHaiQuan.TabIndex = 4;
            this.txtGhiChuHaiQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(26, 48);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Tên người khai báo";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(26, 75);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Địa chỉ người khai báo";
            // 
            // txtDiaChiNguoiKhaiBao
            // 
            this.txtDiaChiNguoiKhaiBao.Enabled = false;
            this.txtDiaChiNguoiKhaiBao.Location = new System.Drawing.Point(192, 70);
            this.txtDiaChiNguoiKhaiBao.Name = "txtDiaChiNguoiKhaiBao";
            this.txtDiaChiNguoiKhaiBao.Size = new System.Drawing.Size(511, 21);
            this.txtDiaChiNguoiKhaiBao.TabIndex = 1;
            this.txtDiaChiNguoiKhaiBao.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(26, 132);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(87, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "Ghi chú Hải quan";
            // 
            // txtSoToKhai_HYS
            // 
            this.txtSoToKhai_HYS.DecimalDigits = 0;
            this.txtSoToKhai_HYS.Enabled = false;
            this.txtSoToKhai_HYS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai_HYS.Location = new System.Drawing.Point(192, 170);
            this.txtSoToKhai_HYS.MaxLength = 12;
            this.txtSoToKhai_HYS.Name = "txtSoToKhai_HYS";
            this.txtSoToKhai_HYS.Size = new System.Drawing.Size(121, 21);
            this.txtSoToKhai_HYS.TabIndex = 5;
            this.txtSoToKhai_HYS.Text = "0";
            this.txtSoToKhai_HYS.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtSoDeLayTepDinhKem
            // 
            this.txtSoDeLayTepDinhKem.DecimalDigits = 0;
            this.txtSoDeLayTepDinhKem.Enabled = false;
            this.txtSoDeLayTepDinhKem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoDeLayTepDinhKem.Location = new System.Drawing.Point(454, 99);
            this.txtSoDeLayTepDinhKem.MaxLength = 13;
            this.txtSoDeLayTepDinhKem.Name = "txtSoDeLayTepDinhKem";
            this.txtSoDeLayTepDinhKem.Size = new System.Drawing.Size(249, 21);
            this.txtSoDeLayTepDinhKem.TabIndex = 3;
            this.txtSoDeLayTepDinhKem.Text = "0";
            this.txtSoDeLayTepDinhKem.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoDeLayTepDinhKem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(318, 104);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(130, 13);
            this.label20.TabIndex = 6;
            this.label20.Text = "Số để lấy tệp tin đính kèm";
            // 
            // VNACC_ChungTuDinhKemForm
            // 
            this.AcceptButton = this.btnGhi;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(990, 708);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChungTuDinhKemForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin chứng từ kèm";
            this.Load += new System.EventHandler(this.VNACC_ChungTuDinhKemForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageMSB.ResumeLayout(false);
            this.uiTabPageMSB.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.uiTabPageHYS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.uiTabPageFile.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtTieuDe;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnAddNew;
        private Janus.Windows.EditControls.UIButton btnXemFile;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label lbTrangThai;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.EditControls.UIButton btnXoaChungTu;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl lblTongDungLuong;
        private DevExpress.XtraEditors.LabelControl lblLuuY;
        private Janus.Windows.EditControls.UIButton btnKetQuaXuLy;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageMSB;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageFile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageHYS;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCoQuanHaiQuan;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCoQuanHaiQuan_HYS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu_HYS;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoQuanLyTrongNoiBoDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiNguoiKhaiBao;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucPhanLoaiThuTucKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy ctrNhomXuLyHS;
        private Company.KDT.SHARE.VNACCS.Controls.ucNhomXuLy ucNhomXuLyHS_HYS;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaChungTu;
        private System.Windows.Forms.LinkLabel lblLinkExcelTemplate;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaChungTu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdTTHQ;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhaiBao;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucTrangThaiKhaiBao;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayHoanThanhKiemTraHoSo;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgaySuaCuoiCung;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayKhaiBao;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuHaiQuan;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiKhaiBao;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhai_HYS;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDeLayTepDinhKem;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
    }
}
