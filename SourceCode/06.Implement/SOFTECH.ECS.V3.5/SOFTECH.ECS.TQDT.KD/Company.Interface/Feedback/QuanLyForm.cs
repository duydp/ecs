﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class QuanLyForm : BaseForm
    {
        private DataTable dtThuVienCauHoi = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

        public QuanLyForm()
        {
            InitializeComponent();
            this.FormClosing += new FormClosingEventHandler(Form_FormClosing);

            FeedbackGlobalSettings.KhoiTao_GopYMacDinh();
        }

        private void QuanLyForm_Load(object sender, EventArgs e)
        {
            if (dtThuVienCauHoi == null)
                dtThuVienCauHoi = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["ThuVienCauHoi"];

            LoadDanhSachCauHoi();
        }

        void Form_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (base.IsChanged)
            {
                if (Globals.ShowMessage("Bạn chưa lưu thay đổi thông tin trên form, bạn có muốn lưu không?", true) == "Yes")
                {
                    Save();
                }
            }
        }

        private void LoadDanhSachCauHoi()
        {
            try
            {
                //cauHoiUserControl1.DataBinding(linhVucUserControl1.GetMa());
            }
            catch (Exception ex)
            {
                Globals.ShowMessage(ex.Message, false);
            }
        }

        private void btnLinhVuc_Click(object sender, EventArgs e)
        {
            LinhVucForm f = new LinhVucForm();
            f.ShowDialog();

            //linhVucUserControl1.ReLoad();
        }

        private void btnCauHoi_Click(object sender, EventArgs e)
        {
            CauHoiForm f = new CauHoiForm();
            f.ShowDialog();

            LoadDanhSachCauHoi();

            SetChanged(false);
        }

        private void linhVucUserControl1_LinhVucRowCheckStateChanged(object sender, Janus.Windows.GridEX.RowCheckStateChangeEventArgs e)
        {
            LoadDanhSachCauHoi();
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void Reset()
        {
            FeedbackGlobalSettings.LayDanhMucThuVienCauHoi();

            LoadDanhSachCauHoi();

            SetChanged(false);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (Globals.ShowMessage("Bạn có chắc chắn muốn xóa thông tin câu hỏi này không?", true) == "Yes")
            {
                XoaDuLieu();
            }
        }

        private void XoaDuLieu()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataRow rowSelected = cauHoiUserControl1.CurrentRow();

                if (rowSelected == null)
                    return;

                //Delete in memory
                DataRow[] rowDelete = dtThuVienCauHoi.Select("MaCauHoi = '" + rowSelected["MaCauHoi"] + "'");

                if (rowDelete.Length > 0)
                    dtThuVienCauHoi.Rows.Remove(rowDelete[0]);

                //Delete in database
                FeedbackGlobalSettings.XoaCauHoi(rowSelected["MaCauHoi"].ToString());

                Globals.ShowMessage("Đã thực hiện xóa thành công.", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
            finally
            {
                Cursor = Cursors.Default;

                Reset();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Save()
        {
            try
            {
                if (IsChanged)
                {
                    DataRow rowSelected = cauHoiUserControl1.CurrentRow();

                    if (rowSelected == null)
                        return;

                    //Delete in memory
                    DataRow[] rows = dtThuVienCauHoi.Select("MaCauHoi = '" + rowSelected["MaCauHoi"].ToString() + "'");

                    if (rows.Length > 0)
                    {
                        rows[0]["NoiDungCauHoi"] = txtNoiDung.Text;
                        rows[0]["TraLoi"] = txtTraLoi.Text;

                        ThuVienCauHoi cauhoi = new ThuVienCauHoi();
                        cauhoi.MaCauHoi = rows[0]["MaCauHoi"].ToString();
                        cauhoi.LinhVuc = rows[0]["LinhVuc"].ToString();
                        cauhoi.Phanloai = rows[0]["Phanloai"].ToString();
                        cauhoi.TieuDe = rows[0]["TieuDe"].ToString();
                        cauhoi.NoiDungCauHoi = rows[0]["NoiDungCauHoi"].ToString();
                        cauhoi.TraLoi = rows[0]["TraLoi"].ToString();
                        cauhoi.HienThi = Convert.ToBoolean(rows[0]["HienThi"]);
                        cauhoi.GhiChu = rows[0]["GhiChu"].ToString();

                        //Save in database
                        string msg = FeedbackGlobalSettings.TaoSuaCauHoi(ref cauhoi);
                        if (msg == "")
                        {
                            Globals.ShowMessage("Lưu thông tin thành công.", false);

                            SetChanged(false);
                        }
                        else
                            Globals.ShowMessage("Lỗi lưu thông tin:\r\n" + msg, false);
                    }
                }
            }
            catch (Exception ex)
            {
                Globals.ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void cauHoiUserControl1_CauHoiSelectionChanged(object sender, EventArgs e)
        {
            LoadNoiDungCauHoi();
        }

        private void LoadNoiDungCauHoi()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataRow rowSelected = cauHoiUserControl1.CurrentRow();

                if (rowSelected == null)
                {
                    txtNoiDung.Text = txtTraLoi.Text = "";
                    return;
                }

                //Delete in memory
                DataRow[] rows = dtThuVienCauHoi.Select("MaCauHoi = '" + rowSelected["MaCauHoi"] + "'");

                if (rows.Length > 0)
                {
                    txtNoiDung.TextChanged -= new EventHandler(textBox_TextChanged);
                    txtTraLoi.TextChanged -= new EventHandler(textBox_TextChanged);

                    txtNoiDung.Text = rows[0]["NoiDungCauHoi"].ToString();
                    txtTraLoi.Text = rows[0]["TraLoi"].ToString();

                    txtNoiDung.TextChanged += new EventHandler(textBox_TextChanged);
                    txtTraLoi.TextChanged += new EventHandler(textBox_TextChanged);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void textBox_TextChanged(object sender, EventArgs e)
        {
            SetChanged(true);
        }
    }
}
