﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GoodItems { set; get; }
		public decimal STT { set; get; }
		public long ID_HopDong { set; get; }
		public string SoHopDong { set; get; }
		public DateTime NgayHopDong { set; get; }
		public string MaHQ { set; get; }
		public DateTime NgayHetHan { set; get; }
		public string GhiChuKhac { set; get; }
        public List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail> GoodItemDetailCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection = new List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GoodItems"))) entity.GoodItems = reader.GetInt64(reader.GetOrdinal("GoodItems"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_HopDong"))) entity.ID_HopDong = reader.GetInt64(reader.GetOrdinal("ID_HopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details VALUES(@GoodItems, @STT, @ID_HopDong, @SoHopDong, @NgayHopDong, @MaHQ, @NgayHetHan, @GhiChuKhac)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details SET GoodItems = @GoodItems, STT = @STT, ID_HopDong = @ID_HopDong, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, MaHQ = @MaHQ, NgayHetHan = @NgayHetHan, GhiChuKhac = @GhiChuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GoodItems", SqlDbType.BigInt, "GoodItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GoodItems", SqlDbType.BigInt, "GoodItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details VALUES(@GoodItems, @STT, @ID_HopDong, @SoHopDong, @NgayHopDong, @MaHQ, @NgayHetHan, @GhiChuKhac)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details SET GoodItems = @GoodItems, STT = @STT, ID_HopDong = @ID_HopDong, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, MaHQ = @MaHQ, NgayHetHan = @NgayHetHan, GhiChuKhac = @GhiChuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GoodItems", SqlDbType.BigInt, "GoodItems", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GoodItems", SqlDbType.BigInt, "GoodItems", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ID_HopDong", SqlDbType.BigInt, "ID_HopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> SelectCollectionBy_GoodItems(long goodItems)
		{
            IDataReader reader = SelectReaderBy_GoodItems(goodItems);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GoodItems(long goodItems)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectBy_GoodItems]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItems", SqlDbType.BigInt, goodItems);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GoodItems(long goodItems)
		{
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_SelectBy_GoodItems";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItems", SqlDbType.BigInt, goodItems);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail(long goodItems, decimal sTT, long iD_HopDong, string soHopDong, DateTime ngayHopDong, string maHQ, DateTime ngayHetHan, string ghiChuKhac)
		{
			KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();	
			entity.GoodItems = goodItems;
			entity.STT = sTT;
			entity.ID_HopDong = iD_HopDong;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQ = maHQ;
			entity.NgayHetHan = ngayHetHan;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GoodItems", SqlDbType.BigInt, GoodItems);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, ID_HopDong);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail(long id, long goodItems, decimal sTT, long iD_HopDong, string soHopDong, DateTime ngayHopDong, string maHQ, DateTime ngayHetHan, string ghiChuKhac)
		{
			KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();			
			entity.ID = id;
			entity.GoodItems = goodItems;
			entity.STT = sTT;
			entity.ID_HopDong = iD_HopDong;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQ = maHQ;
			entity.NgayHetHan = ngayHetHan;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GoodItems", SqlDbType.BigInt, GoodItems);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, ID_HopDong);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail(long id, long goodItems, decimal sTT, long iD_HopDong, string soHopDong, DateTime ngayHopDong, string maHQ, DateTime ngayHetHan, string ghiChuKhac)
		{
			KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();			
			entity.ID = id;
			entity.GoodItems = goodItems;
			entity.STT = sTT;
			entity.ID_HopDong = iD_HopDong;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQ = maHQ;
			entity.NgayHetHan = ngayHetHan;
			entity.GhiChuKhac = ghiChuKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GoodItems", SqlDbType.BigInt, GoodItems);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@ID_HopDong", SqlDbType.BigInt, ID_HopDong);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail(long id)
		{
			KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public  int DeleteBy_GoodItems(long goodItems)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteBy_GoodItems]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GoodItems", SqlDbType.BigInt, goodItems);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
        public void InsertUpdateFull()
        {
            try
            {
                if (this.ID == 0)
                {
                    this.ID = this.Insert();
                }
                else
                {
                    this.Update();
                }
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in GoodItemDetailCollection)
                {
                    item.Contract_ID = this.ID;
                    if (item.ID == 0)
                    {
                        item.Insert();
                    }
                    else
                    {
                        item.InsertUpdate();
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {

            }
        }
        public void DeleteFull()
        {
            try
            {
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail item in GoodItemDetailCollection)
                {
                    item.Delete();
                }
                this.Delete();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}