﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace KeySecurity
{
    public class Active
    {
        const string svActiveName = "ActivatarKey";
        public static Active Install = null;
        public License License { get; private set; }
        public Active()
        {

        }
        static Active()
        {
            Install = new Active();
            Install.License = Install.Load();
        }
        private void Write(License license)
        {
            if (license != null)
            {

                string content = KeyCode.Serializer(license, true, true);
                content = KeyCode.EncryptRijndael(content);
                KeyCode.WriteToFile(content);
            }
        }
        public License ActiveKey(string key)
        {
            License lic = new License()
            {
                KeyInfo = new KeyInfo()
                {
                    Key = key.Trim(),
                    ProductId = Config.Install.ProductID,
                    MachineCode = KeyCode.ProcessInfo()
                }
            };
            try
            {
                string sfmtCode = KeyCode.Serializer(lic, true, true);
                sfmtCode = KeyCode.ConvertToBase64(sfmtCode);
                sfmtCode = KeyCode.Encrypt(sfmtCode);
                ActiveKey.ActivationKey active = new ActiveKey.ActivationKey();
                active.Url = string.Format(Config.Install.ActiveUrl, svActiveName);
                active.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                string ret = active.Active(sfmtCode);
                ret = KeyCode.Decrypt(ret);
                ret = KeyCode.ConvertFromBase64(ret);
                lic = KeyCode.Deserialize<License>(ret);
                License license = null;
                switch (lic.Status)
                {
                    case LicenseStatus.Licensed:
                        license = new License()
                        {
                            KeyInfo = new KeyInfo()
                            {
                                Key = lic.KeyInfo.Key,
                                ProductId = lic.KeyInfo.ProductId,
                                TrialDays = lic.KeyInfo.TrialDays,
                                ExpiredDate = lic.KeyInfo.ExpiredDate,
                                MachineCode = lic.KeyInfo.MachineCode
                            }
                        }; 
                        break;
                    case LicenseStatus.TrialVersion:
                        license = new License()
                        {
                            KeyInfo = new KeyInfo()
                            {
                                Key = lic.KeyInfo.Key,
                                ProductId = lic.KeyInfo.ProductId,
                                TrialDays = lic.KeyInfo.TrialDays,
                                ExpiredDate = lic.KeyInfo.ExpiredDate,
                                MachineCode = lic.KeyInfo.MachineCode
                            }
                        };
                        break;
                }

                if (license == null)
                    return lic;
                else
                {
                    Write(license);
                    return license;
                }
            }
            catch
            {
                return new License()
                {
                    Status = LicenseStatus.InternalError,
                    Message = "Lỗi khi thực hiện kích hoạt bản quyền\r\nVui lòng liên hệ nhà cung cấp để được hướng đẫn"

                };
            }

        }

        public License ActiveKey(string key, KeyInfo OldKey)
        {
            License lic = new License()
            {
                KeyInfo = new KeyInfo()
                {
                    Key = key.Trim(),
                    ProductId = Config.Install.ProductID,
                    MachineCode = KeyCode.ProcessInfo()
                }
            };
            try
            {
                string sfmtCode = KeyCode.Serializer(lic, true, true);
                sfmtCode = KeyCode.ConvertToBase64(sfmtCode);
                sfmtCode = KeyCode.Encrypt(sfmtCode);
                ActiveKey.ActivationKey active = new ActiveKey.ActivationKey();
                active.Url = string.Format(Config.Install.ActiveUrl, svActiveName);
                active.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                string ret = active.Active(sfmtCode);
                ret = KeyCode.Decrypt(ret);
                ret = KeyCode.ConvertFromBase64(ret);
                lic = KeyCode.Deserialize<License>(ret);
                License license = null;
                switch (lic.Status)
                {
                    case LicenseStatus.Licensed:
                        license = new License()
                        {
                            KeyInfo = new KeyInfo()
                            {
                                Key = lic.KeyInfo.Key,
                                ProductId = lic.KeyInfo.ProductId,
                                TrialDays = lic.KeyInfo.TrialDays,
                                ExpiredDate = lic.KeyInfo.ExpiredDate,
                                MachineCode = lic.KeyInfo.MachineCode
                            }
                        };
                        break;
                    case LicenseStatus.TrialVersion:
                        license = new License()
                        {
                            KeyInfo = new KeyInfo()
                            {
                                Key = lic.KeyInfo.Key,
                                ProductId = lic.KeyInfo.ProductId,
                                TrialDays = lic.KeyInfo.TrialDays,
                                ExpiredDate = lic.KeyInfo.ExpiredDate,
                                MachineCode = lic.KeyInfo.MachineCode
                            }
                        };
                        break;
                }

                if (license == null)
                    return lic;
                else
                {
                    Write(license);
                    return license;
                }
            }
            catch
            {
                return new License()
                {
                    Status = LicenseStatus.InternalError,
                    Message = "Lỗi khi thực hiện kích hoạt bản quyền\r\nVui lòng liên hệ nhà cung cấp để được hướng đẫn"

                };
            }

        }


        public License ActiveKey(string key, string machineCode, string ProductID)
        {
            License lic = new License()
            {
                KeyInfo = new KeyInfo()
                {
                    Key = key.Trim(),
                    ProductId = ProductID,
                    MachineCode = machineCode
                }
            };
            try
            {
                string sfmtCode = KeyCode.Serializer(lic, true, true);
                sfmtCode = KeyCode.ConvertToBase64(sfmtCode);
                sfmtCode = KeyCode.Encrypt(sfmtCode);
                ActiveKey.ActivationKey active = new ActiveKey.ActivationKey();
                active.Url = string.Format(Config.Install.ActiveUrl, svActiveName);
                active.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                string ret = active.Active(sfmtCode);
                ret = KeyCode.Decrypt(ret);
                ret = KeyCode.ConvertFromBase64(ret);
                lic = KeyCode.Deserialize<License>(ret);
                License license = null;
                switch (lic.Status)
                {
                    case LicenseStatus.Licensed:
                        license = new License()
                        {
                            KeyInfo = new KeyInfo()
                            {
                                Key = lic.KeyInfo.Key,
                                ProductId = lic.KeyInfo.ProductId,
                                TrialDays = lic.KeyInfo.TrialDays,
                                ExpiredDate = lic.KeyInfo.ExpiredDate,
                                MachineCode = lic.KeyInfo.MachineCode
                            }
                        };
                        break;
                    case LicenseStatus.TrialVersion:
                        license = new License()
                        {
                            KeyInfo = new KeyInfo()
                            {
                                Key = lic.KeyInfo.Key,
                                ProductId = lic.KeyInfo.ProductId,
                                TrialDays = lic.KeyInfo.TrialDays,
                                ExpiredDate = lic.KeyInfo.ExpiredDate,
                                MachineCode = lic.KeyInfo.MachineCode
                            }
                        };
                        break;
                }

                if (license == null)
                    return lic;
                else
                {
                    Write(license);
                    return license;
                }
            }
            catch (Exception ex)
            {
                return new License()
                {
                    Status = LicenseStatus.InternalError,
                    Message = "Lỗi khi thực hiện kích hoạt bản quyền\r\nVui lòng liên hệ nhà cung cấp để được hướng đẫn" + ex.Message

                };
            }

        }



        private License Load()
        {


            try
            {
                License license = null;
                bool isOffLine = false;
                #region Online
                try
                {
                    #region Check License

                    license = new License()
                    {
                        KeyInfo = new KeyInfo()
                        {
                            ProductId = Config.Install.ProductID,
                            MachineCode = KeyCode.ProcessInfo()
                        }
                    };
                    string sfmtCode = KeyCode.Serializer(license, true, true);
                    sfmtCode = KeyCode.ConvertToBase64(sfmtCode);
                    sfmtCode = KeyCode.Encrypt(sfmtCode);
                    ActiveKey.ActivationKey active = new ActiveKey.ActivationKey();
                    active.Url = string.Format(Config.Install.ActiveUrl, svActiveName);
                    active.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                    string ret = active.CheckInfo(sfmtCode);
                    ret = KeyCode.Decrypt(ret);
                    ret = KeyCode.ConvertFromBase64(ret);
                    license = KeyCode.Deserialize<License>(ret);
                    #endregion
                }
                catch
                {
                    isOffLine = true;
                }
                #endregion

                if (isOffLine)
                {
                    if (!File.Exists(Config.Install.LicenseFile))
                    {
                        return new License()
                         {
                             KeyInfo = null,
                             Status = LicenseStatus.NotFound,
                             Message = "Chưa đăng ký sử dụng phần mềm"
                         };
                    }

                    string lic = KeyCode.ReadFile();
                    lic = KeyCode.DecryptRijndael(lic);
                    license = KeyCode.Deserialize<License>(lic);
                    KeyInfo key = license.KeyInfo;
                    string machineCode = KeyCode.ProcessInfo();
                    if (key.MachineCode == machineCode)
                    {


                        if (key.ExpiredDate.Year != DateTime.Now.Year
                            || key.ExpiredDate.Month != DateTime.Now.Month
                            || key.ExpiredDate.Day != DateTime.Now.Day
                             )
                        {
                            TimeSpan t1 = new TimeSpan(DateTime.Now.Ticks);
                            TimeSpan t2 = new TimeSpan(key.ExpiredDate.Ticks);
                            key.TrialDays = (t2 - t1).Days;
                            if (key.TrialDays < 0)
                            {
                                license.Status = LicenseStatus.Expired;
                            }
                        }
                    }
                    else
                    {
                        license = new License()
                        {
                            Status = LicenseStatus.MachineHashMismatch,
                            Message = "Bạn đã thực hiện sao chép phần mềm từ máy khác sang\r\nVui lòng liên hệ nhà cung cấp để được hướng dẫn",
                            KeyInfo = new KeyInfo()
                            {
                                TrialDays = -1,
                                Key = string.Empty,
                                ProductId = license.KeyInfo.ProductId,
                                MachineCode = machineCode,
                                IsShow = false,
                                Notice = string.Empty
                            }
                        };
                    }
                }
                Write(license);
                return license;
            }
            catch
            {
                return new License()
                {
                    Status = LicenseStatus.InternalError,
                    Message = "Lỗi khi thực hiện kiểm tra bản quyền\r\nVui lòng liên hệ nhà cung cấp để được hướng đẫn"
                };
            }
        }
        public string ECS_updateInfo(string MessInfo)
        {
            string info_send = KeyCode.ConvertToBase64(MessInfo);
            ActiveKey.ActivationKey active = new ActiveKey.ActivationKey();
            active.Url = string.Format(Config.Install.ActiveUrl, svActiveName);
            active.Proxy = System.Net.WebRequest.GetSystemWebProxy();
            return active.Ecs_Update_Info(info_send);
        }


    }
}
