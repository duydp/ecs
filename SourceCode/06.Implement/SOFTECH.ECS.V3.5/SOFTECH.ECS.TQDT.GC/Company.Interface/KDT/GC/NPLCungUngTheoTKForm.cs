using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using System.IO;
using Janus.Windows.GridEX;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class NPLCungUngTheoTKForm : BaseForm
    {
        public BKCungUngDangKy BKCU = new BKCungUngDangKy();
        public ToKhaiMauDich TKMD = null;
        public ToKhaiChuyenTiep TKCT = null;
        HopDong HD = new HopDong();
        public bool boolFlag = false;
        public NPLCungUngTheoTKForm()
        {
            InitializeComponent();
        }

        private void NPLCungUngTheoTKSendForm_Load(object sender, EventArgs e)
        {
            string sfmtWhere = string.Empty;
            if (TKMD != null)
            {
                HD.ID = TKMD.IDHopDong;
                sfmtWhere = string.Format("TKMD_ID={0}", TKMD.ID);
                BKCU.TKMD_ID = TKMD.ID;
            }
            else
            {
                HD.ID = TKCT.IDHopDong;
                BKCU.TKCT_ID = TKCT.ID;
                sfmtWhere = string.Format("TKCT_ID={0}", TKCT.ID);
            }
            HD = HopDong.Load(HD.ID);
            dgList.Tables[0].Columns["LuongXK"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["LuongCUSanPham"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            BKCungUngDangKyCollection bks = BKCU.SelectCollectionDynamic(sfmtWhere, "");
            if (bks.Count > 0)
                BKCU = bks[0];
            if (BKCU.ID == 0 && this.BKCU.SanPhamCungUngCollection == null)
                this.BKCU.SanPhamCungUngCollection = new SanPhanCungUngCollection();
            else
            {
                if (BKCU.ID > 0)
                {
                    BKCU.LoadSanPhamCungUngCollection();
                    foreach (SanPhanCungUng item in BKCU.SanPhamCungUngCollection)
                    {
                        item.LoadNPLCungUngCollection();
                    }
                }
                if (TKMD != null)
                {
                    TKMD.Load();
                    TKMD.LoadHMDCollection();

                    BKCU.TrangThaiXuLy = TKMD.TrangThaiXuLy;

                }
                else
                {
                    TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    TKCT.LoadHCTCollection();
                    BKCU.TrangThaiXuLy = TKCT.TrangThaiXuLy;
                }
            }
            setCommandStatus();
            dgList.DataSource = this.BKCU.SanPhamCungUngCollection;

        }
        private bool PhanBo()
        {
            try
            {
                long master_ID;
                if (TKMD != null && TKMD.ID != 0)
                {
                    master_ID = TKMD.ID;
                    List<NPLCungUng> nplDeletes = NPLCungUng.SelectCollectionDynamic("TKMDID=" + master_ID, "");
                    foreach (NPLCungUng item in nplDeletes)
                    {
                        NPLCungUngDetail.DeleteDynamic("Master_ID=" + item.ID);

                    }
                    NPLCungUng.DeleteCollection(nplDeletes);

                    List<NPLCungUng> nplCungUngs = NPLCungUng.PhanBoNPLCungUngs(BKCU.ID, master_ID, TKMD.IDHopDong);
                    List<NPLCungUngDetail> nplCungUngChiTiets = NPLCungUng.CungUngChiTiets(TKMD.ID, BKCU.ID, TKMD.IDHopDong);
                    foreach (NPLCungUng item in nplCungUngs)
                    {

                        item.Insert();
                        item.NPLCungUngDetails = new List<NPLCungUngDetail>();
                        int SoChungTu = 0;
                        foreach (NPLCungUngDetail itemDetail in nplCungUngChiTiets)
                        {

                            if (item.MaHang.Trim() == itemDetail.MaNguyenPhuLieu.Trim())
                            {
                                itemDetail.Master_ID = item.ID;
                                itemDetail.SoChungTu = SoChungTu++.ToString();
                                item.NPLCungUngDetails.Add(itemDetail);
                            }
                        }
                        NPLCungUngDetail.InsertCollection(item.NPLCungUngDetails);
                    }
                }
                else
                {
                    master_ID = TKCT.ID;
                    List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng> nplDeletes = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng.SelectCollectionDynamic("TKCTID=" + TKCT.ID, "");
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng item in nplDeletes)
                    {
                        Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail.DeleteDynamic("Master_ID=" + item.ID);

                    }
                    Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng.DeleteCollection(nplDeletes);

                    List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng> nplCungUngs = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng.PhanBoNPLCungUngsCT(BKCU.ID, master_ID, TKCT.IDHopDong);
                    List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail> nplCungUngChiTiets = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng.CungUngChiTietsCT(master_ID, BKCU.ID, TKCT.IDHopDong);
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng item in nplCungUngs)
                    {

                        item.Insert();
                        item.NPLCungUngDetails = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail>();
                        int SoChungTu = 0;
                        foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail itemDetail in nplCungUngChiTiets)
                        {

                            if (item.MaHang.Trim() == itemDetail.MaNguyenPhuLieu.Trim())
                            {
                                itemDetail.Master_ID = item.ID;
                                itemDetail.SoChungTu = SoChungTu++.ToString();
                                item.NPLCungUngDetails.Add(itemDetail);
                            }
                        }
                        Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail.InsertCollection(item.NPLCungUngDetails);
                    }

                }
                return true;
                //ShowMessageTQDT("Phân bổ thành công", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(GlobalSettings.MA_HAI_QUAN, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
                return false;
            }

        }
        private void cmMainNPLCungUng_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

            switch (e.Command.Key)
            {
                case "cmdSave":
                    Save();
                    break;
                case "cmdAddSP":
                    ShowThemSPCungUngForm();
                    break;
                //case "InPhieuTN":
                //    this.inPhieuTN();
                //    break;
                //case "cmdPhanBo":
                //    PhanBo();
                //    break;
            }
        }
        private void inPhieuTN()
        {
            if (this.BKCU.SoTiepNhan == 0) return;
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "NGUYÊN PHỤ LIỆU CUNG ỨNG";
            phieuTN.soTN = this.BKCU.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.BKCU.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = BKCU.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }

        private void Save()
        {
            try
            {
                
                if (BKCU.SanPhamCungUngCollection.Count == 0)
                {
                    showMsg("MSG_2702013");
                    //MLMessages("Bạn chưa chọn sản phẩm để khai cung ứng.", "MSG_WRN11", "sản phẩm", false);
                    return;
                }
                if (BKCU.TKCT_ID > 0)
                {
                    BKCU.TKCT_ID = TKCT.ID;
                    //TKCT = ToKhaiChuyenTiep.Load(TKCT.ID);
                    //HD.ID = TKCT.IDHopDong;


                }
                else
                {
                    BKCU.TKMD_ID = TKMD.ID;
                    //HD.ID = TKMD.IDHopDong;
                }
                //HD = HopDong.Load(HD.ID);
                this.BKCU.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.BKCU.MaHaiQuan = HD.MaHaiQuan;
                this.BKCU.InsertUpdateFull();
                if (!PhanBo())
                {
                    ShowMessageTQDT("Phân bổ không thành công", false);
                    return;
                };
                showMsg("MSG_SAV02");
                //MLMessages("Lưu thành công.","MSG_SAV02","", false);
                setCommandStatus();
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }

        }

        private void ShowThemSPCungUngForm()
        {


            SanPhamCungUngForm f = new SanPhamCungUngForm();
            f.BKCU = this.BKCU;
            if (this.TKMD != null)
            {
                f.TKMD = this.TKMD;
                f.TKCT = null;
            }
            else
            {
                f.TKCT = this.TKCT;
                f.TKMD = null;
            }
            f.ShowDialog(this);
            if (f.SPCU.MaSanPham != "")
            {
                this.BKCU.SanPhamCungUngCollection.Add(f.SPCU);
            }
            dgList.DataSource = this.BKCU.SanPhamCungUngCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            //}
        }
        private HangMauDich GetHMDCuaToKhaiByMaHang(string maHang)
        {
            TKMD.LoadHMDCollection();
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private HangChuyenTiep GetHCTCuaToKhaiByMaHang(string maHang)
        {
            TKCT.LoadHCTCollection();
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                if (hmd.MaHang.Trim().ToUpper() == maHang.Trim().ToUpper())
                    return hmd;
            }
            return null;
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            string maSP = e.Row.Cells["MaSanPham"].Text;
            if (TKMD != null)
            {
                HangMauDich hmd = GetHMDCuaToKhaiByMaHang(maSP);
                if (hmd != null)
                {
                    e.Row.Cells["TenSP"].Text = hmd.TenHang;
                    e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(hmd.DVT_ID);
                    e.Row.Cells["LuongXK"].Text = hmd.SoLuong + "";
                }
            }
            else
            {
                HangChuyenTiep hmd = GetHCTCuaToKhaiByMaHang(maSP);
                if (hmd != null)
                {
                    e.Row.Cells["TenSP"].Text = hmd.TenHang;
                    e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(hmd.ID_DVT);
                    e.Row.Cells["LuongXK"].Text = hmd.SoLuong + "";
                }
            }
        }

        private void setCommandStatus()
        {
            bool isEnable = (BKCU.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || BKCU.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET
                 || BKCU.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || BKCU.TrangThaiXuLy == TrangThaiXuLy.DA_HUY);
            cmdAddSP.Enabled = cmdAddSP1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = isEnable ? Janus.Windows.UI.InheritableBoolean.True : Janus.Windows.UI.InheritableBoolean.False;
            cmdPhanBo.Visible = InPhieuTN.Visible = Janus.Windows.UI.InheritableBoolean.False;
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (BKCU.TKCT_ID == 0 && BKCU.TKMD_ID == 0)
            {
                ShowMessageTQDT("Vui lòng lưu thông tin", false);
                return;
            }
            if (e.Row.RowType == RowType.Record)
            {
                SanPhanCungUng SPCU = (SanPhanCungUng)e.Row.DataRow;
                SanPhamCungUngEditForm f = new SanPhamCungUngEditForm();
                f.BKCU = this.BKCU;
                f.SPCU = SPCU;
                if (SPCU.NPLCungUngCollection == null || SPCU.NPLCungUngCollection.Count == 0)
                {
                    SPCU.NPLCungUngCollection = new NguyenPhuLieuCungUngCollection();
                    SPCU.LoadNPLCungUngCollection();
                }
                f.ShowDialog(this);
                SPCU = f.SPCU;
            }
        }

        private void NPLCungUngTheoTKSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            HopDong HD = new HopDong();
            if (BKCU.TKCT_ID > 0)
            {
                HD.ID = TKCT.IDHopDong;
            }
            else
            {
                HD.ID = TKMD.IDHopDong;
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa các sản phẩm này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPhanCungUng spCungUng = (SanPhanCungUng)i.GetRow().DataRow;
                        if (spCungUng.ID > 0)
                            spCungUng.Delete(HD);
                    }
                }
            }
            else
                e.Cancel = true;
        }

        private void grbToKhai_Click(object sender, EventArgs e)
        {

        }

        private void txtSoTiepNhanTK_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnLayTuPhanBo_Click(object sender, EventArgs e)
        {
            try
            {
                if (BKCU.SanPhamCungUngCollection.Count > 0)
                {
                    string rel = ShowMessage("Chương trình sẽ tự động xóa dữ liệu đang có trên lưới để lấy từ bảng phân bổ.Bạn có muốn tiếp tục không ?", true);
                    if (rel != "Yes")
                        return;

                    foreach (SanPhanCungUng spCU in BKCU.SanPhamCungUngCollection)
                        spCU.Delete(HD);
                    BKCU.SanPhamCungUngCollection.Clear();
                }
                long id = 0;
                if (TKMD != null)
                    id = TKMD.ID;
                else
                    id = TKCT.ID;
                HopDong hd = new HopDong();
                hd.ID = HD.ID;
                hd = HopDong.Load(hd.ID);
                BKCungUngDangKy.GetSanPhamCungUngByPhanBo(id, BKCU, hd, GlobalSettings.SoThapPhan.LuongSP);
                dgList.DataSource = BKCU.SanPhamCungUngCollection;
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            catch (Exception exxx)
            {
                ShowMessage(" Lỗi : " + exxx.Message, false);
            }
        }
    }
}