﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Category
{
    public partial class VNACC_Category_CityUNLOCODE : BaseForm
    {
        public Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE cityUNLOCODE;
        public string where = "";
        public VNACC_Category_CityUNLOCODE()
        {
            InitializeComponent();
        }

        private void VNACC_Category_CityUNLOCODE_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
        private void ClearText()
        {
            //txtCode.Text = String.Empty;
            txtName.Text = String.Empty;
            txtNotes.Text = String.Empty;
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtCode, errorProvider, "Mã địa điểm ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtName, errorProvider, "Tên địa điểm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNotes, errorProvider, "Ghi chú ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                cityUNLOCODE.LOCODE = txtCode.Text.Trim().ToUpper();
                cityUNLOCODE.CityNameOrStateName = txtName.Text.Trim().ToUpper();
                cityUNLOCODE.Notes = txtNotes.Text.Trim().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void Set()
        {
            try
            {
                txtCode.Text = cityUNLOCODE.LOCODE;
                txtName.Text = cityUNLOCODE.CityNameOrStateName;
                txtNotes.Text = cityUNLOCODE.Notes;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1 = 1";
                if (!String.IsNullOrEmpty(txtCode.Text))
                    where += " AND LOCODE = '" + txtCode.Text.Trim() + "'";
                if (!String.IsNullOrEmpty(txtName.Text))
                    where += " AND CityNameOrStateName LIKE N'%" + txtName.Text.Trim() + "%'";
                if (!String.IsNullOrEmpty(txtNotes.Text))
                    where += " AND Notes LIKE N'%" + txtNotes.Text.Trim() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return where;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE.SelectCollectionDynamic(GetSearchWhere(), null);
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                Get();
                cityUNLOCODE.TableID = "A016A";
                cityUNLOCODE.InsertUpdate();
                ShowMessage("Lưu thành công ", false);
                cityUNLOCODE = new Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Get();
                if (ShowMessage("Bạn có muốn xóa dòng này không ? ", true) == "Yes")
                {
                    cityUNLOCODE.Delete();
                }
                ShowMessage("Xóa thành công ", false);
                cityUNLOCODE = new Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE();
                ClearText();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    cityUNLOCODE = new Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE();
                    cityUNLOCODE = (Company.KDT.SHARE.VNACCS.VNACC_Category_CityUNLOCODE)e.Row.DataRow;
                    Set();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
