﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    partial class ToKhaiMauDichManageForm
    {
        private UIGroupBox uiGroupBox1;
        private GridEX dgList;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichManageForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.timToKhai1 = new Company.Interface.Controls.TimToKhai();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepTK = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepHH = new System.Windows.Forms.ToolStripMenuItem();
            this.print = new System.Windows.Forms.ToolStripMenuItem();
            this.PhieuTNMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToKhaiA4MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.InToKhaiSuaDoiBoSung = new System.Windows.Forms.ToolStripMenuItem();
            this.InContainer = new System.Windows.Forms.ToolStripMenuItem();
            this.inToKhai196 = new System.Windows.Forms.ToolStripMenuItem();
            this.inBangKeCont196 = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCSDaDuyet = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportTK = new System.Windows.Forms.ToolStripMenuItem();
            this.mniImportMSG = new System.Windows.Forms.ToolStripMenuItem();
            this.mniSuaToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mniHuyToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdDelete1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdNPLCungUng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.SaoChep3 = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.cmdXuatToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdCSDaDuyet1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdPrint2 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.SaoChep = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.SaoChepToKhaiHang1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.SaoChepALL1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepALL = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepToKhaiHang = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.cmdInA41 = new Janus.Windows.UI.CommandBars.UICommand("cmdInA4");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.Export = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.ToKhaiViet = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiViet");
            this.InPhieuTN2 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdInA4 = new Janus.Windows.UI.CommandBars.UICommand("cmdInA4");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.cmdNPLCungUng = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(966, 368);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox1.Controls.Add(this.timToKhai1);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 28);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(966, 368);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(120, 3);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(433, 22);
            this.donViHaiQuanControl1.TabIndex = 302;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // timToKhai1
            // 
            this.timToKhai1.BackColor = System.Drawing.Color.Transparent;
            this.timToKhai1.Location = new System.Drawing.Point(40, 27);
            this.timToKhai1.Name = "timToKhai1";
            this.timToKhai1.Size = new System.Drawing.Size(763, 23);
            this.timToKhai1.TabIndex = 298;
            this.timToKhai1.Search += new System.EventHandler<Company.Interface.Controls.StringEventArgs>(this.timToKhai1_Search);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Blue;
            this.label1.Location = new System.Drawing.Point(548, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 13);
            this.label1.TabIndex = 297;
            this.label1.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 295;
            this.label2.Text = "Hải quan tiếp nhận";
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 56);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(966, 309);
            this.dgList.TabIndex = 12;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SaoChepCha,
            this.print,
            this.mnuCSDaDuyet,
            this.mniImportTK,
            this.mniImportMSG,
            this.mniSuaToKhai,
            this.mniHuyToKhai});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(176, 180);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saoChepTK,
            this.saoChepHH});
            this.SaoChepCha.Image = ((System.Drawing.Image)(resources.GetObject("SaoChepCha.Image")));
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.Size = new System.Drawing.Size(175, 22);
            this.SaoChepCha.Text = "Sao chép";
            this.SaoChepCha.Click += new System.EventHandler(this.SaoChepCha_Click);
            // 
            // saoChepTK
            // 
            this.saoChepTK.Image = ((System.Drawing.Image)(resources.GetObject("saoChepTK.Image")));
            this.saoChepTK.Name = "saoChepTK";
            this.saoChepTK.Size = new System.Drawing.Size(154, 22);
            this.saoChepTK.Text = "Tờ khai";
            this.saoChepTK.Click += new System.EventHandler(this.saoChepTK_Click);
            // 
            // saoChepHH
            // 
            this.saoChepHH.Image = ((System.Drawing.Image)(resources.GetObject("saoChepHH.Image")));
            this.saoChepHH.Name = "saoChepHH";
            this.saoChepHH.Size = new System.Drawing.Size(154, 22);
            this.saoChepHH.Text = "Tờ khai + hàng";
            this.saoChepHH.Click += new System.EventHandler(this.saoChepHH_Click);
            // 
            // print
            // 
            this.print.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.PhieuTNMenuItem,
            this.ToKhaiMenuItem,
            this.ToKhaiA4MenuItem,
            this.InToKhaiSuaDoiBoSung,
            this.InContainer,
            this.inToKhai196,
            this.inBangKeCont196});
            this.print.Image = ((System.Drawing.Image)(resources.GetObject("print.Image")));
            this.print.Name = "print";
            this.print.Size = new System.Drawing.Size(175, 22);
            this.print.Text = "In ";
            this.print.Click += new System.EventHandler(this.print_Click);
            // 
            // PhieuTNMenuItem
            // 
            this.PhieuTNMenuItem.Name = "PhieuTNMenuItem";
            this.PhieuTNMenuItem.Size = new System.Drawing.Size(264, 22);
            this.PhieuTNMenuItem.Text = "Phiếu tiếp nhận";
            this.PhieuTNMenuItem.Click += new System.EventHandler(this.PhieuTNMenuItem_Click);
            // 
            // ToKhaiMenuItem
            // 
            this.ToKhaiMenuItem.Name = "ToKhaiMenuItem";
            this.ToKhaiMenuItem.Size = new System.Drawing.Size(264, 22);
            this.ToKhaiMenuItem.Text = "Tờ khai";
            this.ToKhaiMenuItem.Visible = false;
            this.ToKhaiMenuItem.Click += new System.EventHandler(this.ToKhaiMenuItem_Click);
            // 
            // ToKhaiA4MenuItem
            // 
            this.ToKhaiA4MenuItem.Name = "ToKhaiA4MenuItem";
            this.ToKhaiA4MenuItem.Size = new System.Drawing.Size(264, 22);
            this.ToKhaiA4MenuItem.Text = "Tờ khai thông quan điện tử";
            this.ToKhaiA4MenuItem.Click += new System.EventHandler(this.ToKhaiA4MenuItem_Click);
            // 
            // InToKhaiSuaDoiBoSung
            // 
            this.InToKhaiSuaDoiBoSung.Name = "InToKhaiSuaDoiBoSung";
            this.InToKhaiSuaDoiBoSung.Size = new System.Drawing.Size(264, 22);
            this.InToKhaiSuaDoiBoSung.Text = "In tờ khai sửa đổi bổ sung";
            this.InToKhaiSuaDoiBoSung.Click += new System.EventHandler(this.InToKhaiSuaDoiBoSung_Click);
            // 
            // InContainer
            // 
            this.InContainer.Name = "InContainer";
            this.InContainer.Size = new System.Drawing.Size(264, 22);
            this.InContainer.Text = "In bảng kê Container";
            this.InContainer.Click += new System.EventHandler(this.InContainer_Click);
            // 
            // inToKhai196
            // 
            this.inToKhai196.Name = "inToKhai196";
            this.inToKhai196.Size = new System.Drawing.Size(264, 22);
            this.inToKhai196.Text = "Tờ khai thông quan điện tử (TT 196)";
            this.inToKhai196.Click += new System.EventHandler(this.inToKhai196_Click);
            // 
            // inBangKeCont196
            // 
            this.inBangKeCont196.Name = "inBangKeCont196";
            this.inBangKeCont196.Size = new System.Drawing.Size(264, 22);
            this.inBangKeCont196.Text = "Bảng kê Container (TT 196)";
            this.inBangKeCont196.Click += new System.EventHandler(this.inBangKeCont196_Click);
            // 
            // mnuCSDaDuyet
            // 
            this.mnuCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("mnuCSDaDuyet.Image")));
            this.mnuCSDaDuyet.Name = "mnuCSDaDuyet";
            this.mnuCSDaDuyet.Size = new System.Drawing.Size(175, 22);
            this.mnuCSDaDuyet.Text = "Chuyển Trạng Thái";
            this.mnuCSDaDuyet.Visible = false;
            this.mnuCSDaDuyet.Click += new System.EventHandler(this.mnuCSDaDuyet_Click);
            // 
            // mniImportTK
            // 
            this.mniImportTK.Name = "mniImportTK";
            this.mniImportTK.Size = new System.Drawing.Size(175, 22);
            this.mniImportTK.Text = "Import tờ khai";
            // 
            // mniImportMSG
            // 
            this.mniImportMSG.Name = "mniImportMSG";
            this.mniImportMSG.Size = new System.Drawing.Size(175, 22);
            this.mniImportMSG.Text = "Import MSG";
            // 
            // mniSuaToKhai
            // 
            this.mniSuaToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mniSuaToKhai.Image")));
            this.mniSuaToKhai.Name = "mniSuaToKhai";
            this.mniSuaToKhai.Size = new System.Drawing.Size(175, 22);
            this.mniSuaToKhai.Text = "Sửa tờ khai";
            this.mniSuaToKhai.Click += new System.EventHandler(this.mniSuaToKhai_Click);
            // 
            // mniHuyToKhai
            // 
            this.mniHuyToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mniHuyToKhai.Image")));
            this.mniHuyToKhai.Name = "mniHuyToKhai";
            this.mniHuyToKhai.Size = new System.Drawing.Size(175, 22);
            this.mniHuyToKhai.Text = "Hủy tờ khai";
            this.mniHuyToKhai.Click += new System.EventHandler(this.mniHuyToKhai_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "page_excel.png");
            this.ImageList1.Images.SetKeyName(5, "report.png");
            this.ImageList1.Images.SetKeyName(6, "shell32_240.ico");
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSingleDownload,
            this.cmdCancel,
            this.SaoChep,
            this.SaoChepALL,
            this.SaoChepToKhaiHang,
            this.cmdPrint,
            this.XacNhan,
            this.Export,
            this.Import,
            this.cmdCSDaDuyet,
            this.cmdXuatToKhai,
            this.ToKhaiViet,
            this.InPhieuTN2,
            this.cmdInA4,
            this.cmdDelete,
            this.cmdHistory,
            this.cmdNPLCungUng});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDelete1,
            this.cmdNPLCungUng1,
            this.cmdHistory1,
            this.Separator1,
            this.SaoChep3,
            this.cmdXuatToKhai1,
            this.cmdCSDaDuyet1,
            this.cmdPrint2});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.ImageList = this.ImageList1;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.MergeRowOrder = 1;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(966, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.uiCommandBar1_CommandClick);
            // 
            // cmdDelete1
            // 
            this.cmdDelete1.ImageIndex = 6;
            this.cmdDelete1.Key = "cmdDelete";
            this.cmdDelete1.Name = "cmdDelete1";
            // 
            // cmdNPLCungUng1
            // 
            this.cmdNPLCungUng1.ImageIndex = 1;
            this.cmdNPLCungUng1.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng1.Name = "cmdNPLCungUng1";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.ImageIndex = 5;
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // SaoChep3
            // 
            this.SaoChep3.Key = "SaoChep";
            this.SaoChep3.Name = "SaoChep3";
            this.SaoChep3.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.SaoChep3.Text = "&Sao chép";
            // 
            // cmdXuatToKhai1
            // 
            this.cmdXuatToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdXuatToKhai1.Icon")));
            this.cmdXuatToKhai1.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai1.Name = "cmdXuatToKhai1";
            this.cmdXuatToKhai1.Text = "X&uất dữ liệu cho phòng khai";
            // 
            // cmdCSDaDuyet1
            // 
            this.cmdCSDaDuyet1.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet1.Name = "cmdCSDaDuyet1";
            this.cmdCSDaDuyet1.Text = "&Chuyển Trạng Thái";
            // 
            // cmdPrint2
            // 
            this.cmdPrint2.ImageIndex = 2;
            this.cmdPrint2.Key = "cmdPrint";
            this.cmdPrint2.Name = "cmdPrint2";
            this.cmdPrint2.Text = "&In";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSingleDownload.Icon")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu mới của tờ khai đang chọn (Ctrl + D)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCancel.Icon")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + S)";
            // 
            // SaoChep
            // 
            this.SaoChep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.SaoChepToKhaiHang1,
            this.SaoChepALL1});
            this.SaoChep.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChep.Icon")));
            this.SaoChep.Key = "SaoChep";
            this.SaoChep.Name = "SaoChep";
            this.SaoChep.Text = "Sao chép";
            // 
            // SaoChepToKhaiHang1
            // 
            this.SaoChepToKhaiHang1.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChepToKhaiHang1.Icon")));
            this.SaoChepToKhaiHang1.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang1.Name = "SaoChepToKhaiHang1";
            this.SaoChepToKhaiHang1.Text = "Tờ khai";
            // 
            // SaoChepALL1
            // 
            this.SaoChepALL1.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChepALL1.Icon")));
            this.SaoChepALL1.Key = "SaoChepALL";
            this.SaoChepALL1.Name = "SaoChepALL1";
            this.SaoChepALL1.Text = "Tờ khai + hàng";
            // 
            // SaoChepALL
            // 
            this.SaoChepALL.Key = "SaoChepALL";
            this.SaoChepALL.Name = "SaoChepALL";
            this.SaoChepALL.Text = "Tờ khai + hàng";
            // 
            // SaoChepToKhaiHang
            // 
            this.SaoChepToKhaiHang.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Name = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Text = "Tờ khai + hàng";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.InPhieuTN1,
            this.ToKhai1,
            this.cmdInA41});
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.ImageIndex = 2;
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // ToKhai1
            // 
            this.ToKhai1.ImageIndex = 2;
            this.ToKhai1.Key = "ToKhaiViet";
            this.ToKhai1.Name = "ToKhai1";
            this.ToKhai1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdInA41
            // 
            this.cmdInA41.ImageIndex = 2;
            this.cmdInA41.Key = "cmdInA4";
            this.cmdInA41.Name = "cmdInA41";
            this.cmdInA41.Text = "In tờ khai thông quan điện tử";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận";
            // 
            // Export
            // 
            this.Export.Key = "Export";
            this.Export.Name = "Export";
            this.Export.Text = "Export dữ liệu";
            // 
            // Import
            // 
            this.Import.Key = "Import";
            this.Import.Name = "Import";
            this.Import.Text = "Import dữ liệu";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // ToKhaiViet
            // 
            this.ToKhaiViet.Key = "ToKhaiViet";
            this.ToKhaiViet.Name = "ToKhaiViet";
            this.ToKhaiViet.Text = "Tờ khai";
            // 
            // InPhieuTN2
            // 
            this.InPhieuTN2.Key = "InPhieuTN";
            this.InPhieuTN2.Name = "InPhieuTN2";
            this.InPhieuTN2.Text = "Phiếu tiếp nhận";
            // 
            // cmdInA4
            // 
            this.cmdInA4.Key = "cmdInA4";
            this.cmdInA4.Name = "cmdInA4";
            this.cmdInA4.Text = "In Trang A4";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Text = "Xóa";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Kết quả xử lý";
            // 
            // cmdNPLCungUng
            // 
            this.cmdNPLCungUng.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng.Name = "cmdNPLCungUng";
            this.cmdNPLCungUng.Text = "Xem NPL Cung ứng";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(966, 28);
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = this.cmMain;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(810, 0);
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "ECS files|*.ECS";
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // ToKhaiMauDichManageForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(966, 396);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichManageForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiMauDichManageForm";
            this.Text = "Theo dõi tờ khai mậu dịch";
            this.Load += new System.EventHandler(this.ToKhaiMauDichManageForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep3;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem SaoChepCha;
        private ToolStripMenuItem saoChepTK;
        private ToolStripMenuItem saoChepHH;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private ToolStripMenuItem print;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand Export;
        private Janus.Windows.UI.CommandBars.UICommand Import;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private ToolStripMenuItem mnuCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatToKhai;
        private ToolStripMenuItem ToKhaiMenuItem;
        private ToolStripMenuItem PhieuTNMenuItem;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint2;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand ToKhai;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN2;
        private Janus.Windows.UI.CommandBars.UICommand cmdInA4;
        private Janus.Windows.UI.CommandBars.UICommand cmdInA41;
        private ToolStripMenuItem ToKhaiA4MenuItem;
        private ColorDialog colorDialog1;
        private ToolStripMenuItem mniImportTK;
        private ToolStripMenuItem mniImportMSG;
        private ToolStripMenuItem mniSuaToKhai;
        private ToolStripMenuItem mniHuyToKhai;
        private Company.KDT.SHARE.Components.WS.KDTService kdtService1;
        private Janus.Windows.UI.CommandBars.UICommand ToKhaiViet;
        private ImageList ImageList1;
        private Label label1;
        private Label label2;
        private Company.Interface.Controls.TimToKhai timToKhai1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLCungUng1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLCungUng;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private ToolStripMenuItem InToKhaiSuaDoiBoSung;
        private ToolStripMenuItem InContainer;
        private ToolStripMenuItem inToKhai196;
        private ToolStripMenuItem inBangKeCont196;
    }
}
