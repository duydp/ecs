﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKCTForm : BaseForm
    {
        public ToKhaiCT ToKhaiChinhReport = new ToKhaiCT();
        public PhuLucTKCT PhuLucReport = new PhuLucTKCT();
        private System.Drawing.Printing.Margins MarginTKCT = new System.Drawing.Printing.Margins();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public XRTableCell Cell = new XRTableCell();
        public ReportViewTKCTForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.MarginTKCT = GlobalSettings.MarginTKCT;
            cboToKhai.SelectedIndex = 0;
            if (this.TKCT.HCTCollection.Count > 20)
            {
                int count = (this.TKCT.HCTCollection.Count - 1) /20 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            this.ToKhaiChinhReport.Margins = this.MarginTKCT;
            this.ToKhaiChinhReport.TKCT = this.TKCT;
            this.ToKhaiChinhReport.report = this;
            this.ToKhaiChinhReport.BindReport();
            printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
            this.ToKhaiChinhReport.CreateDocument();
            
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {

                HangChuyenTiepCollection HCTReportCollection = new HangChuyenTiepCollection();
                int begin = (cboToKhai.SelectedIndex - 1) * 20;
                int end = cboToKhai.SelectedIndex * 20;
                if (end > this.TKCT.HCTCollection.Count) end = this.TKCT.HCTCollection.Count;
                for (int i = begin; i < end; i++)
                    HCTReportCollection.Add(this.TKCT.HCTCollection[i]);
                this.PhuLucReport = new PhuLucTKCT();
                this.PhuLucReport.report = this;
                this.PhuLucReport.HCTCollection = HCTReportCollection;
                this.PhuLucReport.BindReport();
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.MarginTKCT = printControl1.PrintingSystem.PageMargins;
                this.ToKhaiChinhReport.setVisibleImage(false);
                this.ToKhaiChinhReport.Margins = this.MarginTKCT;
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MarginTKCT", this.MarginTKCT.ToString());

                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {

                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                } 
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }

        private void btnQuickPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.ptbImage.Visible = false;
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.ptbImage.Visible = true;
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                } 
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.setNhomHang(this.Cell, txtTenNhomHang.Text);
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setVisibleImage(false);
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {

                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.CreateDocument();
            }
        }
    }
}