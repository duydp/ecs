using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using BarcodeLib;
using Company.KDT.SHARE.VNACCS;
using ZXing.QrCode;
using ZXing;
using System.Collections.Generic;

namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewDinhDanhHangHoa : DevExpress.XtraReports.UI.XtraReport
    {
        public KDT_VNACC_ToKhaiMauDich TKMD ;
        public ReportViewDinhDanhHangHoa()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = true,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 55,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        private void GeneralQrCode()
        {
            try
            {
                Zen.Barcode.CodeQrBarcodeDraw qrCode = Zen.Barcode.BarcodeDrawFactory.CodeQr;       
                picQrCode.Image = qrCode.Draw(TKMD.VanDonCollection[0].SoDinhDanh.ToString(), 50);
                picQrCodeCoppy.Image = qrCode.Draw(TKMD.VanDonCollection[0].SoDinhDanh.ToString(), 50);
                picQrCodeCP.Image = qrCode.Draw(TKMD.VanDonCollection[0].SoDinhDanh.ToString(), 50);
                picQrCodeCPN.Image = qrCode.Draw(TKMD.VanDonCollection[0].SoDinhDanh.ToString(), 50);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void GeneralQrCodeNew()
        {
            try
            {
                QrCodeEncodingOptions options = new QrCodeEncodingOptions();
                options = new QrCodeEncodingOptions
                {
                    DisableECI = true,
                    CharacterSet = "UTF-8",
                    Width = 150,
                    Height = 150,
                    Margin = 0,                    
                };
                var writer = new BarcodeWriter();
                writer.Format = BarcodeFormat.QR_CODE;
                writer.Options = options;

                var qr = new ZXing.BarcodeWriter();
                qr.Options = options;
                qr.Format = ZXing.BarcodeFormat.QR_CODE;
                var result = new Bitmap(qr.Write(TKMD.VanDonCollection[0].SoDinhDanh.ToString()));
                picQrCode.Image = result;
                picQrCodeCoppy.Image = result;
                picQrCodeCP.Image = result;
                picQrCodeCPN.Image = result;

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        public void BindReport()
        {
            try
            {
                //GeneralQrCode();
                List<T_KDT_VNACC_SoThamChieuNoiBo> Collection = T_KDT_VNACC_SoThamChieuNoiBo.SelectCollectionBy_TKMD_ID(TKMD.ID);
                string SoThamChieu = "";
                if (Collection.Count > 0)
                {
                    foreach (T_KDT_VNACC_SoThamChieuNoiBo item in Collection)
                    {
                        SoThamChieu += item.SoThamChieu.ToString() + " - ";
                    }
                }
                if (SoThamChieu.Length > 0)
                    SoThamChieu = SoThamChieu.Substring(0, SoThamChieu.Length - 3);
                lblSoThamChieuNoiBo.Text = SoThamChieu;
                lblSoThamChieuNoiBoCoppy.Text = SoThamChieu;
                lblSoThamChieuNoiBoCP.Text = SoThamChieu;
                lblSoThamChieuNoiBoCPN.Text = SoThamChieu;

                GeneralQrCodeNew();
                lblSoDinhDanh.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiep.Text = TKMD.MaDonVi;
                lblTenDoanhNghiep.Text = TKMD.TenDonVi;
                lblSoTKXK.Text = TKMD.SoToKhai.ToString();
                picBarCode.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
                picBarCode.Image = GenerateBarCode(TKMD.SoToKhai.ToString());

                lblSoDinhDanhCoppy.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiepCoppy.Text = TKMD.MaDonVi;
                lblTenDoanhNghiepCoppy.Text = TKMD.TenDonVi;
                lblSoTKXKCoppy.Text = TKMD.SoToKhai.ToString();
                picBarCodeCoppy.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
                picBarCodeCoppy.Image = GenerateBarCode(TKMD.SoToKhai.ToString());

                lblSoDinhDanhCP.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiepCP.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiepCP.Text = TKMD.MaDonVi;
                lblTenDoanhNghiepCP.Text = TKMD.TenDonVi;
                lblSoTKXKCP.Text = TKMD.SoToKhai.ToString();
                picBarCodeCP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
                picBarCodeCP.Image = GenerateBarCode(TKMD.SoToKhai.ToString());

                lblSoDinhDanhCPN.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiepCPN.Text = TKMD.VanDonCollection[0].SoDinhDanh.ToString();
                lblMaDoanhNghiepCPN.Text = TKMD.MaDonVi;
                lblTenDoanhNghiepCPN.Text = TKMD.TenDonVi;
                lblSoTKXKCPN.Text = TKMD.SoToKhai.ToString();
                picBarCodeCPN.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
                picBarCodeCPN.Image = GenerateBarCode(TKMD.SoToKhai.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
