GO
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_THONGBAO_PHI') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_THONGBAO_PHI  
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_THONGBAO_FILE') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_THONGBAO_FILE 
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_THONGBAO') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_THONGBAO 
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_DOANHNGHIEP') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_DOANHNGHIEP 
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_TOKHAI_FILE') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_TOKHAI_FILE 
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_TOKHAI_DSCONTAINER') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_TOKHAI_DSCONTAINER 
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_TOKHAI') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_TOKHAI 

IF OBJECT_ID(N'dbo.T_KDT_THUPHI_BIENLAI_PHI') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_BIENLAI_PHI 
IF OBJECT_ID(N'dbo.T_KDT_THUPHI_BIENLAI') IS NOT NULL
	DROP TABLE  dbo.T_KDT_THUPHI_BIENLAI 
GO
CREATE TABLE T_KDT_THUPHI_DOANHNGHIEP
(
  ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
  MaHQ NVARCHAR(10) NULL,
  NgayTiepNhan DATETIME NOT NULL,
  TrangThaiXuLy INT NULL,
  MaDoanhNghiep NVARCHAR(14) NOT NULL,
  TenDoanhNghiep NVARCHAR(80) NOT NULL,
  DiaChi NVARCHAR(80) NOT NULL,
  NguoiLienHe NVARCHAR(200) NOT NULL,
  Email NVARCHAR(200) NOT NULL,
  SoDienThoai NVARCHAR(20) NOT NULL,
  Serial NVARCHAR(300) NOT NULL,
  CertString NVARCHAR(2000) NOT NULL,
  [Subject] NVARCHAR(255) NOT NULL,
  Issuer NVARCHAR(255) NOT NULL,
  ValidFrom DATETIME NOT NULL,
  ValidTo DATETIME NOT NULL,
  GuidStr VARCHAR(MAX) NULL,
)
GO
CREATE TABLE T_KDT_THUPHI_TOKHAI
(
  ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
  MaHaiQuan NVARCHAR(10) NULL,
  SoTiepNhan BIGINT NULL,
  NgayTiepNhan DATETIME NULL,
  TrangThaiXuLy INT NULL,
  DiemThuPhi NVARCHAR(6) NOT NULL,
  MaDoanhNghiep NVARCHAR(14) NOT NULL,
  TenDoanhNghiep NVARCHAR(80) NOT NULL,
  SoTKNP NVARCHAR(14) NOT NULL,
  NgayTKNP DATETIME NOT NULL,
  HinhThucTT NVARCHAR(50) NOT NULL,
  SoTK NVARCHAR(14) NOT NULL,
  NgayTK DATETIME NOT NULL,
  MaLoaiHinh NVARCHAR(10) NOT NULL,
  MaHQ NVARCHAR(10) NOT NULL,
  NhomLoaiHinh NVARCHAR(10) NOT NULL,
  MaPTVC INT NOT NULL,
  MaDiaDiemLuuKho NVARCHAR(10) NOT NULL,
  GhiChu NVARCHAR(2000),
  GuidStr VARCHAR(MAX) NULL,
  LoaiHangHoa INT NOT NULL
)
GO
CREATE TABLE T_KDT_THUPHI_TOKHAI_DSCONTAINER
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	TK_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.T_KDT_THUPHI_TOKHAI(ID),
	SoVanDon NVARCHAR(50) NOT NULL,
	TongTrongLuong NUMERIC(18,4),
	DVT NVARCHAR(4),
	SoContainer NVARCHAR(12) ,
	SoSeal NVARCHAR(255)  ,
	Loai INT  ,
	TinhChat INT ,
	SoLuong INT ,
	GhiChu NVARCHAR(255)
)
GO
CREATE TABLE T_KDT_THUPHI_TOKHAI_FILE
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	TK_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.T_KDT_THUPHI_TOKHAI(ID),
	Loai NVARCHAR(2) NOT NULL,
	TenFile NVARCHAR(100) NOT NULL,
	[FileSize] [bigint] NULL,
	Content  NVARCHAR(MAX) NOT NULL
)

GO

CREATE TABLE T_KDT_THUPHI_THONGBAO
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	TK_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.T_KDT_THUPHI_TOKHAI(ID),
	TrangThai INT NOT NULL,
	SoThongBao NVARCHAR(255) NOT NULL,
	NgayThongBao DATETIME NOT NULL,
	DonViThongBao NVARCHAR(6) NOT NULL,
	MaDonVi NVARCHAR(14) NOT NULL,
	TenDonVi NVARCHAR(80) NOT NULL,
	DiaChi NVARCHAR(255) NOT NULL,
	TenDonViXNK NVARCHAR(80) NOT NULL,
	MaDonViXNK NVARCHAR(14) NOT NULL,
	DiaChiXNK NVARCHAR(255) NOT NULL,
	GuidString VARCHAR(MAX)
)
GO
CREATE TABLE T_KDT_THUPHI_THONGBAO_PHI
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	TB_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.T_KDT_THUPHI_THONGBAO(ID),
	MaKhoanMuc NVARCHAR(12) NOT NULL,
	TenKhoanMuc NVARCHAR(255) NOT NULL,
	SoVanDonOrContainer NVARCHAR(50) NOT NULL,
	MaDVT NVARCHAR(10) NOT NULL,
	TenDVT NVARCHAR(100) NOT NULL,
	SoLuongOrTrongLuong NUMERIC(18,8) NOT NULL,
	DonGia NUMERIC(18,0) NOT NULL,
	ThanhTien NUMERIC(18,0) NOT NULL,
	DienGiai NVARCHAR(255)
)

GO
CREATE TABLE T_KDT_THUPHI_THONGBAO_FILE
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	TB_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.T_KDT_THUPHI_THONGBAO(ID),
    TenFile NVARCHAR(100) NOT NULL,
	[FileSize] [bigint] NULL,
	Content  NVARCHAR(MAX) NOT NULL
)

GO

CREATE TABLE T_KDT_THUPHI_BIENLAI
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	MauBienLai NVARCHAR(254) NOT NULL,
	SoSerial NVARCHAR(50) NOT NULL,
	QuyenBienLai NVARCHAR(12) NOT NULL,
	SoBienLai NVARCHAR(max) NOT NULL,
	NgayBienLai DATETIME NOT NULL,
	TrangThaiBienLai NVARCHAR(1),
	TongTien NUMERIC(18,0),
	MaDoanhNghiepNP NVARCHAR(14),
	TenDoanhNghiepNP NVARCHAR(80),
	DiaChiDoanhNghiepNP NVARCHAR(255),
	MST NVARCHAR(20),
	TenNguoiNop NVARCHAR(MAX),
	SoToKhai NVARCHAR(500) NOT NULL,
	NgayToKhai NVARCHAR(10) NOT NULL,
	MaLoaiHinh NVARCHAR(10) NOT NULL,
	NhomLoaiHinh NVARCHAR(10) NOT NULL,
	MaPTVC INT NOT NULL,
	MaDDLK NVARCHAR(10) NOT NULL,
	ChoPhepLayHang INT NOT NULL,
	DienGiai NVARCHAR(255),
	GuidString VARCHAR(MAX)	
)
GO
CREATE TABLE T_KDT_THUPHI_BIENLAI_PHI
(
	ID BIGINT NOT NULL IDENTITY (1,1) PRIMARY KEY ,
	BL_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.T_KDT_THUPHI_BIENLAI(ID),
	MaKhoanMuc NVARCHAR(12) NOT NULL,
	TenKhoanMuc NVARCHAR(255) NOT NULL,
	MaDVT NVARCHAR(10) NOT NULL,
	TenDVT NVARCHAR(100) NOT NULL,
	SoLuongOrTrongLuong NUMERIC(18,8) NOT NULL,
	DonGia NUMERIC(18,0) NOT NULL,
	ThanhTien NUMERIC(18,0) NOT NULL,
	DienGiai NVARCHAR(255),
	SoVanDon NVARCHAR(50) NOT NULL,
	SoContainer NVARCHAR(12),
	SoSeal NVARCHAR(255),
	Loai INT,
	TinhChat NVARCHAR(50)
)
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Insert]
	@MaHQ nvarchar(10),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@MaDoanhNghiep nvarchar(14),
	@TenDoanhNghiep nvarchar(80),
	@DiaChi nvarchar(80),
	@NguoiLienHe nvarchar(200),
	@Email nvarchar(200),
	@SoDienThoai nvarchar(20),
	@Serial nvarchar(300),
	@CertString nvarchar(2000),
	@Subject nvarchar(255),
	@Issuer nvarchar(255),
	@ValidFrom datetime,
	@ValidTo datetime,
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_DOANHNGHIEP]
(
	[MaHQ],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChi],
	[NguoiLienHe],
	[Email],
	[SoDienThoai],
	[Serial],
	[CertString],
	[Subject],
	[Issuer],
	[ValidFrom],
	[ValidTo],
	[GuidStr]
)
VALUES 
(
	@MaHQ,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@DiaChi,
	@NguoiLienHe,
	@Email,
	@SoDienThoai,
	@Serial,
	@CertString,
	@Subject,
	@Issuer,
	@ValidFrom,
	@ValidTo,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Update]
	@ID bigint,
	@MaHQ nvarchar(10),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@MaDoanhNghiep nvarchar(14),
	@TenDoanhNghiep nvarchar(80),
	@DiaChi nvarchar(80),
	@NguoiLienHe nvarchar(200),
	@Email nvarchar(200),
	@SoDienThoai nvarchar(20),
	@Serial nvarchar(300),
	@CertString nvarchar(2000),
	@Subject nvarchar(255),
	@Issuer nvarchar(255),
	@ValidFrom datetime,
	@ValidTo datetime,
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_DOANHNGHIEP]
SET
	[MaHQ] = @MaHQ,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[DiaChi] = @DiaChi,
	[NguoiLienHe] = @NguoiLienHe,
	[Email] = @Email,
	[SoDienThoai] = @SoDienThoai,
	[Serial] = @Serial,
	[CertString] = @CertString,
	[Subject] = @Subject,
	[Issuer] = @Issuer,
	[ValidFrom] = @ValidFrom,
	[ValidTo] = @ValidTo,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_InsertUpdate]
	@ID bigint,
	@MaHQ nvarchar(10),
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@MaDoanhNghiep nvarchar(14),
	@TenDoanhNghiep nvarchar(80),
	@DiaChi nvarchar(80),
	@NguoiLienHe nvarchar(200),
	@Email nvarchar(200),
	@SoDienThoai nvarchar(20),
	@Serial nvarchar(300),
	@CertString nvarchar(2000),
	@Subject nvarchar(255),
	@Issuer nvarchar(255),
	@ValidFrom datetime,
	@ValidTo datetime,
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_DOANHNGHIEP] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_DOANHNGHIEP] 
		SET
			[MaHQ] = @MaHQ,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[DiaChi] = @DiaChi,
			[NguoiLienHe] = @NguoiLienHe,
			[Email] = @Email,
			[SoDienThoai] = @SoDienThoai,
			[Serial] = @Serial,
			[CertString] = @CertString,
			[Subject] = @Subject,
			[Issuer] = @Issuer,
			[ValidFrom] = @ValidFrom,
			[ValidTo] = @ValidTo,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_DOANHNGHIEP]
		(
			[MaHQ],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[DiaChi],
			[NguoiLienHe],
			[Email],
			[SoDienThoai],
			[Serial],
			[CertString],
			[Subject],
			[Issuer],
			[ValidFrom],
			[ValidTo],
			[GuidStr]
		)
		VALUES 
		(
			@MaHQ,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@DiaChi,
			@NguoiLienHe,
			@Email,
			@SoDienThoai,
			@Serial,
			@CertString,
			@Subject,
			@Issuer,
			@ValidFrom,
			@ValidTo,
			@GuidStr
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_DOANHNGHIEP]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_DOANHNGHIEP] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHQ],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChi],
	[NguoiLienHe],
	[Email],
	[SoDienThoai],
	[Serial],
	[CertString],
	[Subject],
	[Issuer],
	[ValidFrom],
	[ValidTo],
	[GuidStr]
FROM
	[dbo].[T_KDT_THUPHI_DOANHNGHIEP]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaHQ],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChi],
	[NguoiLienHe],
	[Email],
	[SoDienThoai],
	[Serial],
	[CertString],
	[Subject],
	[Issuer],
	[ValidFrom],
	[ValidTo],
	[GuidStr]
FROM [dbo].[T_KDT_THUPHI_DOANHNGHIEP] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_DOANHNGHIEP_SelectAll]

















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHQ],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[DiaChi],
	[NguoiLienHe],
	[Email],
	[SoDienThoai],
	[Serial],
	[CertString],
	[Subject],
	[Issuer],
	[ValidFrom],
	[ValidTo],
	[GuidStr]
FROM
	[dbo].[T_KDT_THUPHI_DOANHNGHIEP]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Insert]
	@MaHaiQuan nvarchar(10),
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@DiemThuPhi nvarchar(6),
	@MaDoanhNghiep nvarchar(14),
	@TenDoanhNghiep nvarchar(80),
	@SoTKNP nvarchar(14),
	@NgayTKNP datetime,
	@HinhThucTT nvarchar(50),
	@SoTK nvarchar(14),
	@NgayTK datetime,
	@MaLoaiHinh nvarchar(10),
	@MaHQ nvarchar(10),
	@NhomLoaiHinh nvarchar(10),
	@MaPTVC int,
	@MaDiaDiemLuuKho nvarchar(10),
	@GhiChu nvarchar(2000),
	@GuidStr varchar(max),
	@LoaiHangHoa int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI]
(
	[MaHaiQuan],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[DiemThuPhi],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[SoTKNP],
	[NgayTKNP],
	[HinhThucTT],
	[SoTK],
	[NgayTK],
	[MaLoaiHinh],
	[MaHQ],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDiaDiemLuuKho],
	[GhiChu],
	[GuidStr],
	[LoaiHangHoa]
)
VALUES 
(
	@MaHaiQuan,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThaiXuLy,
	@DiemThuPhi,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@SoTKNP,
	@NgayTKNP,
	@HinhThucTT,
	@SoTK,
	@NgayTK,
	@MaLoaiHinh,
	@MaHQ,
	@NhomLoaiHinh,
	@MaPTVC,
	@MaDiaDiemLuuKho,
	@GhiChu,
	@GuidStr,
	@LoaiHangHoa
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Update]
	@ID bigint,
	@MaHaiQuan nvarchar(10),
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@DiemThuPhi nvarchar(6),
	@MaDoanhNghiep nvarchar(14),
	@TenDoanhNghiep nvarchar(80),
	@SoTKNP nvarchar(14),
	@NgayTKNP datetime,
	@HinhThucTT nvarchar(50),
	@SoTK nvarchar(14),
	@NgayTK datetime,
	@MaLoaiHinh nvarchar(10),
	@MaHQ nvarchar(10),
	@NhomLoaiHinh nvarchar(10),
	@MaPTVC int,
	@MaDiaDiemLuuKho nvarchar(10),
	@GhiChu nvarchar(2000),
	@GuidStr varchar(max),
	@LoaiHangHoa int
AS

UPDATE
	[dbo].[T_KDT_THUPHI_TOKHAI]
SET
	[MaHaiQuan] = @MaHaiQuan,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[DiemThuPhi] = @DiemThuPhi,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[SoTKNP] = @SoTKNP,
	[NgayTKNP] = @NgayTKNP,
	[HinhThucTT] = @HinhThucTT,
	[SoTK] = @SoTK,
	[NgayTK] = @NgayTK,
	[MaLoaiHinh] = @MaLoaiHinh,
	[MaHQ] = @MaHQ,
	[NhomLoaiHinh] = @NhomLoaiHinh,
	[MaPTVC] = @MaPTVC,
	[MaDiaDiemLuuKho] = @MaDiaDiemLuuKho,
	[GhiChu] = @GhiChu,
	[GuidStr] = @GuidStr,
	[LoaiHangHoa] = @LoaiHangHoa
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_InsertUpdate]
	@ID bigint,
	@MaHaiQuan nvarchar(10),
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThaiXuLy int,
	@DiemThuPhi nvarchar(6),
	@MaDoanhNghiep nvarchar(14),
	@TenDoanhNghiep nvarchar(80),
	@SoTKNP nvarchar(14),
	@NgayTKNP datetime,
	@HinhThucTT nvarchar(50),
	@SoTK nvarchar(14),
	@NgayTK datetime,
	@MaLoaiHinh nvarchar(10),
	@MaHQ nvarchar(10),
	@NhomLoaiHinh nvarchar(10),
	@MaPTVC int,
	@MaDiaDiemLuuKho nvarchar(10),
	@GhiChu nvarchar(2000),
	@GuidStr varchar(max),
	@LoaiHangHoa int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_TOKHAI] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_TOKHAI] 
		SET
			[MaHaiQuan] = @MaHaiQuan,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[DiemThuPhi] = @DiemThuPhi,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[SoTKNP] = @SoTKNP,
			[NgayTKNP] = @NgayTKNP,
			[HinhThucTT] = @HinhThucTT,
			[SoTK] = @SoTK,
			[NgayTK] = @NgayTK,
			[MaLoaiHinh] = @MaLoaiHinh,
			[MaHQ] = @MaHQ,
			[NhomLoaiHinh] = @NhomLoaiHinh,
			[MaPTVC] = @MaPTVC,
			[MaDiaDiemLuuKho] = @MaDiaDiemLuuKho,
			[GhiChu] = @GhiChu,
			[GuidStr] = @GuidStr,
			[LoaiHangHoa] = @LoaiHangHoa
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI]
		(
			[MaHaiQuan],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThaiXuLy],
			[DiemThuPhi],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[SoTKNP],
			[NgayTKNP],
			[HinhThucTT],
			[SoTK],
			[NgayTK],
			[MaLoaiHinh],
			[MaHQ],
			[NhomLoaiHinh],
			[MaPTVC],
			[MaDiaDiemLuuKho],
			[GhiChu],
			[GuidStr],
			[LoaiHangHoa]
		)
		VALUES 
		(
			@MaHaiQuan,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThaiXuLy,
			@DiemThuPhi,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@SoTKNP,
			@NgayTKNP,
			@HinhThucTT,
			@SoTK,
			@NgayTK,
			@MaLoaiHinh,
			@MaHQ,
			@NhomLoaiHinh,
			@MaPTVC,
			@MaDiaDiemLuuKho,
			@GhiChu,
			@GuidStr,
			@LoaiHangHoa
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_TOKHAI]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHaiQuan],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[DiemThuPhi],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[SoTKNP],
	[NgayTKNP],
	[HinhThucTT],
	[SoTK],
	[NgayTK],
	[MaLoaiHinh],
	[MaHQ],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDiaDiemLuuKho],
	[GhiChu],
	[GuidStr],
	[LoaiHangHoa]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaHaiQuan],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[DiemThuPhi],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[SoTKNP],
	[NgayTKNP],
	[HinhThucTT],
	[SoTK],
	[NgayTK],
	[MaLoaiHinh],
	[MaHQ],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDiaDiemLuuKho],
	[GhiChu],
	[GuidStr],
	[LoaiHangHoa]
FROM [dbo].[T_KDT_THUPHI_TOKHAI] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_SelectAll]





















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaHaiQuan],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThaiXuLy],
	[DiemThuPhi],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[SoTKNP],
	[NgayTKNP],
	[HinhThucTT],
	[SoTK],
	[NgayTK],
	[MaLoaiHinh],
	[MaHQ],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDiaDiemLuuKho],
	[GhiChu],
	[GuidStr],
	[LoaiHangHoa]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectBy_TK_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteBy_TK_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Insert]
	@TK_ID bigint,
	@Loai nvarchar(2),
	@TenFile nvarchar(100),
	@FileSize bigint,
	@Content nvarchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI_FILE]
(
	[TK_ID],
	[Loai],
	[TenFile],
	[FileSize],
	[Content]
)
VALUES 
(
	@TK_ID,
	@Loai,
	@TenFile,
	@FileSize,
	@Content
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Update]
	@ID bigint,
	@TK_ID bigint,
	@Loai nvarchar(2),
	@TenFile nvarchar(100),
	@FileSize bigint,
	@Content nvarchar(max)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_TOKHAI_FILE]
SET
	[TK_ID] = @TK_ID,
	[Loai] = @Loai,
	[TenFile] = @TenFile,
	[FileSize] = @FileSize,
	[Content] = @Content
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_InsertUpdate]
	@ID bigint,
	@TK_ID bigint,
	@Loai nvarchar(2),
	@TenFile nvarchar(100),
	@FileSize bigint,
	@Content nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_TOKHAI_FILE] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_TOKHAI_FILE] 
		SET
			[TK_ID] = @TK_ID,
			[Loai] = @Loai,
			[TenFile] = @TenFile,
			[FileSize] = @FileSize,
			[Content] = @Content
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI_FILE]
		(
			[TK_ID],
			[Loai],
			[TenFile],
			[FileSize],
			[Content]
		)
		VALUES 
		(
			@TK_ID,
			@Loai,
			@TenFile,
			@FileSize,
			@Content
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_TOKHAI_FILE]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteBy_TK_ID]
	@TK_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI_FILE]
WHERE
	[TK_ID] = @TK_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI_FILE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[Loai],
	[TenFile],
	[FileSize],
	[Content]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_FILE]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectBy_TK_ID]
	@TK_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[Loai],
	[TenFile],
	[FileSize],
	[Content]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_FILE]
WHERE
	[TK_ID] = @TK_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TK_ID],
	[Loai],
	[TenFile],
	[FileSize],
	[Content]
FROM [dbo].[T_KDT_THUPHI_TOKHAI_FILE] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_FILE_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[Loai],
	[TenFile],
	[FileSize],
	[Content]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_FILE]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Insert]
	@TK_ID bigint,
	@SoVanDon nvarchar(50),
	@TongTrongLuong numeric(18, 4),
	@DVT nvarchar(4),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai nvarchar(50),
	@TinhChat nvarchar(50),
	@SoLuong int,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
(
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
)
VALUES 
(
	@TK_ID,
	@SoVanDon,
	@TongTrongLuong,
	@DVT,
	@SoContainer,
	@SoSeal,
	@Loai,
	@TinhChat,
	@SoLuong,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Update]
	@ID bigint,
	@TK_ID bigint,
	@SoVanDon nvarchar(50),
	@TongTrongLuong numeric(18, 4),
	@DVT nvarchar(4),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai nvarchar(50),
	@TinhChat nvarchar(50),
	@SoLuong int,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
SET
	[TK_ID] = @TK_ID,
	[SoVanDon] = @SoVanDon,
	[TongTrongLuong] = @TongTrongLuong,
	[DVT] = @DVT,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[Loai] = @Loai,
	[TinhChat] = @TinhChat,
	[SoLuong] = @SoLuong,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_InsertUpdate]
	@ID bigint,
	@TK_ID bigint,
	@SoVanDon nvarchar(50),
	@TongTrongLuong numeric(18, 4),
	@DVT nvarchar(4),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai nvarchar(50),
	@TinhChat nvarchar(50),
	@SoLuong int,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] 
		SET
			[TK_ID] = @TK_ID,
			[SoVanDon] = @SoVanDon,
			[TongTrongLuong] = @TongTrongLuong,
			[DVT] = @DVT,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[Loai] = @Loai,
			[TinhChat] = @TinhChat,
			[SoLuong] = @SoLuong,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
		(
			[TK_ID],
			[SoVanDon],
			[TongTrongLuong],
			[DVT],
			[SoContainer],
			[SoSeal],
			[Loai],
			[TinhChat],
			[SoLuong],
			[GhiChu]
		)
		VALUES 
		(
			@TK_ID,
			@SoVanDon,
			@TongTrongLuong,
			@DVT,
			@SoContainer,
			@SoSeal,
			@Loai,
			@TinhChat,
			@SoLuong,
			@GhiChu
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteBy_TK_ID]
	@TK_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[TK_ID] = @TK_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectBy_TK_ID]
	@TK_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]
WHERE
	[TK_ID] = @TK_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM [dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_TOKHAI_DSCONTAINER_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[SoVanDon],
	[TongTrongLuong],
	[DVT],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat],
	[SoLuong],
	[GhiChu]
FROM
	[dbo].[T_KDT_THUPHI_TOKHAI_DSCONTAINER]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_SelectBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectBy_TK_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteBy_TK_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteBy_TK_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Insert]
	@TK_ID bigint,
	@TrangThai int,
	@SoThongBao nvarchar(255),
	@NgayThongBao datetime,
	@DonViThongBao nvarchar(6),
	@MaDonVi nvarchar(14),
	@TenDonVi nvarchar(80),
	@DiaChi nvarchar(255),
	@TenDonViXNK nvarchar(80),
	@MaDonViXNK nvarchar(14),
	@DiaChiXNK nvarchar(255),
	@GuidString varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_THONGBAO]
(
	[TK_ID],
	[TrangThai],
	[SoThongBao],
	[NgayThongBao],
	[DonViThongBao],
	[MaDonVi],
	[TenDonVi],
	[DiaChi],
	[TenDonViXNK],
	[MaDonViXNK],
	[DiaChiXNK],
	[GuidString]
)
VALUES 
(
	@TK_ID,
	@TrangThai,
	@SoThongBao,
	@NgayThongBao,
	@DonViThongBao,
	@MaDonVi,
	@TenDonVi,
	@DiaChi,
	@TenDonViXNK,
	@MaDonViXNK,
	@DiaChiXNK,
	@GuidString
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Update]
	@ID bigint,
	@TK_ID bigint,
	@TrangThai int,
	@SoThongBao nvarchar(255),
	@NgayThongBao datetime,
	@DonViThongBao nvarchar(6),
	@MaDonVi nvarchar(14),
	@TenDonVi nvarchar(80),
	@DiaChi nvarchar(255),
	@TenDonViXNK nvarchar(80),
	@MaDonViXNK nvarchar(14),
	@DiaChiXNK nvarchar(255),
	@GuidString varchar(max)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_THONGBAO]
SET
	[TK_ID] = @TK_ID,
	[TrangThai] = @TrangThai,
	[SoThongBao] = @SoThongBao,
	[NgayThongBao] = @NgayThongBao,
	[DonViThongBao] = @DonViThongBao,
	[MaDonVi] = @MaDonVi,
	[TenDonVi] = @TenDonVi,
	[DiaChi] = @DiaChi,
	[TenDonViXNK] = @TenDonViXNK,
	[MaDonViXNK] = @MaDonViXNK,
	[DiaChiXNK] = @DiaChiXNK,
	[GuidString] = @GuidString
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_InsertUpdate]
	@ID bigint,
	@TK_ID bigint,
	@TrangThai int,
	@SoThongBao nvarchar(255),
	@NgayThongBao datetime,
	@DonViThongBao nvarchar(6),
	@MaDonVi nvarchar(14),
	@TenDonVi nvarchar(80),
	@DiaChi nvarchar(255),
	@TenDonViXNK nvarchar(80),
	@MaDonViXNK nvarchar(14),
	@DiaChiXNK nvarchar(255),
	@GuidString varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_THONGBAO] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_THONGBAO] 
		SET
			[TK_ID] = @TK_ID,
			[TrangThai] = @TrangThai,
			[SoThongBao] = @SoThongBao,
			[NgayThongBao] = @NgayThongBao,
			[DonViThongBao] = @DonViThongBao,
			[MaDonVi] = @MaDonVi,
			[TenDonVi] = @TenDonVi,
			[DiaChi] = @DiaChi,
			[TenDonViXNK] = @TenDonViXNK,
			[MaDonViXNK] = @MaDonViXNK,
			[DiaChiXNK] = @DiaChiXNK,
			[GuidString] = @GuidString
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_THONGBAO]
		(
			[TK_ID],
			[TrangThai],
			[SoThongBao],
			[NgayThongBao],
			[DonViThongBao],
			[MaDonVi],
			[TenDonVi],
			[DiaChi],
			[TenDonViXNK],
			[MaDonViXNK],
			[DiaChiXNK],
			[GuidString]
		)
		VALUES 
		(
			@TK_ID,
			@TrangThai,
			@SoThongBao,
			@NgayThongBao,
			@DonViThongBao,
			@MaDonVi,
			@TenDonVi,
			@DiaChi,
			@TenDonViXNK,
			@MaDonViXNK,
			@DiaChiXNK,
			@GuidString
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_THONGBAO]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteBy_TK_ID]
	@TK_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_THONGBAO]
WHERE
	[TK_ID] = @TK_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_THONGBAO] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[TrangThai],
	[SoThongBao],
	[NgayThongBao],
	[DonViThongBao],
	[MaDonVi],
	[TenDonVi],
	[DiaChi],
	[TenDonViXNK],
	[MaDonViXNK],
	[DiaChiXNK],
	[GuidString]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectBy_TK_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectBy_TK_ID]
	@TK_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[TrangThai],
	[SoThongBao],
	[NgayThongBao],
	[DonViThongBao],
	[MaDonVi],
	[TenDonVi],
	[DiaChi],
	[TenDonViXNK],
	[MaDonViXNK],
	[DiaChiXNK],
	[GuidString]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO]
WHERE
	[TK_ID] = @TK_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TK_ID],
	[TrangThai],
	[SoThongBao],
	[NgayThongBao],
	[DonViThongBao],
	[MaDonVi],
	[TenDonVi],
	[DiaChi],
	[TenDonViXNK],
	[MaDonViXNK],
	[DiaChiXNK],
	[GuidString]
FROM [dbo].[T_KDT_THUPHI_THONGBAO] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TK_ID],
	[TrangThai],
	[SoThongBao],
	[NgayThongBao],
	[DonViThongBao],
	[MaDonVi],
	[TenDonVi],
	[DiaChi],
	[TenDonViXNK],
	[MaDonViXNK],
	[DiaChiXNK],
	[GuidString]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectBy_TB_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectBy_TB_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteBy_TB_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteBy_TB_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Insert]
	@TB_ID bigint,
	@TenFile nvarchar(100),
	@FileSize bigint,
	@Content nvarchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_THONGBAO_FILE]
(
	[TB_ID],
	[TenFile],
	[FileSize],
	[Content]
)
VALUES 
(
	@TB_ID,
	@TenFile,
	@FileSize,
	@Content
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Update]
	@ID bigint,
	@TB_ID bigint,
	@TenFile nvarchar(100),
	@FileSize bigint,
	@Content nvarchar(max)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_THONGBAO_FILE]
SET
	[TB_ID] = @TB_ID,
	[TenFile] = @TenFile,
	[FileSize] = @FileSize,
	[Content] = @Content
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_InsertUpdate]
	@ID bigint,
	@TB_ID bigint,
	@TenFile nvarchar(100),
	@FileSize bigint,
	@Content nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_THONGBAO_FILE] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_THONGBAO_FILE] 
		SET
			[TB_ID] = @TB_ID,
			[TenFile] = @TenFile,
			[FileSize] = @FileSize,
			[Content] = @Content
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_THONGBAO_FILE]
		(
			[TB_ID],
			[TenFile],
			[FileSize],
			[Content]
		)
		VALUES 
		(
			@TB_ID,
			@TenFile,
			@FileSize,
			@Content
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_THONGBAO_FILE]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteBy_TB_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteBy_TB_ID]
	@TB_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_THONGBAO_FILE]
WHERE
	[TB_ID] = @TB_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_THONGBAO_FILE] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TB_ID],
	[TenFile],
	[FileSize],
	[Content]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO_FILE]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectBy_TB_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectBy_TB_ID]
	@TB_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TB_ID],
	[TenFile],
	[FileSize],
	[Content]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO_FILE]
WHERE
	[TB_ID] = @TB_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TB_ID],
	[TenFile],
	[FileSize],
	[Content]
FROM [dbo].[T_KDT_THUPHI_THONGBAO_FILE] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_FILE_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TB_ID],
	[TenFile],
	[FileSize],
	[Content]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO_FILE]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectBy_TB_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectBy_TB_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteBy_TB_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteBy_TB_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Insert]
	@TB_ID bigint,
	@MaKhoanMuc nvarchar(12),
	@TenKhoanMuc nvarchar(255),
	@SoVanDonOrContainer nvarchar(50),
	@MaDVT nvarchar(10),
	@TenDVT nvarchar(100),
	@SoLuongOrTrongLuong numeric(18, 8),
	@DonGia numeric(18, 0),
	@ThanhTien numeric(18, 0),
	@DienGiai nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_THONGBAO_PHI]
(
	[TB_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[SoVanDonOrContainer],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai]
)
VALUES 
(
	@TB_ID,
	@MaKhoanMuc,
	@TenKhoanMuc,
	@SoVanDonOrContainer,
	@MaDVT,
	@TenDVT,
	@SoLuongOrTrongLuong,
	@DonGia,
	@ThanhTien,
	@DienGiai
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Update]
	@ID bigint,
	@TB_ID bigint,
	@MaKhoanMuc nvarchar(12),
	@TenKhoanMuc nvarchar(255),
	@SoVanDonOrContainer nvarchar(50),
	@MaDVT nvarchar(10),
	@TenDVT nvarchar(100),
	@SoLuongOrTrongLuong numeric(18, 8),
	@DonGia numeric(18, 0),
	@ThanhTien numeric(18, 0),
	@DienGiai nvarchar(255)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_THONGBAO_PHI]
SET
	[TB_ID] = @TB_ID,
	[MaKhoanMuc] = @MaKhoanMuc,
	[TenKhoanMuc] = @TenKhoanMuc,
	[SoVanDonOrContainer] = @SoVanDonOrContainer,
	[MaDVT] = @MaDVT,
	[TenDVT] = @TenDVT,
	[SoLuongOrTrongLuong] = @SoLuongOrTrongLuong,
	[DonGia] = @DonGia,
	[ThanhTien] = @ThanhTien,
	[DienGiai] = @DienGiai
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_InsertUpdate]
	@ID bigint,
	@TB_ID bigint,
	@MaKhoanMuc nvarchar(12),
	@TenKhoanMuc nvarchar(255),
	@SoVanDonOrContainer nvarchar(50),
	@MaDVT nvarchar(10),
	@TenDVT nvarchar(100),
	@SoLuongOrTrongLuong numeric(18, 8),
	@DonGia numeric(18, 0),
	@ThanhTien numeric(18, 0),
	@DienGiai nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_THONGBAO_PHI] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_THONGBAO_PHI] 
		SET
			[TB_ID] = @TB_ID,
			[MaKhoanMuc] = @MaKhoanMuc,
			[TenKhoanMuc] = @TenKhoanMuc,
			[SoVanDonOrContainer] = @SoVanDonOrContainer,
			[MaDVT] = @MaDVT,
			[TenDVT] = @TenDVT,
			[SoLuongOrTrongLuong] = @SoLuongOrTrongLuong,
			[DonGia] = @DonGia,
			[ThanhTien] = @ThanhTien,
			[DienGiai] = @DienGiai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_THONGBAO_PHI]
		(
			[TB_ID],
			[MaKhoanMuc],
			[TenKhoanMuc],
			[SoVanDonOrContainer],
			[MaDVT],
			[TenDVT],
			[SoLuongOrTrongLuong],
			[DonGia],
			[ThanhTien],
			[DienGiai]
		)
		VALUES 
		(
			@TB_ID,
			@MaKhoanMuc,
			@TenKhoanMuc,
			@SoVanDonOrContainer,
			@MaDVT,
			@TenDVT,
			@SoLuongOrTrongLuong,
			@DonGia,
			@ThanhTien,
			@DienGiai
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_THONGBAO_PHI]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteBy_TB_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteBy_TB_ID]
	@TB_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_THONGBAO_PHI]
WHERE
	[TB_ID] = @TB_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_THONGBAO_PHI] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TB_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[SoVanDonOrContainer],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO_PHI]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectBy_TB_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectBy_TB_ID]
	@TB_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TB_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[SoVanDonOrContainer],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO_PHI]
WHERE
	[TB_ID] = @TB_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TB_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[SoVanDonOrContainer],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai]
FROM [dbo].[T_KDT_THUPHI_THONGBAO_PHI] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_THONGBAO_PHI_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TB_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[SoVanDonOrContainer],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai]
FROM
	[dbo].[T_KDT_THUPHI_THONGBAO_PHI]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Insert]
	@MauBienLai nvarchar(254),
	@SoSerial nvarchar(50),
	@QuyenBienLai nvarchar(12),
	@SoBienLai nvarchar(max),
	@NgayBienLai datetime,
	@TrangThaiBienLai nvarchar(1),
	@TongTien numeric(18, 0),
	@MaDoanhNghiepNP nvarchar(14),
	@TenDoanhNghiepNP nvarchar(80),
	@DiaChiDoanhNghiepNP nvarchar(255),
	@MST nvarchar(20),
	@TenNguoiNop nvarchar(max),
	@SoToKhai nvarchar(500),
	@NgayToKhai nvarchar(10),
	@MaLoaiHinh nvarchar(10),
	@NhomLoaiHinh nvarchar(10),
	@MaPTVC int,
	@MaDDLK nvarchar(10),
	@ChoPhepLayHang int,
	@DienGiai nvarchar(255),
	@GuidString varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_BIENLAI]
(
	[MauBienLai],
	[SoSerial],
	[QuyenBienLai],
	[SoBienLai],
	[NgayBienLai],
	[TrangThaiBienLai],
	[TongTien],
	[MaDoanhNghiepNP],
	[TenDoanhNghiepNP],
	[DiaChiDoanhNghiepNP],
	[MST],
	[TenNguoiNop],
	[SoToKhai],
	[NgayToKhai],
	[MaLoaiHinh],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDDLK],
	[ChoPhepLayHang],
	[DienGiai],
	[GuidString]
)
VALUES 
(
	@MauBienLai,
	@SoSerial,
	@QuyenBienLai,
	@SoBienLai,
	@NgayBienLai,
	@TrangThaiBienLai,
	@TongTien,
	@MaDoanhNghiepNP,
	@TenDoanhNghiepNP,
	@DiaChiDoanhNghiepNP,
	@MST,
	@TenNguoiNop,
	@SoToKhai,
	@NgayToKhai,
	@MaLoaiHinh,
	@NhomLoaiHinh,
	@MaPTVC,
	@MaDDLK,
	@ChoPhepLayHang,
	@DienGiai,
	@GuidString
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Update]
	@ID bigint,
	@MauBienLai nvarchar(254),
	@SoSerial nvarchar(50),
	@QuyenBienLai nvarchar(12),
	@SoBienLai nvarchar(max),
	@NgayBienLai datetime,
	@TrangThaiBienLai nvarchar(1),
	@TongTien numeric(18, 0),
	@MaDoanhNghiepNP nvarchar(14),
	@TenDoanhNghiepNP nvarchar(80),
	@DiaChiDoanhNghiepNP nvarchar(255),
	@MST nvarchar(20),
	@TenNguoiNop nvarchar(max),
	@SoToKhai nvarchar(500),
	@NgayToKhai nvarchar(10),
	@MaLoaiHinh nvarchar(10),
	@NhomLoaiHinh nvarchar(10),
	@MaPTVC int,
	@MaDDLK nvarchar(10),
	@ChoPhepLayHang int,
	@DienGiai nvarchar(255),
	@GuidString varchar(max)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_BIENLAI]
SET
	[MauBienLai] = @MauBienLai,
	[SoSerial] = @SoSerial,
	[QuyenBienLai] = @QuyenBienLai,
	[SoBienLai] = @SoBienLai,
	[NgayBienLai] = @NgayBienLai,
	[TrangThaiBienLai] = @TrangThaiBienLai,
	[TongTien] = @TongTien,
	[MaDoanhNghiepNP] = @MaDoanhNghiepNP,
	[TenDoanhNghiepNP] = @TenDoanhNghiepNP,
	[DiaChiDoanhNghiepNP] = @DiaChiDoanhNghiepNP,
	[MST] = @MST,
	[TenNguoiNop] = @TenNguoiNop,
	[SoToKhai] = @SoToKhai,
	[NgayToKhai] = @NgayToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NhomLoaiHinh] = @NhomLoaiHinh,
	[MaPTVC] = @MaPTVC,
	[MaDDLK] = @MaDDLK,
	[ChoPhepLayHang] = @ChoPhepLayHang,
	[DienGiai] = @DienGiai,
	[GuidString] = @GuidString
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_InsertUpdate]
	@ID bigint,
	@MauBienLai nvarchar(254),
	@SoSerial nvarchar(50),
	@QuyenBienLai nvarchar(12),
	@SoBienLai nvarchar(max),
	@NgayBienLai datetime,
	@TrangThaiBienLai nvarchar(1),
	@TongTien numeric(18, 0),
	@MaDoanhNghiepNP nvarchar(14),
	@TenDoanhNghiepNP nvarchar(80),
	@DiaChiDoanhNghiepNP nvarchar(255),
	@MST nvarchar(20),
	@TenNguoiNop nvarchar(max),
	@SoToKhai nvarchar(500),
	@NgayToKhai nvarchar(10),
	@MaLoaiHinh nvarchar(10),
	@NhomLoaiHinh nvarchar(10),
	@MaPTVC int,
	@MaDDLK nvarchar(10),
	@ChoPhepLayHang int,
	@DienGiai nvarchar(255),
	@GuidString varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_BIENLAI] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_BIENLAI] 
		SET
			[MauBienLai] = @MauBienLai,
			[SoSerial] = @SoSerial,
			[QuyenBienLai] = @QuyenBienLai,
			[SoBienLai] = @SoBienLai,
			[NgayBienLai] = @NgayBienLai,
			[TrangThaiBienLai] = @TrangThaiBienLai,
			[TongTien] = @TongTien,
			[MaDoanhNghiepNP] = @MaDoanhNghiepNP,
			[TenDoanhNghiepNP] = @TenDoanhNghiepNP,
			[DiaChiDoanhNghiepNP] = @DiaChiDoanhNghiepNP,
			[MST] = @MST,
			[TenNguoiNop] = @TenNguoiNop,
			[SoToKhai] = @SoToKhai,
			[NgayToKhai] = @NgayToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NhomLoaiHinh] = @NhomLoaiHinh,
			[MaPTVC] = @MaPTVC,
			[MaDDLK] = @MaDDLK,
			[ChoPhepLayHang] = @ChoPhepLayHang,
			[DienGiai] = @DienGiai,
			[GuidString] = @GuidString
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_BIENLAI]
		(
			[MauBienLai],
			[SoSerial],
			[QuyenBienLai],
			[SoBienLai],
			[NgayBienLai],
			[TrangThaiBienLai],
			[TongTien],
			[MaDoanhNghiepNP],
			[TenDoanhNghiepNP],
			[DiaChiDoanhNghiepNP],
			[MST],
			[TenNguoiNop],
			[SoToKhai],
			[NgayToKhai],
			[MaLoaiHinh],
			[NhomLoaiHinh],
			[MaPTVC],
			[MaDDLK],
			[ChoPhepLayHang],
			[DienGiai],
			[GuidString]
		)
		VALUES 
		(
			@MauBienLai,
			@SoSerial,
			@QuyenBienLai,
			@SoBienLai,
			@NgayBienLai,
			@TrangThaiBienLai,
			@TongTien,
			@MaDoanhNghiepNP,
			@TenDoanhNghiepNP,
			@DiaChiDoanhNghiepNP,
			@MST,
			@TenNguoiNop,
			@SoToKhai,
			@NgayToKhai,
			@MaLoaiHinh,
			@NhomLoaiHinh,
			@MaPTVC,
			@MaDDLK,
			@ChoPhepLayHang,
			@DienGiai,
			@GuidString
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_BIENLAI]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_BIENLAI] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MauBienLai],
	[SoSerial],
	[QuyenBienLai],
	[SoBienLai],
	[NgayBienLai],
	[TrangThaiBienLai],
	[TongTien],
	[MaDoanhNghiepNP],
	[TenDoanhNghiepNP],
	[DiaChiDoanhNghiepNP],
	[MST],
	[TenNguoiNop],
	[SoToKhai],
	[NgayToKhai],
	[MaLoaiHinh],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDDLK],
	[ChoPhepLayHang],
	[DienGiai],
	[GuidString]
FROM
	[dbo].[T_KDT_THUPHI_BIENLAI]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MauBienLai],
	[SoSerial],
	[QuyenBienLai],
	[SoBienLai],
	[NgayBienLai],
	[TrangThaiBienLai],
	[TongTien],
	[MaDoanhNghiepNP],
	[TenDoanhNghiepNP],
	[DiaChiDoanhNghiepNP],
	[MST],
	[TenNguoiNop],
	[SoToKhai],
	[NgayToKhai],
	[MaLoaiHinh],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDDLK],
	[ChoPhepLayHang],
	[DienGiai],
	[GuidString]
FROM [dbo].[T_KDT_THUPHI_BIENLAI] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_SelectAll]






















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MauBienLai],
	[SoSerial],
	[QuyenBienLai],
	[SoBienLai],
	[NgayBienLai],
	[TrangThaiBienLai],
	[TongTien],
	[MaDoanhNghiepNP],
	[TenDoanhNghiepNP],
	[DiaChiDoanhNghiepNP],
	[MST],
	[TenNguoiNop],
	[SoToKhai],
	[NgayToKhai],
	[MaLoaiHinh],
	[NhomLoaiHinh],
	[MaPTVC],
	[MaDDLK],
	[ChoPhepLayHang],
	[DienGiai],
	[GuidString]
FROM
	[dbo].[T_KDT_THUPHI_BIENLAI]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectBy_BL_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectBy_BL_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteBy_BL_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteBy_BL_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Insert]
	@BL_ID bigint,
	@MaKhoanMuc nvarchar(12),
	@TenKhoanMuc nvarchar(255),
	@MaDVT nvarchar(10),
	@TenDVT nvarchar(100),
	@SoLuongOrTrongLuong numeric(18, 8),
	@DonGia numeric(18, 0),
	@ThanhTien numeric(18, 0),
	@DienGiai nvarchar(255),
	@SoVanDon nvarchar(50),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai int,
	@TinhChat nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_THUPHI_BIENLAI_PHI]
(
	[BL_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat]
)
VALUES 
(
	@BL_ID,
	@MaKhoanMuc,
	@TenKhoanMuc,
	@MaDVT,
	@TenDVT,
	@SoLuongOrTrongLuong,
	@DonGia,
	@ThanhTien,
	@DienGiai,
	@SoVanDon,
	@SoContainer,
	@SoSeal,
	@Loai,
	@TinhChat
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Update]
	@ID bigint,
	@BL_ID bigint,
	@MaKhoanMuc nvarchar(12),
	@TenKhoanMuc nvarchar(255),
	@MaDVT nvarchar(10),
	@TenDVT nvarchar(100),
	@SoLuongOrTrongLuong numeric(18, 8),
	@DonGia numeric(18, 0),
	@ThanhTien numeric(18, 0),
	@DienGiai nvarchar(255),
	@SoVanDon nvarchar(50),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai int,
	@TinhChat nvarchar(50)
AS

UPDATE
	[dbo].[T_KDT_THUPHI_BIENLAI_PHI]
SET
	[BL_ID] = @BL_ID,
	[MaKhoanMuc] = @MaKhoanMuc,
	[TenKhoanMuc] = @TenKhoanMuc,
	[MaDVT] = @MaDVT,
	[TenDVT] = @TenDVT,
	[SoLuongOrTrongLuong] = @SoLuongOrTrongLuong,
	[DonGia] = @DonGia,
	[ThanhTien] = @ThanhTien,
	[DienGiai] = @DienGiai,
	[SoVanDon] = @SoVanDon,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[Loai] = @Loai,
	[TinhChat] = @TinhChat
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_InsertUpdate]
	@ID bigint,
	@BL_ID bigint,
	@MaKhoanMuc nvarchar(12),
	@TenKhoanMuc nvarchar(255),
	@MaDVT nvarchar(10),
	@TenDVT nvarchar(100),
	@SoLuongOrTrongLuong numeric(18, 8),
	@DonGia numeric(18, 0),
	@ThanhTien numeric(18, 0),
	@DienGiai nvarchar(255),
	@SoVanDon nvarchar(50),
	@SoContainer nvarchar(12),
	@SoSeal nvarchar(255),
	@Loai int,
	@TinhChat nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_THUPHI_BIENLAI_PHI] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_THUPHI_BIENLAI_PHI] 
		SET
			[BL_ID] = @BL_ID,
			[MaKhoanMuc] = @MaKhoanMuc,
			[TenKhoanMuc] = @TenKhoanMuc,
			[MaDVT] = @MaDVT,
			[TenDVT] = @TenDVT,
			[SoLuongOrTrongLuong] = @SoLuongOrTrongLuong,
			[DonGia] = @DonGia,
			[ThanhTien] = @ThanhTien,
			[DienGiai] = @DienGiai,
			[SoVanDon] = @SoVanDon,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[Loai] = @Loai,
			[TinhChat] = @TinhChat
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_THUPHI_BIENLAI_PHI]
		(
			[BL_ID],
			[MaKhoanMuc],
			[TenKhoanMuc],
			[MaDVT],
			[TenDVT],
			[SoLuongOrTrongLuong],
			[DonGia],
			[ThanhTien],
			[DienGiai],
			[SoVanDon],
			[SoContainer],
			[SoSeal],
			[Loai],
			[TinhChat]
		)
		VALUES 
		(
			@BL_ID,
			@MaKhoanMuc,
			@TenKhoanMuc,
			@MaDVT,
			@TenDVT,
			@SoLuongOrTrongLuong,
			@DonGia,
			@ThanhTien,
			@DienGiai,
			@SoVanDon,
			@SoContainer,
			@SoSeal,
			@Loai,
			@TinhChat
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_THUPHI_BIENLAI_PHI]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteBy_BL_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteBy_BL_ID]
	@BL_ID bigint
AS

DELETE FROM [dbo].[T_KDT_THUPHI_BIENLAI_PHI]
WHERE
	[BL_ID] = @BL_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_THUPHI_BIENLAI_PHI] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BL_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat]
FROM
	[dbo].[T_KDT_THUPHI_BIENLAI_PHI]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectBy_BL_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectBy_BL_ID]
	@BL_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BL_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat]
FROM
	[dbo].[T_KDT_THUPHI_BIENLAI_PHI]
WHERE
	[BL_ID] = @BL_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[BL_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat]
FROM [dbo].[T_KDT_THUPHI_BIENLAI_PHI] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_THUPHI_BIENLAI_PHI_SelectAll]















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BL_ID],
	[MaKhoanMuc],
	[TenKhoanMuc],
	[MaDVT],
	[TenDVT],
	[SoLuongOrTrongLuong],
	[DonGia],
	[ThanhTien],
	[DienGiai],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[Loai],
	[TinhChat]
FROM
	[dbo].[T_KDT_THUPHI_BIENLAI_PHI]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '40.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('40.9',GETDATE(), N'CẬP NHẬT KHAI BÁO THU PHÍ HẢI QUAN ')
END

GO