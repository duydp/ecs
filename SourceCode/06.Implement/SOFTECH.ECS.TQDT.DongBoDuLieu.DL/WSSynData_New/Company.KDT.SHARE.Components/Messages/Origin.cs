﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Origin
    {
        /// <summary>
        /// Mã nước xuất xứ
        /// </summary>
        [XmlElement("originCountry")]
        public string OriginCountry { set; get; }

    }
}
