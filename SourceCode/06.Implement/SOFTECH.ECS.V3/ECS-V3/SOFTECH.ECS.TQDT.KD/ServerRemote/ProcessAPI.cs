﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using SignRemote;
using System.Security.Cryptography;
using Company.KDT.SHARE.Components;
//using iTextSharp.text.log;
//using Org.BouncyCastle.Security;
//using iTextSharp.text.pdf.security;
//using iTextSharp.text.pdf;
using System.IO;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Data;
using System.Windows.Forms;

namespace ServerRemote
{

    class ProcessAPI
    {
        public event EventHandler<SignEventArgs> Sign;
        public event EventHandler<SignFileEventArgs> FileSign;
        private TimeSpan _usedTime = new TimeSpan();
        private DateTime _lastStartTime;
        private List<MessageSend> _queueMessages = new List<MessageSend>();
        private List<MessageSendFile> _queueMessagesFile = new List<MessageSendFile>();
        public List<INBOX> InBoxCollection;
        public List<OUTBOX> OutBoxCollection;
        public bool _flagSign = false;
        public string FileName;
        public string FileData;
        public System.IO.FileStream fs;
        public byte[] bytes;
        public string filebase64 = "";
        public long filesize = 0;
        public long size;
        public byte[] data;
        public string msgFeedBack;
        public bool IsAbort = false;
        public TimeSpan TotalUsedTime
        {
            get
            {
                return _usedTime.Add(DateTime.Now - _lastStartTime);
            }
        }
        private void OnSign(SignEventArgs e)
        {
            if (Sign != null)
            {
                Sign(this, e);
            }
        }
        private void OnSignFile(SignFileEventArgs e)
        {
            if (FileSign != null)
            {
                FileSign(this, e);
            }
        }
        private bool _isSignLocal = false;
        public ProcessAPI(bool isSignLocal)
        {
            _isSignLocal = isSignLocal;
        }

        public void DoProcess()
        {
            if (!_isSignLocal)
                ThreadPool.QueueUserWorkItem(DoGetContent);
            else ThreadPool.QueueUserWorkItem(DoGetContentLocal);//ThreadPool.QueueUserWorkItem(DoGetContentLocal);

        }
        //public void DoProcessFile()
        //{
        //    if (!_isSignLocal)
        //        ThreadPool.QueueUserWorkItem(DoGetContentFile);
        //    else ThreadPool.QueueUserWorkItem(DoGetContentLocalFile);//ThreadPool.QueueUserWorkItem(DoGetContentLocal);

        //}
        public void DoProcessNew()
        {
            ThreadPool.QueueUserWorkItem(DoGetContentLocalNew);//ThreadPool.QueueUserWorkItem(DoGetContentLocal);
        }
        private void DoProcessContentLocal(object obj)
        {
            while (!IsAbort)
            {
                try
                {
                    foreach (INBOX inBoxRoot in InBoxCollection)
                    {
                        if (inBoxRoot != null)
                        {
                            MessageSend msgSend = new MessageSend()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                SendStatus = SendStatusInfo.Request,
                                Message = new SignRemote.Message()
                                {
                                    Content = inBoxRoot.MSG_ORIGIN,
                                    GuidStr = inBoxRoot.GUIDSTR
                                },
                                TypeMessages = inBoxRoot.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                            };
                            _queueMessages.Add(msgSend);
                            if (!_flagSign)
                            {
                                //ThreadPool.QueueUserWorkItem(DoSignContent);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }

        }
        ISignMessage _service = WebService.SignMessage();
        private void DoGetContent(object obj)
        {

            while (!IsAbort)
            {
                try
                {

                    //_service = WebService.SignMessage();
                    _queueMessages.Clear();
                    MessageSend msgSend = new MessageSend()
                    {
                        From = Globals.UserNameLogin,
                        Password = Globals.PasswordLogin,
                        SendStatus = SendStatusInfo.Request
                    };
                    string msgRequest = _service.ServerSend(Helpers.Serializer(msgSend));
                    msgSend = Helpers.Deserialize<MessageSend>(msgRequest);

                    if (msgSend.SendStatus == SendStatusInfo.NoSign || msgSend.SendStatus == SendStatusInfo.NoSignVNACCS)
                    {
                        msgSend.TypeMessages = msgSend.SendStatus == SendStatusInfo.NoSignVNACCS ? "VNACCS" : string.Empty;
                        _queueMessages.Add(msgSend);
                        if (!_flagSign)
                        {
                            DoSignContent();
                        }

                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 5));

                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            #region DisConnect to server

            MessageSend msg = new MessageSend()
            {
                From = Globals.UserNameLogin,
                Password = Globals.PasswordLogin,
                SendStatus = SendStatusInfo.ServerLogout
            };
            _service = WebService.SignMessage();
            _service.ServerSend(Helpers.Serializer(msg));
            #endregion
        }
        //private void DoGetContentFile(object obj)
        //{

        //    while (!IsAbort)
        //    {
        //        try
        //        {

        //            //_service = WebService.SignMessage();
        //            MessageSendFile msgSend = new MessageSendFile()
        //            {
        //                From = Globals.UserNameLogin,
        //                Password = Globals.PasswordLogin,
        //                SendStatus = SendStatusInfo.Request
        //            };
        //            string msgRequest = _service.ServerSend(Helpers.Serializer(msgSend));
        //            msgSend = Helpers.Deserialize<MessageSendFile>(msgRequest);

        //            if (msgSend.SendStatus == SendStatusInfo.NoSign || msgSend.SendStatus == SendStatusInfo.NoSignVNACCS)
        //            {
        //                msgSend.TypeMessages = msgSend.SendStatus == SendStatusInfo.NoSignVNACCS ? "VNACCS" : string.Empty;
        //                _queueMessagesFile.Add(msgSend);
        //                if (!_flagSign)
        //                {
        //                    ThreadPool.QueueUserWorkItem(DoSignContentFile);
        //                }

        //            }
        //            else
        //                Thread.Sleep(new TimeSpan(0, 0, 5));

        //        }
        //        catch (Exception ex)
        //        {
        //            OnSignFile(new SignFileEventArgs(null, TotalUsedTime, ex));
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //        }
        //    }
        //    #region DisConnect to server

        //    MessageSendFile msg = new MessageSendFile()
        //    {
        //        From = Globals.UserNameLogin,
        //        Password = Globals.PasswordLogin,
        //        SendStatus = SendStatusInfo.ServerLogout
        //    };
        //    _service = WebService.SignMessage();
        //    _service.ServerSend(Helpers.Serializer(msg));
        //    #endregion
        //}
        private void DoGetContentLocal(object obj)
        {
            while (!IsAbort)
            {
                try
                {
                    #region Code Old
                    //INBOX inBoxRoot = INBOX.GetTopDataContent();
                    //if (inBoxRoot != null)
                    //{
                    //    MessageSend msgSend = new MessageSend()
                    //    {
                    //        From = Globals.UserNameLogin,
                    //        Password = Globals.PasswordLogin,
                    //        SendStatus = SendStatusInfo.Request,
                    //        Message = new SignRemote.Message()
                    //        {
                    //            Content = inBoxRoot.MSG_ORIGIN,
                    //            GuidStr = inBoxRoot.GUIDSTR
                    //        },
                    //        TypeMessages = inBoxRoot.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                    //    };
                    //    _queueMessages.Add(msgSend);
                    //    if (!_flagSign)
                    //    {
                    //        ThreadPool.QueueUserWorkItem(DoSignContent);
                    //    }
                    //}
                    //else
                    //    Thread.Sleep(new TimeSpan(0, 0, 5));
                    #endregion
                    #region Code New
                    _queueMessages.Clear();
                    MSG_INBOX INBOX = MSG_INBOX.GetTopDataContent();
                    if (INBOX != null)
                    {
                        MessageSend msgSend = new MessageSend()
                        {
                            From = Globals.UserNameLogin,
                            Password = Globals.PasswordLogin,
                            SendStatus = SendStatusInfo.Request,
                            Message = new SignRemote.Message()
                            {
                                Content = INBOX.MSG_ORIGIN,
                                GuidStr = INBOX.GUIDSTR
                            },
                            TypeMessages = INBOX.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                        };
                        _queueMessages.Add(msgSend);
                        if (!_flagSign)
                        {
                            DoSignContent();
                        }
                    }
                    else
                        Thread.Sleep(new TimeSpan(0, 0, 5));  
                    #endregion
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }

        }
        //private void DoGetContentLocalFile(object obj)
        //{
        //    while (!IsAbort)
        //    {
        //        try
        //        {
        //            #region Code New
        //            MSG_FILE_INBOX INBOX = MSG_FILE_INBOX.GetTopDataContent();
        //            if (INBOX != null)
        //            {
        //                MessageSendFile msgSend = new MessageSendFile()
        //                {
        //                    From = Globals.UserNameLogin,
        //                    Password = Globals.PasswordLogin,
        //                    SendStatus = SendStatusInfo.Request,
        //                    Message = new SignRemote.MessageFile()
        //                    {
        //                        FileName = INBOX.FILE_NAME,
        //                        FileData = INBOX.FILE_DATA,
        //                        GuidStr = INBOX.GUIDSTR
        //                    },
        //                    TypeMessages = INBOX.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
        //                };
        //                _queueMessagesFile.Add(msgSend);
        //                if (!_flagSign)
        //                {
        //                    ThreadPool.QueueUserWorkItem(DoSignContentFile);
        //                }
        //            }
        //            else
        //                Thread.Sleep(new TimeSpan(0, 0, 5));
        //            #endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            OnSignFile(new SignFileEventArgs(null, TotalUsedTime, ex));
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //        }
        //    }

        //}
        private void DoGetContentLocalNew(object obj)
        {
            while (!IsAbort)
            {
                try
                {
                    _queueMessages.Clear();
                    foreach (OUTBOX outBoxRoot in OutBoxCollection)
                    {
                        if (outBoxRoot != null)
                        {
                            MessageSend msgSend = new MessageSend()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                SendStatus = SendStatusInfo.Request,
                                Message = new SignRemote.Message()
                                {
                                    Content = outBoxRoot.MSG_ORIGIN,
                                    GuidStr = outBoxRoot.GUIDSTR
                                },
                                TypeMessages = outBoxRoot.STATUS == SendStatusInfo.SendVNACCS ? "VNACCS" : string.Empty,
                            };
                            _queueMessages.Add(msgSend);
                            if (!_flagSign)
                            {
                                ThreadPool.QueueUserWorkItem(DoSignContentNew);
                            }
                        }
                        else
                            Thread.Sleep(new TimeSpan(0, 0, 5)); 
                    }
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }

        }
        public void DoSignContentNew(object obj)
        {
            _flagSign = _queueMessages.Count != 0;
            while (!IsAbort && _flagSign)
            {
                try
                {
                    bool isSignVNACCS = false;
                    int index = 0;
                    MessageSend msgSend = _queueMessages[index];
                    this._lastStartTime = DateTime.Now;

                    #region SignData
                    isSignVNACCS = msgSend.TypeMessages == "VNACCS";

                    string content;
                    MessageSignature signInfo;
                    if (!isSignVNACCS)
                    {
                        content = Helpers.ConvertFromBase64(msgSend.Message.Content);
                        signInfo = Helpers.GetSignature(content);
                    }
                    else
                    {
                        content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                        Helpers.GetSignature("Test");
                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = Cryptography.GetStoreX509Certificate2New(Globals.GetX509CertificatedName);
                        signInfo = new MessageSignature()
                        {
                            Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                        };
                    }

                     OUTBOX.UpdateDataSignLocal(msgSend.Message.GuidStr, signInfo.Data, signInfo.FileCert);
                     INBOX.DeleteDynamic("GUIDSTR ='" + msgSend.Message.GuidStr + "'");
                    #endregion
                    OnSign(new SignEventArgs(msgSend, this.TotalUsedTime, null));
                    _queueMessages.RemoveAt(index);
                    if (_queueMessages.Count == 0) _flagSign = false;
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
        }
        private void DoSignContent()
        {
            _flagSign = _queueMessages.Count != 0;
            while (!IsAbort && _flagSign)
            {
                try
                {
                    bool isSignVNACCS = false;
                    _flagSign = true;
                    int index = 0;
                    MessageSend msgSend = _queueMessages[index];
                    this._lastStartTime = DateTime.Now;

                    #region SignData
                    isSignVNACCS =  msgSend.TypeMessages == "VNACCS";

                    string content;
                    MessageSignature signInfo;
                    if (!isSignVNACCS)
                    {
                        content = msgSend.Message.Content;
                        //Kiểm tra Content có phải là Base64String
                        if (content.Length % 4 == 0)
                        {
                            if (!content.TrimStart().StartsWith("<"))
                            {
                                content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                            }
                        }
                        signInfo = Helpers.GetSignature(content);
                    }
                    else
                    {
                        content = Helpers.ConvertFromBase64VNACCS(msgSend.Message.Content);
                        System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
                        if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                        {
                            x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName);
                        }
                        else
                        {
                            msgFeedBack = "Chưa cấu hình chữ ký số để thực hiện ký số . Doanh nghiệp hãy cấu hình chữ ký số để thực hiện ký số";
                            MessageBoxControl.ShowMessage(msgFeedBack);
                            IsAbort = true;
                            return;
                        }
                        if (x509Certificate2 == null)
                        {
                            msgFeedBack = "Không tìm thấy chữ ký số . Doanh nghiệp hãy kiểm tra lại Thiết bị USB chữ ký số ";
                            MessageBoxControl.ShowMessage(msgFeedBack);
                            IsAbort = true;
                            return;
                        }
                        signInfo = new MessageSignature()
                        {
                            Data = SignRemoteVNACCS.SignVNACCS.signEdi(content, x509Certificate2, Globals.PasswordSign),
                        };
                    }

                    if (!_isSignLocal)
                    {
                        MessageSend msgReturn = new MessageSend()
                            {
                                From = Globals.UserNameLogin,
                                Password = Globals.PasswordLogin,
                                To = msgSend.From,
                                SendStatus = SendStatusInfo.Send,
                                Message = new SignRemote.Message()
                                {
                                    SignData = signInfo.Data,
                                    SignCert = signInfo.FileCert,
                                    GuidStr = msgSend.Message.GuidStr
                                }
                            };
                        _service.ServerSend(Helpers.Serializer(msgReturn));
                    }
                    else
                    {
                        #region Code New
                        MSG_OUTBOX MSG_OUTBOX = new MSG_OUTBOX();
                        MSG_OUTBOX.MSG_FROM = msgSend.From;
                        MSG_OUTBOX.MSG_TO = msgSend.To;
                        MSG_OUTBOX.CREATE_TIME = DateTime.Now;
                        // 1 : VNACCS ; 0 : V5
                        MSG_OUTBOX.STATUS = msgSend.TypeMessages == "VNACCS" ? 1 : 0;
                        MSG_OUTBOX.MSG_ORIGIN = msgSend.Message.Content;
                        MSG_OUTBOX.GUIDSTR = msgSend.Message.GuidStr;
                        MSG_OUTBOX.MSG_SIGN_DATA = signInfo.Data;
                        MSG_OUTBOX.MSG_SIGN_CERT = signInfo.FileCert;
                        MSG_OUTBOX.InsertUpdate();

                        MSG_INBOX.DeleteDynamic("GUIDSTR = '" + MSG_OUTBOX.GUIDSTR + "'");
                        #endregion
                        //OUTBOX.InsertDataSignLocal(msgSend.Message.GuidStr, signInfo.Data, signInfo.FileCert);
                        #region
                    }
                    #endregion
                    OnSign(new SignEventArgs(msgSend, this.TotalUsedTime, null));
                    _queueMessages.RemoveAt(index);
                    if (_queueMessages.Count == 0) _flagSign = false;
                    #endregion
                }
                catch (Exception ex)
                {
                    OnSign(new SignEventArgs(null, TotalUsedTime, ex));
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    IsAbort = true;
                }
            }


        }
        //private void DoSignContentFile(object obj)
        //{
        //    _flagSign = _queueMessagesFile.Count != 0;
        //    while (!IsAbort && _flagSign)
        //    {
        //        try
        //        {
        //            bool isSignVNACCS = false;
        //            int index = 0;
        //            MessageSendFile msgSend = _queueMessagesFile[index];
        //            this._lastStartTime = DateTime.Now;

        //            #region SignData
        //            if (!isSignVNACCS)
        //            {
        //                string FilePath = "";
        //                string FilePathSiged = "";
        //                FileName = msgSend.Message.FileName;
        //                FileData = msgSend.Message.FileData;

        //                string path = Application.StartupPath + "\\Temp";

        //                if (System.IO.Directory.Exists(path) == false)
        //                {
        //                    System.IO.Directory.CreateDirectory(path);
        //                }
        //                FilePath = path + "\\" + FileName;
        //                FilePathSiged = path + "\\" + FileName.Replace(".pdf","_Signed.pdf");
        //                if (System.IO.File.Exists(FilePath))
        //                {
        //                    System.IO.File.Delete(FilePath);
        //                }
        //                if (System.IO.File.Exists(FilePathSiged))
        //                {
        //                    System.IO.File.Delete(FilePathSiged);
        //                }
        //                fs = new System.IO.FileStream(FilePath, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
        //                bytes = System.Convert.FromBase64String(FileData);
        //                fs.Write(bytes, 0, bytes.Length);
        //                fs.Close();
        //                if (SignFile(FilePath, FilePathSiged))
        //                {
        //                    _flagSign = true;
        //                    fs = new System.IO.FileStream(FilePathSiged, System.IO.FileMode.Open, System.IO.FileAccess.Read);
        //                    size = 0;
        //                    size = fs.Length;
        //                    data = new byte[fs.Length];
        //                    fs.Read(data, 0, data.Length);
        //                    filebase64 = System.Convert.ToBase64String(data);
        //                    fs.Flush();
        //                    fs.Close();
        //                    FileName = FileName.Replace(".pdf", "_Signed.pdf");
        //                    FileData = filebase64;
        //                }
        //            }
        //            if (!_isSignLocal)
        //            {
        //                MessageSendFile msgReturn = new MessageSendFile()
        //                {
        //                    From = Globals.UserNameLogin,
        //                    Password = Globals.PasswordLogin,
        //                    To = msgSend.From,
        //                    SendStatus = SendStatusInfo.Send,
        //                    Message = new SignRemote.MessageFile()
        //                    {
        //                        FileName = FileName,
        //                        FileData = FileData,
        //                        GuidStr = msgSend.Message.GuidStr
        //                    }
        //                };
        //                _service.ServerSend(Helpers.Serializer(msgReturn));
        //            }
        //            else
        //            {
        //                #region Code

        //                MSG_FILE_OUTBOX MSG_FILE_OUTBOX = new MSG_FILE_OUTBOX();
        //                MSG_FILE_OUTBOX.GUIDSTR = msgSend.Message.GuidStr;
        //                MSG_FILE_OUTBOX.FILE_NAME = FileName;
        //                MSG_FILE_OUTBOX.FILE_DATA = FileData;
        //                MSG_FILE_OUTBOX.CREATE_TIME = DateTime.Now;
        //                MSG_FILE_OUTBOX.InsertUpdate();
        //                #endregion
        //            }
        //            OnSignFile(new SignFileEventArgs(msgSend, this.TotalUsedTime, null));
        //            _queueMessagesFile.RemoveAt(index);
        //            if (_queueMessagesFile.Count == 0) _flagSign = false;
        //            #endregion
        //        }
        //        catch (Exception ex)
        //        {
        //            OnSignFile(new SignFileEventArgs(null, TotalUsedTime, ex));
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //        }
        //    }


        //}
        //public void SignPDF(String src, String dest,
        //                 ICollection<Org.BouncyCastle.X509.X509Certificate> chain, X509Certificate2 pk,
        //                 String digestAlgorithm, CryptoStandard subfilter,
        //                 String reason, String location,
        //                 ICollection<ICrlClient> crlList,
        //                 IOcspClient ocspClient,
        //                 ITSAClient tsaClient,
        //                 int estimatedSize)
        //{
        //    // Creating the reader and the stamper
        //    PdfReader reader = null;
        //    PdfStamper stamper = null;
        //    FileStream os = null;
        //    try
        //    {
        //        reader = new PdfReader(src);
        //        os = new FileStream(dest, FileMode.Create);
        //        stamper = PdfStamper.CreateSignature(reader, os, '\0');
        //        // Creating the appearance
        //        PdfSignatureAppearance appearance = stamper.SignatureAppearance;
        //        //appearance.Reason = reason;
        //        //appearance.Location = location;
        //        appearance.SetVisibleSignature(new iTextSharp.text.Rectangle(220, 150, 446, 224), 1, "Signed");
        //        appearance.SignatureRenderingMode = PdfSignatureAppearance.RenderingMode.NAME_AND_DESCRIPTION;
        //        appearance.Layer2Font = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN);

        //        // Creating the signature
        //        IExternalSignature pks = new X509Certificate2Signature(pk, digestAlgorithm);
        //        //
        //        RSACryptoServiceProvider rsa = (RSACryptoServiceProvider)pk.PrivateKey;

        //        CspParameters cspp = new CspParameters();
        //        cspp.KeyContainerName = rsa.CspKeyContainerInfo.KeyContainerName;
        //        cspp.ProviderName = rsa.CspKeyContainerInfo.ProviderName;
        //        // cspp.ProviderName = "Microsoft Smart Card Key Storage Provider";

        //        cspp.ProviderType = rsa.CspKeyContainerInfo.ProviderType;
        //        string password = Company.KDT.SHARE.Components.Globals.PasswordSign.ToString();
        //        var secure = new SecureString();
        //        foreach (char c in password)
        //        {
        //            secure.AppendChar(c);
        //        }

        //        cspp.KeyPassword = secure;
        //        cspp.Flags = CspProviderFlags.UseExistingKey;

        //        RSACryptoServiceProvider rsa2 = new RSACryptoServiceProvider(cspp);
        //        rsa.PersistKeyInCsp = true;
        //        MakeSignature.SignDetached(appearance, pks, chain, crlList, ocspClient, tsaClient, estimatedSize,
        //                                   subfilter);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //    finally
        //    {
        //        if (reader != null)
        //            reader.Close();
        //        if (stamper != null)
        //            stamper.Close();
        //        if (os != null)
        //            os.Close();
        //    }
        //}
        //private bool SignFile(String src, String dest)
        //{
        //    try
        //    {
        //        #region
        //        LoggerFactory.GetInstance().SetLogger(new SysoLogger());


        //        X509Store x509Store = new X509Store("My");
        //        x509Store.Open(OpenFlags.ReadOnly);
        //        X509Certificate2Collection certificates = x509Store.Certificates;
        //        IList<Org.BouncyCastle.X509.X509Certificate> chain = new List<Org.BouncyCastle.X509.X509Certificate>();
        //        X509Certificate2 pk = null;
        //        string items = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChuKySo","");//cbSigns.SelectedValue.ToString();
        //        //ItemSign items = (ItemSign)cbSigns.SelectedValue;
        //        if (certificates.Count > 0)
        //        {
        //            foreach (X509Certificate2 cer in certificates)
        //            {

        //                if (cer.SubjectName.Name == items)
        //                {
        //                    X509Certificate2Collection certificateCollection = new X509Certificate2Collection();
        //                    certificateCollection.Add(cer);
        //                    X509Certificate2Enumerator certificatesEn = certificateCollection.GetEnumerator();
        //                    certificatesEn.MoveNext();
        //                    pk = certificatesEn.Current;

        //                    X509Chain x509chain = new X509Chain();
        //                    x509chain.Build(pk);

        //                    foreach (X509ChainElement x509ChainElement in x509chain.ChainElements)
        //                    {
        //                        chain.Add(DotNetUtilities.FromX509Certificate(x509ChainElement.Certificate));
        //                    }
        //                }
        //            }
        //        }
        //        x509Store.Close();


        //        IOcspClient ocspClient = new OcspClientBouncyCastle();
        //        ITSAClient tsaClient = null;
        //        for (int i = 0; i < chain.Count; i++)
        //        {
        //            Org.BouncyCastle.X509.X509Certificate cert = chain[i];
        //            String tsaUrl = CertificateUtil.GetTSAURL(cert);
        //            if (tsaUrl != null)
        //            {
        //                tsaClient = new TSAClientBouncyCastle(tsaUrl);
        //                break;
        //            }
        //        }
        //        IList<ICrlClient> crlList = new List<ICrlClient>();
        //        crlList.Add(new CrlClientOnline(chain));
        //        string item = Company.KDT.SHARE.Components.Globals.ChuKySo; ;//cbSigns.SelectedValue.ToString();
        //        //ItemSign item = (ItemSign)cbSigns.SelectedValue;
        //        SignPDF(src, dest, chain, pk, DigestAlgorithms.SHA1, CryptoStandard.CMS, "Test",
        // "Ghent",
        // crlList, ocspClient, tsaClient, 0);
        //        return true;
        //        #endregion
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //    }
        //}
    }
}
