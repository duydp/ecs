/****** Script for SelectTopNRows command from SSMS  ******/
--USE [ECS_TQDT_GC_V4]
--GO

IF (select COUNT(*) from information_schema.COLUMNS where TABLE_NAME = 't_HaiQuan_Cuc' AND COLUMN_NAME = 'ServicePathV4') = 0
	BEGIN
		ALTER TABLE [dbo].[t_HaiQuan_Cuc] ADD ServicePathV4 varchar(50) NULL
	END

GO
--Ha noi
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '203.162.250.146', ServicePathV4 = 'TNTTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '01'
--Hai phong
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '203.210.158.232', ServicePathV4 = 'TNTTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '03'
--Ba Ria - Vung Tau
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '113.166.121.102', ServicePathV4 = 'KDTServiceV4/CISService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '51'
--Dong Nai
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = 'www.kbdt-hqdn.vn', ServicePathV4 = 'TQDTV4/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '47'
--Binh Phuoc
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '123.30.23.5', ServicePathV4 = 'KDTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '61'
--Long An
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '222.253.252.170', ServicePathV4 = 'KDTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '48'
--Thua Thien hue
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '203.210.244.139', ServicePathV4 = 'TNTTService_V4/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '33'
--Da Nang
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '113.160.224.26', ServicePathV4 = 'TQDT_V4/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '34'
--Quang Nam
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '123.30.23.5', ServicePathV4 = 'KDTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '60'
--Thanh Hoa
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '113.160.180.180', ServicePathV4 = 'TNTTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '60'
--Binh Dinh
UPDATE [dbo].[t_HaiQuan_Cuc] SET IPService = '113.161.0.127', ServicePathV4 = 'KDTService/CisService.asmx', IsDefault = 'V4', [DateCreated] = GETDATE(), [DateModified] = GETDATE() WHERE ID = '37'

GO
--USE [ECS_TQDT_GC_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_SelectAll]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_SelectDynamic]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Load]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_DeleteDynamic]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Delete]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_InsertUpdate]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Update]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_Update]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Insert]    Script Date: 02/06/2013 09:36:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_Cuc_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_Cuc_Insert]
GO

--USE [ECS_TQDT_GC_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_SelectAll]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified],
	[ServicePathV4]
FROM
	[dbo].[t_HaiQuan_Cuc]	


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_SelectDynamic]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified],
	[ServicePathV4]
FROM [dbo].[t_HaiQuan_Cuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Load]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_Load]
	@ID nvarchar(2)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified],
	[ServicePathV4]
FROM
	[dbo].[t_HaiQuan_Cuc]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_DeleteDynamic]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_Cuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Delete]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_Delete]
	@ID nvarchar(2)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_Cuc]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_InsertUpdate]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_InsertUpdate]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime,
	@ServicePathV4 varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_Cuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_Cuc] 
		SET
			[Ten] = @Ten,
			[IPService] = @IPService,
			[ServicePathV1] = @ServicePathV1,
			[ServicePathV2] = @ServicePathV2,
			[ServicePathV3] = @ServicePathV3,
			[IPServiceCKS] = @IPServiceCKS,
			[ServicePathCKS] = @ServicePathCKS,
			[IsDefault] = @IsDefault,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified,
			[ServicePathV4] = @ServicePathV4
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_Cuc]
	(
			[ID],
			[Ten],
			[IPService],
			[ServicePathV1],
			[ServicePathV2],
			[ServicePathV3],
			[IPServiceCKS],
			[ServicePathCKS],
			[IsDefault],
			[DateCreated],
			[DateModified],
			[ServicePathV4]
	)
	VALUES
	(
			@ID,
			@Ten,
			@IPService,
			@ServicePathV1,
			@ServicePathV2,
			@ServicePathV3,
			@IPServiceCKS,
			@ServicePathCKS,
			@IsDefault,
			@DateCreated,
			@DateModified,
			@ServicePathV4
	)	
	END

GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Update]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_Update]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime,
	@ServicePathV4 varchar(50)
AS

UPDATE
	[dbo].[t_HaiQuan_Cuc]
SET
	[Ten] = @Ten,
	[IPService] = @IPService,
	[ServicePathV1] = @ServicePathV1,
	[ServicePathV2] = @ServicePathV2,
	[ServicePathV3] = @ServicePathV3,
	[IPServiceCKS] = @IPServiceCKS,
	[ServicePathCKS] = @ServicePathCKS,
	[IsDefault] = @IsDefault,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified,
	[ServicePathV4] = @ServicePathV4
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_Cuc_Insert]    Script Date: 02/06/2013 09:36:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_Cuc_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, February 06, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_Cuc_Insert]
	@ID nvarchar(2),
	@Ten nvarchar(50),
	@IPService varchar(150),
	@ServicePathV1 varchar(50),
	@ServicePathV2 varchar(50),
	@ServicePathV3 varchar(50),
	@IPServiceCKS varchar(150),
	@ServicePathCKS varchar(50),
	@IsDefault varchar(3),
	@DateCreated datetime,
	@DateModified datetime,
	@ServicePathV4 varchar(50)
AS
INSERT INTO [dbo].[t_HaiQuan_Cuc]
(
	[ID],
	[Ten],
	[IPService],
	[ServicePathV1],
	[ServicePathV2],
	[ServicePathV3],
	[IPServiceCKS],
	[ServicePathCKS],
	[IsDefault],
	[DateCreated],
	[DateModified],
	[ServicePathV4]
)
VALUES
(
	@ID,
	@Ten,
	@IPService,
	@ServicePathV1,
	@ServicePathV2,
	@ServicePathV3,
	@IPServiceCKS,
	@ServicePathCKS,
	@IsDefault,
	@DateCreated,
	@DateModified,
	@ServicePathV4
)


GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '6.8', [Date] = GETDATE(), Notes = N'Cap nhat dia chi khai bao V4'