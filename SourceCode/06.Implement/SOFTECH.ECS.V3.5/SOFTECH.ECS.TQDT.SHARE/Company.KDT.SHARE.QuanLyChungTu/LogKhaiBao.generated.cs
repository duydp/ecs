using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class LogKhaiBao
	{
		#region Properties.
		
		public long IDLog { set; get; }
		public string LoaiKhaiBao { set; get; }
		public long ID_DK { set; get; }
		public string GUIDSTR_DK { set; get; }
		public string UserNameKhaiBao { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public string UserNameSuaDoi { set; get; }
		public DateTime NgaySuaDoi { set; get; }
		public string GhiChu { set; get; }
		public bool IsDelete { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LogKhaiBao Load(long iDLog)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, iDLog);
			LogKhaiBao entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new LogKhaiBao();
				if (!reader.IsDBNull(reader.GetOrdinal("IDLog"))) entity.IDLog = reader.GetInt64(reader.GetOrdinal("IDLog"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKhaiBao"))) entity.LoaiKhaiBao = reader.GetString(reader.GetOrdinal("LoaiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DK"))) entity.ID_DK = reader.GetInt64(reader.GetOrdinal("ID_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR_DK"))) entity.GUIDSTR_DK = reader.GetString(reader.GetOrdinal("GUIDSTR_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameKhaiBao"))) entity.UserNameKhaiBao = reader.GetString(reader.GetOrdinal("UserNameKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameSuaDoi"))) entity.UserNameSuaDoi = reader.GetString(reader.GetOrdinal("UserNameSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySuaDoi"))) entity.NgaySuaDoi = reader.GetDateTime(reader.GetOrdinal("NgaySuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsDelete"))) entity.IsDelete = reader.GetBoolean(reader.GetOrdinal("IsDelete"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LogKhaiBao> SelectCollectionAll()
		{
			List<LogKhaiBao> collection = new List<LogKhaiBao>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				LogKhaiBao entity = new LogKhaiBao();
				
				if (!reader.IsDBNull(reader.GetOrdinal("IDLog"))) entity.IDLog = reader.GetInt64(reader.GetOrdinal("IDLog"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKhaiBao"))) entity.LoaiKhaiBao = reader.GetString(reader.GetOrdinal("LoaiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DK"))) entity.ID_DK = reader.GetInt64(reader.GetOrdinal("ID_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR_DK"))) entity.GUIDSTR_DK = reader.GetString(reader.GetOrdinal("GUIDSTR_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameKhaiBao"))) entity.UserNameKhaiBao = reader.GetString(reader.GetOrdinal("UserNameKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameSuaDoi"))) entity.UserNameSuaDoi = reader.GetString(reader.GetOrdinal("UserNameSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySuaDoi"))) entity.NgaySuaDoi = reader.GetDateTime(reader.GetOrdinal("NgaySuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsDelete"))) entity.IsDelete = reader.GetBoolean(reader.GetOrdinal("IsDelete"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LogKhaiBao> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<LogKhaiBao> collection = new List<LogKhaiBao>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				LogKhaiBao entity = new LogKhaiBao();
				
				if (!reader.IsDBNull(reader.GetOrdinal("IDLog"))) entity.IDLog = reader.GetInt64(reader.GetOrdinal("IDLog"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKhaiBao"))) entity.LoaiKhaiBao = reader.GetString(reader.GetOrdinal("LoaiKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_DK"))) entity.ID_DK = reader.GetInt64(reader.GetOrdinal("ID_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR_DK"))) entity.GUIDSTR_DK = reader.GetString(reader.GetOrdinal("GUIDSTR_DK"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameKhaiBao"))) entity.UserNameKhaiBao = reader.GetString(reader.GetOrdinal("UserNameKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserNameSuaDoi"))) entity.UserNameSuaDoi = reader.GetString(reader.GetOrdinal("UserNameSuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgaySuaDoi"))) entity.NgaySuaDoi = reader.GetDateTime(reader.GetOrdinal("NgaySuaDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsDelete"))) entity.IsDelete = reader.GetBoolean(reader.GetOrdinal("IsDelete"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertLogKhaiBao(string loaiKhaiBao, long iD_DK, string gUIDSTR_DK, string userNameKhaiBao, DateTime ngayKhaiBao, string userNameSuaDoi, DateTime ngaySuaDoi, string ghiChu, bool isDelete)
		{
			LogKhaiBao entity = new LogKhaiBao();	
			entity.LoaiKhaiBao = loaiKhaiBao;
			entity.ID_DK = iD_DK;
			entity.GUIDSTR_DK = gUIDSTR_DK;
			entity.UserNameKhaiBao = userNameKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.UserNameSuaDoi = userNameSuaDoi;
			entity.NgaySuaDoi = ngaySuaDoi;
			entity.GhiChu = ghiChu;
			entity.IsDelete = isDelete;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@IDLog", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LoaiKhaiBao", SqlDbType.VarChar, LoaiKhaiBao);
			db.AddInParameter(dbCommand, "@ID_DK", SqlDbType.BigInt, ID_DK);
			db.AddInParameter(dbCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, GUIDSTR_DK);
			db.AddInParameter(dbCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, UserNameKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, UserNameSuaDoi);
			db.AddInParameter(dbCommand, "@NgaySuaDoi", SqlDbType.DateTime, NgaySuaDoi.Year <= 1753 ? DBNull.Value : (object) NgaySuaDoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsDelete", SqlDbType.Bit, IsDelete);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				IDLog = (long) db.GetParameterValue(dbCommand, "@IDLog");
				return IDLog;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				IDLog = (long) db.GetParameterValue(dbCommand, "@IDLog");
				return IDLog;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LogKhaiBao item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLogKhaiBao(long iDLog, string loaiKhaiBao, long iD_DK, string gUIDSTR_DK, string userNameKhaiBao, DateTime ngayKhaiBao, string userNameSuaDoi, DateTime ngaySuaDoi, string ghiChu, bool isDelete)
		{
			LogKhaiBao entity = new LogKhaiBao();			
			entity.IDLog = iDLog;
			entity.LoaiKhaiBao = loaiKhaiBao;
			entity.ID_DK = iD_DK;
			entity.GUIDSTR_DK = gUIDSTR_DK;
			entity.UserNameKhaiBao = userNameKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.UserNameSuaDoi = userNameSuaDoi;
			entity.NgaySuaDoi = ngaySuaDoi;
			entity.GhiChu = ghiChu;
			entity.IsDelete = isDelete;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_LogKhaiBao_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, IDLog);
			db.AddInParameter(dbCommand, "@LoaiKhaiBao", SqlDbType.VarChar, LoaiKhaiBao);
			db.AddInParameter(dbCommand, "@ID_DK", SqlDbType.BigInt, ID_DK);
			db.AddInParameter(dbCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, GUIDSTR_DK);
			db.AddInParameter(dbCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, UserNameKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year == 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, UserNameSuaDoi);
			db.AddInParameter(dbCommand, "@NgaySuaDoi", SqlDbType.DateTime, NgaySuaDoi.Year == 1753 ? DBNull.Value : (object) NgaySuaDoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsDelete", SqlDbType.Bit, IsDelete);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LogKhaiBao item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLogKhaiBao(long iDLog, string loaiKhaiBao, long iD_DK, string gUIDSTR_DK, string userNameKhaiBao, DateTime ngayKhaiBao, string userNameSuaDoi, DateTime ngaySuaDoi, string ghiChu, bool isDelete)
		{
			LogKhaiBao entity = new LogKhaiBao();			
			entity.IDLog = iDLog;
			entity.LoaiKhaiBao = loaiKhaiBao;
			entity.ID_DK = iD_DK;
			entity.GUIDSTR_DK = gUIDSTR_DK;
			entity.UserNameKhaiBao = userNameKhaiBao;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.UserNameSuaDoi = userNameSuaDoi;
			entity.NgaySuaDoi = ngaySuaDoi;
			entity.GhiChu = ghiChu;
			entity.IsDelete = isDelete;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, IDLog);
			db.AddInParameter(dbCommand, "@LoaiKhaiBao", SqlDbType.VarChar, LoaiKhaiBao);
			db.AddInParameter(dbCommand, "@ID_DK", SqlDbType.BigInt, ID_DK);
			db.AddInParameter(dbCommand, "@GUIDSTR_DK", SqlDbType.NVarChar, GUIDSTR_DK);
			db.AddInParameter(dbCommand, "@UserNameKhaiBao", SqlDbType.NVarChar, UserNameKhaiBao);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year == 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@UserNameSuaDoi", SqlDbType.NVarChar, UserNameSuaDoi);
			db.AddInParameter(dbCommand, "@NgaySuaDoi", SqlDbType.DateTime, NgaySuaDoi.Year == 1753 ? DBNull.Value : (object) NgaySuaDoi);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@IsDelete", SqlDbType.Bit, IsDelete);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LogKhaiBao item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLogKhaiBao(long iDLog)
		{
			LogKhaiBao entity = new LogKhaiBao();
			entity.IDLog = iDLog;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@IDLog", SqlDbType.BigInt, IDLog);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_LogKhaiBao_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<LogKhaiBao> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LogKhaiBao item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}