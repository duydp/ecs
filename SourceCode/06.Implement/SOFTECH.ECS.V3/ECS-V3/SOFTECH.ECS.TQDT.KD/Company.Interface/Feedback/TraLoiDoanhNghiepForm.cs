﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.Feedback.Components;

namespace Company.Interface.Feedback
{
    public partial class TraLoiDoanhNghiepForm : BaseForm
    {
        private DataTable dtCauHoiDoanhNghiep = FeedbackGlobalSettings.DataSetFeedbackGlobal.Tables["CauHoiDoanhNghiep"];
        public TraLoiDoanhNghiepForm()
        {
            InitializeComponent();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cauhoiDoanhNghiep1_CauHoiDNSelectionChanged(object sender, EventArgs e)
        {
            LoadCauHoiDoanhNghiep();
        }
        private void LoadCauHoiDoanhNghiep()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                DataRow rowSelected = cauhoiDoanhNghiep1.CurrentRow();

                if (rowSelected == null)
                {
                    txtNoiDungCauHoi.Text ="";
                    return;
                }

                //Delete in memory
                if (dtCauHoiDoanhNghiep == null)
                    return;
                DataRow[] rows = dtCauHoiDoanhNghiep.Select("MaCauHoi = '" + rowSelected["MaCauHoi"] + "'");

                if (rows.Length > 0)
                {
                    txtNoiDungCauHoi.TextChanged -= new EventHandler(textBox_TextChanged);
                 
                    txtNoiDungCauHoi.Text = rows[0]["NoiDungCauHoi"].ToString();
                    
                    txtNoiDungCauHoi.TextChanged += new EventHandler(textBox_TextChanged);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Globals.ShowMessage(ex.Message, false);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        private void textBox_TextChanged(object sender, EventArgs e)
        {
            SetChanged(true);
        }


    }
}
