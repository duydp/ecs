-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Insert]
	@TKMD_ID bigint,
	@SoTT int,
	@MaTenDieuChinh varchar(1),
	@MaPhanLoaiDieuChinh varchar(3),
	@MaTTDieuChinhTriGia varchar(3),
	@TriGiaKhoanDieuChinh numeric(24, 4),
	@TongHeSoPhanBo numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
(
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@SoTT,
	@MaTenDieuChinh,
	@MaPhanLoaiDieuChinh,
	@MaTTDieuChinhTriGia,
	@TriGiaKhoanDieuChinh,
	@TongHeSoPhanBo,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaTenDieuChinh varchar(1),
	@MaPhanLoaiDieuChinh varchar(3),
	@MaTTDieuChinhTriGia varchar(3),
	@TriGiaKhoanDieuChinh numeric(24, 4),
	@TongHeSoPhanBo numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoTT] = @SoTT,
	[MaTenDieuChinh] = @MaTenDieuChinh,
	[MaPhanLoaiDieuChinh] = @MaPhanLoaiDieuChinh,
	[MaTTDieuChinhTriGia] = @MaTTDieuChinhTriGia,
	[TriGiaKhoanDieuChinh] = @TriGiaKhoanDieuChinh,
	[TongHeSoPhanBo] = @TongHeSoPhanBo,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoTT int,
	@MaTenDieuChinh varchar(1),
	@MaPhanLoaiDieuChinh varchar(3),
	@MaTTDieuChinhTriGia varchar(3),
	@TriGiaKhoanDieuChinh numeric(24, 4),
	@TongHeSoPhanBo numeric(24, 4),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoTT] = @SoTT,
			[MaTenDieuChinh] = @MaTenDieuChinh,
			[MaPhanLoaiDieuChinh] = @MaPhanLoaiDieuChinh,
			[MaTTDieuChinhTriGia] = @MaTTDieuChinhTriGia,
			[TriGiaKhoanDieuChinh] = @TriGiaKhoanDieuChinh,
			[TongHeSoPhanBo] = @TongHeSoPhanBo,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
		(
			[TKMD_ID],
			[SoTT],
			[MaTenDieuChinh],
			[MaPhanLoaiDieuChinh],
			[MaTTDieuChinhTriGia],
			[TriGiaKhoanDieuChinh],
			[TongHeSoPhanBo],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@SoTT,
			@MaTenDieuChinh,
			@MaPhanLoaiDieuChinh,
			@MaTTDieuChinhTriGia,
			@TriGiaKhoanDieuChinh,
			@TongHeSoPhanBo,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_KhoanDieuChinh] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_KhoanDieuChinh_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[SoTT],
	[MaTenDieuChinh],
	[MaPhanLoaiDieuChinh],
	[MaTTDieuChinhTriGia],
	[TriGiaKhoanDieuChinh],
	[TongHeSoPhanBo],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_KhoanDieuChinh]	

GO

