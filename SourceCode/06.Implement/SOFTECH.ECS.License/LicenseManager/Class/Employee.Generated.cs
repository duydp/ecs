using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Licenses_v2.Class
{
    public partial class Employee
    {
        #region Private members.

        protected string _ID = string.Empty;
        protected string _FirstName = string.Empty;
        protected string _LastName = string.Empty;
        protected string _UserName = string.Empty;
        protected string _PassWord = string.Empty;
        protected string _Department = string.Empty;
        protected string _Title = string.Empty;
        protected string _Email = string.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string FirstName
        {
            set { this._FirstName = value; }
            get { return this._FirstName; }
        }

        public string LastName
        {
            set { this._LastName = value; }
            get { return this._LastName; }
        }

        public string UserName
        {
            set { this._UserName = value; }
            get { return this._UserName; }
        }

        public string PassWord
        {
            set { this._PassWord = value; }
            get { return this._PassWord; }
        }

        public string Department
        {
            set { this._Department = value; }
            get { return this._Department; }
        }

        public string Title
        {
            set { this._Title = value; }
            get { return this._Title; }
        }

        public string Email
        {
            set { this._Email = value; }
            get { return this._Email; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static Employee Load(string iD)
        {
            const string spName = "[dbo].[p_LICENSE_Employee_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, iD);
            Employee entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new Employee();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("FirstName"))) entity.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
                if (!reader.IsDBNull(reader.GetOrdinal("LastName"))) entity.LastName = reader.GetString(reader.GetOrdinal("LastName"));
                if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
                if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
                if (!reader.IsDBNull(reader.GetOrdinal("Department"))) entity.Department = reader.GetString(reader.GetOrdinal("Department"));
                if (!reader.IsDBNull(reader.GetOrdinal("Title"))) entity.Title = reader.GetString(reader.GetOrdinal("Title"));
                if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_Employee_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_Employee_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_Employee_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_Employee_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static string InsertEmployee(string firstName, string lastName, string userName, string passWord, string department, string title, string email)
        {
            Employee entity = new Employee();
            entity.FirstName = firstName;
            entity.LastName = lastName;
            entity.UserName = userName;
            entity.PassWord = passWord;
            entity.Department = department;
            entity.Title = title;
            entity.Email = email;
            return entity.Insert();
        }

        public string Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public string Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Employee_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@FirstName", SqlDbType.NVarChar, FirstName);
            db.AddInParameter(dbCommand, "@LastName", SqlDbType.NVarChar, LastName);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
            db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, PassWord);
            db.AddInParameter(dbCommand, "@Department", SqlDbType.NVarChar, Department);
            db.AddInParameter(dbCommand, "@Title", SqlDbType.NVarChar, Title);
            db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateEmployee(string iD, string firstName, string lastName, string userName, string passWord, string department, string title, string email)
        {
            Employee entity = new Employee();
            entity.FirstName = firstName;
            entity.LastName = lastName;
            entity.UserName = userName;
            entity.PassWord = passWord;
            entity.Department = department;
            entity.Title = title;
            entity.Email = email;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_LICENSE_Employee_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@FirstName", SqlDbType.NVarChar, FirstName);
            db.AddInParameter(dbCommand, "@LastName", SqlDbType.NVarChar, LastName);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
            db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, PassWord);
            db.AddInParameter(dbCommand, "@Department", SqlDbType.NVarChar, Department);
            db.AddInParameter(dbCommand, "@Title", SqlDbType.NVarChar, Title);
            db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateEmployee(string iD, string firstName, string lastName, string userName, string passWord, string department, string title, string email)
        {
            Employee entity = new Employee();
            entity.ID = iD;
            entity.FirstName = firstName;
            entity.LastName = lastName;
            entity.UserName = userName;
            entity.PassWord = passWord;
            entity.Department = department;
            entity.Title = title;
            entity.Email = email;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Employee_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@FirstName", SqlDbType.NVarChar, FirstName);
            db.AddInParameter(dbCommand, "@LastName", SqlDbType.NVarChar, LastName);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
            db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, PassWord);
            db.AddInParameter(dbCommand, "@Department", SqlDbType.NVarChar, Department);
            db.AddInParameter(dbCommand, "@Title", SqlDbType.NVarChar, Title);
            db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteEmployee(string iD)
        {
            Employee entity = new Employee();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_Employee_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}