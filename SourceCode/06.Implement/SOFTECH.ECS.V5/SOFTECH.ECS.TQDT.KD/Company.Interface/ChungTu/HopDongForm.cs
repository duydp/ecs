﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.Messages.Send;
#endif

namespace Company.Interface
{
    public partial class HopDongForm : Company.Interface.BaseForm
    {

        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        public HopDongThuongMai HopDongTM;
        private FeedBackContent feedbackContent;
        private string msgInfor = string.Empty;
        public HopDongForm()
        {
            InitializeComponent();
        }

        private bool checkSoHopDong(string soHopDong)
        {
            foreach (HopDongThuongMai HDTM in TKMD.HopDongThuongMaiCollection)
            {
                if (HDTM.SoHopDongTM.Trim().ToUpper() == soHopDong.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HopDongThuongMaiDetail> HopDongThuongMaiDetailCollection = new List<HopDongThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMaiDetail hdtmdtmp = new HopDongThuongMaiDetail();
                        hdtmdtmp  = (HopDongThuongMaiDetail)i.GetRow().DataRow;
                        //hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                       // hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongThuongMaiDetail hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongThuongMaiDetail hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        private void BindData()
        {
            //Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            //hmd.TKMD_ID = TKMD.ID;
            HopDongTM.ConvertListToDataSet(HangMauDich.SelectBy_TKMD_ID(TKMD.ID).Tables[0]);
            dgList.DataSource = HopDongTM.ListHangMDOfHopDong;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (HopDongTM.ListHangMDOfHopDong.Count == 0)
            {
                ShowMessage("Chưa chọn thông tin hàng hóa.", false);
                return;
            }

            //epError.Clear();
            //if (string.IsNullOrEmpty(txtDiaDiemGiaoHang.Text))
            //{
            //    epError.SetError(txtDiaDiemGiaoHang, "Không được để trống Địa điểm giao hàng");
            //    return;
            //}
            //if (string.IsNullOrEmpty(txtTenDVMua.Text))
            //{
            //    epError.SetError(txtTenDVMua, "Không được để trống Tên đơn vị mua");
            //    return;
            //}


            //TKMD.HopDongThuongMaiCollection.Remove(HopDongTM);
            //HopDongTM.Delete();
            if (HopDongTM.ID == 0 && checkSoHopDong(txtSoHopDong.Text))
            {
                ShowMessage("Số hợp đồng này đã tồn tại.", false);
                return;
            }
            HopDongTM.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text.Trim();
            HopDongTM.DKGH_ID = cbDKGH.SelectedValue.ToString().Trim();
            HopDongTM.MaDonViBan = txtMaDVBan.Text.Trim();
            HopDongTM.MaDonViMua = txtMaDVMua.Text.Trim();
            HopDongTM.NgayHopDongTM = ccNgayHopDong.Value;
            HopDongTM.NguyenTe_ID = nguyenTeControl1.Ma;
            HopDongTM.PTTT_ID = cbPTTT.SelectedValue.ToString().Trim();
            HopDongTM.SoHopDongTM = txtSoHopDong.Text.Trim();
            HopDongTM.TenDonViBan = txtTenDVBan.Text.Trim();
            HopDongTM.TenDonViMua = txtTenDVMua.Text.Trim();
            HopDongTM.ThoiHanThanhToan = ccThoiHanTT.Value;
            HopDongTM.TongTriGia = Convert.ToDecimal(txtTongTriGia.Text);
            HopDongTM.ThongTinKhac = txtThongTinKhac.Text.Trim();
            HopDongTM.TKMD_ID = TKMD.ID;
            if (isKhaiBoSung)
                HopDongTM.LoaiKB = 1;
            else
                HopDongTM.LoaiKB = 0;
            HopDongTM.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
//             GridEXRow[] rowCollection = dgList.GetRows();
//             foreach (GridEXRow row in rowCollection)
//             {
//                 DataRowView rowview = (DataRowView)row.DataRow;
//                 foreach (HopDongThuongMaiDetail item in HopDongTM.ListHangMDOfHopDong)
//                 {
//                     if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
//                     {
//                         item.GhiChu = row.Cells["GhiChu"].Text;
// 
//                         item.MaHS = rowview["MaHS"].ToString();
//                         item.MaPhu = rowview["MaPhu"].ToString();
//                         item.TenHang = rowview["TenHang"].ToString();
//                         item.NuocXX_ID = rowview["NuocXX_ID"].ToString();
//                         item.DVT_ID = rowview["DVT_ID"].ToString();
//                         item.SoLuong = Convert.ToDecimal(rowview["SoLuong"]);
//                         item.DonGiaKB = Convert.ToDouble(rowview["DonGiaKB"]);
//                         item.TriGiaKB = Convert.ToDouble(rowview["TriGiaKB"]);
//                         item.GhiChu = rowview["GhiChu"].ToString();
//                         break;
//                     }
//                 }
//             }
            try
            {
                if (string.IsNullOrEmpty(HopDongTM.GuidStr)) HopDongTM.GuidStr = Guid.NewGuid().ToString();
                if (HopDongTM.NgayTiepNhan.Year < 1900) HopDongTM.NgayTiepNhan = new DateTime(1900, 1, 1);
                bool isAddNew = HopDongTM.ID == 0;
                HopDongTM.InsertUpdateFull();
                if (isAddNew)
                    TKMD.HopDongThuongMaiCollection.Add(HopDongTM);
                BindData();
                ShowMessage("Lưu thành công.", false);
                if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET && TKMD.SoToKhai > 0)
                    btnKhaiBao.Enabled = true;

                TKMD.SoHopDong = HopDongTM.SoHopDongTM;
                TKMD.NgayHopDong = HopDongTM.NgayHopDongTM;
                TKMD.NgayHetHanHopDong = HopDongTM.ThoiHanThanhToan;

            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }


        }
        public List<HangMauDich> ConvertListToHangMauDichCollection(List<HopDongThuongMaiDetail> listHangHopDong, List<HangMauDich> hangCollection)
        {

            List<HangMauDich> tmp = new List<HangMauDich>();

            foreach (HopDongThuongMaiDetail item in listHangHopDong)
            {
                foreach (HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void AddHangTK()
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (HopDongTM.ListHangMDOfHopDong.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(HopDongTM.ListHangMDOfHopDong, TKMD.HMDCollection);
            //}
            f.ShowDialog(this);
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HopDongThuongMaiDetail hangHopDongDetail in HopDongTM.ListHangMDOfHopDong)
                    {
                        if (hangHopDongDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HopDongThuongMaiDetail hopDongDetail = new HopDongThuongMaiDetail();
                        hopDongDetail.HMD_ID = HMD.ID;
                        hopDongDetail.HopDongTM_ID = HopDongTM.ID;
                        hopDongDetail.MaPhu = HMD.MaPhu;
                        hopDongDetail.MaHS = HMD.MaHS;
                        hopDongDetail.TenHang = HMD.TenHang;
                        hopDongDetail.DVT_ID = HMD.DVT_ID;
                        hopDongDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hopDongDetail.SoLuong = HMD.SoLuong;
                        hopDongDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hopDongDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hopDongDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);
                        HopDongTM.ListHangMDOfHopDong.Add(hopDongDetail);
                    }
                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void btnChonHang_Click(object sender, EventArgs e)
        {
            btnChonHang.ContextMenuStrip.Show(btnChonHang, new System.Drawing.Point(0, btnChonHang.Height));
        }

        private void HopDongForm_Load(object sender, EventArgs e)
        {

            //Update by KHANHHN 05/03/2012
            if (HopDongTM == null || HopDongTM.ID == 0)
                HopDongTM = new HopDongThuongMai { TrangThai = TrangThaiXuLy.CHUA_KHAI_BAO };

            // an cac button truoc khi load
            btnKhaiBao.Enabled = false;
            btnLayPhanHoi.Enabled = false;

            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.DisplayMember = cbDKGH.ValueMember = "ID";
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            

            //Start Ngonnt 25/01
            if (TKMD.DKGH_ID != "")
                cbDKGH.SelectedValue = TKMD.DKGH_ID;
            if (TKMD.PTTT_ID != "")
                cbPTTT.SelectedValue = TKMD.PTTT_ID;
            if (TKMD.NguyenTe_ID != "")
                nguyenTeControl1.Ma = TKMD.NguyenTe_ID;
            if (TKMD.CuaKhau_ID != "")
                txtDiaDiemGiaoHang.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
            //End


            //DATLMQ bổ sung lấy HĐ từ TKMD 13/12/2010
            if (HopDongTM != null && HopDongTM.ID <= 0)
            {
                HopDongTM.SoHopDongTM = TKMD.SoHopDong;
                txtSoHopDong.Text = HopDongTM.SoHopDongTM;

                HopDongTM.NgayHopDongTM = TKMD.NgayHopDong;
                ccNgayHopDong.Text = HopDongTM.NgayHopDongTM.ToLongDateString();

                HopDongTM.ThoiHanThanhToan = TKMD.NgayHetHanHopDong;
                ccThoiHanTT.Text = HopDongTM.ThoiHanThanhToan.ToLongDateString();
                //DATLMQ kiểm tra loại hình tờ khai 30/12/2010
                if (TKMD.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    txtMaDVMua.Text = TKMD.MaDoanhNghiep;
                    txtTenDVMua.Text = TKMD.TenDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDonViDoiTac;
                }
                else
                {
                    txtTenDVMua.Text = TKMD.TenDonViDoiTac;
                    txtMaDVBan.Text = TKMD.MaDoanhNghiep;
                    txtTenDVBan.Text = TKMD.TenDoanhNghiep;
                }

                txtDiaDiemGiaoHang.Text = CuaKhau.GetName(TKMD.CuaKhau_ID);
                //if (HopDongTM.ID == 0)
                //{
                //    ccThoiHanTT.Text = ccNgayHopDong.Text = DateTime.Now.ToLongDateString();
                //}
            }

            else if (HopDongTM != null && HopDongTM.ID > 0)
            {
                txtDiaDiemGiaoHang.Text = HopDongTM.DiaDiemGiaoHang;
                cbDKGH.SelectedValue = HopDongTM.DKGH_ID;
                txtMaDVBan.Text = HopDongTM.MaDonViBan;
                txtMaDVMua.Text = HopDongTM.MaDonViMua;
                ccNgayHopDong.Value = HopDongTM.NgayHopDongTM;
                nguyenTeControl1.Ma = HopDongTM.NguyenTe_ID;
                cbPTTT.SelectedValue = HopDongTM.PTTT_ID;
                txtSoHopDong.Text = HopDongTM.SoHopDongTM;
                txtTenDVBan.Text = HopDongTM.TenDonViBan;
                txtTenDVMua.Text = HopDongTM.TenDonViMua;
                ccThoiHanTT.Value = HopDongTM.ThoiHanThanhToan;
                txtTongTriGia.Text = HopDongTM.TongTriGia.ToString();
                txtThongTinKhac.Text = HopDongTM.ThongTinKhac;
                ccNgayHopDong.Text = HopDongTM.NgayHopDongTM.ToShortDateString();
                ccThoiHanTT.Text = HopDongTM.ThoiHanThanhToan.ToShortDateString();
                BindData();
            }

            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (HopDongTM.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            SetButtonStateHOPDONG(TKMD, isKhaiBoSung, HopDongTM);
            btnThuTucHQTruoc.Visible = Company.KDT.SHARE.Components.Globals.IsTest;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (HopDongTM.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }

            try
            {
                if ((HopDongTM.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO) || (HopDongTM.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET))
                    HopDongTM.GuidStr = Guid.NewGuid().ToString();
                else if (HopDongTM.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    return;
                }

                ObjectSend msgSend = SingleMessage.BoSungHopDong(TKMD, HopDongTM);

                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendMessageForm.Message.XmlSaveMessage(HopDongTM.ID, MessageTitle.KhaiBaoBoSungHopDong);
                    HopDongTM.TrangThai = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI;
                    HopDongTM.Update();
                    SetButtonStateHOPDONG(TKMD, isKhaiBoSung, HopDongTM);
                    btnLayPhanHoi_Click(null, null);
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }



        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            #region V3

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, HopDongTM.GuidStr);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateHOPDONG(TKMD,isKhaiBoSung, HopDongTM);
                }


            }
//             ObjectSend msgSend = SingleMessage.FeedBack(TKMD, HopDongTM.GuidStr);
//             SendMessageForm sendMessageForm = new SendMessageForm();
//             sendMessageForm.Send += SendMessage;
//             sendMessageForm.DoSend(msgSend);

            #endregion

        }

        private void LayPhanHoiKhaiBao(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = HopDongTM.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnKhaiBao.Enabled = true;
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHopDongThanhCong);
//                     this.ShowMessage(message, false);
// 
//                     btnKhaiBao.Enabled = false;
//                     txtSoTiepNhan.Text = this.HopDongTM.SoTiepNhan.ToString("N0");
//                     ccNgayTiepNhan.Value = this.HopDongTM.NgayTiepNhan;
//                     ccNgayTiepNhan.Text = this.HopDongTM.NgayTiepNhan.ToShortDateString();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        private void LayPhanHoiDuyet(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = HopDongTM.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan);
//                     if (message.Length == 0)
//                     {
//                         message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.HopDongTM.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
//                         txtSoTiepNhan.Text = "";
//                         ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
//                         ccNgayTiepNhan.Text = "";
//                     }
//                     else
//                         btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
//                     this.ShowMessage(message, false);
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        #region Begin VALIDATE HOP DONG

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc thông.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateHopDong()
        {
            bool isValid = true;

            //So_CT	varchar(50)
            isValid = Globals.ValidateLength(txtSoHopDong, 50, err, "Số hợp đồng");

            //Ma_PTTT	varchar(10)
            isValid &= Globals.ValidateLength(cbPTTT, 10, err, "Phương thức thanh toán");

            //Ma_GH	varchar(7)
            isValid &= Globals.ValidateLength(cbDKGH, 7, err, "Điều kiện giao hàng");

            //Ma_NT	char(3)

            //Ma_DV	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVMua, 14, err, "Mã đơn vị mua");

            //Ma_DV_DT	varchar(14)
            isValid &= Globals.ValidateLength(txtMaDVBan, 14, err, "Mã đơn vị bán");

            return isValid;
        }

        #endregion End VALIDATE HOP DONG


        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form HOP DONG.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateHOPDONG(ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMai hopdong)
        {
            if (hopdong == null)
                return false;

            if (hopdong != null)
            {
                txtSoHopDong.Text = hopdong.SoHopDongTM;
            }
            if (hopdong.LoaiKB == 1)
            {

                switch (hopdong.TrangThai)
                {
                    case -1:
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case -3:
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case 0:
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case 1:
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case 2:
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;

                }
            }
            else
                lbTrangThai.Text = string.Empty;
            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else if (tkmd.SoToKhai > 0 && TKMD.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
            {

                bool khaiBaoEnable = (hopdong.TrangThai == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     hopdong.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET);

                btnKhaiBao.Enabled = btnXoa.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = hopdong.TrangThai == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   hopdong.TrangThai == TrangThaiXuLy.DA_DUYET ||
                                   hopdong.TrangThai == TrangThaiXuLy.CHO_DUYET || hopdong.TrangThai == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
            txtSoTiepNhan.Text = hopdong.SoTiepNhan > 0 ? hopdong.SoTiepNhan.ToString() : string.Empty;
            if (hopdong.NgayTiepNhan.Year > 1900) ccNgayTiepNhan.Value = hopdong.NgayTiepNhan;
            return true;
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (HopDongTM.GuidStr != null && HopDongTM.GuidStr != "")
            {
                ThongDiepForm f = new ThongDiepForm();
                f.DeclarationIssuer = AdditionalDocumentType.HOP_DONG;
                f.ItemID = HopDongTM.ID;
                f.ShowDialog(this);
            }

            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            List<HopDongThuongMaiDetail> HopDongThuongMaiDetailCollection = new List<HopDongThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMaiDetail hdtmdtmp = new HopDongThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongThuongMaiDetail hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongThuongMaiDetail hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            List<HopDongThuongMaiDetail> HopDongThuongMaiDetailCollection = new List<HopDongThuongMaiDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongThuongMaiDetail hdtmdtmp = new HopDongThuongMaiDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hdtmdtmp.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hdtmdtmp.ID = Convert.ToInt64(row["ID"]);
                        HopDongThuongMaiDetailCollection.Add(hdtmdtmp);
                    }
                }
                foreach (HopDongThuongMaiDetail hdtmtmp in HopDongThuongMaiDetailCollection)
                {
                    try
                    {
                        if (hdtmtmp.ID > 0)
                        {
                            hdtmtmp.Delete();
                        }
                        foreach (HopDongThuongMaiDetail hdtmd in HopDongTM.ListHangMDOfHopDong)
                        {
                            if (hdtmd.HMD_ID == hdtmtmp.HMD_ID)
                            {
                                HopDongTM.ListHangMDOfHopDong.Remove(hdtmd);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                HopDongTM.TrangThai = TrangThaiXuLy.KHONG_PHE_DUYET;
                                noidung = "Hải quan từ chối tiếp nhận.\r\nLý do: " + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", HopDongTM.GuidStr), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                HopDongTM.SoTiepNhan = long.Parse(feedbackContent.CustomsReference);
                                if (feedbackContent.Acceptance.Length == 10)
                                    HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                HopDongTM.NamTiepNhan = HopDongTM.NgayTiepNhan.Year;
                                //HopDongTM.NgayTiepNhan = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.KhaiBaoBoSungHopDongDuocChapNhan, noidung);
                                HopDongTM.TrangThai = TrangThaiXuLy.CHO_DUYET;
                                noidung = "Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: " + HopDongTM.SoTiepNhan.ToString() + "\r\nNgày tiếp nhận: " + HopDongTM.NgayTiepNhan.ToString();
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.KhaiBaoBoSungHopDongThanhCong, noidung);
                                HopDongTM.TrangThai = TrangThaiXuLy.DA_DUYET;
                                noidung = "Hợp đồng đã được duyệt.\r\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(HopDongTM.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }

                    if (isUpdate)
                    {
                        HopDongTM.Update();
                        SetButtonStateHOPDONG(TKMD, isKhaiBoSung, HopDongTM);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "DVT_ID")
            {
                int val = DonViTinhID(e.Value.ToString());
                if (val != -1)
                    e.Value = Convert.ToInt32(val);
            }
        }

        private void cmdAddHangTK_Click(object sender, EventArgs e)
        {
            AddHangTK();
        }

        private void cmdAddHang_Click(object sender, EventArgs e)
        {
            AddHangHopDong f = new AddHangHopDong();
            f.NhomLoaiHinh = TKMD.MaLoaiHinh;
            if (this.HopDongTM.ListHangMDOfHopDong == null)
                HopDongTM.ListHangMDOfHopDong = new List<HopDongThuongMaiDetail>();
            f.listHopDongThuongMaiDetail = HopDongTM.ListHangMDOfHopDong;
            f.ShowDialog(this);

            dgList.DataSource = HopDongTM.ListHangMDOfHopDong;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refetch();
            }
        }

        private void cmdAddExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnThuTucHQTruoc_Click(object sender, EventArgs e)
        {
            if (HopDongTM.ID == 0)
                ShowMessage("Vui lòng lưu thông tin Hợp đồng trước khi thêm chứng từ HQ trước đó", false);
            else
            {
                ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                f.Master_ID = HopDongTM.ID;
                f.Type = "HDTM";
                f.ShowDialog(this);
            }
        }
    }
}

