﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class CopyDinhMucGCForm : BaseForm
    {
        public DinhMucCollection DMCollectionCopy = new DinhMucCollection();
        public DinhMucDangKy DMDangKy;
        public CopyDinhMucGCForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];           
        }

    
        private void DinhMucGCEditForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;           
            if (this.OpenType==OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnDelete.Enabled = false;                
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            dgList.DataSource = DMCollectionCopy;
        }
      

        private void btnClose_Click(object sender, EventArgs e)
        {           
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {            
            DinhMucCollection DMTMPCollection = new DinhMucCollection();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (ShowMessage(setText("Bạn có muốn xóa định mức của sản phẩm này không?","Do you want to delete the selected row(s)?"), true) == "Yes")
            {                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)row.GetRow().DataRow;                        
                        DMTMPCollection.Add(dm);                       
                    }
                }
                foreach (DinhMuc dmRemove in DMTMPCollection)
                {
                    DMCollectionCopy.Remove(dmRemove);
                }
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }
        

        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (DinhMuc dm in DMCollectionCopy)
            {
                DMDangKy.DMCollection.Add(dm);
            }
            this.Close();            
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa định mức của sản phẩm này không?", "Do you want to delete the selected row(s)"), true) == "Yes")
            {
                ;
            }
            else
            {
                e.Cancel = true;
            }            
        }
    
    }
}