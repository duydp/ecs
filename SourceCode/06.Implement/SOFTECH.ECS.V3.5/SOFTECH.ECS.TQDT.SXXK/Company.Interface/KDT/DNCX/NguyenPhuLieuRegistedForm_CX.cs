using System;
using System.Data;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.BLL;
using Company.Interface.Report.SXXK;
using Company.Interface.Report;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu.CX;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuRegistedForm_CX : BaseForm
    {
        public HangDuaVao NguyenPhuLieuSelected = new HangDuaVao();
        public List<HangDuaVao> NPLCollection = new List<HangDuaVao>();
        public List<HangDuaVao> NPLCollectionSelected = new List<HangDuaVao>();
        public string LoaiNPL;
        private DataSet dsRegistedList;
        public bool LoadTB_CX = false;
        public bool isBrowser = false;
        public NguyenPhuLieuRegistedForm_CX()
        {
            InitializeComponent();
        }

        private void setDataToComboUserKB()
        {
            DataTable dt = Company.QuanTri.User.SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        //public void BindData()
        //{
        //    this.NPLCollection = new NguyenPhuLieu().SelectCollectionDynamic("MaDoanhNghiep = '" + this.MaDoanhNghiep + "'", "");
        //    dgList.DataSource = this.NPLCollection;
        //}

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = Company.KDT.SHARE.Components.Globals.GlobalDanhMucChuanHQ.Tables["DonViTinh"]; //DonViTinh.SelectAll().Tables[0];

            if (this.CalledForm == "DinhMucSendForm")
            {
                //ctrDonViHaiQuan.Enabled = false;
            }

            //lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            try
            {
                lblTongNPL.Text = "0";
                //btnExportExcel.Visible = true;

                if (GlobalSettings.MA_DON_VI != "4000395355") ; //btnMapping.Visible = false;

                dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;

                setDataToComboUserKB();

                this.khoitao_DuLieuChuan();

                cbUserKB.SelectedIndex = 0;
                // Nguyên phụ liệu đã đăng ký.
                this.search();

                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        //uiButton1.Visible = false;
                        //uiButton3.Visible = false;
                        //btnDelete.Visible = false;
                    }
                }

                dgList.RootTable.RowHeaderWidth = 60;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------

        //private void btnGetListFromHQ_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;
        //        this.Cursor = Cursors.WaitCursor;
        //        this.dsRegistedList = new NguyenPhuLieu().WS_GetDanhSachDaDangKy(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        //        dgList.DataSource = this.dsRegistedList.Tables[0];
        //        // Cập nhật vào CSDL.
        //        bool ret = new NguyenPhuLieu().UpdateRegistedToDatabase(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, this.dsRegistedList);
        //        this.Cursor = Cursors.Default;
        //        if (ret)
        //        {
        //            this.ShowMessage("Cập nhật thành công", false);
        //            BindData();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Cursor = Cursors.Default;
        //        this.ShowMessage("Có lỗi: " + ex.Message, false);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrowser)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maNPL = e.Row.Cells["Ma"].Text;
                    this.NguyenPhuLieuSelected.Ma = maNPL;
                    this.NguyenPhuLieuSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.NguyenPhuLieuSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.NguyenPhuLieuSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.NguyenPhuLieuSelected.LoaiNPL = e.Row.Cells["DVT_ID"].Text;
                    this.NguyenPhuLieuSelected.MucDich = e.Row.Cells["DVT_ID"].Text;
                    this.NguyenPhuLieuSelected.Load(this.NguyenPhuLieuSelected.Ma);
                    this.Close();
                }
            }
            else
            {

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            //ImportNPLForm f = new ImportNPLForm();
            //f.NPLCollection = NPLCollection;
            //f.ShowDialog(this);
            //this.search();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
            f.ShowDialog(this);
            this.search();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //  if (ShowMessage("Bạn có muốn xóa các nguyên phụ liệu này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            {
                Delete();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void Delete()
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;

                if (items.Count <= 0) return;

                string msgLog = "";
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieu npl = (NguyenPhuLieu)i.GetRow().DataRow;

                        //TODO: Kiem tra NPL co su dung trong to khai chua?
                        List<Company.BLL.KDT.ToKhaiMauDich> suDungTK = Company.BLL.KDT.ToKhaiMauDich.CheckUseNPLInToKhai(npl.Ma, npl.MaDoanhNghiep, npl.MaHaiQuan.Trim());

                        if (suDungTK.Count > 0)
                        {
                            msgLog += string.Format(" Tờ khai [ID/ Số tờ khai/ Mã loại hình/ Năm đăng ký] = {0}/{1}/{2}/{3}\r\n", suDungTK[0].ID, suDungTK[0].SoToKhai, suDungTK[0].MaLoaiHinh, suDungTK[0].NgayDangKy.Year);
                        }

                        if (msgLog.Length == 0)
                        {
                            npl.Delete();

                            Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung.DeleteSXXK_NguyenPhuLieuBoSung(npl.MaHaiQuan.Trim(), npl.MaDoanhNghiep, npl.Ma);

                            msgLog += npl.Ma + ", ";

                            //TODO: Ghi log Xoa NPL
                            Company.KDT.SHARE.Components.Globals.SaveMessage("", 0, Guid.Empty.ToString(), MessageTypes.DanhMucNguyenPhuLieu, MessageFunctions.ChuyenTrangThaiTay, MessageTitle.XoaNguyenPhuLieu, string.Format("Xóa các Nguyên phụ liệu trong Đã đăng ký: {0}\r\nNgày xóa: {1}\r\nNgười xóa: {2}", msgLog, DateTime.Now, ((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME));
                        }
                        else
                        {
                            string msgWarning = string.Format("[Không thể xóa] do mã NPL này đang sử dụng trong các tờ khai sau:\r\n" + msgLog);
                            Globals.ShowMessage(msgWarning, false);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            SelectNPLForm f = new SelectNPLForm();
            f.ShowDialog(this);
            ReportViewNPLForm f2 = new ReportViewNPLForm();
            if (f.NPLCollectionSelect.Count > 0)
            {
                f2.NPLCollection = f.NPLCollectionSelect;
                f2.Show();
            }
        }

        private void search()
        {

            // Xây dựng điều kiện tìm kiếm.
            //string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'";
            string where = string.Format("LoaiNPL like '%{0}%'", LoaiNPL);
            if (txtMaNPL.Text.Trim().Length > 0)
            {
                where += " AND Ma Like '%" + txtMaNPL.Text.Trim() + "%'";
            }

            if (txtTenNPL.Text.Trim().Length > 0)
            {
                where += " AND Ten Like N'%" + txtTenNPL.Text.Trim() + "%'";
            }

            this.NPLCollection = (List<HangDuaVao>)HangDuaVao.SelectCollectionDynamic(where, "Ma");

            //TOSO: Load NPL Bo sung
            //if (this.NPLCollection.Count != 0)
            //{
            //    List<Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung> nplBSListKDT = null;
            //    foreach (NguyenPhuLieu npl in this.NPLCollection)
            //    {
            //        nplBSListKDT = (List<Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung>)Company.KDT.SHARE.Components.Common.SXXK.SXXK_NguyenPhuLieuBoSung.SelectCollectionDynamicNguyenPhuLieuBoSung(npl.MaHaiQuan.Trim(), npl.MaDoanhNghiep, npl.Ma);

            //        npl.NPLChinh = nplBSListKDT != null && nplBSListKDT.Count > 0 ? nplBSListKDT[0].NPLChinh : false;
            //    }
            //}

            dgList.DataSource = this.NPLCollection;

            lblTongNPL.Text = NPLCollection.Count.ToString();
        }

        private DataTable dtNPL;
        private void searchNguoiKhaiBao(string userName)
        {
            if (userName.Equals("-1"))
            {
                search();
            }
            else
            {
                //if (dtNPL == null || dtNPL.Rows.Count == 0)
                dtNPL = new NguyenPhuLieu().GetNPLFromUserName(userName).Tables[0];
                dgList.DataSource = dtNPL;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchNguoiKhaiBao(cbUserKB.SelectedItem.Value.ToString());
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // if (ShowMessage("Bạn có muốn xóa các nguyên phụ liệu này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            {
                Delete();

                search();
            }
        }

        private void dgList_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            NguyenPhuLieu NPL = (NguyenPhuLieu)row.DataRow;
            if (NPL.Ten == "")
            {
                // ShowMessage("Tên nguyên phụ liệu không được trống", false);
                MLMessages("Tên nguyên phụ liệu không được trống", "MSG_NPL06", "", false);
                search();
                return;
            }
            NPL.Update();
        }

        private void btnMapping_Click(object sender, EventArgs e)
        {
            NguyenPhuLieuMappingForm f = new NguyenPhuLieuMappingForm();
            f.ShowDialog(this);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NguyenPhuLieuDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog(this);

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdNPL":
                    btnMapping_Click(null, null);
                    break;
                case "cmdAdd":
                    uiButton3_Click(null, null);
                    break;
                case "cmdAddExcel":
                    uiButton1_Click(null, null);
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null, null);
                    break;
                case "cmdDelete":
                    btnDelete_Click(null, null);
                    break;
                case "cmdPrint":
                    uiButton4_Click(null, null);
                    break;
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXRow[] listChecked = dgList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length; i++)
                {
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    HangDuaVao nplSelect = (HangDuaVao)item.DataRow;
                    NPLCollectionSelected.Add(nplSelect);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
    }
}