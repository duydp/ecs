

alter PROCEDURE [dbo].[p_KDT_SXXK_BKNPLTaiXuat_SelectBy_BangKeHoSoThanhLy_ID]  
 @BangKeHoSoThanhLy_ID bigint  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
SELECT  
 [ID],  
 [BangKeHoSoThanhLy_ID],  
 BKTX.SoToKhai,  
 BKTX.MaLoaiHinh,  
 BKTX.NamDangKy,  
 BKTX.MaHaiQuan,  
 BKTX.NgayDangKy,  
 [MaNPL],  
 [TenNPL],  
 [SoToKhaiXuat],  
 [MaLoaiHinhXuat],  
 [NamDangKyXuat],  
 [MaHaiQuanXuat],  
 [NgayDangKyXuat],  
 [LuongTaiXuat],  
 BKTX.DVT_ID,  
 [TenDVT],  
 [STTHang],  
--LinhHT Update  
 (select [NGAY_THN_THX] from [dbo].[t_SXXK_ToKhaiMauDich]  
  where [t_SXXK_ToKhaiMauDich].[SoToKhai]=[SoToKhaiXuat] and  [t_SXXK_ToKhaiMauDich].[MaLoaiHinh]=[MaLoaiHinhXuat]  
  AND [t_SXXK_ToKhaiMauDich].[NamDangKy]=[NamDangKyXuat]  
  AND [t_SXXK_ToKhaiMauDich].[MaHaiQuan]=[MaHaiQuanXuat])  
  as NgayThucXuat,  
  b.DonGiaTT,  
  b.ThueSuatXNK  
    
FROM  
 [dbo].[t_KDT_SXXK_BKNPLTaiXuat] as BKTX  
 Inner join  
 (select  * from t_SXXK_HangMauDich) as b  
 on b.SoToKhai = BKTX.SoToKhai and b.MaLoaiHinh= BKTX.MaLoaiHinh   
 and b.MaHaiQuan=BKTX.MaHaiQuan and b.MaPhu= BKTX.MaNPL   
 and BKTX.BangKeHoSoThanhLy_ID= @BangKeHoSoThanhLy_ID  
 
 Go
              
ALTER PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]          
 @MaDoanhNghiep NVarchar(14),          
 @LanThanhLy int          
AS          
          
SET NOCOUNT ON          
SET TRANSACTION ISOLATION LEVEL READ COMMITTED          
      
Select        
MAX(SoToKhaiNhap) AS SoToKhaiNhap,         
MAX(A.MaLoaiHinhNhap) AS MaLoaiHinh,    
YEAR(MAX(NgayDangKyNhap)) AS NamDK,    
 MaNPL,          
 MAX(TenNPL) as TenNPL,           
 SUM(LuongTonDau) as LuongTonDau,          
 Sum(LuongNhap) as LuongNhap,          
 Sum(LuongNPLSuDung) as LuongNPLSuDung,          
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,          
 Sum(LuongNopThue) as LuongNopThue,          
 Sum(LuongXuatTheoHD) as LuongXuatTheoHD,          
 Sum(LuongTonCuoi) as LuongTonCuoi,          
 B.DVT_ID, --MAX(ID_DVT_NPL) as ID_DVT_NPL,          
 Convert(DECIMAL(25,6),DonGiaTT) AS DonGiaTT,    
 ThueSuat as ThueSuat         
 From           
(           
SELECT          
 SoToKhaiNhap,     
 MaLoaiHinhNhap,         
 NgayDangKyNhap,          
 MaNPL,          
 TenNPL,           
 LuongTonDau,          
 CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end as LuongNhap,           
 Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung,           
 Sum(LuongNPLTaiXuat) as LuongNPLTaiXuat,           
 0 as LuongNopThue,          
 0 as LuongXuatTheoHD,          
 --case           
 -- when (LuongTonDau - Sum(LuongNPLSuDung)) >= 0           
 -- then  LuongTonDau - Sum(LuongNPLSuDung)          
 -- else 0          
 -- end           
 -- as LuongTonCuoi,          
 Case When Min(LuongTonCuoi) < 0 then 0 else Min(LuongTonCuoi) end as LuongTonCuoi,          
 (select ID from t_HaiQuan_DonViTinh where ten=TenDVT_NPL)as ID_DVT_NPL,          
 DonGiaTT,          
 ThueSuat           
FROM          
 [dbo].[t_KDT_SXXK_BCXuatNhapTon]          
WHERE MaDoanhNghiep =@MaDoanhNghiep AND LanThanhLy =@LanThanhLy          
GROUP BY NgayDangKyNhap, SoToKhaiNhap, MaLoaiHinhNhap, MaNPL,TenNPL, TenDVT_NPL,DonGiaTT, LuongTonDau,LuongNhap,ThueSuat          
) as A inner join t_SXXK_NguyenPhuLieu B          
on A.MaNPL = B.Ma  AND B.MaDoanhNghiep = @MaDoanhNghiep        
          
Group by MaNPL,Convert(DECIMAL(25,6),DonGiaTT) ,B.DVT_ID, ThueSuat          
order by MaNPL    


GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.3',GETDATE(), N' cap nhat Store mapper thanh khoan')
END