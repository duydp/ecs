﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;
using Company.Interface.VNACCS.Vouchers;

namespace Company.Interface
{
    public partial class VNACC_TK_VanDonForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACC_TK_SoVanDon VD = new KDT_VNACC_TK_SoVanDon();
        public bool isAdd = true;
        DataTable dt = new DataTable();
        public bool IsCheck1563 = false;
        public bool IsCheck2061 = false;
        public bool IsCheck2722 = false;
        public String Caption;
        public bool IsChange;

        public VNACC_TK_VanDonForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int sott = 1;
                TKMD.VanDonCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow item in dt.Rows)
                {
                    KDT_VNACC_TK_SoVanDon vd = new KDT_VNACC_TK_SoVanDon();
                    if (item["ID"].ToString().Length != 0)
                    {
                        vd.ID = Convert.ToInt32(item["ID"].ToString());
                        vd.TKMD_ID = Convert.ToInt32(item["TKMD_ID"].ToString());
                    }
                    vd.SoTT = sott;
                    vd.SoVanDon = item["SoVanDon"].ToString();
                    vd.NgayVanDon = Convert.ToDateTime(item["NgayVanDon"].ToString());
                    vd.SoVanDonChu = item["SoVanDonChu"].ToString();
                    vd.NamVanDonChu = item["NamVanDonChu"].ToString();
                    vd.SoDinhDanh = "";
                    //duydp Comment
                    if (IsCheck1563)
                    {
                        vd.LoaiDinhDanh = "1563";
                        vd.SoDinhDanh = vd.NgayVanDon.ToString("dd/MM/yy").Replace("/", "") + "" + vd.SoVanDonChu + "";
                        string[] split = vd.SoDinhDanh.Split(' ');
                        string valueTempNew = "";
                        foreach (string word in split)
                        {
                            valueTempNew += word;
                        }
                        vd.SoDinhDanh = valueTempNew;
                    }
                    else if (IsCheck2061)
                    {
                        vd.LoaiDinhDanh = "2061";
                        vd.SoDinhDanh = vd.NamVanDonChu + vd.SoVanDonChu.ToString().Trim() + vd.SoVanDon + "";
                        string[] split = vd.SoDinhDanh.Split(' ');
                        string valueTempNew = "";
                        foreach (string word in split)
                        {
                            valueTempNew += word;
                        }
                        vd.SoDinhDanh = valueTempNew;
                    }
                    else if (IsCheck2722)
                    {
                        vd.LoaiDinhDanh = "2722";
                        vd.SoDinhDanh = vd.NamVanDonChu + vd.SoVanDonChu.ToString().Trim() + vd.SoVanDon + "";
                        string[] split = vd.SoDinhDanh.Split(' ');
                        string valueTempNew = "";
                        foreach (string word in split)
                        {
                            valueTempNew += word;
                        }
                        vd.SoDinhDanh = valueTempNew;
                    }
                    else
                    {
                        vd.SoDinhDanh = vd.SoVanDonChu;
                    }
                    //if (IsCheck1563)
                    //    vd.LoaiDinhDanh = "1563";
                    //if (IsCheck2061)
                    //    vd.LoaiDinhDanh = "2061";
                    //if (item["NgayVanDon"].ToString()!=string.Empty)
                    //{
                    //    vd.NgayVanDon = Convert.ToDateTime(item["NgayVanDon"].ToString());
                    //}
                    //else
                    //{
                    //    vd.NgayVanDon = new DateTime(1900,01,01);
                    //}
                    //if (IsCheck2722)
                    //{
                    //    string valueTemp = "";
                    //    string checkValueTemp = "";
                    //    checkValueTemp = vd.NgayVanDon.ToString("dd/MM/yyyy").Replace("/", "");
                    //    valueTemp = vd.NgayVanDon.ToString("dd/MM/yyyy").Replace("/", "") + "" + vd.SoVanDon + "";
                    //    vd.LoaiDinhDanh = "2722";
                    //    if (checkValueTemp != valueTemp)
                    //    {
                    //        vd.SoVanDon = valueTemp;   
                    //    }
                    //}
                    TKMD.VanDonCollection.Add(vd);
                    sott++;
                    TKMD.ChiTietKhaiTriGia = vd.NgayVanDon.ToString("dd/MM/yyyy").Replace("/", "") + "#&";
                }
                VNACC_VanDonForm_Load(null, null);
                ShowMessage("Lưu thành công !", false);
                this.SetChange(true);
                //this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                //
            }
        }

        private void VNACC_VanDonForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtSoVanDonChu.MaxLength = 255;
                txtSoVanDonTC.MaxLength = 255;
                grList.RootTable.Columns["NamVanDonChu"].Visible = false;
                grList.RootTable.Columns["SoVanDon"].Visible = false;
                //clcNamVD.Enabled = false;
                //txtSoVanDonTC.Enabled = false;
                chkMasterBill.Visible = true;
                if (TKMD.VanDonCollection.Count > 0)
                {
                    if (TKMD.VanDonCollection[0].LoaiDinhDanh != null)
                    {
                        if (TKMD.VanDonCollection[0].LoaiDinhDanh.ToString() == "1563")
                            chk1593.Checked = true;
                        if (TKMD.VanDonCollection[0].LoaiDinhDanh.ToString() == "2061")
                            chk2061.Checked = true;
                        if (TKMD.VanDonCollection[0].LoaiDinhDanh.ToString() == "2722")
                            chk2722.Checked = true;
                    }
                    else
                    {
                        lblMAWB.Text = "Số vận đơn";
                        lblNgayVD.Text = "Ngày vận đơn";
                        txtSoVanDonTC.Enabled = false;
                        chkMasterBill.Visible = false;
                    }
                }
                else
                {
                    lblMAWB.Text = "Số vận đơn";
                    lblNgayVD.Text = "Ngày vận đơn";
                    txtSoVanDonTC.Enabled = false;
                    chkMasterBill.Visible = false;
                }
                //dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.VanDonCollection);
                //grList.DataSource = dt;
                BindData();
                Caption = this.Text;
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnAdd.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }


        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            #region Code Old
            //try
            //{
            //    int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
            //    if (ID > 0)
            //    {
            //        if (ShowMessage("Bạn có muốn xóa thông tin vận đơn này không?", true) == "Yes")
            //        {
            //            KDT_VNACC_TK_SoVanDon.DeleteDynamic("ID=" + ID);
            //            grList.CurrentRow.Delete();
            //        }
            //    }
            //    else
            //        grList.CurrentRow.Delete();
            //    grList.Refetch();

            //    this.Cursor = Cursors.Default;
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    this.Cursor = Cursors.Default;
            //    //
            //}
            #endregion
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_TK_SoVanDon> ItemColl = new List<KDT_VNACC_TK_SoVanDon>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_TK_SoVanDon)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_TK_SoVanDon item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.VanDonCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                    {
                        item.SoTT = k;
                        k++;
                    }
                    BindData();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void chk2061_CheckedChanged(object sender, EventArgs e)
        {
            if (chk2061.CheckState == CheckState.Checked)
            {
                chk1593.Checked = false;
                chk1593.Enabled = false;
                chk2722.Checked = false;
                chk2722.Enabled = false;
                IsCheck2061 = true;
                chkMasterBill.Visible = true;
                grList.RootTable.Columns["NamVanDonChu"].Visible = true;
                grList.RootTable.Columns["SoVanDon"].Visible = true;
                clcNamVD.Enabled = true;
                txtSoVanDonTC.Enabled = true;

                txtSoVanDonChu.MaxLength = 11;
                txtSoVanDonTC.MaxLength = 12;
                lblNgayVD.Text = "Ngày vận đơn / Ngày HAWB";
            }
            else
            {
                IsCheck2061 = false;
                chk1593.Enabled = true;
                chk2722.Enabled = true;
                IsCheck2061 = false;
                chkMasterBill.Visible = false;
                grList.RootTable.Columns["NamVanDonChu"].Visible = false;
                grList.RootTable.Columns["SoVanDon"].Visible = false;
                clcNamVD.Enabled = false;
                txtSoVanDonTC.Enabled = false;
                txtSoVanDonChu.MaxLength = 255;
                txtSoVanDonTC.MaxLength = 255;
                lblMAWB.Text = "Số vận đơn";
                lblNgayVD.Text = "Ngày vận đơn";
            }
        }

        private void chk1593_CheckedChanged(object sender, EventArgs e)
        {
            if (chk1593.CheckState == CheckState.Checked)
            {
                chk2061.Checked = false;
                chk2061.Enabled = false;
                chk2722.Checked = false;
                chk2722.Enabled = false;
                IsCheck1563 = true;
                txtSoVanDonTC.Enabled = false;
                clcNamVD.Enabled = false;
                chkMasterBill.Visible = false;
                grList.RootTable.Columns["SoVanDon"].Visible = false;
                txtSoVanDonTC.Text = String.Empty;
                lblMAWB.Text = "Số vận đơn";
                lblNgayVD.Text = "Ngày vận đơn";
            }
            else
            {
                chk2061.Enabled = true;
                chk2722.Enabled = true;
                IsCheck1563 = false;
                txtSoVanDonTC.Enabled = false;
                clcNamVD.Enabled = false;
                lblMAWB.Text = "Số vận đơn";
                lblNgayVD.Text = "Ngày vận đơn";
            }
        }

        private void chk2722_CheckedChanged(object sender, EventArgs e)
        {
            if (chk2722.CheckState == CheckState.Checked)
            {
                chk2061.Checked = false;
                chk2061.Enabled = false;
                chk1593.Checked = false;
                chk1593.Enabled = false;
                IsCheck2722 = true;
                txtSoVanDonTC.Enabled = false;
                grList.RootTable.Columns["SoVanDon"].Visible = false;
            }
            else
            {
                txtSoVanDonTC.Enabled = true;
                chk1593.Enabled = true;
                chk2061.Enabled = true;
                IsCheck2722 = false;
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                CapSoDinhDanhHangHoaForm f = new CapSoDinhDanhHangHoaForm();
                f.ShowDialog(this);
                txtSoVanDonChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonChu.Text = f.capSoDinhDanh.SoDinhDanh;
                txtSoVanDonChu.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoVanDonChu.Text = f.capSoDinhDanh.SoDinhDanh;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            grList_DeletingRecord(null, null);
        }

        private void chkMasterBill_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMasterBill.CheckState == CheckState.Checked)
            {
                txtSoVanDonTC.Text = String.Empty;
                txtSoVanDonTC.Enabled = false;
                grList.RootTable.Columns["SoVanDon"].Visible = false;
                txtSoVanDonChu.MaxLength = 11;
                txtSoVanDonChu.TextChanged -= new EventHandler(txtSoVanDonChu_TextChanged);
                txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                txtSoVanDonChu.TextChanged += new EventHandler(txtSoVanDonChu_TextChanged);
                lblNgayVD.Text = "Ngày MAWB";
            }
            else
            {
                txtSoVanDonTC.Enabled = true;
                grList.RootTable.Columns["SoVanDon"].Visible = true;
                txtSoVanDonChu.MaxLength = 11;
                lblNgayVD.Text = "Ngày vận đơn / Ngày HAWB";
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoVanDonChu, errorProvider, "Số vận đơn", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNgayVD, errorProvider, "Ngày vận đơn", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcNamVD, errorProvider, "Năm vận đơn", isOnlyWarning);
                if (chk2061.CheckState == CheckState.Checked)
                {
                    if (chkMasterBill.CheckState == CheckState.Unchecked)
                    {
                        isValid &= ValidateControl.ValidateNull(txtSoVanDonTC, errorProvider, "Số vận đơn thứ cấp (HAWB)", isOnlyWarning);
                        isValid &= ValidateControl.ValidateLength(txtSoVanDonTC, 12, errorProvider, "Số vận đơn thứ cấp (HAWB) chỉ nhập được tối đa 12 ký tự", isOnlyWarning);
                        isValid &= ValidateControl.ValidateLength(txtSoVanDonChu, 11, errorProvider, "Số vận đơn chủ (MAWB) chỉ nhập được tối đa 11 ký tự", isOnlyWarning);
                        isValid &= ValidateControl.ValidateNull(txtSoVanDonChu, errorProvider, "Số vận đơn chủ (MAWB)");
                    }
                    else
                    {
                        isValid &= ValidateControl.ValidateLength(txtSoVanDonChu, 11, errorProvider, "Số vận đơn chủ (MAWB) chỉ nhập được tối đa 11 ký tự", isOnlyWarning);
                    }
                }
                isValid &= ValidateControl.ValidateNull(txtSoDinhDanh, errorProvider, "Số định danh hàng hóa", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void BindData()
        {
            try
            {
                grList.Refresh();
                grList.DataSource = TKMD.VanDonCollection;
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetData()
        {
            try
            {
                if (IsCheck1563)
                {
                    VD.LoaiDinhDanh = "1563";
                }
                else if (IsCheck2061)
                {
                    VD.LoaiDinhDanh = "2061";
                }
                VD.TKMD_ID = TKMD.ID;
                VD.SoVanDonChu = txtSoVanDonChu.Text;
                VD.NgayVanDon = clcNgayVD.Value;
                VD.NamVanDonChu = clcNamVD.Value.Year.ToString();
                VD.SoVanDon = txtSoVanDonTC.Text.ToString();
                VD.SoDinhDanh = txtSoDinhDanh.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void SetData()
        {
            try
            {
                if (VD.LoaiDinhDanh == "1563")
                {
                    IsCheck1563 = true;
                    chk1593.CheckState = CheckState.Checked;
                }
                else if (VD.LoaiDinhDanh == "2061")
                {
                    IsCheck2061 = true;
                    chk2061.CheckState = CheckState.Checked;
                }

                txtSoVanDonChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonChu.Text = VD.SoVanDonChu;
                txtSoVanDonChu.TextChanged += new EventHandler(txt_TextChanged);

                clcNgayVD.TextChanged -= new EventHandler(txt_TextChanged);
                clcNgayVD.Value = VD.NgayVanDon;
                clcNgayVD.TextChanged += new EventHandler(txt_TextChanged);

                clcNamVD.TextChanged -= new EventHandler(txt_TextChanged);
                clcNamVD.Value = VD.NgayVanDon;
                clcNamVD.TextChanged += new EventHandler(txt_TextChanged);

                txtSoVanDonTC.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonTC.Text = VD.SoVanDon;
                txtSoVanDonTC.TextChanged += new EventHandler(txt_TextChanged);

                txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDinhDanh.Text = VD.SoDinhDanh;
                txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoVanDonChu.Text = VD.SoVanDonChu;
                //clcNgayVD.Value = VD.NgayVanDon;
                //clcNamVD.Value = Convert.ToDateTime(VD.NamVanDonChu);
                //txtSoVanDonTC.Text = VD.SoVanDon;
                //txtSoDinhDanh.Text = VD.SoDinhDanh;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (chk2061.CheckState == CheckState.Checked)
                {
                    if (chkMasterBill.CheckState == CheckState.Unchecked)
                    {
                        foreach (char c in txtSoVanDonChu.Text.ToString())
                        {
                            if (!char.IsDigit(c) && c != '.')
                            {
                                errorProvider.SetError(txtSoVanDonChu, "Số vận đơn chủ (MAWB) chỉ được nhập là các ký tự số (0-9)");
                                return;
                            }
                        }
                    }
                }
                GetData();
                if (isAdd)
                {
                    if (TKMD.VanDonCollection.Count == 5)
                    {
                        ShowMessageTQDT("Thông báo từ Hệ thống", "Danh sách vận đơn chỉ được nhập tối đa là : 5 vận đơn", false);
                        return;
                    }
                    else
                    {
                        TKMD.VanDonCollection.Add(VD);
                    }
                }
                int i = 1;
                foreach (KDT_VNACC_TK_SoVanDon item in TKMD.VanDonCollection)
                {
                    item.SoTT = i;
                    i++;
                }
                VD = new KDT_VNACC_TK_SoVanDon();

                txtSoVanDonChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonChu.Text = String.Empty;
                txtSoVanDonChu.TextChanged += new EventHandler(txt_TextChanged);

                txtSoVanDonTC.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoVanDonTC.Text = String.Empty;
                txtSoVanDonTC.TextChanged += new EventHandler(txt_TextChanged);

                txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDinhDanh.Text = String.Empty;
                txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                //txtSoVanDonChu.Text = String.Empty;
                //txtSoVanDonTC.Text = String.Empty;
                //txtSoDinhDanh.Text = String.Empty;

                isAdd = true;
                BindData();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoVanDonChu_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsCheck1563)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.ToString().Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2061)
                {
                    if (chkMasterBill.CheckState == CheckState.Checked)
                    {
                        txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                        //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                        txtSoDinhDanh.Text = txtSoVanDonChu.Text.ToString();
                        txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);
                    }
                    else
                    {
                        txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                        //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                        txtSoDinhDanh.Text = txtSoVanDonTC.Text.ToString();
                        txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);
                    }

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    //string[] split = txtSoDinhDanh.Text.Split(' ');
                    //string valueTempNew = "";
                    //foreach (string word in split)
                    //{
                    //    valueTempNew += word;
                    //}
                    //txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = valueTempNew;
                    //txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2722)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void clcNgayVD_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsCheck1563)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.ToString().Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2061)
                {
                    clcNamVD.Value = clcNgayVD.Value;
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.Text = txtSoVanDonTC.Text.ToString();
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    //string[] split = txtSoDinhDanh.Text.Split(' ');
                    //string valueTempNew = "";
                    //foreach (string word in split)
                    //{
                    //    valueTempNew += word;
                    //}
                    //txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = valueTempNew;
                    //txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2722)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void clcNamVD_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsCheck1563)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.ToString().Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2061)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.Text = txtSoVanDonTC.Text.ToString();
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    //string[] split = txtSoDinhDanh.Text.Split(' ');
                    //string valueTempNew = "";
                    //foreach (string word in split)
                    //{
                    //    valueTempNew += word;
                    //}
                    //txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = valueTempNew;
                    //txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2722)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }

                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoVanDonTC_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (IsCheck1563)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNgayVD.Value.ToString("dd/MM/yy").Replace("/", "") + "" + txtSoVanDonChu.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.ToString().Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2061)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.Text = txtSoVanDonTC.Text.ToString();
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";

                    //string[] split = txtSoDinhDanh.Text.Split(' ');
                    //string valueTempNew = "";
                    //foreach (string word in split)
                    //{
                    //    valueTempNew += word;
                    //}
                    //txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    //txtSoDinhDanh.Text = valueTempNew;
                    //txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else if (IsCheck2722)
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = clcNamVD.Value.Year + txtSoVanDonChu.Text.ToString().Trim() + txtSoVanDonTC.Text.ToString() + "";
                    string[] split = txtSoDinhDanh.Text.Split(' ');
                    string valueTempNew = "";
                    foreach (string word in split)
                    {
                        valueTempNew += word;
                    }
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = valueTempNew;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = valueTempNew;
                }
                else
                {
                    txtSoDinhDanh.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                    txtSoDinhDanh.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoDinhDanh.Text = txtSoVanDonChu.Text;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    VD = new KDT_VNACC_TK_SoVanDon();
                    VD = (KDT_VNACC_TK_SoVanDon)e.Row.DataRow;
                    SetData();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtSoVanDonChu_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                VNACCS_BranchDetailRegistedForm f = new VNACCS_BranchDetailRegistedForm();
                f.ShowDialog(this);
                if (f.branchDetail != null)
                {
                    txtSoVanDonChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoVanDonChu.Text = f.branchDetail.SoVanDon.ToString();
                    txtSoVanDonChu.TextChanged += new EventHandler(txt_TextChanged);

                    //txtSoVanDonChu.Text = f.branchDetail.SoVanDon.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
