using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long HMDBoSung_ID { set; get; }
		public string SoDong { set; get; }
		public decimal TriGiaTinhThueTruocKhiKhaiBoSungThuKhac { set; get; }
		public decimal SoLuongTinhThueTruocKhiKhaiBoSungThuKhac { set; get; }
		public string MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac { set; get; }
		public string MaApDungThueSuatTruocKhiKhaiBoSungThuKhac { set; get; }
		public string ThueSuatTruocKhiKhaiBoSungThuKhac { set; get; }
		public string SoTienThueTruocKhiKhaiBoSungThuKhac { set; get; }
		public decimal TriGiaTinhThueSauKhiKhaiBoSungThuKhac { set; get; }
		public decimal SoLuongTinhThueSauKhiKhaiBoSungThuKhac { set; get; }
		public string MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac { set; get; }
		public string MaApDungThueSuatSauKhiKhaiBoSungThuKhac { set; get; }
		public string ThueSuatSauKhiKhaiBoSungThuKhac { set; get; }
		public string SoTienThueSauKhiKhaiBoSungThuKhac { set; get; }
		public string HienThiMienThueVaThuKhacTruocKhiKhaiBoSung { set; get; }
		public string HienThiMienThueVaThuKhacSauKhiKhaiBoSung { set; get; }
		public string HienThiSoTienTangGiamThueVaThuKhac { set; get; }
		public decimal SoTienTangGiamThuKhac { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection = new List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac>();
			while (reader.Read())
			{
				KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac entity = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMDBoSung_ID"))) entity.HMDBoSung_ID = reader.GetInt64(reader.GetOrdinal("HMDBoSung_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDong"))) entity.SoDong = reader.GetString(reader.GetOrdinal("SoDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueTruocKhiKhaiBoSungThuKhac"))) entity.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueTruocKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThueTruocKhiKhaiBoSungThuKhac"))) entity.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThueTruocKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac"))) entity.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaApDungThueSuatTruocKhiKhaiBoSungThuKhac"))) entity.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("MaApDungThueSuatTruocKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTruocKhiKhaiBoSungThuKhac"))) entity.ThueSuatTruocKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("ThueSuatTruocKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThueTruocKhiKhaiBoSungThuKhac"))) entity.SoTienThueTruocKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("SoTienThueTruocKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThueSauKhiKhaiBoSungThuKhac"))) entity.TriGiaTinhThueSauKhiKhaiBoSungThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThueSauKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongTinhThueSauKhiKhaiBoSungThuKhac"))) entity.SoLuongTinhThueSauKhiKhaiBoSungThuKhac = reader.GetDecimal(reader.GetOrdinal("SoLuongTinhThueSauKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac"))) entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaApDungThueSuatSauKhiKhaiBoSungThuKhac"))) entity.MaApDungThueSuatSauKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("MaApDungThueSuatSauKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatSauKhiKhaiBoSungThuKhac"))) entity.ThueSuatSauKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("ThueSuatSauKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienThueSauKhiKhaiBoSungThuKhac"))) entity.SoTienThueSauKhiKhaiBoSungThuKhac = reader.GetString(reader.GetOrdinal("SoTienThueSauKhiKhaiBoSungThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiMienThueVaThuKhacTruocKhiKhaiBoSung"))) entity.HienThiMienThueVaThuKhacTruocKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("HienThiMienThueVaThuKhacTruocKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiMienThueVaThuKhacSauKhiKhaiBoSung"))) entity.HienThiMienThueVaThuKhacSauKhiKhaiBoSung = reader.GetString(reader.GetOrdinal("HienThiMienThueVaThuKhacSauKhiKhaiBoSung"));
				if (!reader.IsDBNull(reader.GetOrdinal("HienThiSoTienTangGiamThueVaThuKhac"))) entity.HienThiSoTienTangGiamThueVaThuKhac = reader.GetString(reader.GetOrdinal("HienThiSoTienTangGiamThueVaThuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTienTangGiamThuKhac"))) entity.SoTienTangGiamThuKhac = reader.GetDecimal(reader.GetOrdinal("SoTienTangGiamThuKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection, long id)
        {
            foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac VALUES(@HMDBoSung_ID, @SoDong, @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac, @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac, @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac, @ThueSuatTruocKhiKhaiBoSungThuKhac, @SoTienThueTruocKhiKhaiBoSungThuKhac, @TriGiaTinhThueSauKhiKhaiBoSungThuKhac, @SoLuongTinhThueSauKhiKhaiBoSungThuKhac, @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, @MaApDungThueSuatSauKhiKhaiBoSungThuKhac, @ThueSuatSauKhiKhaiBoSungThuKhac, @SoTienThueSauKhiKhaiBoSungThuKhac, @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung, @HienThiMienThueVaThuKhacSauKhiKhaiBoSung, @HienThiSoTienTangGiamThueVaThuKhac, @SoTienTangGiamThuKhac)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac SET HMDBoSung_ID = @HMDBoSung_ID, SoDong = @SoDong, TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac, SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac, MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac, ThueSuatTruocKhiKhaiBoSungThuKhac = @ThueSuatTruocKhiKhaiBoSungThuKhac, SoTienThueTruocKhiKhaiBoSungThuKhac = @SoTienThueTruocKhiKhaiBoSungThuKhac, TriGiaTinhThueSauKhiKhaiBoSungThuKhac = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac, SoLuongTinhThueSauKhiKhaiBoSungThuKhac = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, MaApDungThueSuatSauKhiKhaiBoSungThuKhac = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac, ThueSuatSauKhiKhaiBoSungThuKhac = @ThueSuatSauKhiKhaiBoSungThuKhac, SoTienThueSauKhiKhaiBoSungThuKhac = @SoTienThueSauKhiKhaiBoSungThuKhac, HienThiMienThueVaThuKhacTruocKhiKhaiBoSung = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung, HienThiMienThueVaThuKhacSauKhiKhaiBoSung = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung, HienThiSoTienTangGiamThueVaThuKhac = @HienThiSoTienTangGiamThueVaThuKhac, SoTienTangGiamThuKhac = @SoTienTangGiamThuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMDBoSung_ID", SqlDbType.BigInt, "HMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, "HienThiSoTienTangGiamThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, "SoTienTangGiamThuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMDBoSung_ID", SqlDbType.BigInt, "HMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, "HienThiSoTienTangGiamThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, "SoTienTangGiamThuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac VALUES(@HMDBoSung_ID, @SoDong, @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac, @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac, @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac, @ThueSuatTruocKhiKhaiBoSungThuKhac, @SoTienThueTruocKhiKhaiBoSungThuKhac, @TriGiaTinhThueSauKhiKhaiBoSungThuKhac, @SoLuongTinhThueSauKhiKhaiBoSungThuKhac, @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, @MaApDungThueSuatSauKhiKhaiBoSungThuKhac, @ThueSuatSauKhiKhaiBoSungThuKhac, @SoTienThueSauKhiKhaiBoSungThuKhac, @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung, @HienThiMienThueVaThuKhacSauKhiKhaiBoSung, @HienThiSoTienTangGiamThueVaThuKhac, @SoTienTangGiamThuKhac)";
            string update = "UPDATE t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac SET HMDBoSung_ID = @HMDBoSung_ID, SoDong = @SoDong, TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac, SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac, MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac, ThueSuatTruocKhiKhaiBoSungThuKhac = @ThueSuatTruocKhiKhaiBoSungThuKhac, SoTienThueTruocKhiKhaiBoSungThuKhac = @SoTienThueTruocKhiKhaiBoSungThuKhac, TriGiaTinhThueSauKhiKhaiBoSungThuKhac = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac, SoLuongTinhThueSauKhiKhaiBoSungThuKhac = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, MaApDungThueSuatSauKhiKhaiBoSungThuKhac = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac, ThueSuatSauKhiKhaiBoSungThuKhac = @ThueSuatSauKhiKhaiBoSungThuKhac, SoTienThueSauKhiKhaiBoSungThuKhac = @SoTienThueSauKhiKhaiBoSungThuKhac, HienThiMienThueVaThuKhacTruocKhiKhaiBoSung = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung, HienThiMienThueVaThuKhacSauKhiKhaiBoSung = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung, HienThiSoTienTangGiamThueVaThuKhac = @HienThiSoTienTangGiamThueVaThuKhac, SoTienTangGiamThuKhac = @SoTienTangGiamThuKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMDBoSung_ID", SqlDbType.BigInt, "HMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, "HienThiSoTienTangGiamThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, "SoTienTangGiamThuKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMDBoSung_ID", SqlDbType.BigInt, "HMDBoSung_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDong", SqlDbType.VarChar, "SoDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueTruocKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "TriGiaTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, "SoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "MaApDungThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "ThueSuatSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, "SoTienThueSauKhiKhaiBoSungThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, "HienThiMienThueVaThuKhacSauKhiKhaiBoSung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, "HienThiSoTienTangGiamThueVaThuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, "SoTienTangGiamThuKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> SelectCollectionBy_HMDBoSung_ID(long hMDBoSung_ID)
		{
            IDataReader reader = SelectReaderBy_HMDBoSung_ID(hMDBoSung_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HMDBoSung_ID(long hMDBoSung_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMDBoSung_ID", SqlDbType.BigInt, hMDBoSung_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HMDBoSung_ID(long hMDBoSung_ID)
		{
			const string spName = "p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMDBoSung_ID", SqlDbType.BigInt, hMDBoSung_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac(long hMDBoSung_ID, string soDong, decimal triGiaTinhThueTruocKhiKhaiBoSungThuKhac, decimal soLuongTinhThueTruocKhiKhaiBoSungThuKhac, string maDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, string maApDungThueSuatTruocKhiKhaiBoSungThuKhac, string thueSuatTruocKhiKhaiBoSungThuKhac, string soTienThueTruocKhiKhaiBoSungThuKhac, decimal triGiaTinhThueSauKhiKhaiBoSungThuKhac, decimal soLuongTinhThueSauKhiKhaiBoSungThuKhac, string maDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, string maApDungThueSuatSauKhiKhaiBoSungThuKhac, string thueSuatSauKhiKhaiBoSungThuKhac, string soTienThueSauKhiKhaiBoSungThuKhac, string hienThiMienThueVaThuKhacTruocKhiKhaiBoSung, string hienThiMienThueVaThuKhacSauKhiKhaiBoSung, string hienThiSoTienTangGiamThueVaThuKhac, decimal soTienTangGiamThuKhac)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac entity = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();	
			entity.HMDBoSung_ID = hMDBoSung_ID;
			entity.SoDong = soDong;
			entity.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = triGiaTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = soLuongTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = maDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = maApDungThueSuatTruocKhiKhaiBoSungThuKhac;
			entity.ThueSuatTruocKhiKhaiBoSungThuKhac = thueSuatTruocKhiKhaiBoSungThuKhac;
			entity.SoTienThueTruocKhiKhaiBoSungThuKhac = soTienThueTruocKhiKhaiBoSungThuKhac;
			entity.TriGiaTinhThueSauKhiKhaiBoSungThuKhac = triGiaTinhThueSauKhiKhaiBoSungThuKhac;
			entity.SoLuongTinhThueSauKhiKhaiBoSungThuKhac = soLuongTinhThueSauKhiKhaiBoSungThuKhac;
			entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = maDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac;
			entity.MaApDungThueSuatSauKhiKhaiBoSungThuKhac = maApDungThueSuatSauKhiKhaiBoSungThuKhac;
			entity.ThueSuatSauKhiKhaiBoSungThuKhac = thueSuatSauKhiKhaiBoSungThuKhac;
			entity.SoTienThueSauKhiKhaiBoSungThuKhac = soTienThueSauKhiKhaiBoSungThuKhac;
			entity.HienThiMienThueVaThuKhacTruocKhiKhaiBoSung = hienThiMienThueVaThuKhacTruocKhiKhaiBoSung;
			entity.HienThiMienThueVaThuKhacSauKhiKhaiBoSung = hienThiMienThueVaThuKhacSauKhiKhaiBoSung;
			entity.HienThiSoTienTangGiamThueVaThuKhac = hienThiSoTienTangGiamThueVaThuKhac;
			entity.SoTienTangGiamThuKhac = soTienTangGiamThuKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@HMDBoSung_ID", SqlDbType.BigInt, HMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, TriGiaTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, SoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaApDungThueSuatTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, ThueSuatTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, SoTienThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, TriGiaTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, SoLuongTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaApDungThueSuatSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, ThueSuatSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, SoTienThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueVaThuKhacTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueVaThuKhacSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, HienThiSoTienTangGiamThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, SoTienTangGiamThuKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac(long id, long hMDBoSung_ID, string soDong, decimal triGiaTinhThueTruocKhiKhaiBoSungThuKhac, decimal soLuongTinhThueTruocKhiKhaiBoSungThuKhac, string maDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, string maApDungThueSuatTruocKhiKhaiBoSungThuKhac, string thueSuatTruocKhiKhaiBoSungThuKhac, string soTienThueTruocKhiKhaiBoSungThuKhac, decimal triGiaTinhThueSauKhiKhaiBoSungThuKhac, decimal soLuongTinhThueSauKhiKhaiBoSungThuKhac, string maDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, string maApDungThueSuatSauKhiKhaiBoSungThuKhac, string thueSuatSauKhiKhaiBoSungThuKhac, string soTienThueSauKhiKhaiBoSungThuKhac, string hienThiMienThueVaThuKhacTruocKhiKhaiBoSung, string hienThiMienThueVaThuKhacSauKhiKhaiBoSung, string hienThiSoTienTangGiamThueVaThuKhac, decimal soTienTangGiamThuKhac)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac entity = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();			
			entity.ID = id;
			entity.HMDBoSung_ID = hMDBoSung_ID;
			entity.SoDong = soDong;
			entity.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = triGiaTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = soLuongTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = maDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = maApDungThueSuatTruocKhiKhaiBoSungThuKhac;
			entity.ThueSuatTruocKhiKhaiBoSungThuKhac = thueSuatTruocKhiKhaiBoSungThuKhac;
			entity.SoTienThueTruocKhiKhaiBoSungThuKhac = soTienThueTruocKhiKhaiBoSungThuKhac;
			entity.TriGiaTinhThueSauKhiKhaiBoSungThuKhac = triGiaTinhThueSauKhiKhaiBoSungThuKhac;
			entity.SoLuongTinhThueSauKhiKhaiBoSungThuKhac = soLuongTinhThueSauKhiKhaiBoSungThuKhac;
			entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = maDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac;
			entity.MaApDungThueSuatSauKhiKhaiBoSungThuKhac = maApDungThueSuatSauKhiKhaiBoSungThuKhac;
			entity.ThueSuatSauKhiKhaiBoSungThuKhac = thueSuatSauKhiKhaiBoSungThuKhac;
			entity.SoTienThueSauKhiKhaiBoSungThuKhac = soTienThueSauKhiKhaiBoSungThuKhac;
			entity.HienThiMienThueVaThuKhacTruocKhiKhaiBoSung = hienThiMienThueVaThuKhacTruocKhiKhaiBoSung;
			entity.HienThiMienThueVaThuKhacSauKhiKhaiBoSung = hienThiMienThueVaThuKhacSauKhiKhaiBoSung;
			entity.HienThiSoTienTangGiamThueVaThuKhac = hienThiSoTienTangGiamThueVaThuKhac;
			entity.SoTienTangGiamThuKhac = soTienTangGiamThuKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HMDBoSung_ID", SqlDbType.BigInt, HMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, TriGiaTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, SoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaApDungThueSuatTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, ThueSuatTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, SoTienThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, TriGiaTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, SoLuongTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaApDungThueSuatSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, ThueSuatSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, SoTienThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueVaThuKhacTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueVaThuKhacSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, HienThiSoTienTangGiamThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, SoTienTangGiamThuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac(long id, long hMDBoSung_ID, string soDong, decimal triGiaTinhThueTruocKhiKhaiBoSungThuKhac, decimal soLuongTinhThueTruocKhiKhaiBoSungThuKhac, string maDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac, string maApDungThueSuatTruocKhiKhaiBoSungThuKhac, string thueSuatTruocKhiKhaiBoSungThuKhac, string soTienThueTruocKhiKhaiBoSungThuKhac, decimal triGiaTinhThueSauKhiKhaiBoSungThuKhac, decimal soLuongTinhThueSauKhiKhaiBoSungThuKhac, string maDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac, string maApDungThueSuatSauKhiKhaiBoSungThuKhac, string thueSuatSauKhiKhaiBoSungThuKhac, string soTienThueSauKhiKhaiBoSungThuKhac, string hienThiMienThueVaThuKhacTruocKhiKhaiBoSung, string hienThiMienThueVaThuKhacSauKhiKhaiBoSung, string hienThiSoTienTangGiamThueVaThuKhac, decimal soTienTangGiamThuKhac)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac entity = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();			
			entity.ID = id;
			entity.HMDBoSung_ID = hMDBoSung_ID;
			entity.SoDong = soDong;
			entity.TriGiaTinhThueTruocKhiKhaiBoSungThuKhac = triGiaTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.SoLuongTinhThueTruocKhiKhaiBoSungThuKhac = soLuongTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac = maDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac;
			entity.MaApDungThueSuatTruocKhiKhaiBoSungThuKhac = maApDungThueSuatTruocKhiKhaiBoSungThuKhac;
			entity.ThueSuatTruocKhiKhaiBoSungThuKhac = thueSuatTruocKhiKhaiBoSungThuKhac;
			entity.SoTienThueTruocKhiKhaiBoSungThuKhac = soTienThueTruocKhiKhaiBoSungThuKhac;
			entity.TriGiaTinhThueSauKhiKhaiBoSungThuKhac = triGiaTinhThueSauKhiKhaiBoSungThuKhac;
			entity.SoLuongTinhThueSauKhiKhaiBoSungThuKhac = soLuongTinhThueSauKhiKhaiBoSungThuKhac;
			entity.MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac = maDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac;
			entity.MaApDungThueSuatSauKhiKhaiBoSungThuKhac = maApDungThueSuatSauKhiKhaiBoSungThuKhac;
			entity.ThueSuatSauKhiKhaiBoSungThuKhac = thueSuatSauKhiKhaiBoSungThuKhac;
			entity.SoTienThueSauKhiKhaiBoSungThuKhac = soTienThueSauKhiKhaiBoSungThuKhac;
			entity.HienThiMienThueVaThuKhacTruocKhiKhaiBoSung = hienThiMienThueVaThuKhacTruocKhiKhaiBoSung;
			entity.HienThiMienThueVaThuKhacSauKhiKhaiBoSung = hienThiMienThueVaThuKhacSauKhiKhaiBoSung;
			entity.HienThiSoTienTangGiamThueVaThuKhac = hienThiSoTienTangGiamThueVaThuKhac;
			entity.SoTienTangGiamThuKhac = soTienTangGiamThuKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@HMDBoSung_ID", SqlDbType.BigInt, HMDBoSung_ID);
			db.AddInParameter(dbCommand, "@SoDong", SqlDbType.VarChar, SoDong);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, TriGiaTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.Decimal, SoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaApDungThueSuatTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, ThueSuatTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueTruocKhiKhaiBoSungThuKhac", SqlDbType.VarChar, SoTienThueTruocKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@TriGiaTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, TriGiaTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.Decimal, SoLuongTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@MaApDungThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, MaApDungThueSuatSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@ThueSuatSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, ThueSuatSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@SoTienThueSauKhiKhaiBoSungThuKhac", SqlDbType.VarChar, SoTienThueSauKhiKhaiBoSungThuKhac);
			db.AddInParameter(dbCommand, "@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueVaThuKhacTruocKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiMienThueVaThuKhacSauKhiKhaiBoSung", SqlDbType.VarChar, HienThiMienThueVaThuKhacSauKhiKhaiBoSung);
			db.AddInParameter(dbCommand, "@HienThiSoTienTangGiamThueVaThuKhac", SqlDbType.VarChar, HienThiSoTienTangGiamThueVaThuKhac);
			db.AddInParameter(dbCommand, "@SoTienTangGiamThuKhac", SqlDbType.Decimal, SoTienTangGiamThuKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac(long id)
		{
			KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac entity = new KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HMDBoSung_ID(long hMDBoSung_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HMDBoSung_ID", SqlDbType.BigInt, hMDBoSung_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}