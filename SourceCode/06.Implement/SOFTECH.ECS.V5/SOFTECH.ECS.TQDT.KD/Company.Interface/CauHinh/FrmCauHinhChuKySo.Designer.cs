﻿namespace Company.Interface
{
    partial class FrmCauHinhChuKySo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCauHinhChuKySo));
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnCheckConnect = new Janus.Windows.EditControls.UIButton();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtPassword = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chIsRemember = new Janus.Windows.EditControls.UICheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkIsUseSign = new Janus.Windows.EditControls.UICheckBox();
            this.btnExportInfo = new System.Windows.Forms.Button();
            this.btnRefersh = new System.Windows.Forms.Button();
            this.lblNhaCungCap = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbSigns = new Janus.Windows.EditControls.UIComboBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chIsSignOnLan = new Janus.Windows.EditControls.UICheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbDataSignName = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtUserNameSign = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnSignRemote = new Janus.Windows.EditControls.UIButton();
            this.chIsUseSignRemote = new Janus.Windows.EditControls.UICheckBox();
            this.txtPasswordSign = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Size = new System.Drawing.Size(375, 277);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnCheckConnect);
            this.uiGroupBox6.Controls.Add(this.txtPassword);
            this.uiGroupBox6.Controls.Add(this.chIsRemember);
            this.uiGroupBox6.Controls.Add(this.label3);
            this.uiGroupBox6.Controls.Add(this.chkIsUseSign);
            this.uiGroupBox6.Controls.Add(this.btnExportInfo);
            this.uiGroupBox6.Controls.Add(this.btnRefersh);
            this.uiGroupBox6.Controls.Add(this.lblNhaCungCap);
            this.uiGroupBox6.Controls.Add(this.label2);
            this.uiGroupBox6.Controls.Add(this.label4);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbSigns);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(348, 208);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.Text = "Thông tin chữ ký số";
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // btnCheckConnect
            // 
            this.btnCheckConnect.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheckConnect.ImageIndex = 1;
            this.btnCheckConnect.ImageList = this.imageList1;
            this.btnCheckConnect.Location = new System.Drawing.Point(244, 100);
            this.btnCheckConnect.Name = "btnCheckConnect";
            this.btnCheckConnect.Size = new System.Drawing.Size(75, 22);
            this.btnCheckConnect.TabIndex = 6;
            this.btnCheckConnect.Text = "Kiểm tra";
            this.btnCheckConnect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnCheckConnect.Click += new System.EventHandler(this.btnCheckConnect_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "connect.png");
            this.imageList1.Images.SetKeyName(1, "disconnect.png");
            // 
            // txtPassword
            // 
            this.txtPassword.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPassword.Location = new System.Drawing.Point(99, 100);
            this.txtPassword.MaxLength = 100;
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(139, 21);
            this.txtPassword.TabIndex = 5;
            this.txtPassword.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPassword.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPassword.VisualStyleManager = this.vsmMain;
            // 
            // chIsRemember
            // 
            this.chIsRemember.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsRemember.Location = new System.Drawing.Point(99, 127);
            this.chIsRemember.Name = "chIsRemember";
            this.chIsRemember.Size = new System.Drawing.Size(116, 18);
            this.chIsRemember.TabIndex = 7;
            this.chIsRemember.Text = "Ghi nhớ mật khẩu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(17, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Mật khẩu";
            // 
            // chkIsUseSign
            // 
            this.chkIsUseSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIsUseSign.Location = new System.Drawing.Point(99, 21);
            this.chkIsUseSign.Name = "chkIsUseSign";
            this.chkIsUseSign.Size = new System.Drawing.Size(128, 18);
            this.chkIsUseSign.TabIndex = 1;
            this.chkIsUseSign.Text = "Sử dụng chữ ký số";
            // 
            // btnExportInfo
            // 
            this.btnExportInfo.AutoSize = true;
            this.btnExportInfo.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnExportInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportInfo.Image = ((System.Drawing.Image)(resources.GetObject("btnExportInfo.Image")));
            this.btnExportInfo.Location = new System.Drawing.Point(318, 44);
            this.btnExportInfo.Name = "btnExportInfo";
            this.btnExportInfo.Size = new System.Drawing.Size(22, 22);
            this.btnExportInfo.TabIndex = 4;
            this.btnExportInfo.UseVisualStyleBackColor = true;
            this.btnExportInfo.Click += new System.EventHandler(this.btnExportInfo_Click);
            // 
            // btnRefersh
            // 
            this.btnRefersh.AutoSize = true;
            this.btnRefersh.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnRefersh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRefersh.Image = ((System.Drawing.Image)(resources.GetObject("btnRefersh.Image")));
            this.btnRefersh.Location = new System.Drawing.Point(294, 44);
            this.btnRefersh.Name = "btnRefersh";
            this.btnRefersh.Size = new System.Drawing.Size(22, 22);
            this.btnRefersh.TabIndex = 3;
            this.btnRefersh.UseVisualStyleBackColor = true;
            this.btnRefersh.Click += new System.EventHandler(this.btnRefersh_Click);
            // 
            // lblNhaCungCap
            // 
            this.lblNhaCungCap.AutoSize = true;
            this.lblNhaCungCap.BackColor = System.Drawing.Color.Transparent;
            this.lblNhaCungCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhaCungCap.ForeColor = System.Drawing.Color.Red;
            this.lblNhaCungCap.Location = new System.Drawing.Point(99, 75);
            this.lblNhaCungCap.Name = "lblNhaCungCap";
            this.lblNhaCungCap.Size = new System.Drawing.Size(86, 13);
            this.lblNhaCungCap.TabIndex = 3;
            this.lblNhaCungCap.Text = "Chưa xác định";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(17, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Nhà cung cấp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(17, 21);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Cho phép";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(17, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Chữ ký số";
            // 
            // cbSigns
            // 
            this.cbSigns.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbSigns.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSigns.Location = new System.Drawing.Point(99, 45);
            this.cbSigns.Name = "cbSigns";
            this.cbSigns.Size = new System.Drawing.Size(192, 21);
            this.cbSigns.TabIndex = 2;
            this.cbSigns.VisualStyleManager = this.vsmMain;
            this.cbSigns.SelectedValueChanged += new System.EventHandler(this.cbChuKySo_SelectedValueChanged);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(115, 247);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(196, 247);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiTab1
            // 
            this.uiTab1.Location = new System.Drawing.Point(12, 3);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(357, 236);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage3});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.uiGroupBox6);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(355, 214);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Ký trực tiếp";
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.uiGroupBox1);
            this.uiTabPage3.Controls.Add(this.uiGroupBox2);
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(355, 214);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Ký Online / Nội bộ";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chIsSignOnLan);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.cbDataSignName);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 116);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(346, 96);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.Text = "Ký thông tin trong nội bộ";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chIsSignOnLan
            // 
            this.chIsSignOnLan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsSignOnLan.Location = new System.Drawing.Point(99, 47);
            this.chIsSignOnLan.Name = "chIsSignOnLan";
            this.chIsSignOnLan.Size = new System.Drawing.Size(179, 18);
            this.chIsSignOnLan.TabIndex = 1;
            this.chIsSignOnLan.Text = "Sử dụng trong mạng lan";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 25);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Cơ sỡ dữ liệu ký";
            // 
            // cbDataSignName
            // 
            this.cbDataSignName.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDataSignName.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDataSignName.Location = new System.Drawing.Point(99, 20);
            this.cbDataSignName.Name = "cbDataSignName";
            this.cbDataSignName.Size = new System.Drawing.Size(223, 21);
            this.cbDataSignName.TabIndex = 0;
            this.cbDataSignName.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtUserNameSign);
            this.uiGroupBox2.Controls.Add(this.btnSignRemote);
            this.uiGroupBox2.Controls.Add(this.chIsUseSignRemote);
            this.uiGroupBox2.Controls.Add(this.txtPasswordSign);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(346, 107);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Sử dụng hệ thống ký số từ xa (Online)";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtUserNameSign
            // 
            this.txtUserNameSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUserNameSign.Location = new System.Drawing.Point(99, 24);
            this.txtUserNameSign.MaxLength = 100;
            this.txtUserNameSign.Name = "txtUserNameSign";
            this.txtUserNameSign.Size = new System.Drawing.Size(142, 21);
            this.txtUserNameSign.TabIndex = 0;
            this.txtUserNameSign.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtUserNameSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnSignRemote
            // 
            this.btnSignRemote.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSignRemote.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSignRemote.ImageIndex = 1;
            this.btnSignRemote.ImageList = this.imageList1;
            this.btnSignRemote.Location = new System.Drawing.Point(247, 50);
            this.btnSignRemote.Name = "btnSignRemote";
            this.btnSignRemote.Size = new System.Drawing.Size(75, 22);
            this.btnSignRemote.TabIndex = 2;
            this.btnSignRemote.Text = "Kiểm tra";
            this.btnSignRemote.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnSignRemote.Click += new System.EventHandler(this.btnSignRemote_Click);
            // 
            // chIsUseSignRemote
            // 
            this.chIsUseSignRemote.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chIsUseSignRemote.Location = new System.Drawing.Point(99, 78);
            this.chIsUseSignRemote.Name = "chIsUseSignRemote";
            this.chIsUseSignRemote.Size = new System.Drawing.Size(183, 18);
            this.chIsUseSignRemote.TabIndex = 3;
            this.chIsUseSignRemote.Text = "Sử dụng ký số từ xa";
            // 
            // txtPasswordSign
            // 
            this.txtPasswordSign.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPasswordSign.Location = new System.Drawing.Point(99, 51);
            this.txtPasswordSign.MaxLength = 100;
            this.txtPasswordSign.Name = "txtPasswordSign";
            this.txtPasswordSign.PasswordChar = '*';
            this.txtPasswordSign.Size = new System.Drawing.Size(142, 21);
            this.txtPasswordSign.TabIndex = 1;
            this.txtPasswordSign.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtPasswordSign.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(20, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Tên đăng nhập";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(23, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Mật khẩu";
            // 
            // FrmCauHinhChuKySo
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 277);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "FrmCauHinhChuKySo";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thiết lập thông tin chữ ký";
            this.Load += new System.EventHandler(this.FrmCauHinhChuKySo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UICheckBox chkIsUseSign;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIComboBox cbSigns;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRefersh;
        private System.Windows.Forms.Label lblNhaCungCap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ImageList imageList1;
        private Janus.Windows.EditControls.UIButton btnCheckConnect;
        private Janus.Windows.GridEX.EditControls.EditBox txtPassword;
        private Janus.Windows.EditControls.UICheckBox chIsRemember;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnExportInfo;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtUserNameSign;
        private Janus.Windows.EditControls.UIButton btnSignRemote;
        private Janus.Windows.EditControls.UICheckBox chIsUseSignRemote;
        private Janus.Windows.GridEX.EditControls.EditBox txtPasswordSign;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UICheckBox chIsSignOnLan;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIComboBox cbDataSignName;
    }
}