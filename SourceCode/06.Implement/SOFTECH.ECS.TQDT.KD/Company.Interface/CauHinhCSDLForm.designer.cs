﻿namespace Company.Interface
{
    partial class CauHinhCSDLForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhCSDLForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtServerName = new Janus.Windows.EditControls.UIComboBox();
            this.txtPass = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDatabase = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaChiHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvDiaChiHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMayChu = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDatabase = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvPass = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChiHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMayChu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabase)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvPass)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(413, 286);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(413, 286);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(125, 255);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 3;
            this.uiButton1.Text = "Lưu";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click_1);
            // 
            // uiButton2
            // 
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(219, 256);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 0;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtServerName);
            this.uiGroupBox4.Controls.Add(this.txtPass);
            this.uiGroupBox4.Controls.Add(this.txtSa);
            this.uiGroupBox4.Controls.Add(this.txtDatabase);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 95);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(389, 154);
            this.uiGroupBox4.TabIndex = 2;
            this.uiGroupBox4.Text = "Thiết lập thông số kết nối cơ sở dữ liệu";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtServerName
            // 
            this.txtServerName.BackColor = System.Drawing.SystemColors.Control;
            this.txtServerName.Location = new System.Drawing.Point(113, 32);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(245, 21);
            this.txtServerName.TabIndex = 2;
            // 
            // txtPass
            // 
            this.txtPass.BackColor = System.Drawing.SystemColors.Control;
            this.txtPass.Location = new System.Drawing.Point(113, 114);
            this.txtPass.Name = "txtPass";
            this.txtPass.PasswordChar = '*';
            this.txtPass.Size = new System.Drawing.Size(245, 21);
            this.txtPass.TabIndex = 7;
            this.txtPass.VisualStyleManager = this.vsmMain;
            // 
            // txtSa
            // 
            this.txtSa.BackColor = System.Drawing.SystemColors.Control;
            this.txtSa.Location = new System.Drawing.Point(113, 86);
            this.txtSa.Name = "txtSa";
            this.txtSa.Size = new System.Drawing.Size(245, 21);
            this.txtSa.TabIndex = 5;
            this.txtSa.VisualStyleManager = this.vsmMain;
            // 
            // txtDatabase
            // 
            this.txtDatabase.BackColor = System.Drawing.SystemColors.Control;
            this.txtDatabase.Location = new System.Drawing.Point(113, 59);
            this.txtDatabase.Name = "txtDatabase";
            this.txtDatabase.Size = new System.Drawing.Size(245, 21);
            this.txtDatabase.TabIndex = 3;
            this.txtDatabase.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(7, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Mật khẩu truy nhập";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(7, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã truy nhập";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(7, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên cơ sơ dữ liệu";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(7, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Máy chủ";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtDiaChiHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(389, 73);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Thiết lập thông số khai báo";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChiHQ
            // 
            this.txtDiaChiHQ.BackColor = System.Drawing.SystemColors.Control;
            this.txtDiaChiHQ.Location = new System.Drawing.Point(113, 29);
            this.txtDiaChiHQ.Name = "txtDiaChiHQ";
            this.txtDiaChiHQ.Size = new System.Drawing.Size(245, 21);
            this.txtDiaChiHQ.TabIndex = 1;
            this.txtDiaChiHQ.Text = "www.dngcustoms.gov.vn";
            this.txtDiaChiHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Địa chỉ hải quan";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvDiaChiHQ
            // 
            this.rfvDiaChiHQ.ControlToValidate = this.txtDiaChiHQ;
            this.rfvDiaChiHQ.ErrorMessage = "\"Địa chỉ hải quan\" không được để trống";
            this.rfvDiaChiHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChiHQ.Icon")));
            this.rfvDiaChiHQ.Tag = "rfvDiaChiHQ";
            // 
            // rfvMayChu
            // 
            this.rfvMayChu.ControlToValidate = this.txtServerName;
            this.rfvMayChu.ErrorMessage = "\"Tên máy chủ chứa cơ sơ dữ liệu\" không được trống";
            this.rfvMayChu.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMayChu.Icon")));
            this.rfvMayChu.Tag = "rfvMayChu";
            // 
            // rfvDatabase
            // 
            this.rfvDatabase.ControlToValidate = this.txtDatabase;
            this.rfvDatabase.ErrorMessage = "\"Cơ sơ dữ liệu \" không được trống";
            this.rfvDatabase.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDatabase.Icon")));
            this.rfvDatabase.Tag = "rfvDatabase";
            // 
            // rfvSa
            // 
            this.rfvSa.ControlToValidate = this.txtSa;
            this.rfvSa.ErrorMessage = "Chưa nhập tên truy cập";
            this.rfvSa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSa.Icon")));
            this.rfvSa.Tag = "rfvSa";
            // 
            // rfvPass
            // 
            this.rfvPass.ControlToValidate = this.txtPass;
            this.rfvPass.ErrorMessage = "Chưa nhập mật khẩu";
            this.rfvPass.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvPass.Icon")));
            this.rfvPass.Tag = "rfvPass";
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // CauHinhCSDLForm
            // 
            this.AcceptButton = this.uiButton1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(413, 286);
            this.Controls.Add(this.uiGroupBox2);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CauHinhCSDLForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình hệ thống";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChiHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMayChu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDatabase)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvPass)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiHQ;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtPass;
        private Janus.Windows.GridEX.EditControls.EditBox txtSa;
        private Janus.Windows.GridEX.EditControls.EditBox txtDatabase;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChiHQ;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMayChu;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDatabase;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvPass;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIComboBox txtServerName;

    }
}