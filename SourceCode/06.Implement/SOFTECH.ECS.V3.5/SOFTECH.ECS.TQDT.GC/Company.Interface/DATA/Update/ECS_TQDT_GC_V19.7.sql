
IF COL_LENGTH('T_KDT_ContainerDangKy', 'ThoiGianXuatDL') IS NULL
BEGIN
        ALTER TABLE t_KDT_ContainerDangKy
        ADD ThoiGianXuatDL datetime
END
GO
IF COL_LENGTH('T_KDT_ContainerDangKy', 'LuongTK') IS NULL
BEGIN
        ALTER TABLE t_KDT_ContainerDangKy
        ADD LuongTK int
END
Go
IF COL_LENGTH('T_KDT_ContainerDangKy', 'TrangThaiToKhai') IS NULL
BEGIN
        ALTER TABLE t_KDT_ContainerDangKy
        ADD TrangThaiToKhai int
END
GO
IF COL_LENGTH('t_KDT_ContainerBS', 'Code') IS NULL
BEGIN
        ALTER TABLE t_KDT_ContainerBS
        ADD Code nvarchar(Max)
END
GO
IF COL_LENGTH('t_KDT_ContainerBS', 'KVGS') IS NULL
BEGIN
        ALTER TABLE t_KDT_ContainerBS
        ADD KVGS int
END
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ContainerDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@TKMD_ID,
	@TrangThaiXuLy,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@DeXuatKhac,
	@LyDoSua,
	@ThoiGianXuatDL,
	@LuongTK,
	@TrangThaiToKhai
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int
AS

UPDATE
	[dbo].[t_KDT_ContainerDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TKMD_ID] = @TKMD_ID,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ThoiGianXuatDL] = @ThoiGianXuatDL,
	[LuongTK] = @LuongTK,
	[TrangThaiToKhai] = @TrangThaiToKhai
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TKMD_ID bigint,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoSua nvarchar(250),
	@ThoiGianXuatDL datetime,
	@LuongTK int,
	@TrangThaiToKhai int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ContainerDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ContainerDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TKMD_ID] = @TKMD_ID,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoSua] = @LyDoSua,
			[ThoiGianXuatDL] = @ThoiGianXuatDL,
			[LuongTK] = @LuongTK,
			[TrangThaiToKhai] = @TrangThaiToKhai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ContainerDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[TKMD_ID],
			[TrangThaiXuLy],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[DeXuatKhac],
			[LyDoSua],
			[ThoiGianXuatDL],
			[LuongTK],
			[TrangThaiToKhai]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@TKMD_ID,
			@TrangThaiXuLy,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@DeXuatKhac,
			@LyDoSua,
			@ThoiGianXuatDL,
			@LuongTK,
			@TrangThaiToKhai
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ContainerDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ContainerDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai]
FROM
	[dbo].[t_KDT_ContainerDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai]
FROM [dbo].[t_KDT_ContainerDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 07, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TKMD_ID],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoSua],
	[ThoiGianXuatDL],
	[LuongTK],
	[TrangThaiToKhai]
FROM
	[dbo].[t_KDT_ContainerDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ContainerBS_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ContainerBS_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Insert]
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ContainerBS]
(
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS]
)
VALUES 
(
	@Master_id,
	@SoVanDon,
	@SoContainer,
	@SoSeal,
	@GhiChu,
	@Code,
	@KVGS
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Update]
	@ID bigint,
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int
AS

UPDATE
	[dbo].[t_KDT_ContainerBS]
SET
	[Master_id] = @Master_id,
	[SoVanDon] = @SoVanDon,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal,
	[GhiChu] = @GhiChu,
	[Code] = @Code,
	[KVGS] = @KVGS
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@SoVanDon varchar(100),
	@SoContainer varchar(100),
	@SoSeal varchar(100),
	@GhiChu nvarchar(255),
	@Code nvarchar(max),
	@KVGS int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ContainerBS] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ContainerBS] 
		SET
			[Master_id] = @Master_id,
			[SoVanDon] = @SoVanDon,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal,
			[GhiChu] = @GhiChu,
			[Code] = @Code,
			[KVGS] = @KVGS
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ContainerBS]
		(
			[Master_id],
			[SoVanDon],
			[SoContainer],
			[SoSeal],
			[GhiChu],
			[Code],
			[KVGS]
		)
		VALUES 
		(
			@Master_id,
			@SoVanDon,
			@SoContainer,
			@SoSeal,
			@GhiChu,
			@Code,
			@KVGS
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Delete]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ContainerBS]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ContainerBS] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS]
FROM
	[dbo].[t_KDT_ContainerBS]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS]
FROM [dbo].[t_KDT_ContainerBS] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ContainerBS_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: 14 October, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ContainerBS_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoVanDon],
	[SoContainer],
	[SoSeal],
	[GhiChu],
	[Code],
	[KVGS]
FROM
	[dbo].[t_KDT_ContainerBS]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.7',GETDATE(), N' Cập nhật table Khai Bao Bo Sung Container')
END	

