﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAS5130 : BasicVNACC
    {

        public List<VAS5130_HANG> HangHoa { get; set; }

        //public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        //{
        //    StringBuilder str = StrBuild;
        //    str = BuildEdiMessages<VAS5050>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050");
        //    foreach (VAS5050_HANG item in HangHoa)
        //    {
        //        item.BuildEdiMessages<VAS5050_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050_HANG");

        //    }
        //    return str;
        //}
        public void LoadVAS5050(string strResult)
        {
            try
            {
                this.GetObject<VAS5130>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAS5050, false, VAS5130.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAS5130.TongSoByte);
                while (true)
                {
                    VAS5130_HANG ivaHang = new VAS5130_HANG();
                    ivaHang.GetObject<VAS5130_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAS5130_HANG", false, VAS5130_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAS5130_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAS5130_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAS5130_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAS5130_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
