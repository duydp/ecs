﻿namespace Company.Interface
{
    partial class TrangThaiPhanHoiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TrangThaiPhanHoiForm));
            this.picError = new System.Windows.Forms.PictureBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ẩnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.pnWaiting = new System.Windows.Forms.Panel();
            this.picStop = new System.Windows.Forms.PictureBox();
            this.picDownload = new System.Windows.Forms.PictureBox();
            this.lblNew = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.picError)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.pnWaiting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDownload)).BeginInit();
            this.SuspendLayout();
            // 
            // picError
            // 
            this.picError.BackColor = System.Drawing.Color.Transparent;
            this.picError.ContextMenuStrip = this.contextMenuStrip1;
            this.picError.Image = global::Company.Interface.Properties.Resources.Icon_Transparent_Error;
            this.picError.Location = new System.Drawing.Point(24, 12);
            this.picError.Name = "picError";
            this.picError.Size = new System.Drawing.Size(40, 52);
            this.picError.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picError.TabIndex = 0;
            this.picError.TabStop = false;
            this.toolTip1.SetToolTip(this.picError, "Tự động nhận phản hồi gặp lỗi");
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ẩnToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(83, 26);
            // 
            // ẩnToolStripMenuItem
            // 
            this.ẩnToolStripMenuItem.Name = "ẩnToolStripMenuItem";
            this.ẩnToolStripMenuItem.Size = new System.Drawing.Size(82, 22);
            this.ẩnToolStripMenuItem.Text = "Ẩn";
            this.ẩnToolStripMenuItem.Click += new System.EventHandler(this.ẩnToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ContextMenuStrip = this.contextMenuStrip1;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F);
            this.label1.Location = new System.Drawing.Point(0, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "HH:MM:SS";
            // 
            // pnWaiting
            // 
            this.pnWaiting.BackColor = System.Drawing.Color.Transparent;
            this.pnWaiting.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnWaiting.ContextMenuStrip = this.contextMenuStrip1;
            this.pnWaiting.Controls.Add(this.label1);
            this.pnWaiting.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWaiting.Location = new System.Drawing.Point(0, 0);
            this.pnWaiting.Name = "pnWaiting";
            this.pnWaiting.Size = new System.Drawing.Size(64, 64);
            this.pnWaiting.TabIndex = 2;
            this.toolTip1.SetToolTip(this.pnWaiting, "Thời gian chờ cho lần nhận phản hồi tiếp theo");
            // 
            // picStop
            // 
            this.picStop.BackColor = System.Drawing.Color.Transparent;
            this.picStop.ContextMenuStrip = this.contextMenuStrip1;
            this.picStop.Dock = System.Windows.Forms.DockStyle.Fill;
#if SXXK_V4
            this.picStop.Image = global::Company.Interface.Properties.Resources.X_download;
#elif GC_V4
            this.picStop.Image = global::Company.Interface.Properties.Resources.G_download;
#elif KD_V4
            this.picStop.Image = global::Company.Interface.Properties.Resources.K_download;
#endif
            this.picStop.Location = new System.Drawing.Point(0, 0);
            this.picStop.Name = "picStop";
            this.picStop.Size = new System.Drawing.Size(64, 64);
            this.picStop.TabIndex = 3;
            this.picStop.TabStop = false;
            this.toolTip1.SetToolTip(this.picStop, "Dừng nhận phản hồi tự động");
            // 
            // picDownload
            // 
            this.picDownload.BackColor = System.Drawing.Color.Transparent;
            this.picDownload.ContextMenuStrip = this.contextMenuStrip1;
            this.picDownload.ErrorImage = global::Company.Interface.Properties.Resources.download2;
            this.picDownload.Image = global::Company.Interface.Properties.Resources.download2;
            this.picDownload.Location = new System.Drawing.Point(8, 8);
            this.picDownload.Name = "picDownload";
            this.picDownload.Size = new System.Drawing.Size(64, 64);
            this.picDownload.TabIndex = 4;
            this.picDownload.TabStop = false;
            this.toolTip1.SetToolTip(this.picDownload, "Đang nhận phản hồi từ hệ thống VNACCS");
            // 
            // lblNew
            // 
            this.lblNew.AutoSize = true;
            this.lblNew.BackColor = System.Drawing.Color.Transparent;
            this.lblNew.ContextMenuStrip = this.contextMenuStrip1;
            this.lblNew.ForeColor = System.Drawing.Color.Red;
            this.lblNew.Location = new System.Drawing.Point(5, 33);
            this.lblNew.Name = "lblNew";
            this.lblNew.Size = new System.Drawing.Size(41, 13);
            this.lblNew.TabIndex = 5;
            this.lblNew.Text = "NEW !!";
            this.toolTip1.SetToolTip(this.lblNew, "Có thông báo mới từ hệ thống VNACCS");
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon1.BalloonTipText = "Tự động nhận phản hồi";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Tự động nhận phản hồi";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // TrangThaiPhanHoiForm
            // 
            this.Appearance.BackColor = System.Drawing.Color.White;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(64, 64);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.ControlBox = false;
            this.Controls.Add(this.lblNew);
            this.Controls.Add(this.picDownload);
            this.Controls.Add(this.picStop);
            this.Controls.Add(this.pnWaiting);
            this.Controls.Add(this.picError);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(64, 64);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(64, 64);
            this.Name = "TrangThaiPhanHoiForm";
            this.Opacity = 0.8;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.TrangThaiPhanHoiForm_Load);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TrangThaiPhanHoiForm_MouseUp);
            this.Click += new System.EventHandler(this.TrangThaiPhanHoiForm_Click);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TrangThaiPhanHoiForm_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TrangThaiPhanHoiForm_MouseMove);
            ((System.ComponentModel.ISupportInitialize)(this.picError)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.pnWaiting.ResumeLayout(false);
            this.pnWaiting.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picStop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDownload)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picError;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnWaiting;
        private System.Windows.Forms.PictureBox picStop;
        private System.Windows.Forms.PictureBox picDownload;
        private System.Windows.Forms.Label lblNew;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ẩnToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon1;

    }
}