﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components;

namespace Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3
{
    [XmlRoot("SOFTECH_SYNCDATA")]
    public class SyncDaTa
    {
        /// <summary>
        /// Danh sach cac to khai tren server - 
        /// Thong tin so bo to khai
        /// </summary>
        [XmlArray("Declarations")]
        [XmlArrayItem("Declaration")]
        public List<SyncDaTaDetail> Declaration { get; set; }
        /// <summary>
        /// Doanh nghiệp
        /// </summary>
        [XmlElement("issuer")]
        public string issuer;
        /// <summary>
        /// Từ ngày
        /// </summary>
        [XmlElement("fromDate")]
        public string FromDate;
        /// <summary>
        /// Đến ngày 
        /// </summary>
        [XmlElement("toDate")]
        public string ToDate;
        /// <summary>
        /// Phân hệ
        /// </summary>
        [XmlElement("type")]
        public string Type;
        /// <summary>
        /// Phân hệ
        /// </summary>
        


        /// <summary>
        /// Tờ khai
        /// </summary>
        [XmlElement("Content")]
        public Content Content;

        /// <summary>
        /// Trạng thái khi tìm tờ khai
        /// = 1 : tim tat ca
        /// = 0 : tim nhung to khai chua nhan ve
        /// </summary>
        [XmlElement("status")]
        public string Status;
    }
}
