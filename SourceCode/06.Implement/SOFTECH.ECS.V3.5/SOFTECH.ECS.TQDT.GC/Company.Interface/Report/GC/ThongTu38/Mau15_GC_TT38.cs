using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using Logger;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.GC
{
    public partial class Mau15_GC_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public DataTable tb = new DataTable();
        //public HopDong HD = new HopDong();
        private decimal _tongTriGia = 0;
        private decimal _tongTriGiaSD = 0;
        private decimal _tongTriGiaTon = 0;
        private string _maNPL;
        //private static int _stt = 0;
        public Mau15_GC_TT38()
        {
            InitializeComponent();
            
        }

        public void BindReport(string where)
        {
            DataTable dataSource = tb;
            
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontToKhai);
            int i = 0;
            string maNPL = "";
            try
            {
                foreach (DataRow dr in dataSource.Rows)
                {
                    //dr["Ma"] = dr["Ma"].ToString();// +"/" + dr["Ten"].ToString();
                    //dr["SoToKhaiVnacc"] = dr["SoToKhaiVnacc"] + "/" + dr["MaLoaiHinh"].ToString().Substring(2, 3);
                    //dr["TyGiaTT"] = dr["TyGiaTT"] + "/" + dr["MaDVT"].ToString();
                    if (maNPL == dr["Ma"].ToString().Trim())
                    {
                        dr["STT"] = i;
                    }
                    else
                    {
                        maNPL = dr["Ma"].ToString().Trim();
                        i++;
                        dr["STT"] = i;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
            #region Tính Lương tồn
            DataTable temp = dataSource.DefaultView.ToTable(true, "Ma","SoLuongDaNhap","SoLuongCungUng","SoLuongDaDung","LuongTon");
            //string ma = "";
            decimal tongNhap = 0;
            decimal tongSuDung = 0;
            decimal tongTon = 0;
            //try
            //{
            //    dataSource.AcceptChanges();
            //    dataSource.Columns["TriGiaSuDung"].DataType = typeof(decimal);
                
            //}
            //catch (Exception ex)
            //{

            //    //throw;
            //}
            
            foreach (DataRow dr in temp.Select())
            {
                string ma1 = dr["Ma"].ToString();
                tongNhap = Convert.ToDecimal(dr["SoLuongDaNhap"].ToString());
                tongSuDung = Convert.ToDecimal(dr["SoLuongDaDung"].ToString());
                tongTon = Convert.ToDecimal(dr["LuongTon"].ToString());
                foreach (DataRow dr1 in dataSource.Select("Ma='"+dr["Ma"].ToString()+"'","NgayDangKy"))
                {
                    //int lenght = dataSource.Select("Ma='" + dr["Ma"].ToString() + "'", "Ma").Length;
                    string ma2 = dr1["Ma"].ToString();
                    try
                    {
                        #region Tính lượng vs trị giá
                        decimal luongTK = Convert.ToDecimal(dr1["SoLuong"].ToString());
                        decimal luongSD = Convert.ToDecimal(dr1["LuongSD_TK"].ToString());
                        decimal luongTon = Convert.ToDecimal(dr1["LuongTon_TK"].ToString());
                        //
                        decimal triGiaTK = Convert.ToDecimal(dr1["TriGiaTT"].ToString());
                        try
                        {
                            decimal triGiaSD = Convert.ToDecimal(dr1["TriGiaSuDung"].ToString());
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        decimal triGiaTon = Convert.ToDecimal(dr1["TonTriGia"].ToString());
                        decimal donGia = Convert.ToDecimal(dr1["DonGiaTT"].ToString());
                        decimal tyGia = Convert.ToDecimal(dr1["TyGiaTT"].ToString());
                        if (tongSuDung > luongTK) 
                        {
                            dr1["LuongSD_TK"] = luongTK;
                            dr1["LuongTon_TK"] = 0;
                            try
                            {
                                 dr1["TriGiaSuDung"] = Convert.ToInt64(Math.Round(triGiaTK,0).ToString());
                            }
                            catch (Exception ex)
                            {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            dr1["TonTriGia"] = 0;
                            tongSuDung = tongSuDung - luongTK;


                        }
                        else if (tongSuDung > 0 && tongSuDung < luongTK)
                        {
                            dr1["LuongSD_TK"] = tongSuDung;
                            dr1["LuongTon_TK"] = luongTK - tongSuDung;

                            try
                            {
                                dr1["TriGiaSuDung"] = Math.Round(tongSuDung * donGia, 0).ToString();
                                dr1["TonTriGia"] = Math.Round(triGiaTK - (tongSuDung * donGia), 0).ToString();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }


                            tongSuDung = 0;
                        }
                        else 
                        {
                            dr1["LuongSD_TK"] = 0;
                            dr1["TriGiaSuDung"] = 0;
                        }
                        luongSD = Convert.ToDecimal(dr1["LuongSD_TK"].ToString());
                        luongTon = Convert.ToDecimal(dr1["LuongTon_TK"].ToString());
                        #endregion

                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                    
                }
            }
            #endregion


            DetailReport.DataSource = dataSource;
            try
            {
                this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("Ma"),
                new DevExpress.XtraReports.UI.GroupField("SoLuongDaNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoLuongDaDung"),
                new DevExpress.XtraReports.UI.GroupField("SoLuongCungUng"),
                new DevExpress.XtraReports.UI.GroupField("LuongTon"),
            });
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
            
            lbl_STT_G_H.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblMaNPL_G_H.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            lblTenNPL_G_H.DataBindings.Add("Text", DetailReport.DataSource, "Ten");

            lblTongLuongNhap_G_F.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongDaNhap", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //lblTongLuongCU_G_H.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongCungUng", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lbl_TongLuongSD_G_F.DataBindings.Add("Text", DetailReport.DataSource, "SoLuongDaDung", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            lblTongLuongTon_G_F.DataBindings.Add("Text", DetailReport.DataSource, "LuongTon", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            
            lblSoToKhai_D.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhaiVnacc");
            lblNgayDangKy_D.DataBindings.Add("Text", DetailReport.DataSource, "NgayDangKy");
            //lblMaLoaiHinh_D.DataBindings.Add("Text", DetailReport.DataSource, "MaLoaiHinh");

            lblLuongNhapTK_D.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            lblLuongSD_D.DataBindings.Add("Text", DetailReport.DataSource, "LuongSD_TK", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            lblLuongTonTK_D.DataBindings.Add("Text", DetailReport.DataSource, "LuongTon_TK", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            lbl_DVT_NPL_D.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            lblDVT_NPL_G_F.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            lblDonGia_D.DataBindings.Add("Text", DetailReport.DataSource, "DonGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblTriGia_D.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblTriGiaSuDung_TK_D.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblTonTriGia_D.DataBindings.Add("Text", DetailReport.DataSource, "TonTriGia", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblTyGia_D.DataBindings.Add("Text", DetailReport.DataSource, "TyGiaTT", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lbl_DVT_TG_D.DataBindings.Add("Text", DetailReport.DataSource, "MaDVT");

            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            
                        
            xrLabel5.Text = GlobalSettings.TieudeNgay;
        }

        private void NhapXuatTon_TT38_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
             
        }

        private void lblTriGia_D_AfterPrint(object sender, EventArgs e)
        {
            _tongTriGia += Convert.ToDecimal(lblTriGia_D.Text);
        }

        private void lblTriGiaSuDung_TK_D_AfterPrint(object sender, EventArgs e)
        {
            _tongTriGiaSD += Convert.ToDecimal(lblTriGiaSuDung_TK_D.Text);
        }

        private void lblTonTriGia_D_AfterPrint(object sender, EventArgs e)
        {
            _tongTriGiaTon += Convert.ToDecimal(lblTonTriGia_D.Text);
        }

        private void lblTongTriGia_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongTriGia_G_F.Text = _tongTriGia.ToString("N0");
            _tongTriGia = 0;
        }

        private void lblTongTriGiaSuDung_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongTriGiaSuDung_G_F.Text = _tongTriGiaSD.ToString("N0");
            _tongTriGiaSD = 0;
        }

        private void lblMaNPL_G_H_AfterPrint(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(_maNPL) || lblMaNPL_G_H.Text != _maNPL) 
            {
                lblMaNPL_G_H.Text = _maNPL;
            }
            
            
        }

        private void lbl_STT_G_H_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lbl_STT_G_H.Text += 1;
        }

        private void lblTongTonTriGia_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongTonTriGia_G_F.Text = _tongTriGiaTon.ToString("N0");
            _tongTriGiaTon = 0;
        }

        private void lblNgayDangKy_D_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            string d = lblNgayDangKy_D.Text;
            lblNgayDangKy_D.Text = Convert.ToDateTime(d).ToString("dd/MM/yyyy");
        }
       
       
       
    }
}
