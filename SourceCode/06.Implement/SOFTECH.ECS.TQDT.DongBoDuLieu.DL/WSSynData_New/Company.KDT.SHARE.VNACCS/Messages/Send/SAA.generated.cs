using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class SAA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SMC { get; set; }
public PropertiesAttribute SPC { get; set; }
public PropertiesAttribute ADS { get; set; }
public PropertiesAttribute SCC { get; set; }
public PropertiesAttribute SPN { get; set; }
public PropertiesAttribute SFN { get; set; }
public PropertiesAttribute SBE { get; set; }
public PropertiesAttribute APN { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute APT { get; set; }
public PropertiesAttribute APP { get; set; }
public PropertiesAttribute CON { get; set; }
public PropertiesAttribute EXN { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EXA { get; set; }
public PropertiesAttribute EXS { get; set; }
public PropertiesAttribute EXD { get; set; }
public PropertiesAttribute EXC { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EXF { get; set; }
public PropertiesAttribute EXE { get; set; }
public PropertiesAttribute EOC { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute RCN { get; set; }
public PropertiesAttribute BPC { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute BCC { get; set; }
public PropertiesAttribute BPN { get; set; }
public PropertiesAttribute PFX { get; set; }
public PropertiesAttribute BEM { get; set; }
public PropertiesAttribute TCC { get; set; }
public PropertiesAttribute MTT { get; set; }
public PropertiesAttribute IBC { get; set; }
public PropertiesAttribute IBN { get; set; }
public PropertiesAttribute PMN { get; set; }
public PropertiesAttribute QLN { get; set; }
public PropertiesAttribute VCD { get; set; }
public PropertiesAttribute DVC { get; set; }
public PropertiesAttribute ATT { get; set; }
public PropertiesAttribute CCT { get; set; }
public PropertiesAttribute PDC { get; set; }
public PropertiesAttribute QRT { get; set; }
public PropertiesAttribute QRH { get; set; }
public PropertiesAttribute CTL { get; set; }
public PropertiesAttribute CLT { get; set; }
public PropertiesAttribute CLH { get; set; }
public PropertiesAttribute NMC { get; set; }
public PropertiesAttribute OEQ { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute ALC { get; set; }
public PropertiesAttribute TES { get; set; }
public PropertiesAttribute TER { get; set; }
public PropertiesAttribute SHN { get; set; }
public PropertiesAttribute NAT { get; set; }
public PropertiesAttribute CTN { get; set; }
public PropertiesAttribute DTN { get; set; }
public PropertiesAttribute CRN { get; set; }
public PropertiesAttribute PGN { get; set; }
public PropertiesAttribute LDP { get; set; }
public PropertiesAttribute NAP { get; set; }
public PropertiesAttribute DCP { get; set; }
public PropertiesAttribute DOD { get; set; }
public PropertiesAttribute GDC { get; set; }
public PropertiesAttribute FQ { get; set; }
public PropertiesAttribute FQU { get; set; }
public PropertiesAttribute FW { get; set; }
public PropertiesAttribute FWU { get; set; }
public GroupAttribute D__ { get; set; }
public PropertiesAttribute DGD { get; set; }
public PropertiesAttribute DQ { get; set; }
public PropertiesAttribute DQU { get; set; }
public PropertiesAttribute DW { get; set; }
public PropertiesAttribute DWU { get; set; }

public SAA()
        {
SMC = new PropertiesAttribute(13, typeof(string));
SPC = new PropertiesAttribute(7, typeof(string));
ADS = new PropertiesAttribute(300, typeof(string));
SCC = new PropertiesAttribute(2, typeof(string));
SPN = new PropertiesAttribute(20, typeof(string));
SFN = new PropertiesAttribute(20, typeof(string));
SBE = new PropertiesAttribute(210, typeof(string));
APN = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
APT = new PropertiesAttribute(4, typeof(string));
APP = new PropertiesAttribute(6, typeof(string));
CON = new PropertiesAttribute(35, typeof(string));
EXN = new PropertiesAttribute(70, typeof(string));
EPC = new PropertiesAttribute(9, typeof(string));
EXA = new PropertiesAttribute(35, typeof(string));
EXS = new PropertiesAttribute(35, typeof(string));
EXD = new PropertiesAttribute(35, typeof(string));
EXC = new PropertiesAttribute(35, typeof(string));
ECC = new PropertiesAttribute(2, typeof(string));
EPN = new PropertiesAttribute(20, typeof(string));
EXF = new PropertiesAttribute(20, typeof(string));
EXE = new PropertiesAttribute(210, typeof(string));
EOC = new PropertiesAttribute(2, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
RCN = new PropertiesAttribute(35, typeof(string));
BPC = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
BCC = new PropertiesAttribute(2, typeof(string));
BPN = new PropertiesAttribute(20, typeof(string));
PFX = new PropertiesAttribute(20, typeof(string));
BEM = new PropertiesAttribute(210, typeof(string));
TCC = new PropertiesAttribute(2, typeof(string));
MTT = new PropertiesAttribute(2, typeof(string));
IBC = new PropertiesAttribute(6, typeof(string));
IBN = new PropertiesAttribute(35, typeof(string));
PMN = new PropertiesAttribute(35, typeof(string));
QLN = new PropertiesAttribute(150, typeof(string));
VCD = new PropertiesAttribute(100, typeof(string));
DVC = new PropertiesAttribute(8, typeof(DateTime));
ATT = new PropertiesAttribute(100, typeof(string));
CCT = new PropertiesAttribute(100, typeof(string));
PDC = new PropertiesAttribute(70, typeof(string));
QRT = new PropertiesAttribute(8, typeof(DateTime));
QRH = new PropertiesAttribute(4, typeof(string));
CTL = new PropertiesAttribute(150, typeof(string));
CLT = new PropertiesAttribute(8, typeof(DateTime));
CLH = new PropertiesAttribute(4, typeof(string));
NMC = new PropertiesAttribute(2, typeof(int));
OEQ = new PropertiesAttribute(150, typeof(string));
AD = new PropertiesAttribute(750, typeof(string));
ALC = new PropertiesAttribute(250, typeof(string));
TES = new PropertiesAttribute(50, typeof(string));
TER = new PropertiesAttribute(996, typeof(string));
SHN = new PropertiesAttribute(35, typeof(string));
NAT = new PropertiesAttribute(2, typeof(string));
CTN = new PropertiesAttribute(50, typeof(string));
DTN = new PropertiesAttribute(50, typeof(string));
CRN = new PropertiesAttribute(4, typeof(int));
PGN = new PropertiesAttribute(4, typeof(int));
LDP = new PropertiesAttribute(100, typeof(string));
NAP = new PropertiesAttribute(100, typeof(string));
DCP = new PropertiesAttribute(100, typeof(string));
DOD = new PropertiesAttribute(8, typeof(DateTime));
GDC = new PropertiesAttribute(100, typeof(string));
FQ = new PropertiesAttribute(8, typeof(int));
FQU = new PropertiesAttribute(3, typeof(string));
FW = new PropertiesAttribute(10, typeof(int));
FWU = new PropertiesAttribute(3, typeof(string));
DGD = new PropertiesAttribute(100, typeof(string));
DQ = new PropertiesAttribute(8, typeof(int));
DQU = new PropertiesAttribute(3, typeof(string));
DW = new PropertiesAttribute(10, typeof(int));
DWU = new PropertiesAttribute(3, typeof(string));
#region D__
List<PropertiesAttribute> listD__ = new List<PropertiesAttribute>();
listD__.Add(new PropertiesAttribute(100, 10, EnumGroupID.SAA_D__, typeof(string)));
listD__.Add(new PropertiesAttribute(8, 10, EnumGroupID.SAA_Q__, typeof(int)));
listD__.Add(new PropertiesAttribute(3, 10, EnumGroupID.SAA_U__, typeof(string)));
listD__.Add(new PropertiesAttribute(10, 10, EnumGroupID.SAA_W__, typeof(int)));
listD__.Add(new PropertiesAttribute(3, 10, EnumGroupID.SAA_X__, typeof(string)));
listD__.Add(new PropertiesAttribute(100, 10, EnumGroupID.SAA_P__, typeof(string)));
D__ = new GroupAttribute("D__", 10, listD__);
#endregion D__
TongSoByte = 8199;
}

}public partial class EnumGroupID
    {
public static readonly string SAA_D__ = "SAA_D__";
public static readonly string SAA_Q__ = "SAA_Q__";
public static readonly string SAA_U__ = "SAA_U__";
public static readonly string SAA_W__ = "SAA_W__";
public static readonly string SAA_X__ = "SAA_X__";
public static readonly string SAA_P__ = "SAA_P__";
}

}
