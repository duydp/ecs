﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.GC.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Utils;
namespace SOFTECH.ECS.V3.Components.GC
{//AiNPV
    public class ToKhaiNhapChuyenTiep
    {
        /// <summary>
        /// Config To Khai Nhap Chuyen Tiep 
        /// </summary>
        /// <param name="tknhap">int</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>string</returns>
        public static string ConfigTKNhapChuyenTiep(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tknhap, int issuer, int function, int status, string declarationOffice)
        {
            string doc = "";
            string path = Company.GC.BLL.EntityBase.GetPathProram();
            path = tknhap.MaLoaiHinh.Contains("N") ? path + "\\XML\\GC\\TKNhapChuyenTiep.xml" : path + "\\XML\\GC\\TKXuatChuyenTiep.xml";
            XDocument docXML = XDocument.Load(path);
            XmlDocument docRoot = new XmlDocument();
            docRoot.LoadXml(ConfigPhongBi(issuer, function, tknhap));

            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tknhap != null)
                {
                    XElement GoodsShipment = Root.Element(NodeTKNhapChuyenTiep.GoodsShipment);//Thông tin hàng hóa
                    XElement AdditionalDocument = Root.Element(NodeTKNhapChuyenTiep.AdditionalDocument);//Hợp đồng nhận
                    XElement CustomsGoodsItem = GoodsShipment.Element(NodeTKNhapChuyenTiep.CustomsGoodsItem);//Hàng khai báo
                    XElement tempCustomsGoodsItem = new XElement(CustomsGoodsItem);
                    bool checkCustomsGoodsItem = true;
                    IList<Company.GC.BLL.KDT.GC.HangChuyenTiep> lHCTiep = Company.GC.BLL.KDT.GC.HangChuyenTiep.SelectCollectionBy_Master_ID(tknhap.ID);
                    CuaKhau ckxuat = CuaKhau.Load(tknhap.MaHaiQuanKH.Trim());
                    NguyenTe nt = NguyenTe.Load(tknhap.NguyenTe_ID);

                    ToKhaiChuyenTiep.ConfigParent(Root, tknhap, issuer, function, status, declarationOffice.Trim());

                    //Thông tin hàng hóa - GoodsShipment
                    XElement element = Root.Element(NodeTKNhapChuyenTiep.GoodsShipment);
                    if (tknhap.MaLoaiHinh.Contains("N"))
                    {
                        XElement exportationCountry = element.Element(NodeTKNhapChuyenTiep.exportationCountry);
                        exportationCountry.SetValue("VN");
                    }
                    else
                    {
                        XElement importationCountry = element.Element(NodeTKNhapChuyenTiep.importationCountry);
                        importationCountry.SetValue("VN");
                    }
                    // Thông tin thương nhân giao hàng
                    XElement Consignor = element.Element(NodeTKNhapChuyenTiep.Consignor);
                    XElement ConsignorName = Consignor.Element(NodeTKNhapChuyenTiep.name);// Bên giao hàng
                    ConsignorName.SetValue(tknhap.TenKH);
                    XElement ConsignorID = Consignor.Element(NodeTKNhapChuyenTiep.identity);// Mã Bên giao hàng
                    ConsignorID.SetValue(tknhap.MaKhachHang);
                    // Thông tin bên nhận hàng
                    XElement Consignee = element.Element(NodeTKNhapChuyenTiep.Consignee);
                    XElement ConsigneeName = Consignee.Element(NodeTKNhapChuyenTiep.name);// Bên nhận hàng
                    ConsigneeName.SetValue(FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI")));
                    XElement ConsigneeID = Consignee.Element(NodeTKNhapChuyenTiep.identity);// Mã Bên nhận hàng
                    ConsigneeID.SetValue(tknhap.MaDoanhNghiep);
                    //Bên chỉ định giao hàng
                    XElement NotifyParty = element.Element(NodeTKNhapChuyenTiep.NotifyParty);
                    XElement NotifyPartyName = NotifyParty.Element(NodeTKNhapChuyenTiep.name);// Bên chỉ định giao hàng
                    NotifyPartyName.SetValue(tknhap.NguoiChiDinhKH);
                    XElement NotifyPartyID = NotifyParty.Element(NodeTKNhapChuyenTiep.identity);// Mã bên chỉ định giao hàng
                    NotifyPartyID.SetValue("");
                    //Địa điểm giao hàng
                    XElement DeliveryDestination = element.Element(NodeTKNhapChuyenTiep.DeliveryDestination);
                    XElement DeliveryDestinationName = DeliveryDestination.Element(NodeTKNhapChuyenTiep.line);// Địa điểm giao hàng
                    DeliveryDestinationName.SetValue(tknhap.DiaDiemXepHang);
                    XElement DeliveryDestinationID = DeliveryDestination.Element(NodeTKNhapChuyenTiep.time);// Thời điểm giao hàng
                    DeliveryDestinationID.SetValue(tknhap.ThoiGianGiaoHang.ToString(Company.KDT.SHARE.Components.NgayThang.yyyyMMdd));
                    //Cửa khẩu nhập
                    XElement EntryCustomsOffice = element.Element(NodeTKNhapChuyenTiep.EntryCustomsOffice);
                    XElement EntryCustomsOfficeCode = EntryCustomsOffice.Element(NodeTKNhapChuyenTiep.code);// Mã cửa khẩu nhập
                    XElement EntryCustomsOfficeName = EntryCustomsOffice.Element(NodeTKNhapChuyenTiep.name);// Mã cửa khẩu nhập
                    EntryCustomsOfficeCode.SetValue("");
                    EntryCustomsOfficeName.SetValue("");
                    //Cửa khẩu xuất
                    XElement ExitCustomsOffice = element.Element(NodeTKNhapChuyenTiep.ExitCustomsOffice);
                    XElement ExitCustomsOfficeName = ExitCustomsOffice.Element(NodeTKNhapChuyenTiep.name);
                    if (ckxuat != null && ckxuat.Ten != null)
                        ExitCustomsOfficeName.SetValue(ckxuat.Ten);
                    XElement ExitCustomsOfficeCode = ExitCustomsOffice.Element(NodeTKNhapChuyenTiep.code);
                    if (ckxuat != null && ckxuat.ID != null)
                        ExitCustomsOfficeCode.SetValue(ckxuat.ID);
                    if (tknhap.MaLoaiHinh.Contains("N"))
                    {
                        //Exporter - Người xuất khẩu
                        XElement Exporter = element.Element(NodeTKNhapChuyenTiep.Exporter);
                        XElement ExporterIdentity = Exporter.Element(NodeTKNhapChuyenTiep.identity);
                        XElement ExporterName = Exporter.Element(NodeTKNhapChuyenTiep.name);
                        ExporterIdentity.SetValue(tknhap.MaKhachHang);
                        ExporterName.SetValue(tknhap.TenKH);
                    }
                    else
                    {
                        //Importer - Người nhập khẩu
                        XElement Importer = element.Element(NodeTKNhapChuyenTiep.Importer);
                        XElement ImporterIdentity = Importer.Element(NodeTKNhapChuyenTiep.identity);
                        XElement ImporterName = Importer.Element(NodeTKNhapChuyenTiep.name);
                        ImporterIdentity.SetValue(tknhap.MaKhachHang);
                        ImporterName.SetValue(tknhap.TenKH);
                    }

                    //TradeTerm - Điều kiện giao hàng
                    XElement TradeTerm = element.Element(NodeTKNhapChuyenTiep.TradeTerm);
                    XElement TradeTermCondition = TradeTerm.Element(NodeTKNhapChuyenTiep.condition);
                    TradeTermCondition.SetValue(tknhap.DKGH_ID);

                    if (checkCustomsGoodsItem == true)
                    {
                        if (lHCTiep != null)
                        {
                            if (lHCTiep.Count == 1)
                            {
                                configCustomsGoodsItem(CustomsGoodsItem, lHCTiep[0]);
                            }
                            else if (lHCTiep.Count > 1)
                            {
                                configCustomsGoodsItem(CustomsGoodsItem, lHCTiep[0]);
                                for (int i = 1; i < lHCTiep.Count; i++)
                                {
                                    XElement newCustomsGoodsItem = new XElement(tempCustomsGoodsItem);
                                    configCustomsGoodsItem(newCustomsGoodsItem, lHCTiep[i]);
                                    GoodsShipment.Add(newCustomsGoodsItem);
                                }
                            }
                        }
                        checkCustomsGoodsItem = false;
                    }
                }
                else
                {
                    return doc; //return "";
                }
            }
            docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();

            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(doc);

            //luu vao string
            XmlNode root = docRoot.ImportNode(docNPL.SelectSingleNode("Declaration"), true);
            XmlNode Content = docRoot.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);
            return docRoot.InnerXml.ToString();
        }

        public static string ConfigTKNhapChuyenTiepV3(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tknhap, int issuer, int function, int status, string declarationOffice)
        {
            string doc = "";
            string path = "XML\\GC\\TKNhapChuyenTiep.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tknhap != null)
                {
                    XElement GoodsShipment = Root.Element(NodeTKNhapChuyenTiep.GoodsShipment);//Thông tin hàng hóa
                    XElement AdditionalDocument = Root.Element(NodeTKNhapChuyenTiep.AdditionalDocument);//Hợp đồng nhận
                    XElement CustomsGoodsItem = GoodsShipment.Element(NodeTKNhapChuyenTiep.CustomsGoodsItem);//Hàng khai báo
                    XElement tempCustomsGoodsItem = new XElement(CustomsGoodsItem);
                    bool checkCustomsGoodsItem = true;
                    IList<Company.GC.BLL.KDT.GC.HangChuyenTiep> lHCTiep = Company.GC.BLL.KDT.GC.HangChuyenTiep.SelectCollectionBy_Master_ID(tknhap.ID);
                    CuaKhau ckxuat = CuaKhau.Load(tknhap.MaHaiQuanKH.Trim());
                    NguyenTe nt = NguyenTe.Load(tknhap.NguyenTe_ID);

                    ToKhaiChuyenTiep.ConfigParent(Root, tknhap, issuer, function, status, declarationOffice.Trim());

                    foreach (XElement element in Root.Elements())
                    {
                        //Thông tin hàng hóa

                        if (element.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.GoodsShipment.ToUpper()))
                        {
                            foreach (XElement GoodsShipmentChild in element.Elements())
                            {

                                XElement exportationCountry = (XElement)element.Elements(NodeTKNhapChuyenTiep.exportationCountry);
                                exportationCountry.SetValue("nhat linh");
                                if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.exportationCountry.ToUpper()))
                                {
                                    GoodsShipmentChild.SetValue("");//>??? Not Found
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Consignor.ToUpper()))
                                {
                                    foreach (XElement ConsignorChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ConsignorChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.name.ToUpper()))
                                        {
                                            ConsignorChild.SetValue(tknhap.TenKH);
                                        }
                                        else if (ConsignorChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.identity.ToUpper()))
                                        {
                                            ConsignorChild.SetValue(tknhap.MaKhachHang);
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Consignee.ToUpper()))
                                {
                                    foreach (XElement ConsigneeChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ConsigneeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.name.ToUpper()))
                                        {
                                            ConsigneeChild.SetValue("");//>??? Not Found
                                        }
                                        else if (ConsigneeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.identity.ToUpper()))
                                        {
                                            ConsigneeChild.SetValue(tknhap.MaDoanhNghiep);
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.NotifyParty.ToUpper()))
                                {
                                    foreach (XElement NotifyPartyChild in GoodsShipmentChild.Elements())
                                    {
                                        if (NotifyPartyChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.name.ToUpper()))
                                        {
                                            NotifyPartyChild.SetValue(tknhap.NguoiChiDinhKH);
                                        }
                                        else if (NotifyPartyChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.identity.ToUpper()))
                                        {
                                            NotifyPartyChild.SetValue("");//>??? Not Found
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.DeliveryDestination.ToUpper()))
                                {
                                    foreach (XElement DeliveryDestinationChild in GoodsShipmentChild.Elements())
                                    {
                                        if (DeliveryDestinationChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.line.ToUpper()))
                                        {
                                            DeliveryDestinationChild.SetValue(tknhap.DiaDiemXepHang);
                                        }
                                        else if (DeliveryDestinationChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.time.ToUpper()))
                                        {
                                            DeliveryDestinationChild.SetValue("");//>??? Not Found
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.EntryCustomsOffice.ToUpper()))
                                {
                                    foreach (XElement EntryCustomsOfficeChild in GoodsShipmentChild.Elements())
                                    {
                                        if (EntryCustomsOfficeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.code.ToUpper()))
                                        {
                                            EntryCustomsOfficeChild.SetValue(tknhap.MaHaiQuanTiepNhan);
                                            // MaHaiQuanTiepNhan
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.ExitCustomsOffice.ToUpper()))
                                {
                                    //MaHaiQuanKhachHang
                                    foreach (XElement ExitCustomsOfficeChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ExitCustomsOfficeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.name.ToUpper()))
                                        {
                                            if (ckxuat != null && ckxuat.Ten != null)
                                            {
                                                ExitCustomsOfficeChild.SetValue(ckxuat.Ten);
                                            }
                                        }
                                        else if (ExitCustomsOfficeChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.code.ToUpper()))
                                        {
                                            if (ckxuat != null && ckxuat.ID != null)
                                            {
                                                ExitCustomsOfficeChild.SetValue(ckxuat.ID);
                                            }
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.Exporter.ToUpper()))
                                {
                                    foreach (XElement ExporterChild in GoodsShipmentChild.Elements())
                                    {
                                        if (ExporterChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.name.ToUpper()))
                                        {
                                            ExporterChild.SetValue(tknhap.TenKH);
                                        }
                                        else if (ExporterChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.identity.ToUpper()))
                                        {
                                            ExporterChild.SetValue(tknhap.MaKhachHang);
                                        }
                                    }
                                }
                                else if (GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.TradeTerm.ToUpper()))
                                {
                                    foreach (XElement TradeTermChild in GoodsShipmentChild.Elements())
                                    {
                                        if (TradeTermChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.condition.ToUpper()))
                                        {
                                            TradeTermChild.SetValue("");//??? Not Found
                                        }
                                    }
                                }
                                else if ((GoodsShipmentChild.Name.ToString().ToUpper().Equals(NodeTKNhapChuyenTiep.CustomsGoodsItem.ToUpper())) && checkCustomsGoodsItem == true)
                                {
                                    if (lHCTiep != null)
                                    {
                                        if (lHCTiep.Count == 1)
                                        {
                                            configCustomsGoodsItem(CustomsGoodsItem, lHCTiep[0]);
                                        }
                                        else if (lHCTiep.Count > 1)
                                        {
                                            configCustomsGoodsItem(CustomsGoodsItem, lHCTiep[0]);
                                            for (int i = 1; i < lHCTiep.Count; i++)
                                            {
                                                XElement newCustomsGoodsItem = new XElement(tempCustomsGoodsItem);
                                                configCustomsGoodsItem(newCustomsGoodsItem, lHCTiep[i]);
                                                GoodsShipment.Add(newCustomsGoodsItem);
                                            }
                                        }
                                    }
                                    checkCustomsGoodsItem = false;
                                }
                            }
                        }
                    }
                }
                else
                {
                    return doc; //return "";
                }
            }
            docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }

        public static XElement configCustomsGoodsItem(XElement CustomsGoodsItem, Company.GC.BLL.KDT.GC.HangChuyenTiep hct)
        {
            if (hct != null)
            {
                ToKhaiChuyenTiep tkct = ToKhaiChuyenTiep.Load(hct.Master_ID);
                NguyenTe nt = NguyenTe.Load(tkct.NguyenTe_ID);
                //Trị giá khai báo
                XElement customsValue = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.customsValue);
                customsValue.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.TriGia, 9));
                // Số thứ tự hàng
                XElement sequence = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.sequence);
                sequence.SetValue(hct.SoThuTuHang);
                // Trị giá tính thuế
                XElement statisticalValue = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.statisticalValue);
                statisticalValue.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.TriGiaTT, 9));
                // Đơn giá nguyên tệ
                XElement unitPrice = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.unitPrice);
                unitPrice.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.DonGia, 9));
                // Hãng sản xuất
                XElement Manufacturer = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.Manufacturer);
                XElement nameManufacturer = Manufacturer.Element(NodeTKNhapChuyenTiep.name);//Tên hãng SX
                XElement identityManufacturer = Manufacturer.Element(NodeTKNhapChuyenTiep.identity);//Mã hãng SX
                nameManufacturer.SetValue(".");
                identityManufacturer.SetValue(".");
                //Xuất xứ
                XElement Origin = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.Origin);
                XElement originCountry = Origin.Element(NodeTKNhapChuyenTiep.originCountry);
                originCountry.SetValue(hct.ID_NuocXX.Trim());
                // Chi tiết hàng, thuế
                XElement Commodity = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.Commodity);
                XElement descriptionCommodity = Commodity.Element(NodeTKNhapChuyenTiep.description);// tên hàng
                XElement identificationCommodity = Commodity.Element(NodeTKNhapChuyenTiep.identification);// mã hàng
                XElement tariffClassificationCommodity = Commodity.Element(NodeTKNhapChuyenTiep.tariffClassification);// Mã HS
                XElement tariffClassificationExtensionCommodity = Commodity.Element(NodeTKNhapChuyenTiep.tariffClassificationExtension);// Mã HS mở rộng
                XElement brandCommodity = Commodity.Element(NodeTKNhapChuyenTiep.brand);// Nhãn hiệu
                XElement gradeCommodity = Commodity.Element(NodeTKNhapChuyenTiep.grade);// Quy cách, phẩm chất
                XElement ingredientsCommodity = Commodity.Element(NodeTKNhapChuyenTiep.ingredients);// Thành phần
                XElement modelNumberCommodity = Commodity.Element(NodeTKNhapChuyenTiep.modelNumber);// Model của hàng hóa
                XElement typeCommodity = Commodity.Element(NodeTKNhapChuyenTiep.type);
                descriptionCommodity.SetValue(hct.TenHang);
                identificationCommodity.SetValue(hct.MaHang);
                tariffClassificationCommodity.SetValue(hct.MaHS);
                //tariffClassificationExtensionCommodity.SetValue("");
                //brandCommodity.SetValue("");
                //gradeCommodity.SetValue("");
                //ingredientsCommodity.SetValue("");
                //modelNumberCommodity.SetValue("");
                if (tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.NGC18 || tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.PHPLN
                    || tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.XGC18 || tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.PHPLX)
                    typeCommodity.SetValue(1);
                else if (tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.NGC19 || tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.PHSPN
                    || tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.XGC19 || tkct.MaLoaiHinh.ToUpper().Trim() == SOFTECH.ECS.V3.Components.SHARE.Enum.PHSPX)
                    typeCommodity.SetValue(2);
                else
                    typeCommodity.SetValue(3);
                XElement DutyTaxFeeCommodity = Commodity.Element(NodeTKNhapChuyenTiep.DutyTaxFee);//  Thuế
                XElement adValoremTaxBase = DutyTaxFeeCommodity.Element(NodeTKNhapChuyenTiep.adValoremTaxBase);//  Số thuế tự tính
                XElement tax = DutyTaxFeeCommodity.Element(NodeTKNhapChuyenTiep.tax);// Thuế suất
                XElement taxtype = DutyTaxFeeCommodity.Element(NodeTKNhapChuyenTiep.type);// Loại thuế
                adValoremTaxBase.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.ThueXNK, 9));
                tax.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.ThueSuatXNK, 9));
                //taxtype.SetValue("");
                XElement InvoiceLineCommodity = Commodity.Element(NodeTKNhapChuyenTiep.InvoiceLine);// Chưa sử dụng
                XElement itemCharge = InvoiceLineCommodity.Element(NodeTKNhapChuyenTiep.itemCharge);
                XElement line = InvoiceLineCommodity.Element(NodeTKNhapChuyenTiep.line);
                itemCharge.SetValue("");
                line.SetValue("");// Chưa sử dụng
                //
                XElement GoodsMeasure = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.GoodsMeasure);
                XElement tariff = GoodsMeasure.Element(NodeTKNhapChuyenTiep.tariff);// SoLuong
                XElement measureUnit = GoodsMeasure.Element(NodeTKNhapChuyenTiep.measureUnit);//Madonvitinh
                XElement conversionRate = GoodsMeasure.Element(NodeTKNhapChuyenTiep.conversionRate);// Tỷ lệ quy đổi so với đơn vị tính đăng ký
                XElement quantity = GoodsMeasure.Element(NodeTKNhapChuyenTiep.quantity);
                quantity.SetValue(Company.KDT.SHARE.QuanLyChungTu.BaseClass.Round(hct.SoLuong, 9));
                measureUnit.SetValue(hct.ID_DVT);
                //conversionRate.SetValue("");
                //
                XElement CustomsValuation = CustomsGoodsItem.Element(NodeTKNhapChuyenTiep.CustomsValuation);// Trị giá Hải quan
                XElement exitToEntryCharge = CustomsValuation.Element(NodeTKNhapChuyenTiep.exitToEntryCharge);// Tổng chi phí khác (vận tải, bảo hiểm và các chi phí khác từ cảng xuất đến cảng nhập)
                XElement freightCharge = CustomsValuation.Element(NodeTKNhapChuyenTiep.freightCharge);// Phí vận tải
                XElement Method = CustomsValuation.Element(tkct.MaLoaiHinh.Contains("N") ? NodeTKNhapChuyenTiep.method : NodeTKNhapChuyenTiep.Method);// Phương pháp xác định trị giá
                XElement otherChargeDeduction = CustomsValuation.Element(NodeTKNhapChuyenTiep.otherChargeDeduction);// Tổng các khoản phải cộng - tổng các khoản được trừ
                //TODO: sau này Hùng bổ sung giá trị phí 
                exitToEntryCharge.SetValue(0);//Tổng chi phí khác (vận tải, bảo hiểm và các chi phí khác từ cảng )
                freightCharge.SetValue(0);//Phí vận tải
                if (Method != null)
                    Method.SetValue("");//Phương pháp xác định trị giá
                otherChargeDeduction.SetValue(0);//Tổng các khoản phải cộng - tổng các khoản được trừ
            }
            return CustomsGoodsItem;
        }

        public static string ConfigPhongBi(int type, int function, Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct)
        {
            XmlDocument doc = new XmlDocument();
            string path = Company.GC.BLL.EntityBase.GetPathProram();
            doc.Load(path + "\\XML\\GC\\PhongBiV3.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(tkct.MaHaiQuanTiepNhan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = tkct.MaHaiQuanTiepNhan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            /*
            if (tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET || tkct.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                nodeReference.InnerText = tkct.GUIDSTR.ToUpper();
            else
            {
                tkct.GUIDSTR = (System.Guid.NewGuid().ToString().ToUpper());
                nodeReference.InnerText = tkct.GUIDSTR;
            }
            */
            if (string.IsNullOrEmpty(tkct.GUIDSTR)) tkct.GUIDSTR = Guid.NewGuid().ToString();
            nodeReference.InnerText = tkct.GUIDSTR;
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeCreateMessageIssue = doc.GetElementsByTagName("createMessageIssue")[0];
            nodeCreateMessageIssue.InnerText = DateTime.Now.ToString(Company.KDT.SHARE.Components.NgayThang.yyyyMMdd);

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI");//FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = tkct.MaDoanhNghiep.Trim();
            tkct.Update();
            return doc.InnerXml;
        }

        public static string ConfigHuyKhaiBaoTKCT(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNode DeclarationNode = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
            //DeclarationNode.RemoveAll();
            //XmlElement issuer = doc.CreateElement(NodeTKNhapChuyenTiep.issuer);// Loại chứng từ xin hủy
            //DeclarationNode.AppendChild(issuer);
            //issuer.InnerText = doc.GetElementsByTagName("type")[0].InnerText;

            //XmlElement function = doc.CreateElement(NodeTKNhapChuyenTiep.function);// Chức năng hủy = 1
            //DeclarationNode.AppendChild(function);
            //function.InnerText = Message_Functions.Huy.ToString();// "1";

            //XmlElement reference = doc.CreateElement(NodeTKNhapChuyenTiep.reference);//Số tham chiếu đến thông tin xin hủy (message Id hủy)
            //DeclarationNode.AppendChild(reference);
            //reference.InnerText = doc.GetElementsByTagName("reference")[0].InnerText;

            //XmlElement issue = doc.CreateElement(NodeTKNhapChuyenTiep.issue);//Ngày khai báo xin hủy
            //DeclarationNode.AppendChild(issue);
            //issue.InnerText = tkct.NgayTiepNhan.ToString("yyyy-MM-dd"); ;//DateTime.Now.ToString("yyyy-MM-dd"); 

            //XmlElement issueLocation = doc.CreateElement(NodeTKNhapChuyenTiep.issueLocation);//Nơi khai báo xin hủy
            //DeclarationNode.AppendChild(issueLocation);
            //issueLocation.InnerText = "";

            //XmlElement customsReference = doc.CreateElement(NodeTKNhapChuyenTiep.customsReference);//Số tiếp nhận chứng từ xin hủy (do Hải quan đã cấp)
            //DeclarationNode.AppendChild(customsReference);
            //customsReference.InnerText = tkct.SoTiepNhan.ToString();

            //XmlElement acceptance = doc.CreateElement(NodeTKNhapChuyenTiep.acceptance);//Ngày chấp nhận đăng ký chứng từ (ngày cấp số tiếp nhận
            //DeclarationNode.AppendChild(acceptance);
            //acceptance.InnerText = tkct.NgayDangKy.ToString("yyyy-MM-dd");

            //XmlElement declarationOffice = doc.CreateElement(NodeTKNhapChuyenTiep.declarationOffice);//Đơn vị Hải quan tiếp nhận chứng từ
            //DeclarationNode.AppendChild(declarationOffice);
            //declarationOffice.InnerText = tkct.MaHaiQuanTiepNhan;

            //doc.GetElementsByTagName("reference")[1].InnerText = doc.GetElementsByTagName("messageId")[0].InnerText;
            //doc.GetElementsByTagName("reference")[0].InnerText = doc.GetElementsByTagName("messageId")[0].InnerText;
            doc.GetElementsByTagName("function")[0].InnerText = Message_Functions.Huy.ToString();
            doc.GetElementsByTagName("function")[1].InnerText = Message_Functions.Huy.ToString();
            doc.GetElementsByTagName("messageId")[0].InnerText = (System.Guid.NewGuid().ToString().ToUpper());
            doc.GetElementsByTagName("customsReference")[0].InnerText = tkct.SoTiepNhan.ToString();
            doc.GetElementsByTagName("acceptance")[0].InnerText = tkct.NgayTiepNhan.ToString("yyyy-MM-dd");
            doc.GetElementsByTagName("natureOfTransaction")[0].InnerText = doc.GetElementsByTagName("natureOfTransaction")[0].InnerText.Trim();

            return doc.InnerXml;
        }
        public static string ConfigHuyTKCT(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, string xml, int issuer, int function)
        {
            //XmlDocument doc = new XmlDocument();
            //doc.LoadXml(xml);

            //XmlNode DeclarationNode = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
            //doc.GetElementsByTagName("function")[0].InnerText = Message_Functions.Huy.ToString();
            //doc.GetElementsByTagName("function")[1].InnerText = Message_Functions.Huy.ToString();
            //doc.GetElementsByTagName("messageId")[0].InnerText = (System.Guid.NewGuid().ToString().ToUpper());
            //doc.GetElementsByTagName("customsReference")[0].InnerText = tkct.SoToKhai.ToString();
            //doc.GetElementsByTagName("acceptance")[0].InnerText = tkct.NgayDangKy.ToString("yyyy-MM-dd");
            //doc.GetElementsByTagName("natureOfTransaction")[0].InnerText = doc.GetElementsByTagName("natureOfTransaction")[0].InnerText.Trim();

            //XmlDocument docRoot = new XmlDocument();
            //docRoot.LoadXml(ConfigPhongBi(issuer, function, tkct));

            //XmlDocument doc = new XmlDocument();
            //string path = Company.GC.BLL.EntityBase.GetPathProram();
            //doc.Load(path + "\\XML\\GC\\TKCT.xml");

            //XmlNode DeclarationNode = doc.SelectSingleNode("Declaration");
            //doc.GetElementsByTagName("reference")[0].InnerText = tkct.GUIDSTR;
            //doc.GetElementsByTagName("function")[0].InnerText = Message_Functions.Huy.ToString();
            //doc.GetElementsByTagName("customsReference")[0].InnerText = tkct.SoToKhai.ToString();
            //doc.GetElementsByTagName("issue")[0].InnerText = tkct.NgayDangKy.ToString("yyyy-MM-dd");
            //doc.GetElementsByTagName("acceptance")[0].InnerText = tkct.NgayDangKy.ToString("yyyy-MM-dd");
            //doc.GetElementsByTagName("natureOfTransaction")[0].InnerText = doc.GetElementsByTagName("natureOfTransaction")[0].InnerText.Trim();
            //doc.GetElementsByTagName("goodsItem")[0].InnerText = tkct.HCTCollection.Count.ToString();
            //doc.GetElementsByTagName("declarationOffice")[0].InnerText = tkct.MaHaiQuanTiepNhan.ToString();


            ////luu vao string
            //XmlNode root = docRoot.ImportNode(DeclarationNode, true);
            //XmlNode Content = docRoot.GetElementsByTagName("Content")[0];
            //Content.AppendChild(root);

            string messageXML = SOFTECH.ECS.V3.Components.GC.ToKhaiNhapChuyenTiep.ConfigTKNhapChuyenTiep(tkct,
                tkct.MaLoaiHinh.Contains("N") ? Message_Types.To_khai_nhap_khau_Gia_cong_Chuyen_Tiep : Message_Types.To_khai_xuat_khau_Gia_cong_Chuyen_Tiep,
                function, 1, tkct.MaHaiQuanKH);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(messageXML);

            //luu vao string
            XmlNode root = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
            XmlNode node = null;

            //Giay phep
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep.ConvertCollectionGiayPhepToXML(doc, tkct.CTKGiayPhepCollection, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            //ContractDocuments - Hợp đồng
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong.ConvertCollectionHopDongToXML(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            // CommercialInvoices - Hóa đơn
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon.ConvertCollectionHoaDonThuongMaiToXML(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            // CustomsOfficeChangedRequests - Đề nghị chuyển cửa khẩu
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCKToXML(doc, null, tkct.ID);
            if (node != null)
                root.AppendChild(node);

            // AttachDocuments - Chứng từ kèm dạng ảnh
            node = Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh.ConvertCollectionCTDinhKemToXMLV3(doc, null, tkct.ID, null);
            if (node != null)
                root.AppendChild(node);

            XmlNode DeclarationNode = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
            doc.GetElementsByTagName("function")[0].InnerText = Message_Functions.Huy.ToString();
            doc.GetElementsByTagName("function")[1].InnerText = Message_Functions.Huy.ToString();
            doc.GetElementsByTagName("messageId")[0].InnerText = (System.Guid.NewGuid().ToString().ToUpper());
            doc.GetElementsByTagName("customsReference")[0].InnerText = tkct.SoToKhai.ToString();
            doc.GetElementsByTagName("acceptance")[0].InnerText = tkct.NgayDangKy.ToString("yyyy-MM-dd");
            doc.GetElementsByTagName("natureOfTransaction")[0].InnerText = doc.GetElementsByTagName("natureOfTransaction")[0].InnerText.Trim();

            messageXML = doc.InnerXml;

            return messageXML;// docRoot.InnerXml;
            //return doc.InnerXml;
        }

        public static string ConfigHuyTKCT(Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep tkct, string xml)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            XmlNode DeclarationNode = doc.SelectSingleNode("Envelope/Body/Content/Declaration");
            doc.GetElementsByTagName("function")[0].InnerText = Message_Functions.Huy.ToString();
            doc.GetElementsByTagName("function")[1].InnerText = Message_Functions.Huy.ToString();
            doc.GetElementsByTagName("messageId")[0].InnerText = (System.Guid.NewGuid().ToString().ToUpper());
            doc.GetElementsByTagName("customsReference")[0].InnerText = tkct.SoToKhai.ToString();
            doc.GetElementsByTagName("acceptance")[0].InnerText = tkct.NgayDangKy.ToString("yyyy-MM-dd");
            doc.GetElementsByTagName("natureOfTransaction")[0].InnerText = doc.GetElementsByTagName("natureOfTransaction")[0].InnerText.Trim();

            return doc.InnerXml;
        }
    }
}
