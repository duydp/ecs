﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using System.Windows.Forms;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT;
#elif GC_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif
namespace Company.Interface
{
    public partial class ListCOTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListCOTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["LoaiCO"].Text = this.LoaiCO_GetName(e.Row.Cells["LoaiCO"].Value.ToString());
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}
        }




        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<CO> listCO = new List<CO>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        CO co = (CO)i.GetRow().DataRow;
                        listCO.Add(co);
                    }
                }
            }

            foreach (CO co in listCO)
            {
                //Update by KHANHHN - 03/03/2012
                if (co.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || co.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    co.Delete();
                    TKMD.COCollection.Remove(co);
                    TKMD.ListCO.Remove(co);
                }
                else
                {
                    this.ShowMessage(string.Format("Hợp đồng thương mại bổ sung số : {0} đã được gửi đến hải quan. Không thể xóa", co.SoCO), false);
                }
            }


            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }


        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            CoForm f = new CoForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.ShowDialog(this);            
            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            CoForm f = new CoForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = this.isKhaiBoSung;
            f.Co = (CO)e.Row.DataRow;
            if (f.Co.ID > 0 && f.Co.ListHMDofCo.Count == 0)
                f.Co.LoadListHMDofCo();
            f.ShowDialog(this);
            dgList.DataSource = TKMD.COCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }




    }
}
