/*
Run this script on:

        192.168.72.151.ECS_TQDT_SXXK_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_SXXK

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 12/14/2011 2:08:42 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Update]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Update]
-- Database: HaiQuan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong  numeric(38, 15),
	@TrongLuong numeric(38, 15),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam VARCHAR(50)
AS
UPDATE
	[dbo].[t_KDT_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[SoThuTuHang] = @SoThuTuHang,
	[MaHS] = @MaHS,
	[MaPhu] = @MaPhu,
	[TenHang] = @TenHang,
	[NuocXX_ID] = @NuocXX_ID,
	[DVT_ID] = @DVT_ID,
	[SoLuong] = @SoLuong,
	[TrongLuong] = @TrongLuong,
	[DonGiaKB] = @DonGiaKB,
	[DonGiaTT] = @DonGiaTT,
	[TriGiaKB] = @TriGiaKB,
	[TriGiaTT] = @TriGiaTT,
	[TriGiaKB_VND] = @TriGiaKB_VND,
	[ThueSuatXNK] = @ThueSuatXNK,
	[ThueSuatTTDB] = @ThueSuatTTDB,
	[ThueSuatGTGT] = @ThueSuatGTGT,
	[ThueXNK] = @ThueXNK,
	[ThueTTDB] = @ThueTTDB,
	[ThueGTGT] = @ThueGTGT,
	[PhuThu] = @PhuThu,
	[TyLeThuKhac] = @TyLeThuKhac,
	[TriGiaThuKhac] = @TriGiaThuKhac,
	[MienThue] = @MienThue,
	[Ma_HTS] = @Ma_HTS,
	[DVT_HTS] = @DVT_HTS,
	[SoLuong_HTS] = @SoLuong_HTS,
	ThueSuatGiam=@ThueSuatGiam
WHERE
	[ID] = @ID






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdate]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: HaiQuan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(38, 15),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam  VARCHAR(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			ThueSuatGiam=@ThueSuatGiam
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			ThueSuatGiam
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@ThueSuatGiam
		)		
	END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_Insert]'
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_Insert]
-- Database: HaiQuan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_Insert]
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(38, 15),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam VARCHAR(50),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_HangMauDich]
(
	[TKMD_ID],
	[SoThuTuHang],
	[MaHS],
	[MaPhu],
	[TenHang],
	[NuocXX_ID],
	[DVT_ID],
	[SoLuong],
	[TrongLuong],
	[DonGiaKB],
	[DonGiaTT],
	[TriGiaKB],
	[TriGiaTT],
	[TriGiaKB_VND],
	[ThueSuatXNK],
	[ThueSuatTTDB],
	[ThueSuatGTGT],
	[ThueXNK],
	[ThueTTDB],
	[ThueGTGT],
	[PhuThu],
	[TyLeThuKhac],
	[TriGiaThuKhac],
	[MienThue],
	[Ma_HTS],
	[DVT_HTS],
	[SoLuong_HTS],
	[ThueSuatGiam]
)
VALUES 
(
	@TKMD_ID,
	@SoThuTuHang,
	@MaHS,
	@MaPhu,
	@TenHang,
	@NuocXX_ID,
	@DVT_ID,
	@SoLuong,
	@TrongLuong,
	@DonGiaKB,
	@DonGiaTT,
	@TriGiaKB,
	@TriGiaTT,
	@TriGiaKB_VND,
	@ThueSuatXNK,
	@ThueSuatTTDB,
	@ThueSuatGTGT,
	@ThueXNK,
	@ThueTTDB,
	@ThueGTGT,
	@PhuThu,
	@TyLeThuKhac,
	@TriGiaThuKhac,
	@MienThue,
	@Ma_HTS,
	@DVT_HTS,
	@SoLuong_HTS,
	@ThueSuatGiam
	
)

SET @ID = SCOPE_IDENTITY()






GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_HangMauDich_InsertUpdateBy]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_HangMauDich_InsertUpdate]
-- Database: HaiQuan
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 28, 2008
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_HangMauDich_InsertUpdateBy]
	@ID bigint,
	@TKMD_ID bigint,
	@SoThuTuHang int,
	@MaHS varchar(12),
	@MaPhu varchar(30),
	@TenHang nvarchar(80),
	@NuocXX_ID char(3),
	@DVT_ID char(3),
	@SoLuong numeric(38, 15),
	@TrongLuong numeric(38, 15),
	@DonGiaKB numeric(38, 15),
	@DonGiaTT numeric(38, 15),
	@TriGiaKB numeric(38, 15),
	@TriGiaTT numeric(38, 15),
	@TriGiaKB_VND numeric(38, 15),
	@ThueSuatXNK numeric(5, 2),
	@ThueSuatTTDB numeric(5, 2),
	@ThueSuatGTGT numeric(5, 2),
	@ThueXNK money,
	@ThueTTDB money,
	@ThueGTGT money,
	@PhuThu money,
	@TyLeThuKhac numeric(5, 2),
	@TriGiaThuKhac money,
	@MienThue tinyint,
	@Ma_HTS varchar(50),
	@DVT_HTS char(3),
	@SoLuong_HTS numeric(18, 3),
	@ThueSuatGiam  VARCHAR(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_HangMauDich] 
		WHERE 	
			TKMD_ID = @TKMD_ID AND
			MaHS = @MaHS AND
			MaPhu = @MaPhu AND
			TenHang = @TenHang AND
			NuocXX_ID = @NuocXX_ID AND
			DVT_ID = @DVT_ID AND
			SoLuong = @SoLuong
)
	BEGIN
		UPDATE
			[dbo].[t_KDT_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[SoThuTuHang] = @SoThuTuHang,
			[MaHS] = @MaHS,
			[MaPhu] = @MaPhu,
			[TenHang] = @TenHang,
			[NuocXX_ID] = @NuocXX_ID,
			[DVT_ID] = @DVT_ID,
			[SoLuong] = @SoLuong,
			[TrongLuong] = @TrongLuong,
			[DonGiaKB] = @DonGiaKB,
			[DonGiaTT] = @DonGiaTT,
			[TriGiaKB] = @TriGiaKB,
			[TriGiaTT] = @TriGiaTT,
			[TriGiaKB_VND] = @TriGiaKB_VND,
			[ThueSuatXNK] = @ThueSuatXNK,
			[ThueSuatTTDB] = @ThueSuatTTDB,
			[ThueSuatGTGT] = @ThueSuatGTGT,
			[ThueXNK] = @ThueXNK,
			[ThueTTDB] = @ThueTTDB,
			[ThueGTGT] = @ThueGTGT,
			[PhuThu] = @PhuThu,
			[TyLeThuKhac] = @TyLeThuKhac,
			[TriGiaThuKhac] = @TriGiaThuKhac,
			[MienThue] = @MienThue,
			[Ma_HTS] = @Ma_HTS,
			[DVT_HTS] = @DVT_HTS,
			[SoLuong_HTS] = @SoLuong_HTS,
			ThueSuatGiam=@ThueSuatGiam
		WHERE
			TKMD_ID = @TKMD_ID AND
			MaHS = @MaHS AND
			MaPhu = @MaPhu AND
			TenHang = @TenHang AND
			NuocXX_ID = @NuocXX_ID AND
			DVT_ID = @DVT_ID AND
			SoLuong = @SoLuong
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_HangMauDich]
		(
			[TKMD_ID],
			[SoThuTuHang],
			[MaHS],
			[MaPhu],
			[TenHang],
			[NuocXX_ID],
			[DVT_ID],
			[SoLuong],
			[TrongLuong],
			[DonGiaKB],
			[DonGiaTT],
			[TriGiaKB],
			[TriGiaTT],
			[TriGiaKB_VND],
			[ThueSuatXNK],
			[ThueSuatTTDB],
			[ThueSuatGTGT],
			[ThueXNK],
			[ThueTTDB],
			[ThueGTGT],
			[PhuThu],
			[TyLeThuKhac],
			[TriGiaThuKhac],
			[MienThue],
			[Ma_HTS],
			[DVT_HTS],
			[SoLuong_HTS],
			ThueSuatGiam
		)
		VALUES 
		(
			@TKMD_ID,
			@SoThuTuHang,
			@MaHS,
			@MaPhu,
			@TenHang,
			@NuocXX_ID,
			@DVT_ID,
			@SoLuong,
			@TrongLuong,
			@DonGiaKB,
			@DonGiaTT,
			@TriGiaKB,
			@TriGiaTT,
			@TriGiaKB_VND,
			@ThueSuatXNK,
			@ThueSuatTTDB,
			@ThueSuatGTGT,
			@ThueXNK,
			@ThueTTDB,
			@ThueGTGT,
			@PhuThu,
			@TyLeThuKhac,
			@TriGiaThuKhac,
			@MienThue,
			@Ma_HTS,
			@DVT_HTS,
			@SoLuong_HTS,
			@ThueSuatGiam
		)		
	END





GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='1.3.0', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('1.3.0', getdate(), null)
	end