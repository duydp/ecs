using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public partial class KDT_GC_PhieuNhapKho 
    {
        List<KDT_GC_PhieuNhapKho_Hang> ListHangNhap = new List<KDT_GC_PhieuNhapKho_Hang>();
        //List<KDT_GC_PhieuXuatKho_Hang> ListHangXuat = new List<KDT_GC_PhieuXuatKho_Hang>();
        public List<KDT_GC_PhieuNhapKho_Hang> HangNhapCollection
        {
            set { this.ListHangNhap = value; }
            get { return this.ListHangNhap; }
        }
        //public List<KDT_GC_PhieuXuatKho_Hang> HangXuatCollection
        //{
        //    set { this.ListHangXuat = value; }
        //    get { return this.ListHangXuat; }
        //}
        public bool InsertUpdateFul()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update();
                    foreach (KDT_GC_PhieuNhapKho_Hang itemHangN in this.HangNhapCollection)
                    {
                        itemHangN.TKMD_ID = this.ID;
                        if (itemHangN.ID == 0)
                        {                           
                            itemHangN.ID = itemHangN.Insert(transaction);
                        }
                        else 
                        {
                            itemHangN.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    //foreach (KDT_GC_PhieuXuatKho_Hang itemHangX in this.HangXuatCollection)
                    //{
                    //    if (itemHangX.ID == 0)
                    //    {
                    //        itemHangX.TKMD_ID = this.ID;
                    //        itemHangX.ID = itemHangX.Insert(transaction);
                    //    }
                    //    else
                    //    {
                    //        itemHangX.Update(transaction);
                    //    }
                    //}
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public static KDT_GC_PhieuNhapKho LoadPNK(int tkmd_id)
        {
            string spName = "Select * from t_KDT_GC_PhieuNhapKho  where tkmd_id =" + tkmd_id;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            List<KDT_GC_PhieuNhapKho> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }
        public static int DeleteDynamic(string whereCondition, SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuNhapKho_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public bool DeleteFull(long ID)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string where_TKMD = "TKMD_ID =" + ID;
                    KDT_GC_PhieuNhapKho_Hang.DeleteDynamic(where_TKMD);
                    KDT_GC_PhieuNhapKho.DeleteDynamic("ID = " + ID, transaction);

                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public static decimal GetSoPhieuMax(long HopDongID)
        {
            string spName = "SELECT MAX(SoPhieu) FROM dbo.t_KDT_GC_PhieuNhapKho " +
                                  "WHERE  TKMD_ID in (Select ID from t_kdt_vnacc_tokhaimaudich where HopDong_ID = " + HopDongID + ")";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                return System.Convert.ToDecimal(obj);
            }
            else
                return 0;
        }
    }
}