﻿namespace Company.Interface.VNACCS.Vouchers
{
    partial class VNACC_BranchContainerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout dgListContainer_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_BranchContainerForm));
            this.grbContainer = new Janus.Windows.EditControls.UIGroupBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtSoSeal = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnDeleteContainer = new Janus.Windows.EditControls.UIButton();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnAddContainer = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox13 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListContainer = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbContainer)).BeginInit();
            this.grbContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).BeginInit();
            this.uiGroupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListContainer)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox13);
            this.grbMain.Controls.Add(this.grbContainer);
            this.grbMain.Size = new System.Drawing.Size(760, 383);
            // 
            // grbContainer
            // 
            this.grbContainer.AutoScroll = true;
            this.grbContainer.BackColor = System.Drawing.Color.Transparent;
            this.grbContainer.Controls.Add(this.label24);
            this.grbContainer.Controls.Add(this.label23);
            this.grbContainer.Controls.Add(this.txtSoSeal);
            this.grbContainer.Controls.Add(this.txtSoContainer);
            this.grbContainer.Controls.Add(this.btnDeleteContainer);
            this.grbContainer.Controls.Add(this.uiButton1);
            this.grbContainer.Controls.Add(this.btnAddContainer);
            this.grbContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.grbContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbContainer.ForeColor = System.Drawing.SystemColors.ControlText;
            this.grbContainer.Location = new System.Drawing.Point(0, 0);
            this.grbContainer.Name = "grbContainer";
            this.grbContainer.Size = new System.Drawing.Size(760, 133);
            this.grbContainer.TabIndex = 1;
            this.grbContainer.Text = "Thông tin về container";
            this.grbContainer.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(28, 67);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(48, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Số seal :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(28, 27);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Số container :";
            // 
            // txtSoSeal
            // 
            this.txtSoSeal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoSeal.Location = new System.Drawing.Point(115, 63);
            this.txtSoSeal.Name = "txtSoSeal";
            this.txtSoSeal.Size = new System.Drawing.Size(292, 21);
            this.txtSoSeal.TabIndex = 28;
            this.txtSoSeal.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoContainer
            // 
            this.txtSoContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoContainer.Location = new System.Drawing.Point(115, 23);
            this.txtSoContainer.Name = "txtSoContainer";
            this.txtSoContainer.Size = new System.Drawing.Size(292, 21);
            this.txtSoContainer.TabIndex = 27;
            this.txtSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnDeleteContainer
            // 
            this.btnDeleteContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteContainer.Image")));
            this.btnDeleteContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDeleteContainer.Location = new System.Drawing.Point(313, 95);
            this.btnDeleteContainer.Name = "btnDeleteContainer";
            this.btnDeleteContainer.Size = new System.Drawing.Size(94, 23);
            this.btnDeleteContainer.TabIndex = 30;
            this.btnDeleteContainer.Text = "Xóa";
            this.btnDeleteContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(115, 95);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(94, 23);
            this.uiButton1.TabIndex = 29;
            this.uiButton1.Text = "Nhập Excel";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnAddContainer
            // 
            this.btnAddContainer.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddContainer.Image = ((System.Drawing.Image)(resources.GetObject("btnAddContainer.Image")));
            this.btnAddContainer.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAddContainer.Location = new System.Drawing.Point(213, 95);
            this.btnAddContainer.Name = "btnAddContainer";
            this.btnAddContainer.Size = new System.Drawing.Size(94, 23);
            this.btnAddContainer.TabIndex = 29;
            this.btnAddContainer.Text = "Ghi";
            this.btnAddContainer.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAddContainer.Click += new System.EventHandler(this.btnAddContainer_Click);
            // 
            // uiGroupBox13
            // 
            this.uiGroupBox13.AutoScroll = true;
            this.uiGroupBox13.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox13.Controls.Add(this.dgListContainer);
            this.uiGroupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox13.Location = new System.Drawing.Point(0, 133);
            this.uiGroupBox13.Name = "uiGroupBox13";
            this.uiGroupBox13.Size = new System.Drawing.Size(760, 250);
            this.uiGroupBox13.TabIndex = 2;
            this.uiGroupBox13.Text = "Danh sách container của vận đơn nhánh";
            this.uiGroupBox13.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgListContainer
            // 
            this.dgListContainer.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListContainer.AlternatingColors = true;
            this.dgListContainer.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListContainer.ColumnAutoResize = true;
            dgListContainer_DesignTimeLayout.LayoutString = resources.GetString("dgListContainer_DesignTimeLayout.LayoutString");
            this.dgListContainer.DesignTimeLayout = dgListContainer_DesignTimeLayout;
            this.dgListContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListContainer.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListContainer.FrozenColumns = 3;
            this.dgListContainer.GroupByBoxVisible = false;
            this.dgListContainer.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListContainer.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListContainer.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListContainer.Location = new System.Drawing.Point(3, 17);
            this.dgListContainer.Margin = new System.Windows.Forms.Padding(0);
            this.dgListContainer.Name = "dgListContainer";
            this.dgListContainer.RecordNavigator = true;
            this.dgListContainer.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListContainer.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListContainer.Size = new System.Drawing.Size(754, 230);
            this.dgListContainer.TabIndex = 15;
            this.dgListContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_BranchContainerForm
            // 
            this.ClientSize = new System.Drawing.Size(760, 383);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_BranchContainerForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách Container của vận đơn nhánh";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grbContainer)).EndInit();
            this.grbContainer.ResumeLayout(false);
            this.grbContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox13)).EndInit();
            this.uiGroupBox13.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListContainer)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox grbContainer;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoContainer;
        private Janus.Windows.EditControls.UIButton btnDeleteContainer;
        private Janus.Windows.EditControls.UIButton btnAddContainer;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox13;
        private Janus.Windows.GridEX.GridEX dgListContainer;
        private Janus.Windows.EditControls.UIButton uiButton1;
    }
}
