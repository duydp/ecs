﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.QuanLyChungTu.CTTT;

namespace Company.Interface
{
    public partial class FrmQuanLyHopDongXuatKhau : Company.Interface.BaseForm
    {
        //Fields       
        private List<CTTT_HopDongXuatKhau> HDXKCollection = new List<CTTT_HopDongXuatKhau>();
        private List<HopDongThuongMai> HopDongThuongMaiCollections = new List<HopDongThuongMai>();
        private CTTT_HopDongXuatKhau hdxk;
        string errors = null;

        public FrmQuanLyHopDongXuatKhau()
        {
            InitializeComponent();
        }

        private void btnThemMoi_Click(object sender, EventArgs e)
        {
            if (!AddNewEntity())
                ShowMessage("Thất bại, Vui Lòng kiểm tra lại dữ liệu!", false);
            else
            {
                ShowMessage("Đã thêm vào lưới", false);
                this.refreshGrid();
            }
        }

        /// <summary>
        /// Thêm mới 1 Hợp Đồng Xuất Khẩu
        /// </summary>
        /// <returns> bool</returns>
        private bool AddNewEntity()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //thông tin không hợp lệ
                if (!ValidateInfo())
                    return false;
                //nếu đã tồn tại
                if (TestDelarationContract() > 0)
                {
                    ShowMessage("Số hợp đồng '" + cboSoHopDong.Text + "' này đã tồn tại!", false);
                    return false;
                }
                hdxk = new CTTT_HopDongXuatKhau();
                hdxk.SoHopDong = cboSoHopDong.Text;
                hdxk.NgayKy = dtNgayKyHopDong.Value;
                hdxk.TriGia = numTriGia.Value;
                hdxk.GhiChu = txtGhiChu.Text;

                CTTT_HopDongXuatKhau.InsertCTTT_HopDongXuatKhau(hdxk.SoHopDong, hdxk.NgayKy, hdxk.TriGia, hdxk.GhiChu);

                //làm rỗng các textboxt
                DeleteTextbox();

                btnThemMoi.Enabled = true;
                btnCapNhat.Enabled = false;

                return true;
            }
            catch (Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                errors = ex.ToString();

                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally //trả về trạng thái mặc định dù có lỗi or không.
            {
                Cursor = Cursors.Default;
            }
        }

        private void DeleteTextbox()
        {
            cboSoHopDong.Text = "";
            numTriGia.Value = 0;
            txtGhiChu.Text = "";
            dtNgayKyHopDong.Value = new DateTime(1900, 1, 1);

            hdxk = null;
        }

        private void FrmQuanLyHopDongXuatKhau_Load(object sender, EventArgs e)
        {
            LoadNamHopDongThuongMai();

            refreshGrid();
            btnCapNhat.Enabled = false;
        }

        private void refreshGrid()
        {
            HDXKCollection = CTTT_HopDongXuatKhau.SelectCollectionAll();
            bindingSource1.DataSource = HDXKCollection;
            grdHopDongXuatKhau.DataSource = bindingSource1.DataSource;
        }

        private void grdHopDongXuatKhau_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            btnThemMoi.Enabled = false;
            btnCapNhat.Enabled = true;
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                hdxk = (CTTT_HopDongXuatKhau)e.Row.DataRow;

                cboSoHopDong.Text = hdxk.SoHopDong;
                dtNgayKyHopDong.Value = hdxk.NgayKy;
                numTriGia.Value = hdxk.TriGia;
                txtGhiChu.Text = hdxk.GhiChu;
            }
        }

        //private void btnXoa_Click(object sender, EventArgs e)
        //{
        //    Janus.Windows.GridEX.GridEXSelectedItemCollection items = grdHopDongXuatKhau.SelectedItems;
        //    if (items.Count <= 0)
        //        return;
        //    if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
        //    {
        //        this.Cursor = Cursors.WaitCursor;

        //        foreach (Janus.Windows.GridEX.GridEXSelectedItem i in items)
        //        {
        //            if (i.RowType == Janus.Windows.GridEX.RowType.Record)

        //                hdxk = (CTTT_HopDongXuatKhau)i.GetRow().DataRow;

        //        }
        //    }
        //    try
        //    {
        //        //  CTTT_HopDongXuatKhau.DeleteHopDongXuatKhau(hdxk.Id);             
        //        ////  HDXKCollection.Remove(hdxk);               
        //        // // MLMessages("Xóa thành công!", "MSG_PUB14", "", false);               
        //        //  MessageBox.Show("Xóa thành công", "thông báo");
        //        //  refreshGrid();

        //        HDXKCollection.Remove(hdxk);
        //        CTTT_HopDongXuatKhau.DeleteHopDongXuatKhau(hdxk.Id);
        //        refreshGrid();
        //        //MLMessages("Xóa thành công!", "MSG_PUB14", "", false);
        //        //grdHopDongXuatKhau.Refetch();
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowMessage(" " + ex.Message, false);
        //        this.Cursor = Cursors.Default;
        //    }
        //    this.Cursor = Cursors.Default;

        //}

        private void btnCapNhat_Click_1(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn muốn cập nhật không?", true) == "Yes")
            {
                if (!ValidateInfo()) return;
                if (TestDelarationContract() > 1)
                {
                    ShowMessage("Số hợp đồng '" + cboSoHopDong.Text + "' này đã tồn tại!", false);
                    return;
                }
                try
                {
                    CTTT_HopDongXuatKhau.InsertUpdateCTTT_HopDongXuatKhau(hdxk.Id, cboSoHopDong.Text, dtNgayKyHopDong.Value, numTriGia.Value, txtGhiChu.Text);

                    refreshGrid();
                    btnCapNhat.Enabled = false;
                    btnThemMoi.Enabled = true;
                    ShowMessage("Cập nhật thành công!", false);

                    DeleteTextbox();
                }
                catch (Exception exe)
                {
                    ShowMessage("Cập nhật không thành công!", false, true, exe.Message);
                    Logger.LocalLogger.Instance().WriteMessage(exe);
                }
            }
            else
            {
                btnCapNhat.Enabled = false;
                btnThemMoi.Enabled = true;
                DeleteTextbox();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void LoadNamHopDongThuongMai()
        {
            try
            {
                List<String> collections = HopDongThuongMai.SelectAllNamHopDong(GlobalSettings.MA_DON_VI);

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();

                cboNam.Items.Clear();
                foreach (string nam in collections)
                {
                    cboNam.Items.Add(nam);
                }

                cboNam.SelectedIndex = (cboNam.Items.Count - 1);
                cboNam.Focus();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void LoadSoHopDongThuongMai()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                HopDongThuongMaiCollections = HopDongThuongMai.SelectAllBy(GlobalSettings.MA_DON_VI, cboNam.SelectedValue != null ? Convert.ToInt32(cboNam.SelectedValue) : DateTime.Today.Year);

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();

                cboSoHopDong.Items.Clear();
                foreach (HopDongThuongMai hd in HopDongThuongMaiCollections)
                {
                    cboSoHopDong.Items.Add(hd.SoHopDongTM);
                }

                cboSoHopDong.SelectedIndex = 0;
                cboSoHopDong.Focus();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void cboSoHopDong_Leave(object sender, EventArgs e)
        {
            try
            {
                //Lay thong tin hop dong
                HopDongThuongMai hd = HopDongThuongMaiCollections.Find(delegate(HopDongThuongMai o) { return o.SoHopDongTM == cboSoHopDong.Text; });

                //Gan gia tri
                dtNgayKyHopDong.Value = hd != null ? hd.NgayHopDongTM : DateTime.Today;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #region Begin VALIDATE HOP DONG

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateInfo()
        {
            bool isValid = true;

            err.SetError(cboSoHopDong, "");
            if (cboSoHopDong.Text == "")
            {
                err.SetError(cboSoHopDong, "Số hợp đồng không được để trống!");
                isValid = false;
            }

            if (cboSoHopDong.Text.Length > 150)
            {
                err.SetError(cboSoHopDong, "Số hợp đồng quá dài, 0< chiều dài<=150");
                isValid = false;
            }

            //TriGia
            err.SetError(numTriGia, "");
            if (numTriGia.Value == 0)
            {
                err.SetError(numTriGia, "Trị giá hợp đồng phải > 0");
                isValid = false;
            }
            //if (numTriGia.Value > 1000000000)
            //{
            //    err.SetError(numTriGia, "Trị giá hợp đồng phải <= 1000000000");
            //    isValid = false;
            //}
            //ghi chu
            if (txtGhiChu.Text.Length > 255)
            {
                err.SetError(numTriGia, "Chuổi quá dài, độ dài phải <= 255 ký tự");
                isValid = false;
            }

            return isValid;
        }

        #endregion End VALIDATE HOP DONG

        private void cboSoHopDong_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboSoHopDong.Invoke(new MethodInvoker(
                delegate
                {
                    cboSoHopDong_Leave(sender, e);
                }
                ));
        }

        /// <summary>
        /// kiểm tra nếu tồn tại rồi thì  không Insert or update
        /// </summary>
        /// <returns></returns>
        private int TestDelarationContract()
        {
            string condition = string.Format("SoHopDong = '{0}'", cboSoHopDong.Text);

            HDXKCollection = CTTT_HopDongXuatKhau.SelectCollectionDynamic(condition, null);
            int countRow = HDXKCollection.Count;
            return countRow;
        }

        int rowindex;
        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = grdHopDongXuatKhau.SelectedItems;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        CTTT_HopDongXuatKhau hdxk = (CTTT_HopDongXuatKhau)i.GetRow().DataRow;
                        try
                        {
                            hdxk.Delete();
                        }
                        catch (Exception exe)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(exe);
                        }
                    }
                }

                try
                {
                    ShowMessage("Thành công!", false);
                    refreshGrid();
                }
                catch { grdHopDongXuatKhau.Refresh(); }
            }
        }

        private void cboNam_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSoHopDongThuongMai();
        }

    }
}

