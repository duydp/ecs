using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_929 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        public int SoHoSo;
        private DataTable DtGhiChu = new DataTable();
        private int SoToKhaiNhap = 0;

        public BCNPLXuatNhapTon_929()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().GetBCNXT929(GlobalSettings.MA_DON_VI,this.LanThanhLy);
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
            
           
            lblTenDoanhNghiep.Text =GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHoSo > 0)
                lblSHSTK.Text = SoHoSo + "";
            ////lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName +".NgayDangKyNhap","{0:dd/MM/yy}");
            //lblNgayHoanThanhNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhNhap", "{0:dd/MM/yy}");
            //lblLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n"+ GlobalSettings.SoThapPhan.LuongNPL +"}");
            //lblTongLuongNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNhap", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            ////lblMaSP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaSP");
            //lblSoToKhaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiXuat");
            //lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            //if(GlobalSettings.SoThapPhan.MauBC04 == 0)
            //    lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyXuat", "{0:dd/MM/yy}");
            //else
            //    lblNgayHoanThanhXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanhXuat", "{0:dd/MM/yy}");
            lblLuongSXXK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTenDVT_SP.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_SP");
            //lblLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblSoToKhaiTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiTaiXuat");
            //lblNgayTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayTaiXuat", "{0:dd/MM/yy}");
            //lblLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblLuongNPLTaiXuat1.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongXuatTra.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            ////lblThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep", "{0:n3}");
            ////lblChuyenMucDichKhac.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ChuyenMucDichKhac", "{0:n3}");
            //lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonCuoi.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonCuoi", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //lblTongThanhKhoanTiep.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThanhKhoanTiep");
            xrLabel5.Text =xrLabel77.Text = GlobalSettings.TieudeNgay;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        
    }
}
