using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class TIA_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CMD { get; set; }
public PropertiesAttribute QT { get; set; }
public PropertiesAttribute QTU { get; set; }
public PropertiesAttribute UT { get; set; }
public PropertiesAttribute UTU { get; set; }

public TIA_HANG()
        {
CMD = new PropertiesAttribute(12, typeof(string));
QT = new PropertiesAttribute(15, typeof(int));
QTU = new PropertiesAttribute(4, typeof(string));
UT = new PropertiesAttribute(15, typeof(int));
UTU = new PropertiesAttribute(4, typeof(string));
TongSoByte = 60;
}

}public partial class EnumGroupID
    {
}

}
