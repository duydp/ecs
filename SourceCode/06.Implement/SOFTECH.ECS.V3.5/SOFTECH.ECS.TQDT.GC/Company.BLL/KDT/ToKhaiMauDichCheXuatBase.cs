using System;
using System.Data;
using System.Data.SqlClient;
using Company.GC.BLL;

namespace Company.GC.BLL.KDT
{
    public partial class ToKhaiMauDichCheXuat : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected long _SoTiepNhan;
        protected DateTime _NgayTiepNhan = new DateTime(1900, 01, 01);
        protected string _MaHaiQuan = String.Empty;
        protected int _SoToKhai;
        protected string _MaLoaiHinh = String.Empty;
        protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
        protected string _MaDoanhNghiep = String.Empty;
        protected string _TenDoanhNghiep = String.Empty;
        protected string _MaDaiLyTTHQ = String.Empty;
        protected string _TenDaiLyTTHQ = String.Empty;
        protected string _TenDonViDoiTac = String.Empty;
        protected string _ChiTietDonViDoiTac = String.Empty;
        protected string _SoGiayPhep = String.Empty;
        protected DateTime _NgayGiayPhep = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHanGiayPhep = new DateTime(1900, 01, 01);
        protected string _SoHopDong = String.Empty;
        protected DateTime _NgayHopDong = new DateTime(1900, 01, 01);
        protected DateTime _NgayHetHanHopDong = new DateTime(1900, 01, 01);
        protected string _SoHoaDonThuongMai = String.Empty;
        protected DateTime _NgayHoaDonThuongMai = new DateTime(1900, 01, 01);
        protected string _PTVT_ID = String.Empty;
        protected string _SoHieuPTVT = String.Empty;
        protected DateTime _NgayDenPTVT = new DateTime(1900, 01, 01);
        protected string _QuocTichPTVT_ID = String.Empty;
        protected string _LoaiVanDon = String.Empty;
        protected string _SoVanDon = String.Empty;
        protected DateTime _NgayVanDon = new DateTime(1900, 01, 01);
        protected string _NuocXK_ID = String.Empty;
        protected string _NuocNK_ID = String.Empty;
        protected string _DiaDiemXepHang = String.Empty;
        protected string _CuaKhau_ID = String.Empty;
        protected string _DKGH_ID = String.Empty;
        protected string _NguyenTe_ID = String.Empty;
        protected decimal _TyGiaTinhThue;
        protected decimal _TyGiaUSD;
        protected string _PTTT_ID = String.Empty;
        protected short _SoHang;
        protected short _SoLuongPLTK;
        protected string _TenChuHang = String.Empty;
        protected decimal _SoContainer20;
        protected decimal _SoContainer40;
        protected decimal _SoKien;
        protected decimal _TrongLuong;
        protected decimal _TongTriGiaKhaiBao;
        protected decimal _TongTriGiaTinhThue;
        protected string _LoaiToKhaiGiaCong = String.Empty;
        protected decimal _LePhiHaiQuan;
        protected decimal _PhiBaoHiem;
        protected decimal _PhiVanChuyen;
        protected decimal _PhiXepDoHang;
        protected decimal _PhiKhac;
        protected string _CanBoDangKy = String.Empty;
        protected bool _QuanLyMay;
        protected int _TrangThaiXuLy=-1;
        protected string _LoaiHangHoa = String.Empty;
        protected string _GiayTo = String.Empty;
        protected string _PhanLuong = String.Empty;
        protected string _MaDonViUT = String.Empty;
        protected string _TenDonViUT = String.Empty;
        protected decimal _SoTienKhoan;
        protected string _MaMid = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long SoTiepNhan
        {
            set { this._SoTiepNhan = value; }
            get { return this._SoTiepNhan; }
        }
        public DateTime NgayTiepNhan
        {
            set { this._NgayTiepNhan = value; }
            get { return this._NgayTiepNhan; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value.Trim(); }
            get { return this._MaHaiQuan.Trim(); }
        }
        public int SoToKhai
        {
            set { this._SoToKhai = value; }
            get { return this._SoToKhai; }
        }
        public string MaLoaiHinh
        {
            set { this._MaLoaiHinh = value; }
            get { return this._MaLoaiHinh; }
        }
        public DateTime NgayDangKy
        {
            set { this._NgayDangKy = value; }
            get { return this._NgayDangKy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
        }
        public string TenDoanhNghiep
        {
            set { this._TenDoanhNghiep = value; }
            get { return this._TenDoanhNghiep; }
        }
        public string MaDaiLyTTHQ
        {
            set { this._MaDaiLyTTHQ = value; }
            get { return this._MaDaiLyTTHQ; }
        }
        public string TenDaiLyTTHQ
        {
            set { this._TenDaiLyTTHQ = value; }
            get { return this._TenDaiLyTTHQ; }
        }
        public string TenDonViDoiTac
        {
            set { this._TenDonViDoiTac = value; }
            get { return this._TenDonViDoiTac; }
        }
        public string ChiTietDonViDoiTac
        {
            set { this._ChiTietDonViDoiTac = value; }
            get { return this._ChiTietDonViDoiTac; }
        }
        public string SoGiayPhep
        {
            set { this._SoGiayPhep = value; }
            get { return this._SoGiayPhep; }
        }
        public DateTime NgayGiayPhep
        {
            set { this._NgayGiayPhep = value; }
            get { return this._NgayGiayPhep; }
        }
        public DateTime NgayHetHanGiayPhep
        {
            set { this._NgayHetHanGiayPhep = value; }
            get { return this._NgayHetHanGiayPhep; }
        }
        public string SoHopDong
        {
            set { this._SoHopDong = value; }
            get { return this._SoHopDong; }
        }
        public DateTime NgayHopDong
        {
            set { this._NgayHopDong = value; }
            get { return this._NgayHopDong; }
        }
        public DateTime NgayHetHanHopDong
        {
            set { this._NgayHetHanHopDong = value; }
            get { return this._NgayHetHanHopDong; }
        }
        public string SoHoaDonThuongMai
        {
            set { this._SoHoaDonThuongMai = value; }
            get { return this._SoHoaDonThuongMai; }
        }
        public DateTime NgayHoaDonThuongMai
        {
            set { this._NgayHoaDonThuongMai = value; }
            get { return this._NgayHoaDonThuongMai; }
        }
        public string PTVT_ID
        {
            set { this._PTVT_ID = value; }
            get { return this._PTVT_ID; }
        }
        public string SoHieuPTVT
        {
            set { this._SoHieuPTVT = value; }
            get { return this._SoHieuPTVT; }
        }
        public DateTime NgayDenPTVT
        {
            set { this._NgayDenPTVT = value; }
            get { return this._NgayDenPTVT; }
        }
        public string QuocTichPTVT_ID
        {
            set { this._QuocTichPTVT_ID = value; }
            get { return this._QuocTichPTVT_ID; }
        }
        public string LoaiVanDon
        {
            set { this._LoaiVanDon = value; }
            get { return this._LoaiVanDon; }
        }
        public string SoVanDon
        {
            set { this._SoVanDon = value; }
            get { return this._SoVanDon; }
        }
        public DateTime NgayVanDon
        {
            set { this._NgayVanDon = value; }
            get { return this._NgayVanDon; }
        }
        public string NuocXK_ID
        {
            set { this._NuocXK_ID = value; }
            get { return this._NuocXK_ID; }
        }
        public string NuocNK_ID
        {
            set { this._NuocNK_ID = value; }
            get { return this._NuocNK_ID; }
        }
        public string DiaDiemXepHang
        {
            set { this._DiaDiemXepHang = value; }
            get { return this._DiaDiemXepHang; }
        }
        public string CuaKhau_ID
        {
            set { this._CuaKhau_ID = value; }
            get { return this._CuaKhau_ID; }
        }
        public string DKGH_ID
        {
            set { this._DKGH_ID = value; }
            get { return this._DKGH_ID; }
        }
        public string NguyenTe_ID
        {
            set { this._NguyenTe_ID = value; }
            get { return this._NguyenTe_ID; }
        }
        public decimal TyGiaTinhThue
        {
            set { this._TyGiaTinhThue = value; }
            get { return this._TyGiaTinhThue; }
        }
        public decimal TyGiaUSD
        {
            set { this._TyGiaUSD = value; }
            get { return this._TyGiaUSD; }
        }
        public string PTTT_ID
        {
            set { this._PTTT_ID = value; }
            get { return this._PTTT_ID; }
        }
        public short SoHang
        {
            set { this._SoHang = value; }
            get { return this._SoHang; }
        }
        public short SoLuongPLTK
        {
            set { this._SoLuongPLTK = value; }
            get { return this._SoLuongPLTK; }
        }
        public string TenChuHang
        {
            set { this._TenChuHang = value; }
            get { return this._TenChuHang; }
        }
        public decimal SoContainer20
        {
            set { this._SoContainer20 = value; }
            get { return this._SoContainer20; }
        }
        public decimal SoContainer40
        {
            set { this._SoContainer40 = value; }
            get { return this._SoContainer40; }
        }
        public decimal SoKien
        {
            set { this._SoKien = value; }
            get { return this._SoKien; }
        }
        public decimal TrongLuong
        {
            set { this._TrongLuong = value; }
            get { return this._TrongLuong; }
        }
        public decimal TongTriGiaKhaiBao
        {
            set { this._TongTriGiaKhaiBao = value; }
            get { return this._TongTriGiaKhaiBao; }
        }
        public decimal TongTriGiaTinhThue
        {
            set { this._TongTriGiaTinhThue = value; }
            get { return this._TongTriGiaTinhThue; }
        }
        public string LoaiToKhaiGiaCong
        {
            set { this._LoaiToKhaiGiaCong = value; }
            get { return this._LoaiToKhaiGiaCong; }
        }
        public decimal LePhiHaiQuan
        {
            set { this._LePhiHaiQuan = value; }
            get { return this._LePhiHaiQuan; }
        }
        public decimal PhiBaoHiem
        {
            set { this._PhiBaoHiem = value; }
            get { return this._PhiBaoHiem; }
        }
        public decimal PhiVanChuyen
        {
            set { this._PhiVanChuyen = value; }
            get { return this._PhiVanChuyen; }
        }
        public decimal PhiXepDoHang
        {
            set { this._PhiXepDoHang = value; }
            get { return this._PhiXepDoHang; }
        }
        public decimal PhiKhac
        {
            set { this._PhiKhac = value; }
            get { return this._PhiKhac; }
        }
        public string CanBoDangKy
        {
            set { this._CanBoDangKy = value; }
            get { return this._CanBoDangKy; }
        }
        public bool QuanLyMay
        {
            set { this._QuanLyMay = value; }
            get { return this._QuanLyMay; }
        }
        public int TrangThaiXuLy
        {
            set { this._TrangThaiXuLy = value; }
            get { return this._TrangThaiXuLy; }
        }
        public string LoaiHangHoa
        {
            set { this._LoaiHangHoa = value; }
            get { return this._LoaiHangHoa; }
        }
        public string GiayTo
        {
            set { this._GiayTo = value; }
            get { return this._GiayTo; }
        }
        public string PhanLuong
        {
            set { this._PhanLuong = value; }
            get { return this._PhanLuong; }
        }
        public string MaDonViUT
        {
            set { this._MaDonViUT = value; }
            get { return this._MaDonViUT; }
        }
        public string TenDonViUT
        {
            set { this._TenDonViUT = value; }
            get { return this._TenDonViUT; }
        }
        public decimal SoTienKhoan
        {
            set { this._SoTienKhoan = value; }
            get { return this._SoTienKhoan; }
        }
        public string MaMid
        {
            set { this._MaMid = value; }
            get { return this._MaMid; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) this._SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) this._NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) this._CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) this._QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) this._TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) this._LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) this._GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this._PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) this._TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) this._SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) this._MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCheXuatCollection SelectCollectionAll()
        {
            ToKhaiMauDichCheXuatCollection collection = new ToKhaiMauDichCheXuatCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                ToKhaiMauDichCheXuat entity = new ToKhaiMauDichCheXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public ToKhaiMauDichCheXuatCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCheXuatCollection collection = new ToKhaiMauDichCheXuatCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDichCheXuat entity = new ToKhaiMauDichCheXuat();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuanLyMay"))) entity.QuanLyMay = reader.GetBoolean(reader.GetOrdinal("QuanLyMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiayTo"))) entity.GiayTo = reader.GetString(reader.GetOrdinal("GiayTo"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTienKhoan"))) entity.SoTienKhoan = reader.GetDecimal(reader.GetOrdinal("SoTienKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaMid"))) entity.MaMid = reader.GetString(reader.GetOrdinal("MaMid"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(ToKhaiMauDichCheXuatCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiMauDichCheXuat item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, ToKhaiMauDichCheXuatCollection collection)
        {
            foreach (ToKhaiMauDichCheXuat item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(ToKhaiMauDichCheXuatCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiMauDichCheXuat item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, this._SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, this._NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
            this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
            this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
            this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
            this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
            this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
            this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
            this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
            this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
            this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
            this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
            this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
            this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
            this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
            this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
            this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
            this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
            this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
            this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
            this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
            this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
            this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
            this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
            this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
            this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
            this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
            this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
            this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
            this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
            this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
            this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.VarChar, this._CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@QuanLyMay", SqlDbType.Bit, this._QuanLyMay);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, this._TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@LoaiHangHoa", SqlDbType.Char, this._LoaiHangHoa);
            this.db.AddInParameter(dbCommand, "@GiayTo", SqlDbType.NVarChar, this._GiayTo);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this._PhanLuong);
            this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TenDonViUT", SqlDbType.NVarChar, this._TenDonViUT);
            this.db.AddInParameter(dbCommand, "@SoTienKhoan", SqlDbType.Money, this._SoTienKhoan);
            this.db.AddInParameter(dbCommand, "@MaMid", SqlDbType.VarChar, this._MaMid);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(ToKhaiMauDichCheXuatCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDichCheXuat item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_ToKhaiMauDichCheXuat_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(ToKhaiMauDichCheXuatCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDichCheXuat item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(ToKhaiMauDichCheXuatCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ToKhaiMauDichCheXuat item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}