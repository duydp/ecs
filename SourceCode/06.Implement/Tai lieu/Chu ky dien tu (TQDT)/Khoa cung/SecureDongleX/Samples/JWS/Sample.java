import java.io.*;
import javax.swing.*;
import java.awt.*;

public class Sample extends JFrame
{
	JPanel pButton = new JPanel(new FlowLayout());
	List lText = new List();

	int iRetcode,iHandle,iBlock_index;
	int[]  iUid = new int[1], iHid = new int[1] , iLen = new int[1];
	byte [] cBuffer = new byte[1000];
	String seed ="12345\0";
	String data;


	JSDX sdx = new JSDX();

	Sample()
	{
		super("SDX Sample");
		setLayout(new BorderLayout());
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		add(lText);
		setSize(300,300);
		setVisible(true);

		iRetcode = sdx.SDX_Find();
        if(iRetcode < 0)
        {
           lText.add("Error Finding SecureDongle X: " + Integer.toHexString(iRetcode));
            return;
        }
		if(iRetcode == 0)
		{
         lText.add("No SecureDongle X plugged");
			return;
		}

      iUid[0]=715400947;
      iRetcode = sdx.SDX_Open(1, iUid[0], iHid);
		if(iRetcode < 0)
		{
            lText.add("Error Opening SecureDongle X: " + Integer.toHexString(iRetcode));
			return;
		}
		lText.add("Success Open SecureDongle X. HID : " + Integer.toHexString(iHid[0]));
		iHandle = iRetcode;

		iBlock_index =0;
		data = "SDX Test Write";
		cBuffer = data.getBytes();

 		iRetcode = sdx.SDX_Write(iHandle, iBlock_index, cBuffer);

		if(iRetcode < 0)
		{
         lText.add("Error Write SecureDongle X: " + Integer.toHexString(iRetcode));
			return;
		}
		lText.add("Success Write to SecureDongle X");

		iBlock_index =0;
		cBuffer = new byte[512];

 		iRetcode = sdx.SDX_Read(iHandle, iBlock_index, cBuffer);

		if(iRetcode < 0)
		{
         lText.add("Error Read SecureDongle X: " + Integer.toHexString(iRetcode));
			return;
		}
		lText.add("Success Read : " + (new String(cBuffer, 0, BufLen(cBuffer, 512))));

		sdx.SDX_Close(iHandle);
		lText.add("SecureDongle X Closed ");

	}
	public static int BufLen(byte[] buffer, int len)
	{
	      int i = 0;
	      for (i = 0; i < len && buffer[i] != 0; i++) ;
	      return i;
    }
    public static void main(String[] args)
    {
		new Sample();

  	}
}


