﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class PhuKienGoodsShipment
    {
        [XmlElement("CustomsGoodsItem")]
        public List<PhuKienCustomsGoodsItem> CustomsGoodsItem { get; set; }



    }

    public class PhuKienCustomsGoodsItem
    {
        [XmlElement("sequence")]
        public string sequence { get; set; }

        [XmlElement("OldCommodity")]
        public PhuKien_OldCommodity OldCommodity { get; set; }

        [XmlElement("NewCommodity")]
        public PhuKien_NewCommodity NewCommodity { get; set; }

        [XmlElement("Commodity")]
        public PhuKien_Commodity Commodity { get; set; }

        [XmlElement("GoodsMeasure")]
        public PhuKien_GoodsMeasure GoodsMeasure { get; set; }
    }
     public class PhuKien_Commodity
    {
         /// <summary>
        /// Mã hàng hóa 
         /// </summary>
         [XmlElement("identification")]
         public string Identification { get; set; }
    }
    /// <summary>
    /// Chi tiết hàng cũ
    /// </summary>
    public class PhuKien_OldCommodity
    {
        /// <summary>
        /// Mã hàng cũ
        /// </summary>
        [XmlElement("oldIdentification")]
        public string OldIdentification { get; set; }
        [XmlElement("oldRegisterCustoms")]
        public string OldRegisterCustoms { get; set; }
        [XmlElement("oldType")]
        public string OldType { get; set; }


    }
    /// <summary>
    /// Chi tiết hàng mới
    /// </summary>
    public class PhuKien_NewCommodity
    {
        [XmlElement("newIdentification")]
        public string NewIdentification { get; set; }
        [XmlElement("newRegisterCustoms")]
        public string NewRegisterCustoms { get; set; }
        [XmlElement("newType")]
        public string NewType { get; set; }
    }
    public class PhuKien_GoodsMeasure
    {
        [XmlElement("oldQuantity")]
        public string OldQuantity { get; set; }

        [XmlElement("newQuantity")]
        public string NewQuantity { get; set; }

        [XmlElement("oldMeasureUnit")]
        public string OldMeasureUnit { get; set; }

        [XmlElement("newMeasureUnit")]
        public string NewMeasureUnit { get; set; }
    }
}
