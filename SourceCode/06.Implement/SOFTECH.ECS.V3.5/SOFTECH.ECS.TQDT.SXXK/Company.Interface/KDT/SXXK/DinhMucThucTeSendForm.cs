﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Company.Interface.Report.GC.TT39;
using Company.QuanTri;
using Company.Interface.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.Interface.KDT.SXXK
{
    public partial class DinhMucThucTeSendForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_SXXK_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_SXXK_DinhMucThucTeDangKy ();
        public KDT_SXXK_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
        public KDT_SXXK_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
        public bool isAdd = true;
        public bool isExits = false;
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        public DinhMucThucTeSendForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void LenhSanXuatGCForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (dinhMucThucTeDangKy.ID == 0)
                {
                    cbbLenhSanXuat.DataSource = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
                    cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                    cbbLenhSanXuat.ValueMember = "ID";
                    dinhMucThucTeDangKy.GuidString = Guid.NewGuid().ToString();
                    txtGuidString.Text = dinhMucThucTeDangKy.GuidString.ToString().ToUpper();
                    if (isEdit)
                    {
                        dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if(isCancel)
                    {
                        dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                }
                else
                {

                    Set();
                    cbbLenhSanXuat.DataSource = KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
                    cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                    cbbLenhSanXuat.ValueMember = "ID";
                    BindDataSP();
                    cbbLenhSanXuat.ReadOnly = true;
                }
                LoadDataDefault();
                setCommandStatus();
                CountToTalRow();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadDataDefault()
        {
            try
            {

                cbbMaNPL.DataSource = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                cbbMaNPL.DisplayMember = "Ma";
                cbbMaNPL.ValueMember = "Ma";
            }
            catch (Exception ex)
            {                
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = dinhMucThucTeDangKy.SPCollection;
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                dgListNPL.Refetch();
                dgListNPL.DataSource = DinhMucThucTeSP.DMCollection;
                dgListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void setCommandStatus()
        {
            if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                btnSelect.Enabled = false;
                btnDeleteSP.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
                btnSelect.Enabled = true;
                btnDeleteSP.Enabled = true;

                clcNgayTiepNhan.Value = DateTime.Now;
                lblTrangThai.Text = dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                btnSelect.Enabled = false;
                btnDeleteSP.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                btnSelect.Enabled = false;
                btnDeleteSP.Enabled = false;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
                btnSelect.Enabled = true;
                btnDeleteSP.Enabled = true;

                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã sửa , Chưa khai báo", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
                btnSelect.Enabled = true;
                btnDeleteSP.Enabled = true;

                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }

        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAddExcel":
                    btnImportExcel_Click(null,null);
                    break;
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdSend":
                    this.SendV5(false);
                    break;
                case "cmdSendEdit":
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    this.SendV5(false);
                    break;
                case "cmdCancel":
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    this.SendV5(true);
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGuidString();
                    break;
                case "cmdPrint":
                    this.Print();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }
        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageDinhMucThucTe(dinhMucThucTeDangKy);
                    dinhMucThucTeDangKy.InsertUpdateFull();
                    if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        dinhMucThucTeDangKy.TransferDataToSXXK();
                    }
                    Log.LogDinhMucTT(dinhMucThucTeDangKy, MessageTitle.CapNhatKetQuaXuLy, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    ShowMessage("Cập nhật thông tin thành công .", false);
                    setCommandStatus();
                    BindDataNPL();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void History()
        {
            try
            {
                Company.Interface.KDT.GC.HistoryForm f = new Company.Interface.KDT.GC.HistoryForm();
                f.ID = dinhMucThucTeDangKy.ID;
                f.loaiKhaiBao = LoaiKhaiBao.DinhMuc;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", dinhMucThucTeDangKy.ID, LoaiKhaiBao.DinhMuc), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Print()
        {
            try
            {
                Report_16_ĐMTT_GSQL f = new Report_16_ĐMTT_GSQL();
                f.BindReport(dinhMucThucTeDangKy);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateGuidString()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_DinhMucThucTeDangKy", "", Convert.ToInt32(dinhMucThucTeDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = dinhMucThucTeDangKy.ID;
                form.DeclarationIssuer = DeclarationIssuer.SXXK_DINH_MUC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void SendV5(bool isCancel)
        {
            if (dinhMucThucTeDangKy.SPCollection.Count == 0)
            {
                ShowMessage("Doanh nghiệp chưa nhập danh sách sản phẩm khai báo định mức", false);
                return;
            }

            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dinhMucThucTeDangKy.ID;
            if (sendXML.Load())
            {
                if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Khai báo đã được gửi đến hải quan. nhấn nút [Lấy phản hồi] để nhận thông tin từ hải quan", false);
                    return;
                }
            }
            try
            {
                if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                dinhMucThucTeDangKy.GuidString = Guid.NewGuid().ToString();
                txtGuidString.Text = dinhMucThucTeDangKy.GuidString.ToString().ToUpper();
                SXXK_DinhMucSP dm = Company.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_SXXK_DinhMuc(dinhMucThucTeDangKy, false, false, GlobalSettings.TEN_DON_VI);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dinhMucThucTeDangKy.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dinhMucThucTeDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dinhMucThucTeDangKy.MaHaiQuan).Trim() : dinhMucThucTeDangKy.MaHaiQuan.Trim()

                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.SXXK_DINH_MUC,
                                    Function = dm.Function,
                                    Reference = dinhMucThucTeDangKy.GuidString,
                                }
                                ,
                                dm);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dinhMucThucTeDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoDinhMuc);
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
                    sendXML.master_id = dinhMucThucTeDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            dinhMucThucTeDangKy.DeleteFull();
                        }
                        else
                        {
                            dinhMucThucTeDangKy.TransferDataToSXXK();
                        }
                        dinhMucThucTeDangKy.Update();
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        setCommandStatus();
                        FeedBackV5();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    sendForm.Message.XmlSaveMessage(dinhMucThucTeDangKy.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiDinhMuc);
                    ShowMessageTQDT(msgInfor, false);
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(dinhMucThucTeDangKy.MaHaiQuan, new Company.KDT.SHARE.Components.Messages.Send.SendEventArgs(ex));
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.DinhMucSendHandler(dinhMucThucTeDangKy, ref msgInfor, e);

        }
        private void FeedBackV5()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.DinhMuc;
            sendXML.master_id = dinhMucThucTeDangKy.ID;
            if (!sendXML.Load())
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.SXXK_DINH_MUC,
                Reference = dinhMucThucTeDangKy.GuidString,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.SXXK_DINH_MUC,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dinhMucThucTeDangKy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dinhMucThucTeDangKy.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dinhMucThucTeDangKy.MaHaiQuan).Trim() : dinhMucThucTeDangKy.MaHaiQuan.Trim()
                                          }, subjectBase, null);
            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        if (dinhMucThucTeDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            dinhMucThucTeDangKy.DeleteFull();
                        }
                        else
                        {
                            dinhMucThucTeDangKy.TransferDataToSXXK();
                        }
                        dinhMucThucTeDangKy.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        dinhMucThucTeDangKy.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        private bool Validate(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcTuNgay, errorProvider, "TỪ NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcDenNgay, errorProvider, "ĐẾN NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "TÌNH TRẠNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void Get()
        {
            try
            {
                dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                dinhMucThucTeDangKy.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text);
                dinhMucThucTeDangKy.NgayTiepNhan = clcNgayTiepNhan.Value;
                dinhMucThucTeDangKy.GhiChu = txtGhiChu.Text.ToString();
                dinhMucThucTeDangKy.LenhSanXuat_ID = Convert.ToInt32(cbbLenhSanXuat.Value);
                dinhMucThucTeDangKy.GuidString = txtGuidString.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }
        private void Set()
        {
            try
            {
                cbbLenhSanXuat.Value = dinhMucThucTeDangKy.LenhSanXuat_ID;
                txtGhiChu.Text = dinhMucThucTeDangKy.GhiChu;
                txtSoTiepNhan.Text = dinhMucThucTeDangKy.SoTiepNhan.ToString();
                clcNgayTiepNhan.Value = dinhMucThucTeDangKy.NgayTiepNhan;
                txtGuidString.Text = dinhMucThucTeDangKy.GuidString.ToString().ToUpper();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Save()
        {
            try
            {
                if (!Validate(false))
                    return;
                Get();
                if (dinhMucThucTeDangKy.SPCollection.Count == 0)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "Danh sách sản phẩm chưa được nhập", false);
                    return;
                }
                else
                {
                    string errorToTal = "";
                    string errorSP = "";
                    int i = 1;
                    foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                    {
                        if (item.DMCollection.Count==0)
                        {
                            errorSP += "" + i + " - " + item.MaSanPham + "\n";
                            i++;
                        }
                    }
                    if (!String.IsNullOrEmpty(errorSP))
                    {
                        errorToTal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " CHƯA NHẬP ĐỊNH MỨC";
                        ShowMessageTQDT("Thông báo từ hệ thống", errorToTal, false);
                        return;
                    }
                }
                dinhMucThucTeDangKy.InsertUpdateFull();
                ShowMessageTQDT("Thông báo từ hệ thống", "Lưu thành công", false);
                cbbLenhSanXuat.ReadOnly = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                string where = " LenhSanXuat_ID = " + Convert.ToInt32(cbbLenhSanXuat.Value) + " AND MaSanPham NOT IN ('',";
                KDT_LenhSanXuat lenhsanxuat = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                SanPhamLenhSanXuatRegistedForm SPRegistedForm = new SanPhamLenhSanXuatRegistedForm();
                foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                {
                    where += "'" + item.MaSanPham + "',";
                }
                where = where.Substring(0, where.Length - 1);
                where += ")";
                SPRegistedForm.whereCondition = where;
                SPRegistedForm.lenhsanxuat = lenhsanxuat;
                SPRegistedForm.ShowDialog(this);
                if (SPRegistedForm.SanPhamSelectCollection.Count >=1)
                {
                    List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionAll();
                    foreach (KDT_LenhSanXuat_SP item in SPRegistedForm.SanPhamSelectCollection)
                    {
                        KDT_SXXK_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
                        DinhMucThucTeSP.MaSanPham = item.MaSanPham;
                        DinhMucThucTeSP.TenSanPham = item.TenSanPham;
                        DinhMucThucTeSP.MaHS = item.MaHS;
                        DinhMucThucTeSP.DVT_ID = item.DVT_ID;
                        List<SXXK_DinhMuc> dmCollection = SXXK_DinhMuc.SelectCollectionDynamic(" MaSanPham = '" + item.MaSanPham + "' AND LenhSanXuat_ID = " + cbbLenhSanXuat.Value + "", "");
                        foreach (SXXK_DinhMuc dinhmuc in dmCollection)
                        {
                            KDT_SXXK_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                            DinhMucThucTeDinhMuc.MaSanPham = item.MaSanPham;
                            DinhMucThucTeDinhMuc.TenSanPham = item.TenSanPham;
                            DinhMucThucTeDinhMuc.DVT_SP = item.DVT_ID;
                            DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                            foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu npl in NPLCollection)
                            {
                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                {
                                    DinhMucThucTeDinhMuc.TenNPL = npl.Ten;
                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                    break;
                                }
                            }
                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                            if (dinhmuc.TyLeHaoHut > 0)
                            {
                                DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                                DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                            }
                            DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                        }
                        dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);   
                    }
                }
                BindDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            if (!Validate(false))
                return;
            Company.Interface.KDT.GC.ReadExcelDinhMucThucTeForm f = new Company.Interface.KDT.GC.ReadExcelDinhMucThucTeForm();
            f.dinhMucThucTeDangKy = dinhMucThucTeDangKy;
            f.ShowDialog(this);
            BindDataSP();
            CountToTalRow();
        }

        private void btnDeleteSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                List<KDT_SXXK_DinhMucThucTe_SP> ItemColl = new List<KDT_SXXK_DinhMucThucTe_SP>();
                if (dgListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_SXXK_DinhMucThucTe_SP)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_SXXK_DinhMucThucTe_SP item in ItemColl)
                    {
                        foreach (KDT_SXXK_DinhMucThucTe_DinhMuc itemss in item.DMCollection)
                        {
                            if(itemss.ID > 0)
                                itemss.Delete();                            
                        }
                        if (item.ID > 0)
                            item.Delete();
                        dinhMucThucTeDangKy.SPCollection.Remove(item);
                    }
                    BindDataSP();
                    CountToTalRow();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetDinhMuc()
        {
            try
            {
                DinhMucThucTeDinhMuc.MaSanPham = DinhMucThucTeSP.MaSanPham;
                DinhMucThucTeDinhMuc.TenSanPham = DinhMucThucTeSP.TenSanPham;
                DinhMucThucTeDinhMuc.DVT_SP = DinhMucThucTeSP.DVT_ID;
                DinhMucThucTeDinhMuc.MaNPL = cbbMaNPL.Value.ToString();
                DinhMucThucTeDinhMuc.TenNPL = txtTenNPL.Text.ToString();
                DinhMucThucTeDinhMuc.DVT_NPL = txtDonViTinhNPL.SelectedValue.ToString();
                DinhMucThucTeDinhMuc.MaHS = txtMaHSNPL.Text;
                DinhMucThucTeDinhMuc.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                DinhMucThucTeDinhMuc.GhiChu = txtGhiChuNPL.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetDinhMuc()
        {
            try
            {
                cbbMaNPL.Value = DinhMucThucTeDinhMuc.MaNPL;
                txtTenNPL.Text = DinhMucThucTeDinhMuc.TenNPL;
                txtDonViTinhNPL.SelectedValue = DinhMucThucTeDinhMuc.DVT_NPL;
                txtMaHSNPL.Text = DinhMucThucTeDinhMuc.MaHS;
                txtDinhMuc.Text = DinhMucThucTeDinhMuc.DinhMucSuDung.ToString();
                txtGhiChu.Text = DinhMucThucTeDinhMuc.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaNPL, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "TÊN/MÔ TẢ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDinhMuc, errorProvider, "ĐỊNH MỨC SỬ DỤNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetDinhMuc();
                if (isAdd)
                    DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                DinhMucThucTeDinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                cbbMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtDonViTinhNPL.Text = String.Empty;
                txtDinhMuc.Text = String.Empty;
                txtGhiChuNPL.Text = String.Empty;
                BindDataNPL();
                CountToTalRow();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void CountToTalRow()
        {
            try
            {
                int count = 0;
                foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                {
                    count += item.DMCollection.Count;
                }
                lblTotalRow.Text = count.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnExcel_Click(object sender, EventArgs e)
        {

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListNPL.SelectedItems;
                List<KDT_SXXK_DinhMucThucTe_DinhMuc> ItemColl = new List<KDT_SXXK_DinhMucThucTe_DinhMuc>();
                if (dgListNPL.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_SXXK_DinhMucThucTe_DinhMuc)i.GetRow().DataRow);
                        }
                    }
                    foreach (KDT_SXXK_DinhMucThucTe_DinhMuc item in ItemColl)
                    {
                        if(item.ID > 0)
                            item.Delete();
                        DinhMucThucTeSP.DMCollection.Remove(item);
                    }
                    BindDataNPL();
                    CountToTalRow();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
                        DinhMucThucTeSP = (KDT_SXXK_DinhMucThucTe_SP)i.GetRow().DataRow;
                        Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                        if (DinhMucThucTeSP.DMCollection.Count == 0)
                        {
                            style.BackColor = Color.GreenYellow;
                            i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                        }
                        else
                        {
                            style.BackColor = Color.Transparent;
                            i.GetRow().Cells["MaSanPham"].FormatStyle = style;
                        }
                    }
                }
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_SelectionChanged(object sender, EventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(grListSP.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                grListSP.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                    DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
                    DinhMucThucTeSP = (KDT_SXXK_DinhMucThucTe_SP)e.Row.DataRow;
                    Janus.Windows.GridEX.GridEXFormatStyle style = new Janus.Windows.GridEX.GridEXFormatStyle();
                    if (DinhMucThucTeSP.DMCollection.Count == 0)
                    {
                        style.BackColor = Color.GreenYellow;
                        e.Row.Cells["MaSanPham"].FormatStyle = style;
                    }
                    else
                    {
                        style.BackColor = Color.Transparent;
                        e.Row.Cells["MaSanPham"].FormatStyle = style;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucThucTeDinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                    DinhMucThucTeDinhMuc = (KDT_SXXK_DinhMucThucTe_DinhMuc)e.Row.DataRow;
                    SetDinhMuc();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaNPL_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaNPL.Value != null)
                {
                    List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionAll();
                    foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                    {
                        if (item.Ma == cbbMaNPL.Value.ToString())
                        {
                            txtTenNPL.Text = item.Ten.ToString();
                            txtDonViTinhNPL.SelectedValue = item.DVT_ID;
                            txtMaHSNPL.Text = item.MaHS;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_NPL"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_NPL"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSP_FormattingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    DinhMucThucTeSP = new KDT_SXXK_DinhMucThucTe_SP();
                    DinhMucThucTeSP = (KDT_SXXK_DinhMucThucTe_SP)e.Row.DataRow;
                    if (DinhMucThucTeSP.DMCollection.Count == 0)
                    {
                        e.Row.Cells["MaSanPham"].FormatStyle = new GridEXFormatStyle() { BackColor = Color.GreenYellow };
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value != null)
                {
                    if (Convert.ToInt64(cbbLenhSanXuat.Value) != dinhMucThucTeDangKy.LenhSanXuat_ID)
                    {
                        KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                        lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                        if (lenhSX.ID > 0)
                        {
                            clcTuNgay.Value = lenhSX.TuNgay;
                            clcDenNgay.Value = lenhSX.DenNgay;
                            txtPO.Text = lenhSX.SoDonHang;
                            cbbTinhTrang.SelectedValue = lenhSX.TinhTrang;
                            txtGhiChu.Text = lenhSX.GhiChu;
                            dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSX.ID;

                            List<KDT_SXXK_DinhMucThucTe_SP> SPCollection = new List<KDT_SXXK_DinhMucThucTe_SP>();
                            foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                            {
                                foreach (KDT_SXXK_DinhMucThucTe_DinhMuc itemss in item.DMCollection)
                                {
                                    if (itemss.ID > 0)
                                        itemss.Delete();
                                }
                                if (item.ID > 0)
                                    item.Delete();
                                SPCollection.Add(item);
                            }
                            foreach (KDT_SXXK_DinhMucThucTe_SP item in SPCollection)
                            {
                                dinhMucThucTeDangKy.SPCollection.Remove(item);
                            }
                            BindDataSP();
                        }
                    }
                    else
                    {
                        KDT_LenhSanXuat lenhSX = new KDT_LenhSanXuat();
                        lenhSX = KDT_LenhSanXuat.Load(Convert.ToInt64(cbbLenhSanXuat.Value));
                        if (lenhSX.ID > 0)
                        {
                            clcTuNgay.Value = lenhSX.TuNgay;
                            clcDenNgay.Value = lenhSX.DenNgay;
                            txtPO.Text = lenhSX.SoDonHang;
                            cbbTinhTrang.SelectedValue = lenhSX.TinhTrang;
                            txtGhiChu.Text = lenhSX.GhiChu;
                            dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSX.ID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
