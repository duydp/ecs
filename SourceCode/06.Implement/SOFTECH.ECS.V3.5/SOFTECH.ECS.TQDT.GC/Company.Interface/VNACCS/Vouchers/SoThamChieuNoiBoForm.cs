﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class SoThamChieuNoiBoForm : Company.Interface.BaseForm
    {
        public T_KDT_VNACC_SoThamChieuNoiBo soThamChieuNoiBo = new T_KDT_VNACC_SoThamChieuNoiBo();
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public bool isAdd = true;
        public SoThamChieuNoiBoForm()
        {
            InitializeComponent();
        }

        private void SoThamChieuNoiBoForm_Load(object sender, EventArgs e)
        {
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = T_KDT_VNACC_SoThamChieuNoiBo.SelectCollectionBy_TKMD_ID(TKMD.ID);
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                soThamChieuNoiBo.TKMD_ID = TKMD.ID;
                soThamChieuNoiBo.SoThamChieu = txtSoThamChieu.Text.ToString().Trim();
                soThamChieuNoiBo.InsertUpdate();
                soThamChieuNoiBo = new T_KDT_VNACC_SoThamChieuNoiBo();
                txtSoThamChieu.Text = String.Empty;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<T_KDT_VNACC_SoThamChieuNoiBo> ItemColl = new List<T_KDT_VNACC_SoThamChieuNoiBo>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa Số tham chiếu nội bộ này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KDT_VNACC_SoThamChieuNoiBo)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KDT_VNACC_SoThamChieuNoiBo item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                    }
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    soThamChieuNoiBo = new T_KDT_VNACC_SoThamChieuNoiBo();
                    soThamChieuNoiBo = (T_KDT_VNACC_SoThamChieuNoiBo)e.Row.DataRow;
                    txtSoThamChieu.Text = soThamChieuNoiBo.SoThamChieu;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
