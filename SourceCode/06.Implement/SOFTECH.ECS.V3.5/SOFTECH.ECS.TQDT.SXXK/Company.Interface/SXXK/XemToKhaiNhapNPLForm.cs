﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK
{
    public partial class XemToKhaiNhapNPLForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public string MaNPL = "";
        public XemToKhaiNhapNPLForm()
        {
            InitializeComponent();
        }

        private void XemDinhMucNPLForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Text = "Xem tờ khai nhập NPL '" + this.MaNPL + "'";
                this.dgList.Tables[0].Columns["TonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                this.dgList.Tables[0].Columns["TonDau"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

                this.dgList.DataSource = this.HSTL.GetDanhSachNPLNhapTonByMaNPL(GlobalSettings.SoThapPhan.LuongNPL, this.MaNPL);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách tờ khai nhập NPL _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }
    }
}

