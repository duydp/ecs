﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC01_DKNVLForm : BaseForm
    {
        public DataSet dsNguyenPhuLieu = new DataSet();
        public DataSet dsReturn = new DataSet();
        private BangKe01_DKNVL BC01 = new BangKe01_DKNVL();
        public HopDong HD = new HopDong();
        public Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
        public ReportViewBC01_DKNVLForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            dsNguyenPhuLieu = NPL.GetNguyenPhuLieuDangKy(this.HD.ID);
            dsReturn = this.CreatSchemaDataSetNewRP01();
            this.BC01.HD = this.HD;
            if (dsReturn.Tables.Count > 0)
                this.BC01.BindReport(dsReturn.Tables[0]);
            else
                ShowMessage("Không tìm thấy nguyên phụ liệu hoặc phụ kiện nào cho hợp đồng gia công này.", false);
            printControl1.PrintingSystem = this.BC01.PrintingSystem;
            this.BC01.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch
            {
                ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                ShowMessage("Lỗi khi in!", false);
            }   
        }


        public DataSet CreatSchemaDataSetNewRP01()
        {
            DataSet dsTemp = new DataSet();
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu");
            DataColumn[] dcCol = new DataColumn[9];

            dcCol[0] = new DataColumn("STT", typeof(int));
            dcCol[0].Caption = "STT";
            dcCol[1] = new DataColumn("Ten", typeof(string));
            dcCol[1].Caption = "Ten";
            dcCol[2] = new DataColumn("Ma", typeof(string));
            dcCol[2].Caption = "Ma";
            dcCol[3] = new DataColumn("DVT", typeof(string));
            dcCol[3].Caption = "DVT";
            dcCol[4] = new DataColumn("SoLuong", typeof(decimal));
            dcCol[4].Caption = "SoLuong";
            dcCol[5] = new DataColumn("TriGia", typeof(decimal));
            dcCol[5].Caption = "TriGia";
            dcCol[6] = new DataColumn("Loai", typeof(string));
            dcCol[6].Caption = "Loai";
            dcCol[7] = new DataColumn("HinhThuc", typeof(string));
            dcCol[7].Caption = "HinhThuc";
            dcCol[8] = new DataColumn("GhiChu", typeof(string));
            dcCol[8].Caption = "GhiChu";

            dttemp.Columns.AddRange(dcCol);

            int sothutu = 1;

            foreach (DataRow dr in dsNguyenPhuLieu.Tables[0].Rows)
            {
                DataRow drData = dttemp.NewRow();
                drData[0] = sothutu;
                drData[1] = dr.ItemArray[2].ToString();
                drData[2] = dr.ItemArray[1].ToString();
                drData[3] = DonViTinh_GetName(dr.ItemArray[3]);
                drData[4] = Decimal.Parse(dr.ItemArray[4].ToString());
                drData[6] = dr.ItemArray[2].ToString();
                drData[7] = "Nhập khẩu và cung ứng";
                sothutu++;
                dttemp.Rows.Add(drData);
            }

            dsTemp.Tables.Add(dttemp);
            return dsTemp;
        }
    }
}