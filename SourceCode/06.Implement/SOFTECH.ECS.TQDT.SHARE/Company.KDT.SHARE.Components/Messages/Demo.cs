﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public class Demo
    {
        class MySend : IMessage
        {
            public MessageInfo Send(string password)
            {
                return new MessageInfo();
            }
            public MessageInfo Request(string password)
            {
                return new MessageInfo();
            }
        }
        public void Excute()
        {
            //KhaiBaoToKhaiViet khaibao = new KhaiBaoToKhaiViet();
            //IMessage msg;
            //khaibao.ProcessRequest(msg.Send("myPass"));

        }
        #region Tờ khai nhập
        public static KD_ToKhaiNhap KD_tokhainhap()
        {
            KD_ToKhaiNhap tkn = new KD_ToKhaiNhap()
                        {
                            //Loại chứng từ (= 929) 
                            Issuer = "929",
                            //Số tham chiếu tờ khai
                            Reference = "reference",
                            //Ngay Khai bao
                            Issue = DateTime.Now.ToString().ToString(),
                            //Chức năng (Khai báo = "8", sửa =5)
                            Function = "8",
                            //Nơi khai báo
                            IssueLocation = "issueLocation",
                            //Trạng thái của chứng từ
                            Status = "1",
                            //Số tiếp nhận tờ khai; Dùng trong trường hợp khai sửa bản đăng ký
                            CustomsReference = "12",
                            //Ngày đăng ký chứng từ 
                            Acceptance = DateTime.Now.ToString().ToString(),
                            //Đơn vị HQ khai báo
                            DeclarationOffice = "declarationOffice",
                            //Số lượng hàng
                            GoodsItem = "1",
                            //Số lượng chứng từ, phụ lục đính kèm
                            LoadingList = "1",
                            //Trọng lượng (kg)
                            TotalGrossMass = "23",
                            //trong luong tinh(kg)
                            TotalNetGrossMass = "2f",
                            //Mã loại hình

                            NatureOfTransaction = "natureOfTransaction",
                            //Mã phương thức thanh toán
                            PaymentMethod = "CASH",
                            //////////List Dai_ly
                            Agents = new List<Agent>(),
                            //Nguyên tệ
                            CurrencyExchange = new CurrencyExchange()
                            {
                                CurrencyType = "CurrencyType",
                                Rate = "12.3",

                            },
                            ////////////Số kiện
                            DeclarationPackaging = new Packaging()
                            {

                                Quantity = "23",


                            },
                            //List AdditionalDocument(Hop dong, Van Don, Giay Phep)
                            AdditionalDocuments = new List<AdditionalDocument>(),
                            // Hóa đơn thương mại
                            Invoice = new Invoice()
                            {
                                Issue = DateTime.Now.ToString().ToString(),
                                Reference = "abc",
                                Type = "abc"

                            },
                            ////////Người nhập khẩu
                            Importer = new NameBase()
                            {
                                Name = "Nhan",
                                Identity = "12"
                            },
                            ///////////////Người đại diện doanh nghiệp
                            RepresentativePerson = new RepresentativePerson()
                            {
                                ContactFunction = "chuc vu",
                                Name = "name"
                            },
                            /////////////////Thông tin tờ khai trị giá
                            AdditionalInformations = new List<AdditionalInformation>(),
                            
                            //////////////////////Thông tin hàng hóa
                            GoodsShipment = new GoodsShipment()
                            {
                                //Mã nước xuất khẩu  
                                ExportationCountry = "CU",
                                //Người giao hàng
                                Consignor = new NameBase()
                                {
                                    Name = "TenNguoiGH",
                                    Identity = "MaNguoiGH"
                                },
                                //Người nhận hàng
                                Consignee = new NameBase()
                                {
                                    Identity = "May 10",
                                    Name = "MaNguoiNhHang"
                                },
                                //Người nhận hàng trung gian
                                NotifyParty = new NameBase()
                                {
                                    Identity = "MaNguoiTB",
                                    Name = "TenNguoiThongBao"
                                },
                                // Địa điểm giao hàng
                                DeliveryDestination = new DeliveryDestination()
                                {
                                    Line = "DDGHang"
                                },
                                //Cửa khẩu nhập
                                EntryCustomsOffice = new LocationNameBase()
                                {
                                    Code = "C02L",
                                    Name = "(Ma khong dung nua)Cang Tan cang Tp.HCM"
                                }
                                ,
                                //Cửa khẩu xuất
                                ExitCustomsOffice = new LocationNameBase()
                                {
                                    Name = "CangXH_VD",
                                    Code = "code"
                                },
                                // Người xuất khẩu
                                Exporter = new NameBase()
                                {
                                    Name = "Ten nguoi uy thac Dia chi nguoi uy thac",
                                    Identity = ""

                                },
                                //Điều kiện giao hàng
                                TradeTerm = new TradeTerm()
                                {
                                    Condition = "CF"
                                },
                                //Hàng khai báo
                                CustomsGoodsItems = new List<CustomsGoodsItem>(),

                            },
                            ////////////////Danh sách giấy phép NK đi kèm
                            Licenses = new List<License>(),
                            //Danh sách hợp đồng khai kèm
                            ContractDocuments = new List<ContractDocument>(),
                            //Danh sách hóa đơn thương mại khai kèm
                            CommercialInvoices = new List<CommercialInvoice>(),
                            //Danh sách C/O khai kèm 
                            CertificateOfOrigins = new List<CertificateOfOrigin>(),
                            //Danh sách vận đơn đi kèm
                            BillOfLadings = new List<BillOfLading>(),
                            //Đơn xin chuyển cửa khẩu
                            CustomsOfficeChangedRequests = new List<CustomsOfficeChangedRequest>()
                           ,
                            //Danh sách chứng từ đính kèm, có thể lặp lại
                            AttachDocuments = new List<AttachDocumentItem>(),
                            //
                            AdditionalDocument = new List<AdditionalDocument>()


                        };
            tkn.AdditionalInformations.Add(new AdditionalInformation()
            {
                Content = new Content() { Text = "ghi chu" },

                Statement = "Statement",


            });
            //Add CustomsOfficeChangedRequests
            tkn.CustomsOfficeChangedRequests.Add(new CustomsOfficeChangedRequest()
            {
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" },
                    ExaminationPlace = "examinationPlace",
                    Time = DateTime.Now.ToString().ToString(),
                    Route = "route"
                },
            });
            //AdditionalDocuments
            tkn.AdditionalDocuments.Add(new AdditionalDocument()
            {
                Type = "Type",
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "VN",
                Issuer = "issuer",
                Expire = DateTime.Now.ToString().ToString(),
                IsDebt = "1",
                Submit = DateTime.Now.ToString().ToString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },


            });
            tkn.AdditionalDocuments.Add(new AdditionalDocument()
            {
                Type = "Type",
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "VN",
                Issuer = "issuer",
                Expire = DateTime.Now.ToString().ToString(),
                IsDebt = "1",
                Submit = DateTime.Now.ToString().ToString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },


            });
            //AttachDocumentItem
            tkn.AttachDocuments.Add(new AttachDocumentItem()
            {
                Sequence = "STT",
                Issuer = "issuer",
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "So chug tu",
                Description = "",
                AttachedFiles = new List<AttachedFile>(),

            });
            //AttachedFiles
            tkn.AttachDocuments[0].AttachedFiles.Add(new AttachedFile()
            {
                Content = new Content() { Base64 = "bin.base64", Text = "abc" },
                FileName = "001.JPG"
            });
            //Add BillOfLadings
            tkn.BillOfLadings.Add(new BillOfLading()
            {
                Reference = "reference1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "issueLocation1",
                BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = "identity",
                    Identification = "identification",
                    Journey = "journey",
                    ModeAndType = "modeAndType",
                    Departure = DateTime.Now.ToString().ToString(),
                    RegistrationNationality = "RegistrationNationality"
                },
                MyProCarrierperty = new NameBase() { Identity = "Identity", Name = "Name" },
                Consignment = new Consignment()
                {
                    Consignor = new NameBase { Identity = "identity", Name = "name" },
                    Consignee = new NameBase { Identity = "", Name = "" },
                    NotifyParty = new NameBase { Identity = "", Name = "" },
                    LoadingLocation = new LoadingLocation() { Name = "Name", Code = "Code", Loading = DateTime.Now.ToString().ToString() },
                    UnloadingLocation = new UnloadingLocation() { Name = "Name", Code = "Code", Arrival = DateTime.Now.ToString().ToString() },
                    DeliveryDestination = new DeliveryDestination() { Line = "" },
                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = "12",
                        Type = "Type",
                    },
                    TransportEquipments = new List<TransportEquipment>(),
                    ConsignmentItem = new ConsignmentItem()
                    {
                        Sequence = "1",
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = "2",
                            Type = "Type",
                            MarkNumber = "MarkNumber",

                        },
                        Commodity = new Commodity()
                        {
                            Description = "ten hang",
                            Identification = "identification",
                            TariffClassification = "TariffClassification"
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            Quantity = "23.1",
                            MeasureUnit = "measureUnit"
                        },
                        EquipmentIdentification = new EquipmentIdentification() { identification = "iden" },

                    }
                }
            });
            //TransportEquipments
            tkn.BillOfLadings[0].Consignment.TransportEquipments.Add(new TransportEquipment()
            {
                Characteristic = "characteris",
                Fullness = "fullness",
                Seal = "seal",
                EquipmentIdentifications = new EquipmentIdentification() { identification = "identification" }
            });
            tkn.BillOfLadings[0].Consignment.TransportEquipments.Add(new TransportEquipment()
            {
                Characteristic = "characteris",
                Fullness = "fullness",
                Seal = "seal",
                EquipmentIdentifications = new EquipmentIdentification() { identification = "identification" }
            });
            //Add CertificateOfOrigins
            tkn.CertificateOfOrigins.Add(new CertificateOfOrigin()
            {
                Reference = "reference1",
                Type = "Type1",
                Issuer = "Issuer1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "CU",
                Representative = "Representative1",
                ExporterEx = new NameBase() { Identity = "Identity", Name = "Name" },
                ExportationCountryEx = new LocationNameBase() { Name = "Name", Code = "Code" },
                ImporterEx = new NameBase() { Identity = "Identity", Name = "Name" },
                ImportationCountryEx = new LocationNameBase() { Name = "Name", Code = "Code" },
                LoadingLocation = new LoadingLocation() { Name = "Name", Code = "Code", Loading = DateTime.Now.ToString().ToString() },
                UnloadingLocation = new UnloadingLocation() { Name = "Name", Code = "Code" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },
                GoodsItems = new List<GoodsItem>(),
                IsDebt = "23",
                Submit = "34"
            });
            //GoodsItems
            tkn.CertificateOfOrigins[0].GoodsItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "21",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUS", },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "2",
                    Type = "Type",
                    MarkNumber = "MarkNumber",

                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                Origin = new Origin() { OriginCountry = "VN" },
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                },

            });
            tkn.CertificateOfOrigins[0].GoodsItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "21",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUS", },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "2",
                    Type = "Type",
                    MarkNumber = "MarkNumber",

                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                Origin = new Origin() { OriginCountry = "VN" },
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                },

            });

            //CommercialInvoices

            tkn.CommercialInvoices.Add(new CommercialInvoice()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Seller = new NameBase() { Identity = "identity", Name = "Name" },
                Buyer = new NameBase() { Identity = "identity", Name = "Name" },
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                Payment = new Payment()
                {
                    Method = "CASH"
                },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD"

                },
                TradeTerm = new TradeTerm() { Condition = "condition" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),


            });
            //CommercialInvoiceItems
            tkn.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "23.2",
                StatisticalValue = "23.1",
                Origin = new Origin() { OriginCountry = "VN" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
          

            //contractdocments

            tkn.ContractDocuments.Add(new ContractDocument()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Expire = DateTime.Now.ToString().ToString(),
                Payment = new Payment()
                {
                    Method = "CASH"

                },
                TradeTerm = new TradeTerm()
                {
                    Condition = "condition"

                },
                DeliveryDestination = new DeliveryDestination() { Line = "line" },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD",


                },
                TotalValue = "2332.324",
                Buyer = new NameBase() { Name = "buyer1", Identity = "34" },
                Seller = new NameBase()
                {
                    Name = "seller1",
                    Identity = "s01"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },
                ContractItems = new List<ContractItem>(),

            });
            //ContractItems
            tkn.ContractDocuments[0].ContractItems.Add(new ContractItem()
            {
                unitPrice = "32",
                statisticalValue = "34.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                }
            });
            tkn.ContractDocuments[0].ContractItems.Add(new ContractItem()
            {
                unitPrice = "2",
                statisticalValue = "4.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                }
            });
            //licenses
            tkn.Licenses.Add(new License()
            {
                Issuer = "issuer",
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "CU",
                Type = "Type",
                Expire = DateTime.Now.ToString().ToString()

            });
            tkn.Licenses.Add(new License()
            {
                Issuer = "issuer1",
                Reference = "reference1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "VN",
                Type = "Type1",
                Expire = DateTime.Now.ToString().ToString()

            });
            tkn.Licenses.Add(new License()
            {
                Issuer = "issuer2",
                Reference = "reference2",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "MY",
                Type = "Type2",
                Expire = DateTime.Now.ToString().ToString()

            });
            ///////////////////////Agent
            tkn.Agents.Add(new Agent()
            {
                Identity = "0100101308",
                Name = "May 10",
                Status = "1"
            });
            tkn.Agents.Add(new Agent()
            {
                Name = "Ðon v? cú mó:MaNguoiUyThac kh?ng cú trong danh m?c",
                Identity = "MaNguoiUyThac",
                Status = "2"
            });
            tkn.Agents.Add(new Agent()
            {
                Name = "May 10 05113791111 05113791112",
                Identity = "0100101308",
                Status = "4"
            });
            /////////////////////AdditionalDocument-Hop Dong
            tkn.AdditionalDocuments.Add(new AdditionalDocument()

              {
                  Issue = DateTime.Now.ToString().ToString(),
                  Reference = "SOHDDTKN",
                  Type = "32",
                  Name = "HopDong",
                  Expire = DateTime.Now.ToString().ToString()
              });
            /////////////////////AdditionalDocument-So Van Don
            tkn.AdditionalDocuments.Add(new AdditionalDocument()

            {
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "SOVANDON",
                Type = "32",
                Name = "Van Don",

            });
            /////////////////////AdditionalDocument-Giay phep
            tkn.AdditionalDocuments.Add(new AdditionalDocument()

            {
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "SOGPTKN",
                Type = "32",
                Name = "Giay phep",
                Expire = DateTime.Now.ToString().ToString()
            });

            //Thông tin tờ khai trị giá
            tkn.GoodsShipment.CustomsGoodsItems.Add(new CustomsGoodsItem()
            {
                CustomsValue = "12.32",
                Sequence = "1",
                StatisticalValue = "324.21",
                UnitPrice = "3.12",
                StatisticalUnitPrice = "32.23",
                Manufacturer = new NameBase()
                {
                    Identity = "s",
                    Name = "fd"
                },
                Origin = new Origin()
                {
                    OriginCountry = "VN"
                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "Identification",
                    TariffClassification = "3434343",
                    TariffClassificationExtension = "dsf",
                    Brand = "nhanhieu",
                    Grade = "QuyCach",
                    Ingredients = "ingredients",
                    ModelNumber = "number1",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine()
                    {
                        ItemCharge = "",
                        Line = ""

                    }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.2",
                    MeasureUnit = "measureUnit"
                },
                AdditionalInformations = new List<AdditionalInformation>(),
                // tkn.GoodsShipment.CustomsGoodsItems[1].Commodity.DutyTaxFee.Add(new duty),
                //cho nay ,cai lop DutyTaxFee lam sao de new dtuong no bay gi,thank
                // neu ko dc thi sua index thanh` 1 nhe',ok
                CustomsValuation = new CustomsValuation()
                {
                    ExitToEntryCharge = "23.2",
                    FreightCharge = "322.324",
                    Method = "method",
                    OtherChargeDeduction = "32"
                },

                ValuationAdjustments = new List<ValuationAdjustment>()

            });
            tkn.GoodsShipment.CustomsGoodsItems.Add(new CustomsGoodsItem()
            {
                CustomsValue = "12.32",
                Sequence = "1",
                StatisticalValue = "324.21",
                UnitPrice = "3.12",
                StatisticalUnitPrice = "32.23",
                Manufacturer = new NameBase()
                {
                    Identity = "s",
                    Name = "fd"
                },
                Origin = new Origin()
                {
                    OriginCountry = "VN"
                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "Identification",
                    TariffClassification = "3434343",
                    TariffClassificationExtension = "dsf",
                    Brand = "nhanhieu",
                    Grade = "QuyCach",
                    Ingredients = "ingredients",
                    ModelNumber = "number1",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine()
                    {
                        ItemCharge = "",
                        Line = ""

                    }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.2",
                    MeasureUnit = "measureUnit"
                },
                AdditionalInformations = new List<AdditionalInformation>(),

                // tkn.GoodsShipment.CustomsGoodsItems[1].Commodity.DutyTaxFee.Add(new duty),
                //cho nay ,cai lop DutyTaxFee lam sao de new dtuong no bay gi,thank
                // neu ko dc thi sua index thanh` 1 nhe',ok
                CustomsValuation = new CustomsValuation()
                {
                    ExitToEntryCharge = "23.2",
                    FreightCharge = "322.324",
                    Method = "method",
                    OtherChargeDeduction = "32"
                },
                ValuationAdjustments = new List<ValuationAdjustment>()

            });

            //ValuationAdjustment
            tkn.GoodsShipment.CustomsGoodsItems[0].ValuationAdjustments.Add(new ValuationAdjustment()
            {
                Addition = "32",
                Percentage = "3.2",
                Amount = "3.4"
            });
            tkn.GoodsShipment.CustomsGoodsItems[0].ValuationAdjustments.Add(new ValuationAdjustment()
            {
                Addition = "23",
                Percentage = "3.2",
                Amount = "3.4"
            });
            tkn.GoodsShipment.CustomsGoodsItems[0].ValuationAdjustments.Add(new ValuationAdjustment()
            {
                Addition = "22",
                Percentage = "3.2",
                Amount = "3.4"
            });
            tkn.GoodsShipment.CustomsGoodsItems[0].ValuationAdjustments.Add(new ValuationAdjustment()
            {
                Addition = "23",
                Percentage = "3.2",
                Amount = "3.4"
            });
            // tkn.GoodsShipment.CustomsGoodsItems.GoodsMeasure
            tkn.GoodsShipment.CustomsGoodsItems[1].GoodsMeasure = new GoodsMeasure()
            {
                Quantity = "34",
                MeasureUnit = "23",
            };
            //tkn.licenses.license.AdditionalInformation
            tkn.Licenses[0].AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Thoong tin" } };
            //tkn.licenses.license.AdditionalInformation
            tkn.Licenses[1].AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Thoong tin" } };
            //tkn.licenses.license.AdditionalInformation
            tkn.Licenses[2].AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Thoong tin" } };

            //DutyTaxFee
            tkn.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "23.2",
                DutyRegime = "DutyRegime",
                SpecificTaxBase = "23",
                Tax = "Tax",
                Type = "Type"

            }
            );

            tkn.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "2.2",
                DutyRegime = "DutyRegime2",
                SpecificTaxBase = "3222",
                Tax = "Tax",
                Type = "Type"

            }
            );
            tkn.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "3.2",
                DutyRegime = "DutyRegime3",
                SpecificTaxBase = "232",
                Tax = "Tax",
                Type = "Type"

            }
            );
            //DutyTaxFee
            tkn.GoodsShipment.CustomsGoodsItems[1].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "23.2",
                DutyRegime = "DutyRegime",
                SpecificTaxBase = "23",
                Tax = "Tax",
                Type = "Type"

            }
            );

            tkn.GoodsShipment.CustomsGoodsItems[1].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "2.2",
                DutyRegime = "DutyRegime2",
                SpecificTaxBase = "3222",
                Tax = "Tax",
                Type = "Type"

            }
            );
            tkn.GoodsShipment.CustomsGoodsItems[1].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "3.2",
                DutyRegime = "DutyRegime3",
                SpecificTaxBase = "232",
                Tax = "Tax",
                Type = "Type"

            }
            );
            //AdditionalInformations
            tkn.GoodsShipment.CustomsGoodsItems[0].AdditionalInformations.Add(new AdditionalInformation()
            {
                Content = new Content() { Text = "ghi chu" },


                Statement = "statement1",
                StatementDescription = "statementDcription1",

            });
            tkn.GoodsShipment.CustomsGoodsItems[0].AdditionalInformations.Add(new AdditionalInformation()
            {
                Content = new Content() { Text = "ghi chu" },


                Statement = "statement1",
                StatementDescription = "statementDcription1",
            });
            tkn.GoodsShipment.CustomsGoodsItems[0].AdditionalInformations.Add(new AdditionalInformation()
            {
                Content = new Content() { Text = "ghi chu" },


                Statement = "statement1",
                StatementDescription = "statementDcription1",
            });


            string msg = Helpers.Serializer(tkn);
            return tkn;
        }
        #endregion
        #region SXXK_tkn
        public static void SXXK_tkn()
        {
            SXXK_TKN SXXK_Tkn = new SXXK_TKN()
            {
                //Loại chứng từ (= 929) 
                Issuer = "929",
                //Số tham chiếu tờ khai
                Reference = "reference",
                //Ngay Khai bao
                Issue = DateTime.Now.ToString().ToString(),
                //Chức năng (Khai báo = "8", sửa =5)
                Function = "8",
                //Nơi khai báo
                IssueLocation = "issueLocation",
                //Trạng thái của chứng từ
                Status = "1",
                //Số tiếp nhận tờ khai; Dùng trong trường hợp khai sửa bản đăng ký
                CustomsReference = "12",
                //Ngày đăng ký chứng từ 
                Acceptance = DateTime.Now.ToString().ToString(),
                //Đơn vị HQ khai báo
                DeclarationOffice = "declarationOffice",
                //Số lượng hàng
                GoodsItem = "1",
                //Số lượng chứng từ, phụ lục đính kèm
                LoadingList = "1",
                //Trọng lượng (kg)
                TotalGrossMass = "23",
                //trong luong tinh(kg)
                TotalNetGrossMass = "2f",
                //Mã loại hình

                NatureOfTransaction = "natureOfTransaction",
                //Mã phương thức thanh toán
                PaymentMethod = "CASH",
                ////////List Dai_ly
                Agents = new List<Agent>(),
                //Nguyên tệ
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "CurrencyType",
                    Rate = "12",

                },
                ////////////Số kiện
                DeclarationPackaging = new Packaging()
                {

                    Quantity = "23",


                },
                //List AdditionalDocument(Hop dong, Van Don, Giay Phep)
                AdditionalDocument = new List<AdditionalDocument>(),
                // Hóa đơn thương mại
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                    Type = "abc"

                },
                ////////Người nhập khẩu
                Importer = new NameBase()
                {
                    Name = "Nhan",
                    Identity = "12"
                },
                ///////////////Người đại diện doanh nghiệp
                RepresentativePerson = new RepresentativePerson()
                {
                    ContactFunction = "chuc vu",
                    Name = "name"
                },
                /////////////////Thông tin tờ khai trị giá
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" },

                    Statement = "Statement",


                },
                //////////////////////Thông tin hàng hóa
                GoodsShipment = new GoodsShipment()
                {
                    //Mã nước xuất khẩu  
                    ExportationCountry = "CU",
                    //Người giao hàng
                    Consignor = new NameBase()
                    {
                        Name = "TenNguoiGH",
                        Identity = "MaNguoiGH"
                    },
                    //Người nhận hàng
                    Consignee = new NameBase()
                    {
                        Identity = "May 10",
                        Name = "MaNguoiNhHang"
                    },
                    //Người nhận hàng trung gian
                    NotifyParty = new NameBase()
                    {
                        Identity = "MaNguoiTB",
                        Name = "TenNguoiThongBao"
                    },
                    // Địa điểm giao hàng
                    DeliveryDestination = new DeliveryDestination()
                    {
                        Line = "DDGHang"
                    },
                    //Cửa khẩu nhập
                    EntryCustomsOffice = new LocationNameBase()
                    {
                        Code = "C02L",
                        Name = "(Ma khong dung nua)Cang Tan cang Tp.HCM"
                    }
                    ,
                    //Cửa khẩu xuất
                    ExitCustomsOffice = new LocationNameBase()
                    {
                        Name = "CangXH_VD",
                        Code = "code"
                    },
                    // Người xuất khẩu
                    Exporter = new NameBase()
                    {
                        Name = "Ten nguoi uy thac Dia chi nguoi uy thac",
                        Identity = ""

                    },
                    //Điều kiện giao hàng
                    TradeTerm = new TradeTerm()
                    {
                        Condition = "CF"
                    },
                    //Hàng khai báo
                    CustomsGoodsItems = new List<CustomsGoodsItem>(),

                },
                ////////////////Danh sách giấy phép NK đi kèm
                Licenses = new List<License>(),
                //Danh sách hợp đồng khai kèm
                ContractDocuments = new List<ContractDocument>(),
                //Danh sách hóa đơn thương mại khai kèm
                CommercialInvoices = new List<CommercialInvoice>(),
                //Danh sách C/O khai kèm 
                CertificateOfOrigins = new List<CertificateOfOrigin>(),
                //Danh sách vận đơn đi kèm
                BillOfLadings = new List<BillOfLading>(),
                //Đơn xin chuyển cửa khẩu
                CustomsOfficeChangedRequest = new CustomsOfficeChangedRequest()
                {
                    AdditionalDocument = new AdditionalDocument()
                    {
                        Reference = "reference",
                        Issue = DateTime.Now.ToString().ToString()
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content() { Text = "ghi chu" },
                        ExaminationPlace = "examinationPlace",
                        Time = DateTime.Now.ToString().ToString(),
                        Route = "route"
                    },
                },
                //Danh sách chứng từ đính kèm, có thể lặp lại
                AttachDocumentItem = new List<AttachDocumentItem>(),
                //
                AdditionalDocumentEx = new List<AdditionalDocument>()


            };
            //AdditionalDocuments
            SXXK_Tkn.AdditionalDocumentEx.Add(new AdditionalDocument()
            {
                Type = "Type",
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "VN",
                Issuer = "issuer",
                Expire = DateTime.Now.ToString(),
                IsDebt = "1",
                Submit = DateTime.Now.ToString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },


            });

            //AttachDocumentItem
            SXXK_Tkn.AttachDocumentItem.Add(new AttachDocumentItem()
            {
                Sequence = "STT",
                Issuer = "issuer",
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "So chug tu",
                Description = "",
                AttachedFiles = new List<AttachedFile>(),

            });
            //AttachedFiles

            SXXK_Tkn.AttachDocumentItem[0].AttachedFiles.Add(new AttachedFile()
            {
                Content = new Content() { Base64 = "bin.base64", Text = "bc" },
                FileName = "001.JPG"
            });
            //BillOfLadings
            SXXK_Tkn.BillOfLadings.Add(new BillOfLading()
            {
                Reference = "reference1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "issueLocation1",
                BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = "identity",
                    Identification = "identification",
                    Journey = "journey",
                    ModeAndType = "modeAndType",
                    Departure = DateTime.Now.ToString().ToString(),
                    RegistrationNationality = "RegistrationNationality"
                },

                MyProCarrierperty = new NameBase() { Identity = "Identity", Name = "Name" },
                Consignment = new Consignment()
                {
                    Consignor = new NameBase { Identity = "identity", Name = "name" },
                    Consignee = new NameBase { Identity = "", Name = "" },
                    NotifyParty = new NameBase { Identity = "", Name = "" },
                    LoadingLocation = new LoadingLocation() { Name = "Name", Code = "Code", Loading = DateTime.Now.ToString().ToString() },
                    UnloadingLocation = new UnloadingLocation() { Name = "Name", Code = "Code", Arrival = DateTime.Now.ToString().ToString() },
                    DeliveryDestination = new DeliveryDestination() { Line = "" },

                    TransportEquipment_List = new List<TransportEquipment>()
                    ,

                }
            });
            //Consignment.TransportEquipments
            SXXK_Tkn.BillOfLadings[0].Consignment.TransportEquipment_List.Add(new TransportEquipment()
            {
                Characteristic = "characteristic",
                Fullness = "fullness",
                Seal = "sealNo",
                EquipmentIdentifications = new EquipmentIdentification() { identification = "identification" }
            });

            //SXXK_Tkn.BillOfLadings[0].Consignment.TransportEquipments.Add(new TransportEquipment()
            //{
            //    Characteristic = "characteris",
            //    Fullness = "fullness",
            //    Seal = "seal",
            //    EquipmentIdentifications = new EquipmentIdentification() { identification = "identification" }
            //});
            //SXXK_Tkn.BillOfLadings[0].Consignment.TransportEquipments.Add(new TransportEquipment()
            //{
            //    Characteristic = "characteris",
            //    Fullness = "fullness",
            //    Seal = "seal",
            //    EquipmentIdentification = new EquipmentIdentification() { identification = "identification" }
            //});
            ////CertificateOfOrigins
            SXXK_Tkn.CertificateOfOrigins.Add(new CertificateOfOrigin()
            {
                Reference = "reference1",
                Type = "Type1",
                Issuer = "Issuer1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "CU",
                Representative = "Representative1",
                ExporterEx = new NameBase() { Identity = "Identity", Name = "Name" },
                ExportationCountryEx = new LocationNameBase() { Name = "Name", Code = "Code" },
                ImporterEx = new NameBase() { Identity = "Identity", Name = "Name" },
                ImportationCountryEx = new LocationNameBase() { Name = "Name", Code = "Code" },
                LoadingLocation = new LoadingLocation() { Name = "Name", Code = "Code", Loading = DateTime.Now.ToString().ToString() },
                UnloadingLocation = new UnloadingLocation() { Name = "Name", Code = "Code" },
                IsDebt = "1",
                Submit = DateTime.Now.ToString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },
                GoodsItems = new List<GoodsItem>()
            });
            //GoodsItems
            SXXK_Tkn.CertificateOfOrigins[0].GoodsItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "21",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUS", },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "2",
                    Type = "Type",
                    MarkNumber = "MarkNumber",

                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                Origin = new Origin() { OriginCountry = "VN" },
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                },

            });
            SXXK_Tkn.CertificateOfOrigins[0].GoodsItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "21",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUS", },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "2",
                    Type = "Type",
                    MarkNumber = "MarkNumber",

                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                Origin = new Origin() { OriginCountry = "VN" },
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                },

            });

            //CommercialInvoices

            SXXK_Tkn.CommercialInvoices.Add(new CommercialInvoice()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Seller = new NameBase() { Identity = "identity", Name = "Name" },
                Buyer = new NameBase() { Identity = "identity", Name = "Name" },
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                Payment = new Payment()
                {
                    Method = "CASH"
                },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD"

                },
                TradeTerm = new TradeTerm() { Condition = "condition" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),


            });
            //CommercialInvoiceItems
            SXXK_Tkn.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "23.2",
                StatisticalValue = "23.1",
                Origin = new Origin() { OriginCountry = "VN" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
            SXXK_Tkn.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "2",
                UnitPrice = "3.2",
                StatisticalValue = "2.1",
                Origin = new Origin() { OriginCountry = "My" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });

            //contractdocments

            SXXK_Tkn.ContractDocuments.Add(new ContractDocument()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString(),
                Expire = DateTime.Now.ToString(),
                Payment = new Payment()
                {
                    Method = "CASH"

                },
                TradeTerm = new TradeTerm()
                {
                    Condition = "condition"

                },
                DeliveryDestination = new DeliveryDestination() { Line = "line" },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD",

                },
                TotalValue = "2332.324",
                Buyer = new NameBase() { Name = "buyer1", Identity = "34" },
                Seller = new NameBase()
                {
                    Name = "seller1",
                    Identity = "s01"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },
                ContractItems = new List<ContractItem>(),

            });
            //ContractItems
            SXXK_Tkn.ContractDocuments[0].ContractItems.Add(new ContractItem()
            {
                unitPrice = "32",
                statisticalValue = "34.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                }
            });
            SXXK_Tkn.ContractDocuments[0].ContractItems.Add(new ContractItem()
            {
                unitPrice = "2",
                statisticalValue = "4.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                }
            });

            //licenses
            SXXK_Tkn.Licenses.Add(new License()
            {
                Issuer = "issuer",
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "CU",
                Type = "Type",
                Expire = DateTime.Now.ToString().ToString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },
                GoodItems = new List<GoodsItem>()
            });
            //SXXK.licenses.license.GoodItems
            SXXK_Tkn.Licenses[0].GoodItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "statisticalValue",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "VND" },
                Commodity = new Commodity()
                {
                    Description = "ten hang",
                    Identification = "iden",
                    TariffClassification = "MaHS"

                },
                GoodsMeasure = new GoodsMeasure() { Quantity = "32", MeasureUnit = "MU" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "" } }
            });


            ///////////////////////Agent
            SXXK_Tkn.Agents.Add(new Agent()
            {
                Identity = "0100101308",
                Name = "May 10",
                Status = "1"
            });
            SXXK_Tkn.Agents.Add(new Agent()
            {
                Name = "Ðon v? cú mó:MaNguoiUyThac kh?ng cú trong danh m?c",
                Identity = "MaNguoiUyThac",
                Status = "2"
            });
            SXXK_Tkn.Agents.Add(new Agent()
            {
                Name = "May 10 05113791111 05113791112",
                Identity = "0100101308",
                Status = "4"
            });
            /////////////////////AdditionalDocument-Hop Dong
            SXXK_Tkn.AdditionalDocument.Add(new AdditionalDocument()

            {
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "SOHDDTKN",
                Type = "32",
                Name = "HopDong",
                Expire = DateTime.Now.ToString().ToString()
            });
            /////////////////////AdditionalDocument-So Van Don
            SXXK_Tkn.AdditionalDocument.Add(new AdditionalDocument()

            {
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "SOVANDON",
                Type = "32",
                Name = "Van Don",

            });
            /////////////////////AdditionalDocument-Giay phep
            SXXK_Tkn.AdditionalDocument.Add(new AdditionalDocument()

            {
                Issue = DateTime.Now.ToString().ToString(),
                Reference = "SOGPTKN",
                Type = "32",
                Name = "Giay phep",
                Expire = DateTime.Now.ToString().ToString()
            });

            //Thông tin tờ khai trị giá
            SXXK_Tkn.GoodsShipment.CustomsGoodsItems.Add(new CustomsGoodsItem()
            {
                CustomsValue = "12.32",
                Sequence = "1",
                StatisticalValue = "324.21",
                UnitPrice = "3.12",
                StatisticalUnitPrice = "32.23",
                Manufacturer = new NameBase()
                {
                    Identity = "s",
                    Name = "fd"
                },
                Origin = new Origin()
                {
                    OriginCountry = "VN"
                },
                Commodity = new Commodity()
                {
                    Type = "1",
                    Description = "dcription",
                    Identification = "Identification",
                    TariffClassification = "3434343",
                    TariffClassificationExtension = "dsf",
                    Brand = "nhanhieu",
                    Grade = "QuyCach",
                    Ingredients = "ingredients",
                    ModelNumber = "number1",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine()
                    {
                        ItemCharge = "",
                        Line = ""

                    }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.2",
                    MeasureUnit = "measureUnit"
                },
                CertificateOfOrigin = new CertificateOfOrigin()
                {
                    Issue = DateTime.Now.ToString(),
                    Issuer = "To chuc cap",
                    IssueLocation = "CU",
                    Reference = "SOCO",
                    Type = "007",
                    Name = "CO",
                    Expire = "",
                    Exporter = "exporter",
                    ExportationCountry = "exportationCountry",
                    Importer = "importer",
                    ImportationCountry = "VN",
                    Content = "content",
                    IsDebt = "3",
                    Submit = "42"

                },
                AdditionalDocument = new AdditionalDocument()
                {
                    Issue = DateTime.Now.ToString(),
                    Issuer = "issuer",
                    IssueLocation = "issuelocation",
                    Reference = "ref",
                    Type = "Type",
                    Name = "name",
                    Expire = DateTime.Now.ToString()
                },
                // neu ko dc thi sua index thanh` "1" nhe',ok
                CustomsValuation = new CustomsValuation()
                {
                    ExitToEntryCharge = "23.2",
                    FreightCharge = "322.324",
                    Method = "method",
                    OtherChargeDeduction = "32.32"
                },


            });

            //DutyTaxFee
            SXXK_Tkn.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "23.2",

                Tax = "Tax",
                Type = "Type"

            }
            );

            SXXK_Tkn.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "2.2",

                Tax = "Tax",
                Type = "Type"

            }
            );
            SXXK_Tkn.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee()
            {
                AdValoremTaxBase = "3.2",

                Tax = "Tax",
                Type = "Type"

            }
            );



            string msg = Helpers.Serializer(SXXK_Tkn);
            //return SXXK_Tkn;

        }

        #endregion SXXK_tkn
        #region Tờ Khai Xuất kinh doanh
        public static KD_ToKhaiXuat KD_ToKhaiXuat()
        {
            KD_ToKhaiXuat tkx = new KD_ToKhaiXuat()
            {
                Issuer = "930",
                Reference = "874F543147FA4DB79066A5B854E6B9A4",
                Issue = DateTime.Today.ToString(),
                Function = "8",
                IssueLocation = "",
                Status = "1",
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = "C43C",
                GoodsItem = "2",
                LoadingList = "0",
                TotalGrossMass = "11",
                TotalNetGrossMass = "34f",
                NatureOfTransaction = "XKD01",
                PaymentMethod = "CASH",
                Agents = new List<Agent>(),
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD", Rate = "1" },
                DeclarationPackaging = new Packaging { Quantity = "1111" },
                AdditionalDocument = new List<AdditionalDocument>(),
                Invoice = new Invoice { Issue = DateTime.Today.ToString(), Reference = "SoHDTM", Type = "380" },
                Exporter = new NameBase { Name = "May 10 05113791111 05113791112", Identity = "0100101308" },
                RepresentativePerson = new RepresentativePerson { ContactFunction = ".", Name = "Nguoi Gui" },
                AdditionalInformation = new AdditionalInformation { Statement = "001", Content = new Content() { Text = "ghi chu" } },
                //---------------------------------------------------------------
                GoodsShipment = new GoodsShipment
                {
                    ImportationCountry = "CU",
                    Consignor = new NameBase { Identity = "", Name = "" },
                    Consignee = new NameBase { Identity = "", Name = "" },
                    NotifyParty = new NameBase { Identity = "", Name = "" },
                    DeliveryDestination = new DeliveryDestination { Line = "" },
                    EntryCustomsOffice = new LocationNameBase { Name = "", Code = "" },
                    ExitCustomsOffice = new LocationNameBase { Name = "(Ma khong dung nua)Cang Tan cang Tp.HCM", Code = "C02L" },
                    Importer = new NameBase { Name = "TenNguoiNhap DCNguoiNhap", Identity = "" },
                    TradeTerm = new TradeTerm { Condition = "CF" },
                    CustomsGoodsItems = new List<CustomsGoodsItem>(),
                },
                //-------------------------------------------------------
                License = new List<License>(),
                ContractDocument = new List<ContractDocument>(),
                CommercialInvoices = new List<CommercialInvoice>(),
                CertificateOfOrigin = new List<CertificateOfOrigin>(),
                BillOfLadings = "",
                //---------------------------------------------------------
                CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),
                AttachDocumentItem = new List<AttachDocumentItem>(),
                AdditionalDocumentEx = new List<AdditionalDocument>(),

            };
            // --- add Agents -----
            tkx.Agents.Add(new Agent
            {
                Identity = "May 10 05113791111 05113791112",
                Name = "0100101308",
                Status = "3",
            });
            tkx.Agents.Add(new Agent
            {
                Identity = "TENDNYTHAC",
                Name = "MADNYTHAC",
                Status = "2",
            });
            tkx.Agents.Add(new Agent
            {
                Identity = "May 10 05113791111 05113791112",
                Name = "0100101308",
                Status = "4",
            });
            //----- Add AdditionalDocument -------
            tkx.AdditionalDocument.Add(new AdditionalDocument
            {
                Issue = DateTime.Today.ToString(),
                Reference = "SOHDDTKX",
                Type = "315",
                Name = "Hop Dong",
                Expire = DateTime.Today.ToString(),

            });
            tkx.AdditionalDocument.Add(new AdditionalDocument
            {
                Issue = DateTime.Today.ToString(),
                Reference = "SOGPTKX",
                Type = "811",
                Name = "Giay phep",
                Expire = DateTime.Today.ToString(),
            });
            //Add CustomsGoodsItems
            tkx.GoodsShipment.CustomsGoodsItems.Add(new CustomsGoodsItem
            {
                CustomsValue = "144",
                Sequence = "1",
                StatisticalValue = "144",
                UnitPrice = "12",
                StatisticalUnitPrice = "12",
                Manufacturer = new NameBase { Identity = "TenHangSX", Name = "MaHangSX" },
                Origin = new Origin { OriginCountry = "VN" },
                Commodity = new Commodity
                {
                    Description = "Ten hang",
                    Identification = "",
                    TariffClassification = "12345678",
                    TariffClassificationExtension = "MaHSMoRong",
                    Brand = "Nhan hieu",
                    Grade = "Quy Cach Pham",
                    Ingredients = "Thanh Phan",
                    ModelNumber = "Model",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine { ItemCharge = "", Line = "" },
                },
                GoodsMeasure = new GoodsMeasure { Quantity = "12", MeasureUnit = "93" },
                CertificateOfOrigin = new CertificateOfOrigin
                {
                    Issue = DateTime.Today.ToString(),
                    Issuer = "Nguoi Cap",
                    IssueLocation = "CU",
                    Reference = "SOCO",
                    Type = "007",
                    Name = "CO",
                    Expire = DateTime.Today.ToString().ToString(),
                    Exporter = "Ten, dia chi nguoi xuat khau tren CO",
                    ExportationCountry = "VN",
                    Importer = "Ten, dia chi nguoi nhap",
                    ImportationCountry = "AF",
                    Content = "Thong tin khac",
                    IsDebt = "1",
                    Submit = DateTime.Today.ToString()

                },
                AdditionalDocument = new AdditionalDocument
                {
                    Issue = DateTime.Today.ToString(),
                    Issuer = "nguoi cap",
                    IssueLocation = "noi cap",
                    Reference = "SOGPTKX",
                    Type = "811",
                    Name = "GP",
                    Expire = DateTime.Today.ToString()
                },
                CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = "0",
                    FreightCharge = "0",
                    Method = "",
                    OtherChargeDeduction = "0",
                }
            });
            // Add DutyTaxFee
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "19",
                DutyRegime = "",
                SpecificTaxBase = "",
                Tax = "13",
                Type = "1"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "8",
                DutyRegime = "",
                SpecificTaxBase = "",
                Tax = "5",
                Type = "2"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "2",
                DutyRegime = "",
                SpecificTaxBase = "",
                Tax = "1",
                Type = "3"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "0",
                DutyRegime = "",
                SpecificTaxBase = "",
                Tax = "0",
                Type = "4"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "0",
                DutyRegime = "",
                SpecificTaxBase = "",
                Tax = "0",
                Type = "5"
            });
            //------- Add License ---------------
            tkx.License.Add(new License
            {
                Issuer = "Nguoi Cap",
                Issue = DateTime.Today.ToString(),
                Reference = "SOGPKTX",
                IssueLocation = "noi cap",
                Type = "",
                Expire = DateTime.Today.ToString(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                GoodItems = new List<GoodsItem>()

            });
            tkx.License[0].GoodItems.Add(new GoodsItem
            {
                Sequence = "1",
                StatisticalValue = "1",
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD", Rate = "1" },
                Commodity = new Commodity { Description = "Ten hang", Identification = "", TariffClassification = "MaHS" },
                GoodsMeasure = new GoodsMeasure { Quantity = "12", MeasureUnit = "93" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });
            //------ Add ContractDocument --------
            tkx.ContractDocument.Add(new ContractDocument
            {
                Reference = "SOHDDTKX",
                Issue = DateTime.Today.ToString(),
                Expire = DateTime.Today.ToString(),
                Payment = new Payment { Method = "CASH" },
                TradeTerm = new TradeTerm { Condition = "CF" },
                DeliveryDestination = new DeliveryDestination { Line = "Dia Diem giao hang" },
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD", Rate = "1" },
                TotalValue = "1111.11",
                Buyer = new NameBase { Identity = "Ma Don Vi mua", Name = "Ten Don Vi mua" },
                Seller = new NameBase { Identity = "Ma Don Vi mua", Name = "Ten Don Vi mua" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                ContractItems = new List<ContractItem>()
            });
            tkx.ContractDocument[0].ContractItems.Add(new ContractItem
            {
                unitPrice = "12",
                statisticalValue = "144",
                Commodity = new Commodity { Description = "Ten hang", Identification = "Ma hang", TariffClassification = "Ma HS" },
                Origin = new Origin { OriginCountry = "VN" },
                GoodsMeasure = new GoodsMeasure { Quantity = "12", MeasureUnit = "93" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });
            //-------- Add CommericalInvoices ----------
            tkx.CommercialInvoices.Add(new CommercialInvoice
            {
                Reference = "SoHDTM",
                Issue = DateTime.Today.ToString(),
                Seller = new NameBase { Name = "TenNguoiNhap DCNguoiNhap", Identity = "dvm" },
                Buyer = new NameBase { Name = "May 10 05113791111 05113791112", Identity = "0100101308" },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = DateTime.Today.ToString() },
                Payment = new Payment { Method = "CASH" },
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD", Rate = "1" },
                TradeTerm = new TradeTerm { Condition = "CF" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()


            });
            tkx.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem
            {
                Sequence = "1",
                UnitPrice = "12",
                StatisticalValue = "144",
                Origin = new Origin { OriginCountry = "VN" },
                Commodity = new Commodity { Description = "Ten Hang", Identification = "", TariffClassification = "Ma HS" },
                GoodsMeasure = new GoodsMeasure { Quantity = "12", MeasureUnit = "93" },
                ValuationAdjustment = new ValuationAdjustment { Addition = "1603", Deduction = "3008" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });
            //------- add CertificateOfOrigin -----------
            CertificateOfOrigin certificateOfOrigin = new CertificateOfOrigin
               {
                   Reference = "SOCO",
                   Type = "7",
                   Issue = DateTime.Today.ToString(),
                   Issuer = "To Chuc Cap",
                   IssueLocation = "Noi cap",
                   Representative = "Nguoi Dang ky",
                   ExporterEx = new NameBase { Identity = "0100101308", Name = "Ten, dia chi nguoi xuat khau tren C0" },
                   ExportationCountryEx = new LocationNameBase { Name = "Viet Nam", Code = "VN" },
                   ImporterEx = new NameBase { Name = "TenNguoiNhap DCNguoiNhap", Identity = "0100101308" },
                   ImportationCountryEx = new LocationNameBase { Name = "Afganistan", Code = "AF" },
                   LoadingLocation = new LoadingLocation { Name = "", Code = "", Loading = "2012-04-04" },
                   UnloadingLocation = new UnloadingLocation { Name = "Cang Tan cang Tp.HCM", Code = "C02L" },
                   IsDebt = "1",
                   Submit = DateTime.Today.ToString(),
                   AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                   GoodsItems = new List<GoodsItem>()
               };
            GoodsItem goodsItem = new GoodsItem
                {
                    Sequence = "1",
                    StatisticalValue = "144",
                    CurrencyExchange = new CurrencyExchange { CurrencyType = "USD", Rate = "1" },
                    ConsignmentItemPackaging = new Packaging { Quantity = "12345", Type = "Kieu Dong goi", MarkNumber = "So Hieu Kien" },
                    Commodity = new Commodity { Description = "Ten Hang", Identification = "Ma hang xuat", TariffClassification = "MaHS" },
                    GoodsMeasure = new GoodsMeasure { GrossMass = "123", MeasureUnit = "93" },
                    Origin = new Origin { OriginCountry = "VN" },
                    Invoice = new Invoice { Reference = "SoHoaD", Issue = DateTime.Today.ToString() },
                };
            certificateOfOrigin.GoodsItems.Add(goodsItem);
            tkx.CertificateOfOrigin.Add(certificateOfOrigin);
            tkx.AttachDocumentItem.Add(new AttachDocumentItem
            {
                Sequence = "1",
                Issuer = "200",
                Issue = DateTime.Today.ToString(),
                Reference = "So Chung Tu",
                Description = "Thong Tin Khac",
                AttachedFiles = new List<AttachedFile>()


            });
            tkx.CustomsOfficeChangedRequest.Add(new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = "So Van DOn", Issue = DateTime.Today.ToString() },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content() { Text = "ghi chu" },
                    ExaminationPlace = "Dia Diem Kiem tra",
                    Time = DateTime.Today.ToString(),
                    Route = "Tuyen Duong"
                }
            });
            tkx.AttachDocumentItem[0].AttachedFiles.Add(new AttachedFile
            {
                FileName = "img.jpg",
                Content = new Content { Text = "Tm9pIGR1bmcgdGVwIHRpbiBhbmg=" }
            });
            //------- Add additionalDocumentEx -----------
            tkx.AdditionalDocumentEx.Add(new AdditionalDocument
            {
                Type = "202",
                Reference = "So Chung Tu No",
                Issue = DateTime.Today.ToString(),
                IssueLocation = "Noi Cap",
                Issuer = "To Chuc Cap",
                Expire = DateTime.Today.ToString(),
                IsDebt = "1",
                Submit = DateTime.Today.ToString(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }

            });

            string msg = Helpers.Serializer(tkx);
            return tkx;
        }
        #endregion
        #region Khai Báo Danh mục sản phẩm sản xuất xuất khẩu
        public static void SXXK_DMSanPham()
        {
            SXXK_SanPham SXXK_SP = new SXXK_SanPham()
            {
                Issuer = "101",
                Function = "8",
                Reference = "581920D736E74D2DA8542BBDCEB70058",
                Issue = DateTime.Now.ToString(),
                IssueLocation = "",
                DeclarationOffice = "C34C",
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToString(),
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "May 10", Identity = "0100101308" },
                Product = new List<Product>(),
            };
            SXXK_SP.Agents.Add(new Agent
            {
                Name = "may 10",
                Identity = "0100101308",
                Status = "3"
            });
            SXXK_SP.Product.Add(new Product
            {
                Commodity = new Commodity { Identification = "MaSP1", Description = "TenSP1", TariffClassification = "MaHSSp1" },
                GoodsMeasure = new GoodsMeasure { MeasureUnit = "2" }
            });
            SXXK_SP.Product.Add(new Product
            {
                Commodity = new Commodity { Identification = "MaSP2", Description = "TenSP2", TariffClassification = "MaHSSp2" },
                GoodsMeasure = new GoodsMeasure { MeasureUnit = "1" }
            });
            string msg = Helpers.Serializer(SXXK_SP);
        }
        #endregion
        //public static void SXXK_DMNguyenphuLieu()
        //{
        //    SXXK_SanPham SXXK_SP = new SXXK_SanPham()
        //    {
        //        Issuer = 100,
        //        Function = "8",
        //        Reference = "581920D736E74D2DA8542BBDCEB70058",
        //        Issue = DateTime.Now.ToString().ToString(),
        //        IssueLocation = "",
        //        DeclarationOffice = "C34C",
        //        Status = "1",
        //        CustomsReference = "",
        //        Acceptance = DateTime.Now.ToString().ToString(),
        //        Agents = new List<Agent>(),
        //        Importer = new NameBase { Name = "May 10", Identity = "0100101308" },
        //        Product = new List<Product>(),
        //    };
        //    SXXK_SP.Agents.Add(new Agent
        //    {
        //        Name = "may 10",
        //        Identity = "0100101308",
        //        Status = 3
        //    });
        //    SXXK_SP.Product.Add(new Product
        //    {
        //        Commodity = new Commodity { Identification = "MaSP1", Description = "TenSP1", TariffClassification = "MaHSSp1" },
        //        GoodsMeasure = new GoodsMeasure { MeasureUnit = "2" }
        //    });
        //    SXXK_SP.Product.Add(new Product
        //    {
        //        Commodity = new Commodity { Identification = "MaSP2", Description = "TenSP2", TariffClassification = "MaHSSp2" },
        //        GoodsMeasure = new GoodsMeasure { MeasureUnit = "1" }
        //    });
        //    string msg = Helpers.Serializer(SXXK_SP);
        //}
        ////public static void SXXK_DMNguyenphuLieu()
        ////{
        ////    SXXK_NguyenPhuLieu SXXK_npl = new SXXK_NguyenPhuLieu()
        ////    {
        ////        Issuer = 100,
        ////        Function = "8",
        ////        Reference = "581920D736E74D2DA8542BBDCEB70058",
        ////        Issue = DateTime.Now.ToString(),
        ////        IssueLocation = "",
        ////        DeclarationOffice = "C34C",
        ////        Status = "1",
        ////        CustomsReference = "",
        ////        Acceptance = DateTime.Now.ToString(),
        ////        Agents = new List<Agent>(),
        ////        Importer = new NameBase { Name = "May 10", Identity = "0100101308" },
        ////        Product = new List<Product>(),
        ////    };
        ////    SXXK_npl.Agents.Add(new Agent
        ////    {
        ////        Name = "may 10",
        ////        Identity = "0100101308",
        ////        Status = 3
        ////    });
        ////    SXXK_npl.Product.Add(new Product
        ////    {
        ////        Commodity = new CommodityBase { Identification = "MaSP1", Description = "TenSP1", TariffClassification = "MaHSSp1" },
        ////        GoodsMeasure = new GoodsMeasure { MeasureUnit = "2" }
        ////    });
        ////    SXXK_npl.Product.Add(new Product
        ////    {
        ////        Commodity = new CommodityBase { Identification = "MaSP2", Description = "TenSP2", TariffClassification = "MaHSSp2" },
        ////        GoodsMeasure = new GoodsMeasure { MeasureUnit = "1" }
        ////    });
        ////    string msg = Helpers.Serializer(SXXK_npl);
        ////}
        //#endregion
        #region Khai báo định mức thực tế của sản phẩm
        public static void SXXK_DMSP()
        {

            SXXK_DinhMucSP SXXK_DM = new SXXK_DinhMucSP
            {
                Issuer = "102",
                Function = "8",
                Reference = "291D09DBA28D41FEAA0A91A1B0FA16D3",
                Issue = DateTime.Today.ToString(),
                IssueLocation = "",
                DeclarationOffice = "C34C",
                Status = "1",
                CustomsReference = "",
                Acceptance = "",
                Agents = new List<Agent>(),
                Importer = new NameBase { Identity = "0100101308", Name = "May 10" },
                ProductionNorm = new List<ProductionNorm>()
            };

            SXXK_DM.Agents.Add(new Agent
            {
                Name = "may 10",
                Identity = "0100101308",
                Status = "3",
            });
            SXXK_DM.ProductionNorm.Add(new ProductionNorm
            {
                Product = new Product
                {
                    Commodity = new Commodity { Identification = "MaSP2", Description = "TenSP2", TariffClassification = "123456789" },
                    GoodsMeasure = new GoodsMeasure { MeasureUnit = "2" }
                },
                MaterialsNorm = new List<MaterialsNorm>(),
            });
            MaterialsNorm _materialsNorm = new MaterialsNorm
            {
                Material = new Product
                {
                    Commodity = new Commodity { Identification = "Ma NPL1", Description = "Ten NPL1", TariffClassification = "1234567890" },
                    GoodsMeasure = new GoodsMeasure { MeasureUnit = "12" }
                },
                Norm = "3.883495",
                Loss = "3"
            };
            SXXK_DM.ProductionNorm[0].MaterialsNorm.Add(_materialsNorm);
            SXXK_DM.ProductionNorm[0].MaterialsNorm.Add(_materialsNorm);
            SXXK_DM.ProductionNorm.Add(new ProductionNorm
            {
                Product = new Product
                {
                    Commodity = new Commodity { Identification = "MaSp3", Description = "TenSp3", TariffClassification = "1234567890" },
                    GoodsMeasure = new GoodsMeasure { MeasureUnit = "2" }
                },
                MaterialsNorm = new List<MaterialsNorm>()
            });
            SXXK_DM.ProductionNorm[1].MaterialsNorm.Add(_materialsNorm);
            SXXK_DM.ProductionNorm[1].MaterialsNorm.Add(_materialsNorm);
            string msg = Helpers.Serializer(SXXK_DM);

        }

        #endregion
        #region Tờ Khai Xuất sản xuất xuất khẩu
        public static void SXXK_ToKhaiXuat()
        {
            SXXK_ToKhaiXuat tkx = new SXXK_ToKhaiXuat
            {
                Issuer = "932",
                Reference = "D4913AD645DD41D88CDB92436FB34345",
                Issue = DateTime.Today.ToString(),
                Function = "8",
                IssueLocation = "",
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Today.ToString(),
                DeclarationOffice = "C43C",
                GoodsItem = "1",
                LoadingList = "0",
                TotalGrossMass = "25",
                TotalNetGrossMass = "10f",
                NatureOfTransaction = "XSX01",
                PaymentMethod = "CASH",
                Agents = new List<Agent>(),
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD", Rate = "22" },
                DeclarationPackaging = new Packaging { Quantity = "1231 " },
                AdditionalDocument = new List<AdditionalDocument>(),
                Invoice = new Invoice { Issue = DateTime.Today.ToString(), Reference = "SoHDTM", Type = "380" },
                Exporter = new NameBase { Name = "ay 10 DC Danh nghiep khai bao", Identity = "0100101308" },
                RepresentativePerson = new RepresentativePerson { ContactFunction = ".", Name = "Nguoi Gui" },
                AdditionalInformation = new AdditionalInformation { Statement = "001", Content = new Content() { Text = "ghi chu" } },
                //---------------------------------------------------------------
                GoodsShipment = new GoodsShipment
                {
                    ImportationCountry = "CU",
                    Consignee = new NameBase { Identity = "MaNguoiNhan", Name = "TenNguoiNhan" },
                    //--- Consignment
                    Consignment = new Consignment
                    {
                        Container = "0",
                        ArrivalTransportMeans = new ArrivalTransportMeans
                        {
                            Identification = "Ten PTVT",
                            Identity = "So hieu PTVT",
                            ModeAndType = 099,
                            RegistrationNationality = "AF"
                        },
                        Carrier = new NameBase { Identity = "MaHangVT", Name = "TenHangVT" },
                        LoadingLocation = new LoadingLocation { Code = "C019", Name = "C019" },
                        TransportEquipment_List = new List<TransportEquipment>(),

                        UnloadingLocation = new UnloadingLocation { Code = "CO19", Name = "Cang do hang", Arrival = DateTime.Today.ToString() }
                    },
                    //------------
                    Consignor = new NameBase { Identity = "Ten Nguoi Giao", Name = "MaNguoiGiao" },
                    CustomsGoodsItems = new List<CustomsGoodsItem>(),
                    NotifyParty = new NameBase { Identity = "Ten nguoi duoc Thong Bao", Name = "MaNgDcTB" },
                    DeliveryDestination = new DeliveryDestination { Line = "DiaDiemGiaoHang" },
                    ExitCustomsOffice = new LocationNameBase { Code = "C019" },
                    Importer = new NameBase { Name = "TenNguoiNhap DCNguoiNhap", Identity = "" },
                    TradeTerm = new TradeTerm { Condition = "CF" },

                },
                //-------------------------------------------------------
                License = new List<License>(),
                ContractDocument = new List<ContractDocument>(),
                CommercialInvoices = new List<CommercialInvoice>(),
                CertificateOfOrigin = new List<CertificateOfOrigin>(),
                BillOfLading = new List<BillOfLading>(),
                //---------------------------------------------------------
                CustomsOfficeChangedRequest = new List<CustomsOfficeChangedRequest>(),

                AttachDocumentItem = new List<AttachDocumentItem>(),
                AdditionalDocumentEx = new List<AdditionalDocument>(),

            };
            tkx.CustomsOfficeChangedRequest.Add(new CustomsOfficeChangedRequest
            {
                AdditionalDocument = new AdditionalDocument { Reference = "So Van DOn", Issue = DateTime.Today.ToString() },
                AdditionalInformation = new AdditionalInformation
                {
                    Content = new Content() { Text = "ghi chu" },
                    ExaminationPlace = "Dia Diem Kiem tra",
                    Time = DateTime.Today.ToString(),
                    Route = "Tuyen Duong"
                }
            });
            tkx.BillOfLading[0].Consignment.TransportEquipment_List.Add(new TransportEquipment()
            {
                Characteristic = "4",
                Fullness = "1",
                Seal = "SealNo",
                EquipmentIdentifications = new EquipmentIdentification { identification = "So hieu container" }
            });
            // --- add Agents -----
            tkx.Agents.Add(new Agent
            {
                Identity = "May 10 DC Danh nghiep khai bao",
                Name = "0100101308",
                Status = "3",
            });
            tkx.Agents.Add(new Agent
            {
                Identity = "TENDNYTHAC",
                Name = "MADNYTHAC",
                Status = "2",
            });
            tkx.Agents.Add(new Agent
            {
                Identity = "May 10 DC Danh nghiep khai bao",
                Name = "0100101308",
                Status = "4",
            });
            tkx.Agents.Add(new Agent
            {
                Identity = "MAMID",
                Name = "MAMID",
                Status = "5"
            });
            //----- Add AdditionalDocument -------
            tkx.AdditionalDocument.Add(new AdditionalDocument
            {
                Issue = DateTime.Today.ToString(),
                Reference = "SOHD",
                Type = "315",
                Name = "Hop Dong",
                Expire = DateTime.Today.ToString(),

            });
            tkx.AdditionalDocument.Add(new AdditionalDocument
            {
                Issue = DateTime.Today.ToString(),
                Reference = "SOGP",
                Type = "811",
                Name = "Giay phep",
                Expire = DateTime.Today.ToString(),
            });
            //Add CustomsGoodsItems-------------------------------------
            tkx.GoodsShipment.CustomsGoodsItems.Add(new CustomsGoodsItem
            {
                CustomsValue = "16348",
                Sequence = " 1",
                StatisticalValue = "359656",
                UnitPrice = "122",
                StatisticalUnitPrice = "2684",
                Manufacturer = new NameBase { Identity = "TenHangSX", Name = "MaHangSX" },
                Origin = new Origin { OriginCountry = "VN" },
                Commodity = new Commodity
                {
                    Type = "2",
                    Description = "Ten SP",
                    Identification = "Ma SP",
                    TariffClassification = "12345678",
                    TariffClassificationExtension = "MaHSMoRong",
                    Brand = "Nhan hieu",
                    Grade = "Quy Cach Pham",
                    Ingredients = "Thanh Phan",
                    ModelNumber = "Model",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine { ItemCharge = "", Line = "" },
                },
                GoodsMeasure = new GoodsMeasure { Quantity = "134", MeasureUnit = "2" },
                CustomsValuation = new CustomsValuation
                {
                    ExitToEntryCharge = "0",
                    FreightCharge = "0",
                    Method = "",
                    OtherChargeDeduction = "0",
                },
                SpecializedManagement = new SpecializedManagement
                {
                    Type = "1",
                    Identification = "Ma HTS",
                    Quantity = "5",
                    MeasureUnit = "0",
                    UnitPrice = "6",
                }
            });
            // Add DutyTaxFee
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "19",
                DutyRegime = "",
                Tax = "13",
                Type = "1"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "8",
                DutyRegime = "",
                Tax = "5",
                Type = "2"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "2",
                DutyRegime = "",
                Tax = "1",
                Type = "3"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "0",
                DutyRegime = "",
                Tax = "0",
                Type = "4"
            });
            tkx.GoodsShipment.CustomsGoodsItems[0].Commodity.DutyTaxFee.Add(new DutyTaxFee
            {
                AdValoremTaxBase = "0",
                DutyRegime = "",
                Tax = "0",
                Type = "5"
            });
            //------- Add License ---------------
            tkx.License.Add(new License
            {
                Issuer = "Nguoi Cap",
                Issue = DateTime.Today.ToString(),
                Reference = "SOGP",
                IssueLocation = "noi cap",
                Type = "",
                Expire = DateTime.Today.ToString(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                GoodItems = new List<GoodsItem>()

            });
            tkx.License[0].GoodItems.Add(new GoodsItem
            {
                Sequence = "1",
                StatisticalValue = "1",
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD" },
                Commodity = new Commodity { Description = "Ten hang XK", Identification = "MaHangXK", TariffClassification = "MaHS" },
                GoodsMeasure = new GoodsMeasure { Quantity = "134", MeasureUnit = "93" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });
            //------ Add ContractDocument --------
            tkx.ContractDocument.Add(new ContractDocument
            {
                Reference = "SOHD",
                Issue = DateTime.Today.ToString(),
                Expire = DateTime.Today.ToString(),
                Payment = new Payment { Method = "CASH" },
                TradeTerm = new TradeTerm { Condition = "CF" },
                DeliveryDestination = new DeliveryDestination { Line = "Dia Diem giao hang" },
                CurrencyExchange = new CurrencyExchange { CurrencyType = "VND" },
                TotalValue = "11111",
                Buyer = new NameBase { Identity = "Ma Don Vi mua", Name = "Ten Don Vi mua" },
                Seller = new NameBase { Identity = "Ma Don Vi mua", Name = "Ten Don Vi mua" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                ContractItems = new List<ContractItem>()
            });
            tkx.ContractDocument[0].ContractItems.Add(new ContractItem
            {
                unitPrice = "122",
                statisticalValue = "16348",
                Commodity = new Commodity { Description = "Ten hang", Identification = "Ma hang", TariffClassification = "Ma HS" },
                Origin = new Origin { OriginCountry = "VN" },
                GoodsMeasure = new GoodsMeasure { Quantity = "12", MeasureUnit = "93" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });
            //-------- Add CommericalInvoices ----------
            tkx.CommercialInvoices.Add(new CommercialInvoice
            {
                Reference = "SoHDTM",
                Issue = DateTime.Today.ToString(),
                Seller = new NameBase { Name = "TenNguoiNhap DCNguoiNhap", Identity = "Ma DV mua" },
                Buyer = new NameBase { Name = "May 10 05113791111 05113791112", Identity = "0100101308" },
                AdditionalDocument = new AdditionalDocument { Reference = "", Issue = DateTime.Today.ToString() },
                Payment = new Payment { Method = "CASH" },
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD" },
                TradeTerm = new TradeTerm { Condition = "CF" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()


            });
            tkx.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem
            {
                Sequence = "1",
                UnitPrice = "122",
                StatisticalValue = "14348",
                Origin = new Origin { OriginCountry = "VN" },
                Commodity = new Commodity { Description = "Ten Hang", Identification = "", TariffClassification = "Ma HS" },
                GoodsMeasure = new GoodsMeasure { Quantity = "12", MeasureUnit = "93" },
                ValuationAdjustment = new ValuationAdjustment { Addition = "1", Deduction = "2" },
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });
            //------- add CertificateOfOrigin -----------
            CertificateOfOrigin certificateOfOrigin = new CertificateOfOrigin
            {
                Reference = "SOCO",
                Type = "007",
                Issue = DateTime.Today.ToString(),
                Issuer = "To Chuc Cap",
                IssueLocation = "Noi cap",
                Representative = "Nguoi Dang ky",
                ExporterEx = new NameBase { Identity = "0100101308", Name = "Ten, dia chi nguoi xuat khau tren C0" },
                ExportationCountryEx = new LocationNameBase { Name = "Viet Nam", Code = "VN" },
                ImporterEx = new NameBase { Name = "TenNguoiNhap DCNguoiNhap", Identity = "0100101308" },
                ImportationCountryEx = new LocationNameBase { Name = "Afganistan", Code = "AF" },
                LoadingLocation = new LoadingLocation { Name = "cang Xep Hang", Code = "", Loading = "2012-04-04" },
                UnloadingLocation = new UnloadingLocation { Name = "Cang Tong Hop Binh Duong", Code = "C047" },
                IsDebt = "1",
                Submit = DateTime.Today.ToString(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                GoodsItems = new List<GoodsItem>()
            };
            GoodsItem goodsItem = new GoodsItem
            {
                Sequence = "1",
                StatisticalValue = "16348",
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD" },
                ConsignmentItemPackaging = new Packaging { Quantity = "1", Type = "Kieu Dong goi", MarkNumber = "So Hieu Kien" },
                Commodity = new Commodity { Description = "Ten Hang", Identification = "Ma hang xuat", TariffClassification = "MaHS" },
                GoodsMeasure = new GoodsMeasure { GrossMass = "123", MeasureUnit = "93" },
                Origin = new Origin { OriginCountry = "VN" },
                Invoice = new Invoice { Reference = "SoHoaD", Issue = DateTime.Today.ToString() },
            };
            certificateOfOrigin.GoodsItems.Add(goodsItem);
            tkx.CertificateOfOrigin.Add(certificateOfOrigin);
            //--------------------------
            //---Bill Of Ladings
            tkx.BillOfLading.Add(new BillOfLading
            {
                Reference = "reference1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "issueLocation1",
                BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = "identity",
                    Identification = "identification",
                    Journey = "SoHieuChuyendi",
                    ModeAndType = "006",
                    Departure = DateTime.Now.ToString().ToString(),
                    RegistrationNationality = "RegistrationNationality"
                },

                MyProCarrierperty = new NameBase() { Identity = "Identity", Name = "Name" },
                Consignment = new Consignment()
                {
                    Consignor = new NameBase { Identity = "Ten Nguoi Giao", Name = "MaNguoiGiao" },
                    Consignee = new NameBase { Identity = "TenNguoiNhan", Name = "MaNguoiNhan<" },
                    NotifyParty = new NameBase { Identity = " ", Name = "MaNgDcTB" },
                    LoadingLocation = new LoadingLocation() { Name = "CangDoHang", Code = "003", Loading = DateTime.Now.ToString().ToString() },
                    UnloadingLocation = new UnloadingLocation() { Name = "", Code = "", Arrival = DateTime.Now.ToString() },
                    DeliveryDestination = new DeliveryDestination() { Line = "DiaDiemGiaoHang<" },
                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = "23",
                        Type = "16",
                    },
                    TransportEquipment_List = new List<TransportEquipment>(),

                    ConsignmentItem = new ConsignmentItem()
                    {
                        Sequence = "1",
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = "2",
                            Type = "Type",
                            MarkNumber = "MarkNumber",

                        },
                        Commodity = new Commodity()
                        {
                            Description = "dcription",
                            Identification = "identification",
                            TariffClassification = "TariffClassification"
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            Quantity = "3",
                            MeasureUnit = "55"
                        },
                        EquipmentIdentification = new EquipmentIdentification() { identification = "SoHieuContainer" },

                    },
                }
            });
            tkx.BillOfLading[0].Consignment.TransportEquipment_List.Add(new TransportEquipment()
            {
                Characteristic = "4",
                Fullness = "1",
                Seal = "SealNo",
                EquipmentIdentifications = new EquipmentIdentification { identification = "SoHieuContainer" }
            });
            tkx.AttachDocumentItem.Add(new AttachDocumentItem
            {
                Sequence = "1",
                Issuer = "811",
                Issue = DateTime.Today.ToString(),
                Reference = "So Chung Tu",
                Description = "Thong Tin Khac",
                AttachedFiles = new List<AttachedFile>()


            });
            tkx.AttachDocumentItem[0].AttachedFiles.Add(new AttachedFile
            {
                FileName = "truongsa.jpg",
                Content = new Content { Text = "/9j/4AAQSkZJRgABAQIAJQAlAAD/4gv4SUNDX1BST0ZJTEUAAQEAAAvoAAAAAAIAAABtbnRyf/Z" }
            });
            //------- Add additionalDocumentEx -----------
            tkx.AdditionalDocumentEx.Add(new AdditionalDocument
            {
                Type = "201",
                Reference = "So Chung Tu No",
                Issue = DateTime.Today.ToString(),
                IssueLocation = "Noi Cap",
                Issuer = "To Chuc Cap",
                Expire = DateTime.Today.ToString(),
                IsDebt = "0",
                Submit = DateTime.Today.ToString(),
                AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } }
            });


            string msg = Helpers.Serializer(tkx);
        }
        #endregion
        #region Gia công: Khai báo hợp đồng
        public static void GC_KhaiBaoHopDong()
        {
            GC_HopDong hd = new GC_HopDong
            {
                Issuer = "601",
                Function = "8",
                Reference = "AC50BB8ABADC48BDA812DCDAE5A9DAAB",
                Issue = DateTime.Today.ToString(),
                IssueLocation = "",
                DeclarationOffice = "C34C",
                Status = "1",
                CustomsReference = "",
                Agents = new List<Agent>(),
                Importer = new NameBase { Name = "May 10", Identity = "10100101308" },
                ContractDocument = new ContractDocument()
                {
                    Reference = "So Họp Dong",
                    Issue = DateTime.Today.ToString(),
                    Expire = DateTime.Today.ToString(),
                    Payment = new Payment { Method = "CASH" },
                    CurrencyExchange = new CurrencyExchange { CurrencyType = "USD" },
                    Importer = new NameBase { Name = "May10", Identity = "10100101308", Address = "Dia Chi" },
                    Exporter = new NameBase { Name = "Ben Thue Gia Cong", Identity = "Ma ben thue gia cong", Address = "Dia Chi" },
                    CustomsValue = new CustomsValue { TotalPaymentValue = "300000", TotalProductValue = "1000000" },
                    ImportationCountry = "VN",
                    ExportationCountry = "CU",
                    AdditionalInformation = new AdditionalInformation { Content = new Content() { Text = "ghi chu" } },
                    Item = new List<Item>(),

                    Product = new List<Product>(),

                    ////Equipment = new List<Equipment>(),

                    SampleProduct = new List<Product>()

                }
            };
            Item item = new Item

                {
                    Identity = "EBA",
                    Name = "bộ phận của máy và oto",
                    Quantity = "10000",
                    ProductValue = "500000",
                    PaymentValue = "150000"

                };
            Product product = new Product
            {
                Commodity = new Commodity
                {
                    Identification = "Ma Sp",
                    Description = "Ten SP",
                    TariffClassification = "1234567890",
                    ProductGroup = "EBA"
                },
                GoodsMeasure = new GoodsMeasure { MeasureUnit = "17" }
            };
            Equipment equipment = new Equipment
            {
                Commodity = new Commodity { Identification = " Ma TB", Description = "Ten TB", TariffClassification = "1234567890" },
                GoodsMeasure = new GoodsMeasure { Quantity = "2000", MeasureUnit = "17" },
                CurrencyExchange = new CurrencyExchange { CurrencyType = "USD" },
                Origin = new Origin { OriginCountry = "VN" },
                CustomsValue = new CustomsValue { unitPrice = "200" },
                Status = "0",
            };
            Product sample = new Product
            {

                Commodity = new Commodity { Identification = "Ma Hang Mau", Description = "Ten Hang Mau", TariffClassification = "1234567890" },
                GoodsMeasure = new GoodsMeasure { Quantity = "20", MeasureUnit = "17" }

            };
            Agent agent = new Agent
            {
                Name = "may 10",
                Identity = "1010010308",
                Status = "3"
            };
            hd.Agents.Add(agent);
            hd.Agents.Add(agent);
            hd.ContractDocument.Item.Add(item);
            hd.ContractDocument.Product.Add(product);
            hd.ContractDocument.Equipment.Add(equipment);
            hd.ContractDocument.SampleProduct.Add(sample);

            string msg = Helpers.Serializer(hd);
        }
        #endregion
        #region Gia Công: khai báo định mức
        public static void GC_DMSP()
        {
            GC_DinhMucSP dm = new GC_DinhMucSP
            {
                Issuer = "603",
                Function = "8",
                Reference = "E66A42D32E6B4771AF08BF3E78677663",
                Issue = DateTime.Today.ToString(),
                IssueLocation = "",
                DeclarationOffice = "C34C",
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Today.ToString(),
                Agents = new List<Agent>(),
                Importer = new NameBase
                {
                    Name = "May 10",
                    Identity = "10100101308",
                },
                ContractReference = new AdditionalDocument
                {
                    Reference = "So Hop Dong",
                    Issue = DateTime.Today.ToString(),
                    Expire = DateTime.Today.ToString(),
                    DeclarationOffice = "C34C",
                    CustomsReference = "So HOp Dong",
                },
                ProductionNorm = new ProductionNorm
                {
                    Product = new Product
                    {
                        Commodity = new Commodity { Identification = "MaSP", Description = "TenSP", TariffClassification = "1234567890" },
                        GoodsMeasure = new GoodsMeasure { MeasureUnit = "17" }
                    },
                    MaterialsNorm = new List<MaterialsNorm>()
                }


            };
            dm.Agents.Add(new Agent
            {
                Name = "may 10",
                Identity = "10100101308",
                Status = "3"
            });
            dm.ProductionNorm.MaterialsNorm.Add(new MaterialsNorm
            {
                Material = new Product
                {
                    Commodity = new Commodity { Identification = "MaNPL", Description = "TenNPL", TariffClassification = "1234567890" },
                    GoodsMeasure = new GoodsMeasure { MeasureUnit = "2" }
                },
                Norm = "3",
                Loss = "1"
            });
            string msg = Helpers.Serializer(dm);
        }
        #endregion
        //public static void PackSend()
        //{
        //    MessageRequest msgRequest = new MessageRequest()
        //    {
        //        MessageHeader = new MessageHeader()
        //        {
        //            ProcedureType = "2",
        //            From = new NameBase() { Name = "SOFTECH", Identity = "ST" },
        //            To = new NameBase() { Name = "C34C", Identity = "HAI QUAN" },
        //            Van = new NameBase()
        //        },
        //        ObjectContent = KD_ToKhaiXuat()

        //    };
        //    string msg = Helpers.Serializer(msgRequest);

        //}

        #region KD Huy To Khai Nhap
         public static KD_ToKhaiNhap HuyTKN()
        {
            KD_ToKhaiNhap Huy_ToKhaiNhap = new KD_ToKhaiNhap() 
            {
                Issuer = "929",
                Reference = "C3D71F8ED122485B8774915255570A5F",
                Issue = DateTime.Now.ToString(),
                Function = "5",
                IssueLocation = "CU",
                Status = "1",
                CustomsReference = "321",
                Acceptance = DateTime.Now.ToShortDateString(),
                DeclarationOffice = "C34C",
                Agents = new List<Agent>(),
                AdditionalInformations= new List<AdditionalInformation>()
            };
            //add Agents
            Huy_ToKhaiNhap.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //   //add AdditionalInformation
            Huy_ToKhaiNhap.AdditionalInformations.Add(new AdditionalInformation()
            {
               
                Content = new Content() { Text = "Ly do Huy" }
            });
            string msg = Helpers.Serializer(Huy_ToKhaiNhap);
            return Huy_ToKhaiNhap;
        }
        #endregion
        #region KD_Sua To Khai Nhap
        public static KD_ToKhaiNhap SuaTKN()
        {
            KD_ToKhaiNhap Sua_ToKhaiNhap = new KD_ToKhaiNhap()
            {
                Issuer = "929",
                Reference = "C3D71F8ED122485B8774915255570A5F",
                Issue = DateTime.Now.ToString(),
                Function = "5",
                IssueLocation = "CU",
                Status = "1",
                CustomsReference = "321",
                Acceptance = DateTime.Now.ToShortDateString(),
                DeclarationOffice = "C34C",
                GoodsItem = "2",
                LoadingList = "1",
                TotalGrossMass = "30",
                TotalNetGrossMass = "29",
                NatureOfTransaction = "NSX05",
                PaymentMethod = "CASH",
                Agents = new List<Agent>(),
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "AUD",
                    Rate = "12000"
                },
                DeclarationPackaging = new Packaging() { Quantity = "25" },
                AdditionalDocuments = new List<AdditionalDocument>(),
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToShortDateString(),
                    Reference = "So Hoa don",
                    Type = "380",
                },
                Importer = new NameBase() { Name = "name", Identity = "Iden" },
                RepresentativePerson = new RepresentativePerson()
                {
                    ContactFunction = "contactFuntion",
                    Name = "Name"
                },
                AdditionalInformations = new List<AdditionalInformation>(),
                GoodsShipment = new GoodsShipment()
                {
                    ExportationCountry = "MY",
                    Consignor = new NameBase() { Name = "Ten Nguoi GH", Identity = "Ma Nguoi GH" },
                    Consignee = new NameBase() { Name = "Ten Nguoi NH", Identity = "Ma Nguoi NH" },
                    NotifyParty = new NameBase() { Name = "Ten Nguoi TB", Identity = "Ma Nguoi TB" },
                    DeliveryDestination = new DeliveryDestination() { Line = "Dia diem GH" },
                    EntryCustomsOffice = new LocationNameBase() { Name = "name", Code = "code" },
                    ExitCustomsOffice = new LocationNameBase() { Name = "name", Code = "code" },
                    Exporter = new NameBase() { Name = "name", Identity = "iden" },
                    TradeTerm = new TradeTerm() { Condition = "Ma dieu kien GH" },
                    CustomsGoodsItems= new List<CustomsGoodsItem>()
                },
                Licenses= new List<License>(),
                ContractDocuments=new List<ContractDocument>(),
                CommercialInvoices=new List<CommercialInvoice>(),
                CertificateOfOrigins= new List<CertificateOfOrigin>(),
                BillOfLadings= new List<BillOfLading>(),
                CustomsOfficeChangedRequests= new List<CustomsOfficeChangedRequest>(),
                AdditionalDocument= new List<AdditionalDocument>()

            };
             //add Agents
            Sua_ToKhaiNhap.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            Sua_ToKhaiNhap.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            Sua_ToKhaiNhap.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //add AdditionalDocuments
            Sua_ToKhaiNhap.AdditionalDocument.Add(new AdditionalDocument() 
            { 
                Issue= DateTime.Now.ToShortDateString(),
                Reference="SoHDDTKN",
                Type="315",
                Name="Hop Dong",
                Expire= DateTime.Now.ToShortDateString()
            });
            Sua_ToKhaiNhap.AdditionalDocument.Add(new AdditionalDocument()
            {
                Issue = DateTime.Now.ToShortDateString(),
                Reference = "SOVANDON",
                Type = "706",
                Name = "Bill of Lading",
               
            });
            Sua_ToKhaiNhap.AdditionalDocument.Add(new AdditionalDocument()
            {
                Issue = DateTime.Now.ToShortDateString(),
                Reference = "SOGPTKN",
                Type = "911",
                Name = "Giay phep",
                Expire = DateTime.Now.ToShortDateString()
            });
            //add AdditionalInformation
            Sua_ToKhaiNhap.AdditionalInformations.Add(new AdditionalInformation()
            {
                Statement = "005",
                Content = new Content() { Text = "Ly do sua" }
            });
            Sua_ToKhaiNhap.AdditionalInformations.Add(new AdditionalInformation() 
            {
                Statement= "001",
                Content= new Content(){Text=""}
            });
        //   add Sua_ToKhaiNhap.GoodsShipment.CustomsGoodsItems
            CustomsGoodsItem item = new CustomsGoodsItem()
            {
                CustomsValue = "800",
                Sequence = "1",
                StatisticalValue = "16662400",
                UnitPrice = "32",
                StatisticalUnitPrice = "666496",
                Manufacturer = new NameBase() { Name = "name", Identity = "iden" },
                Origin = new Origin() { OriginCountry = "CU" },
                Commodity = new Commodity()
                {
                    Description = "decription",
                    Identification = "Identification",
                    TariffClassification = "12345678",
                    TariffClassificationExtension = "MaHS_MoRong",
                    Brand = "nhan hieu1",
                    Grade = "Quy Cach1",
                    Ingredients = "thanh phan1",
                    ModelNumber = "Model1",
                    DutyTaxFee = new List<DutyTaxFee>(),
                    InvoiceLine = new InvoiceLine() { ItemCharge = "", Line = "" }
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "25",
                    MeasureUnit = "12",
                },
                AdditionalInformations = new List<AdditionalInformation>(),
                CustomsValuation = new CustomsValuation()
                { 
                    ExitToEntryCharge="15.43534",
                    FreightCharge="7.6435",
                    Method="1",
                    OtherChargeDeduction="0"
                },
                ValuationAdjustments= new List<ValuationAdjustment>()

            };
            //dutyTaxFee
            DutyTaxFee dutItem = new DutyTaxFee()
            {
                AdValoremTaxBase="666496",
                DutyRegime="",
                SpecificTaxBase="",
                Tax="4",
                Type="1"
            };
            item.Commodity.DutyTaxFee.Add(dutItem);
            item.Commodity.DutyTaxFee.Add(dutItem);
            item.Commodity.DutyTaxFee.Add(dutItem);
            item.Commodity.DutyTaxFee.Add(dutItem);
            item.Commodity.DutyTaxFee.Add(dutItem);
            //AdditionalInformation
            AdditionalInformation additionItem = new AdditionalInformation()
            {
                Content = new Content() { Text = "12" },
                Statement="109",
                StatementDescription="ToSO"
            };
            AdditionalInformation additionItem1 = new AdditionalInformation()
            {
                Content = new Content() { Text = "2012-03-03" },
                Statement = "101",
                StatementDescription = "NGAY_XK"
            };
            AdditionalInformation additionItem2 = new AdditionalInformation()
            {
                Content = new Content() { Text = "1" },
                Statement = "102",
                StatementDescription = "QuyenSD"
            };
            AdditionalInformation additionItem3 = new AdditionalInformation()
            {
                Content = new Content() { Text = "0" },
                Statement = "103",
                StatementDescription = "KhongXD"
            };
            AdditionalInformation additionItem4 = new AdditionalInformation()
            {
                Content = new Content() { Text = "0" },
                Statement = "104",
                StatementDescription = "TraThem"
            };
            AdditionalInformation additionItem5 = new AdditionalInformation()
            {
                Content = new Content() { Text = "0" },
                Statement = "105",
                StatementDescription = "TienTra16"
            };
            AdditionalInformation additionItem6 = new AdditionalInformation()
            {
                Content = new Content() { Text = "1" },
                Statement = "106",
                StatementDescription = "CO_QHDB"
            };
            AdditionalInformation additionItem7 = new AdditionalInformation()
            {
                Content = new Content() { Text = "ad" },
                Statement = "107",
                StatementDescription = "Kieu_QHDB"
            };
            AdditionalInformation additionItem8 = new AdditionalInformation()
            {
                Content = new Content() { Text = "1" },
                Statement = "108",
                StatementDescription = "AnhHuongQH"
            };
            item.AdditionalInformations.Add(additionItem);
            item.AdditionalInformations.Add(additionItem1);
            item.AdditionalInformations.Add(additionItem2);
            item.AdditionalInformations.Add(additionItem3);
            item.AdditionalInformations.Add(additionItem4);
            item.AdditionalInformations.Add(additionItem5);
            item.AdditionalInformations.Add(additionItem6);
            item.AdditionalInformations.Add(additionItem7);
            item.AdditionalInformations.Add(additionItem8);
            //ValuationAdjustments
            ValuationAdjustment valuaAdj = new ValuationAdjustment() 
            {
                Addition="3242343",
                Percentage="12",
                Amount="3"
            };
            for (int i = 0; i < 20; i++) { item.ValuationAdjustments.Add(valuaAdj); }
            Sua_ToKhaiNhap.GoodsShipment.CustomsGoodsItems.Add(item);
            Sua_ToKhaiNhap.GoodsShipment.CustomsGoodsItems.Add(item);
            //add Licenses
            License licen = new License()
            {
                Issuer = "nguoi cap",
                Reference = "so GPTKN",
                Issue = DateTime.Now.ToShortDateString(),
                IssueLocation = "Noi Cap",
                Type = "",
                Expire = DateTime.Now.ToShortDateString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() {Text="Thong tin Khac" } },
                GoodItems= new List<GoodsItem>()
            };
            //GoodsItem
            GoodsItem goodsItem = new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "11",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUD", Rate = "12233" },
                Commodity = new Commodity()
                {
                    Description = "ten hang",
                    Identification = "Ma hang",
                    TariffClassification = "Ma HS"
                },
                GoodsMeasure = new GoodsMeasure() { Quantity = "20", MeasureUnit = "12" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Ghi chu" } },
            }; 
            licen.GoodItems.Add(goodsItem);
            Sua_ToKhaiNhap.Licenses.Add(licen);
            Sua_ToKhaiNhap.Licenses.Add(licen);
            Sua_ToKhaiNhap.Licenses.Add(licen);
            //Add contractdocments

            Sua_ToKhaiNhap.ContractDocuments.Add(new ContractDocument()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Expire = DateTime.Now.ToString().ToString(),
                Payment = new Payment()
                {
                    Method = "CASH"

                },
                TradeTerm = new TradeTerm()
                {
                    Condition = "condition"

                },
                DeliveryDestination = new DeliveryDestination() { Line = "line" },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD",


                },
                TotalValue = "2332.324",
                Buyer = new NameBase() { Name = "buyer1", Identity = "34" },
                Seller = new NameBase()
                {
                    Name = "seller1",
                    Identity = "s01"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },
                ContractItems = new List<ContractItem>(),

            });
            //Add contractdocments

            Sua_ToKhaiNhap.ContractDocuments.Add(new ContractDocument()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Expire = DateTime.Now.ToString().ToString(),
                Payment = new Payment()
                {
                    Method = "CASH"

                },
                TradeTerm = new TradeTerm()
                {
                    Condition = "condition"

                },
                DeliveryDestination = new DeliveryDestination() { Line = "line" },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD",


                },
                TotalValue = "2332.324",
                Buyer = new NameBase() { Name = "buyer1", Identity = "34" },
                Seller = new NameBase()
                {
                    Name = "seller1",
                    Identity = "s01"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },
                ContractItems = new List<ContractItem>(),

            });
            Sua_ToKhaiNhap.ContractDocuments.Add(new ContractDocument()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Expire = DateTime.Now.ToString().ToString(),
                Payment = new Payment()
                {
                    Method = "CASH"

                },
                TradeTerm = new TradeTerm()
                {
                    Condition = "condition"

                },
                DeliveryDestination = new DeliveryDestination() { Line = "line" },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD",


                },
                TotalValue = "2332.324",
                Buyer = new NameBase() { Name = "buyer1", Identity = "34" },
                Seller = new NameBase()
                {
                    Name = "seller1",
                    Identity = "s01"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },
                ContractItems = new List<ContractItem>(),

            });
            //ContractItems
            Sua_ToKhaiNhap.ContractDocuments[0].ContractItems.Add(new ContractItem()
            {
                unitPrice = "32",
                statisticalValue = "34.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "hang hoa trong Hop Dong" }
                }
            });
            Sua_ToKhaiNhap.ContractDocuments[0].ContractItems.Add(new ContractItem()
            {
                unitPrice = "2",
                statisticalValue = "4.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "hang hoa trong Hop Dong" }
                }
            });
            Sua_ToKhaiNhap.ContractDocuments[1].ContractItems.Add(new ContractItem()
            {
                unitPrice = "32",
                statisticalValue = "34.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                }
            });
            Sua_ToKhaiNhap.ContractDocuments[1].ContractItems.Add(new ContractItem()
            {
                unitPrice = "2",
                statisticalValue = "4.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                }
            });
            Sua_ToKhaiNhap.ContractDocuments[2].ContractItems.Add(new ContractItem()
            {
                unitPrice = "32",
                statisticalValue = "34.2",
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"

                },
                Origin = new Origin() { OriginCountry = "My" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23",
                    MeasureUnit = "measureunit"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "hang hoa trong Hop Dong" }
                }
            });
           
            //CommercialInvoices

            Sua_ToKhaiNhap.CommercialInvoices.Add(new CommercialInvoice()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Seller = new NameBase() { Identity = "identity", Name = "Name" },
                Buyer = new NameBase() { Identity = "identity", Name = "Name" },
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                Payment = new Payment()
                {
                    Method = "CASH"
                },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD"

                },
                TradeTerm = new TradeTerm() { Condition = "condition" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),


            });
            //CommercialInvoiceItems
            Sua_ToKhaiNhap.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "23.2",
                StatisticalValue = "23.1",
                Origin = new Origin() { OriginCountry = "VN" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
            Sua_ToKhaiNhap.CommercialInvoices[0].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "2",
                UnitPrice = "3.2",
                StatisticalValue = "2.1",
                Origin = new Origin() { OriginCountry = "My" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
            ////////////////////
            //CommercialInvoices1

            Sua_ToKhaiNhap.CommercialInvoices.Add(new CommercialInvoice()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Seller = new NameBase() { Identity = "identity", Name = "Name" },
                Buyer = new NameBase() { Identity = "identity", Name = "Name" },
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                Payment = new Payment()
                {
                    Method = "CASH"
                },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD"

                },
                TradeTerm = new TradeTerm() { Condition = "condition" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),


            });
            //CommercialInvoiceItems
            Sua_ToKhaiNhap.CommercialInvoices[1].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "23.2",
                StatisticalValue = "23.1",
                Origin = new Origin() { OriginCountry = "VN" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
            Sua_ToKhaiNhap.CommercialInvoices[1].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "2",
                UnitPrice = "3.2",
                StatisticalValue = "2.1",
                Origin = new Origin() { OriginCountry = "My" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
            //////////////////
            //CommercialInvoices2

            Sua_ToKhaiNhap.CommercialInvoices.Add(new CommercialInvoice()
            {
                Reference = "reference",
                Issue = DateTime.Now.ToString().ToString(),
                Seller = new NameBase() { Identity = "identity", Name = "Name" },
                Buyer = new NameBase() { Identity = "identity", Name = "Name" },
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                Payment = new Payment()
                {
                    Method = "CASH"
                },
                CurrencyExchange = new CurrencyExchange()
                {
                    CurrencyType = "USD"

                },
                TradeTerm = new TradeTerm() { Condition = "condition" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>(),


            });
            //CommercialInvoiceItems
            Sua_ToKhaiNhap.CommercialInvoices[2].CommercialInvoiceItems.Add(new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "23.2",
                StatisticalValue = "23.1",
                Origin = new Origin() { OriginCountry = "VN" },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "2",
                    Deduction = "deduction"
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" }
                },

            });
            


            //Add CertificateOfOrigins
            Sua_ToKhaiNhap.CertificateOfOrigins.Add(new CertificateOfOrigin()
            {
                Reference = "reference1",
                Type = "Type1",
                Issuer = "Issuer1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "CU",
                Representative = "Representative1",
                ExporterEx = new NameBase() { Identity = "Identity", Name = "Name" },
                ExportationCountryEx = new LocationNameBase() { Name = "Name", Code = "Code" },
                ImporterEx = new NameBase() { Identity = "Identity", Name = "Name" },
                ImportationCountryEx = new LocationNameBase() { Name = "Name", Code = "Code" },
                LoadingLocation = new LoadingLocation() { Name = "Name", Code = "Code", Loading = DateTime.Now.ToString().ToString() },
                UnloadingLocation = new UnloadingLocation() { Name = "Name", Code = "Code" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "ghi chu" } },
                GoodsItems = new List<GoodsItem>(),
                IsDebt = "23",
                Submit = "34"
            });
            //GoodsItems
            Sua_ToKhaiNhap.CertificateOfOrigins[0].GoodsItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "21",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUS", },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "2",
                    Type = "Type",
                    MarkNumber = "MarkNumber",

                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                Origin = new Origin() { OriginCountry = "VN" },
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                },

            });
            Sua_ToKhaiNhap.CertificateOfOrigins[0].GoodsItems.Add(new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "21",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUS", },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "2",
                    Type = "Type",
                    MarkNumber = "MarkNumber",

                },
                Commodity = new Commodity()
                {
                    Description = "dcription",
                    Identification = "identification",
                    TariffClassification = "TariffClassification"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "23.1",
                    MeasureUnit = "measureUnit"
                },
                Origin = new Origin() { OriginCountry = "VN" },
                Invoice = new Invoice()
                {
                    Issue = DateTime.Now.ToString().ToString(),
                    Reference = "abc",
                },

            });
            //Add BillOfLadings
            Sua_ToKhaiNhap.BillOfLadings.Add(new BillOfLading()
            {
                Reference = "reference1",
                Issue = DateTime.Now.ToString().ToString(),
                IssueLocation = "issueLocation1",
                BorderTransportMeans = new BorderTransportMeans()
                {
                    Identity = "identity",
                    Identification = "identification",
                    Journey = "journey",
                    ModeAndType = "modeAndType",
                    Departure = DateTime.Now.ToString().ToString(),
                    RegistrationNationality = "RegistrationNationality"
                },
                MyProCarrierperty = new NameBase() { Identity = "Identity", Name = "Name" },
                Consignment = new Consignment()
                {
                    Consignor = new NameBase { Identity = "identity", Name = "name" },
                    Consignee = new NameBase { Identity = "", Name = "" },
                    NotifyParty = new NameBase { Identity = "", Name = "" },
                    LoadingLocation = new LoadingLocation() { Name = "Name", Code = "Code", Loading = DateTime.Now.ToString().ToString() },
                    UnloadingLocation = new UnloadingLocation() { Name = "Name", Code = "Code", Arrival = DateTime.Now.ToString().ToString() },
                    DeliveryDestination = new DeliveryDestination() { Line = "" },
                    ConsignmentItemPackaging = new Packaging()
                    {
                        Quantity = "12",
                        Type = "Type",
                    },
                    TransportEquipment_List = new List<TransportEquipment>(),
                    ConsignmentItem = new ConsignmentItem()
                    {
                        Sequence = "1",
                        ConsignmentItemPackaging = new Packaging()
                        {
                            Quantity = "2",
                            Type = "Type",
                            MarkNumber = "MarkNumber",

                        },
                        Commodity = new Commodity()
                        {
                            Description = "ten hang",
                            Identification = "identification",
                            TariffClassification = "TariffClassification"
                        },
                        GoodsMeasure = new GoodsMeasure()
                        {
                            Quantity = "23.1",
                            MeasureUnit = "measureUnit"
                        },
                        EquipmentIdentification = new EquipmentIdentification() { identification = "iden" },

                    }
                }
            });
            //TransportEquipments
            Sua_ToKhaiNhap.BillOfLadings[0].Consignment.TransportEquipment_List.Add(new TransportEquipment()
            {
                Characteristic = "characteris",
                Fullness = "fullness",
                Seal = "seal",
                EquipmentIdentifications = new EquipmentIdentification() { identification = "identification" }
            });
            Sua_ToKhaiNhap.BillOfLadings[0].Consignment.TransportEquipment_List.Add(new TransportEquipment()
            {
                Characteristic = "characteris",
                Fullness = "fullness",
                Seal = "seal",
                EquipmentIdentifications = new EquipmentIdentification() { identification = "identification" }
            });
            //Add CustomsOfficeChangedRequests
            Sua_ToKhaiNhap.CustomsOfficeChangedRequests.Add(new CustomsOfficeChangedRequest()
            {
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" },
                    ExaminationPlace = "examinationPlace",
                    Time = DateTime.Now.ToString().ToString(),
                    Route = "route"
                },
            });
            //Add CustomsOfficeChangedRequests
            Sua_ToKhaiNhap.CustomsOfficeChangedRequests.Add(new CustomsOfficeChangedRequest()
            {
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString()
                },
                AdditionalInformation = new AdditionalInformation()
                {
                    Content = new Content() { Text = "ghi chu" },
                    ExaminationPlace = "examinationPlace",
                    Time = DateTime.Now.ToString().ToString(),
                    Route = "route"
                },
            });
          
            string msg = Helpers.Serializer(Sua_ToKhaiNhap);
            return Sua_ToKhaiNhap;
        }

        #endregion
        #region KD Bo Sung Chung tu Dinh Kem Dang Anh
        public static KD_BoSungCtuDangAnh CtuDangAnh()
        {
            KD_BoSungCtuDangAnh KD_BSCTDA = new KD_BoSungCtuDangAnh()
            {
                Issuer = "100",
                Function = "8",
                Reference = "7DF227A290B94BAF94D88EA8AD237023",
                Issue = DateTime.Now.ToString(),
                IssueLocation = "",
                DeclarationOffice = "C34C",
                Status = "1",
                CustomsReference = "",
                Acceptance = "",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "name", Identity = "Iden" },
                AttachDocuments = new List<AttachDocumentItem>()
            };
            //add Agents
            KD_BSCTDA.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //add AttachDocumentItem
            AttachDocumentItem item = new AttachDocumentItem()
            {
                Sequence = "1",
                Issuer = "200",
                Issue = DateTime.Now.ToShortDateString(),
                Reference = "so chung tu",
                Description = "decription",
                AttachedFiles = new List<AttachedFile>(),
            };
            AttachedFile AttItem = new AttachedFile()
            {
                FileName = "042.JPG",
                Content = new Content()
                {
                    Base64 = "bin.base64",
                    Text = "frgfdgdfgfdgfg"
                },
            };
            item.AttachedFiles.Add(AttItem);
            KD_BSCTDA.AttachDocuments.Add(item);
            string msg = Helpers.Serializer(KD_BSCTDA);
            return KD_BSCTDA;

        }
        #endregion
        #region Bo sung Don xin chuyen CK
        public static KD_BS_DonXinChuyenCK KD_DXCCK()
        {
            KD_BS_DonXinChuyenCK KD_DonXCCK = new KD_BS_DonXinChuyenCK()
            {
                Issuer = "405",
                Reference = "B22B9938A2B842F69B5CE0564825F34B",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "My",
                Status = "1",
                CustomsReference = "123",
                Acceptance = DateTime.Now.ToShortDateString(),
                DeclarationOffice = "C34C",
                NatureOfTransaction = "NSX05",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "name", Identity = "Iden" },
                CustomsOfficeChangedRequest = new CustomsOfficeChangedRequest()
                {
                    AdditionalDocument = new AdditionalDocument()
                    {
                        Reference = "SOVANDON",
                        Issue = DateTime.Now.ToShortDateString()
                    },
                    AdditionalInformation = new AdditionalInformation()
                    {
                        Content = new Content() { Text = "Noi dung xin chuyen CK" },
                        ExaminationPlace = "Dia diem kiem tra",
                        Time = "",
                        Route = "tuyen duong"
                    }
                }
            };
            //add Agents
            KD_DonXCCK.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            KD_DonXCCK.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            KD_DonXCCK.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            string msg = Helpers.Serializer(KD_DonXCCK);
            return KD_DonXCCK;
        }
        #endregion
        #region Bo sung Hoa don Thuong Mai
        public static KD_BSHoaDonTM KD_BSHD()
        {
            KD_BSHoaDonTM KD_HDTM = new KD_BSHoaDonTM()
            {
                Issuer = "380",
                Reference = "88FA5AC751AA4A2D9A528D5CACF72083",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "",
                Status = "1",
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = "C34C",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "name", Identity = "Iden" },
                DeclarationDocument = new DeclarationBase()
                {
                    Reference = "ref",
                    Issue = DateTime.Now.ToString(),
                    NatureOfTransaction = "natureOfTransaction",
                    DeclarationOffice = "DeclarationOffice"
                },
                CommercialInvoices = new List<CommercialInvoice>()

            };
            //add Agents
            KD_HDTM.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            KD_HDTM.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            KD_HDTM.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //add commercialInvoice
            CommercialInvoice item = new CommercialInvoice()
            {
                Reference = "ref",
                Issue = DateTime.Now.ToString(),
                Seller = new NameBase() { Name = "name", Identity = "Iden" },
                Buyer = new NameBase() { Name = "name", Identity = "Iden" },
                AdditionalDocument = new AdditionalDocument()
                {
                    Reference = "ref",
                    Issue = DateTime.Now.ToString(),
                },
                Payment = new Payment()
                {
                    Method = "Cash"
                },
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "USD" },
                TradeTerm = new TradeTerm() { Condition = "CF" },
                CommercialInvoiceItems = new List<CommercialInvoiceItem>()
            };
            CommercialInvoiceItem comerItem = new CommercialInvoiceItem()
            {
                Sequence = "1",
                UnitPrice = "12",
                StatisticalValue = "360",
                Origin = new Origin() { OriginCountry = "CU" },
                Commodity = new Commodity()
                {
                    Description = "Ten hang",
                    Identification = "Ma Hamg",
                    TariffClassification = "MaHS"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "30",
                    MeasureUnit = "12"
                },
                ValuationAdjustment = new ValuationAdjustment()
                {
                    Addition = "34",
                    Deduction = "54"
                },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Ghi chu khac" } }


            };
            item.CommercialInvoiceItems.Add(comerItem);
            KD_HDTM.CommercialInvoices.Add(item);
            string msg = Helpers.Serializer(KD_HDTM);
            return KD_HDTM;

        }
        #endregion
        #region BoSungCo
        public static KD_BoSungCo KD_BSungCO()
        {
            KD_BoSungCo KDBSCO = new KD_BoSungCo()
            {

                Issuer = "861",
                Reference = "3707B7FCE31D4C8EA8A117FEBAAEC1B6",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "",
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToString(),
                DeclarationOffice = "C34C",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = "iden", Name = "name" },
                DeclarationDocument = new DeclarationBase
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString(),
                    NatureOfTransaction = "NatureOfTransaction",
                    DeclarationOffice = "C34C"
                },
                CertificateOfOrigins = new List<CertificateOfOrigin>()

            };
            //add Agents
            KDBSCO.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            KDBSCO.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            KDBSCO.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //add CertificateOfOrigins
            KDBSCO.CertificateOfOrigins.Add(new CertificateOfOrigin()
            {
                Reference = "SoCO",
                Type = "007",
                Issuer = "To chuc cap",
                Issue = DateTime.Now.ToString(),
                IssueLocation = "AF",
                Representative = "Nguoi Ky",
                ExporterEx = new NameBase() { Name = "Ten,dia chi ", Identity = "identity" },
                ExportationCountryEx = new LocationNameBase() { Name = "locateName", Code = "code" },
                ImporterEx = new NameBase() { Name = "Ten,dia chi", Identity = "Iden" },
                ImportationCountryEx = new LocationNameBase() { Name = "LocaleNane", Code = "code" },
                LoadingLocation = new LoadingLocation() { Name = "Cang xep hang", Code = "code", Loading = DateTime.Now.ToString() },
                UnloadingLocation = new UnloadingLocation() { Name = "Buu dien Hai Phong", Code = "Code" },
                IsDebt = "1",
                Submit = DateTime.Now.ToString(),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Thong tin khac" } },
                GoodsItems = new List<GoodsItem>(),
            });
            //add GoodItem
            GoodsItem item = new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "23",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "EUR" },
                ConsignmentItemPackaging = new Packaging()
                {
                    Quantity = "21",
                    Type = "kieu dong goi",
                    MarkNumber = "So hieu kien",
                },
                Commodity = new Commodity()
                {
                    Description = "decription",
                    Identification = "Identification",
                    TariffClassification = "MaHS"
                },
                GoodsMeasure = new GoodsMeasure() { GrossMass = "342", MeasureUnit = "1" },
                Origin = new Origin() { OriginCountry = "AF" },
                Invoice = new Invoice() { Reference = "reference", Issue = DateTime.Now.ToString() },

            };
            KDBSCO.CertificateOfOrigins[0].GoodsItems.Add(item);
            string msg = Helpers.Serializer(KDBSCO);

            return KDBSCO;
        }
        #endregion
        #region Bo sung GiayPhep
        public static KD_BSGiayPhep gphep()
        {
            KD_BSGiayPhep gp = new KD_BSGiayPhep()
            {
                Issuer = "911",
                Reference = "0A45495F52A842B3A4F274D9C05CF0F7",
                Issue = DateTime.Now.ToString(),
                Function = "8",
                IssueLocation = "",
                Status = "1",
                CustomsReference = "",
                Acceptance = "",
                DeclarationOffice = "C34C",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "importer", Identity = "idenImporter" },
                DeclarationDocument = new DeclarationBase()
                {
                    Reference = "123",
                    Issue = DateTime.Now.ToString(),
                    NatureOfTransaction = "natureOfTransaction",
                    DeclarationOffice = "C34C"
                },
                Licenses = new List<License>()
            };
            //add Agents
            gp.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            gp.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            gp.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //add Licenses
            gp.Licenses.Add(new License()
            {
                Issuer = "Nguoi cap",
                Reference = "SOGPTKN",
                Issue = DateTime.Now.ToString("yyyy-MM-dd"),
                IssueLocation = "noi cap",
                Type = "type",
                Expire = DateTime.Now.ToString("yyyy-MM-dd"),
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "content" } },
                GoodItems = new List<GoodsItem>()
            });
            //add goodItem
            GoodsItem item = new GoodsItem()
            {
                Sequence = "1",
                StatisticalValue = "11",
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "AUD" },
                Commodity = new Commodity()
                {
                    Description = "Ten hang",
                    Identification = "Ma hang",
                    TariffClassification = "Ma HS"
                },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "20",
                    MeasureUnit = "13"
                },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Ghi Chu" } },
            };
            gp.Licenses[0].GoodItems.Add(item);
            string msg = Helpers.Serializer(gp);

            return gp;
        }
        #endregion
        #region Bo Sung Hop Dong
        public static KD_BSHopDong HopDong()
        {
            KD_BSHopDong HD = new KD_BSHopDong()
            {
                Issuer = "315",
                Reference = "EDC27403CDE84F0DB9FCF9DBE940868B",
                Issue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                Function = "8",
                IssueLocation = "",
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToShortDateString(),
                DeclarationOffice = "C34C",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Name = "name", Identity = "Identity" },
                DeclarationDocument = new DeclarationBase()
                {
                    Reference = "123",
                    Issue = DateTime.Now.ToString(),
                    NatureOfTransaction = "NSX05",
                    DeclarationOffice = "C34C"
                },
                ContractDocuments = new List<ContractDocument>()
            };
            //add Agents
            HD.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            HD.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            HD.Agents.Add(new Agent()
            {
                Name = "name",
                Identity = "Identity",
                Status = "Status"
            });
            //add ContractDocument
            ContractDocument item = new ContractDocument()
            {
                Reference = "SoHDDKTKN",
                Issue = DateTime.Now.ToShortDateString(),
                Expire = DateTime.Now.ToShortDateString(),
                Payment = new Payment() { Method = "Cash" },
                TradeTerm = new TradeTerm() { Condition = "CF" },
                DeliveryDestination = new DeliveryDestination() { Line = "Dia diem giao hang" },
                CurrencyExchange = new CurrencyExchange() { CurrencyType = "USD" },
                TotalValue = "1222",
                Buyer = new NameBase() { Name = "name", Identity = "Iden" },
                Seller = new NameBase() { Name = "name", Identity = "Iden" },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Thong tin khac" } },
                ContractItems = new List<ContractItem>()
            };
            ContractItem ctrItem = new ContractItem()
            {
                unitPrice = "30",
                statisticalValue = "600",
                Commodity = new Commodity()
                {
                    Description = "Ten hang",
                    Identification = "Ma hang",
                    TariffClassification = "MaHS",
                },
                Origin = new Origin() { OriginCountry = "MY" },
                GoodsMeasure = new GoodsMeasure()
                {
                    Quantity = "20",
                    MeasureUnit = "2"
                },
                AdditionalInformation = new AdditionalInformation() { Content = new Content() { Text = "Ghi chu" } }
            };
            item.ContractItems.Add(ctrItem);
            HD.ContractDocuments.Add(item);
            string msg = Helpers.Serializer(HD);

            return HD;
        }
        #endregion
        #region GC_PKien
        public static void GC_PKien()
        {
            GC_PhuKienHD PkHD = new GC_PhuKienHD()
            {
                Issuer = "602",
                Reference = "U910CD5AF5E541DA970D0FF38D5BA0DE",
                Issue = DateTime.Now.ToString().ToString(),
                Function = "8",
                IssueLocation = "issueLocation",
                Status = "",
                CustomsReference = "customref",
                Acceptance = DateTime.Now.ToString().ToString(),
                DeclarationOffice = "declarationOffice",
                Agents = new List<Agent>(),
                Importer = new NameBase() { Identity = "iden", Name = "May10" },
                ContractReference = new AdditionalDocument()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString(),
                    DeclarationOffice = "declarationOffice"

                },
                SubContract = new Subcontract()
                {
                    Reference = "reference",
                    Issue = DateTime.Now.ToString().ToString(),
                    Decription = "decription"
                },
                AdditionalInformations = new List<AdditionalInformation>(),

            };
            //AdditionalInformations
            PkHD.AdditionalInformations.Add(new AdditionalInformation()
            {
                Statement = "802",
                Content = new Content()
                {
                    Declaration = new DeclarationPhuKien()
                    {
                        Product = new Product()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = "iden",
                                Description = "decription",
                                TariffClassification = "tariffClassification",
                                ProductGroup = "productGroup"
                            },
                            GoodsMeasure = new GoodsMeasure() { MeasureUnit = "measureUnit" },

                        }
                    }
                }

            });
            PkHD.AdditionalInformations.Add(new AdditionalInformation()
            {
                Statement = "802",
                Content = new Content()
                {
                    Declaration = new DeclarationPhuKien()
                    {
                        Material = new Product()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = "iden",
                                Description = "decription",
                                TariffClassification = "tariffClassification",

                            },
                            GoodsMeasure = new GoodsMeasure() { MeasureUnit = "measureUnit" },

                        }
                    }
                }

            });
            PkHD.AdditionalInformations.Add(new AdditionalInformation()
            {
                Statement = "802",
                Content = new Content()
                {
                    Declaration = new DeclarationPhuKien()
                    {
                        Equipment = new Equipment()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = "iden",
                                Description = "decription",
                                TariffClassification = "tariffClassification",
                                ProductGroup = "productGroup"
                            },
                            GoodsMeasure = new GoodsMeasure() { MeasureUnit = "measureUnit" },
                            CurrencyExchange = new CurrencyExchange() { CurrencyType = "VND" },
                            Origin = new Origin() { OriginCountry = "CU" },
                            CustomsValue = new CustomsValue() { unitPrice = "34f" },
                            Status = "34"
                        }
                    }
                }

            });
            PkHD.AdditionalInformations.Add(new AdditionalInformation()
            {
                Statement = "802",
                Content = new Content()
                {
                    Declaration = new DeclarationPhuKien()
                    {
                        SampleProduct = new Equipment()
                        {
                            Commodity = new Commodity()
                            {
                                Identification = "iden",
                                Description = "decription",
                                TariffClassification = "tariffClassification",
                                ProductGroup = "productGroup"
                            },
                            GoodsMeasure = new GoodsMeasure() { MeasureUnit = "measureUnit" },
                            CurrencyExchange = new CurrencyExchange() { CurrencyType = "VND" },

                        }
                    }
                }

            });



            //Agents
            PkHD.Agents.Add(new Agent() { Identity = "iden1", Name = "name1", Status = "3" });
            PkHD.Agents.Add(new Agent() { Identity = "iden2", Name = "name2", Status = "2" });
            PkHD.Agents.Add(new Agent() { Identity = "iden3", Name = "name3", Status = "23" });
            string msg = Helpers.Serializer(PkHD);
        }
        #endregion
    }
    #region ApproverViet
    public class KhaiBaoToKhaiViet : ApproverMessageInfo
    {
        public override bool ProcessMessageHandler(object sender, MessageEventArgs e)
        {
            return false;
        }
    }
    public class LayPhanHoiToKhaiViet : ApproverMessageInfo
    {
        public override bool ProcessMessageHandler(object sender, MessageEventArgs e)
        {
            return false;
        }
    }
    #endregion ApproverViet

    #region ApproverEng
    public class KhaiBaoToKhaiEnghlish : ApproverMessageInfo
    {
        public override bool ProcessMessageHandler(object sender, MessageEventArgs e)
        {
            return false;
        }
    }
    #endregion ApproverEng

}
