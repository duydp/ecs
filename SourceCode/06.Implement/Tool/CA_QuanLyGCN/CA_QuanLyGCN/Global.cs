﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CA_QuanLyGCN
{
    public  class Global
    {

        public static string ConnectionString { get; set; }

        public static string TenCoQuanCapPhep { get; set; }
        public static string TenPhongBanCapPhep { get; set; }
        public static string FullName { get; set; }
        public static SizeFont sizeFont { get; set; }
     




        public static void LoadConnStr(string AppPath)
        {
            ConnectionString = string.Format(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};
Jet OLEDB:Database Password={1}", AppPath + "\\Data\\Data.mdb", "MKkb1980");
        }

        public static void LoadCauHinh()
        {
            TenCoQuanCapPhep = new CauHinh().Search("CoQuan")[0].KeyValue;
            TenPhongBanCapPhep = new CauHinh().Search("Phong")[0].KeyValue;
            FullName = new CauHinh().Search("FullName")[0].KeyValue;
            sizeFont = new SizeFont();
            sizeFont.ReadXml(System.Windows.Forms.Application.StartupPath);
        }
      
    }
}

