﻿using System;
using System.Windows.Forms;
using Company.GC.BLL.SXXK;
using Company.GC.BLL.SXXK.ToKhai;
using System.Threading;
using System.Data;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Xml;

namespace Company.Interface
{
    public partial class ThongTinDNAndHQForm2 : BaseForm
    {
        private ErrorProvider errorProvider1 = new ErrorProvider();

        public ThongTinDNAndHQForm2()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            /* Update by HungTQ 22/12/2010*/

            txtDiaChi.Text = GlobalSettings.DIA_CHI;
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            txtTenNganHQ.Text = GlobalSettings.TEN_HAI_QUAN_NGAN;
            donViHaiQuanControl1.Ma = GlobalSettings.MA_HAI_QUAN;
            txtTenCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
            txtMailDoanhNghiep.Text = GlobalSettings.MailDoanhNghiep;
            txtMailHaiQuan.Text = GlobalSettings.MailHaiQuan;
            txtMaCuc.Text = GlobalSettings.MA_CUC_HAI_QUAN;
            txtMaMid.Text = GlobalSettings.MaMID;
            donViHaiQuanControl1.ReadOnly = false;
            txtDienThoaiDN.Text = GlobalSettings.SoDienThoaiDN;
            txtSoFaxDN.Text = GlobalSettings.SoFaxDN;
            txtNguoiLienHeDN.Text = GlobalSettings.NguoiLienHe;
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
                return;

            if (!Company.Interface.Globals.ValidateNull(donViHaiQuanControl1, errorProvider1, "Mã chi cục Hải quan"))
                return;
            if (!Company.Interface.Globals.ValidateNull(txtDienThoaiDN, errorProvider1, "Điện thoại"))
                return;
            if (!Company.Interface.Globals.ValidateNull(txtMailDoanhNghiep, errorProvider1, "Email (Thư điện tử)"))
                return;

            //Hungtq 22/12/2010. Luu cau hinh
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DIA_CHI", txtDiaChi.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_DON_VI", txtMaDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_HAI_QUAN", donViHaiQuanControl1.Ma.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN", donViHaiQuanControl1.Ten);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_HAI_QUAN_NGAN", txtTenNganHQ.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_DON_VI", txtTenDN.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MA_CUC_HAI_QUAN", txtMaCuc.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TEN_CUC_HAI_QUAN", txtTenCucHQ.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailDoanhNghiep", txtMailDoanhNghiep.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MailHaiQuan", txtMailHaiQuan.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("MaMID", txtMaMid.Text);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FromMail", txtMailDoanhNghiep.Text.Trim());

            /*DATLMQ update Lưu cấu hình vào file config 18/01/2011.*/
            XmlDocument doc = new XmlDocument();
            string path = Company.GC.BLL.EntityBase.GetPathProgram() + "\\ConfigDoanhNghiep";
            //Hungtq update 28/01/2011.
            string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

            doc.Load(fileName);

            //Set thông tin MaCucHQ
            //XmlNode nodeMaCucHQ = doc.GetElementsByTagName("Config")[5];
            XmlNode nodeMaCucHQ = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ");
            GlobalSettings.MA_CUC_HAI_QUAN = nodeMaCucHQ.InnerText = txtMaCuc.Text.Trim();
            //Set thông tin TenCucHQ
            //XmlNode nodeTenCucHQ = doc.GetElementsByTagName("Config")[6];
            XmlNode nodeTenCucHQ = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ");
            GlobalSettings.TEN_CUC_HAI_QUAN = nodeTenCucHQ.InnerText = txtTenCucHQ.Text.Trim();
            //Set thông tin MaChiCucHQ
            //XmlNode nodeMaChiCucHQ = doc.GetElementsByTagName("Config")[7];
            XmlNode nodeMaChiCucHQ = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ");
            GlobalSettings.MA_HAI_QUAN = nodeMaChiCucHQ.InnerText = donViHaiQuanControl1.Ma.Trim();
            //Set thông tin TenChiCucHQ
            //XmlNode nodeTenChiCucHQ = doc.GetElementsByTagName("Config")[8];
            XmlNode nodeTenChiCucHQ = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ");
            GlobalSettings.TEN_HAI_QUAN = nodeTenChiCucHQ.InnerText = donViHaiQuanControl1.Ten.Trim();
            //Set thông tin TenNganChiCucHQ
            //XmlNode nodeTenNganChiCucHQ = doc.GetElementsByTagName("Config")[9];
            XmlNode nodeTenNganChiCucHQ = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ");
            GlobalSettings.TEN_HAI_QUAN_NGAN = nodeTenNganChiCucHQ.InnerText = txtTenNganHQ.Text.Trim();
            //Set thông tin MailHQ
            //XmlNode nodeMailHQ = doc.GetElementsByTagName("Config")[10];
            XmlNode nodeMailHQ = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ");
            GlobalSettings.MailHaiQuan = nodeMailHQ.InnerText = txtMailHaiQuan.Text.Trim();
            //Set thông tin MaDoanhNghiep
            //XmlNode nodeMaDoanhNghiep = doc.GetElementsByTagName("Config")[11];
            XmlNode nodeMaDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep");
            GlobalSettings.MA_DON_VI = nodeMaDoanhNghiep.InnerText = txtMaDN.Text.Trim();
            //Set thông tin TenDoanhNghiep
            //XmlNode nodeTenDoanhnghiep = doc.GetElementsByTagName("Config")[12];
            XmlNode nodeTenDoanhnghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep");
            GlobalSettings.TEN_DON_VI = nodeTenDoanhnghiep.InnerText = txtTenDN.Text.Trim();
            //Set thông tin DiaChiDoanhNghiep
            //XmlNode nodeDiaChiDN = doc.GetElementsByTagName("Config")[13];
            XmlNode nodeDiaChiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep");
            GlobalSettings.DIA_CHI = nodeDiaChiDN.InnerText = txtDiaChi.Text.Trim();
            //Set thông tin MaMid
            //XmlNode nodeMaMID = doc.GetElementsByTagName("Config")[14];
            XmlNode nodeMaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID");
            GlobalSettings.MaMID = nodeMaMID.InnerText = txtMaMid.Text.Trim();
            //Set thông tin MailDoanhNghiep
            //XmlNode nodeMailDN = doc.GetElementsByTagName("Config")[15];
            XmlNode nodeMailDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep");
            GlobalSettings.MailDoanhNghiep = nodeMailDN.InnerText = txtMailDoanhNghiep.Text.Trim();
            //Set thông tin SoDienThoai
            //XmlNode nodeSoDienThoai = doc.GetElementsByTagName("Config")[16];
            XmlNode nodeSoDienThoai = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai");
            GlobalSettings.SoDienThoaiDN = nodeSoDienThoai.InnerText = txtDienThoaiDN.Text.Trim();
            //Set thông tin SoFax
            //XmlNode nodeSoFax = doc.GetElementsByTagName("Config")[17];
            XmlNode nodeSoFax = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax");
            GlobalSettings.SoFaxDN = nodeSoFax.InnerText = txtSoFaxDN.Text.Trim();
            //Set thông tin NguoiLienHe
            //XmlNode nodeNguoiLienHe = doc.GetElementsByTagName("Config")[18];
            XmlNode nodeNguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe");
            GlobalSettings.NguoiLienHe = nodeNguoiLienHe.InnerText = txtNguoiLienHeDN.Text.Trim();

            //Lưu file cấu hình
            doc.Save(fileName);

            try
            {
                CheckUser.Bll.UserInfor customer = new CheckUser.Bll.UserInfor();
                customer.MaDonVi = GlobalSettings.MA_DON_VI;
                customer.TenDonVi = GlobalSettings.TEN_DON_VI;
                customer.SoDTLienHe = GlobalSettings.SoDienThoaiDN;
                customer.Email = GlobalSettings.MailDoanhNghiep;
                customer.DiaChi = GlobalSettings.DIA_CHI;
                customer.MaMayKichHoat = Program.getProcessorID();
                customer.TenPhienBan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AssemblyName");
                customer.TinhTrang = Program.lic.licenseName + "\r\nMã chi cục: " + donViHaiQuanControl1.Ma + "\r\nTên chi cục: " + donViHaiQuanControl1.Ten;
                customer.NgayHetHan = System.Convert.ToDateTime(Program.lic.dayExpires);
                customer.KeyLicense = Program.lic.codeActivate;
                WSCustomer.Service1 ws = new Company.Interface.WSCustomer.Service1();
                ws.InsertUser(customer);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage("Lưu thông tin Doanh nghiệp qua Web Service.", ex); }

            ShowMessage("Lưu file cấu hình Thông tin Doanh nghiệp và Hải quan thành công.", false);

            GlobalSettings.RefreshKey();
            this.Close();
        }

        private void txtMailHaiQuan_Leave(object sender, EventArgs e)
        {
            if (txtMailHaiQuan.Text != "")
            {
                string regularExpression = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                if (Regex.IsMatch(txtMailHaiQuan.Text.ToString().Trim(), regularExpression))
                {
                    ep.SetError(txtMailHaiQuan, "");
                }
                else
                {
                    ep.SetError(txtMailHaiQuan, "Nhập sai định dạng email");
                }
            }
        }

        private void txtMailDoanhNghiep_Leave(object sender, EventArgs e)
        {
            if (txtMailDoanhNghiep.Text != "")
            {
                string regularExpression = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
                if (Regex.IsMatch(txtMailDoanhNghiep.Text.ToString().Trim(), regularExpression))
                {
                    ep.SetError(txtMailDoanhNghiep, "");
                }
                else
                {
                    ep.SetError(txtMailDoanhNghiep, "Nhập sai định dạng email");
                }
            }
        }
    }
}