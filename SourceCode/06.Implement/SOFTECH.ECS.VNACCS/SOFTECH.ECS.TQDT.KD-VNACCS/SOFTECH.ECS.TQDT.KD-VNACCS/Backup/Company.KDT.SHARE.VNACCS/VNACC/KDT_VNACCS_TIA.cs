﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;


namespace Company.KDT.SHARE.VNACCS
{
     public partial class KDT_VNACCS_TIA
    {
         List<KDT_VNACCS_TIA_HangHoa> _ListHang = new List<KDT_VNACCS_TIA_HangHoa>();

         public List<KDT_VNACCS_TIA_HangHoa> HangCollection
        {
            set { this._ListHang = value; }
            get { return this._ListHang; }
        }

         public bool InsertUpdateFul()
         {
             bool ret;
             SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
             using (SqlConnection connection = (SqlConnection)db.CreateConnection())
             {
                 connection.Open();
                 SqlTransaction transaction = connection.BeginTransaction();
                 try
                 {
                     if (this.ID == 0)
                     {
                         //this.TrangThaiXuLy = "1";
                         this.ID = this.Insert();
                     }
                     else
                         this.Update();

                     // luu hang to khai
                     foreach (KDT_VNACCS_TIA_HangHoa item in this.HangCollection)
                     {
                         if (item.ID == 0)
                         {
                             item.Master_ID = this.ID;
                             item.ID = item.Insert();
                         }
                         else
                         {
                             item.Update();
                         }
                     }

                   

                  
                     ret = true;
                 }
                 catch (Exception ex)
                 {
                     transaction.Rollback();
                     this.ID = 0;
                     throw new Exception(ex.Message);
                 }
                 finally
                 {
                     connection.Close();
                 }
             }
             return ret;
         }

         public void LoadHang()
         {
             if(this.ID >0 )
             {
                 _ListHang = KDT_VNACCS_TIA_HangHoa.SelectCollectionDynamic("Master_ID = " + this.ID, null);
             }

         }
    }
}
