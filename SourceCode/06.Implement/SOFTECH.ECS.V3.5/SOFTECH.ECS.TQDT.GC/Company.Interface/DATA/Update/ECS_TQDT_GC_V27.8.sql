-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_SelectBy_ManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectBy_ManufactureFactory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careery_DeleteBy_ManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteBy_ManufactureFactory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Insert]
	@LoaiNganhNghe numeric(1, 0),
	@ChuKySanXuat nvarchar(2000),
	@ManufactureFactory_ID bigint,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_Careeries]
(
	[LoaiNganhNghe],
	[ChuKySanXuat],
	[ManufactureFactory_ID]
)
VALUES 
(
	@LoaiNganhNghe,
	@ChuKySanXuat,
	@ManufactureFactory_ID
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Update]
	@ID bigint,
	@LoaiNganhNghe numeric(1, 0),
	@ChuKySanXuat nvarchar(2000),
	@ManufactureFactory_ID bigint
AS

UPDATE
	[dbo].[t_KDT_VNACCS_Careeries]
SET
	[LoaiNganhNghe] = @LoaiNganhNghe,
	[ChuKySanXuat] = @ChuKySanXuat,
	[ManufactureFactory_ID] = @ManufactureFactory_ID
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_InsertUpdate]
	@ID bigint,
	@LoaiNganhNghe numeric(1, 0),
	@ChuKySanXuat nvarchar(2000),
	@ManufactureFactory_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_Careeries] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_Careeries] 
		SET
			[LoaiNganhNghe] = @LoaiNganhNghe,
			[ChuKySanXuat] = @ChuKySanXuat,
			[ManufactureFactory_ID] = @ManufactureFactory_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_Careeries]
		(
			[LoaiNganhNghe],
			[ChuKySanXuat],
			[ManufactureFactory_ID]
		)
		VALUES 
		(
			@LoaiNganhNghe,
			@ChuKySanXuat,
			@ManufactureFactory_ID
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_DeleteBy_ManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteBy_ManufactureFactory_ID]
	@ManufactureFactory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[ManufactureFactory_ID] = @ManufactureFactory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_Careeries] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiNganhNghe],
	[ChuKySanXuat],
	[ManufactureFactory_ID]
FROM
	[dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_SelectBy_ManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectBy_ManufactureFactory_ID]
	@ManufactureFactory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiNganhNghe],
	[ChuKySanXuat],
	[ManufactureFactory_ID]
FROM
	[dbo].[t_KDT_VNACCS_Careeries]
WHERE
	[ManufactureFactory_ID] = @ManufactureFactory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[LoaiNganhNghe],
	[ChuKySanXuat],
	[ManufactureFactory_ID]
FROM [dbo].[t_KDT_VNACCS_Careeries] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careery_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careery_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[LoaiNganhNghe],
	[ChuKySanXuat],
	[ManufactureFactory_ID]
FROM
	[dbo].[t_KDT_VNACCS_Careeries]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiTruSoChinh nvarchar(255),
	@LoaiDiaChiTruSoChinh numeric(1, 0),
	@NuocDauTu nvarchar(255),
	@NganhNgheSanXuat nvarchar(255),
	@TenDoanhNghiepTKTD nvarchar(255),
	@MaDoanhNghiepTKTD nvarchar(17),
	@LyDoChuyenDoi nvarchar(255),
	@SoCMNDCT nvarchar(17),
	@NgayCapGiayPhepCT datetime,
	@NoiCapGiayPhepCT nvarchar(255),
	@NoiDangKyHKTTCT nvarchar(255),
	@SoDienThoaiCT nvarchar(17),
	@SoCMNDGD nvarchar(17),
	@NgayCapGiayPhepGD datetime,
	@NoiCapGiayPhepGD nvarchar(255),
	@NoiDangKyHKTTGD nvarchar(255),
	@SoDienThoaiGD nvarchar(17),
	@DaDuocCQHQKT numeric(1, 0),
	@BiXuPhatVeBuonLau numeric(1, 0),
	@BiXuPhatVeTronThue numeric(1, 0),
	@BiXuPhatVeKeToan numeric(1, 0),
	@ThoiGianSanXuat numeric(1, 0),
	@SoLuongSanPham numeric(10, 0),
	@BoPhanQuanLy numeric(10, 0),
	@SoLuongCongNhan numeric(10, 0),
	@TenCongTyMe nvarchar(255),
	@MaCongTyMe nvarchar(17),
	@SoLuongThanhVien numeric(10, 0),
	@SoLuongChiNhanh numeric(10, 0),
	@SoLuongThanhVienCTM numeric(10, 0),
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@NgayKetThucNamTC datetime,
	@LoaiHinhDN int,
	@LoaiSua int,
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr],
	[NgayKetThucNamTC],
	[LoaiHinhDN],
	[LoaiSua],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@TenDoanhNghiep,
	@MaDoanhNghiep,
	@DiaChiTruSoChinh,
	@LoaiDiaChiTruSoChinh,
	@NuocDauTu,
	@NganhNgheSanXuat,
	@TenDoanhNghiepTKTD,
	@MaDoanhNghiepTKTD,
	@LyDoChuyenDoi,
	@SoCMNDCT,
	@NgayCapGiayPhepCT,
	@NoiCapGiayPhepCT,
	@NoiDangKyHKTTCT,
	@SoDienThoaiCT,
	@SoCMNDGD,
	@NgayCapGiayPhepGD,
	@NoiCapGiayPhepGD,
	@NoiDangKyHKTTGD,
	@SoDienThoaiGD,
	@DaDuocCQHQKT,
	@BiXuPhatVeBuonLau,
	@BiXuPhatVeTronThue,
	@BiXuPhatVeKeToan,
	@ThoiGianSanXuat,
	@SoLuongSanPham,
	@BoPhanQuanLy,
	@SoLuongCongNhan,
	@TenCongTyMe,
	@MaCongTyMe,
	@SoLuongThanhVien,
	@SoLuongChiNhanh,
	@SoLuongThanhVienCTM,
	@GhiChuKhac,
	@GuidStr,
	@NgayKetThucNamTC,
	@LoaiHinhDN,
	@LoaiSua,
	@SoLuongSoHuu,
	@SoLuongDiThue,
	@SoLuongKhac,
	@TongSoLuong
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiTruSoChinh nvarchar(255),
	@LoaiDiaChiTruSoChinh numeric(1, 0),
	@NuocDauTu nvarchar(255),
	@NganhNgheSanXuat nvarchar(255),
	@TenDoanhNghiepTKTD nvarchar(255),
	@MaDoanhNghiepTKTD nvarchar(17),
	@LyDoChuyenDoi nvarchar(255),
	@SoCMNDCT nvarchar(17),
	@NgayCapGiayPhepCT datetime,
	@NoiCapGiayPhepCT nvarchar(255),
	@NoiDangKyHKTTCT nvarchar(255),
	@SoDienThoaiCT nvarchar(17),
	@SoCMNDGD nvarchar(17),
	@NgayCapGiayPhepGD datetime,
	@NoiCapGiayPhepGD nvarchar(255),
	@NoiDangKyHKTTGD nvarchar(255),
	@SoDienThoaiGD nvarchar(17),
	@DaDuocCQHQKT numeric(1, 0),
	@BiXuPhatVeBuonLau numeric(1, 0),
	@BiXuPhatVeTronThue numeric(1, 0),
	@BiXuPhatVeKeToan numeric(1, 0),
	@ThoiGianSanXuat numeric(1, 0),
	@SoLuongSanPham numeric(10, 0),
	@BoPhanQuanLy numeric(10, 0),
	@SoLuongCongNhan numeric(10, 0),
	@TenCongTyMe nvarchar(255),
	@MaCongTyMe nvarchar(17),
	@SoLuongThanhVien numeric(10, 0),
	@SoLuongChiNhanh numeric(10, 0),
	@SoLuongThanhVienCTM numeric(10, 0),
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@NgayKetThucNamTC datetime,
	@LoaiHinhDN int,
	@LoaiSua int,
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[DiaChiTruSoChinh] = @DiaChiTruSoChinh,
	[LoaiDiaChiTruSoChinh] = @LoaiDiaChiTruSoChinh,
	[NuocDauTu] = @NuocDauTu,
	[NganhNgheSanXuat] = @NganhNgheSanXuat,
	[TenDoanhNghiepTKTD] = @TenDoanhNghiepTKTD,
	[MaDoanhNghiepTKTD] = @MaDoanhNghiepTKTD,
	[LyDoChuyenDoi] = @LyDoChuyenDoi,
	[SoCMNDCT] = @SoCMNDCT,
	[NgayCapGiayPhepCT] = @NgayCapGiayPhepCT,
	[NoiCapGiayPhepCT] = @NoiCapGiayPhepCT,
	[NoiDangKyHKTTCT] = @NoiDangKyHKTTCT,
	[SoDienThoaiCT] = @SoDienThoaiCT,
	[SoCMNDGD] = @SoCMNDGD,
	[NgayCapGiayPhepGD] = @NgayCapGiayPhepGD,
	[NoiCapGiayPhepGD] = @NoiCapGiayPhepGD,
	[NoiDangKyHKTTGD] = @NoiDangKyHKTTGD,
	[SoDienThoaiGD] = @SoDienThoaiGD,
	[DaDuocCQHQKT] = @DaDuocCQHQKT,
	[BiXuPhatVeBuonLau] = @BiXuPhatVeBuonLau,
	[BiXuPhatVeTronThue] = @BiXuPhatVeTronThue,
	[BiXuPhatVeKeToan] = @BiXuPhatVeKeToan,
	[ThoiGianSanXuat] = @ThoiGianSanXuat,
	[SoLuongSanPham] = @SoLuongSanPham,
	[BoPhanQuanLy] = @BoPhanQuanLy,
	[SoLuongCongNhan] = @SoLuongCongNhan,
	[TenCongTyMe] = @TenCongTyMe,
	[MaCongTyMe] = @MaCongTyMe,
	[SoLuongThanhVien] = @SoLuongThanhVien,
	[SoLuongChiNhanh] = @SoLuongChiNhanh,
	[SoLuongThanhVienCTM] = @SoLuongThanhVienCTM,
	[GhiChuKhac] = @GhiChuKhac,
	[GuidStr] = @GuidStr,
	[NgayKetThucNamTC] = @NgayKetThucNamTC,
	[LoaiHinhDN] = @LoaiHinhDN,
	[LoaiSua] = @LoaiSua,
	[SoLuongSoHuu] = @SoLuongSoHuu,
	[SoLuongDiThue] = @SoLuongDiThue,
	[SoLuongKhac] = @SoLuongKhac,
	[TongSoLuong] = @TongSoLuong
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@TenDoanhNghiep nvarchar(255),
	@MaDoanhNghiep nvarchar(17),
	@DiaChiTruSoChinh nvarchar(255),
	@LoaiDiaChiTruSoChinh numeric(1, 0),
	@NuocDauTu nvarchar(255),
	@NganhNgheSanXuat nvarchar(255),
	@TenDoanhNghiepTKTD nvarchar(255),
	@MaDoanhNghiepTKTD nvarchar(17),
	@LyDoChuyenDoi nvarchar(255),
	@SoCMNDCT nvarchar(17),
	@NgayCapGiayPhepCT datetime,
	@NoiCapGiayPhepCT nvarchar(255),
	@NoiDangKyHKTTCT nvarchar(255),
	@SoDienThoaiCT nvarchar(17),
	@SoCMNDGD nvarchar(17),
	@NgayCapGiayPhepGD datetime,
	@NoiCapGiayPhepGD nvarchar(255),
	@NoiDangKyHKTTGD nvarchar(255),
	@SoDienThoaiGD nvarchar(17),
	@DaDuocCQHQKT numeric(1, 0),
	@BiXuPhatVeBuonLau numeric(1, 0),
	@BiXuPhatVeTronThue numeric(1, 0),
	@BiXuPhatVeKeToan numeric(1, 0),
	@ThoiGianSanXuat numeric(1, 0),
	@SoLuongSanPham numeric(10, 0),
	@BoPhanQuanLy numeric(10, 0),
	@SoLuongCongNhan numeric(10, 0),
	@TenCongTyMe nvarchar(255),
	@MaCongTyMe nvarchar(17),
	@SoLuongThanhVien numeric(10, 0),
	@SoLuongChiNhanh numeric(10, 0),
	@SoLuongThanhVienCTM numeric(10, 0),
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@NgayKetThucNamTC datetime,
	@LoaiHinhDN int,
	@LoaiSua int,
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_StorageAreasProduction] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[DiaChiTruSoChinh] = @DiaChiTruSoChinh,
			[LoaiDiaChiTruSoChinh] = @LoaiDiaChiTruSoChinh,
			[NuocDauTu] = @NuocDauTu,
			[NganhNgheSanXuat] = @NganhNgheSanXuat,
			[TenDoanhNghiepTKTD] = @TenDoanhNghiepTKTD,
			[MaDoanhNghiepTKTD] = @MaDoanhNghiepTKTD,
			[LyDoChuyenDoi] = @LyDoChuyenDoi,
			[SoCMNDCT] = @SoCMNDCT,
			[NgayCapGiayPhepCT] = @NgayCapGiayPhepCT,
			[NoiCapGiayPhepCT] = @NoiCapGiayPhepCT,
			[NoiDangKyHKTTCT] = @NoiDangKyHKTTCT,
			[SoDienThoaiCT] = @SoDienThoaiCT,
			[SoCMNDGD] = @SoCMNDGD,
			[NgayCapGiayPhepGD] = @NgayCapGiayPhepGD,
			[NoiCapGiayPhepGD] = @NoiCapGiayPhepGD,
			[NoiDangKyHKTTGD] = @NoiDangKyHKTTGD,
			[SoDienThoaiGD] = @SoDienThoaiGD,
			[DaDuocCQHQKT] = @DaDuocCQHQKT,
			[BiXuPhatVeBuonLau] = @BiXuPhatVeBuonLau,
			[BiXuPhatVeTronThue] = @BiXuPhatVeTronThue,
			[BiXuPhatVeKeToan] = @BiXuPhatVeKeToan,
			[ThoiGianSanXuat] = @ThoiGianSanXuat,
			[SoLuongSanPham] = @SoLuongSanPham,
			[BoPhanQuanLy] = @BoPhanQuanLy,
			[SoLuongCongNhan] = @SoLuongCongNhan,
			[TenCongTyMe] = @TenCongTyMe,
			[MaCongTyMe] = @MaCongTyMe,
			[SoLuongThanhVien] = @SoLuongThanhVien,
			[SoLuongChiNhanh] = @SoLuongChiNhanh,
			[SoLuongThanhVienCTM] = @SoLuongThanhVienCTM,
			[GhiChuKhac] = @GhiChuKhac,
			[GuidStr] = @GuidStr,
			[NgayKetThucNamTC] = @NgayKetThucNamTC,
			[LoaiHinhDN] = @LoaiHinhDN,
			[LoaiSua] = @LoaiSua,
			[SoLuongSoHuu] = @SoLuongSoHuu,
			[SoLuongDiThue] = @SoLuongDiThue,
			[SoLuongKhac] = @SoLuongKhac,
			[TongSoLuong] = @TongSoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[TenDoanhNghiep],
			[MaDoanhNghiep],
			[DiaChiTruSoChinh],
			[LoaiDiaChiTruSoChinh],
			[NuocDauTu],
			[NganhNgheSanXuat],
			[TenDoanhNghiepTKTD],
			[MaDoanhNghiepTKTD],
			[LyDoChuyenDoi],
			[SoCMNDCT],
			[NgayCapGiayPhepCT],
			[NoiCapGiayPhepCT],
			[NoiDangKyHKTTCT],
			[SoDienThoaiCT],
			[SoCMNDGD],
			[NgayCapGiayPhepGD],
			[NoiCapGiayPhepGD],
			[NoiDangKyHKTTGD],
			[SoDienThoaiGD],
			[DaDuocCQHQKT],
			[BiXuPhatVeBuonLau],
			[BiXuPhatVeTronThue],
			[BiXuPhatVeKeToan],
			[ThoiGianSanXuat],
			[SoLuongSanPham],
			[BoPhanQuanLy],
			[SoLuongCongNhan],
			[TenCongTyMe],
			[MaCongTyMe],
			[SoLuongThanhVien],
			[SoLuongChiNhanh],
			[SoLuongThanhVienCTM],
			[GhiChuKhac],
			[GuidStr],
			[NgayKetThucNamTC],
			[LoaiHinhDN],
			[LoaiSua],
			[SoLuongSoHuu],
			[SoLuongDiThue],
			[SoLuongKhac],
			[TongSoLuong]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@TenDoanhNghiep,
			@MaDoanhNghiep,
			@DiaChiTruSoChinh,
			@LoaiDiaChiTruSoChinh,
			@NuocDauTu,
			@NganhNgheSanXuat,
			@TenDoanhNghiepTKTD,
			@MaDoanhNghiepTKTD,
			@LyDoChuyenDoi,
			@SoCMNDCT,
			@NgayCapGiayPhepCT,
			@NoiCapGiayPhepCT,
			@NoiDangKyHKTTCT,
			@SoDienThoaiCT,
			@SoCMNDGD,
			@NgayCapGiayPhepGD,
			@NoiCapGiayPhepGD,
			@NoiDangKyHKTTGD,
			@SoDienThoaiGD,
			@DaDuocCQHQKT,
			@BiXuPhatVeBuonLau,
			@BiXuPhatVeTronThue,
			@BiXuPhatVeKeToan,
			@ThoiGianSanXuat,
			@SoLuongSanPham,
			@BoPhanQuanLy,
			@SoLuongCongNhan,
			@TenCongTyMe,
			@MaCongTyMe,
			@SoLuongThanhVien,
			@SoLuongChiNhanh,
			@SoLuongThanhVienCTM,
			@GhiChuKhac,
			@GuidStr,
			@NgayKetThucNamTC,
			@LoaiHinhDN,
			@LoaiSua,
			@SoLuongSoHuu,
			@SoLuongDiThue,
			@SoLuongKhac,
			@TongSoLuong
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr],
	[NgayKetThucNamTC],
	[LoaiHinhDN],
	[LoaiSua],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr],
	[NgayKetThucNamTC],
	[LoaiHinhDN],
	[LoaiSua],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong]
FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_SelectAll]














































AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[TenDoanhNghiep],
	[MaDoanhNghiep],
	[DiaChiTruSoChinh],
	[LoaiDiaChiTruSoChinh],
	[NuocDauTu],
	[NganhNgheSanXuat],
	[TenDoanhNghiepTKTD],
	[MaDoanhNghiepTKTD],
	[LyDoChuyenDoi],
	[SoCMNDCT],
	[NgayCapGiayPhepCT],
	[NoiCapGiayPhepCT],
	[NoiDangKyHKTTCT],
	[SoDienThoaiCT],
	[SoCMNDGD],
	[NgayCapGiayPhepGD],
	[NoiCapGiayPhepGD],
	[NoiDangKyHKTTGD],
	[SoDienThoaiGD],
	[DaDuocCQHQKT],
	[BiXuPhatVeBuonLau],
	[BiXuPhatVeTronThue],
	[BiXuPhatVeKeToan],
	[ThoiGianSanXuat],
	[SoLuongSanPham],
	[BoPhanQuanLy],
	[SoLuongCongNhan],
	[TenCongTyMe],
	[MaCongTyMe],
	[SoLuongThanhVien],
	[SoLuongChiNhanh],
	[SoLuongThanhVienCTM],
	[GhiChuKhac],
	[GuidStr],
	[NgayKetThucNamTC],
	[LoaiHinhDN],
	[LoaiSua],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]
	@StorageAreasProduction_ID bigint,
	@LoaiCSSX numeric(1, 0),
	@DiaChiCSSX nvarchar(255),
	@LoaiDiaChiCSSX numeric(1, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSanXuat nvarchar(2000),
	@DienTichNhaXuong numeric(20, 4),
	@SoLuongCongNhan numeric(10, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ManufactureFactory]
(
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat],
	[DienTichNhaXuong],
	[SoLuongCongNhan]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@LoaiCSSX,
	@DiaChiCSSX,
	@LoaiDiaChiCSSX,
	@SoLuongSoHuu,
	@SoLuongDiThue,
	@SoLuongKhac,
	@TongSoLuong,
	@NangLucSanXuat,
	@DienTichNhaXuong,
	@SoLuongCongNhan
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiCSSX numeric(1, 0),
	@DiaChiCSSX nvarchar(255),
	@LoaiDiaChiCSSX numeric(1, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSanXuat nvarchar(2000),
	@DienTichNhaXuong numeric(20, 4),
	@SoLuongCongNhan numeric(10, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[LoaiCSSX] = @LoaiCSSX,
	[DiaChiCSSX] = @DiaChiCSSX,
	[LoaiDiaChiCSSX] = @LoaiDiaChiCSSX,
	[SoLuongSoHuu] = @SoLuongSoHuu,
	[SoLuongDiThue] = @SoLuongDiThue,
	[SoLuongKhac] = @SoLuongKhac,
	[TongSoLuong] = @TongSoLuong,
	[NangLucSanXuat] = @NangLucSanXuat,
	[DienTichNhaXuong] = @DienTichNhaXuong,
	[SoLuongCongNhan] = @SoLuongCongNhan
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@LoaiCSSX numeric(1, 0),
	@DiaChiCSSX nvarchar(255),
	@LoaiDiaChiCSSX numeric(1, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSanXuat nvarchar(2000),
	@DienTichNhaXuong numeric(20, 4),
	@SoLuongCongNhan numeric(10, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ManufactureFactory] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ManufactureFactory] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[LoaiCSSX] = @LoaiCSSX,
			[DiaChiCSSX] = @DiaChiCSSX,
			[LoaiDiaChiCSSX] = @LoaiDiaChiCSSX,
			[SoLuongSoHuu] = @SoLuongSoHuu,
			[SoLuongDiThue] = @SoLuongDiThue,
			[SoLuongKhac] = @SoLuongKhac,
			[TongSoLuong] = @TongSoLuong,
			[NangLucSanXuat] = @NangLucSanXuat,
			[DienTichNhaXuong] = @DienTichNhaXuong,
			[SoLuongCongNhan] = @SoLuongCongNhan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ManufactureFactory]
		(
			[StorageAreasProduction_ID],
			[LoaiCSSX],
			[DiaChiCSSX],
			[LoaiDiaChiCSSX],
			[SoLuongSoHuu],
			[SoLuongDiThue],
			[SoLuongKhac],
			[TongSoLuong],
			[NangLucSanXuat],
			[DienTichNhaXuong],
			[SoLuongCongNhan]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@LoaiCSSX,
			@DiaChiCSSX,
			@LoaiDiaChiCSSX,
			@SoLuongSoHuu,
			@SoLuongDiThue,
			@SoLuongKhac,
			@TongSoLuong,
			@NangLucSanXuat,
			@DienTichNhaXuong,
			@SoLuongCongNhan
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ManufactureFactory] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat],
	[DienTichNhaXuong],
	[SoLuongCongNhan]
FROM
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat],
	[DienTichNhaXuong],
	[SoLuongCongNhan]
FROM
	[dbo].[t_KDT_VNACCS_ManufactureFactory]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat],
	[DienTichNhaXuong],
	[SoLuongCongNhan]
FROM [dbo].[t_KDT_VNACCS_ManufactureFactory] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]












AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[LoaiCSSX],
	[DiaChiCSSX],
	[LoaiDiaChiCSSX],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSanXuat],
	[DienTichNhaXuong],
	[SoLuongCongNhan]
FROM
	[dbo].[t_KDT_VNACCS_ManufactureFactory]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageOfGood_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_DeleteBy_StorageAreasProduction_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Insert]
	@StorageAreasProduction_ID bigint,
	@Ten nvarchar(255),
	@Ma nvarchar(7),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_StorageOfGoods]
(
	[StorageAreasProduction_ID],
	[Ten],
	[Ma]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@Ten,
	@Ma
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@Ten nvarchar(255),
	@Ma nvarchar(7)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_StorageOfGoods]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[Ten] = @Ten,
	[Ma] = @Ma
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@Ten nvarchar(255),
	@Ma nvarchar(7)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_StorageOfGoods] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_StorageOfGoods] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[Ten] = @Ten,
			[Ma] = @Ma
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_StorageOfGoods]
		(
			[StorageAreasProduction_ID],
			[Ten],
			[Ma]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@Ten,
			@Ma
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_StorageOfGoods]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_StorageOfGoods]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_StorageOfGoods] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[Ten],
	[Ma]
FROM
	[dbo].[t_KDT_VNACCS_StorageOfGoods]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[Ten],
	[Ma]
FROM
	[dbo].[t_KDT_VNACCS_StorageOfGoods]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[Ten],
	[Ma]
FROM [dbo].[t_KDT_VNACCS_StorageOfGoods] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageOfGood_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageOfGood_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[Ten],
	[Ma]
FROM
	[dbo].[t_KDT_VNACCS_StorageOfGoods]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_SelectBy_Careeries_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_SelectBy_Careeries_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_Careeries_Product_DeleteBy_Careeries_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_DeleteBy_Careeries_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Insert]
	@Careeries_ID bigint,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@ChuKySXTG numeric(5, 0),
	@ChuKySXDVT int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_Careeries_Product]
(
	[Careeries_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
)
VALUES 
(
	@Careeries_ID,
	@MaSP,
	@MaHS,
	@ChuKySXTG,
	@ChuKySXDVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Update]
	@ID bigint,
	@Careeries_ID bigint,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@ChuKySXTG numeric(5, 0),
	@ChuKySXDVT int
AS

UPDATE
	[dbo].[t_KDT_VNACCS_Careeries_Product]
SET
	[Careeries_ID] = @Careeries_ID,
	[MaSP] = @MaSP,
	[MaHS] = @MaHS,
	[ChuKySXTG] = @ChuKySXTG,
	[ChuKySXDVT] = @ChuKySXDVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_InsertUpdate]
	@ID bigint,
	@Careeries_ID bigint,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@ChuKySXTG numeric(5, 0),
	@ChuKySXDVT int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_Careeries_Product] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_Careeries_Product] 
		SET
			[Careeries_ID] = @Careeries_ID,
			[MaSP] = @MaSP,
			[MaHS] = @MaHS,
			[ChuKySXTG] = @ChuKySXTG,
			[ChuKySXDVT] = @ChuKySXDVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_Careeries_Product]
		(
			[Careeries_ID],
			[MaSP],
			[MaHS],
			[ChuKySXTG],
			[ChuKySXDVT]
		)
		VALUES 
		(
			@Careeries_ID,
			@MaSP,
			@MaHS,
			@ChuKySXTG,
			@ChuKySXDVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_Careeries_Product]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_DeleteBy_Careeries_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_DeleteBy_Careeries_ID]
	@Careeries_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_Careeries_Product]
WHERE
	[Careeries_ID] = @Careeries_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_Careeries_Product] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Careeries_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM
	[dbo].[t_KDT_VNACCS_Careeries_Product]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_SelectBy_Careeries_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_SelectBy_Careeries_ID]
	@Careeries_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Careeries_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM
	[dbo].[t_KDT_VNACCS_Careeries_Product]
WHERE
	[Careeries_ID] = @Careeries_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Careeries_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM [dbo].[t_KDT_VNACCS_Careeries_Product] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_Careeries_Product_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_Careeries_Product_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Careeries_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM
	[dbo].[t_KDT_VNACCS_Careeries_Product]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectBy_Careeries_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectBy_Careeries_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteBy_Careeries_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteBy_Careeries_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Insert]
	@Careeries_ID bigint,
	@ThoiGianSXTG numeric(5, 0),
	@ThoiGianSXDVT int,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@DVT nvarchar(4),
	@SoLuong numeric(10, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
(
	[Careeries_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
)
VALUES 
(
	@Careeries_ID,
	@ThoiGianSXTG,
	@ThoiGianSXDVT,
	@MaSP,
	@MaHS,
	@DVT,
	@SoLuong
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Update]
	@ID bigint,
	@Careeries_ID bigint,
	@ThoiGianSXTG numeric(5, 0),
	@ThoiGianSXDVT int,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@DVT nvarchar(4),
	@SoLuong numeric(10, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
SET
	[Careeries_ID] = @Careeries_ID,
	[ThoiGianSXTG] = @ThoiGianSXTG,
	[ThoiGianSXDVT] = @ThoiGianSXDVT,
	[MaSP] = @MaSP,
	[MaHS] = @MaHS,
	[DVT] = @DVT,
	[SoLuong] = @SoLuong
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_InsertUpdate]
	@ID bigint,
	@Careeries_ID bigint,
	@ThoiGianSXTG numeric(5, 0),
	@ThoiGianSXDVT int,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@DVT nvarchar(4),
	@SoLuong numeric(10, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_ProductionCapacity_Product] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_ProductionCapacity_Product] 
		SET
			[Careeries_ID] = @Careeries_ID,
			[ThoiGianSXTG] = @ThoiGianSXTG,
			[ThoiGianSXDVT] = @ThoiGianSXDVT,
			[MaSP] = @MaSP,
			[MaHS] = @MaHS,
			[DVT] = @DVT,
			[SoLuong] = @SoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
		(
			[Careeries_ID],
			[ThoiGianSXTG],
			[ThoiGianSXDVT],
			[MaSP],
			[MaHS],
			[DVT],
			[SoLuong]
		)
		VALUES 
		(
			@Careeries_ID,
			@ThoiGianSXTG,
			@ThoiGianSXDVT,
			@MaSP,
			@MaHS,
			@DVT,
			@SoLuong
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteBy_Careeries_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteBy_Careeries_ID]
	@Careeries_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
WHERE
	[Careeries_ID] = @Careeries_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_ProductionCapacity_Product] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Careeries_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM
	[dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectBy_Careeries_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectBy_Careeries_ID]
	@Careeries_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Careeries_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM
	[dbo].[t_KDT_VNACCS_ProductionCapacity_Product]
WHERE
	[Careeries_ID] = @Careeries_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Careeries_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM [dbo].[t_KDT_VNACCS_ProductionCapacity_Product] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ProductionCapacity_Product_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Careeries_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM
	[dbo].[t_KDT_VNACCS_ProductionCapacity_Product]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteBy_StorageAreasProduction_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Insert]
	@StorageAreasProduction_ID bigint,
	@TenDoiTac nvarchar(255),
	@MaDoiTac nvarchar(7),
	@DiaChiDoiTac nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
(
	[StorageAreasProduction_ID],
	[TenDoiTac],
	[MaDoiTac],
	[DiaChiDoiTac]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@TenDoiTac,
	@MaDoiTac,
	@DiaChiDoiTac
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoiTac nvarchar(255),
	@MaDoiTac nvarchar(7),
	@DiaChiDoiTac nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[TenDoiTac] = @TenDoiTac,
	[MaDoiTac] = @MaDoiTac,
	[DiaChiDoiTac] = @DiaChiDoiTac
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@TenDoiTac nvarchar(255),
	@MaDoiTac nvarchar(7),
	@DiaChiDoiTac nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[TenDoiTac] = @TenDoiTac,
			[MaDoiTac] = @MaDoiTac,
			[DiaChiDoiTac] = @DiaChiDoiTac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
		(
			[StorageAreasProduction_ID],
			[TenDoiTac],
			[MaDoiTac],
			[DiaChiDoiTac]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@TenDoiTac,
			@MaDoiTac,
			@DiaChiDoiTac
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoiTac],
	[MaDoiTac],
	[DiaChiDoiTac]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoiTac],
	[MaDoiTac],
	[DiaChiDoiTac]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[TenDoiTac],
	[MaDoiTac],
	[DiaChiDoiTac]
FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[TenDoiTac],
	[MaDoiTac],
	[DiaChiDoiTac]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectBy_OutsourcingManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectBy_OutsourcingManufactureFactory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteBy_OutsourcingManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteBy_OutsourcingManufactureFactory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Insert]
	@OutsourcingManufactureFactory_ID bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
(
	[OutsourcingManufactureFactory_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan]
)
VALUES 
(
	@OutsourcingManufactureFactory_ID,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHan
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Update]
	@ID bigint,
	@OutsourcingManufactureFactory_ID bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@NgayHetHan datetime
AS

UPDATE
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
SET
	[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHan] = @NgayHetHan
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_InsertUpdate]
	@ID bigint,
	@OutsourcingManufactureFactory_ID bigint,
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@NgayHetHan datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument] 
		SET
			[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHan] = @NgayHetHan
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
		(
			[OutsourcingManufactureFactory_ID],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHan]
		)
		VALUES 
		(
			@OutsourcingManufactureFactory_ID,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHan
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteBy_OutsourcingManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteBy_OutsourcingManufactureFactory_ID]
	@OutsourcingManufactureFactory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
WHERE
	[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectBy_OutsourcingManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectBy_OutsourcingManufactureFactory_ID]
	@OutsourcingManufactureFactory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]
WHERE
	[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[OutsourcingManufactureFactory_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan]
FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ContractDocument]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectBy_OutsourcingManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectBy_OutsourcingManufactureFactory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteBy_OutsourcingManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteBy_OutsourcingManufactureFactory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Insert]
	@OutsourcingManufactureFactory_ID bigint,
	@DiaChiCSSX nvarchar(255),
	@SoLuongCongNhan numeric(10, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSX nvarchar(200),
	@DienTichNX numeric(20, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
(
	[OutsourcingManufactureFactory_ID],
	[DiaChiCSSX],
	[SoLuongCongNhan],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSX],
	[DienTichNX]
)
VALUES 
(
	@OutsourcingManufactureFactory_ID,
	@DiaChiCSSX,
	@SoLuongCongNhan,
	@SoLuongSoHuu,
	@SoLuongDiThue,
	@SoLuongKhac,
	@TongSoLuong,
	@NangLucSX,
	@DienTichNX
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Update]
	@ID bigint,
	@OutsourcingManufactureFactory_ID bigint,
	@DiaChiCSSX nvarchar(255),
	@SoLuongCongNhan numeric(10, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSX nvarchar(200),
	@DienTichNX numeric(20, 4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
SET
	[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID,
	[DiaChiCSSX] = @DiaChiCSSX,
	[SoLuongCongNhan] = @SoLuongCongNhan,
	[SoLuongSoHuu] = @SoLuongSoHuu,
	[SoLuongDiThue] = @SoLuongDiThue,
	[SoLuongKhac] = @SoLuongKhac,
	[TongSoLuong] = @TongSoLuong,
	[NangLucSX] = @NangLucSX,
	[DienTichNX] = @DienTichNX
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_InsertUpdate]
	@ID bigint,
	@OutsourcingManufactureFactory_ID bigint,
	@DiaChiCSSX nvarchar(255),
	@SoLuongCongNhan numeric(10, 0),
	@SoLuongSoHuu numeric(10, 0),
	@SoLuongDiThue numeric(10, 0),
	@SoLuongKhac numeric(10, 0),
	@TongSoLuong numeric(10, 0),
	@NangLucSX nvarchar(200),
	@DienTichNX numeric(20, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory] 
		SET
			[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID,
			[DiaChiCSSX] = @DiaChiCSSX,
			[SoLuongCongNhan] = @SoLuongCongNhan,
			[SoLuongSoHuu] = @SoLuongSoHuu,
			[SoLuongDiThue] = @SoLuongDiThue,
			[SoLuongKhac] = @SoLuongKhac,
			[TongSoLuong] = @TongSoLuong,
			[NangLucSX] = @NangLucSX,
			[DienTichNX] = @DienTichNX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
		(
			[OutsourcingManufactureFactory_ID],
			[DiaChiCSSX],
			[SoLuongCongNhan],
			[SoLuongSoHuu],
			[SoLuongDiThue],
			[SoLuongKhac],
			[TongSoLuong],
			[NangLucSX],
			[DienTichNX]
		)
		VALUES 
		(
			@OutsourcingManufactureFactory_ID,
			@DiaChiCSSX,
			@SoLuongCongNhan,
			@SoLuongSoHuu,
			@SoLuongDiThue,
			@SoLuongKhac,
			@TongSoLuong,
			@NangLucSX,
			@DienTichNX
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteBy_OutsourcingManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteBy_OutsourcingManufactureFactory_ID]
	@OutsourcingManufactureFactory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
WHERE
	[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ID],
	[DiaChiCSSX],
	[SoLuongCongNhan],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSX],
	[DienTichNX]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectBy_OutsourcingManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectBy_OutsourcingManufactureFactory_ID]
	@OutsourcingManufactureFactory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ID],
	[DiaChiCSSX],
	[SoLuongCongNhan],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSX],
	[DienTichNX]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]
WHERE
	[OutsourcingManufactureFactory_ID] = @OutsourcingManufactureFactory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[OutsourcingManufactureFactory_ID],
	[DiaChiCSSX],
	[SoLuongCongNhan],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSX],
	[DienTichNX]
FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory_SelectAll]










AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ID],
	[DiaChiCSSX],
	[SoLuongCongNhan],
	[SoLuongSoHuu],
	[SoLuongDiThue],
	[SoLuongKhac],
	[TongSoLuong],
	[NangLucSX],
	[DienTichNX]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ManufactureFactory]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Insert]
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@ChuKySXTG numeric(5, 0),
	@ChuKySXDVT int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
(
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
)
VALUES 
(
	@OutsourcingManufactureFactory_ManufactureFactory_ID,
	@MaSP,
	@MaHS,
	@ChuKySXTG,
	@ChuKySXDVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Update]
	@ID bigint,
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@ChuKySXTG numeric(5, 0),
	@ChuKySXDVT int
AS

UPDATE
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
SET
	[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID,
	[MaSP] = @MaSP,
	[MaHS] = @MaHS,
	[ChuKySXTG] = @ChuKySXTG,
	[ChuKySXDVT] = @ChuKySXDVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_InsertUpdate]
	@ID bigint,
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@ChuKySXTG numeric(5, 0),
	@ChuKySXDVT int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product] 
		SET
			[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID,
			[MaSP] = @MaSP,
			[MaHS] = @MaHS,
			[ChuKySXTG] = @ChuKySXTG,
			[ChuKySXDVT] = @ChuKySXDVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
		(
			[OutsourcingManufactureFactory_ManufactureFactory_ID],
			[MaSP],
			[MaHS],
			[ChuKySXTG],
			[ChuKySXDVT]
		)
		VALUES 
		(
			@OutsourcingManufactureFactory_ManufactureFactory_ID,
			@MaSP,
			@MaHS,
			@ChuKySXTG,
			@ChuKySXDVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
WHERE
	[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]
WHERE
	[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_Product_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[MaSP],
	[MaHS],
	[ChuKySXTG],
	[ChuKySXDVT]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_Product]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactoryProductionCapacityProduct_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactoryProductionCapacityProduct_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_OutsourcingManufactureFactoryProductionCapacityProduct_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactoryProductionCapacityProduct_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Insert]
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint,
	@ThoiGianSXTG numeric(5, 0),
	@ThoiGianSXDVT int,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@DVT nvarchar(4),
	@SoLuong numeric(10, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
(
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
)
VALUES 
(
	@OutsourcingManufactureFactory_ManufactureFactory_ID,
	@ThoiGianSXTG,
	@ThoiGianSXDVT,
	@MaSP,
	@MaHS,
	@DVT,
	@SoLuong
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Update]
	@ID bigint,
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint,
	@ThoiGianSXTG numeric(5, 0),
	@ThoiGianSXDVT int,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@DVT nvarchar(4),
	@SoLuong numeric(10, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
SET
	[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID,
	[ThoiGianSXTG] = @ThoiGianSXTG,
	[ThoiGianSXDVT] = @ThoiGianSXDVT,
	[MaSP] = @MaSP,
	[MaHS] = @MaHS,
	[DVT] = @DVT,
	[SoLuong] = @SoLuong
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_InsertUpdate]
	@ID bigint,
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint,
	@ThoiGianSXTG numeric(5, 0),
	@ThoiGianSXDVT int,
	@MaSP nvarchar(50),
	@MaHS nvarchar(12),
	@DVT nvarchar(4),
	@SoLuong numeric(10, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product] 
		SET
			[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID,
			[ThoiGianSXTG] = @ThoiGianSXTG,
			[ThoiGianSXDVT] = @ThoiGianSXDVT,
			[MaSP] = @MaSP,
			[MaHS] = @MaHS,
			[DVT] = @DVT,
			[SoLuong] = @SoLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
		(
			[OutsourcingManufactureFactory_ManufactureFactory_ID],
			[ThoiGianSXTG],
			[ThoiGianSXDVT],
			[MaSP],
			[MaHS],
			[DVT],
			[SoLuong]
		)
		VALUES 
		(
			@OutsourcingManufactureFactory_ManufactureFactory_ID,
			@ThoiGianSXTG,
			@ThoiGianSXDVT,
			@MaSP,
			@MaHS,
			@DVT,
			@SoLuong
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactoryProductionCapacityProduct_DeleteBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
WHERE
	[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactoryProductionCapacityProduct_SelectBy_OutsourcingManufactureFactory_ManufactureFactory_ID]
	@OutsourcingManufactureFactory_ManufactureFactory_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]
WHERE
	[OutsourcingManufactureFactory_ManufactureFactory_ID] = @OutsourcingManufactureFactory_ManufactureFactory_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM [dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[OutsourcingManufactureFactory_ManufactureFactory_ID],
	[ThoiGianSXTG],
	[ThoiGianSXDVT],
	[MaSP],
	[MaHS],
	[DVT],
	[SoLuong]
FROM
	[dbo].[t_KDT_VNACCS_OutsourcingManufactureFactory_ProductionCapacity_Product]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Insert]
	@StorageAreasProduction_ID bigint,
	@FileName nvarchar(25),
	@FileSize bigint,
	@Content nvarchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
(
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
)
VALUES 
(
	@StorageAreasProduction_ID,
	@FileName,
	@FileSize,
	@Content
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Update]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@FileName nvarchar(25),
	@FileSize bigint,
	@Content nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
SET
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
	[FileName] = @FileName,
	[FileSize] = @FileSize,
	[Content] = @Content
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_InsertUpdate]
	@ID bigint,
	@StorageAreasProduction_ID bigint,
	@FileName nvarchar(25),
	@FileSize bigint,
	@Content nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] 
		SET
			[StorageAreasProduction_ID] = @StorageAreasProduction_ID,
			[FileName] = @FileName,
			[FileSize] = @FileSize,
			[Content] = @Content
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
		(
			[StorageAreasProduction_ID],
			[FileName],
			[FileSize],
			[Content]
		)
		VALUES 
		(
			@StorageAreasProduction_ID,
			@FileName,
			@FileSize,
			@Content
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectBy_StorageAreasProduction_ID]
	@StorageAreasProduction_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]
WHERE
	[StorageAreasProduction_ID] = @StorageAreasProduction_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM [dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_StorageAreasProduction_AttachedFile_SelectAll]





AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[StorageAreasProduction_ID],
	[FileName],
	[FileSize],
	[Content]
FROM
	[dbo].[t_KDT_VNACCS_StorageAreasProduction_AttachedFiles]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '27.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('27.8',GETDATE(), N'CẬP NHẬT KHAI BÁO CƠ SỞ SẢN XUẤT')
END