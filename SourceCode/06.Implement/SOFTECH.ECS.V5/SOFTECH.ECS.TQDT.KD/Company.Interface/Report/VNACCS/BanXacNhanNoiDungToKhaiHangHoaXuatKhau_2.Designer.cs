namespace Company.Interface.Report.VNACCS
{
    partial class BanXacNhanNoiDungToKhaiHangHoaXuatKhau_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiachixephang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTendiachixephang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxephang5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxephang4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxephang2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxephang3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaxephang1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSTT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSocontainer5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DetailReport1 = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail2 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaychithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoidungchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblPhanloai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaitamnhaptaixuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaikiemtra = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTencoquanhaiquantiepnhantokhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaydangky = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoihantainhapxuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiodangky = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSotokhaidautien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuthitruonghophethan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSonhanhtokhaichianho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongsotokhaichianho = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMasothuedaidien = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaloaihinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaphanloaihanghoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMahieuphuongthucvanchuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNhomxulyhoso = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgaythaydoidangky = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiothaydoidangky = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel24,
            this.lblDiachixephang,
            this.lblTendiachixephang,
            this.lblMaxephang5,
            this.xrLabel30,
            this.lblMaxephang4,
            this.xrLabel27,
            this.lblMaxephang2,
            this.xrLabel25,
            this.lblMaxephang3,
            this.lblMaxephang1,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel16,
            this.xrLabel17});
            this.Detail.HeightF = 82.27194F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 71.27193F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(72.50002F, 11.00001F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.Text = "Số container";
            // 
            // lblDiachixephang
            // 
            this.lblDiachixephang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiachixephang.LocationFloat = new DevExpress.Utils.PointFloat(72.50002F, 44.00002F);
            this.lblDiachixephang.Multiline = true;
            this.lblDiachixephang.Name = "lblDiachixephang";
            this.lblDiachixephang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiachixephang.SizeF = new System.Drawing.SizeF(640.5F, 26.97535F);
            this.lblDiachixephang.StylePriority.UseFont = false;
            this.lblDiachixephang.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            // 
            // lblTendiachixephang
            // 
            this.lblTendiachixephang.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTendiachixephang.LocationFloat = new DevExpress.Utils.PointFloat(72.49998F, 33.00002F);
            this.lblTendiachixephang.Multiline = true;
            this.lblTendiachixephang.Name = "lblTendiachixephang";
            this.lblTendiachixephang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTendiachixephang.SizeF = new System.Drawing.SizeF(640.5F, 11F);
            this.lblTendiachixephang.StylePriority.UseFont = false;
            this.lblTendiachixephang.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6";
            // 
            // lblMaxephang5
            // 
            this.lblMaxephang5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxephang5.LocationFloat = new DevExpress.Utils.PointFloat(399.9943F, 22.00001F);
            this.lblMaxephang5.Name = "lblMaxephang5";
            this.lblMaxephang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxephang5.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.lblMaxephang5.StylePriority.UseFont = false;
            this.lblMaxephang5.Text = "XXXE";
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(384.5796F, 22.00001F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(15.4148F, 11F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.Text = "5";
            // 
            // lblMaxephang4
            // 
            this.lblMaxephang4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxephang4.LocationFloat = new DevExpress.Utils.PointFloat(322.2198F, 22.00001F);
            this.lblMaxephang4.Name = "lblMaxephang4";
            this.lblMaxephang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxephang4.SizeF = new System.Drawing.SizeF(62.35983F, 11.00001F);
            this.lblMaxephang4.StylePriority.UseFont = false;
            this.lblMaxephang4.Text = "XXXE";
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(228.8902F, 22.00001F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(15.4148F, 11F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.Text = "3";
            // 
            // lblMaxephang2
            // 
            this.lblMaxephang2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxephang2.LocationFloat = new DevExpress.Utils.PointFloat(166.3902F, 22.00001F);
            this.lblMaxephang2.Name = "lblMaxephang2";
            this.lblMaxephang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxephang2.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.lblMaxephang2.StylePriority.UseFont = false;
            this.lblMaxephang2.Text = "XXXE";
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(306.8049F, 22.00001F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(15.4148F, 11F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.Text = "4";
            // 
            // lblMaxephang3
            // 
            this.lblMaxephang3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxephang3.LocationFloat = new DevExpress.Utils.PointFloat(244.3049F, 22.00001F);
            this.lblMaxephang3.Name = "lblMaxephang3";
            this.lblMaxephang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxephang3.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.lblMaxephang3.StylePriority.UseFont = false;
            this.lblMaxephang3.Text = "XXXE";
            // 
            // lblMaxephang1
            // 
            this.lblMaxephang1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaxephang1.LocationFloat = new DevExpress.Utils.PointFloat(87.91478F, 22.00001F);
            this.lblMaxephang1.Name = "lblMaxephang1";
            this.lblMaxephang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaxephang1.SizeF = new System.Drawing.SizeF(63.06058F, 11.00001F);
            this.lblMaxephang1.StylePriority.UseFont = false;
            this.lblMaxephang1.Text = "XXXE";
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(150.9754F, 22.00001F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(15.4148F, 11F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.Text = "2";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(72.49998F, 22.00001F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(15.4148F, 11F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "1";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 44.00002F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.Text = "Địa chỉ";
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 33.00002F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.Text = "Tên";
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 22.00001F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.Text = "Mã";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(62.50001F, 11F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.Text = "Vanning";
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(9.999993F, 11F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(190F, 11F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.Text = "Địa điểm xếp hàng lên xe chở hàng";
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTongsotokhaichianho,
            this.xrLabel12,
            this.xrLabel14,
            this.xrLabel13,
            this.lblBieuthitruonghophethan,
            this.xrLabel10,
            this.lblSonhanhtokhaichianho,
            this.xrLabel11,
            this.xrLabel15,
            this.lblNgaythaydoidangky,
            this.lblNhomxulyhoso,
            this.xrLine3,
            this.lblGiothaydoidangky,
            this.lblMaloaihinh,
            this.lblMasothuedaidien,
            this.lblMahieuphuongthucvanchuyen,
            this.lblMaphanloaihanghoa,
            this.xrLabel4,
            this.lblSotokhaitamnhaptaixuat,
            this.xrLabel5,
            this.lblMaphanloaikiemtra,
            this.xrLabel2,
            this.xrLabel3,
            this.xrLabel1,
            this.lblSotokhai,
            this.lblTencoquanhaiquantiepnhantokhai,
            this.xrLabel8,
            this.lblGiodangky,
            this.xrLabel9,
            this.lblSotokhaidautien,
            this.lblNgaydangky,
            this.xrLabel6,
            this.lblThoihantainhapxuat,
            this.xrLabel7});
            this.TopMargin.HeightF = 143.7871F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detail1.HeightF = 12.04166F;
            this.Detail1.Name = "Detail1";
            this.Detail1.StylePriority.UseFont = false;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow4});
            this.xrTable1.SizeF = new System.Drawing.SizeF(684.1472F, 11F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT1,
            this.lblSocontainer1,
            this.lblSTT2,
            this.lblSocontainer2,
            this.lblSTT3,
            this.lblSocontainer3,
            this.lblSTT4,
            this.lblSocontainer4,
            this.lblSTT5,
            this.lblSocontainer5});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1;
            // 
            // lblSTT1
            // 
            this.lblSTT1.Name = "lblSTT1";
            this.lblSTT1.StylePriority.UseTextAlignment = false;
            this.lblSTT1.Text = "1";
            this.lblSTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTT1.Weight = 0.18999998046787478;
            // 
            // lblSocontainer1
            // 
            this.lblSocontainer1.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSocontainer1.Name = "lblSocontainer1";
            this.lblSocontainer1.StylePriority.UseFont = false;
            this.lblSocontainer1.StylePriority.UseTextAlignment = false;
            this.lblSocontainer1.Text = "XXXXXXXXX1XE";
            this.lblSocontainer1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSocontainer1.Weight = 1.149999876391834;
            // 
            // lblSTT2
            // 
            this.lblSTT2.Name = "lblSTT2";
            this.lblSTT2.StylePriority.UseTextAlignment = false;
            this.lblSTT2.Text = "2";
            this.lblSTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTT2.Weight = 0.18999999396125317;
            // 
            // lblSocontainer2
            // 
            this.lblSocontainer2.Name = "lblSocontainer2";
            this.lblSocontainer2.StylePriority.UseTextAlignment = false;
            this.lblSocontainer2.Text = "XXXXXXXXX1XE";
            this.lblSocontainer2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSocontainer2.Weight = 1.1499999244627845;
            // 
            // lblSTT3
            // 
            this.lblSTT3.Name = "lblSTT3";
            this.lblSTT3.StylePriority.UseTextAlignment = false;
            this.lblSTT3.Text = "3";
            this.lblSTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTT3.Weight = 0.16474889734397527;
            // 
            // lblSocontainer3
            // 
            this.lblSocontainer3.Name = "lblSocontainer3";
            this.lblSocontainer3.StylePriority.UseTextAlignment = false;
            this.lblSocontainer3.Text = "XXXXXXXXX1XE";
            this.lblSocontainer3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSocontainer3.Weight = 1.14999990801307;
            // 
            // lblSTT4
            // 
            this.lblSTT4.Name = "lblSTT4";
            this.lblSTT4.StylePriority.UseTextAlignment = false;
            this.lblSTT4.Text = "4";
            this.lblSTT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTT4.Weight = 0.25816831356445868;
            // 
            // lblSocontainer4
            // 
            this.lblSocontainer4.Name = "lblSocontainer4";
            this.lblSocontainer4.StylePriority.UseTextAlignment = false;
            this.lblSocontainer4.Text = "XXXXXXXXX1XE";
            this.lblSocontainer4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSocontainer4.Weight = 1.1499999403132397;
            // 
            // lblSTT5
            // 
            this.lblSTT5.Name = "lblSTT5";
            this.lblSTT5.StylePriority.UseTextAlignment = false;
            this.lblSTT5.Text = "5";
            this.lblSTT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTT5.Weight = 0.28855389311333424;
            // 
            // lblSocontainer5
            // 
            this.lblSocontainer5.Name = "lblSocontainer5";
            this.lblSocontainer5.StylePriority.UseTextAlignment = false;
            this.lblSocontainer5.Text = "XXXXXXXXX1XE";
            this.lblSocontainer5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSocontainer5.Weight = 1.1500002988529059;
            // 
            // DetailReport1
            // 
            this.DetailReport1.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail2,
            this.ReportHeader});
            this.DetailReport1.Level = 1;
            this.DetailReport1.Name = "DetailReport1";
            // 
            // Detail2
            // 
            this.Detail2.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.Detail2.HeightF = 51.44882F;
            this.Detail2.Name = "Detail2";
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(713F, 33.33333F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTChiThiHQ,
            this.lblNgaychithihaiquan1,
            this.lblTenchithihaiquan1,
            this.lblNoidungchithihaiquan1});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // lblSTTChiThiHQ
            // 
            this.lblSTTChiThiHQ.Name = "lblSTTChiThiHQ";
            this.lblSTTChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblSTTChiThiHQ.Text = "1";
            this.lblSTTChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTTChiThiHQ.Weight = 0.0849716401675839;
            // 
            // lblNgaychithihaiquan1
            // 
            this.lblNgaychithihaiquan1.Name = "lblNgaychithihaiquan1";
            this.lblNgaychithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNgaychithihaiquan1.Text = "dd/MM/yyyy";
            this.lblNgaychithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaychithihaiquan1.Weight = 0.31827812064556837;
            // 
            // lblTenchithihaiquan1
            // 
            this.lblTenchithihaiquan1.Multiline = true;
            this.lblTenchithihaiquan1.Name = "lblTenchithihaiquan1";
            this.lblTenchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblTenchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblTenchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenchithihaiquan1.Weight = 0.9727978354146275;
            // 
            // lblNoidungchithihaiquan1
            // 
            this.lblNoidungchithihaiquan1.Multiline = true;
            this.lblNoidungchithihaiquan1.Name = "lblNoidungchithihaiquan1";
            this.lblNoidungchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNoidungchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblNoidungchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblNoidungchithihaiquan1.Weight = 1.8943584407220695;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblPhanloai,
            this.xrTable2,
            this.xrLabel23,
            this.xrLine2});
            this.ReportHeader.HeightF = 26.5F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblPhanloai
            // 
            this.lblPhanloai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanloai.LocationFloat = new DevExpress.Utils.PointFloat(98.95834F, 2F);
            this.lblPhanloai.Name = "lblPhanloai";
            this.lblPhanloai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanloai.SizeF = new System.Drawing.SizeF(101.0416F, 11F);
            this.lblPhanloai.StylePriority.UseFont = false;
            this.lblPhanloai.StylePriority.UseTextAlignment = false;
            this.lblPhanloai.Text = "X";
            this.lblPhanloai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable2
            // 
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(18.52519F, 14F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(694.4747F, 11F);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.StylePriority.UseTextAlignment = false;
            this.xrTableCell4.Text = "Ngày";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell4.Weight = 0.31827800268217976;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseTextAlignment = false;
            this.xrTableCell5.Text = "Tên";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell5.Weight = 0.972797856383839;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseTextAlignment = false;
            this.xrTableCell6.Text = "Nội dung";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell6.Weight = 1.8943579318641224;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(0F, 2F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(98.95834F, 10.99999F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.Text = "Chỉ thị của Hải quan";
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(712.9999F, 2F);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 51.5F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(216.6667F, 11F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 39.72603F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(63.54166F, 11F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.Text = "Số tờ khai";
            // 
            // lblSotokhai
            // 
            this.lblSotokhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhai.LocationFloat = new DevExpress.Utils.PointFloat(63.54167F, 39.72603F);
            this.lblSotokhai.Name = "lblSotokhai";
            this.lblSotokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhai.SizeF = new System.Drawing.SizeF(122.9167F, 11F);
            this.lblSotokhai.StylePriority.UseFont = false;
            this.lblSotokhai.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 12.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(492.0833F, 27.16667F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Bản xác nhận nội dung tờ khai hàng hóa xuất khẩu";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSotokhaitamnhaptaixuat
            // 
            this.lblSotokhaitamnhaptaixuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaitamnhaptaixuat.LocationFloat = new DevExpress.Utils.PointFloat(261.875F, 51.5F);
            this.lblSotokhaitamnhaptaixuat.Name = "lblSotokhaitamnhaptaixuat";
            this.lblSotokhaitamnhaptaixuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaitamnhaptaixuat.SizeF = new System.Drawing.SizeF(118.75F, 11F);
            this.lblSotokhaitamnhaptaixuat.StylePriority.UseFont = false;
            this.lblSotokhaitamnhaptaixuat.Text = "NNNNNNNNN1NE";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(0F, 62.5F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(127.0833F, 11F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.Text = "Mã phân loại kiểm tra";
            // 
            // lblMaphanloaikiemtra
            // 
            this.lblMaphanloaikiemtra.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaikiemtra.LocationFloat = new DevExpress.Utils.PointFloat(127.0833F, 62.5F);
            this.lblMaphanloaikiemtra.Name = "lblMaphanloaikiemtra";
            this.lblMaphanloaikiemtra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaikiemtra.SizeF = new System.Drawing.SizeF(42.70833F, 11F);
            this.lblMaphanloaikiemtra.StylePriority.UseFont = false;
            this.lblMaphanloaikiemtra.Text = "XXE ";
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(0.7652168F, 73.49999F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(228.125F, 11F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            // 
            // lblTencoquanhaiquantiepnhantokhai
            // 
            this.lblTencoquanhaiquantiepnhantokhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTencoquanhaiquantiepnhantokhai.LocationFloat = new DevExpress.Utils.PointFloat(228.8902F, 73.49999F);
            this.lblTencoquanhaiquantiepnhantokhai.Name = "lblTencoquanhaiquantiepnhantokhai";
            this.lblTencoquanhaiquantiepnhantokhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTencoquanhaiquantiepnhantokhai.SizeF = new System.Drawing.SizeF(107.2916F, 11F);
            this.lblTencoquanhaiquantiepnhantokhai.StylePriority.UseFont = false;
            this.lblTencoquanhaiquantiepnhantokhai.Text = "XXXXXXXXXE";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 84.49998F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(100F, 10.41668F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.Text = "Ngày đăng ký";
            // 
            // lblNgaydangky
            // 
            this.lblNgaydangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaydangky.LocationFloat = new DevExpress.Utils.PointFloat(101.0416F, 84.49997F);
            this.lblNgaydangky.Name = "lblNgaydangky";
            this.lblNgaydangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaydangky.SizeF = new System.Drawing.SizeF(86.45834F, 10.41669F);
            this.lblNgaydangky.StylePriority.UseFont = false;
            this.lblNgaydangky.Text = "dd/MM/yyyy";
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(0F, 94.91666F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(159.375F, 11F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.Text = "Thời hạn tái nhập/ tái xuất";
            // 
            // lblThoihantainhapxuat
            // 
            this.lblThoihantainhapxuat.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThoihantainhapxuat.LocationFloat = new DevExpress.Utils.PointFloat(159.375F, 94.91666F);
            this.lblThoihantainhapxuat.Name = "lblThoihantainhapxuat";
            this.lblThoihantainhapxuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoihantainhapxuat.SizeF = new System.Drawing.SizeF(79.16667F, 11F);
            this.lblThoihantainhapxuat.StylePriority.UseFont = false;
            this.lblThoihantainhapxuat.Text = "dd/MM/yyyy";
            // 
            // lblGiodangky
            // 
            this.lblGiodangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiodangky.LocationFloat = new DevExpress.Utils.PointFloat(187.5F, 84.49997F);
            this.lblGiodangky.Name = "lblGiodangky";
            this.lblGiodangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiodangky.SizeF = new System.Drawing.SizeF(67.70828F, 10.41669F);
            this.lblGiodangky.StylePriority.UseFont = false;
            this.lblGiodangky.Text = "hh:mm:ss";
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(261.875F, 39.66667F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(113.125F, 11F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.Text = "Số tờ khai đầu tiên";
            // 
            // lblSotokhaidautien
            // 
            this.lblSotokhaidautien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSotokhaidautien.LocationFloat = new DevExpress.Utils.PointFloat(375F, 39.72603F);
            this.lblSotokhaidautien.Name = "lblSotokhaidautien";
            this.lblSotokhaidautien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSotokhaidautien.SizeF = new System.Drawing.SizeF(117.7083F, 11F);
            this.lblSotokhaidautien.StylePriority.UseFont = false;
            this.lblSotokhaidautien.Text = "XXXXXXXXX1XE";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(200F, 62.5F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.Text = "Mã loại hình";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(238.5417F, 94.91666F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(15.62492F, 11F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.Text = "-";
            // 
            // lblBieuthitruonghophethan
            // 
            this.lblBieuthitruonghophethan.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuthitruonghophethan.LocationFloat = new DevExpress.Utils.PointFloat(254.1666F, 94.91666F);
            this.lblBieuthitruonghophethan.Name = "lblBieuthitruonghophethan";
            this.lblBieuthitruonghophethan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuthitruonghophethan.SizeF = new System.Drawing.SizeF(69.79169F, 11F);
            this.lblBieuthitruonghophethan.StylePriority.UseFont = false;
            this.lblBieuthitruonghophethan.StylePriority.UseTextAlignment = false;
            this.lblBieuthitruonghophethan.Text = "X";
            this.lblBieuthitruonghophethan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(492.7083F, 39.72603F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(27.08328F, 11F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "-";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSonhanhtokhaichianho
            // 
            this.lblSonhanhtokhaichianho.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSonhanhtokhaichianho.LocationFloat = new DevExpress.Utils.PointFloat(519.7916F, 39.66667F);
            this.lblSonhanhtokhaichianho.Name = "lblSonhanhtokhaichianho";
            this.lblSonhanhtokhaichianho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSonhanhtokhaichianho.SizeF = new System.Drawing.SizeF(38.95828F, 11F);
            this.lblSonhanhtokhaichianho.StylePriority.UseFont = false;
            this.lblSonhanhtokhaichianho.StylePriority.UseTextAlignment = false;
            this.lblSonhanhtokhaichianho.Text = "NE";
            this.lblSonhanhtokhaichianho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(558.7499F, 39.66667F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(13.5416F, 11F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "/";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongsotokhaichianho
            // 
            this.lblTongsotokhaichianho.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongsotokhaichianho.LocationFloat = new DevExpress.Utils.PointFloat(572.2916F, 39.66667F);
            this.lblTongsotokhaichianho.Name = "lblTongsotokhaichianho";
            this.lblTongsotokhaichianho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongsotokhaichianho.SizeF = new System.Drawing.SizeF(59.375F, 11F);
            this.lblTongsotokhaichianho.StylePriority.UseFont = false;
            this.lblTongsotokhaichianho.StylePriority.UseTextAlignment = false;
            this.lblTongsotokhaichianho.Text = "NE";
            this.lblTongsotokhaichianho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(412.5F, 62.5F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(151.0417F, 11.00001F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.Text = "Mã số thuế đại diện";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(412.5F, 73.49999F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(151.0418F, 11F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.Text = "Mã bộ phận xử lý tờ khai";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(412.5F, 84.49998F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(151.0418F, 10.99999F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.Text = "Ngày thay đổi đăng ký";
            // 
            // lblMasothuedaidien
            // 
            this.lblMasothuedaidien.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMasothuedaidien.LocationFloat = new DevExpress.Utils.PointFloat(563.5417F, 62.5F);
            this.lblMasothuedaidien.Name = "lblMasothuedaidien";
            this.lblMasothuedaidien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMasothuedaidien.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.lblMasothuedaidien.StylePriority.UseFont = false;
            this.lblMasothuedaidien.Text = "XXXE";
            // 
            // lblMaloaihinh
            // 
            this.lblMaloaihinh.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaloaihinh.LocationFloat = new DevExpress.Utils.PointFloat(300F, 62.5F);
            this.lblMaloaihinh.Name = "lblMaloaihinh";
            this.lblMaloaihinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaloaihinh.SizeF = new System.Drawing.SizeF(45.83337F, 11F);
            this.lblMaloaihinh.StylePriority.UseFont = false;
            this.lblMaloaihinh.Text = "XXE";
            // 
            // lblMaphanloaihanghoa
            // 
            this.lblMaphanloaihanghoa.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaphanloaihanghoa.LocationFloat = new DevExpress.Utils.PointFloat(345.8334F, 62.5F);
            this.lblMaphanloaihanghoa.Name = "lblMaphanloaihanghoa";
            this.lblMaphanloaihanghoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaphanloaihanghoa.SizeF = new System.Drawing.SizeF(25.62503F, 11F);
            this.lblMaphanloaihanghoa.StylePriority.UseFont = false;
            this.lblMaphanloaihanghoa.Text = "X";
            // 
            // lblMahieuphuongthucvanchuyen
            // 
            this.lblMahieuphuongthucvanchuyen.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMahieuphuongthucvanchuyen.LocationFloat = new DevExpress.Utils.PointFloat(371.4584F, 62.5F);
            this.lblMahieuphuongthucvanchuyen.Name = "lblMahieuphuongthucvanchuyen";
            this.lblMahieuphuongthucvanchuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMahieuphuongthucvanchuyen.SizeF = new System.Drawing.SizeF(25F, 11F);
            this.lblMahieuphuongthucvanchuyen.StylePriority.UseFont = false;
            this.lblMahieuphuongthucvanchuyen.Text = "X";
            // 
            // lblNhomxulyhoso
            // 
            this.lblNhomxulyhoso.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNhomxulyhoso.LocationFloat = new DevExpress.Utils.PointFloat(563.5417F, 73.49999F);
            this.lblNhomxulyhoso.Name = "lblNhomxulyhoso";
            this.lblNhomxulyhoso.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNhomxulyhoso.SizeF = new System.Drawing.SizeF(100F, 11F);
            this.lblNhomxulyhoso.StylePriority.UseFont = false;
            this.lblNhomxulyhoso.Text = "XE";
            // 
            // lblNgaythaydoidangky
            // 
            this.lblNgaythaydoidangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaythaydoidangky.LocationFloat = new DevExpress.Utils.PointFloat(563.5417F, 84.49998F);
            this.lblNgaythaydoidangky.Name = "lblNgaythaydoidangky";
            this.lblNgaythaydoidangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgaythaydoidangky.SizeF = new System.Drawing.SizeF(77.08319F, 11F);
            this.lblNgaythaydoidangky.StylePriority.UseFont = false;
            this.lblNgaythaydoidangky.Text = "dd/MM/yyyy";
            // 
            // lblGiothaydoidangky
            // 
            this.lblGiothaydoidangky.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiothaydoidangky.LocationFloat = new DevExpress.Utils.PointFloat(640.6248F, 84.49998F);
            this.lblGiothaydoidangky.Name = "lblGiothaydoidangky";
            this.lblGiothaydoidangky.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiothaydoidangky.SizeF = new System.Drawing.SizeF(65.00006F, 11F);
            this.lblGiothaydoidangky.StylePriority.UseFont = false;
            this.lblGiothaydoidangky.Text = "hh:mm:ss";
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(0.0001644266F, 105.9167F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(712.9998F, 3.002419F);
            // 
            // BanXacNhanNoiDungToKhaiHangHoaXuatKhau_2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport,
            this.DetailReport1});
            this.Margins = new System.Drawing.Printing.Margins(69, 68, 144, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lblMaxephang5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lblMaxephang4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel lblMaxephang2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel lblMaxephang3;
        private DevExpress.XtraReports.UI.XRLabel lblMaxephang1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lblDiachixephang;
        private DevExpress.XtraReports.UI.XRLabel lblTendiachixephang;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport1;
        private DevExpress.XtraReports.UI.DetailBand Detail2;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTChiThiHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblTenchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblNoidungchithihaiquan1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT1;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT2;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT3;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer3;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT4;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer4;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT5;
        private DevExpress.XtraReports.UI.XRTableCell lblSocontainer5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lblPhanloai;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaychithihaiquan1;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblTongsotokhaichianho;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel lblBieuthitruonghophethan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblSonhanhtokhaichianho;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lblNgaythaydoidangky;
        private DevExpress.XtraReports.UI.XRLabel lblNhomxulyhoso;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblGiothaydoidangky;
        private DevExpress.XtraReports.UI.XRLabel lblMaloaihinh;
        private DevExpress.XtraReports.UI.XRLabel lblMasothuedaidien;
        private DevExpress.XtraReports.UI.XRLabel lblMahieuphuongthucvanchuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaihanghoa;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaitamnhaptaixuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lblMaphanloaikiemtra;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhai;
        private DevExpress.XtraReports.UI.XRLabel lblTencoquanhaiquantiepnhantokhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lblGiodangky;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSotokhaidautien;
        private DevExpress.XtraReports.UI.XRLabel lblNgaydangky;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lblThoihantainhapxuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
    }
}
