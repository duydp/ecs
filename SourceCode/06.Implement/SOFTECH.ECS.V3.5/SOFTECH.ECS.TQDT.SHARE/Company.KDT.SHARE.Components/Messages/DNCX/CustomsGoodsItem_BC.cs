﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.DNCX
{
    public class CustomsGoodsItem_BC
    {
        /// <summary>
        /// Thông tin hàng
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// Số lượng và đơn vị tính
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure_BC GoodsMeasure { get; set; }
        /// <summary>
        /// Danh sách TKNK/ chứng từ đưa hàng vào trong kỳ
        /// </summary>
        [XmlElement("DeclarationDocument")]
        public List<DeclarationBase> DeclarationDocuments { get; set; }
        /// <summary>
        /// Mã sản phẩm khai báo sửa (NPL)
        /// </summary>
        [XmlElement("preIdentification")]
        public string PreIdentification { get; set; }

        [XmlElement("Reference")]
        public DeclarationBase reference { get; set; }

    }
}
