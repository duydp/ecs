﻿namespace Company.Interface
{
    partial class TKMD_BaoLanhVaTraThueThay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TKMD_BaoLanhVaTraThueThay));
            this.label9 = new System.Windows.Forms.Label();
            this.txtNamPhatHanhHanMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbMaThoiHanNopThue = new Janus.Windows.EditControls.UIComboBox();
            this.txtKyHieu_CT_HanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSo_CT_HanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNganHangTraThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtKyHieu_CT_BaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSo_CT_BaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNganHangBaoLanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamPhatHanhBaoLanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(647, 261);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(12, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(138, 13);
            this.label9.TabIndex = 325;
            this.label9.Text = "Mã ngân hàng trả thuế thay";
            // 
            // txtNamPhatHanhHanMuc
            // 
            this.txtNamPhatHanhHanMuc.DecimalDigits = 0;
            this.txtNamPhatHanhHanMuc.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhHanMuc.FormatString = "#####";
            this.txtNamPhatHanhHanMuc.Location = new System.Drawing.Point(477, 21);
            this.txtNamPhatHanhHanMuc.MaxLength = 4;
            this.txtNamPhatHanhHanMuc.Name = "txtNamPhatHanhHanMuc";
            this.txtNamPhatHanhHanMuc.Size = new System.Drawing.Size(54, 20);
            this.txtNamPhatHanhHanMuc.TabIndex = 3;
            this.txtNamPhatHanhHanMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtNamPhatHanhHanMuc.Value = ((ulong)(0ul));
            this.txtNamPhatHanhHanMuc.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtNamPhatHanhHanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(344, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 13);
            this.label1.TabIndex = 325;
            this.label1.Text = "Năm phát hành hạn mức";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(344, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 325;
            this.label2.Text = "Ký hiệu chứng từ hạn mức";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 325;
            this.label3.Text = "Số chứng từ hạn mức";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 13);
            this.label4.TabIndex = 325;
            this.label4.Text = "Mã xát định thời hạn nộp thuế";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 13);
            this.label5.TabIndex = 325;
            this.label5.Text = "Mã ngân hàng bảo lãnh";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(344, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 325;
            this.label6.Text = "Năm phát hành bảo lãnh";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(344, 63);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(131, 13);
            this.label7.TabIndex = 325;
            this.label7.Text = "Ký hiệu chứng từ bảo lãnh";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(12, 63);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(109, 13);
            this.label8.TabIndex = 325;
            this.label8.Text = "Số chứng từ bảo lãnh";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cbbMaThoiHanNopThue);
            this.uiGroupBox1.Controls.Add(this.txtKyHieu_CT_HanMuc);
            this.uiGroupBox1.Controls.Add(this.txtSo_CT_HanMuc);
            this.uiGroupBox1.Controls.Add(this.txtMaNganHangTraThue);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtNamPhatHanhHanMuc);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(647, 128);
            this.uiGroupBox1.TabIndex = 326;
            this.uiGroupBox1.Text = "Trả thuế thay";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cbbMaThoiHanNopThue
            // 
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "A: Thời hạn cho bảo lãnh cá nhân";
            uiComboBoxItem1.Value = "A";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "B: Thời hạn cho bảo lãnh riêng";
            uiComboBoxItem2.Value = "B";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "C: Thời hạn không có bảo lãnh";
            uiComboBoxItem3.Value = "C";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "D: Thanh toán ngay";
            uiComboBoxItem4.Value = "D";
            this.cbbMaThoiHanNopThue.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbMaThoiHanNopThue.Location = new System.Drawing.Point(164, 91);
            this.cbbMaThoiHanNopThue.Name = "cbbMaThoiHanNopThue";
            this.cbbMaThoiHanNopThue.Size = new System.Drawing.Size(149, 21);
            this.cbbMaThoiHanNopThue.TabIndex = 326;
            this.cbbMaThoiHanNopThue.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtKyHieu_CT_HanMuc
            // 
            this.txtKyHieu_CT_HanMuc.Location = new System.Drawing.Point(477, 55);
            this.txtKyHieu_CT_HanMuc.Name = "txtKyHieu_CT_HanMuc";
            this.txtKyHieu_CT_HanMuc.Size = new System.Drawing.Size(149, 21);
            this.txtKyHieu_CT_HanMuc.TabIndex = 4;
            this.txtKyHieu_CT_HanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSo_CT_HanMuc
            // 
            this.txtSo_CT_HanMuc.Location = new System.Drawing.Point(164, 56);
            this.txtSo_CT_HanMuc.Name = "txtSo_CT_HanMuc";
            this.txtSo_CT_HanMuc.Size = new System.Drawing.Size(149, 21);
            this.txtSo_CT_HanMuc.TabIndex = 1;
            this.txtSo_CT_HanMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNganHangTraThue
            // 
            this.txtMaNganHangTraThue.Location = new System.Drawing.Point(164, 20);
            this.txtMaNganHangTraThue.Name = "txtMaNganHangTraThue";
            this.txtMaNganHangTraThue.Size = new System.Drawing.Size(149, 21);
            this.txtMaNganHangTraThue.TabIndex = 0;
            this.txtMaNganHangTraThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnGhi);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 225);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(647, 36);
            this.uiGroupBox2.TabIndex = 327;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(551, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(476, 6);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(69, 23);
            this.btnGhi.TabIndex = 0;
            this.btnGhi.Text = "Ghi";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtKyHieu_CT_BaoLanh);
            this.uiGroupBox3.Controls.Add(this.txtSo_CT_BaoLanh);
            this.uiGroupBox3.Controls.Add(this.txtMaNganHangBaoLanh);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.txtNamPhatHanhBaoLanh);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 128);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(647, 97);
            this.uiGroupBox3.TabIndex = 328;
            this.uiGroupBox3.Text = "Bảo lãnh thuế";
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtKyHieu_CT_BaoLanh
            // 
            this.txtKyHieu_CT_BaoLanh.Location = new System.Drawing.Point(477, 59);
            this.txtKyHieu_CT_BaoLanh.Name = "txtKyHieu_CT_BaoLanh";
            this.txtKyHieu_CT_BaoLanh.Size = new System.Drawing.Size(149, 21);
            this.txtKyHieu_CT_BaoLanh.TabIndex = 3;
            this.txtKyHieu_CT_BaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSo_CT_BaoLanh
            // 
            this.txtSo_CT_BaoLanh.Location = new System.Drawing.Point(164, 59);
            this.txtSo_CT_BaoLanh.Name = "txtSo_CT_BaoLanh";
            this.txtSo_CT_BaoLanh.Size = new System.Drawing.Size(149, 21);
            this.txtSo_CT_BaoLanh.TabIndex = 1;
            this.txtSo_CT_BaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNganHangBaoLanh
            // 
            this.txtMaNganHangBaoLanh.Location = new System.Drawing.Point(164, 20);
            this.txtMaNganHangBaoLanh.Name = "txtMaNganHangBaoLanh";
            this.txtMaNganHangBaoLanh.Size = new System.Drawing.Size(149, 21);
            this.txtMaNganHangBaoLanh.TabIndex = 0;
            this.txtMaNganHangBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamPhatHanhBaoLanh
            // 
            this.txtNamPhatHanhBaoLanh.DecimalDigits = 0;
            this.txtNamPhatHanhBaoLanh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamPhatHanhBaoLanh.FormatString = "#####";
            this.txtNamPhatHanhBaoLanh.Location = new System.Drawing.Point(477, 20);
            this.txtNamPhatHanhBaoLanh.MaxLength = 4;
            this.txtNamPhatHanhBaoLanh.Name = "txtNamPhatHanhBaoLanh";
            this.txtNamPhatHanhBaoLanh.Size = new System.Drawing.Size(54, 20);
            this.txtNamPhatHanhBaoLanh.TabIndex = 2;
            this.txtNamPhatHanhBaoLanh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtNamPhatHanhBaoLanh.Value = ((ulong)(0ul));
            this.txtNamPhatHanhBaoLanh.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtNamPhatHanhBaoLanh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // TKMD_BaoLanhVaTraThueThay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(647, 261);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TKMD_BaoLanhVaTraThueThay";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin bảo lãnh thuế và trả thuế thay";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhHanMuc;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangTraThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieu_CT_HanMuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtSo_CT_HanMuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtKyHieu_CT_BaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtSo_CT_BaoLanh;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangBaoLanh;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanhBaoLanh;
        private Janus.Windows.EditControls.UIComboBox cbbMaThoiHanNopThue;
    }
}