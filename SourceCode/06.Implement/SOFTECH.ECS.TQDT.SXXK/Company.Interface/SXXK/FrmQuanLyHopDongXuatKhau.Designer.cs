﻿namespace Company.Interface
{
    partial class FrmQuanLyHopDongXuatKhau
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHopDongXuatKhau_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQuanLyHopDongXuatKhau));
            this.lblSoTK = new System.Windows.Forms.Label();
            this.lblMaLoaiHinh = new System.Windows.Forms.Label();
            this.lblNgayDangKy = new System.Windows.Forms.Label();
            this.lblTriGia = new System.Windows.Forms.Label();
            this.cboMaLoaiHinh = new System.Windows.Forms.ComboBox();
            this.dtNgayDangKy = new System.Windows.Forms.DateTimePicker();
            this.grpSoHopDong = new System.Windows.Forms.GroupBox();
            this.txtGhiChu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numTriGia = new System.Windows.Forms.NumericUpDown();
            this.lblNgayDK = new System.Windows.Forms.Label();
            this.lblSoHD = new System.Windows.Forms.Label();
            this.txtSoHopDong = new System.Windows.Forms.TextBox();
            this.dtNgayKyHopDong = new System.Windows.Forms.DateTimePicker();
            this.grdHopDongXuatKhau = new Janus.Windows.GridEX.GridEX();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnThemMoi = new Janus.Windows.EditControls.UIButton();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.btnCapNhat = new Janus.Windows.EditControls.UIButton();
            this.cboSoToKhai = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.grpSoHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTriGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdHopDongXuatKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.cboSoToKhai);
            this.grbMain.Controls.Add(this.btnCapNhat);
            this.grbMain.Controls.Add(this.btnThemMoi);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.grdHopDongXuatKhau);
            this.grbMain.Controls.Add(this.grpSoHopDong);
            this.grbMain.Controls.Add(this.dtNgayDangKy);
            this.grbMain.Controls.Add(this.cboMaLoaiHinh);
            this.grbMain.Controls.Add(this.lblNgayDangKy);
            this.grbMain.Controls.Add(this.lblMaLoaiHinh);
            this.grbMain.Controls.Add(this.lblSoTK);
            this.grbMain.Size = new System.Drawing.Size(665, 367);
            // 
            // lblSoTK
            // 
            this.lblSoTK.AutoSize = true;
            this.lblSoTK.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTK.Location = new System.Drawing.Point(31, 16);
            this.lblSoTK.Name = "lblSoTK";
            this.lblSoTK.Size = new System.Drawing.Size(58, 13);
            this.lblSoTK.TabIndex = 0;
            this.lblSoTK.Text = "Số tờ khai:";
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.AutoSize = true;
            this.lblMaLoaiHinh.BackColor = System.Drawing.Color.Transparent;
            this.lblMaLoaiHinh.Location = new System.Drawing.Point(22, 46);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Size = new System.Drawing.Size(67, 13);
            this.lblMaLoaiHinh.TabIndex = 0;
            this.lblMaLoaiHinh.Text = "Mã loại hình:";
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.AutoSize = true;
            this.lblNgayDangKy.BackColor = System.Drawing.Color.Transparent;
            this.lblNgayDangKy.Location = new System.Drawing.Point(12, 70);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Size = new System.Drawing.Size(77, 13);
            this.lblNgayDangKy.TabIndex = 0;
            this.lblNgayDangKy.Text = "Ngày đăng ký:";
            // 
            // lblTriGia
            // 
            this.lblTriGia.AutoSize = true;
            this.lblTriGia.BackColor = System.Drawing.Color.Transparent;
            this.lblTriGia.Location = new System.Drawing.Point(11, 47);
            this.lblTriGia.Name = "lblTriGia";
            this.lblTriGia.Size = new System.Drawing.Size(40, 13);
            this.lblTriGia.TabIndex = 0;
            this.lblTriGia.Text = "Trị giá:";
            // 
            // cboMaLoaiHinh
            // 
            this.cboMaLoaiHinh.Enabled = false;
            this.cboMaLoaiHinh.FormattingEnabled = true;
            this.cboMaLoaiHinh.Items.AddRange(new object[] {
            "XSX"});
            this.cboMaLoaiHinh.Location = new System.Drawing.Point(95, 39);
            this.cboMaLoaiHinh.Name = "cboMaLoaiHinh";
            this.cboMaLoaiHinh.Size = new System.Drawing.Size(100, 21);
            this.cboMaLoaiHinh.TabIndex = 1;
            this.cboMaLoaiHinh.Text = "SXX";
            // 
            // dtNgayDangKy
            // 
            this.dtNgayDangKy.CustomFormat = "dd/MM/yyyy";
            this.dtNgayDangKy.Enabled = false;
            this.dtNgayDangKy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayDangKy.Location = new System.Drawing.Point(95, 66);
            this.dtNgayDangKy.Name = "dtNgayDangKy";
            this.dtNgayDangKy.Size = new System.Drawing.Size(100, 21);
            this.dtNgayDangKy.TabIndex = 2;
            // 
            // grpSoHopDong
            // 
            this.grpSoHopDong.BackColor = System.Drawing.Color.Transparent;
            this.grpSoHopDong.Controls.Add(this.txtGhiChu);
            this.grpSoHopDong.Controls.Add(this.label1);
            this.grpSoHopDong.Controls.Add(this.numTriGia);
            this.grpSoHopDong.Controls.Add(this.lblNgayDK);
            this.grpSoHopDong.Controls.Add(this.lblSoHD);
            this.grpSoHopDong.Controls.Add(this.txtSoHopDong);
            this.grpSoHopDong.Controls.Add(this.lblTriGia);
            this.grpSoHopDong.Controls.Add(this.dtNgayKyHopDong);
            this.grpSoHopDong.Location = new System.Drawing.Point(227, 2);
            this.grpSoHopDong.Name = "grpSoHopDong";
            this.grpSoHopDong.Size = new System.Drawing.Size(353, 97);
            this.grpSoHopDong.TabIndex = 3;
            this.grpSoHopDong.TabStop = false;
            this.grpSoHopDong.Text = "Hợp đồng xuất khẩu";
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(57, 70);
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(289, 21);
            this.txtGhiChu.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ghi Chú:";
            // 
            // numTriGia
            // 
            this.numTriGia.DecimalPlaces = 8;
            this.numTriGia.Location = new System.Drawing.Point(57, 43);
            this.numTriGia.Maximum = new decimal(new int[] {
            1410065408,
            2,
            0,
            0});
            this.numTriGia.Name = "numTriGia";
            this.numTriGia.Size = new System.Drawing.Size(128, 21);
            this.numTriGia.TabIndex = 3;
            this.numTriGia.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numTriGia.ThousandsSeparator = true;
            // 
            // lblNgayDK
            // 
            this.lblNgayDK.AutoSize = true;
            this.lblNgayDK.Location = new System.Drawing.Point(191, 20);
            this.lblNgayDK.Name = "lblNgayDK";
            this.lblNgayDK.Size = new System.Drawing.Size(50, 13);
            this.lblNgayDK.TabIndex = 1;
            this.lblNgayDK.Text = "Ngày ký:";
            // 
            // lblSoHD
            // 
            this.lblSoHD.AutoSize = true;
            this.lblSoHD.Location = new System.Drawing.Point(10, 22);
            this.lblSoHD.Name = "lblSoHD";
            this.lblSoHD.Size = new System.Drawing.Size(41, 13);
            this.lblSoHD.TabIndex = 0;
            this.lblSoHD.Text = "Số HĐ:";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(57, 16);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(128, 21);
            this.txtSoHopDong.TabIndex = 0;
            // 
            // dtNgayKyHopDong
            // 
            this.dtNgayKyHopDong.CustomFormat = "dd/MM/yyyy";
            this.dtNgayKyHopDong.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayKyHopDong.Location = new System.Drawing.Point(246, 16);
            this.dtNgayKyHopDong.Name = "dtNgayKyHopDong";
            this.dtNgayKyHopDong.Size = new System.Drawing.Size(100, 21);
            this.dtNgayKyHopDong.TabIndex = 2;
            // 
            // grdHopDongXuatKhau
            // 
            this.grdHopDongXuatKhau.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHopDongXuatKhau.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdHopDongXuatKhau.CardCaptionFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHopDongXuatKhau.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            grdHopDongXuatKhau_DesignTimeLayout.LayoutString = resources.GetString("grdHopDongXuatKhau_DesignTimeLayout.LayoutString");
            this.grdHopDongXuatKhau.DesignTimeLayout = grdHopDongXuatKhau_DesignTimeLayout;
            this.grdHopDongXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grdHopDongXuatKhau.Location = new System.Drawing.Point(0, 105);
            this.grdHopDongXuatKhau.Name = "grdHopDongXuatKhau";
            this.grdHopDongXuatKhau.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.grdHopDongXuatKhau.Size = new System.Drawing.Size(662, 230);
            this.grdHopDongXuatKhau.TabIndex = 5;
            this.grdHopDongXuatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grdHopDongXuatKhau.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grdHopDongXuatKhau_RowDoubleClick);
            this.grdHopDongXuatKhau.Click += new System.EventHandler(this.grdHopDongXuatKhau_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(587, 341);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "83.png");
            this.imageList.Images.SetKeyName(1, "disk.png");
            this.imageList.Images.SetKeyName(2, "edit.png");
            this.imageList.Images.SetKeyName(3, "erase.png");
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.ImageIndex = 3;
            this.btnXoa.ImageList = this.imageList;
            this.btnXoa.Location = new System.Drawing.Point(506, 341);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 7;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnThemMoi
            // 
            this.btnThemMoi.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnThemMoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemMoi.ImageIndex = 0;
            this.btnThemMoi.ImageList = this.imageList;
            this.btnThemMoi.Location = new System.Drawing.Point(586, 12);
            this.btnThemMoi.Name = "btnThemMoi";
            this.btnThemMoi.Size = new System.Drawing.Size(75, 23);
            this.btnThemMoi.TabIndex = 4;
            this.btnThemMoi.Text = "Thêm";
            this.btnThemMoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThemMoi.Click += new System.EventHandler(this.btnThemMoi_Click);
            // 
            // btnCapNhat
            // 
            this.btnCapNhat.Enabled = false;
            this.btnCapNhat.ImageIndex = 2;
            this.btnCapNhat.ImageList = this.imageList;
            this.btnCapNhat.Location = new System.Drawing.Point(586, 41);
            this.btnCapNhat.Name = "btnCapNhat";
            this.btnCapNhat.Size = new System.Drawing.Size(75, 23);
            this.btnCapNhat.TabIndex = 11;
            this.btnCapNhat.Text = "Cập Nhật";
            this.btnCapNhat.VisualStyleManager = this.vsmMain;
            this.btnCapNhat.Click += new System.EventHandler(this.btnCapNhat_Click_1);
            // 
            // cboSoToKhai
            // 
            this.cboSoToKhai.Location = new System.Drawing.Point(95, 12);
            this.cboSoToKhai.Name = "cboSoToKhai";
            this.cboSoToKhai.Size = new System.Drawing.Size(100, 21);
            this.cboSoToKhai.TabIndex = 12;
            this.cboSoToKhai.SelectedIndexChanged += new System.EventHandler(this.cboSoToKhai_SelectedIndexChanged);
            this.cboSoToKhai.Leave += new System.EventHandler(this.cboSoToKhai_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 344);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(265, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Double Click dòng trên lưới để chỉnh sửa";
            // 
            // FrmQuanLyHopDongXuatKhau
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 367);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "FrmQuanLyHopDongXuatKhau";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quản lý hợp đồng xuất khẩu";
            this.Load += new System.EventHandler(this.FrmQuanLyHopDongXuatKhau_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            this.grpSoHopDong.ResumeLayout(false);
            this.grpSoHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTriGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grdHopDongXuatKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTriGia;
        private System.Windows.Forms.Label lblNgayDangKy;
        private System.Windows.Forms.Label lblMaLoaiHinh;
        private System.Windows.Forms.Label lblSoTK;
        private System.Windows.Forms.ComboBox cboMaLoaiHinh;
        private System.Windows.Forms.GroupBox grpSoHopDong;
        private System.Windows.Forms.Label lblNgayDK;
        private System.Windows.Forms.Label lblSoHD;
        private System.Windows.Forms.DateTimePicker dtNgayDangKy;
        private System.Windows.Forms.TextBox txtSoHopDong;
        private Janus.Windows.GridEX.GridEX grdHopDongXuatKhau;
        private Janus.Windows.EditControls.UIButton btnThemMoi;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.DateTimePicker dtNgayKyHopDong;
        private System.Windows.Forms.NumericUpDown numTriGia;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.TextBox txtGhiChu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.BindingSource bindingSource1;
        private Janus.Windows.EditControls.UIButton btnCapNhat;
        private System.Windows.Forms.ComboBox cboSoToKhai;
        private System.Windows.Forms.Label label2;
    }
}
