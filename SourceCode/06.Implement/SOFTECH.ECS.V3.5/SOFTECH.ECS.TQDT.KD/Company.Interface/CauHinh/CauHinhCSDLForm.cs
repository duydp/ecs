﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;

namespace Company.Interface
{
    public partial class CauHinhCSDLForm : BaseForm
    {
        public CauHinhCSDLForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            txtDiaChiHQ.ReadOnly = true;
            //try
            //{
                
            //    SQLDMO.Application app = new SQLDMO.Application();
            //    SQLDMO.NameList names = app.ListAvailableSQLServers();
            //    foreach (string st in names)
            //    {
            //        txtServerName.Items.Add(st, st);
            //    }
            //}
            //catch { }
            txtDatabase.Text = GlobalSettings.DATABASE_NAME;
            txtSa.Text = GlobalSettings.USER;
            txtServerName.Text = GlobalSettings.SERVER_NAME;
            txtDiaChiHQ.Text = GlobalSettings.DiaChiWS;
            txtPass.Text = GlobalSettings.PASS;
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            //this.Hide();
            //LoginForm loginForm = new LoginForm();
            //loginForm.ShowDialog(this);
            this.Close();
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
                return;
            if (txtPass.Text.Trim().Length > 0)
                Properties.Settings.Default.pass = txtPass.Text.Trim();
            Properties.Settings.Default.ServerName = txtServerName.Text.Trim();
            Properties.Settings.Default.DATABASE_NAME = txtDatabase.Text.Trim();
            Properties.Settings.Default.user = txtSa.Text.Trim();
            if(txtDiaChiHQ.Text.Trim()!=Properties.Settings.Default.DiaChiWS)
                Properties.Settings.Default.DiaChiWS = txtDiaChiHQ.Text.Trim();
 
            //Cấu hình lại connectionString.
            string st = "Server=" + txtServerName.Text.Trim() + ";Database=" + Properties.Settings.Default.DATABASE_NAME + ";Uid=" + Properties.Settings.Default.user + ";Pwd=" + Properties.Settings.Default.pass;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = st;            
            //cấu hình WS
            //SQLDMO.SQLServer2Class server = new SQLDMO.SQLServer2Class();
            //try
            //{
            //    server.Connect(txtServerName.Text.Trim(), txtSa.Text.Trim(), txtPass.Text.Trim());
            //}
            //catch
            //{
            //    MLMessages("Không kết nối được tới máy chủ này","MSG_PUB05","", false);
            //    return;
            //}
            //server.DisConnect();
            ConfigurationSection section = config.ConnectionStrings;
            if ((section.SectionInformation.IsProtected == false) &&
                (section.ElementInformation.IsLocked == false))
            {
                // Save the encrypted section.
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Modified);
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("Connection String");           
            Properties.Settings.Default.Save();
            GlobalSettings.RefreshKey();
            XmlDocument doc = new XmlDocument();
            doc.Load(Application.StartupPath+"\\eDeclaration2007.exe.config");
            doc.SelectSingleNode("//setting[@name='Company_BLL_WS_KhaiDienTu_KDTService']").SelectSingleNode("value").InnerText = "http://" + txtDiaChiHQ.Text.Trim() + "/kdtservice/kdtservice.asmx";
            doc.Save("eDeclaration2007.exe.config");
            ConfigurationManager.RefreshSection("applicationSettings");
            //this.Hide();
            //LoginForm loginForm = new LoginForm();
            //loginForm.ShowDialog(this);
            this.Close();            
        }

        //private void txtDiaChiHQ_TextChanged(object sender, EventArgs e)
        //{

        //}

        //private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        //{
        //    //txtDiaChiHQ.ReadOnly = false;
        //    //txtDiaChiHQ.BackColor = System.Drawing.Color.White;
        //}

        //private void checkBox2_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtServerName.ReadOnly = false;
        //    txtServerName.BackColor = System.Drawing.Color.White;
        //}

        //private void checkBox3_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtDatabase.ReadOnly = false;
        //    txtDatabase.BackColor = System.Drawing.Color.White;
        //}

        //private void checkBox4_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtSa.ReadOnly = false;
        //    txtSa.BackColor = System.Drawing.Color.White;
        //}

        //private void checkBox5_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtPass.ReadOnly = false;
        //    txtPass.BackColor = System.Drawing.Color.White;
            
        //}

        //private void checkBox1_CheckedChanged(object sender, EventArgs e)
        //{
        //    txtDiaChiHQ.ReadOnly = false;
        //    txtDiaChiHQ.BackColor = System.Drawing.Color.White;
        //}
    }
}