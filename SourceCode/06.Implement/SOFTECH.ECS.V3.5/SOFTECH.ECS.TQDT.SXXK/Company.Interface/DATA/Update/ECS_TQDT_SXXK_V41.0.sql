GO    
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamic_SanPham]  
-- Database: ECS_TQ_SXXK  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, March 22, 2010  
------------------------------------------------------------------------------------------------------------------------  
  
ALTER PROCEDURE [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamic_SanPham]  
 @WhereCondition nvarchar(500),  
 @OrderByExpression nvarchar(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL nvarchar(3250)  
  
SET @SQL = 'SELECT  SP_DK.[ID],  
 [SoTiepNhan],  
 [NgayTiepNhan],  
 [MaHaiQuan],  
 [MaDoanhNghiep],  
 [MaDaiLy],  
 [TrangThaiXuLy],  
 [GUIDSTR],  
 [DeXuatKhac],  
 [LyDoSua],  
 [ActionStatus],  
 [GuidReference],  
 [NamDK],  
 [HUONGDAN],  
 [PhanLuong],  
 [Huongdan_PL]  
 FROM [dbo].[t_KDT_SXXK_SanPhamDangKy] SP_DK  
 inner join t_KDT_SXXK_SanPham SP   
 on SP_DK.id= SP.master_id   WHERE
 ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
  
GO
------------------------------------------------------------------------------------------------------------------------        
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamicAll]        
-- Database: ECS_TQ_SXXK        
-- Author: Ngo Thanh Tung        
-- Time created: Monday, March 22, 2010        
------------------------------------------------------------------------------------------------------------------------        
        
ALTER PROCEDURE [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamicAll]        
 @WhereCondition nvarchar(500),        
 @OrderByExpression nvarchar(250) = NULL        
AS        
        
SET NOCOUNT ON        
SET TRANSACTION ISOLATION LEVEL READ COMMITTED        
        
DECLARE @SQL nvarchar(3250)        
        
SET @SQL = ' SELECT SP_DK.ID ,      
        SoTiepNhan ,      
        NgayTiepNhan ,      
        MaHaiQuan ,      
        MaDoanhNghiep ,      
        MaDaiLy ,      
        TrangThaiXuLy ,      
        GUIDSTR ,      
        DeXuatKhac ,      
        LyDoSua ,      
        ActionStatus ,      
        GuidReference ,      
        NamDK ,      
        HUONGDAN ,      
        PhanLuong ,      
        Huongdan_PL ,      
        Ma ,      
        Ten ,      
        MaHS ,      
  CASE WHEN Ma IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_SanPham WHERE Ma = Ma)) ELSE DVT_ID END AS DVT      
        FROM [dbo].[t_KDT_SXXK_SanPhamDangKy] SP_DK INNER JOIN dbo.t_KDT_SXXK_SanPham SP ON SP.Master_ID = SP_DK.ID  WHERE ' + @WhereCondition        
        
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0        
BEGIN        
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression        
END        
        
EXEC sp_executesql @SQL        


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.0',GETDATE(), N'CẬP NHẬT PROCEDURE SEARCH SẢN PHẨM ')
END

GO