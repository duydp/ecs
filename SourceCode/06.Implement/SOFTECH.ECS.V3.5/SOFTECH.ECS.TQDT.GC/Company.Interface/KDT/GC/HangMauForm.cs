﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.QuanTri;

namespace Company.Interface.KDT.GC
{
    public partial class HangMauForm : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public HangMau hmDetail = new HangMau();
        public bool isBrower = false;
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        public HangMauForm()
        {
            InitializeComponent();
            txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindData()
        {
            try
            {
                dgHangMau.Refresh();
                dgHangMau.DataSource = HD.HangMauCollection;
                dgHangMau.Refetch();
            }
            catch (Exception exx)
            {
                Logger.LocalLogger.Instance().WriteMessage(exx);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = hmDetail.Ma.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = hmDetail.Ten.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = hmDetail.MaHS.Trim();
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = hmDetail.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = (hmDetail.SoLuongDangKy.ToString());
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = hmDetail.GhiChu;
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void HangMauForm_Load(object sender, EventArgs e)
        {
            try
            {
                txtSoLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                Caption = this.Text;
                if (!isBrower)
                {
                    txtMa.Focus();
                    this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                    cbDonViTinh.DataSource = this._DonViTinh;
                    cbDonViTinh.DisplayMember = "Ten";
                    cbDonViTinh.ValueMember = "ID";
                    if (!string.IsNullOrEmpty(hmDetail.Ma))
                    {
                        SetData();
                    }
                    else
                    {
                        cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                        cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
                        cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);
                    }


                    if (this.OpenType == OpenFormType.View)
                    {
                        btnAdd.Enabled = false;
                        btnXoa.Enabled = false;
                        dgHangMau.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False; ;
                    }

                }
                else
                {
                    uiGroupBox2.Visible = false;
                    HD.LoadCollection();
                    dgHangMau.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    this.Width = dgHangMau.Width;
                    this.Height = dgHangMau.Height;
                }
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private void reset()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = "";
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = "";
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = "";
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                txtSoLuong.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoLuong.Text = "";
                txtSoLuong.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = "";
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                error.SetError(txtMaHS, null);
                error.SetError(txtMa, null);
                error.SetError(txtTen, null);
                BindData();
                hmDetail = new HangMau();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }
        private void setError()
        {
            try
            {
                error.Clear();
                error.SetError(txtTen, null);
                error.SetError(txtMa, null);
                error.SetError(txtMaHS, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã hàng mẫu", isOnlyWarning);
                isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã hàng mẫu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên/Mô tả hàng mẫu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtSoLuong, errorProvider, "Số lượng");
                isValid &= ValidateControl.ValidateChoose(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                txtMa.Focus();
                if (!ValidateForm(false))
                    return;
                checkExitsAndSTTHang();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        private void checkExitsAndSTTHang()
        {
            try
            {
                HD.HangMauCollection.Remove(hmDetail);
                foreach (HangMau hm in HD.HangMauCollection)
                {
                    if (hm.Ma.Trim() == txtMa.Text.Trim())
                    {
                        showMsg("Hàng mẫu này đã được thêm vào trước đây", false);
                        if (hmDetail.Ma != "")
                            HD.HangMauCollection.Add(hmDetail);
                        return;
                    }
                }
                hmDetail.Ma = txtMa.Text.Trim();
                hmDetail.Ten = txtTen.Text.Trim();
                hmDetail.MaHS = txtMaHS.Text.Trim();
                hmDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                hmDetail.HopDong_ID = HD.ID;
                hmDetail.GhiChu = txtGhiChu.Text;
                hmDetail.SoLuongDangKy = Convert.ToDecimal(txtSoLuong.Value);
                HD.HangMauCollection.Add(hmDetail);
                reset();
                this.setError();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }
        
        private void dgHangMau_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                hmDetail = (HangMau)e.Row.DataRow;
                hmDetail.HopDong_ID = HD.ID;
                if (!isBrower)
                {
                    SetData();
                }
                else
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }



        private void dgHangMau_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }

        private void ctrNguyenTe_Load(object sender, EventArgs e)
        {

        }

        private void dgHangMau_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.Edit)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
                        List<HangMau> ItemDelete = new List<HangMau>();
                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.HangMau tbDelete = (Company.GC.BLL.KDT.GC.HangMau)row.GetRow().DataRow;
                            tbDelete.Delete();
                            Company.GC.BLL.GC.GC_HangMau.DeleteGC_HangMau(HD.ID, tbDelete.Ma);
                            HD.HangMauCollection.Remove(tbDelete);
                        }
                        if (ItemDelete.Count >= 1)
                            Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaHangMau, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        this.SetChange(true);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                    BindData();
                }    
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                List<HangMau> tbColl = new List<HangMau>();
                if (HD.HangMauCollection.Count <= 0) return;
                GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
                if (items.Count <= 0) return;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        HangMau tbDelete = (HangMau)row.GetRow().DataRow;
                        tbDelete.Delete();
                        Company.GC.BLL.GC.GC_HangMau.DeleteGC_HangMau(HD.ID, tbDelete.Ma);
                        tbColl.Add(tbDelete);
                    }
                    foreach (HangMau tbt in tbColl)
                    {
                        HD.HangMauCollection.Remove(tbt);
                    }
                    if (tbColl.Count >= 1)
                        Log.LogHDGC(HD, tbColl, MessageTitle.XoaHangMau, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindData();
                    this.setError();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                 
            }

        }

        private void dgHangMau_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            hmDetail = (HangMau)i.GetRow().DataRow;
                            hmDetail.HopDong_ID = HD.ID;
                            if (!isBrower)
                            {
                                SetData();
                            }
                            else
                            {
                                this.Close();
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgHangMau.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaHM.Text);
                dgHangMau.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}