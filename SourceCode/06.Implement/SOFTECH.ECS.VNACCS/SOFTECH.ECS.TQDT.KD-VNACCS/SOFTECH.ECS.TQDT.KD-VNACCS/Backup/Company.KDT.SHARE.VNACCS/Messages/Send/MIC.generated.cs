using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class MIC : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute JYO { get; set; }
public PropertiesAttribute SKB { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IMY { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute IMT { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EPY { get; set; }
public PropertiesAttribute EPA { get; set; }
public PropertiesAttribute EP2 { get; set; }
public PropertiesAttribute EP3 { get; set; }
public PropertiesAttribute EP4 { get; set; }
public PropertiesAttribute EPO { get; set; }
public PropertiesAttribute HAB { get; set; }
public PropertiesAttribute MAB { get; set; }
public PropertiesAttribute NO { get; set; }
public PropertiesAttribute GW { get; set; }
public PropertiesAttribute ST { get; set; }
public PropertiesAttribute VSN { get; set; }
public PropertiesAttribute ARR { get; set; }
public PropertiesAttribute DST { get; set; }
public PropertiesAttribute PSC { get; set; }
public PropertiesAttribute IP1 { get; set; }
public PropertiesAttribute IP2 { get; set; }
public PropertiesAttribute IP3 { get; set; }
public PropertiesAttribute IP4 { get; set; }
public PropertiesAttribute FR1 { get; set; }
public PropertiesAttribute FR2 { get; set; }
public PropertiesAttribute FR3 { get; set; }
public PropertiesAttribute IN1 { get; set; }
public PropertiesAttribute IN2 { get; set; }
public PropertiesAttribute IN3 { get; set; }
public PropertiesAttribute CMN { get; set; }
public PropertiesAttribute OR { get; set; }
public PropertiesAttribute DPR { get; set; }
public PropertiesAttribute NT1 { get; set; }
public PropertiesAttribute REF { get; set; }

public MIC()
        {
ICN = new PropertiesAttribute(12, typeof(int));
JYO = new PropertiesAttribute(1, typeof(string));
SKB = new PropertiesAttribute(1, typeof(string));
CH = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IMY = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
IMT = new PropertiesAttribute(20, typeof(string));
EPC = new PropertiesAttribute(13, typeof(string));
EPN = new PropertiesAttribute(70, typeof(string));
EPY = new PropertiesAttribute(9, typeof(string));
EPA = new PropertiesAttribute(35, typeof(string));
EP2 = new PropertiesAttribute(35, typeof(string));
EP3 = new PropertiesAttribute(35, typeof(string));
EP4 = new PropertiesAttribute(35, typeof(string));
EPO = new PropertiesAttribute(2, typeof(string));
HAB = new PropertiesAttribute(20, typeof(string));
MAB = new PropertiesAttribute(20, typeof(string));
NO = new PropertiesAttribute(6, typeof(int));
GW = new PropertiesAttribute(8, typeof(int));
ST = new PropertiesAttribute(7, typeof(string));
VSN = new PropertiesAttribute(12, typeof(string));
ARR = new PropertiesAttribute(8, typeof(DateTime));
DST = new PropertiesAttribute(3, typeof(string));
PSC = new PropertiesAttribute(5, typeof(string));
IP1 = new PropertiesAttribute(1, typeof(string));
IP2 = new PropertiesAttribute(3, typeof(string));
IP3 = new PropertiesAttribute(3, typeof(string));
IP4 = new PropertiesAttribute(20, typeof(int));
FR1 = new PropertiesAttribute(1, typeof(string));
FR2 = new PropertiesAttribute(3, typeof(string));
FR3 = new PropertiesAttribute(18, typeof(int));
IN1 = new PropertiesAttribute(1, typeof(string));
IN2 = new PropertiesAttribute(3, typeof(string));
IN3 = new PropertiesAttribute(16, typeof(int));
CMN = new PropertiesAttribute(600, typeof(string));
OR = new PropertiesAttribute(2, typeof(string));
DPR = new PropertiesAttribute(8, typeof(int));
NT1 = new PropertiesAttribute(300, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
TongSoByte = 2068;
}

}public partial class EnumGroupID
    {
}

}
