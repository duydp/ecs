﻿//Lypt created
//Date 07/01/2010
using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT;
using Company.BLL.DuLieuChuan;
using System.Windows.Forms;

namespace Company.Interface.Report.SXXK
{
    public partial class TQDTToKhaiNK : DevExpress.XtraReports.UI.XtraReport
    {
        //public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        //public string PTVT_Name = "";
        //public string PTTT_Name = "";
        //public string DKGH = "";
        //public string DongTienTT = "";
        //public string NuocXK = "";
        //linhhtn
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKNTQDTForm report;
        public bool BanLuuHaiQuan = false;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public TQDTToKhaiNK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            try
            {
                xrLine6.Height = 50;

                /*trạng thái xử lý =1  thì hiện mã vạch các trạng
                 thái còn lại thì ẩn mã vạch*/
                lblMaVach.Visible = (TKMD.TrangThaiXuLy == 1);

                this.PrintingSystem.ShowMarginsWarning = false;
                decimal tongTriGiaNT = 0;
                decimal tongTienThueXNK = 0;
                decimal tongTienThueTatCa = 0;
                decimal tongTriGiaTT = 0;
                decimal tongTriGiaTTGTGT = 0;
                decimal tongThueGTGT = 0;
                decimal tongTriGiaThuKhac = 0;
                decimal tongPhuThu = 0;

                lblMienThueNK.Visible = MienThue1;
                lblMienThueGTGT.Visible = MienThue2;
                lblMienThueNK.Text = GlobalSettings.TieuDeInDinhMuc;
                lblMienThueGTGT.Text = GlobalSettings.MienThueGTGT;

                DateTime minDate = new DateTime(1900, 1, 1);
                //if (this.TKMD.SoTiepNhan != 0)
                //    this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;


                lblChiCucHQ.Text = string.Format("{0} ({1})", GlobalSettings.TEN_HAI_QUAN, GlobalSettings.MA_HAI_QUAN);


                string chicuc = Company.BLL.KDT.SXXK.NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHQCK.Text = chicuc;


                //số tham chiếu
                //TODO:Cao Hữu Tú:updated 06-09-2011:lấy ID của Tờ Khai làm số tham chiếu            
                long sothamchieu = TKMD.SoTiepNhan;
                if (sothamchieu > 0)
                    lblThamChieu.Text = sothamchieu.ToString();

                //Ngay gui
                if (this.TKMD.NgayTiepNhan > minDate)
                    lblNgayGui.Text = this.TKMD.NgayTiepNhan.ToString("dd/MM/yyyy");
                else
                    lblNgayGui.Text = "";

                //So to khai
                if (this.TKMD.SoToKhai > 0)
                    lblToKhai.Text = this.TKMD.SoToKhai + "";
                else
                    lblToKhai.Text = "";

                //Ngay dang ký
                if (this.TKMD.NgayDangKy > minDate)
                    lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
                else
                    lblNgayDangKy.Text = "";

                //1. Nguoi xuat khau          
                lblNguoiXK.Text = TKMD.TenDonViDoiTac.ToUpper().Replace("\\R\\N", "\r\n");
                lblNguoiXK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                //if (lblNguoiXK.Text.Length > 130)
                //    lblNguoiXK.Text = lblNguoiXK.Text.Trim().Substring(0, 130);

                //2. Nguoi nhap khau
                if (Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap")))
                    //lblNguoiNK.Text = (this.TKMD.TenDoanhNghiep + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
                    lblNguoiNK.Text = (GlobalSettings.TEN_DON_VI + "\r\n" + GlobalSettings.DIA_CHI).ToUpper();
                else
                    //lblNguoiNK.Text = (this.TKMD.TenDoanhNghiep + ". " + GlobalSettings.DIA_CHI).ToUpper(); //lblMaDoanhNghiep1
                    lblNguoiNK.Text = (GlobalSettings.TEN_DON_VI + ". " + GlobalSettings.DIA_CHI).ToUpper(); //lblMaDoanhNghiep1

                //if (lblNguoiNK.Text.Length > 130)
                //    lblNguoiNK.Text = lblNguoiNK.Text.Trim().Substring(0, 130);

                lblMaDoanhNghiep.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep);// +"\r\n" + GlobalSettings.DIA_CHI.ToUpper();
                lblNguoiNK.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));

                //3. Nguoi uy thac
                lblNguoiUyThac.Text = TKMD.TenDonViUT;

                //4. Dai ly lam thu tuc
                //lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
                //lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;

                //5 Loai hinh
                string stlh = "";
                stlh = BLL.DuLieuChuan.LoaiHinhMauDich.GetName(TKMD.MaLoaiHinh);
                lblLoaiHinh.Text = TKMD.MaLoaiHinh + stlh;

                //6. Hoa don  thuong mai
                lblHoaDonThuongMai.Text = "" + this.TKMD.SoHoaDonThuongMai;

                if (this.TKMD.NgayHoaDonThuongMai > minDate)
                    lblNgayHoaDon.Text = this.TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
                else
                    lblNgayHoaDon.Text = "";

                //7. Giay phep
                if (TKMD.SoGiayPhep != "")
                    lblGiayPhep.Text = "" + this.TKMD.SoGiayPhep;
                else
                    lblGiayPhep.Text = "";

                if (this.TKMD.NgayGiayPhep > minDate)
                    lblNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayGiayPhep.Text = "";

                if (this.TKMD.NgayHetHanGiayPhep > minDate)
                    lblNgayHetHanGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanGiayPhep.Text = "";

                //8. Hop dong
                //if (this.TKMD.SoHopDong.Length > 36)
                //    lblHopDong.Font = new Font("Times New Roman", 6.5f);
                try
                {
                    lblHopDong.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoHD")));
                }
                catch
                {
                    lblHopDong.Font = new Font("Times New Roman", 8f);
                }
                lblHopDong.Text = "" + this.TKMD.SoHopDong;

                if (this.TKMD.NgayHopDong > minDate)
                    lblNgayHopDong.Text = this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHopDong.Text = " ";

                if (this.TKMD.NgayHetHanHopDong > minDate)
                    lblNgayHetHanHopDong.Text = "" + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
                else
                    lblNgayHetHanHopDong.Text = " ";

                //9. Van tai don
                lblVanTaiDon.Text = this.TKMD.SoVanDon;
                if (this.TKMD.NgayVanDon > minDate)
                    lblNgayVanTaiDon.Text = this.TKMD.NgayVanDon.ToString("dd/MM/yyyy");
                else
                    lblNgayVanTaiDon.Text = "";

                //10. Cang xep hang
                lblCangXepHang.Text = this.TKMD.DiaDiemXepHang;

                //11. Cang do hang
                lblMaCangdoHang.Text = this.TKMD.CuaKhau_ID;
                lblCangDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);

                //12. Phuong tien van tai
                lblTenPTVT.Text = PhuongThucVanTai.getName(TKMD.PTVT_ID);
                lblPhuongTienVanTai.Text = " " + this.TKMD.SoHieuPTVT;
                //if (this.TKMD.NgayDenPTVT > minDate)
                //     lblNgayDenPTVT.Text = "Ngày đến: " + this.TKMD.NgayDenPTVT.ToString("dd/MM/yyyy");
                //else
                //    lblNgayDenPTVT.Text = "Ngày đến: ";

                //13. Nuox xuat khau
                lblNuocXK.Text = this.TKMD.NuocXK_ID;
                lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocXK_ID);

                //14. Dieu kien giao hang
                lblDieuKienGiaoHang.Text = this.TKMD.DKGH_ID;

                //15. Phuong thuc thanh toan
                lblPhuongThucThanhToan.Text = this.TKMD.PTTT_ID;

                //16. Dong tin thanh toan
                lblDongTienThanhToan.Text = this.TKMD.NguyenTe_ID;

                //17. Ty gia tinh thue
                lblTyGiaTinhThue.Text = this.TKMD.TyGiaTinhThue.ToString("G10");

                //18. Phan luong & huong dan cua hai quan
                //if (TKMD.PhanLuong == "1")
                //    lblLyDoTK.Text = "Tờ khai được thông quan";
                //else if (TKMD.PhanLuong == "2")
                //    lblLyDoTK.Text = "Tờ khai phải xuất trình giấy tờ";
                //else if (TKMD.PhanLuong == "3")
                //    lblLyDoTK.Text = "Tờ khai phải kiểm hóa";
                //else
                lblLyDoTK.Text = "";

                lblHuongDan.Text = TKMD.HUONGDAN;

                //19. Chung tu hai quan truoc
                lblChungTu.Text = "";


                //truoc 26. Phi bao hiem
                string st = "";
                if (this.TKMD.PhiBaoHiem > 0)
                    st = "I = " + this.TKMD.PhiBaoHiem.ToString("N2");
                if (this.TKMD.PhiVanChuyen > 0)
                    st += " F = " + this.TKMD.PhiVanChuyen.ToString("N2");
                if (this.TKMD.PhiKhac > 0)
                    st += " Phí khác = " + this.TKMD.PhiKhac.ToString("N2");
                if (GlobalSettings.ChiPhiKhac == 1)
                    lblPhiBaoHiem.Text = st;


                //31. Tong trong luong
                if (TKMD.TrongLuong > 0)
                    lbltongtrongluong.Text = TKMD.TrongLuong + " kg ";
                else
                    lbltongtrongluong.Text = "";

                if (TKMD.SoContainer20 > 3 || TKMD.SoContainer40 > 3)
                    lblChitietCon.Text = "danh sách container theo bảng kê đính kèm";
                else
                {
                    string soHieuKien = "";
                    Company.KDT.SHARE.QuanLyChungTu.Container objContainer = null;

                    if (TKMD.VanTaiDon != null)
                    {
                        for (int cnt = 0; cnt < TKMD.VanTaiDon.ContainerCollection.Count; cnt++)
                        {
                            objContainer = TKMD.VanTaiDon.ContainerCollection[cnt];

                            soHieuKien += objContainer.SoHieu + "/" + objContainer.Seal_No;

                            if (cnt < TKMD.VanTaiDon.ContainerCollection.Count - 1)
                            {
                                soHieuKien += "; ";
                            }
                        }
                    }
                    lblChitietCon.Text = soHieuKien;
                }

                //datlmq bo sung de xuat khac
                lblDeXuatKhac.Text = "32. Ghi chép khác: " + TKMD.DeXuatKhac;
                //Hungtq updated 08/05/2012.
                lblDeXuatKhac.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));

                //Tong so CONTAINER
                string tsContainer = "";
                string cont20 = "", cont40 = "", soKien = "";
                if (TKMD.SoContainer20 > 0)
                {
                    cont20 = "Cont20: " + Convert.ToInt32(TKMD.SoContainer20);

                    tsContainer += cont20 + "; ";
                }
                else
                    cont20 = "";

                if (TKMD.SoContainer40 > 0)
                {
                    cont40 = "Cont40: " + Convert.ToInt32(TKMD.SoContainer40);

                    tsContainer += cont40 + "; ";
                }
                else
                    cont40 = "";

                if (TKMD.SoKien > 0)
                {
                    soKien = "Tổng số kiện: " + TKMD.SoKien;

                    tsContainer += soKien;
                }
                else
                    soKien = "";

                TongSoContainer.Text = tsContainer.Length > 0 ? "Tổng số container: " + tsContainer : "Tổng số container:";


                if (this.TKMD.HMDCollection.Count <= 3)
                {
                    if (this.TKMD.HMDCollection.Count >= 1)
                    {
                        #region Dong hang 1
                        HangMauDich hmd = this.TKMD.HMDCollection[0];
                        if (!inMaHang)
                            TenHang1.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang1.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang1.Text = hmd.TenHang;
                        }
                        TenHang1.WordWrap = true;
                        try
                        {
                            TenHang1.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        }
                        catch
                        {
                            MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                            return;
                        }
                        MaHS1.Text = hmd.MaHS;
                        XuatXu1.Text = hmd.NuocXX_ID;
                        Luong1.Text = hmd.SoLuong.ToString("G15");
                        DVT1.Text = DonViTinh.GetName(hmd.DVT_ID);

                        DonGiaNT1.Text = hmd.DonGiaKB.ToString("G10");
                        TriGiaNT1.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));
                        TriGiaTT1.Text = hmd.TriGiaTT.ToString("N0");

                        TriGiaThuKhac1.Text = hmd.PhuThu.ToString("N0");
                        TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                        TienThueXNK1.Text = hmd.ThueXNK.ToString("N0");
                        ThueSuatXNK1.Text = hmd.ThueSuatXNK.ToString("N0");

                        tongTienThueXNK += hmd.ThueXNK;

                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            double TriGiaTTGTGT = 0;// hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTGTGT.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Trim() == "")
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatVATGiam;
                            TienThueGTGT1.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            double TriGiaTTTTDB = 0;// hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam;
                            TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac1.Text = hmd.TriGiaThuKhac.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");

                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;

                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            double TriGiaTTTTDB = 0;//hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT1.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT1.Text = hmd.ThueSuatTTDBGiam;
                            TienThueGTGT1.Text = hmd.ThueTTDB.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Length == 0)
                            //    TyLeThuKhac1.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    TyLeThuKhac1.Text = hmd.ThueSuatVATGiam;
                            TriGiaThuKhac1.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac1.Text = hmd.TyLeThuKhac.ToString("N0");

                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                        if (hmd.TyLeThuKhac + hmd.PhuThu == 0)
                        {
                            TriGiaThuKhac1.Text = TyLeThuKhac1.Text = string.Empty;
                        }
                        //}
                        if (MienThue1)
                        {
                            TriGiaTT1.Text = "";
                            ThueSuatXNK1.Text = "";
                            TienThueXNK1.Text = "";
                            TriGiaThuKhac1.Text = string.Empty;
                            TyLeThuKhac1.Text = string.Empty;

                        }
                        if (MienThue2)
                        {

                        }

                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.TriGiaNT, MidpointRounding.AwayFromZero);
                        tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT + hmd.PhuThu;
                        tongPhuThu = hmd.PhuThu;
                        #endregion
                    }
                    if (this.TKMD.HMDCollection.Count >= 2)
                    {
                        #region Dong hang 2
                        HangMauDich hmd = this.TKMD.HMDCollection[1];
                        if (!inMaHang)
                            TenHang2.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang2.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang2.Text = hmd.TenHang;
                        }
                        TenHang2.WordWrap = true;
                        try
                        {
                            TenHang2.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        }
                        catch
                        {
                            MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                            return;
                        }
                        MaHS2.Text = hmd.MaHS;
                        XuatXu2.Text = hmd.NuocXX_ID;
                        Luong2.Text = hmd.SoLuong.ToString("G15");
                        DVT2.Text = DonViTinh.GetName(hmd.DVT_ID);

                        DonGiaNT2.Text = hmd.DonGiaKB.ToString("G10");
                        TriGiaNT2.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));
                        TriGiaTT2.Text = hmd.TriGiaTT.ToString("N0");

                        TriGiaThuKhac2.Text = hmd.PhuThu.ToString("N0");
                        TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                        if (hmd.TyLeThuKhac + hmd.PhuThu == 0)
                        {
                            TriGiaThuKhac2.Text = TyLeThuKhac2.Text = string.Empty;
                        }
                        if (hmd.TyLeThuKhac + hmd.PhuThu == 0)
                        {
                            TriGiaThuKhac1.Text = TyLeThuKhac1.Text = string.Empty;
                        }
                        TienThueXNK2.Text = hmd.ThueXNK.ToString("N0");
                        ThueSuatXNK2.Text = hmd.ThueSuatXNK.ToString("N0");

                        tongTienThueXNK += hmd.ThueXNK;

                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTGTGT.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Trim() == "")
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatVATGiam;
                            TienThueGTGT2.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");

                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                            TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac2.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac2.Text = hmd.TriGiaThuKhac.ToString("N0");
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT2.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT2.Text = hmd.ThueSuatTTDBGiam;
                            TienThueGTGT2.Text = hmd.ThueTTDB.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Length == 0)
                            //    TyLeThuKhac2.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    TyLeThuKhac2.Text = hmd.ThueSuatVATGiam;
                            TriGiaThuKhac2.Text = hmd.ThueGTGT.ToString("N0");
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                        //}

                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.TriGiaNT, MidpointRounding.AwayFromZero);
                        tongTienThueTatCa = tongTienThueTatCa + hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT + hmd.PhuThu;
                        tongPhuThu += hmd.PhuThu;
                        #endregion
                    }
                    if (this.TKMD.HMDCollection.Count == 3)
                    {
                        #region Dong hang 3
                        HangMauDich hmd = this.TKMD.HMDCollection[2];
                        if (!inMaHang)
                            TenHang3.Text = hmd.TenHang;
                        else
                        {
                            if (hmd.MaPhu.Trim().Length > 0)
                                TenHang3.Text = hmd.TenHang + "/" + hmd.MaPhu;
                            else
                                TenHang3.Text = hmd.TenHang;
                        }
                        TenHang3.WordWrap = true;
                        try
                        {
                            TenHang3.Font = new Font("Times New Roman", float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang")));
                        }
                        catch
                        {
                            MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                            return;
                        }
                        MaHS3.Text = hmd.MaHS;
                        XuatXu3.Text = hmd.NuocXX_ID;
                        Luong3.Text = hmd.SoLuong.ToString("G15");
                        DVT3.Text = DonViTinh.GetName(hmd.DVT_ID);

                        DonGiaNT3.Text = hmd.DonGiaKB.ToString("G10");
                        TriGiaNT3.Text = hmd.TriGiaKB.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));
                        TriGiaTT3.Text = hmd.TriGiaTT.ToString("N0");

                        TriGiaThuKhac3.Text = hmd.PhuThu.ToString("N0");
                        TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");

                        if (hmd.TyLeThuKhac + hmd.PhuThu == 0)
                        {
                            TriGiaThuKhac3.Text = TyLeThuKhac3.Text = string.Empty;
                        }

                        TienThueXNK3.Text = hmd.ThueXNK.ToString("N0");
                        ThueSuatXNK3.Text = hmd.ThueSuatXNK.ToString("N0");

                        tongTienThueXNK += hmd.ThueXNK;
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTGTGT.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Trim() == "")
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatVATGiam;
                            TienThueGTGT3.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");

                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                            TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");
                            TriGiaThuKhac3.Text = hmd.TriGiaThuKhac.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");

                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                            TriGiaTTGTGT3.Text = TriGiaTTTTDB.ToString("N0");
                            //if (hmd.ThueSuatTTDBGiam.Trim() == "")
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDB.ToString("N0");
                            //else
                            //    ThueSuatGTGT3.Text = hmd.ThueSuatTTDBGiam;
                            TienThueGTGT3.Text = hmd.ThueTTDB.ToString("N0");
                            //if (hmd.ThueSuatVATGiam.Length == 0)
                            //    TyLeThuKhac3.Text = hmd.ThueSuatGTGT.ToString("N0");
                            //else
                            //    TyLeThuKhac3.Text = hmd.ThueSuatVATGiam;
                            TriGiaThuKhac3.Text = hmd.ThueGTGT.ToString("N0");
                            TyLeThuKhac3.Text = hmd.TyLeThuKhac.ToString("N0");

                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                        //}
                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.TriGiaNT, MidpointRounding.AwayFromZero);
                        tongTienThueTatCa += hmd.ThueXNK + hmd.TriGiaThuKhac + hmd.ThueTTDB + hmd.ThueGTGT + hmd.PhuThu;
                        tongPhuThu += hmd.PhuThu;
                        #endregion
                    }
                }
                else
                {
                    #region PHU LUC

                    TenHang1.Text = "HÀNG HÓA NHẬP";
                    TenHang2.Text = "(Chi tiết theo phụ lục đính kèm)";
                    tongPhuThu = 0;
                    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                    {
                        tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.TriGiaNT, MidpointRounding.AwayFromZero);
                        tongTienThueXNK += hmd.ThueXNK;
                        tongPhuThu += hmd.PhuThu;
                        tongTriGiaTT += hmd.TriGiaTT;
                        if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueGTGT;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                        }
                        else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                        {
                            tongTriGiaTTGTGT += hmd.TriGiaTT + hmd.ThueXNK;
                            tongThueGTGT += hmd.ThueTTDB;
                            tongTriGiaThuKhac += hmd.ThueGTGT;
                        }
                    }

                    //XuatXu1.Text = Nuoc.GetName(this.TKMD.HMDCollection[0].NuocXX_ID);

                    /* 
                     //Khi co phu luc thi khong hien thi :
                    TriGiaNT1.Text = tongTriGiaNT.ToString("N2");

                    TriGiaTT1.Text = tongTriGiaTT.ToString("N0");
                    TienThueXNK1.Text = tongTienThueXNK.ToString("N0");
                    TriGiaTTGTGT1.Text = tongTriGiaTTGTGT.ToString("N0");
                    TienThueGTGT1.Text = tongThueGTGT.ToString("N0");
                    TriGiaThuKhac1.Text = tongTriGiaThuKhac.ToString("N0");
                     */
                    #endregion
                }
                //Linhhtn - Không công phí VC và các phí khác vào Tổng trị giá nguyên tệ
                //tongTriGiaNT += Convert.ToDecimal(this.TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
                //3391.18055
                //tongTriGiaNT = decimal.Parse("3391,81055");
                lblTongTriGiaNT.Text = tongTriGiaNT.ToString(Company.KDT.SHARE.Components.Globals.GetPrecision(GlobalSettings.TriGiaNT));

                lblTongThueXNK.Text = tongTienThueXNK.ToString("N0");
                lblTongTriGiaThuKhac.Text = tongPhuThu == 0 ? string.Empty : tongPhuThu.ToString("N0");

                if (tongThueGTGT > 0)
                    lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
                else
                    lblTongTienThueGTGT.Text = "";

                //if (tongTriGiaThuKhac > 0)
                //    lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
                //else
                //    lblTongTriGiaThuKhac.Text = "";

                //30. Tong so tien thue & Thu khac
                lblTongThueXNKSo.Text = this.TinhTongThueHMD().ToString("N0");

                string s = Company.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
                s = s[0].ToString().ToUpper() + s.Substring(1);

                lblTongThueXNKChu.Text = s.Replace("  ", " ");
                lblTongTriGiaThuKhac.Text = tongPhuThu == 0 ? string.Empty : tongPhuThu.ToString("N0");
                if (MienThue1)
                {
                    TriGiaTT1.Text = "";
                    ThueSuatXNK1.Text = "";
                    TienThueXNK1.Text = "";
                    TriGiaTT2.Text = "";
                    ThueSuatXNK2.Text = "";
                    TienThueXNK2.Text = "";
                    TriGiaTT3.Text = "";
                    ThueSuatXNK3.Text = "";
                    TienThueXNK3.Text = "";
                    lblTongThueXNK.Text = "";
                    lblTongTriGiaThuKhac.Text = "";
                }
                if (MienThue2)
                {
                    TriGiaTTGTGT1.Text = "";
                    ThueSuatGTGT1.Text = "";
                    TienThueGTGT1.Text = "";
                    TriGiaTTGTGT2.Text = "";
                    ThueSuatGTGT2.Text = "";
                    TienThueGTGT2.Text = "";
                    TriGiaTTGTGT3.Text = "";
                    ThueSuatGTGT3.Text = "";
                    TienThueGTGT3.Text = "";
                    lblTongTienThueGTGT.Text = "";
                }

                if (MienThue1 && MienThue2)
                {
                    lblTongThueXNKSo.Text = lblTongThueXNKChu.Text = "";
                }

                //Neu co phu luc dinh kem -> khong hien thi gia tri thue
                if (this.TKMD.HMDCollection.Count > 3)
                {
                    lblTongThueXNK.Text = lblTongTienThueGTGT.Text = lblTongTriGiaThuKhac.Text = "";
                }

                //Ngay thang nam in to khai
                if (TKMD.NgayDangKy == new DateTime(1900, 1, 1))
                    lblNgayIn.Text = "Ngày " + DateTime.Today.Day + " tháng " + DateTime.Today.Month + " năm " + DateTime.Today.Year;
                else
                    lblNgayIn.Text = "Ngày " + TKMD.NgayTiepNhan.Day + " tháng " + TKMD.NgayTiepNhan.Month + " năm " + TKMD.NgayTiepNhan.Year;

                if (System.IO.File.Exists(System.Windows.Forms.Application.StartupPath + "\\Image\\lblMaVach.Image.gif"))
                    lblMaVach.Image = Image.FromFile(System.Windows.Forms.Application.StartupPath + "\\Image\\lblMaVach.Image.gif");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + " ";
            temp += s[s.Length - 1];
            return temp;
        }

        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac + hmd.PhuThu;
            }
            return tong;
        }
        public void setNhomHang(string tenNhomHang)
        {
            TenHang1.Text = tenNhomHang;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            //lblMienThueGTGT.Visible = t;
            //xrTable2.Visible = !t;
            //lblTongThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = lblTongThueXNKChu.Visible = !t;//xrTablesaaa.Visible = lblTongThueXNKChu.Visible = !t;

        }
        public void setThongTinHang(XRControl cell, string thongTin)
        {

            if (cell.Name.Equals("lblDeXuatKhac"))
                cell.Text = "32. Ghi chép khác: " + thongTin;
            else if (cell.Name.Equals("lblChiCucHQCK")) { }
            else
                cell.Text = thongTin;
        }
        public void setThongTinChiCucHQCuaKhau(string thongTin)
        {
            lblChiCucHQCK.Text = thongTin;
        }

        public void setDeXuatKhac(string deXuatKhac)
        {
            lblDeXuatKhac.Text = "32. Ghi chép khác: " + deXuatKhac;
        }

        private void TriGiaTT1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {

        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblDeXuatKhac_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblNgayIn_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void lblChiCucHQCK_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtChiCucHQCuaKhau.Text = cell.Text;
            //report.lblChiCucHQCuaKhau.Text = cell.Tag.ToString();
        }

        //public void BindData()
        //{
        //    DateTime minDate = new DateTime(1900, 1, 1);            
        //    //BindData of Detail
        //    lblSTT.DataBindings.Add("Text", this.DataSource, "SoThuTuHang");
        //    lblTenHang.DataBindings.Add("Text", this.DataSource, "TenHang");
        //    lblMaSoHH.DataBindings.Add("Text", this.DataSource, "MaHS");
        //    lblXuatXu.DataBindings.Add("Text", this.DataSource, "XuatXu");
        //    lblSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuong");
        //    lblDonViTinh.DataBindings.Add("Text", this.DataSource, "DonViTinh");
        //    lblDonGiaNT.DataBindings.Add("Text", this.DataSource, "DonGiaTT", "{0:N2}");
        //    lblTriGiaNT.DataBindings.Add("Text", this.DataSource, "TriGiaTT", "{0:N2}");

        //    //BindData header
        //    lblCangDoHang.Text = TKMD.TenDaiLyTTHQ.ToString();
        //    lblCangXepHang.Text = TKMD.DiaDiemXepHang.ToString();
        //    //lblChiCucHQ.Text
        //    //lblChiCucHQCK.Text
        //    lblDieuKienGiaoHang.Text = DKGH;
        //    lblDongTienThanhToan.Text = DongTienTT;
        //    lblGiayPhep.Text = TKMD.SoGiayPhep;
        //    lblHoaDonThuongMai.Text = TKMD.SoHoaDonThuongMai;
        //    lblHopDong.Tag = TKMD.SoHopDong;
        //    lblLoaiHinh.Text =  TKMD.LoaiVanDon;
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayDangKy.Text = TKMD.NgayDangKy.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGiayPhep.Text = TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayGui.Text = TKMD.NgayTiepNhan.ToString();
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanGiayPhep.Text = TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHetHanHopDong.Text = TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHoaDon.Text = TKMD.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayHopDong.Text = TKMD.NgayHopDong.ToString("dd/MM/yyyy");
        //    if (TKMD.NgayDangKy > minDate)
        //        lblNgayVanTaiDon.Text = TKMD.NgayVanDon.ToString("dd/MM/yyyy");
        //    lblNguoiNK.Text = TKMD.TenChuHang;
        //    lblNguoiUyThac.Text = TKMD.TenDonViUT;
        //    lblNguoiXK.Text = TKMD.TenDonViDoiTac;
        //    lblNuocXK.Text = NuocXK;
        //    lblPhuongThucThanhToan.Text = PTTT_Name;
        //    lblPhuongTienVanTai.Text = PTVT_Name;
        //    //lblThamChieu.Text
        //    lblToKhai.Text = TKMD.SoToKhai.ToString();
        //    lblVanTaiDon.Text = TKMD.LoaiVanDon;
        //    lblTyGiaTinhThue.Text = TKMD.TyGiaTinhThue.ToString();
        //    lblHuongDan.Text = TKMD.HUONGDAN;
        //    string ctu = "";
        //    foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
        //    {
        //        ctu += ct.TenChungTu + ", ";
        //    }
        //    lblChungTu.Text = ctu;

        //}
    }
}
