﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using Company.BLL.SXXK;

namespace Company.Interface
{
    public partial class NPLSuDungTrongTKXuatForm : BaseForm
    {
        public long IDHopDong = 0;
        public string MaNPL = "";
        public string dateFrom = "";
        public string dateTo = "";
        public NPLSuDungTrongTKXuatForm()
        {
            InitializeComponent();
        }

        private void NPLXuatForm_Load(object sender, EventArgs e)
        {
            dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
            dgList.Tables[0].Columns["LuongNPL"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["SoLuong"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;            

            dgList.DataSource = NguyenPhuLieu.GetNPLQuyDoiTheoToKhaiXuat(MaNPL,dateFrom,dateTo).Tables[0]; 
            btnExcel.Text = setText("Xuất Excel","Export to Excel");
            
        }

        private void btnExcel_Click(object sender, EventArgs e)
        {
            if (dgList.GetRows().Length <1  ) return;
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporterNPL.Export(str);
                str.Close();
                //if (showMsg("MSG_MAL08", true) == "Yes")
                if (ShowMessage("Bạn có muốn mở File Excel này không ?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }
       
    }
}