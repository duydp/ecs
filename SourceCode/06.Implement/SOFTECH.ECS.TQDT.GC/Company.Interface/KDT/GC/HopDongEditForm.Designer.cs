﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Company.Interface.KDT.GC
{
    partial class HopDongEditForm
    {
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HopDongEditForm));
            Janus.Windows.GridEX.GridEXLayout dgNhomSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgNguyenPhuLieu_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgThietBi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.rfvSoHopDong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayKyHD = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ccNgayKyHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.rfvNgayKetThucHD = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.ccNgayKetThucHD = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dtNuocThueGC = new System.Data.DataTable();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn7 = new System.Data.DataColumn();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdAddThietBi = new Janus.Windows.UI.CommandBars.UICommand("cmdAddThietBi");
            this.cmdAddThemPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdAddThemPhuKien");
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmMainHDGC = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbTopBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.NhapHang1 = new Janus.Windows.UI.CommandBars.UICommand("NhapHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieuHD1 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieuHD");
            this.HuyKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("HuyKhaiBao");
            this.XacNhan1 = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.cmdCopyHD1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCopyHD");
            this.InPhieuTN1 = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.cmdAddLoaiSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdAddLoaiSanPham");
            this.cmdAddNguyenPhuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdAddNguyenPhuLieu");
            this.cmdAddSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSanPham");
            this.HuyKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("HuyKhaiBao");
            this.NhanDuLieuHD = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieuHD");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.NhapHang = new Janus.Windows.UI.CommandBars.UICommand("NhapHang");
            this.cmdAddLoaiSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddLoaiSanPham");
            this.cmdAddNguyenPhuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddNguyenPhuLieu");
            this.cmdAddSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddSanPham");
            this.cmdAddThietBi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAddThietBi");
            this.cmdExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExcel");
            this.cmdExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExcel");
            this.cmdNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLExcel");
            this.cmdSPExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSPExcel");
            this.cmdThietBi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietBi");
            this.cmdLoaiSPGCExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiSPGCExcel");
            this.cmdNPLExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLExcel");
            this.cmdSPExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdSPExcel");
            this.cmdThietBiExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdThietBi");
            this.cmdCopyHD = new Janus.Windows.UI.CommandBars.UICommand("cmdCopyHD");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.tabHopDong = new Janus.Windows.UI.Tab.UITab();
            this.tpNhomSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnThemSPGC = new Janus.Windows.EditControls.UIButton();
            this.dgNhomSanPham = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tpNguyenPhuLieu = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXoaNPL = new Janus.Windows.EditControls.UIButton();
            this.btnImportNPL = new Janus.Windows.EditControls.UIButton();
            this.btnThemNguyenPhuLieu = new Janus.Windows.EditControls.UIButton();
            this.dgNguyenPhuLieu = new Janus.Windows.GridEX.GridEX();
            this.tpSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnImportSP = new Janus.Windows.EditControls.UIButton();
            this.btnThemSanPham = new Janus.Windows.EditControls.UIButton();
            this.dgSanPham = new Janus.Windows.GridEX.GridEX();
            this.tpThietBi = new Janus.Windows.UI.Tab.UITabPage();
            this.btnXoaTB = new Janus.Windows.EditControls.UIButton();
            this.btnImportTB = new Janus.Windows.EditControls.UIButton();
            this.btnThemThietBi = new Janus.Windows.EditControls.UIButton();
            this.dgThietBi = new Janus.Windows.GridEX.GridEX();
            this.dataColumn2 = new System.Data.DataColumn();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDVDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDCDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.nuocHControl1 = new Company.Interface.Controls.NuocHControl();
            this.nguyenTeControl1 = new Company.Interface.Controls.NguyenTeControl();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.ccNgayDangKy = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ccNgayGiaHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ds = new System.Data.DataSet();
            this.dtLoaiSanPham = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvDate = new Company.Controls.CustomValidation.CompareValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKyHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKetThucHD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNuocThueGC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainHDGC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTopBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).BeginInit();
            this.tabHopDong.SuspendLayout();
            this.tpNhomSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNhomSanPham)).BeginInit();
            this.tpNguyenPhuLieu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).BeginInit();
            this.tpSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).BeginInit();
            this.tpThietBi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvDate)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(750, 403);
            // 
            // dataColumn11
            // 
            this.dataColumn11.AllowDBNull = false;
            this.dataColumn11.ColumnName = "nguyente_Ma";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "nguyente_Ten";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "nuoc_Ten";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn11,
            this.dataColumn12});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn11};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // rfvSoHopDong
            // 
            this.rfvSoHopDong.ControlToValidate = this.txtSoHopDong;
            this.rfvSoHopDong.ErrorMessage = "\"Số hợp đồng\" không được bỏ trống.";
            this.rfvSoHopDong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHopDong.Icon")));
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.BackColor = System.Drawing.Color.White;
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(146, 75);
            this.txtSoHopDong.MaxLength = 40;
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(100, 21);
            this.txtSoHopDong.TabIndex = 7;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayKyHD
            // 
            this.rfvNgayKyHD.ControlToValidate = this.ccNgayKyHD;
            this.rfvNgayKyHD.ErrorMessage = "\"Ngày ký hợp đồng\" không được bỏ trống.";
            this.rfvNgayKyHD.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayKyHD.Icon")));
            this.rfvNgayKyHD.Tag = "rfvNgayKyHD";
            // 
            // ccNgayKyHD
            // 
            this.ccNgayKyHD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayKyHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKyHD.DropDownCalendar.Name = "";
            this.ccNgayKyHD.DropDownCalendar.Visible = false;
            this.ccNgayKyHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKyHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKyHD.Location = new System.Drawing.Point(374, 75);
            this.ccNgayKyHD.Name = "ccNgayKyHD";
            this.ccNgayKyHD.Nullable = true;
            this.ccNgayKyHD.NullButtonText = "Xóa";
            this.ccNgayKyHD.ShowNullButton = true;
            this.ccNgayKyHD.Size = new System.Drawing.Size(100, 21);
            this.ccNgayKyHD.TabIndex = 9;
            this.ccNgayKyHD.TodayButtonText = "Hôm nay";
            this.ccNgayKyHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKyHD.VisualStyleManager = this.vsmMain;
            // 
            // rfvNgayKetThucHD
            // 
            this.rfvNgayKetThucHD.ControlToValidate = this.ccNgayKetThucHD;
            this.rfvNgayKetThucHD.ErrorMessage = "\"Ngày kết thúc hợp đồng\" không được bỏ trống.";
            this.rfvNgayKetThucHD.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayKetThucHD.Icon")));
            this.rfvNgayKetThucHD.Tag = "rfvNgayKetThucHD";
            // 
            // ccNgayKetThucHD
            // 
            this.ccNgayKetThucHD.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayKetThucHD.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayKetThucHD.DropDownCalendar.Name = "";
            this.ccNgayKetThucHD.DropDownCalendar.Visible = false;
            this.ccNgayKetThucHD.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKetThucHD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayKetThucHD.Location = new System.Drawing.Point(146, 102);
            this.ccNgayKetThucHD.Name = "ccNgayKetThucHD";
            this.ccNgayKetThucHD.Nullable = true;
            this.ccNgayKetThucHD.NullButtonText = "Xóa";
            this.ccNgayKetThucHD.ShowNullButton = true;
            this.ccNgayKetThucHD.Size = new System.Drawing.Size(100, 21);
            this.ccNgayKetThucHD.TabIndex = 11;
            this.ccNgayKetThucHD.TodayButtonText = "Hôm nay";
            this.ccNgayKetThucHD.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayKetThucHD.VisualStyleManager = this.vsmMain;
            // 
            // dataColumn9
            // 
            this.dataColumn9.AllowDBNull = false;
            this.dataColumn9.ColumnName = "nuoc_Ma";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "lsp_Gia";
            this.dataColumn5.DataType = typeof(decimal);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "donvitinh_Ma";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "lsp_SoLuong";
            this.dataColumn4.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "donvihaiquan_Ten";
            // 
            // dtNuocThueGC
            // 
            this.dtNuocThueGC.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn9,
            this.dataColumn10});
            this.dtNuocThueGC.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtNuocThueGC.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn9};
            this.dtNuocThueGC.TableName = "NuocThueGC";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn7,
            this.dataColumn8});
            this.dtDonViHaiQuan.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "donvihaiquan_Ma"}, true)});
            this.dtDonViHaiQuan.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn7};
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn7
            // 
            this.dataColumn7.AllowDBNull = false;
            this.dataColumn7.ColumnName = "donvihaiquan_Ma";
            // 
            // cmdSave
            // 
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            this.cmdSave.ToolTipText = "Lưu thông tin (Ctrl + S)";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlG;
            this.cmdSend.Text = "Gửi thông tin";
            this.cmdSend.ToolTipText = "Gửi thông tin (Ctrl + G)";
            // 
            // cmdAddThietBi
            // 
            this.cmdAddThietBi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddThietBi.Icon")));
            this.cmdAddThietBi.Key = "cmdAddThietBi";
            this.cmdAddThietBi.Name = "cmdAddThietBi";
            this.cmdAddThietBi.Shortcut = System.Windows.Forms.Shortcut.Ctrl4;
            this.cmdAddThietBi.Text = "Thêm Thiết Bị";
            this.cmdAddThietBi.ToolTipText = "Thêm Thiết Bị (Ctrl + 4)";
            // 
            // cmdAddThemPhuKien
            // 
            this.cmdAddThemPhuKien.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddThemPhuKien.Icon")));
            this.cmdAddThemPhuKien.Key = "cmdAddThemPhuKien";
            this.cmdAddThemPhuKien.Name = "cmdAddThemPhuKien";
            this.cmdAddThemPhuKien.Shortcut = System.Windows.Forms.Shortcut.Ctrl5;
            this.cmdAddThemPhuKien.Text = "Thêm phụ kiên";
            this.cmdAddThemPhuKien.ToolTipText = "Thêm phụ kiên (Ctrl + 5)";
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMainHDGC;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(638, 32);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 356);
            // 
            // cmMainHDGC
            // 
            this.cmMainHDGC.BottomRebar = this.BottomRebar1;
            this.cmMainHDGC.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbTopBar});
            this.cmMainHDGC.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddLoaiSanPham,
            this.cmdAddNguyenPhuLieu,
            this.cmdAddSanPham,
            this.cmdAddThietBi,
            this.cmdAddThemPhuKien,
            this.cmdSave,
            this.cmdSend,
            this.HuyKhaiBao,
            this.NhanDuLieuHD,
            this.XacNhan,
            this.NhapHang,
            this.cmdExcel,
            this.cmdLoaiSPGCExcel,
            this.cmdNPLExcel,
            this.cmdSPExcel,
            this.cmdThietBiExcel,
            this.cmdCopyHD,
            this.InPhieuTN});
            this.cmMainHDGC.ContainerControl = this;
            this.cmMainHDGC.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMainHDGC.ImageList = this.ImageList1;
            this.cmMainHDGC.LeftRebar = this.LeftRebar1;
            this.cmMainHDGC.RightRebar = this.RightRebar1;
            this.cmMainHDGC.ShowShortcutInToolTips = true;
            this.cmMainHDGC.Tag = null;
            this.cmMainHDGC.TopRebar = this.TopRebar1;
            this.cmMainHDGC.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMainHDGC.VisualStyleManager = this.vsmMain;
            this.cmMainHDGC.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMainHDGC;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 388);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(638, 0);
            // 
            // cmbTopBar
            // 
            this.cmbTopBar.CommandManager = this.cmMainHDGC;
            this.cmbTopBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.NhapHang1,
            this.cmdSave1,
            this.Separator1,
            this.cmdSend1,
            this.NhanDuLieuHD1,
            this.HuyKhaiBao1,
            this.XacNhan1,
            this.cmdCopyHD1,
            this.InPhieuTN1});
            this.cmbTopBar.Key = "cmbTopBar";
            this.cmbTopBar.Location = new System.Drawing.Point(0, 0);
            this.cmbTopBar.Name = "cmbTopBar";
            this.cmbTopBar.RowIndex = 0;
            this.cmbTopBar.Size = new System.Drawing.Size(750, 32);
            this.cmbTopBar.Text = "cmbTopBar";
            // 
            // NhapHang1
            // 
            this.NhapHang1.Key = "NhapHang";
            this.NhapHang1.Name = "NhapHang1";
            this.NhapHang1.Text = "Nhậ&p hàng";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.cmdSave1.Text = "&Lưu thông tin";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            this.cmdSend1.Shortcut = System.Windows.Forms.Shortcut.CtrlK;
            this.cmdSend1.Text = "&Khai báo";
            // 
            // NhanDuLieuHD1
            // 
            this.NhanDuLieuHD1.Icon = ((System.Drawing.Icon)(resources.GetObject("NhanDuLieuHD1.Icon")));
            this.NhanDuLieuHD1.Key = "NhanDuLieuHD";
            this.NhanDuLieuHD1.Name = "NhanDuLieuHD1";
            this.NhanDuLieuHD1.Shortcut = System.Windows.Forms.Shortcut.CtrlN;
            this.NhanDuLieuHD1.Text = "&Nhận dữ liệu";
            // 
            // HuyKhaiBao1
            // 
            this.HuyKhaiBao1.Icon = ((System.Drawing.Icon)(resources.GetObject("HuyKhaiBao1.Icon")));
            this.HuyKhaiBao1.Key = "HuyKhaiBao";
            this.HuyKhaiBao1.Name = "HuyKhaiBao1";
            this.HuyKhaiBao1.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.HuyKhaiBao1.Text = "&Hủy khai báo";
            // 
            // XacNhan1
            // 
            this.XacNhan1.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan1.Icon")));
            this.XacNhan1.Key = "XacNhan";
            this.XacNhan1.Name = "XacNhan1";
            this.XacNhan1.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.XacNhan1.Text = "&Xác nhận";
            this.XacNhan1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // cmdCopyHD1
            // 
            this.cmdCopyHD1.ImageIndex = 4;
            this.cmdCopyHD1.Key = "cmdCopyHD";
            this.cmdCopyHD1.Name = "cmdCopyHD1";
            this.cmdCopyHD1.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCopyHD1.Text = "&Sao chép HĐ";
            // 
            // InPhieuTN1
            // 
            this.InPhieuTN1.ImageIndex = 5;
            this.InPhieuTN1.Key = "InPhieuTN";
            this.InPhieuTN1.Name = "InPhieuTN1";
            // 
            // cmdAddLoaiSanPham
            // 
            this.cmdAddLoaiSanPham.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddLoaiSanPham.Icon")));
            this.cmdAddLoaiSanPham.Key = "cmdAddLoaiSanPham";
            this.cmdAddLoaiSanPham.Name = "cmdAddLoaiSanPham";
            this.cmdAddLoaiSanPham.Shortcut = System.Windows.Forms.Shortcut.Ctrl0;
            this.cmdAddLoaiSanPham.Text = "Thêm Loại SPGC";
            this.cmdAddLoaiSanPham.ToolTipText = "Thêm Loại SPGC (Ctrl + 1)";
            // 
            // cmdAddNguyenPhuLieu
            // 
            this.cmdAddNguyenPhuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddNguyenPhuLieu.Icon")));
            this.cmdAddNguyenPhuLieu.Key = "cmdAddNguyenPhuLieu";
            this.cmdAddNguyenPhuLieu.Name = "cmdAddNguyenPhuLieu";
            this.cmdAddNguyenPhuLieu.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            this.cmdAddNguyenPhuLieu.Text = "Thêm Nguyên Phụ Liệu";
            this.cmdAddNguyenPhuLieu.ToolTipText = "Thêm Nguyên Phụ Liệu (Ctrl + 2)";
            // 
            // cmdAddSanPham
            // 
            this.cmdAddSanPham.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAddSanPham.Icon")));
            this.cmdAddSanPham.Key = "cmdAddSanPham";
            this.cmdAddSanPham.Name = "cmdAddSanPham";
            this.cmdAddSanPham.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            this.cmdAddSanPham.Text = "Thêm Sản Phẩm";
            this.cmdAddSanPham.ToolTipText = "Thêm Sản Phẩm (Ctrl + 3)";
            // 
            // HuyKhaiBao
            // 
            this.HuyKhaiBao.Key = "HuyKhaiBao";
            this.HuyKhaiBao.Name = "HuyKhaiBao";
            this.HuyKhaiBao.Text = "Hủy khai báo";
            // 
            // NhanDuLieuHD
            // 
            this.NhanDuLieuHD.Key = "NhanDuLieuHD";
            this.NhanDuLieuHD.Name = "NhanDuLieuHD";
            this.NhanDuLieuHD.Text = "Nhận dữ liệu";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // NhapHang
            // 
            this.NhapHang.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdAddLoaiSanPham1,
            this.cmdAddNguyenPhuLieu1,
            this.cmdAddSanPham1,
            this.cmdAddThietBi1,
            this.cmdExcel1});
            this.NhapHang.Icon = ((System.Drawing.Icon)(resources.GetObject("NhapHang.Icon")));
            this.NhapHang.Key = "NhapHang";
            this.NhapHang.Name = "NhapHang";
            this.NhapHang.Text = "Nhập hàng";
            // 
            // cmdAddLoaiSanPham1
            // 
            this.cmdAddLoaiSanPham1.Key = "cmdAddLoaiSanPham";
            this.cmdAddLoaiSanPham1.Name = "cmdAddLoaiSanPham1";
            this.cmdAddLoaiSanPham1.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            this.cmdAddLoaiSanPham1.ToolTipText = "Thêm Loại SPGC";
            // 
            // cmdAddNguyenPhuLieu1
            // 
            this.cmdAddNguyenPhuLieu1.Key = "cmdAddNguyenPhuLieu";
            this.cmdAddNguyenPhuLieu1.Name = "cmdAddNguyenPhuLieu1";
            this.cmdAddNguyenPhuLieu1.ToolTipText = "Thêm Nguyên Phụ Liệu";
            // 
            // cmdAddSanPham1
            // 
            this.cmdAddSanPham1.Key = "cmdAddSanPham";
            this.cmdAddSanPham1.Name = "cmdAddSanPham1";
            this.cmdAddSanPham1.ToolTipText = "Thêm Sản Phẩm";
            // 
            // cmdAddThietBi1
            // 
            this.cmdAddThietBi1.Key = "cmdAddThietBi";
            this.cmdAddThietBi1.Name = "cmdAddThietBi1";
            this.cmdAddThietBi1.ToolTipText = "Thêm Thiết Bị";
            // 
            // cmdExcel1
            // 
            this.cmdExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExcel1.Icon")));
            this.cmdExcel1.Key = "cmdExcel";
            this.cmdExcel1.Name = "cmdExcel1";
            this.cmdExcel1.ToolTipText = "Nhập từ Excel";
            // 
            // cmdExcel
            // 
            this.cmdExcel.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPL1,
            this.cmdSPExcel1,
            this.cmdThietBi1});
            this.cmdExcel.Key = "cmdExcel";
            this.cmdExcel.Name = "cmdExcel";
            this.cmdExcel.Text = "Nhập từ Excel";
            // 
            // cmdNPL1
            // 
            this.cmdNPL1.Key = "cmdNPLExcel";
            this.cmdNPL1.Name = "cmdNPL1";
            this.cmdNPL1.Shortcut = System.Windows.Forms.Shortcut.CtrlQ;
            // 
            // cmdSPExcel1
            // 
            this.cmdSPExcel1.Key = "cmdSPExcel";
            this.cmdSPExcel1.Name = "cmdSPExcel1";
            this.cmdSPExcel1.Shortcut = System.Windows.Forms.Shortcut.CtrlR;
            // 
            // cmdThietBi1
            // 
            this.cmdThietBi1.Key = "cmdThietBi";
            this.cmdThietBi1.Name = "cmdThietBi1";
            this.cmdThietBi1.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            // 
            // cmdLoaiSPGCExcel
            // 
            this.cmdLoaiSPGCExcel.Key = "cmdLoaiSPGCExcel";
            this.cmdLoaiSPGCExcel.Name = "cmdLoaiSPGCExcel";
            this.cmdLoaiSPGCExcel.Text = "Thêm Loại SPGC";
            // 
            // cmdNPLExcel
            // 
            this.cmdNPLExcel.Key = "cmdNPLExcel";
            this.cmdNPLExcel.Name = "cmdNPLExcel";
            this.cmdNPLExcel.Text = "Thêm Nguyên Phụ Liệu";
            // 
            // cmdSPExcel
            // 
            this.cmdSPExcel.Key = "cmdSPExcel";
            this.cmdSPExcel.Name = "cmdSPExcel";
            this.cmdSPExcel.Text = "Thêm Sản Phẩm";
            // 
            // cmdThietBiExcel
            // 
            this.cmdThietBiExcel.Key = "cmdThietBi";
            this.cmdThietBiExcel.Name = "cmdThietBiExcel";
            this.cmdThietBiExcel.Text = "Thêm Thiết Bị";
            // 
            // cmdCopyHD
            // 
            this.cmdCopyHD.Key = "cmdCopyHD";
            this.cmdCopyHD.Name = "cmdCopyHD";
            this.cmdCopyHD.Text = "Sao chép HĐ";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Shortcut = System.Windows.Forms.Shortcut.CtrlI;
            this.InPhieuTN.Text = "&In phiếu tiếp nhận";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMainHDGC;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 32);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 356);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbTopBar});
            this.TopRebar1.CommandManager = this.cmMainHDGC;
            this.TopRebar1.Controls.Add(this.cmbTopBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(750, 32);
            // 
            // tabHopDong
            // 
            this.tabHopDong.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabHopDong.BackColor = System.Drawing.Color.Transparent;
            this.tabHopDong.Location = new System.Drawing.Point(0, 200);
            this.tabHopDong.Name = "tabHopDong";
            this.tabHopDong.Size = new System.Drawing.Size(750, 212);
            this.tabHopDong.TabIndex = 1;
            this.tabHopDong.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tpNhomSanPham,
            this.tpNguyenPhuLieu,
            this.tpSanPham,
            this.tpThietBi});
            this.tabHopDong.TabsStateStyles.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.tabHopDong.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.tabHopDong.VisualStyleManager = this.vsmMain;
            // 
            // tpNhomSanPham
            // 
            this.tpNhomSanPham.Controls.Add(this.btnXoa);
            this.tpNhomSanPham.Controls.Add(this.btnThemSPGC);
            this.tpNhomSanPham.Controls.Add(this.dgNhomSanPham);
            this.tpNhomSanPham.Location = new System.Drawing.Point(1, 21);
            this.tpNhomSanPham.Name = "tpNhomSanPham";
            this.tpNhomSanPham.Size = new System.Drawing.Size(748, 190);
            this.tpNhomSanPham.TabStop = true;
            this.tpNhomSanPham.Text = "Loại Sản phẩm gia công";
            // 
            // btnXoa
            // 
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(94, 6);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 9;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click_1);
            // 
            // btnThemSPGC
            // 
            this.btnThemSPGC.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemSPGC.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThemSPGC.Icon")));
            this.btnThemSPGC.Location = new System.Drawing.Point(3, 6);
            this.btnThemSPGC.Name = "btnThemSPGC";
            this.btnThemSPGC.Size = new System.Drawing.Size(83, 23);
            this.btnThemSPGC.TabIndex = 7;
            this.btnThemSPGC.Text = "Thêm mới";
            this.btnThemSPGC.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnThemSPGC.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThemSPGC.VisualStyleManager = this.vsmMain;
            this.btnThemSPGC.Click += new System.EventHandler(this.btnClickEvent);
            // 
            // dgNhomSanPham
            // 
            this.dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNhomSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNhomSanPham.AlternatingColors = true;
            this.dgNhomSanPham.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgNhomSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNhomSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNhomSanPham.ColumnAutoResize = true;
            dgNhomSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgNhomSanPham_DesignTimeLayout.LayoutString");
            this.dgNhomSanPham.DesignTimeLayout = dgNhomSanPham_DesignTimeLayout;
            this.dgNhomSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNhomSanPham.GroupByBoxVisible = false;
            this.dgNhomSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNhomSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNhomSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNhomSanPham.ImageList = this.ImageList1;
            this.dgNhomSanPham.Location = new System.Drawing.Point(0, 32);
            this.dgNhomSanPham.Name = "dgNhomSanPham";
            this.dgNhomSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNhomSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNhomSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNhomSanPham.Size = new System.Drawing.Size(748, 190);
            this.dgNhomSanPham.TabIndex = 0;
            this.dgNhomSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNhomSanPham.VisualStyleManager = this.vsmMain;
            this.dgNhomSanPham.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgNhomSanPham_RowDoubleClick);
            this.dgNhomSanPham.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgNhomSanPham_DeletingRecords);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "copy24.ico");
            this.ImageList1.Images.SetKeyName(5, "printer.png");
            // 
            // tpNguyenPhuLieu
            // 
            this.tpNguyenPhuLieu.Controls.Add(this.btnXoaNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.btnImportNPL);
            this.tpNguyenPhuLieu.Controls.Add(this.btnThemNguyenPhuLieu);
            this.tpNguyenPhuLieu.Controls.Add(this.dgNguyenPhuLieu);
            this.tpNguyenPhuLieu.Location = new System.Drawing.Point(1, 21);
            this.tpNguyenPhuLieu.Name = "tpNguyenPhuLieu";
            this.tpNguyenPhuLieu.Size = new System.Drawing.Size(748, 190);
            this.tpNguyenPhuLieu.TabStop = true;
            this.tpNguyenPhuLieu.Text = "Nguyên phụ liệu";
            // 
            // btnXoaNPL
            // 
            this.btnXoaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoaNPL.Icon")));
            this.btnXoaNPL.Location = new System.Drawing.Point(234, 6);
            this.btnXoaNPL.Name = "btnXoaNPL";
            this.btnXoaNPL.Size = new System.Drawing.Size(75, 23);
            this.btnXoaNPL.TabIndex = 10;
            this.btnXoaNPL.Text = "Xóa";
            this.btnXoaNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoaNPL.VisualStyleManager = this.vsmMain;
            this.btnXoaNPL.Click += new System.EventHandler(this.btnXoaNPL_Click);
            // 
            // btnImportNPL
            // 
            this.btnImportNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("btnImportNPL.Icon")));
            this.btnImportNPL.Location = new System.Drawing.Point(91, 6);
            this.btnImportNPL.Name = "btnImportNPL";
            this.btnImportNPL.Size = new System.Drawing.Size(138, 23);
            this.btnImportNPL.TabIndex = 9;
            this.btnImportNPL.Text = "Thêm mới từ Excel";
            this.btnImportNPL.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnImportNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnImportNPL.VisualStyleManager = this.vsmMain;
            this.btnImportNPL.Click += new System.EventHandler(this.btnClickEvent);
            // 
            // btnThemNguyenPhuLieu
            // 
            this.btnThemNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemNguyenPhuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThemNguyenPhuLieu.Icon")));
            this.btnThemNguyenPhuLieu.Location = new System.Drawing.Point(3, 6);
            this.btnThemNguyenPhuLieu.Name = "btnThemNguyenPhuLieu";
            this.btnThemNguyenPhuLieu.Size = new System.Drawing.Size(83, 23);
            this.btnThemNguyenPhuLieu.TabIndex = 8;
            this.btnThemNguyenPhuLieu.Text = "Thêm mới";
            this.btnThemNguyenPhuLieu.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnThemNguyenPhuLieu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThemNguyenPhuLieu.VisualStyleManager = this.vsmMain;
            this.btnThemNguyenPhuLieu.Click += new System.EventHandler(this.btnClickEvent);
            // 
            // dgNguyenPhuLieu
            // 
            this.dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgNguyenPhuLieu.AlternatingColors = true;
            this.dgNguyenPhuLieu.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgNguyenPhuLieu.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgNguyenPhuLieu.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgNguyenPhuLieu.ColumnAutoResize = true;
            dgNguyenPhuLieu_DesignTimeLayout.LayoutString = resources.GetString("dgNguyenPhuLieu_DesignTimeLayout.LayoutString");
            this.dgNguyenPhuLieu.DesignTimeLayout = dgNguyenPhuLieu_DesignTimeLayout;
            this.dgNguyenPhuLieu.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgNguyenPhuLieu.GroupByBoxVisible = false;
            this.dgNguyenPhuLieu.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgNguyenPhuLieu.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgNguyenPhuLieu.ImageList = this.ImageList1;
            this.dgNguyenPhuLieu.Location = new System.Drawing.Point(0, 32);
            this.dgNguyenPhuLieu.Name = "dgNguyenPhuLieu";
            this.dgNguyenPhuLieu.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgNguyenPhuLieu.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgNguyenPhuLieu.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgNguyenPhuLieu.Size = new System.Drawing.Size(748, 147);
            this.dgNguyenPhuLieu.TabIndex = 1;
            this.dgNguyenPhuLieu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgNguyenPhuLieu.VisualStyleManager = this.vsmMain;
            this.dgNguyenPhuLieu.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgNguyenPhuLieu_RowDoubleClick);
            this.dgNguyenPhuLieu.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgNguyenPhuLieu_DeletingRecords);
            this.dgNguyenPhuLieu.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgNguyenPhuLieu_LoadingRow);
            // 
            // tpSanPham
            // 
            this.tpSanPham.Controls.Add(this.uiButton1);
            this.tpSanPham.Controls.Add(this.btnImportSP);
            this.tpSanPham.Controls.Add(this.btnThemSanPham);
            this.tpSanPham.Controls.Add(this.dgSanPham);
            this.tpSanPham.Location = new System.Drawing.Point(1, 21);
            this.tpSanPham.Name = "tpSanPham";
            this.tpSanPham.Size = new System.Drawing.Size(748, 190);
            this.tpSanPham.TabStop = true;
            this.tpSanPham.Text = "Sản phẩm";
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(234, 6);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(75, 23);
            this.uiButton1.TabIndex = 11;
            this.uiButton1.Text = "Xóa";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.btnXoaSP_Click);
            // 
            // btnImportSP
            // 
            this.btnImportSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportSP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnImportSP.Icon")));
            this.btnImportSP.Location = new System.Drawing.Point(91, 6);
            this.btnImportSP.Name = "btnImportSP";
            this.btnImportSP.Size = new System.Drawing.Size(138, 23);
            this.btnImportSP.TabIndex = 10;
            this.btnImportSP.Text = "Thêm mới từ Excel";
            this.btnImportSP.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnImportSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnImportSP.VisualStyleManager = this.vsmMain;
            this.btnImportSP.Click += new System.EventHandler(this.btnClickEvent);
            // 
            // btnThemSanPham
            // 
            this.btnThemSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemSanPham.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThemSanPham.Icon")));
            this.btnThemSanPham.Location = new System.Drawing.Point(3, 6);
            this.btnThemSanPham.Name = "btnThemSanPham";
            this.btnThemSanPham.Size = new System.Drawing.Size(83, 23);
            this.btnThemSanPham.TabIndex = 9;
            this.btnThemSanPham.Text = "Thêm mới";
            this.btnThemSanPham.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnThemSanPham.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThemSanPham.VisualStyleManager = this.vsmMain;
            // 
            // dgSanPham
            // 
            this.dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgSanPham.AlternatingColors = true;
            this.dgSanPham.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSanPham.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgSanPham.ColumnAutoResize = true;
            dgSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgSanPham_DesignTimeLayout.LayoutString");
            this.dgSanPham.DesignTimeLayout = dgSanPham_DesignTimeLayout;
            this.dgSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgSanPham.GroupByBoxVisible = false;
            this.dgSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgSanPham.ImageList = this.ImageList1;
            this.dgSanPham.Location = new System.Drawing.Point(0, 32);
            this.dgSanPham.Name = "dgSanPham";
            this.dgSanPham.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgSanPham.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgSanPham.Size = new System.Drawing.Size(748, 158);
            this.dgSanPham.TabIndex = 1;
            this.dgSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgSanPham.VisualStyleManager = this.vsmMain;
            this.dgSanPham.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgSanPham_RowDoubleClick);
            this.dgSanPham.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgSanPham_DeletingRecords);
            this.dgSanPham.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgSanPham_LoadingRow);
            // 
            // tpThietBi
            // 
            this.tpThietBi.Controls.Add(this.btnXoaTB);
            this.tpThietBi.Controls.Add(this.btnImportTB);
            this.tpThietBi.Controls.Add(this.btnThemThietBi);
            this.tpThietBi.Controls.Add(this.dgThietBi);
            this.tpThietBi.Location = new System.Drawing.Point(1, 21);
            this.tpThietBi.Name = "tpThietBi";
            this.tpThietBi.Size = new System.Drawing.Size(748, 131);
            this.tpThietBi.TabStop = true;
            this.tpThietBi.Text = "Thiết bị";
            // 
            // btnXoaTB
            // 
            this.btnXoaTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoaTB.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoaTB.Icon")));
            this.btnXoaTB.Location = new System.Drawing.Point(237, 6);
            this.btnXoaTB.Name = "btnXoaTB";
            this.btnXoaTB.Size = new System.Drawing.Size(75, 23);
            this.btnXoaTB.TabIndex = 12;
            this.btnXoaTB.Text = "Xóa";
            this.btnXoaTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoaTB.VisualStyleManager = this.vsmMain;
            this.btnXoaTB.Click += new System.EventHandler(this.btnXoaTB_Click_1);
            // 
            // btnImportTB
            // 
            this.btnImportTB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImportTB.Icon = ((System.Drawing.Icon)(resources.GetObject("btnImportTB.Icon")));
            this.btnImportTB.Location = new System.Drawing.Point(94, 6);
            this.btnImportTB.Name = "btnImportTB";
            this.btnImportTB.Size = new System.Drawing.Size(138, 23);
            this.btnImportTB.TabIndex = 11;
            this.btnImportTB.Text = "Thêm mới từ Excel";
            this.btnImportTB.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnImportTB.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnImportTB.VisualStyleManager = this.vsmMain;
            this.btnImportTB.Click += new System.EventHandler(this.btnClickEvent);
            // 
            // btnThemThietBi
            // 
            this.btnThemThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThemThietBi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnThemThietBi.Icon")));
            this.btnThemThietBi.Location = new System.Drawing.Point(6, 6);
            this.btnThemThietBi.Name = "btnThemThietBi";
            this.btnThemThietBi.Size = new System.Drawing.Size(83, 23);
            this.btnThemThietBi.TabIndex = 10;
            this.btnThemThietBi.Text = "Thêm mới";
            this.btnThemThietBi.TextHorizontalAlignment = Janus.Windows.EditControls.TextAlignment.Near;
            this.btnThemThietBi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnThemThietBi.VisualStyleManager = this.vsmMain;
            this.btnThemThietBi.Click += new System.EventHandler(this.btnClickEvent);
            // 
            // dgThietBi
            // 
            this.dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgThietBi.AlternatingColors = true;
            this.dgThietBi.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgThietBi.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgThietBi.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgThietBi_DesignTimeLayout.LayoutString = resources.GetString("dgThietBi_DesignTimeLayout.LayoutString");
            this.dgThietBi.DesignTimeLayout = dgThietBi_DesignTimeLayout;
            this.dgThietBi.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgThietBi.GroupByBoxVisible = false;
            this.dgThietBi.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgThietBi.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgThietBi.ImageList = this.ImageList1;
            this.dgThietBi.Location = new System.Drawing.Point(0, 32);
            this.dgThietBi.Name = "dgThietBi";
            this.dgThietBi.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgThietBi.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgThietBi.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgThietBi.Size = new System.Drawing.Size(748, 99);
            this.dgThietBi.TabIndex = 5;
            this.dgThietBi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgThietBi.VisualStyleManager = this.vsmMain;
            this.dgThietBi.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgThietBi_RowDoubleClick);
            this.dgThietBi.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgThietBi_DeletingRecords);
            this.dgThietBi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgThietBi_LoadingRow);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "lsp_Ten";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.tabHopDong);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 32);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(750, 403);
            this.uiGroupBox1.TabIndex = 5;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label16);
            this.uiGroupBox2.Controls.Add(this.label15);
            this.uiGroupBox2.Controls.Add(this.txtDVDT);
            this.uiGroupBox2.Controls.Add(this.txtDCDT);
            this.uiGroupBox2.Controls.Add(this.label13);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.nuocHControl1);
            this.uiGroupBox2.Controls.Add(this.nguyenTeControl1);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label11);
            this.uiGroupBox2.Controls.Add(this.ccNgayTiepNhan);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.ccNgayDangKy);
            this.uiGroupBox2.Controls.Add(this.ccNgayKyHD);
            this.uiGroupBox2.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.lblTrangThai);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.ccNgayGiaHan);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.ccNgayKetThucHD);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 6);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(750, 188);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin hợp đồng gia công";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(146, 20);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(293, 22);
            this.donViHaiQuanControl1.TabIndex = 29;
            this.donViHaiQuanControl1.VisualStyleManager = null;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(21, 29);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(119, 13);
            this.label7.TabIndex = 28;
            this.label7.Text = "Chọn hải quan khai báo";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(246, 110);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 27;
            this.label16.Text = "*";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(474, 80);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 26;
            this.label15.Text = "*";
            // 
            // txtDVDT
            // 
            this.txtDVDT.BackColor = System.Drawing.Color.White;
            this.txtDVDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVDT.Location = new System.Drawing.Point(585, 75);
            this.txtDVDT.MaxLength = 40;
            this.txtDVDT.Name = "txtDVDT";
            this.txtDVDT.Size = new System.Drawing.Size(100, 21);
            this.txtDVDT.TabIndex = 23;
            this.txtDVDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDVDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDVDT.VisualStyleManager = this.vsmMain;
            // 
            // txtDCDT
            // 
            this.txtDCDT.BackColor = System.Drawing.Color.White;
            this.txtDCDT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDCDT.Location = new System.Drawing.Point(585, 102);
            this.txtDCDT.MaxLength = 80;
            this.txtDCDT.Name = "txtDCDT";
            this.txtDCDT.Size = new System.Drawing.Size(100, 21);
            this.txtDCDT.TabIndex = 25;
            this.txtDCDT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDCDT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDCDT.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(505, 107);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 13);
            this.label13.TabIndex = 24;
            this.label13.Text = "Địa chỉ đối tác";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(505, 80);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Đơn vị đối tác";
            // 
            // nuocHControl1
            // 
            this.nuocHControl1.BackColor = System.Drawing.Color.Transparent;
            this.nuocHControl1.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.nuocHControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nuocHControl1.Location = new System.Drawing.Point(374, 128);
            this.nuocHControl1.Ma = "";
            this.nuocHControl1.Name = "nuocHControl1";
            this.nuocHControl1.ReadOnly = false;
            this.nuocHControl1.Size = new System.Drawing.Size(324, 22);
            this.nuocHControl1.TabIndex = 17;
            this.nuocHControl1.VisualStyleManager = this.vsmMain;
            // 
            // nguyenTeControl1
            // 
            this.nguyenTeControl1.BackColor = System.Drawing.Color.Transparent;
            this.nguyenTeControl1.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.nguyenTeControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nguyenTeControl1.Location = new System.Drawing.Point(374, 155);
            this.nguyenTeControl1.Ma = "";
            this.nguyenTeControl1.Name = "nguyenTeControl1";
            this.nguyenTeControl1.ReadOnly = false;
            this.nguyenTeControl1.Size = new System.Drawing.Size(324, 22);
            this.nguyenTeControl1.TabIndex = 21;
            this.nguyenTeControl1.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(274, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Nguyên tệ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(274, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Nước thuê gia công";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(274, 110);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 12;
            this.label11.Text = "Ngày tiếp nhận";
            // 
            // ccNgayTiepNhan
            // 
            this.ccNgayTiepNhan.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.Visible = false;
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(374, 102);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(100, 21);
            this.ccNgayTiepNhan.TabIndex = 13;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(23, 161);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "Ngày đăng ký";
            // 
            // ccNgayDangKy
            // 
            this.ccNgayDangKy.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccNgayDangKy.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayDangKy.DropDownCalendar.Name = "";
            this.ccNgayDangKy.DropDownCalendar.Visible = false;
            this.ccNgayDangKy.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDangKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDangKy.Location = new System.Drawing.Point(146, 156);
            this.ccNgayDangKy.Name = "ccNgayDangKy";
            this.ccNgayDangKy.Nullable = true;
            this.ccNgayDangKy.NullButtonText = "Xóa";
            this.ccNgayDangKy.ShowNullButton = true;
            this.ccNgayDangKy.Size = new System.Drawing.Size(100, 21);
            this.ccNgayDangKy.TabIndex = 19;
            this.ccNgayDangKy.TodayButtonText = "Hôm nay";
            this.ccNgayDangKy.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayDangKy.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.BackColor = System.Drawing.Color.White;
            this.txtSoTiepNhan.Location = new System.Drawing.Point(146, 48);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(23, 53);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số tiếp nhận";
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.Location = new System.Drawing.Point(371, 53);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(111, 13);
            this.lblTrangThai.TabIndex = 3;
            this.lblTrangThai.Text = "Chưa gửi thông tin";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(275, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Trạng thái";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(23, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Ngày kết thúc";
            // 
            // ccNgayGiaHan
            // 
            // 
            // 
            // 
            this.ccNgayGiaHan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccNgayGiaHan.DropDownCalendar.Name = "";
            this.ccNgayGiaHan.DropDownCalendar.Visible = false;
            this.ccNgayGiaHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiaHan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiaHan.IsNullDate = true;
            this.ccNgayGiaHan.Location = new System.Drawing.Point(146, 129);
            this.ccNgayGiaHan.Name = "ccNgayGiaHan";
            this.ccNgayGiaHan.Nullable = true;
            this.ccNgayGiaHan.NullButtonText = "Xóa";
            this.ccNgayGiaHan.ReadOnly = true;
            this.ccNgayGiaHan.ShowNullButton = true;
            this.ccNgayGiaHan.Size = new System.Drawing.Size(100, 21);
            this.ccNgayGiaHan.TabIndex = 15;
            this.ccNgayGiaHan.TodayButtonText = "Hôm nay";
            this.ccNgayGiaHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayGiaHan.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 134);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Ngày gia hạn";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(274, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Ngày ký hợp đồng";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(246, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "*";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(23, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Số hợp đồng";
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtLoaiSanPham,
            this.dtDonViHaiQuan,
            this.dtNuocThueGC,
            this.dtNguyenTe});
            // 
            // dtLoaiSanPham
            // 
            this.dtLoaiSanPham.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6});
            this.dtLoaiSanPham.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "lsp_Ma"}, true)});
            this.dtLoaiSanPham.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.dtLoaiSanPham.TableName = "LoaiSanPham";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "lsp_Ma";
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // cvDate
            // 
            this.cvDate.ControlToCompare = this.ccNgayKyHD;
            this.cvDate.ControlToValidate = this.ccNgayKetThucHD;
            this.cvDate.ErrorMessage = "\"Ngày kết thúc HĐ\" phải lớn hơn \"Ngày ký HĐ\"";
            this.cvDate.Icon = ((System.Drawing.Icon)(resources.GetObject("cvDate.Icon")));
            this.cvDate.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvDate.Tag = "cvDate";
            this.cvDate.Type = Company.Controls.CustomValidation.ValidationDataType.Date;
            // 
            // HopDongEditForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(750, 435);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "HopDongEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Hợp đồng gia công";
            this.Load += new System.EventHandler(this.HopDongEditForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HopDongEditForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHopDong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKyHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayKetThucHD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNuocThueGC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMainHDGC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTopBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabHopDong)).EndInit();
            this.tabHopDong.ResumeLayout(false);
            this.tpNhomSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNhomSanPham)).EndInit();
            this.tpNguyenPhuLieu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgNguyenPhuLieu)).EndInit();
            this.tpSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSanPham)).EndInit();
            this.tpThietBi.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgThietBi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvDate)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataTable dtNguyenTe;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoHopDong;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMainHDGC;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar cmbTopBar;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddLoaiSanPham;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddNguyenPhuLieu;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSanPham;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddThietBi;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddThemPhuKien;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.Tab.UITab tabHopDong;
        private Janus.Windows.UI.Tab.UITabPage tpNhomSanPham;
        private Janus.Windows.GridEX.GridEX dgNhomSanPham;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.UI.Tab.UITabPage tpNguyenPhuLieu;
        private Janus.Windows.UI.Tab.UITabPage tpSanPham;
        private Janus.Windows.UI.Tab.UITabPage tpThietBi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayKyHD;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayKetThucHD;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataTable dtNuocThueGC;
        private System.Data.DataTable dtDonViHaiQuan;
        private System.Data.DataColumn dataColumn7;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataSet ds;
        private System.Data.DataTable dtLoaiSanPham;
        private System.Data.DataColumn dataColumn1;
        private Janus.Windows.GridEX.GridEX dgThietBi;
        private Janus.Windows.GridEX.GridEX dgNguyenPhuLieu;
        private Janus.Windows.GridEX.GridEX dgSanPham;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.UI.CommandBars.UICommand HuyKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieuHD;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand NhapHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddLoaiSanPham1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddNguyenPhuLieu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddSanPham1;
        private Janus.Windows.UI.CommandBars.UICommand cmdAddThietBi1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private Company.Interface.Controls.NuocHControl nuocHControl1;
        private Company.Interface.Controls.NguyenTeControl nguyenTeControl1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayDangKy;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKyHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblTrangThai;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayGiaHan;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayKetThucHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVDT;
        private Janus.Windows.GridEX.EditControls.EditBox txtDCDT;
        private Janus.Windows.UI.CommandBars.UICommand cmdExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPL1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLoaiSPGCExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdSPExcel1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThietBi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSPExcel;
        private Janus.Windows.UI.CommandBars.UICommand cmdThietBiExcel;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.EditControls.UIButton btnThemSPGC;
        private Janus.Windows.EditControls.UIButton btnThemNguyenPhuLieu;
        private Janus.Windows.EditControls.UIButton btnThemSanPham;
        private Janus.Windows.EditControls.UIButton btnThemThietBi;
        private Janus.Windows.EditControls.UIButton btnImportNPL;
        private Janus.Windows.EditControls.UIButton btnImportSP;
        private Janus.Windows.EditControls.UIButton btnImportTB;
        private Janus.Windows.UI.CommandBars.UICommand cmdCopyHD;
        private Janus.Windows.UI.CommandBars.UICommand NhapHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand NhanDuLieuHD1;
        private Janus.Windows.UI.CommandBars.UICommand HuyKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCopyHD1;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnXoaNPL;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnXoaTB;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN;
        private Janus.Windows.UI.CommandBars.UICommand InPhieuTN1;
        private Company.Controls.CustomValidation.CompareValidator cvDate;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private System.Windows.Forms.Label label7;



    }
}
