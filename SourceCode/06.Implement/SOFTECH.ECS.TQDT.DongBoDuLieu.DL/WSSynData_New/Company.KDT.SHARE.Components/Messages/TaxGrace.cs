﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    /// <summary>
    /// Ân hạn thuế
    /// </summary>
    public class TaxGrace
    {
        ///<summary>
        ///Có được ân hạn thuế không
        ///</summary>
        [XmlElement("isGrace")]
        public string IsGrace { set; get; }
        ///<summary>
        ///Lý do ân hạn thuế
        ///</summary>
        [XmlElement("reason")]
        public string Reason { set; get; }
        ///<summary>
        ///Số ngày ân hạn
        ///</summary>
        [XmlElement("value")]
        public string Value { set; get; }
    }
}
