using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
	public partial class DeNghiChuyenCuaKhau
	{
		#region Properties.
		
		public long ID { set; get; }
		public string ThongTinKhac { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string GuidStr { set; get; }
		public int LoaiKB { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public int TrangThai { set; get; }
		public int NamTiepNhan { set; get; }
		public string SoVanDon { set; get; }
		public DateTime NgayVanDon { set; get; }
		public DateTime ThoiGianDen { set; get; }
		public string DiaDiemKiemTra { set; get; }
		public string TuyenDuong { set; get; }
		public long ID_TK { set; get; }
		public string LoaiTK { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<DeNghiChuyenCuaKhau> ConvertToCollection(IDataReader reader)
		{
			IList<DeNghiChuyenCuaKhau> collection = new List<DeNghiChuyenCuaKhau>();
			while (reader.Read())
			{
				DeNghiChuyenCuaKhau entity = new DeNghiChuyenCuaKhau();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianDen"))) entity.ThoiGianDen = reader.GetDateTime(reader.GetOrdinal("ThoiGianDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemKiemTra"))) entity.DiaDiemKiemTra = reader.GetString(reader.GetOrdinal("DiaDiemKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuyenDuong"))) entity.TuyenDuong = reader.GetString(reader.GetOrdinal("TuyenDuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("ID_TK"))) entity.ID_TK = reader.GetInt64(reader.GetOrdinal("ID_TK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiTK"))) entity.LoaiTK = reader.GetString(reader.GetOrdinal("LoaiTK"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DeNghiChuyenCuaKhau Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<DeNghiChuyenCuaKhau> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<DeNghiChuyenCuaKhau> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<DeNghiChuyenCuaKhau> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<DeNghiChuyenCuaKhau> SelectCollectionBy_ID_TK(long iD_TK)
		{
            IDataReader reader = SelectReaderBy_ID_TK(iD_TK);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_ID_TK(long iD_TK)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_SelectBy_ID_TK]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_TK", SqlDbType.BigInt, iD_TK);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_ID_TK(long iD_TK)
		{
			const string spName = "p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_SelectBy_ID_TK";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_TK", SqlDbType.BigInt, iD_TK);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertDeNghiChuyenCuaKhau(string thongTinKhac, string maDoanhNghiep, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string soVanDon, DateTime ngayVanDon, DateTime thoiGianDen, string diaDiemKiemTra, string tuyenDuong, long iD_TK, string loaiTK)
		{
			DeNghiChuyenCuaKhau entity = new DeNghiChuyenCuaKhau();	
			entity.ThongTinKhac = thongTinKhac;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			entity.NamTiepNhan = namTiepNhan;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.ThoiGianDen = thoiGianDen;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.TuyenDuong = tuyenDuong;
			entity.ID_TK = iD_TK;
			entity.LoaiTK = loaiTK;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@ThoiGianDen", SqlDbType.DateTime, ThoiGianDen.Year <= 1753 ? DBNull.Value : (object) ThoiGianDen);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);
			db.AddInParameter(dbCommand, "@ID_TK", SqlDbType.BigInt, ID_TK);
			db.AddInParameter(dbCommand, "@LoaiTK", SqlDbType.VarChar, LoaiTK);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<DeNghiChuyenCuaKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DeNghiChuyenCuaKhau item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateDeNghiChuyenCuaKhau(long id, string thongTinKhac, string maDoanhNghiep, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string soVanDon, DateTime ngayVanDon, DateTime thoiGianDen, string diaDiemKiemTra, string tuyenDuong, long iD_TK, string loaiTK)
		{
			DeNghiChuyenCuaKhau entity = new DeNghiChuyenCuaKhau();			
			entity.ID = id;
			entity.ThongTinKhac = thongTinKhac;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			entity.NamTiepNhan = namTiepNhan;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.ThoiGianDen = thoiGianDen;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.TuyenDuong = tuyenDuong;
			entity.ID_TK = iD_TK;
			entity.LoaiTK = loaiTK;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@ThoiGianDen", SqlDbType.DateTime, ThoiGianDen.Year <= 1753 ? DBNull.Value : (object) ThoiGianDen);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);
			db.AddInParameter(dbCommand, "@ID_TK", SqlDbType.BigInt, ID_TK);
			db.AddInParameter(dbCommand, "@LoaiTK", SqlDbType.VarChar, LoaiTK);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<DeNghiChuyenCuaKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DeNghiChuyenCuaKhau item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateDeNghiChuyenCuaKhau(long id, string thongTinKhac, string maDoanhNghiep, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan, string soVanDon, DateTime ngayVanDon, DateTime thoiGianDen, string diaDiemKiemTra, string tuyenDuong, long iD_TK, string loaiTK)
		{
			DeNghiChuyenCuaKhau entity = new DeNghiChuyenCuaKhau();			
			entity.ID = id;
			entity.ThongTinKhac = thongTinKhac;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.LoaiKB = loaiKB;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TrangThai = trangThai;
			entity.NamTiepNhan = namTiepNhan;
			entity.SoVanDon = soVanDon;
			entity.NgayVanDon = ngayVanDon;
			entity.ThoiGianDen = thoiGianDen;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.TuyenDuong = tuyenDuong;
			entity.ID_TK = iD_TK;
			entity.LoaiTK = loaiTK;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@ThoiGianDen", SqlDbType.DateTime, ThoiGianDen.Year <= 1753 ? DBNull.Value : (object) ThoiGianDen);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);
			db.AddInParameter(dbCommand, "@ID_TK", SqlDbType.BigInt, ID_TK);
			db.AddInParameter(dbCommand, "@LoaiTK", SqlDbType.VarChar, LoaiTK);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<DeNghiChuyenCuaKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DeNghiChuyenCuaKhau item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteDeNghiChuyenCuaKhau(long id)
		{
			DeNghiChuyenCuaKhau entity = new DeNghiChuyenCuaKhau();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_ID_TK(long iD_TK)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_DeleteBy_ID_TK]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID_TK", SqlDbType.BigInt, iD_TK);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ChungTuKem_DeNghiChuyenCuaKhau_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<DeNghiChuyenCuaKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DeNghiChuyenCuaKhau item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}