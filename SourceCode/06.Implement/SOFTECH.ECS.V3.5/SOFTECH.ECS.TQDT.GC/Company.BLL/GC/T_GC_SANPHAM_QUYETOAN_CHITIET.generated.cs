﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class T_GC_SANPHAM_QUYETOAN_CHITIET : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public decimal TOKHAIXUAT { set; get; }
		public string MASP { set; get; }
		public string TENSP { set; get; }
		public string DVT { set; get; }
		public decimal LUONGXUAT { set; get; }
		public decimal DONGIA { set; get; }
		public decimal TRIGIAXUAT { set; get; }
		public int TYGIATINHTHUE { set; get; }
		public long HOPDONG_ID { set; get; }
		public int NAMQUYETTOAN { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_GC_SANPHAM_QUYETOAN_CHITIET> ConvertToCollection(IDataReader reader)
		{
			List<T_GC_SANPHAM_QUYETOAN_CHITIET> collection = new List<T_GC_SANPHAM_QUYETOAN_CHITIET>();
			while (reader.Read())
			{
				T_GC_SANPHAM_QUYETOAN_CHITIET entity = new T_GC_SANPHAM_QUYETOAN_CHITIET();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TOKHAIXUAT"))) entity.TOKHAIXUAT = reader.GetDecimal(reader.GetOrdinal("TOKHAIXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MASP"))) entity.MASP = reader.GetString(reader.GetOrdinal("MASP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENSP"))) entity.TENSP = reader.GetString(reader.GetOrdinal("TENSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGXUAT"))) entity.LUONGXUAT = reader.GetDecimal(reader.GetOrdinal("LUONGXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DONGIA"))) entity.DONGIA = reader.GetDecimal(reader.GetOrdinal("DONGIA"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIAXUAT"))) entity.TRIGIAXUAT = reader.GetDecimal(reader.GetOrdinal("TRIGIAXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TYGIATINHTHUE"))) entity.TYGIATINHTHUE = reader.GetInt32(reader.GetOrdinal("TYGIATINHTHUE"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NAMQUYETTOAN"))) entity.NAMQUYETTOAN = reader.GetInt32(reader.GetOrdinal("NAMQUYETTOAN"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_GC_SANPHAM_QUYETOAN_CHITIET VALUES(@ID, @TOKHAIXUAT, @MASP, @TENSP, @DVT, @LUONGXUAT, @DONGIA, @TRIGIAXUAT, @TYGIATINHTHUE, @HOPDONG_ID, @NAMQUYETTOAN)";
            string update = "UPDATE T_GC_SANPHAM_QUYETOAN_CHITIET SET ID = @ID, TOKHAIXUAT = @TOKHAIXUAT, MASP = @MASP, TENSP = @TENSP, DVT = @DVT, LUONGXUAT = @LUONGXUAT, DONGIA = @DONGIA, TRIGIAXUAT = @TRIGIAXUAT, TYGIATINHTHUE = @TYGIATINHTHUE, HOPDONG_ID = @HOPDONG_ID, NAMQUYETTOAN = @NAMQUYETTOAN WHERE ID = @ID";
            string delete = "DELETE FROM T_GC_SANPHAM_QUYETOAN_CHITIET WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_GC_SANPHAM_QUYETOAN_CHITIET VALUES(@ID, @TOKHAIXUAT, @MASP, @TENSP, @DVT, @LUONGXUAT, @DONGIA, @TRIGIAXUAT, @TYGIATINHTHUE, @HOPDONG_ID, @NAMQUYETTOAN)";
            string update = "UPDATE T_GC_SANPHAM_QUYETOAN_CHITIET SET ID = @ID, TOKHAIXUAT = @TOKHAIXUAT, MASP = @MASP, TENSP = @TENSP, DVT = @DVT, LUONGXUAT = @LUONGXUAT, DONGIA = @DONGIA, TRIGIAXUAT = @TRIGIAXUAT, TYGIATINHTHUE = @TYGIATINHTHUE, HOPDONG_ID = @HOPDONG_ID, NAMQUYETTOAN = @NAMQUYETTOAN WHERE ID = @ID";
            string delete = "DELETE FROM T_GC_SANPHAM_QUYETOAN_CHITIET WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DONGIA", SqlDbType.Decimal, "DONGIA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TYGIATINHTHUE", SqlDbType.Int, "TYGIATINHTHUE", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@TOKHAIXUAT", SqlDbType.Decimal, "TOKHAIXUAT", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MASP", SqlDbType.NVarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_GC_SANPHAM_QUYETOAN_CHITIET Load(long id, decimal tOKHAIXUAT, string mASP, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, tOKHAIXUAT);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, mASP);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, hOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, nAMQUYETTOAN);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_GC_SANPHAM_QUYETOAN_CHITIET> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_GC_SANPHAM_QUYETOAN_CHITIET> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_GC_SANPHAM_QUYETOAN_CHITIET> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public DataSet SelectSPXuatBy_HD_ID(long HOPDONG_ID, int NamQuyetToan)
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectSPXuatBy_HD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            dbCommand.CommandTimeout = 6000;

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HOPDONG_ID);
            db.AddInParameter(dbCommand, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertT_GC_SANPHAM_QUYETOAN_CHITIET(long iD, decimal tOKHAIXUAT, string mASP, string tENSP, string dVT, decimal lUONGXUAT, decimal dONGIA, decimal tRIGIAXUAT, int tYGIATINHTHUE, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN_CHITIET entity = new T_GC_SANPHAM_QUYETOAN_CHITIET();	
			entity.ID = iD;
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT = dVT;
			entity.LUONGXUAT = lUONGXUAT;
			entity.DONGIA = dONGIA;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.TYGIATINHTHUE = tYGIATINHTHUE;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			//db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@TYGIATINHTHUE", SqlDbType.Int, TYGIATINHTHUE);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_GC_SANPHAM_QUYETOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN_CHITIET item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_GC_SANPHAM_QUYETOAN_CHITIET(long id, decimal tOKHAIXUAT, string mASP, string tENSP, string dVT, decimal lUONGXUAT, decimal dONGIA, decimal tRIGIAXUAT, int tYGIATINHTHUE, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN_CHITIET entity = new T_GC_SANPHAM_QUYETOAN_CHITIET();			
			entity.ID = id;
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT = dVT;
			entity.LUONGXUAT = lUONGXUAT;
			entity.DONGIA = dONGIA;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.TYGIATINHTHUE = tYGIATINHTHUE;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_GC_SANPHAM_QUYETOAN_CHITIET_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@TYGIATINHTHUE", SqlDbType.Int, TYGIATINHTHUE);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_GC_SANPHAM_QUYETOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN_CHITIET item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_GC_SANPHAM_QUYETOAN_CHITIET(long id, decimal tOKHAIXUAT, string mASP, string tENSP, string dVT, decimal lUONGXUAT, decimal dONGIA, decimal tRIGIAXUAT, int tYGIATINHTHUE, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN_CHITIET entity = new T_GC_SANPHAM_QUYETOAN_CHITIET();			
			entity.ID = id;
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT = dVT;
			entity.LUONGXUAT = lUONGXUAT;
			entity.DONGIA = dONGIA;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.TYGIATINHTHUE = tYGIATINHTHUE;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@DONGIA", SqlDbType.Decimal, DONGIA);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@TYGIATINHTHUE", SqlDbType.Int, TYGIATINHTHUE);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_GC_SANPHAM_QUYETOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN_CHITIET item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_GC_SANPHAM_QUYETOAN_CHITIET(long id, decimal tOKHAIXUAT, string mASP, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN_CHITIET entity = new T_GC_SANPHAM_QUYETOAN_CHITIET();
			entity.ID = id;
			entity.TOKHAIXUAT = tOKHAIXUAT;
			entity.MASP = mASP;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TOKHAIXUAT", SqlDbType.Decimal, TOKHAIXUAT);
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.NVarChar, MASP);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------

        public int DeleteBy_HD_ID(long HOPDONG_ID, int NAMQUYETTOAN)
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteBy_HD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
            db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);

            return db.ExecuteNonQuery(dbCommand);
        }
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_GC_SANPHAM_QUYETOAN_CHITIET> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN_CHITIET item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}