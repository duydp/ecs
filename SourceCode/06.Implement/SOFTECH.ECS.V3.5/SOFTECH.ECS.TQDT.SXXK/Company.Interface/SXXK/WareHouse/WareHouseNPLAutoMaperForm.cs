﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using System.Threading;
using Janus.Windows.GridEX;
using Company.BLL.SXXK;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseNPLAutoMaperForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_NGUYENPHULIEU_MAP NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public T_KHOKETOAN_SANPHAM_MAP SPMap = new T_KHOKETOAN_SANPHAM_MAP();
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        public SanPhamCollection SPCollection = new SanPhamCollection();
        public String Type = "";
        public WareHouseNPLAutoMaperForm()
        {
            InitializeComponent();

            cbbMaKho.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionAll();
            cbbMaKho.DisplayMember = "MAKHO";
            cbbMaKho.ValueMember = "MAKHO";

            cbbTK.DataSource = T_KHOKETOAN_TAIKHOAN.SelectCollectionAll();
            cbbTK.DisplayMember = "MATK";
            cbbTK.ValueMember = "MATK";
        }

        private void WareHouseNPLAutoMaperForm_Load(object sender, EventArgs e)
        {
            cbbMaKho.SelectedIndex = 0;
            if (Type=="NPL")
            {
                cbbTK.Value = "152";
                BindDataNPL();
            }
            else
            {
                cbbTK.Value = "155";
                BindDataSP();
            }
        }
        private void BindDataNPL()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = NguyenPhuLieu.SelectCollectionDynamicBy(" Ma NOT IN ( SELECT MANPL FROM dbo.T_KHOKETOAN_NGUYENPHULIEU )","");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSP()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = SanPham.SelectCollectionDynamicBy(" Ma NOT IN (SELECT MASP FROM dbo.T_KHOKETOAN_SANPHAM )", "");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaKho, errorProvider, "Mã kho", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbTK, errorProvider, "Tài khoản");
                isValid &= ValidateControl.ValidateNull(txtDonHang, errorProvider, "Đơn hàng");
                isValid &= ValidateControl.ValidateNull(txtNguoiBan, errorProvider, "Người bán");
                isValid &= ValidateControl.ValidateNull(txtKichThuoc, errorProvider, "Kích thước");
                isValid &= ValidateControl.ValidateNull(txtMauSac, errorProvider, "Màu sắc");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnProcess_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                btnProcess.Enabled = false;
                btnClose.Enabled = false;
                System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                    SetError(string.Empty);
                    if (Type=="NPL")
                    {
                        // Lấy danh sách Tất cả các NPL đã đăng ký của Hợp đồng bỏ qua các NPL đã Map trước đó .
                        NPLCollection = NguyenPhuLieu.SelectCollectionDynamicBy(" Ma NOT IN (SELECT MANPL FROM dbo.T_KHOKETOAN_NGUYENPHULIEU )", "");
                        int i = 1;
                        if (NPLCollection.Count >= 1)
                        {
                            foreach (NguyenPhuLieu item in NPLCollection)
                            {

                                NPL = new T_KHOKETOAN_NGUYENPHULIEU();
                                NPL.MANPL = item.Ma.ToString();
                                NPL.TENNPL = item.Ten.ToString();
                                NPL.MAHS = item.MaHS.ToString();
                                NPL.DVT = DonViTinh_GetName(item.DVT_ID).ToString();
                                NPL.MAKHO = cbbMaKho.Value.ToString();
                                NPL.TAIKHOAN = cbbTK.Value.ToString();
                                NPL.NGUOIBAN = txtNguoiBan.Text.ToString();
                                NPL.KICHTHUOC = txtKichThuoc.Text.ToString();
                                NPL.MAUSAC = txtMauSac.Text.ToString();
                                NPL.DONHANG = txtDonHang.Text.ToString();

                                NPLMap = new T_KHOKETOAN_NGUYENPHULIEU_MAP();
                                NPLMap.MANPL = item.Ma.ToString();
                                NPLMap.TENNPL = item.Ten.ToString();
                                NPLMap.DVT = DonViTinh_GetName(item.DVT_ID).ToString();
                                NPLMap.MANPLMAP = item.Ma.ToString();
                                NPLMap.TENNPLMAP = item.Ten.ToString();
                                NPLMap.DVTMAP = DonViTinh_GetName(item.DVT_ID).ToString();
                                NPLMap.TYLEQD = 1;
                                NPL.NPLMapCollection.Add(NPLMap);

                                NPL.InsertUpdateFull();

                                SetProcessBar((i * 100 / NPLCollection.Count));
                                i++;
                            }
                            if (InvokeRequired)
                            {
                                this.Invoke(new MethodInvoker(delegate
                                {
                                    BindDataNPL();
                                    btnProcess.Enabled = true; btnClose.Enabled = true;

                                }));
                            }
                            else
                            {
                                BindDataNPL();
                                btnProcess.Enabled = true; btnClose.Enabled = true;
                            }
                            SetProcessBar(0);
                            SetError("Hoàn thành");
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        // Lấy danh sách Tất cả các Sản phẩm đã đăng ký của Hợp đồng bỏ qua các SP đã Map trước đó .
                        SPCollection = SanPham.SelectCollectionDynamicBy(" Ma NOT IN (SELECT MASP FROM dbo.T_KHOKETOAN_SANPHAM )", "");
                        int i = 1;
                        if (SPCollection.Count >= 1)
                        {
                            foreach (SanPham item in SPCollection)
                            {

                                SP = new T_KHOKETOAN_SANPHAM();
                                SP.MASP = item.Ma.ToString();
                                SP.TENSP = item.Ten.ToString();
                                SP.MAHS = item.MaHS.ToString();
                                SP.DVT = DonViTinh_GetName(item.DVT_ID).ToString();
                                SP.MAKHO = cbbMaKho.Value.ToString();
                                SP.TAIKHOAN = cbbTK.Value.ToString();
                                SP.NGUOIBAN = txtNguoiBan.Text.ToString();
                                SP.KICHTHUOC = txtKichThuoc.Text.ToString();
                                SP.MAUSAC = txtMauSac.Text.ToString();
                                SP.DONHANG = txtDonHang.Text.ToString();

                                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                                SPMap.MASP = item.Ma.ToString();
                                SPMap.TENSP = item.Ten.ToString();
                                SPMap.DVT = DonViTinh_GetName(item.DVT_ID).ToString();
                                SPMap.MASPMAP = item.Ma.ToString();
                                SPMap.TENSPMAP = item.Ten.ToString();
                                SPMap.DVTMAP = DonViTinh_GetName(item.DVT_ID).ToString();
                                SPMap.TYLEQD = 1;
                                SP.SPCollection.Add(SPMap);

                                SP.InsertUpdateFull();
                                SetProcessBar((i * 100 / SPCollection.Count));
                                i++;
                            }
                            if (InvokeRequired)
                            {
                                this.Invoke(new MethodInvoker(delegate
                                {
                                    BindDataNPL();
                                    btnProcess.Enabled = true; btnClose.Enabled = true;

                                }));
                            }
                            else
                            {
                                BindDataNPL();
                                btnProcess.Enabled = true; btnClose.Enabled = true;
                            }
                            SetProcessBar(0);
                            SetError("Hoàn thành");
                        }
                        else
                        {
                            return;
                        }
                    }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SetError(string.Empty);
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
