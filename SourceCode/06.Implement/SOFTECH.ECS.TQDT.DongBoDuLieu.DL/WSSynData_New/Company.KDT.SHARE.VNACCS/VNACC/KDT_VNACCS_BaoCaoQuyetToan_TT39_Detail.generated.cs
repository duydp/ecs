﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Contract_ID { set; get; }
		public decimal STT { set; get; }
		public string TenHangHoa { set; get; }
		public string MaHangHoa { set; get; }
		public string DVT { set; get; }
		public decimal LuongTonDauKy { set; get; }
		public decimal LuongNhapTrongKy { set; get; }
		public decimal LuongTaiXuat { set; get; }
		public decimal LuongChuyenMucDichSD { set; get; }
		public decimal LuongXuatKhau { set; get; }
		public decimal LuongXuatKhac { set; get; }
		public decimal LuongTonCuoiKy { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
			while (reader.Read())
			{
				KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Contract_ID"))) entity.Contract_ID = reader.GetInt64(reader.GetOrdinal("Contract_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.TenHangHoa = reader.GetString(reader.GetOrdinal("TenHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHangHoa"))) entity.MaHangHoa = reader.GetString(reader.GetOrdinal("MaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDauKy"))) entity.LuongTonDauKy = reader.GetDecimal(reader.GetOrdinal("LuongTonDauKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhapTrongKy"))) entity.LuongNhapTrongKy = reader.GetDecimal(reader.GetOrdinal("LuongNhapTrongKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTaiXuat"))) entity.LuongTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongTaiXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongChuyenMucDichSD"))) entity.LuongChuyenMucDichSD = reader.GetDecimal(reader.GetOrdinal("LuongChuyenMucDichSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuatKhau"))) entity.LuongXuatKhau = reader.GetDecimal(reader.GetOrdinal("LuongXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuatKhac"))) entity.LuongXuatKhac = reader.GetDecimal(reader.GetOrdinal("LuongXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoiKy"))) entity.LuongTonCuoiKy = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoiKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection, long id)
        {
            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details VALUES(@Contract_ID, @STT, @TenHangHoa, @MaHangHoa, @DVT, @LuongTonDauKy, @LuongNhapTrongKy, @LuongTaiXuat, @LuongChuyenMucDichSD, @LuongXuatKhau, @LuongXuatKhac, @LuongTonCuoiKy, @GhiChu)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details SET Contract_ID = @Contract_ID, STT = @STT, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, DVT = @DVT, LuongTonDauKy = @LuongTonDauKy, LuongNhapTrongKy = @LuongNhapTrongKy, LuongTaiXuat = @LuongTaiXuat, LuongChuyenMucDichSD = @LuongChuyenMucDichSD, LuongXuatKhau = @LuongXuatKhau, LuongXuatKhac = @LuongXuatKhac, LuongTonCuoiKy = @LuongTonCuoiKy, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Contract_ID", SqlDbType.BigInt, "Contract_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonDauKy", SqlDbType.Decimal, "LuongTonDauKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTaiXuat", SqlDbType.Decimal, "LuongTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, "LuongChuyenMucDichSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatKhau", SqlDbType.Decimal, "LuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatKhac", SqlDbType.Decimal, "LuongXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, "LuongTonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Contract_ID", SqlDbType.BigInt, "Contract_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonDauKy", SqlDbType.Decimal, "LuongTonDauKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTaiXuat", SqlDbType.Decimal, "LuongTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, "LuongChuyenMucDichSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatKhau", SqlDbType.Decimal, "LuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatKhac", SqlDbType.Decimal, "LuongXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, "LuongTonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details VALUES(@Contract_ID, @STT, @TenHangHoa, @MaHangHoa, @DVT, @LuongTonDauKy, @LuongNhapTrongKy, @LuongTaiXuat, @LuongChuyenMucDichSD, @LuongXuatKhau, @LuongXuatKhac, @LuongTonCuoiKy, @GhiChu)";
            string update = "UPDATE t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details SET Contract_ID = @Contract_ID, STT = @STT, TenHangHoa = @TenHangHoa, MaHangHoa = @MaHangHoa, DVT = @DVT, LuongTonDauKy = @LuongTonDauKy, LuongNhapTrongKy = @LuongNhapTrongKy, LuongTaiXuat = @LuongTaiXuat, LuongChuyenMucDichSD = @LuongChuyenMucDichSD, LuongXuatKhau = @LuongXuatKhau, LuongXuatKhac = @LuongXuatKhac, LuongTonCuoiKy = @LuongTonCuoiKy, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BaoCaoQuyetToan_TT39_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Contract_ID", SqlDbType.BigInt, "Contract_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonDauKy", SqlDbType.Decimal, "LuongTonDauKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTaiXuat", SqlDbType.Decimal, "LuongTaiXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, "LuongChuyenMucDichSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatKhau", SqlDbType.Decimal, "LuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuatKhac", SqlDbType.Decimal, "LuongXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, "LuongTonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Contract_ID", SqlDbType.BigInt, "Contract_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHangHoa", SqlDbType.NVarChar, "MaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonDauKy", SqlDbType.Decimal, "LuongTonDauKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, "LuongNhapTrongKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTaiXuat", SqlDbType.Decimal, "LuongTaiXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, "LuongChuyenMucDichSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatKhau", SqlDbType.Decimal, "LuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuatKhac", SqlDbType.Decimal, "LuongXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, "LuongTonCuoiKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> SelectCollectionBy_Contract_ID(long contract_ID)
		{
            IDataReader reader = SelectReaderBy_Contract_ID(contract_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Contract_ID(long contract_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectBy_Contract_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Contract_ID", SqlDbType.BigInt, contract_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Contract_ID(long contract_ID)
		{
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_SelectBy_Contract_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Contract_ID", SqlDbType.BigInt, contract_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BaoCaoQuyetToan_TT39_Detail(long contract_ID, decimal sTT, string tenHangHoa, string maHangHoa, string dVT, decimal luongTonDauKy, decimal luongNhapTrongKy, decimal luongTaiXuat, decimal luongChuyenMucDichSD, decimal luongXuatKhau, decimal luongXuatKhac, decimal luongTonCuoiKy, string ghiChu)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();	
			entity.Contract_ID = contract_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.LuongTonDauKy = luongTonDauKy;
			entity.LuongNhapTrongKy = luongNhapTrongKy;
			entity.LuongTaiXuat = luongTaiXuat;
			entity.LuongChuyenMucDichSD = luongChuyenMucDichSD;
			entity.LuongXuatKhau = luongXuatKhau;
			entity.LuongXuatKhac = luongXuatKhac;
			entity.LuongTonCuoiKy = luongTonCuoiKy;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Contract_ID", SqlDbType.BigInt, Contract_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LuongTonDauKy", SqlDbType.Decimal, LuongTonDauKy);
			db.AddInParameter(dbCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, LuongNhapTrongKy);
			db.AddInParameter(dbCommand, "@LuongTaiXuat", SqlDbType.Decimal, LuongTaiXuat);
			db.AddInParameter(dbCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, LuongChuyenMucDichSD);
			db.AddInParameter(dbCommand, "@LuongXuatKhau", SqlDbType.Decimal, LuongXuatKhau);
			db.AddInParameter(dbCommand, "@LuongXuatKhac", SqlDbType.Decimal, LuongXuatKhac);
			db.AddInParameter(dbCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, LuongTonCuoiKy);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BaoCaoQuyetToan_TT39_Detail(long id, long contract_ID, decimal sTT, string tenHangHoa, string maHangHoa, string dVT, decimal luongTonDauKy, decimal luongNhapTrongKy, decimal luongTaiXuat, decimal luongChuyenMucDichSD, decimal luongXuatKhau, decimal luongXuatKhac, decimal luongTonCuoiKy, string ghiChu)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();			
			entity.ID = id;
			entity.Contract_ID = contract_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.LuongTonDauKy = luongTonDauKy;
			entity.LuongNhapTrongKy = luongNhapTrongKy;
			entity.LuongTaiXuat = luongTaiXuat;
			entity.LuongChuyenMucDichSD = luongChuyenMucDichSD;
			entity.LuongXuatKhau = luongXuatKhau;
			entity.LuongXuatKhac = luongXuatKhac;
			entity.LuongTonCuoiKy = luongTonCuoiKy;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Contract_ID", SqlDbType.BigInt, Contract_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LuongTonDauKy", SqlDbType.Decimal, LuongTonDauKy);
			db.AddInParameter(dbCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, LuongNhapTrongKy);
			db.AddInParameter(dbCommand, "@LuongTaiXuat", SqlDbType.Decimal, LuongTaiXuat);
			db.AddInParameter(dbCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, LuongChuyenMucDichSD);
			db.AddInParameter(dbCommand, "@LuongXuatKhau", SqlDbType.Decimal, LuongXuatKhau);
			db.AddInParameter(dbCommand, "@LuongXuatKhac", SqlDbType.Decimal, LuongXuatKhac);
			db.AddInParameter(dbCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, LuongTonCuoiKy);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BaoCaoQuyetToan_TT39_Detail(long id, long contract_ID, decimal sTT, string tenHangHoa, string maHangHoa, string dVT, decimal luongTonDauKy, decimal luongNhapTrongKy, decimal luongTaiXuat, decimal luongChuyenMucDichSD, decimal luongXuatKhau, decimal luongXuatKhac, decimal luongTonCuoiKy, string ghiChu)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();			
			entity.ID = id;
			entity.Contract_ID = contract_ID;
			entity.STT = sTT;
			entity.TenHangHoa = tenHangHoa;
			entity.MaHangHoa = maHangHoa;
			entity.DVT = dVT;
			entity.LuongTonDauKy = luongTonDauKy;
			entity.LuongNhapTrongKy = luongNhapTrongKy;
			entity.LuongTaiXuat = luongTaiXuat;
			entity.LuongChuyenMucDichSD = luongChuyenMucDichSD;
			entity.LuongXuatKhau = luongXuatKhau;
			entity.LuongXuatKhac = luongXuatKhac;
			entity.LuongTonCuoiKy = luongTonCuoiKy;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Contract_ID", SqlDbType.BigInt, Contract_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaHangHoa", SqlDbType.NVarChar, MaHangHoa);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LuongTonDauKy", SqlDbType.Decimal, LuongTonDauKy);
			db.AddInParameter(dbCommand, "@LuongNhapTrongKy", SqlDbType.Decimal, LuongNhapTrongKy);
			db.AddInParameter(dbCommand, "@LuongTaiXuat", SqlDbType.Decimal, LuongTaiXuat);
			db.AddInParameter(dbCommand, "@LuongChuyenMucDichSD", SqlDbType.Decimal, LuongChuyenMucDichSD);
			db.AddInParameter(dbCommand, "@LuongXuatKhau", SqlDbType.Decimal, LuongXuatKhau);
			db.AddInParameter(dbCommand, "@LuongXuatKhac", SqlDbType.Decimal, LuongXuatKhac);
			db.AddInParameter(dbCommand, "@LuongTonCuoiKy", SqlDbType.Decimal, LuongTonCuoiKy);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_BaoCaoQuyetToan_TT39_Detail(long id)
		{
			KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail entity = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Contract_ID(long contract_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteBy_Contract_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Contract_ID", SqlDbType.BigInt, contract_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}