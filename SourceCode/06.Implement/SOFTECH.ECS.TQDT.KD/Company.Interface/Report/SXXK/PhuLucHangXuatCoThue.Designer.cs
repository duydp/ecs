﻿namespace Company.Interface.Report.SXXK
{
    partial class PhuLucHangXuatCoThue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuLucHangXuatCoThue));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TriGiaTT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ThueSuatXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TienThueXNK9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TyLeThuKhac9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaThuKhac9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThueBangChu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThueBangSo = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaThuKhac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTienThueXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrControlStyle1 = new DevExpress.XtraReports.UI.XRControlStyle();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.lblTongTienThueBangChu,
            this.lblTongTienThueBangSo,
            this.lblTongTriGiaThuKhac,
            this.lblTongTienThueXNK,
            this.lblNgayDangKy,
            this.lblMaHaiQuan,
            this.xrLabel3,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrPictureBox1});
            this.Detail.Height = 1180;
            this.Detail.Name = "Detail";
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(50, 210);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable2.Size = new System.Drawing.Size(508, 318);
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT1,
            this.ThueSuatXNK1,
            this.TienThueXNK1,
            this.TyLeThuKhac1,
            this.TriGiaThuKhac1});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Size = new System.Drawing.Size(508, 35);
            // 
            // TriGiaTT1
            // 
            this.TriGiaTT1.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT1.Name = "TriGiaTT1";
            this.TriGiaTT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT1.Size = new System.Drawing.Size(150, 35);
            this.TriGiaTT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK1
            // 
            this.ThueSuatXNK1.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK1.Name = "ThueSuatXNK1";
            this.ThueSuatXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK1.Size = new System.Drawing.Size(57, 35);
            this.ThueSuatXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK1
            // 
            this.TienThueXNK1.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK1.Name = "TienThueXNK1";
            this.TienThueXNK1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK1.Size = new System.Drawing.Size(134, 35);
            this.TienThueXNK1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac1
            // 
            this.TyLeThuKhac1.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac1.Name = "TyLeThuKhac1";
            this.TyLeThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac1.Size = new System.Drawing.Size(50, 35);
            this.TyLeThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac1
            // 
            this.TriGiaThuKhac1.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac1.Name = "TriGiaThuKhac1";
            this.TriGiaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac1.Size = new System.Drawing.Size(117, 35);
            this.TriGiaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT2,
            this.ThueSuatXNK2,
            this.TienThueXNK2,
            this.TyLeThuKhac2,
            this.TriGiaThuKhac2});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Size = new System.Drawing.Size(508, 35);
            // 
            // TriGiaTT2
            // 
            this.TriGiaTT2.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT2.Name = "TriGiaTT2";
            this.TriGiaTT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT2.Size = new System.Drawing.Size(150, 35);
            this.TriGiaTT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK2
            // 
            this.ThueSuatXNK2.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK2.Name = "ThueSuatXNK2";
            this.ThueSuatXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK2.Size = new System.Drawing.Size(57, 35);
            this.ThueSuatXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK2
            // 
            this.TienThueXNK2.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK2.Name = "TienThueXNK2";
            this.TienThueXNK2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK2.Size = new System.Drawing.Size(134, 35);
            this.TienThueXNK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac2
            // 
            this.TyLeThuKhac2.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac2.Name = "TyLeThuKhac2";
            this.TyLeThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac2.Size = new System.Drawing.Size(50, 35);
            this.TyLeThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac2
            // 
            this.TriGiaThuKhac2.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac2.Name = "TriGiaThuKhac2";
            this.TriGiaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac2.Size = new System.Drawing.Size(117, 35);
            this.TriGiaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT3,
            this.ThueSuatXNK3,
            this.TienThueXNK3,
            this.TyLeThuKhac3,
            this.TriGiaThuKhac3});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Size = new System.Drawing.Size(508, 35);
            // 
            // TriGiaTT3
            // 
            this.TriGiaTT3.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT3.Name = "TriGiaTT3";
            this.TriGiaTT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT3.Size = new System.Drawing.Size(150, 35);
            this.TriGiaTT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK3
            // 
            this.ThueSuatXNK3.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK3.Name = "ThueSuatXNK3";
            this.ThueSuatXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK3.Size = new System.Drawing.Size(57, 35);
            this.ThueSuatXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK3
            // 
            this.TienThueXNK3.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK3.Name = "TienThueXNK3";
            this.TienThueXNK3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK3.Size = new System.Drawing.Size(134, 35);
            this.TienThueXNK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac3
            // 
            this.TyLeThuKhac3.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac3.Name = "TyLeThuKhac3";
            this.TyLeThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac3.Size = new System.Drawing.Size(50, 35);
            this.TyLeThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac3
            // 
            this.TriGiaThuKhac3.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac3.Name = "TriGiaThuKhac3";
            this.TriGiaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac3.Size = new System.Drawing.Size(117, 35);
            this.TriGiaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT4,
            this.ThueSuatXNK4,
            this.TienThueXNK4,
            this.TyLeThuKhac4,
            this.TriGiaThuKhac4});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Size = new System.Drawing.Size(508, 35);
            // 
            // TriGiaTT4
            // 
            this.TriGiaTT4.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT4.Name = "TriGiaTT4";
            this.TriGiaTT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT4.Size = new System.Drawing.Size(150, 35);
            this.TriGiaTT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK4
            // 
            this.ThueSuatXNK4.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK4.Name = "ThueSuatXNK4";
            this.ThueSuatXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK4.Size = new System.Drawing.Size(57, 35);
            this.ThueSuatXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK4
            // 
            this.TienThueXNK4.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK4.Name = "TienThueXNK4";
            this.TienThueXNK4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK4.Size = new System.Drawing.Size(134, 35);
            this.TienThueXNK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac4
            // 
            this.TyLeThuKhac4.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac4.Name = "TyLeThuKhac4";
            this.TyLeThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac4.Size = new System.Drawing.Size(50, 35);
            this.TyLeThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac4
            // 
            this.TriGiaThuKhac4.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac4.Name = "TriGiaThuKhac4";
            this.TriGiaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac4.Size = new System.Drawing.Size(117, 35);
            this.TriGiaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT5,
            this.ThueSuatXNK5,
            this.TienThueXNK5,
            this.TyLeThuKhac5,
            this.TriGiaThuKhac5});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Size = new System.Drawing.Size(508, 35);
            // 
            // TriGiaTT5
            // 
            this.TriGiaTT5.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT5.Name = "TriGiaTT5";
            this.TriGiaTT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT5.Size = new System.Drawing.Size(150, 35);
            this.TriGiaTT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK5
            // 
            this.ThueSuatXNK5.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK5.Name = "ThueSuatXNK5";
            this.ThueSuatXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK5.Size = new System.Drawing.Size(57, 35);
            this.ThueSuatXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK5
            // 
            this.TienThueXNK5.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK5.Name = "TienThueXNK5";
            this.TienThueXNK5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK5.Size = new System.Drawing.Size(134, 35);
            this.TienThueXNK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac5
            // 
            this.TyLeThuKhac5.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac5.Name = "TyLeThuKhac5";
            this.TyLeThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac5.Size = new System.Drawing.Size(50, 35);
            this.TyLeThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac5
            // 
            this.TriGiaThuKhac5.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac5.Name = "TriGiaThuKhac5";
            this.TriGiaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac5.Size = new System.Drawing.Size(117, 35);
            this.TriGiaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT6,
            this.ThueSuatXNK6,
            this.TienThueXNK6,
            this.TyLeThuKhac6,
            this.TriGiaThuKhac6});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Size = new System.Drawing.Size(508, 35);
            // 
            // TriGiaTT6
            // 
            this.TriGiaTT6.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT6.Name = "TriGiaTT6";
            this.TriGiaTT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT6.Size = new System.Drawing.Size(150, 35);
            this.TriGiaTT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK6
            // 
            this.ThueSuatXNK6.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK6.Name = "ThueSuatXNK6";
            this.ThueSuatXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK6.Size = new System.Drawing.Size(57, 35);
            this.ThueSuatXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK6
            // 
            this.TienThueXNK6.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK6.Name = "TienThueXNK6";
            this.TienThueXNK6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK6.Size = new System.Drawing.Size(134, 35);
            this.TienThueXNK6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac6
            // 
            this.TyLeThuKhac6.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac6.Name = "TyLeThuKhac6";
            this.TyLeThuKhac6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac6.Size = new System.Drawing.Size(50, 35);
            this.TyLeThuKhac6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac6
            // 
            this.TriGiaThuKhac6.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac6.Name = "TriGiaThuKhac6";
            this.TriGiaThuKhac6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac6.Size = new System.Drawing.Size(117, 35);
            this.TriGiaThuKhac6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT7,
            this.ThueSuatXNK7,
            this.TienThueXNK7,
            this.TyLeThuKhac7,
            this.TriGiaThuKhac7});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Size = new System.Drawing.Size(508, 36);
            // 
            // TriGiaTT7
            // 
            this.TriGiaTT7.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT7.Name = "TriGiaTT7";
            this.TriGiaTT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT7.Size = new System.Drawing.Size(150, 36);
            this.TriGiaTT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK7
            // 
            this.ThueSuatXNK7.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK7.Name = "ThueSuatXNK7";
            this.ThueSuatXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK7.Size = new System.Drawing.Size(57, 36);
            this.ThueSuatXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK7
            // 
            this.TienThueXNK7.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK7.Name = "TienThueXNK7";
            this.TienThueXNK7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK7.Size = new System.Drawing.Size(134, 36);
            this.TienThueXNK7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac7
            // 
            this.TyLeThuKhac7.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac7.Name = "TyLeThuKhac7";
            this.TyLeThuKhac7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac7.Size = new System.Drawing.Size(50, 36);
            this.TyLeThuKhac7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac7
            // 
            this.TriGiaThuKhac7.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac7.Name = "TriGiaThuKhac7";
            this.TriGiaThuKhac7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac7.Size = new System.Drawing.Size(117, 36);
            this.TriGiaThuKhac7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT8,
            this.ThueSuatXNK8,
            this.TienThueXNK8,
            this.TyLeThuKhac8,
            this.TriGiaThuKhac8});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Size = new System.Drawing.Size(508, 36);
            // 
            // TriGiaTT8
            // 
            this.TriGiaTT8.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT8.Name = "TriGiaTT8";
            this.TriGiaTT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT8.Size = new System.Drawing.Size(150, 36);
            this.TriGiaTT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK8
            // 
            this.ThueSuatXNK8.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK8.Name = "ThueSuatXNK8";
            this.ThueSuatXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK8.Size = new System.Drawing.Size(57, 36);
            this.ThueSuatXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK8
            // 
            this.TienThueXNK8.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK8.Name = "TienThueXNK8";
            this.TienThueXNK8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK8.Size = new System.Drawing.Size(134, 36);
            this.TienThueXNK8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac8
            // 
            this.TyLeThuKhac8.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac8.Name = "TyLeThuKhac8";
            this.TyLeThuKhac8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac8.Size = new System.Drawing.Size(50, 36);
            this.TyLeThuKhac8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac8
            // 
            this.TriGiaThuKhac8.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac8.Name = "TriGiaThuKhac8";
            this.TriGiaThuKhac8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac8.Size = new System.Drawing.Size(117, 36);
            this.TriGiaThuKhac8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TriGiaTT9,
            this.ThueSuatXNK9,
            this.TienThueXNK9,
            this.TyLeThuKhac9,
            this.TriGiaThuKhac9});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Size = new System.Drawing.Size(508, 36);
            // 
            // TriGiaTT9
            // 
            this.TriGiaTT9.Location = new System.Drawing.Point(0, 0);
            this.TriGiaTT9.Name = "TriGiaTT9";
            this.TriGiaTT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaTT9.Size = new System.Drawing.Size(150, 36);
            this.TriGiaTT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ThueSuatXNK9
            // 
            this.ThueSuatXNK9.Location = new System.Drawing.Point(150, 0);
            this.ThueSuatXNK9.Name = "ThueSuatXNK9";
            this.ThueSuatXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.ThueSuatXNK9.Size = new System.Drawing.Size(57, 36);
            this.ThueSuatXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TienThueXNK9
            // 
            this.TienThueXNK9.Location = new System.Drawing.Point(207, 0);
            this.TienThueXNK9.Name = "TienThueXNK9";
            this.TienThueXNK9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TienThueXNK9.Size = new System.Drawing.Size(134, 36);
            this.TienThueXNK9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TyLeThuKhac9
            // 
            this.TyLeThuKhac9.Location = new System.Drawing.Point(341, 0);
            this.TyLeThuKhac9.Name = "TyLeThuKhac9";
            this.TyLeThuKhac9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TyLeThuKhac9.Size = new System.Drawing.Size(50, 36);
            this.TyLeThuKhac9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // TriGiaThuKhac9
            // 
            this.TriGiaThuKhac9.Location = new System.Drawing.Point(391, 0);
            this.TriGiaThuKhac9.Name = "TriGiaThuKhac9";
            this.TriGiaThuKhac9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaThuKhac9.Size = new System.Drawing.Size(117, 36);
            this.TriGiaThuKhac9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblTongTienThueBangChu
            // 
            this.lblTongTienThueBangChu.Location = new System.Drawing.Point(567, 233);
            this.lblTongTienThueBangChu.Multiline = true;
            this.lblTongTienThueBangChu.Name = "lblTongTienThueBangChu";
            this.lblTongTienThueBangChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueBangChu.Size = new System.Drawing.Size(216, 109);
            this.lblTongTienThueBangChu.Text = "một triệu hai trăm ba mứoi hai nghin năm trăm đồng";
            // 
            // lblTongTienThueBangSo
            // 
            this.lblTongTienThueBangSo.Location = new System.Drawing.Point(608, 183);
            this.lblTongTienThueBangSo.Name = "lblTongTienThueBangSo";
            this.lblTongTienThueBangSo.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueBangSo.Size = new System.Drawing.Size(175, 25);
            this.lblTongTienThueBangSo.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTongTriGiaThuKhac
            // 
            this.lblTongTriGiaThuKhac.Location = new System.Drawing.Point(417, 533);
            this.lblTongTriGiaThuKhac.Name = "lblTongTriGiaThuKhac";
            this.lblTongTriGiaThuKhac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaThuKhac.Size = new System.Drawing.Size(142, 26);
            this.lblTongTriGiaThuKhac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTongTienThueXNK
            // 
            this.lblTongTienThueXNK.Location = new System.Drawing.Point(267, 533);
            this.lblTongTienThueXNK.Name = "lblTongTienThueXNK";
            this.lblTongTienThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTienThueXNK.Size = new System.Drawing.Size(125, 26);
            this.lblTongTienThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Location = new System.Drawing.Point(325, 104);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(83, 17);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaHaiQuan
            // 
            this.lblMaHaiQuan.Location = new System.Drawing.Point(508, 83);
            this.lblMaHaiQuan.Name = "lblMaHaiQuan";
            this.lblMaHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHaiQuan.Size = new System.Drawing.Size(50, 17);
            this.lblMaHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Location = new System.Drawing.Point(442, 83);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(50, 17);
            this.xrLabel3.Text = "KD";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Location = new System.Drawing.Point(325, 83);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.Size = new System.Drawing.Size(83, 17);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Location = new System.Drawing.Point(325, 67);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(100, 17);
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Size = new System.Drawing.Size(815, 1145);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // xrControlStyle1
            // 
            this.xrControlStyle1.Name = "xrControlStyle1";
            // 
            // PhuLucHangXuatCoThue
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(7, 5, 6, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.StyleSheet.AddRange(new DevExpress.XtraReports.UI.XRControlStyle[] {
            this.xrControlStyle1});
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblMaHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThueXNK;
        public DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaThuKhac;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThueBangChu;
        private DevExpress.XtraReports.UI.XRLabel lblTongTienThueBangSo;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT1;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK1;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT2;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK2;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT3;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK3;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT4;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK4;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK4;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT5;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK5;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK5;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT6;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK6;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK6;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT7;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK7;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK7;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT8;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK8;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK8;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaTT9;
        private DevExpress.XtraReports.UI.XRTableCell ThueSuatXNK9;
        private DevExpress.XtraReports.UI.XRTableCell TienThueXNK9;
        private DevExpress.XtraReports.UI.XRTableCell TyLeThuKhac9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaThuKhac9;
        private DevExpress.XtraReports.UI.XRControlStyle xrControlStyle1;
    }
}
