﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class DeNghiChuyenCuaKhau
    {
        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_DeNghiChuyenCuaKhau_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@ThoiGianDen", SqlDbType.DateTime, ThoiGianDen.Year <= 1753 ? DBNull.Value : (object)ThoiGianDen);
            db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
            db.AddInParameter(dbCommand, "@TuyenDuong", SqlDbType.NVarChar, TuyenDuong);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public static XmlNode ConvertCollectionDeNghiChuyenCuaKhauToXML(XmlDocument doc, List<DeNghiChuyenCuaKhau> ChuyenCuaKhauCollection, long TKMD_ID)
        {
            if (ChuyenCuaKhauCollection == null || ChuyenCuaKhauCollection.Count == 0)
            {
                ChuyenCuaKhauCollection = DeNghiChuyenCuaKhau.SelectCollectionBy_TKMD_ID(TKMD_ID);
            }
            XmlElement CHUNG_TU_DENGHICHUYENCK = doc.CreateElement("CHUNG_TU_DENGHICHUYENCK");
            foreach (DeNghiChuyenCuaKhau deNghichuyen in ChuyenCuaKhauCollection)
            {
                XmlElement CHUNG_TU_DENGHICHUYENCKItem = doc.CreateElement("CHUNG_TU_DENGHICHUYENCK.ITEM");
                CHUNG_TU_DENGHICHUYENCK.AppendChild(CHUNG_TU_DENGHICHUYENCKItem);

                XmlElement DoanhNghiep = doc.CreateElement("DoanhNghiep");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DoanhNghiep);

                XmlAttribute MaDN = doc.CreateAttribute("MaDN");
                MaDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(MaDN);

                XmlAttribute TenDN = doc.CreateAttribute("TenDN");
                TenDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(TenDN);

                XmlElement VanDon = doc.CreateElement("VanDon");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(VanDon);

                XmlAttribute SoVanDon = doc.CreateAttribute("SoVanDon");
                SoVanDon.Value = deNghichuyen.SoVanDon;
                VanDon.Attributes.Append(SoVanDon);

                XmlAttribute NgayVanDon = doc.CreateAttribute("NgayVanDon");
                //NgayVanDon.Value = deNghichuyen.NgayVanDon.ToString("yyyy-MM-dd");
                NgayVanDon.Value = deNghichuyen.NgayVanDon.ToString("yyyy-MM-dd");
                VanDon.Attributes.Append(NgayVanDon);

                XmlElement NoiDung = doc.CreateElement("NoiDung");
                NoiDung.InnerText = deNghichuyen.ThongTinKhac;
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(NoiDung);

                XmlElement DiaDiemKiemTra = doc.CreateElement("DiaDiemKiemTra");
                DiaDiemKiemTra.InnerText = deNghichuyen.DiaDiemKiemTra;
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DiaDiemKiemTra);

                XmlElement ThoiGianDen = doc.CreateElement("ThoiGianDen");
                ThoiGianDen.InnerText = deNghichuyen.ThoiGianDen.ToString("yyyy-MM-dd");
                //ThoiGianDen.InnerText = deNghichuyen.ThoiGianDen.ToString("yyyy-MM-dd");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(ThoiGianDen);

                XmlElement TuyenDuong = doc.CreateElement("TuyenDuong");
                TuyenDuong.InnerText = deNghichuyen.TuyenDuong;
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(TuyenDuong);

            }
            if (CHUNG_TU_DENGHICHUYENCK.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_DENGHICHUYENCK;

        }

        public string send(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (this.GuidStr == "")
            {
                this.GuidStr = (System.Guid.NewGuid().ToString()); ;

                this.Update();
            }
            nodeReference.InnerText = this.GuidStr;
            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<DeNghiChuyenCuaKhau> listDeNghiChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
            listDeNghiChuyenCuaKhau.Add(this);
            nodeDulieu.AppendChild(DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, listDeNghiChuyenCuaKhau, TKMD_ID));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public string LayPhanHoi(string password, string maHQ)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GuidStr;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString()); ;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            //if (GuidStr == "")
            //{
            //    GuidStr = (System.Guid.NewGuid().ToString()); ;
            //    nodeReference.InnerText = this.GuidStr;
            //    this.Update();
            //}

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;




            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.GuidStr;

            KDTService kdt = WebService.GetWS();

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            XmlDocument docNPL = new XmlDocument();
            for (i = 1; i <= 1; ++i)
            {
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, password);
                }
                catch (Exception ex)
                {
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }

                docNPL.LoadXml(kq);
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            return "";
        }

        public bool WSKhaiBaoChuyenCK(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<DeNghiChuyenCuaKhau> list, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GuidStr;
            this.Update();


            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";

            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<DeNghiChuyenCuaKhau> listDeNghiChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
            listDeNghiChuyenCuaKhau.Add(this);
            xmlNodeDulieu.AppendChild(DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(xmlDocument, listDeNghiChuyenCuaKhau, TKMD_ID));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            string msgError = string.Empty;
            try
            {


                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
                xmlDocument.InnerXml.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungDeNghiChuyenCK);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                        //XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        //XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");

                        //kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLayPhanHoiDeNghiChuyenCK);
                        return true;
                    }
                }
                else
                {
                    msgError = FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception(msgError);
                }
                return false;
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    kq.XmlSaveMessage(ID, MessageTitle.Error);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }
        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
                else
                    doc.Load(path + @"\TemplateXML\PhongBi.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GuidStr = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            //BaseClass.LoadConfigure("WS_URL");
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi: " + ex.Message);
            }

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;

            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }

                if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLayPhanHoiDeNghiChuyenCK, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));
                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }
        }

        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);

                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }

                if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHQTuChoiDeNghiChuyenCK, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                    // Cập lại số tiếp nhận
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.Update();

                    return true;
                }
                else if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLayPhanHoiDeNghiChuyenCK, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                    return true;
                }
                else if (xmlNodeResult.Attributes["SOTN"] != null)
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLayPhanHoiDeNghiChuyenCK, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                    this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    this.Update();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                }
                throw;
            }
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID, SqlDatabase db)
        {
            string spName = "[dbo].[p_KDT_DeNghiChuyenCuaKhau_DeleteBy_TKMD_ID]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }
        public static List<DeNghiChuyenCuaKhau> SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, SqlDatabase db)
        {
            List<DeNghiChuyenCuaKhau> collection = new List<DeNghiChuyenCuaKhau>();

            const string spName = "[dbo].[p_KDT_DeNghiChuyenCuaKhau_SelectDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand,transaction);
            while (reader.Read())
            {
                DeNghiChuyenCuaKhau entity = new DeNghiChuyenCuaKhau();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianDen"))) entity.ThoiGianDen = reader.GetDateTime(reader.GetOrdinal("ThoiGianDen"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemKiemTra"))) entity.DiaDiemKiemTra = reader.GetString(reader.GetOrdinal("DiaDiemKiemTra"));
                if (!reader.IsDBNull(reader.GetOrdinal("TuyenDuong"))) entity.TuyenDuong = reader.GetString(reader.GetOrdinal("TuyenDuong"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
    }
}