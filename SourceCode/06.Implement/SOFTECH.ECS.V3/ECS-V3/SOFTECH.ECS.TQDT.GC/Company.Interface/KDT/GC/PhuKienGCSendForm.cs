﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.KDT.GC.PCTFormTemplate;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class PhuKienGCSendForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public PhuKienDangKy pkdk = new PhuKienDangKy();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        //Comment
        //-----------------------------------------------------------------------------------------
        public PhuKienGCSendForm()
        {
            InitializeComponent();
            createLoaiPhuKien();
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            HD = HopDong.Load(HD.ID);
            txtSoHopDong.Text = HD.SoHopDong;
            createLoaiPhuKien();
        }

        private void setCommandStatus()
        {
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                lblTrangThai.Text = pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Wait for approval ") :
                    setText("Chờ hủy", "Wait for cancel");
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? Janus.Windows.UI.InheritableBoolean.True :
                     Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ? setText("Không phê duyệt", "Not approval") :
                    setText("Đã hủy", "Canceled");
                btnDelete.Enabled = true;
            }
            else
            {
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = pkdk.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO ? setText("Hủy khai báo", "") : setText("Chưa khai báo", "Not declared yet");
                btnDelete.Enabled = true;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
            sendXML.master_id = pkdk.ID;
            if (sendXML.Load())
            {
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? Janus.Windows.UI.InheritableBoolean.True :
                  Janus.Windows.UI.InheritableBoolean.False;
                cmdAdd.Enabled = cmdAddNew1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
        }
        //-----------------------------------------------------------------------------------------
        private void PhuKienGCSendForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();

            if (pkdk.ID > 0)
            {
                txtSoPhuKien.Text = pkdk.SoPhuKien.Trim();
                ccNgayPhuKien.Value = pkdk.NgayPhuKien;
                txtNoiDung.Text = pkdk.GhiChu;

                txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                //cbLoaiPhuKien.Value = pkdk.MaP
            }
            pkdk.LoadCollection();
            dgList.DataSource = pkdk.PKCollection;

            if (pkdk.ID > 0)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                msg.master_id = this.pkdk.ID;
                msg.LoaiHS = "PK";
                if (msg.Load())
                {
                    lblTrangThai.Text = setText("Chờ xác nhận ", "Wait for confirmation");
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }
            setCommandStatus();

        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo thông tin về loại phụ kiện.
        /// </summary>
        private void createLoaiPhuKien()
        {
            cbLoaiPhuKien.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.LoaiPhuKien.SelectAll();
            cbLoaiPhuKien.DisplayMember = "TenLoaiPhuKien";
            cbLoaiPhuKien.ValueMember = "ID_LoaiPhuKien";
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
        private void send()
        {

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }

            if (pkdk.PKCollection.Count == 0)
            {
                showMsg("MSG_2702015");
                //MLMessages("Chưa nhập phụ kiện","MSG_WRN19","", false);
                return;
            }
            //try
            //{
            //    pkdk.InsertUpDateFull();
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Lưu thông tin vào cơ sở dữ liệu bị lỗi. Không khai báo thông tin tới hải quan được.Lỗi : " + ex.Message, false);
            //    return;
            //}
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = pkdk.WSSend(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Phụ kiện . Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //MLMessages("Chưa có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
                    return;
                }
            }
            string xmlCurrent = "";
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = pkdk.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", pkdk.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkdk.SoTiepNhan,"MSG_SEN02",pkdk.SoTiepNhan.ToString(), false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");

                    txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //MLMessages("Đã hủy phụ kiện này","MSG_CAN01","", false);
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.","MSG_SEN03","", false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu","MSG_SEN05","", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not Approved");
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            string xmlCurrent = "";
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                //DATLMQ BỔ SUNG XML LAYPHANHOIDADUYET 08/12/2010
                //Tao XML Header
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(Globals.ConfigPhongBiPhanHoi((int)MessgaseType.ThongTin, (int)MessageFunctions.LayPhanHoi, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, pkdk.GUIDSTR.Trim()));

                //Tao Body XML
                XmlDocument docNPL = new XmlDocument();
                string path = EntityBase.GetPathProgram();
                docNPL.Load(path + @"\B03GiaCong\LayPhanHoiDaDuyet.xml");

                XmlNode root = xml.ImportNode(docNPL.SelectSingleNode("Root"), true);
                //root.SelectSingleNode("SoTKHQTC").InnerText = this.SoTiepNhan.ToString();
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = GlobalSettings.MA_DON_VI;
                root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = GlobalSettings.MA_DON_VI;

                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = GlobalSettings.MA_HAI_QUAN.Trim();
                root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN));

                //this.GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                //this.Update();
                root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = HD.GUIDSTR.Trim();

                XmlNode Content = xml.GetElementsByTagName("Content")[0];
                Content.AppendChild(root);

                //xmlCurrent = pkdk.LayPhanHoi(pass, sendXML.msg);//linh mark
                xmlCurrent = pkdk.TQDTLayPhanHoi(pass, xml.InnerXml);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(pass);
                        }
                        else
                        {
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            NhanDuLieuPK.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            XacNhanThongTin.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        return;
                    }
                }

                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", pkdk.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + pkdk.SoTiepNhan,"MSG_SEN02",pkdk.SoTiepNhan.ToString(), false);
                    lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                    txtSoTiepNhan.Text = pkdk.SoTiepNhan.ToString();
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702019");
                    //MLMessages("Đã hủy phụ kiện này", "MSG_CAN01", "", false);
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                    txtSoTiepNhan.Text = "";
                }
                else if (sendXML.func == 2)
                {
                    if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";

                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_2702018", mess);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN03", "", false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");

                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa xử lý danh sách sản phẩm này!", "MSG_STN06", "", false);
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN05", "", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not Approved");
                        txtSoTiepNhan.Text = "";
                    }
                }
                setCommandStatus();
                //xoa thông tin msg nay trong database
                sendXML.Delete();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận thông tin. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới phụ kiện.
        /// </summary>
        private void add()
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            if (pkdk.ID == 0)
            {
                pkdk.HopDong_ID = this.HD.ID;
                long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if (id > 0)
                {
                    // showMsg("MSG_240202", id);
                    //MLMessages("Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID=" + id.ToString(),"MSG_WRN33",id.ToString(), false);
                    // return;
                }
                pkdk.GhiChu = txtNoiDung.Text.Trim();
                pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                pkdk.TrangThaiXuLy = -1;

                pkdk.NgayPhuKien = ccNgayPhuKien.Value;

                pkdk.MaHaiQuan = HD.MaHaiQuan;
                pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (pkdk.ID == 0)
                    pkdk.Insert();
                else
                    pkdk.Update();
                //pkdk.InsertUpDateFull();
                this.setErro();
                setCommandStatus();

            }
            if (cbLoaiPhuKien.Value == null || cbLoaiPhuKien.Value.ToString() == "")
            {
                showMsg("MSG_2702020");
                //MLMessages("Bạn chưa chọn loại phụ kiện.","MSG_WRN11","", false);
                return;
            }
            string id_loaiphukien = cbLoaiPhuKien.Value.ToString();
            //long id = pkdk.checkPhuKienCungLoai(id_loaiphukien, pkdk.ID, pkdk.HopDong_ID);
            //if (id > 0 && id_loaiphukien != "S13" && id_loaiphukien != "T07" && id_loaiphukien!="N01")
            //{
            //    showMsg("MSG_240201", id);
            //    //MLMessages("Có phụ kiện cùng loại này chưa được hải quan duyệt nằm trong danh sách phụ kiện có ID=" + id.ToString(), "MSG_WRN32", id.ToString(), false);
            //    return;
            //}
            ShowForm(id_loaiphukien);
        }

        private void ShowForm(string id_loaiphukien)
        {
            switch (id_loaiphukien.Trim())
            {
                case "101": { showPKHuyHD(true); } break;//Hủy hợp đồng
                case "201": { showPKGiaHanHD(false); } break;//Gia hạn hợp đồng
                #region Sản phẩm
                case "802": { showPKDieuChinhNhomSP(false, "802"); } break;//Thêm mới
                case "502": { showPKDieuChinhNhomSP(false, "502"); } break;//Sửa
                case "102": { showPKDieuChinhNhomSP(false, "102"); } break;//Hủy
                #endregion Sản phẩm

                #region Nguyên phụ liệu
                case "803": { showPKBoSungNPL(true, "803"); } break;//Thêm mới
                case "503": { showPKBoSungNPL(true, "503"); } break;//Sửa
                case "103": { showPKBoSungNPL(true, "103"); } break;//Hủy
                #endregion Nguyên phụ liệu
                #region Thiết bị
                case "804": { showPKBoSungThietBi(false, "804"); } break;//Thêm mới
                case "504": { showPKBoSungThietBi(false, "504"); } break;//Sửa
                case "104": { showPKBoSungThietBi(false, "104"); } break;//Hủy
                #endregion Thiết bị
                #region Hàng mẫu
                case "805": { showHangMau(false, "805"); } break;//Thêm mới
                case "505": { showHangMau(false, "505"); } break;//Sửa
                case "105": { showHangMau(false, "105"); } break;//Hủy
                #endregion Hàng mẫu


            }
        }
        private void showPKDieuChinhSLSanPham(bool isAdd)
        {
            FormDieuChinhSLSP f = new FormDieuChinhSLSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            string id_loaiphukien = e.Row.Cells[1].Text.Trim();
            ShowForm(id_loaiphukien);
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        //private static string loaipk = "";
        private void save()
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            try
            {
                HD = HopDong.Load(HD.ID);
                pkdk.HopDong_ID = this.HD.ID;
                long id = pkdk.checkExitsSoPhuKien(txtSoPhuKien.Text.Trim());
                if (id > 0)
                {
                    //showMsg("MSG_240202", id);
                    ////MLMessages("Số phụ kiện này đã được đăng ký trong danh sách phụ kiện có ID=" + id.ToString(),"MSG_WRN33",id.ToString(), false);
                    //return;
                }
                this.Cursor = Cursors.WaitCursor;
                {
                    pkdk.GhiChu = txtNoiDung.Text.Trim();

                    pkdk.SoPhuKien = txtSoPhuKien.Text.Trim();
                    pkdk.TrangThaiXuLy = -1;

                    pkdk.NgayPhuKien = ccNgayPhuKien.Value;

                    pkdk.MaHaiQuan = HD.MaHaiQuan;
                    //pkdk.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    pkdk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (string.IsNullOrEmpty(pkdk.GUIDSTR))
                        pkdk.GUIDSTR = Guid.NewGuid().ToString();
                    if (pkdk.ID == 0)
                    {

                        pkdk.Insert();
                    }
                    else
                        pkdk.Update();

                    #region Lưu log thao tác
                    if (this.OpenType == OpenFormType.Insert)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", pkdk.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                        else
                        {
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao log = new Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao();
                            log.LoaiKhaiBao = Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien;
                            log.ID_DK = pkdk.ID;
                            log.GUIDSTR_DK = pkdk.GUIDSTR;
                            log.UserNameKhaiBao = GlobalSettings.UserLog;
                            log.NgayKhaiBao = DateTime.Now;
                            log.UserNameSuaDoi = GlobalSettings.UserLog;
                            log.NgaySuaDoi = DateTime.Now;
                            log.GhiChu = "";
                            log.IsDelete = false;
                            log.Insert();
                        }
                    }
                    else if (this.OpenType == OpenFormType.Edit)
                    {
                        string where = "1 = 1";
                        where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", pkdk.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.PhuKien);
                        List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                        if (listLog.Count > 0)
                        {
                            long idLog = listLog[0].IDLog;
                            string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                            long idDK = listLog[0].ID_DK;
                            string guidstr = listLog[0].GUIDSTR_DK;
                            string userKhaiBao = listLog[0].UserNameKhaiBao;
                            DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                            string userSuaDoi = GlobalSettings.UserLog;
                            DateTime ngaySuaDoi = DateTime.Now;
                            string ghiChu = listLog[0].GhiChu;
                            bool isDelete = listLog[0].IsDelete;
                            Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                        userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                        }
                    }
                    #endregion

                    //pkdk.InsertUpDateFull();
                    showMsg("MSG_SAV02");
                    this.setErro();
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //MessageBox.Show("Có lỗi:" + ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtSoHopDong, null);
            error.SetError(ccNgayPhuKien, null);
            //error.SetError(txtMaHS, null);
            error.SetError(cbLoaiPhuKien, null);

        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.save();
                    break;
                case "cmdAdd":
                    this.add();
                    break;
                case "cmdSend":
                    this.SendV3();
                    //this.send();
                    break;
                case "HuyKhaiBao":
                    this.CanceldV3();
                    //this.Huy();
                    break;
                case "NhanDuLieuPK":
                    //this.NhanDuLieuPK12();
                    FeedBackV3();
                    break;
                case "XacNhanThongTin":
                    FeedBackV3();
                    //this.LaySoTiepNhanDT();
                    break;
                case "InPhieuTN": this.inPhieuTN(); break;
            }
        }
        private void inPhieuTN()
        {
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "PHỤ KIỆN";
            phieuTN.soTN = this.pkdk.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.pkdk.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = pkdk.MaHaiQuan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        public void NhanDuLieuPK12()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }
            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //DATLMQ comment 09/01/2011
                //string xmlCurrent = pkdk.WSDownLoad(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);

            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        public void Huy()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    showMsg("MSG_240203");
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }

            string password = "";
            WSForm wsForm = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                wsForm.ShowDialog(this);
                if (!wsForm.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = wsForm.txtMatKhau.Text.Trim();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                string xmlCurrent = pkdk.WSCancel(password);
                this.Cursor = Cursors.Default;
                sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = "PK";
                sendXML.master_id = pkdk.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.","MSG_WRN12","", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message.Trim());
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message,"MSG_WRN13",ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy phụ kiện. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void showPKGiaHanHD(bool isAdd)
        {
            PKGiahanHDForm f = new PKGiahanHDForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void showPKHuyHD(bool isAdd)
        {
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "101")
                {
                    this.ShowMessage("Loại phụ kiện \"" + pk.NoiDung + "\" đã tồn tại", false);
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "101";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKMoPhuKienDocLap(bool isAdd)
        {
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "H11")
                {
                    return;
                }
            }
            if (isAdd)
            {
                try
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "H11";
                    LoaiPK.NoiDung = cbLoaiPhuKien.Text;
                    LoaiPK.Master_ID = pkdk.ID;
                    if (LoaiPK.ID == 0)
                        LoaiPK.Insert();
                    else
                        LoaiPK.Update();
                    pkdk.PKCollection.Add(LoaiPK);
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi : " + ex.Message, false);
                }
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungNPL(bool isAdd, string MaLPK)
        {
            NguyenPhuLieuGCBoSungForm f = new NguyenPhuLieuGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = MaLPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhSoLuongNPL(bool isAdd)
        {
            FormDieuChinhSLNPL f = new FormDieuChinhSLNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhDVTNPL(bool isAdd)
        {
            FormDieuChinhDVTNPL f = new FormDieuChinhDVTNPL();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungNPLVietNam(bool isAdd)
        {
            FormNPLNhapVN f = new FormNPLNhapVN();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhDVTSP(bool isAdd)
        {
            FormDieuChinhDVTSP f = new FormDieuChinhDVTSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhMaSP(bool isAdd)
        {
            FormDieuChinhMaSP f = new FormDieuChinhMaSP();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhChiTietMaSP(bool isAdd)
        {
            SanPhamGCBoSungForm f = new SanPhamGCBoSungForm();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhNhomSP(bool isAdd, string maLoaiPK)
        {

            FormSanPham f = new FormSanPham();
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = maLoaiPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showHangMau(bool isAdd, string maLoaiPK)
        {

            FormHangMau f = new FormHangMau();
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = maLoaiPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKBoSungThietBi(bool isAdd, string maLoaiPK)
        {
            ThietBiGCBoSungForm f = new ThietBiGCBoSungForm();
            f.MaLoaiPK = maLoaiPK;
            f.HD = HD;
            f.pkdk = pkdk;
            f.MaLoaiPK = maLoaiPK;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void showPKDieuChinhThietBi(bool isAdd)
        {
            FormDieuChinhSLThietBi f = new FormDieuChinhSLThietBi();
            f.HD = HD;
            f.pkdk = pkdk;
            f.ShowDialog();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        public bool CheckExitNPLInMuaVN()
        {
            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
            Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPKMuaVN = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N01")
                {
                    LoaiPK = pk;
                    break;
                }
            }

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "N11")
                {
                    LoaiPKMuaVN = pk;
                    break;
                }
            }
            foreach (HangPhuKien HangPKNPLBoSung in LoaiPK.HPKCollection)
            {
                foreach (HangPhuKien HangPK in LoaiPKMuaVN.HPKCollection)
                {
                    if (HangPK.MaHang.Trim().ToUpper() == HangPKNPLBoSung.MaHang.Trim().ToUpper())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)e.Row.DataRow;
                    if (LoaiPK.ID > 0)
                    {
                        if (LoaiPK.MaPhuKien.Trim() == "N01")
                        {
                            if (CheckExitNPLInMuaVN())
                            {
                                showMsg("MSG_2702067");
                                e.Cancel = true;
                                return;
                            }
                        }
                        LoaiPK.Delete(this.pkdk.HopDong_ID);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void txtSoPhuKien_TextChanged(object sender, EventArgs e)
        {
            // this.isUpdate = true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                Company.GC.BLL.KDT.GC.LoaiPhuKienCollection loaiPKColl = new LoaiPhuKienCollection();
                foreach (GridEXSelectedItem item in items)
                {
                    if (item.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = (Company.GC.BLL.KDT.GC.LoaiPhuKien)item.GetRow().DataRow;
                        loaiPKColl.Add(LoaiPK);
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien item in loaiPKColl)
                {
                    if (item.ID > 0)
                    {
                        if (item.MaPhuKien.Trim() == "N01")
                        {
                            if (CheckExitNPLInMuaVN())
                            {
                                showMsg("MSG_2702067");
                                //MLMessages("Danh sách nguyên phụ liệu bổ sung có nguyên phụ liệu liên quan tới bên loại phụ kiện mua Việt Nam nên không xóa được.","MSG_WRN34","", false);
                                return;
                            }
                        }
                        pkdk.PKCollection.Remove(item);
                    }
                }
                dgList.DataSource = pkdk.PKCollection;
                dgList.Refetch();
            }
        }
        #region V3 - 14/03/2012
        private void SendV3()
        {

            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            {
                sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                sendXML.master_id = pkdk.ID;
                if (sendXML.Load())
                {
                    NhanDuLieuPK.Enabled = NhanDuLieuPK1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    showMsg("MSG_240203");
                    //MLMessages("Thông tin này đã gửi thông tin tới hải quan nhưng chưa có phản hồi.","MSG_SEN11","", false);
                    return;
                }
            }
            if (pkdk.ID == 0)
            {
                ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                return;
            }
            if (pkdk.PKCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập phụ kiện hoặc chưa lưu thông tin", false);
                //MLMessages("Chưa nhập phụ kiện","MSG_WRN19","", false);
                return;
            }
            try
            {
                pkdk.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_PhuKien pk = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferPhuKien(pkdk, HD);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = pkdk.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan),
                                     Identity = pkdk.MaHaiQuan
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                                    Function = pk.Function,
                                    Reference = pkdk.GUIDSTR,
                                }
                                ,
                                pk);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoPhuKien);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
                    sendXML.master_id = pkdk.ID;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();
                    pkdk.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Thông báo lỗi\r\n" + ex.Message, false);
            }
        }
        void SendMessage(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<Company.KDT.SHARE.Components.Messages.Send.SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, Company.KDT.SHARE.Components.Messages.Send.SendEventArgs e)
        {
            feedbackContent = SingleMessage.PhuKienSendHandler(pkdk, ref msgInfor, e);

        }
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                Reference = pkdk.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = pkdk.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan.Trim()),
                                              Identity = pkdk.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        setCommandStatus();
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                }
            }
            setCommandStatus();
        }
        private void CanceldV3()
        {
            //Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien;
            //sendXML.master_id = pkdk.ID;
            //if (sendXML.Load())
            //{
            //    showMsg("MSG_WRN05");
            //    //ShowMessage("Định mức đang đợi xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
            //    return;
            //}
            if (ShowMessage("Bạn có muốn hủy khai báo", true) == "No") return;
            Company.KDT.SHARE.Components.DeclarationBase phukien = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG, pkdk.GUIDSTR, pkdk.SoTiepNhan, pkdk.MaHaiQuan, pkdk.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = pkdk.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(pkdk.MaHaiQuan),
                                 Identity = pkdk.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.GC_PHU_KIEN_HOP_DONG,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = pkdk.GUIDSTR,
                            }
                            ,
                            phukien);
            SendMessageForm sendForm = new SendMessageForm();
            pkdk.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(pkdk.ID, Company.KDT.SHARE.Components.MessageTitle.HQHuyKhaiBaoToKhai);
                //sendXML.func = 3;
                // sendXML.msg = msgSend;
                //sendXML.InsertUpdate();
                pkdk.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }

        #endregion
    }
}
