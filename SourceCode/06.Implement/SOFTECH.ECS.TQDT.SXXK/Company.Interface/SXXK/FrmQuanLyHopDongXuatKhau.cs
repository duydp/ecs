﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class FrmQuanLyHopDongXuatKhau : Company.Interface.BaseForm
    {
        //Fields       
        private List<HopDongXuatKhau> HDXKCollection = new List<HopDongXuatKhau>();
        private HopDongXuatKhau hdxk;
        public Company.BLL.KDT.SXXK.HoSoThanhLyDangKy HSTL = new Company.BLL.KDT.SXXK.HoSoThanhLyDangKy();
        string errors=null;

        public FrmQuanLyHopDongXuatKhau()
        {
            InitializeComponent();
        }

        /// <summary>
        ///Kiểm tra dữ liệu của các textbox trước khi đưa vào lưới
        /// </summary>
        /// <returns> bool</returns>       

        private void btnThemMoi_Click(object sender, EventArgs e)
        {
            if (!AddNewEntity())
                MessageBox.Show("Thất bại, Vui Lòng kiểm tra lại dữ liệu!");
            else
            {
                MessageBox.Show("Đã thêm vào lưới");
                this.refreshGrid();
                
               
            }
        }

        /// <summary>
        /// Thêm mới 1 Hợp Đồng Xuất Khẩu
        /// </summary>
        /// <returns> bool</returns>
        private bool AddNewEntity()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //thông tin không hợp lệ
                if (!ValidateInfo()) 
                    return false;
                //nếu đã tồn tại
                if (TestDelarationContract() > 0)
                {
                    MessageBox.Show("Dòng này đã tồn tại!");
                    return false;
                   
                }
                hdxk = new HopDongXuatKhau();
                hdxk.SoToKhai = Int64.Parse(cboSoToKhai.Text);
                hdxk.MaLoaiHinh = cboMaLoaiHinh.Text;
                hdxk.NgayDangKy = dtNgayDangKy.Value;
                hdxk.SoHopDong = txtSoHopDong.Text;
                hdxk.NgayKy = dtNgayKyHopDong.Value;
                hdxk.TriGia = numTriGia.Value;
                hdxk.GhiChu = txtGhiChu.Text;


                HopDongXuatKhau.InsertHopDongXuatKhau(hdxk.SoToKhai, hdxk.MaLoaiHinh, hdxk.NgayDangKy, hdxk.SoHopDong, hdxk.NgayKy, hdxk.TriGia, hdxk.GhiChu);

                //làm rỗng các textboxt
                DeleteTextbox();
                return true;



            }
            catch (Exception ex)
            {
                //Logger.LocalLogger.Instance().WriteMessage(ex);
                errors = ex.ToString();

                Logger.LocalLogger.Instance().WriteMessage(ex);                
                return false;
            }
            finally //trả về trạng thái mặc định dù có lỗi or không.
            {
                Cursor = Cursors.Default;
            }

        }

        private void DeleteTextbox()
        {

            cboSoToKhai.Text = "";
            
            txtSoHopDong.Text = "";
            numTriGia.Value = 0;
            txtGhiChu.Text = "";
        }

        private void FrmQuanLyHopDongXuatKhau_Load(object sender, EventArgs e)
        {
            LoadSoToKhai();

            refreshGrid();
            btnCapNhat.Enabled = false;
        }

        private void refreshGrid()
        {
            HDXKCollection = HopDongXuatKhau.SelectCollectionAll();
            bindingSource1.DataSource = HDXKCollection;
            grdHopDongXuatKhau.DataSource = bindingSource1.DataSource;
        }

       

        private void btnSua_Click(object sender, EventArgs e)
        {

        }

        private void grdHopDongXuatKhau_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            btnCapNhat.Enabled = true;
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                hdxk = (HopDongXuatKhau)e.Row.DataRow;


                cboSoToKhai.Text = hdxk.SoToKhai.ToString();
                cboMaLoaiHinh.Text = hdxk.MaLoaiHinh;
                dtNgayDangKy.Value = hdxk.NgayDangKy;
                txtSoHopDong.Text = hdxk.SoHopDong;
                dtNgayKyHopDong.Value = hdxk.NgayKy;
                numTriGia.Value = hdxk.TriGia;
                txtGhiChu.Text = hdxk.GhiChu;

            }
        }

        //private void btnXoa_Click(object sender, EventArgs e)
        //{
        //    Janus.Windows.GridEX.GridEXSelectedItemCollection items = grdHopDongXuatKhau.SelectedItems;
        //    if (items.Count <= 0)
        //        return;
        //    if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
        //    {
        //        this.Cursor = Cursors.WaitCursor;

        //        foreach (Janus.Windows.GridEX.GridEXSelectedItem i in items)
        //        {
        //            if (i.RowType == Janus.Windows.GridEX.RowType.Record)

        //                hdxk = (HopDongXuatKhau)i.GetRow().DataRow;

        //        }
        //    }
        //    try
        //    {
        //        //  HopDongXuatKhau.DeleteHopDongXuatKhau(hdxk.Id);             
        //        ////  HDXKCollection.Remove(hdxk);               
        //        // // MLMessages("Xóa thành công!", "MSG_PUB14", "", false);               
        //        //  MessageBox.Show("Xóa thành công", "thông báo");
        //        //  refreshGrid();

        //        HDXKCollection.Remove(hdxk);
        //        HopDongXuatKhau.DeleteHopDongXuatKhau(hdxk.Id);
        //        refreshGrid();
        //        //MLMessages("Xóa thành công!", "MSG_PUB14", "", false);
        //        //grdHopDongXuatKhau.Refetch();
        //    }
        //    catch (Exception ex)
        //    {
        //        ShowMessage(" " + ex.Message, false);
        //        this.Cursor = Cursors.Default;
        //    }
        //    this.Cursor = Cursors.Default;

        //}

        private void btnCapNhat_Click_1(object sender, EventArgs e)
        {
           
                if (ShowMessage("Bạn muốn cập nhật không?", true) == "Yes")
                {
                    if (!ValidateInfo()) return;
                    if (TestDelarationContract() > 0)
                    {
                        MessageBox.Show("Dòng này đã tồn tại!");
                        return;
                    }
                    try
                    {
                        HopDongXuatKhau.InsertUpdateHopDongXuatKhau(hdxk.Id, long.Parse(cboSoToKhai.Text), cboMaLoaiHinh.Text, dtNgayDangKy.Value, txtSoHopDong.Text, dtNgayKyHopDong.Value, numTriGia.Value, txtGhiChu.Text);

                        refreshGrid();
                        btnCapNhat.Enabled = false;
                        btnThemMoi.Enabled = true;
                        MessageBox.Show("cập nhật không thành công!");
                        DeleteTextbox();

                    }
                    catch (Exception exe)
                    {
                        MessageBox.Show("cập nhật không thành công!");
                        Logger.LocalLogger.Instance().WriteMessage(exe);

                    }
                }

            
        }

       private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region BEGIN TextAutoCompleted

        private Company.BLL.KDT.SXXK.BKToKhaiXuatCollection toKhaiXuatCollection = new Company.BLL.KDT.SXXK.BKToKhaiXuatCollection();

        private void TextAutoCompleted()
        {
            try
            {
                if (HSTL == null) return;

                int idBKTKX = this.HSTL.getBKToKhaiXuat();

                this.HSTL.BKCollection[idBKTKX].LoadChiTietBangKe();

                toKhaiXuatCollection = this.HSTL.BKCollection[idBKTKX].bkTKXColletion;

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();

                foreach (Company.BLL.KDT.SXXK.BKToKhaiXuat bk in toKhaiXuatCollection)
                {
                    col.Add(bk.SoToKhai.ToString());
                }

                cboSoToKhai.AutoCompleteCustomSource = col;

                cboSoToKhai.Focus();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void LoadSoToKhai()
        {
            try
            {
                if (HSTL == null) return;

                int idBKTKX = this.HSTL.getBKToKhaiXuat();

                this.HSTL.BKCollection[idBKTKX].LoadChiTietBangKe();

                toKhaiXuatCollection = this.HSTL.BKCollection[idBKTKX].bkTKXColletion;

                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();

                cboSoToKhai.Items.Clear();
                foreach (Company.BLL.KDT.SXXK.BKToKhaiXuat bk in toKhaiXuatCollection)
                {
                    cboSoToKhai.Items.Add(bk.SoToKhai);
                }

                cboSoToKhai.SelectedIndex = 0;
                cboSoToKhai.Focus();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private Company.BLL.KDT.SXXK.BKToKhaiXuat FindBangKe(Company.BLL.KDT.SXXK.BKToKhaiXuatCollection collections, long soToKhai)
        {
            foreach (Company.BLL.KDT.SXXK.BKToKhaiXuat bk in collections)
            {
                if (bk.SoToKhai == soToKhai)
                {
                    return bk;
                }
            }

            return null;
        }

        private void cboSoToKhai_Leave(object sender, EventArgs e)
        {
            try
            {
                long soToKhai = 0;

                if (Company.KDT.SHARE.Components.Globals.isNumber(cboSoToKhai.Text.Trim()))
                    soToKhai = Convert.ToInt64(cboSoToKhai.Text.Trim());

                //Lay to khai xuat trong Bang ke to khai xuat
                Company.BLL.KDT.SXXK.BKToKhaiXuat bk = FindBangKe(toKhaiXuatCollection, soToKhai);

                //Lay thong tin to khai
                Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                TKMD.SoToKhai = bk.SoToKhai;
                TKMD.NamDangKy = bk.NamDangKy;
                TKMD.MaLoaiHinh = bk.MaLoaiHinh;
                TKMD.MaHaiQuan = bk.MaHaiQuan;
                TKMD.Load();

                //Gan gia tri
                cboMaLoaiHinh.Text = TKMD.MaLoaiHinh;
                dtNgayDangKy.Value = TKMD.NgayDangKy;
                txtSoHopDong.Text = TKMD.SoHopDong;
                dtNgayKyHopDong.Value = TKMD.NgayHopDong;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion END TextAutoCompleted

        #region Begin VALIDATE HOP DONG

        ErrorProvider err = new ErrorProvider();

        /// <summary>
        /// Kiểm tra ràng buộc.
        /// </summary>
        /// <returns></returns>
        /// Hungtq, Update 30052010.
        private bool ValidateInfo()
        {
            bool isValid = true;

            //SoToKhai
            isValid = Globals.ValidateNull(cboSoToKhai, err, "Số tờ khai");

            //MaLoaiHinh
            isValid &= Globals.ValidateNull(cboMaLoaiHinh, err, "Mã loại hình");
            
            //SoHopDong
            //isValid &= Globals.ValidateNull(txtSoHopDong, err, "Số hợp đồng");
            err.SetError(txtSoHopDong, "");
            if (txtSoHopDong.Text == "")
            {
                err.SetError(txtSoHopDong, "Số hợp đồng không được để trống!");
                isValid = false;
 
            }
            if (txtSoHopDong.Text.Length > 150)
            {
                err.SetError(txtSoHopDong, "Số hợp đồng quá dài, 0< chiều dài<=150");
                isValid = false;
 
 
            }

            //TriGia
            //isValid &= Globals.ValidateNumber(numTriGia, err, "Trị giá hợp đồng");
            err.SetError(numTriGia, "");
            if (numTriGia.Value == 0)
            {
                err.SetError(numTriGia, "Trị giá hợp đồng phải > 0");
                isValid = false;
            }
            if (numTriGia.Value >1000000000)
            {
                err.SetError(numTriGia, "Trị giá hợp đồng phải <= 1000000000");
                isValid = false;
            }
            //ghi chu
            if (txtGhiChu.Text.Length >255)
            {
                err.SetError(numTriGia, "chuổi quá dài, độ dài phải <=255");
                isValid = false;
            }
            

            return isValid;
        }

        #endregion End VALIDATE HOP DONG

        private void cboSoToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboSoToKhai.Invoke(new MethodInvoker(
                delegate
                {
                    cboSoToKhai_Leave(sender, e);
                }
                ));
        }

        /// <summary>
        /// kiểm tra nếu tồn tại rồi thì  không Insert or update
        /// </summary>
        /// <returns></returns>
        private int TestDelarationContract()
        {
            string condition = string.Format("SoToKhai = {0} and SoHopDong = '{1}'",cboSoToKhai.Text,txtSoHopDong.Text);

            HDXKCollection = HopDongXuatKhau.SelectCollectionDynamic(condition, null);
            int countRow = HDXKCollection.Count;
            return countRow;

        }

        int rowindex;
        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = grdHopDongXuatKhau.SelectedItems;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDongXuatKhau hdxk = (HopDongXuatKhau)i.GetRow().DataRow;
                        try
                        {
                            hdxk.Delete();
                            
                        }
                        catch (Exception exe)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(exe);
                        }
                    }
                }
               // grdHopDongXuatKhau.DataSource = new MsgSend().SelectCollectionAll();
                try
                {
                    MessageBox.Show("Thành công!");
                    refreshGrid(); 
                }
                catch { grdHopDongXuatKhau.Refresh(); }

            }
        }

        private void grdHopDongXuatKhau_Click(object sender, EventArgs e)
        {
            
        }

        

       
       

    }
}

