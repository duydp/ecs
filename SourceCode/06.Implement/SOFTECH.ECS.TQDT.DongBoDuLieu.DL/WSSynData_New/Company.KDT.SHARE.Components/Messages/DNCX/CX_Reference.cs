﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.DNCX
{
    public class CX_Reference
    {
        /// <summary>
        /// Số tờ khai 
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        /// <summary>
        /// Mã loại hình xuất khẩu
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
        /// <summary>
        /// Ngày đăng ký tờ khai xuất khẩu
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }
        /// <summary>
        /// Mã hải quan đăng ký tờ khai xuất khẩu
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

    }
}
