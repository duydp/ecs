﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Net;

namespace Company.Interface.ProdInfo
{
    public partial class frmSendSupport : Company.Interface.BaseForm
    {
#if KD_V3 || KD_V4
        string PhanHe = "KINH DOANH VÀ ĐẦU TƯ V5";
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif SXXK_V3 || SXXK_V4
        string PhanHe = "SẢN XUẤT - XUẤT KHẨU V5";
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#elif GC_V3 || GC_V4
        string PhanHe = "GIA CÔNG V5";
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#endif
        public frmSendSupport()
        {
            InitializeComponent();
        }

        private void frmSendSupport_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            txtTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            txtDiaChiDoanhNghiep.Text = GlobalSettings.DIA_CHI;
            txtNguoiLH.Text = GlobalSettings.NguoiLienHe;
            txtDienThoai.Text = GlobalSettings.SoDienThoaiDN;
            txtEmail.Text = GlobalSettings.MailDoanhNghiep;

            #region Load image from  this form's resource

            // Gets a reference to the same assembly that 
            // contains the type that is creating the ResourceManager.
            System.Reflection.Assembly myAssembly;
            myAssembly = this.GetType().Assembly;

            // Gets a reference to a different assembly.
            System.Reflection.Assembly myOtherAssembly;
            myOtherAssembly = System.Reflection.Assembly.Load(AsemblyName);

            // Creates the ResourceManager.
            System.Resources.ResourceManager myManager = new
               System.Resources.ResourceManager("Company.Interface.ProdInfo.frmRegisteOnline",
               myAssembly);

            // Retrieves String and Image resources.
            System.Drawing.Image myImage;
            myImage = (System.Drawing.Image)myManager.GetObject(imageName);
            #endregion
            if (myImage !=null)
            {
                pictureBox1.Image = myImage;
            }
            else
            {
#if GC_V4
                pictureBox1.Image = Image.FromFile(@"Resources\GC.png");
#elif SXXK_V4
                pictureBox1.Image = Image.FromFile(@"Resources\SXXK.png");
#endif
            }      
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTenDoanhNghiep, errorProvider, " Tên doanh nghiệp ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaDoanhNghiep, errorProvider, " Mã doanh nghiệp ");
                isValid &= ValidateControl.ValidateNull(txtDiaChiDoanhNghiep, errorProvider, " Địa chỉ doanh nghiệp ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDienThoai, errorProvider, " Số điện thoại ", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtEmail, errorProvider, " Email ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNguoiLH, errorProvider, " Người liên hệ ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                    if (!ValidateForm(false))
                        return;
                    if (!CheckEmail(txtEmail.Text))
                    {
                        ShowMessage("Email không hợp lệ", false);
                        return;
                    }
                    string notice;
                    if (!sendmail())
                    {

                        notice = "Không thể gửi thông tin Hỗ trợ phần mềm xin kiểm tra lại kết nối mạng (hoặc có thể bị chặn bởi firewall hoặc AntiVirust)";
                        notice += "\r\n\r\n-----------------------------------------------------------------";
                        notice += "\r\nNếu không thể gửi thông tin lại, bạn có thể gửi mail liên lạc đến địa chỉ: ecs@softech.vn";
                        notice += "\r\nHoặc qua số điện thoại : (0236) 3.840.888";
                        this.ShowMessageTQDT(notice, false);
                    }
                    else
                    {
                        notice = " Bạn đã gửi thông tin Yêu cầu Hỗ trợ phần mềm đến chúng tôi.\r\n Tối đa trong vòng 15 phút tới chúng tôi sẽ liên lạc với bạn để hỗ trợ.";
                        notice += "\r\n\r\n-----------------------------------------------------------------";
                        notice += "\r\n Mọi chi tiết xin liên hệ: CÔNG TY CP SOFTECH - 38 YÊN BÁI - TP.ĐÀ NẴNG.\r\n Số điện thoại: (0236) 3.840.888";
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ismail", "1");
                        this.ShowMessageTQDT(notice, false);
                        this.Close();
                    }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private static bool CheckEmail(string str)
        {
            Regex re = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }
        public bool sendmail()
        {
            bool isSend;
            try
            {
                string tenDN = txtTenDoanhNghiep.Text.Trim();
                string maDn = txtMaDoanhNghiep.Text.Trim();
                string dienthoai = txtDienThoai.Text.Trim();
                string nguoiLH = txtNguoiLH.Text.Trim();
                string diachi = txtDiaChiDoanhNghiep.Text.Trim();
                string email = txtEmail.Text.Trim();
                string TeamviewID = txtTeamviewID.Text;
                string TeamviewPassword = txtTeamviewPW.Text;
                string UtraviewID = txtUtraviewID.Text;
                string UtraviewPassword = txtUtraviewPW.Text;
                string subj = "GỬI YÊU CẦU HỖ TRỢ PHẦN MỀM " + PhanHe;
                isSend = sendEmail(subj, tenDN, maDn,
                    dienthoai, nguoiLH, diachi, email,TeamviewID,TeamviewPassword,UtraviewID,UtraviewPassword);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            return isSend;
        }
        public bool sendEmail(string subject, string tenDN, string maDN, string soDT, string nguoiLH, string diachi, string email, string TeamviewID, string TeamviewPassword, string UtraviewID, string UtraviewPassword)
        {
            try
            {
                string toEmail = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ToMail");
                if (!CheckEmail(toEmail))
                    toEmail = "ecs@softech.vn";
                string fromEmail = "hotrotqdt@gmail.com";
                string content = "<font color=#1F497D><b>GỬI YÊU CẦU HỖ TRỢ PHẦN MỀM " + PhanHe + "</b><br><b>Tên doanh nghiệp:</b> " + tenDN + "<br>";
                content += string.IsNullOrEmpty(maDN) ? string.Empty : "<b>Mã doanh nghiệp: </b>" + maDN + "<br>";
                content += string.IsNullOrEmpty(diachi) ? string.Empty : "<b>Địa chỉ doanh nghiệp: </b>" + diachi + "<br>";
                content += string.IsNullOrEmpty(email) ? string.Empty : "<b>Email liên hệ: </b>" + email + "<br>";
                content += string.IsNullOrEmpty(soDT) ? string.Empty : "<b>Số điện thoại: </b>" + soDT + "<br>";
                content += string.IsNullOrEmpty(nguoiLH) ? string.Empty : "<b>Người liên hệ: </b>" + nguoiLH + "<br>";
                content += string.IsNullOrEmpty(nguoiLH) ? string.Empty : "<b>Teamview ID: </b>" + TeamviewID + "<br>";
                content += string.IsNullOrEmpty(nguoiLH) ? string.Empty : "<b>Teamview Password : </b>" + TeamviewPassword + "<br>";
                content += string.IsNullOrEmpty(nguoiLH) ? string.Empty : "<b>Utraview ID : </b>" + UtraviewID + "<br>";
                content += string.IsNullOrEmpty(nguoiLH) ? string.Empty : "<b>Utraview Password : </b>" + UtraviewPassword + "<br>";
                MailMessage messageObj = new MailMessage();
                messageObj.Subject = subject;
                messageObj.Body = content;
                messageObj.IsBodyHtml = true;
                messageObj.Priority = MailPriority.Normal;

                #region Send Mail

                SmtpClient client = new SmtpClient("smtp.gmail.com", 25);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(fromEmail, "hotrotqdt");
                //client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                messageObj.From = new MailAddress(fromEmail);
                messageObj.To.Add(toEmail);
                #endregion Send Mail
                client.Timeout = 15000;
                this.Cursor = Cursors.WaitCursor;
                client.Send(messageObj);
                this.Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkDowloadTV_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.teamviewer.com/vi/tai-xuong-tu-dong-teamviewer/");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void linkDowloadUtraview_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://ultraviewer.net/vi/download.html");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
