/*
Run this script on:

        192.168.72.151.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.151.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 06/19/2012 10:32:10 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping foreign keys from [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] DROP
CONSTRAINT [FK_t_KDT_AnDinhThue_t_KDT_ToKhaiMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TKMD_ID] [bigint] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TKMD_Ref] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [SoQuyetDinh] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [NgayQuyetDinh] [datetime] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [NgayHetHan] [datetime] NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TaiKhoanKhoBac] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ALTER COLUMN [TenKhoBac] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_DBDL_KetQuaXuLy]'
GO
CREATE TABLE [dbo].[t_KDT_DBDL_KetQuaXuLy]
(
[ID] [bigint] NOT NULL,
[SoToKhai] [bigint] NULL,
[MaDoanhNghiep] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayDongBo] [datetime] NULL,
[TrangThai] [int] NULL,
[GhiChu] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_DBDL_KetQuaXuLy] on [dbo].[t_KDT_DBDL_KetQuaXuLy]'
GO
ALTER TABLE [dbo].[t_KDT_DBDL_KetQuaXuLy] ADD CONSTRAINT [PK_t_KDT_DBDL_KetQuaXuLy] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Insert]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS
INSERT INTO [dbo].[t_KDT_DBDL_KetQuaXuLy]
(
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
)
VALUES
(
	@ID,
	@SoToKhai,
	@MaDoanhNghiep,
	@NgayDongBo,
	@TrangThai,
	@GhiChu,
	@GUIDSTR
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_InsertUpdate]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DBDL_KetQuaXuLy] 
		SET
			[SoToKhai] = @SoToKhai,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[NgayDongBo] = @NgayDongBo,
			[TrangThai] = @TrangThai,
			[GhiChu] = @GhiChu,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_DBDL_KetQuaXuLy]
	(
			[ID],
			[SoToKhai],
			[MaDoanhNghiep],
			[NgayDongBo],
			[TrangThai],
			[GhiChu],
			[GUIDSTR]
	)
	VALUES
	(
			@ID,
			@SoToKhai,
			@MaDoanhNghiep,
			@NgayDongBo,
			@TrangThai,
			@GhiChu,
			@GUIDSTR
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_DBDL_KetQuaXuLy]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_Update]
	@ID bigint,
	@SoToKhai bigint,
	@MaDoanhNghiep nvarchar(15),
	@NgayDongBo datetime,
	@TrangThai int,
	@GhiChu nvarchar(max),
	@GUIDSTR nvarchar(36)
AS

UPDATE
	[dbo].[t_KDT_DBDL_KetQuaXuLy]
SET
	[SoToKhai] = @SoToKhai,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[NgayDongBo] = @NgayDongBo,
	[TrangThai] = @TrangThai,
	[GhiChu] = @GhiChu,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_DBDL_Status]'
GO
CREATE TABLE [dbo].[t_KDT_DBDL_Status]
(
[GUIDSTR] [nvarchar] (36) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MSG_STATUS] [int] NULL,
[MSG_TYPE] [int] NULL,
[CREATE_TIME] [datetime] NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_DBDL_Status] on [dbo].[t_KDT_DBDL_Status]'
GO
ALTER TABLE [dbo].[t_KDT_DBDL_Status] ADD CONSTRAINT [PK_t_KDT_DBDL_Status] PRIMARY KEY CLUSTERED  ([GUIDSTR])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Delete]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Delete]
	@GUIDSTR nvarchar(36)
AS

DELETE FROM 
	[dbo].[t_KDT_DBDL_Status]
WHERE
	[GUIDSTR] = @GUIDSTR


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Insert]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS
INSERT INTO [dbo].[t_KDT_DBDL_Status]
(
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
)
VALUES
(
	@GUIDSTR,
	@MSG_STATUS,
	@MSG_TYPE,
	@CREATE_TIME
)


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_InsertUpdate]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_InsertUpdate]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS
IF EXISTS(SELECT [GUIDSTR] FROM [dbo].[t_KDT_DBDL_Status] WHERE [GUIDSTR] = @GUIDSTR)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DBDL_Status] 
		SET
			[MSG_STATUS] = @MSG_STATUS,
			[MSG_TYPE] = @MSG_TYPE,
			[CREATE_TIME] = @CREATE_TIME
		WHERE
			[GUIDSTR] = @GUIDSTR
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_KDT_DBDL_Status]
	(
			[GUIDSTR],
			[MSG_STATUS],
			[MSG_TYPE],
			[CREATE_TIME]
	)
	VALUES
	(
			@GUIDSTR,
			@MSG_STATUS,
			@MSG_TYPE,
			@CREATE_TIME
	)	
	END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Load]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Load]
	@GUIDSTR nvarchar(36)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM
	[dbo].[t_KDT_DBDL_Status]
WHERE
	[GUIDSTR] = @GUIDSTR

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_SelectAll]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM
	[dbo].[t_KDT_DBDL_Status]	


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_Update]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_Update]
	@GUIDSTR nvarchar(36),
	@MSG_STATUS int,
	@MSG_TYPE int,
	@CREATE_TIME datetime
AS

UPDATE
	[dbo].[t_KDT_DBDL_Status]
SET
	[MSG_STATUS] = @MSG_STATUS,
	[MSG_TYPE] = @MSG_TYPE,
	[CREATE_TIME] = @CREATE_TIME
WHERE
	[GUIDSTR] = @GUIDSTR


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, May 22, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_KetQuaXuLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[MaDoanhNghiep],
	[NgayDongBo],
	[TrangThai],
	[GhiChu],
	[GUIDSTR]
FROM [dbo].[t_KDT_DBDL_KetQuaXuLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_DeleteDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DBDL_Status] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_DBDL_Status_SelectDynamic]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_DBDL_Status_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Thursday, May 24, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DBDL_Status_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[GUIDSTR],
	[MSG_STATUS],
	[MSG_TYPE],
	[CREATE_TIME]
FROM [dbo].[t_KDT_DBDL_Status] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamic]
-- Database: ECS_GCTQ
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, January 06, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicDongBoDuLieu]
	@WhereCondition nvarchar(500),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(3250)

SET @SQL = 'SELECT     t_KDT_DBDL_Status.MSG_STATUS, t_KDT_ToKhaiMauDich.*
FROM         t_KDT_ToKhaiMauDich LEFT OUTER JOIN
                      t_KDT_DBDL_Status ON t_KDT_ToKhaiMauDich.GUIDSTR = t_KDT_DBDL_Status.GUIDSTR WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL
GO
if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='3.1', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('3.1', getdate(), null)
	end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_AnDinhThue]'
GO
ALTER TABLE [dbo].[t_KDT_AnDinhThue] ADD
CONSTRAINT [FK_t_KDT_AnDinhThue_t_KDT_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
