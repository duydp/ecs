﻿namespace TestVNACC
{
    partial class FormTestControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.txtCode = new DevExpress.XtraEditors.TextEdit();
            this.txtName = new DevExpress.XtraEditors.TextEdit();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.txtRefDB = new DevExpress.XtraEditors.TextEdit();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.cboName = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtMa = new DevExpress.XtraEditors.TextEdit();
            this.lookUpEdit1 = new DevExpress.XtraEditors.LookUpEdit();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl9 = new DevExpress.XtraEditors.LabelControl();
            this.txtCodeNull = new DevExpress.XtraEditors.TextEdit();
            this.txtREFNull = new DevExpress.XtraEditors.TextEdit();
            this.txtNameNull = new DevExpress.XtraEditors.TextEdit();
            this.labelControl10 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl11 = new DevExpress.XtraEditors.LabelControl();
            this.txtCodeInput = new DevExpress.XtraEditors.TextEdit();
            this.txtNameInput = new DevExpress.XtraEditors.TextEdit();
            this.button1 = new System.Windows.Forms.Button();
            this.ucCategoryAllowEmpty5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucCategoryAllowEmpty4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucCategoryAllowEmpty3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucCategoryAllowEmpty2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucCategoryAllowEmpty1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucCategoryExtend5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCuaKhauXuatNhap1 = new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap();
            this.ucMaLoaiHinhIDA1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategoryExtend14 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend17 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend20 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucCategoryExtend23 = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ctrDiaDiemXepHang = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategory1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategory2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategory3 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategory4 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategory5 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboName.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeNull.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREFNull.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameNull.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeInput.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameInput.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // labelControl1
            // 
            this.labelControl1.Enabled = false;
            this.labelControl1.Location = new System.Drawing.Point(23, 30);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(89, 13);
            this.labelControl1.TabIndex = 1;
            this.labelControl1.Text = "Mã loại hình (IDA):";
            // 
            // labelControl2
            // 
            this.labelControl2.Enabled = false;
            this.labelControl2.Location = new System.Drawing.Point(81, 126);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(29, 13);
            this.labelControl2.TabIndex = 2;
            this.labelControl2.Text = "Code:";
            // 
            // labelControl3
            // 
            this.labelControl3.Enabled = false;
            this.labelControl3.Location = new System.Drawing.Point(81, 152);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(31, 13);
            this.labelControl3.TabIndex = 2;
            this.labelControl3.Text = "Name:";
            // 
            // txtCode
            // 
            this.txtCode.Location = new System.Drawing.Point(118, 119);
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(202, 20);
            this.txtCode.TabIndex = 3;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(118, 145);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(202, 20);
            this.txtName.TabIndex = 3;
            // 
            // labelControl4
            // 
            this.labelControl4.Enabled = false;
            this.labelControl4.Location = new System.Drawing.Point(45, 100);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(67, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "ReferenceDB:";
            // 
            // txtRefDB
            // 
            this.txtRefDB.Location = new System.Drawing.Point(118, 93);
            this.txtRefDB.Name = "txtRefDB";
            this.txtRefDB.Size = new System.Drawing.Size(202, 20);
            this.txtRefDB.TabIndex = 3;
            // 
            // labelControl5
            // 
            this.labelControl5.Enabled = false;
            this.labelControl5.Location = new System.Drawing.Point(81, 237);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(29, 13);
            this.labelControl5.TabIndex = 2;
            this.labelControl5.Text = "Code:";
            // 
            // labelControl6
            // 
            this.labelControl6.Enabled = false;
            this.labelControl6.Location = new System.Drawing.Point(81, 263);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(31, 13);
            this.labelControl6.TabIndex = 2;
            this.labelControl6.Text = "Name:";
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(118, 230);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(202, 20);
            this.textEdit1.TabIndex = 3;
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(118, 256);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(202, 20);
            this.textEdit2.TabIndex = 3;
            // 
            // cboName
            // 
            this.cboName.Location = new System.Drawing.Point(118, 282);
            this.cboName.Name = "cboName";
            this.cboName.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cboName.Size = new System.Drawing.Size(202, 20);
            this.cboName.TabIndex = 6;
            this.cboName.EditValueChanged += new System.EventHandler(this.cboName_EditValueChanged);
            // 
            // txtMa
            // 
            this.txtMa.Enabled = false;
            this.txtMa.Location = new System.Drawing.Point(63, 282);
            this.txtMa.Name = "txtMa";
            this.txtMa.Size = new System.Drawing.Size(49, 20);
            this.txtMa.TabIndex = 3;
            this.txtMa.Leave += new System.EventHandler(this.txtMa_Leave);
            // 
            // lookUpEdit1
            // 
            this.lookUpEdit1.Location = new System.Drawing.Point(118, 308);
            this.lookUpEdit1.Name = "lookUpEdit1";
            this.lookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.lookUpEdit1.Properties.HeaderClickMode = DevExpress.XtraEditors.Controls.HeaderClickMode.AutoSearch;
            this.lookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            this.lookUpEdit1.Size = new System.Drawing.Size(100, 20);
            this.lookUpEdit1.TabIndex = 7;
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(81, 439);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(29, 13);
            this.labelControl7.TabIndex = 2;
            this.labelControl7.Text = "Code:";
            // 
            // labelControl8
            // 
            this.labelControl8.Location = new System.Drawing.Point(81, 465);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(31, 13);
            this.labelControl8.TabIndex = 2;
            this.labelControl8.Text = "Name:";
            // 
            // labelControl9
            // 
            this.labelControl9.Location = new System.Drawing.Point(45, 413);
            this.labelControl9.Name = "labelControl9";
            this.labelControl9.Size = new System.Drawing.Size(67, 13);
            this.labelControl9.TabIndex = 2;
            this.labelControl9.Text = "ReferenceDB:";
            // 
            // txtCodeNull
            // 
            this.txtCodeNull.Location = new System.Drawing.Point(118, 432);
            this.txtCodeNull.Name = "txtCodeNull";
            this.txtCodeNull.Size = new System.Drawing.Size(202, 20);
            this.txtCodeNull.TabIndex = 3;
            // 
            // txtREFNull
            // 
            this.txtREFNull.Location = new System.Drawing.Point(118, 406);
            this.txtREFNull.Name = "txtREFNull";
            this.txtREFNull.Size = new System.Drawing.Size(202, 20);
            this.txtREFNull.TabIndex = 3;
            // 
            // txtNameNull
            // 
            this.txtNameNull.Location = new System.Drawing.Point(118, 458);
            this.txtNameNull.Name = "txtNameNull";
            this.txtNameNull.Size = new System.Drawing.Size(202, 20);
            this.txtNameNull.TabIndex = 3;
            // 
            // labelControl10
            // 
            this.labelControl10.Location = new System.Drawing.Point(56, 514);
            this.labelControl10.Name = "labelControl10";
            this.labelControl10.Size = new System.Drawing.Size(56, 13);
            this.labelControl10.TabIndex = 2;
            this.labelControl10.Text = "Code input:";
            // 
            // labelControl11
            // 
            this.labelControl11.Location = new System.Drawing.Point(54, 544);
            this.labelControl11.Name = "labelControl11";
            this.labelControl11.Size = new System.Drawing.Size(58, 13);
            this.labelControl11.TabIndex = 2;
            this.labelControl11.Text = "Name input:";
            // 
            // txtCodeInput
            // 
            this.txtCodeInput.Location = new System.Drawing.Point(118, 511);
            this.txtCodeInput.Name = "txtCodeInput";
            this.txtCodeInput.Size = new System.Drawing.Size(202, 20);
            this.txtCodeInput.TabIndex = 3;
            this.txtCodeInput.EditValueChanged += new System.EventHandler(this.txtCodeInput_EditValueChanged);
            // 
            // txtNameInput
            // 
            this.txtNameInput.Location = new System.Drawing.Point(118, 537);
            this.txtNameInput.Name = "txtNameInput";
            this.txtNameInput.Size = new System.Drawing.Size(202, 20);
            this.txtNameInput.TabIndex = 3;
            this.txtNameInput.EditValueChanged += new System.EventHandler(this.txtNameInput_EditValueChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(416, 23);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Hien thi loi";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ucCategoryAllowEmpty5
            // 
            this.ucCategoryAllowEmpty5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A401;
            this.ucCategoryAllowEmpty5.Code = "";
            this.ucCategoryAllowEmpty5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryAllowEmpty5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryAllowEmpty5.IsValidate = true;
            this.ucCategoryAllowEmpty5.Location = new System.Drawing.Point(394, 479);
            this.ucCategoryAllowEmpty5.Name = "ucCategoryAllowEmpty5";
            this.ucCategoryAllowEmpty5.Name_VN = "";
            this.ucCategoryAllowEmpty5.SetValidate = false;
            this.ucCategoryAllowEmpty5.ShowColumnCode = true;
            this.ucCategoryAllowEmpty5.ShowColumnName = true;
            this.ucCategoryAllowEmpty5.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryAllowEmpty5.TabIndex = 8;
            this.ucCategoryAllowEmpty5.TagCode = "";
            this.ucCategoryAllowEmpty5.TagName = "";
            this.ucCategoryAllowEmpty5.WhereCondition = "";
            this.ucCategoryAllowEmpty5.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(this.ucCategoryAllowEmpty1_EditValueChanged);
            // 
            // ucCategoryAllowEmpty4
            // 
            this.ucCategoryAllowEmpty4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucCategoryAllowEmpty4.Code = "";
            this.ucCategoryAllowEmpty4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryAllowEmpty4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryAllowEmpty4.IsValidate = true;
            this.ucCategoryAllowEmpty4.Location = new System.Drawing.Point(394, 447);
            this.ucCategoryAllowEmpty4.Name = "ucCategoryAllowEmpty4";
            this.ucCategoryAllowEmpty4.Name_VN = "";
            this.ucCategoryAllowEmpty4.SetValidate = false;
            this.ucCategoryAllowEmpty4.ShowColumnCode = true;
            this.ucCategoryAllowEmpty4.ShowColumnName = true;
            this.ucCategoryAllowEmpty4.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryAllowEmpty4.TabIndex = 8;
            this.ucCategoryAllowEmpty4.TagCode = "";
            this.ucCategoryAllowEmpty4.TagName = "";
            this.ucCategoryAllowEmpty4.WhereCondition = "";
            this.ucCategoryAllowEmpty4.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(this.ucCategoryAllowEmpty1_EditValueChanged);
            // 
            // ucCategoryAllowEmpty3
            // 
            this.ucCategoryAllowEmpty3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A301;
            this.ucCategoryAllowEmpty3.Code = "";
            this.ucCategoryAllowEmpty3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryAllowEmpty3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryAllowEmpty3.IsValidate = true;
            this.ucCategoryAllowEmpty3.Location = new System.Drawing.Point(394, 415);
            this.ucCategoryAllowEmpty3.Name = "ucCategoryAllowEmpty3";
            this.ucCategoryAllowEmpty3.Name_VN = "";
            this.ucCategoryAllowEmpty3.SetValidate = false;
            this.ucCategoryAllowEmpty3.ShowColumnCode = true;
            this.ucCategoryAllowEmpty3.ShowColumnName = true;
            this.ucCategoryAllowEmpty3.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryAllowEmpty3.TabIndex = 8;
            this.ucCategoryAllowEmpty3.TagCode = "";
            this.ucCategoryAllowEmpty3.TagName = "";
            this.ucCategoryAllowEmpty3.WhereCondition = "";
            this.ucCategoryAllowEmpty3.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(this.ucCategoryAllowEmpty1_EditValueChanged);
            // 
            // ucCategoryAllowEmpty2
            // 
            this.ucCategoryAllowEmpty2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A202;
            this.ucCategoryAllowEmpty2.Code = "";
            this.ucCategoryAllowEmpty2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryAllowEmpty2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryAllowEmpty2.IsValidate = true;
            this.ucCategoryAllowEmpty2.Location = new System.Drawing.Point(394, 374);
            this.ucCategoryAllowEmpty2.Name = "ucCategoryAllowEmpty2";
            this.ucCategoryAllowEmpty2.Name_VN = "";
            this.ucCategoryAllowEmpty2.SetValidate = false;
            this.ucCategoryAllowEmpty2.ShowColumnCode = true;
            this.ucCategoryAllowEmpty2.ShowColumnName = true;
            this.ucCategoryAllowEmpty2.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryAllowEmpty2.TabIndex = 8;
            this.ucCategoryAllowEmpty2.TagCode = "";
            this.ucCategoryAllowEmpty2.TagName = "";
            this.ucCategoryAllowEmpty2.WhereCondition = "";
            this.ucCategoryAllowEmpty2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(this.ucCategoryAllowEmpty1_EditValueChanged);
            // 
            // ucCategoryAllowEmpty1
            // 
            this.ucCategoryAllowEmpty1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ucCategoryAllowEmpty1.Code = "";
            this.ucCategoryAllowEmpty1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryAllowEmpty1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryAllowEmpty1.IsValidate = true;
            this.ucCategoryAllowEmpty1.Location = new System.Drawing.Point(118, 374);
            this.ucCategoryAllowEmpty1.Name = "ucCategoryAllowEmpty1";
            this.ucCategoryAllowEmpty1.Name_VN = "";
            this.ucCategoryAllowEmpty1.SetValidate = false;
            this.ucCategoryAllowEmpty1.ShowColumnCode = true;
            this.ucCategoryAllowEmpty1.ShowColumnName = true;
            this.ucCategoryAllowEmpty1.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryAllowEmpty1.TabIndex = 8;
            this.ucCategoryAllowEmpty1.TagCode = "";
            this.ucCategoryAllowEmpty1.TagName = "";
            this.ucCategoryAllowEmpty1.WhereCondition = "";
            this.ucCategoryAllowEmpty1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty.EditValueChangedHandle(this.ucCategoryAllowEmpty1_EditValueChanged);
            // 
            // ucCategoryExtend5
            // 
            this.ucCategoryExtend5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A546;
            this.ucCategoryExtend5.Code = "";
            this.ucCategoryExtend5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend5.IsValidate = true;
            this.ucCategoryExtend5.Location = new System.Drawing.Point(394, 160);
            this.ucCategoryExtend5.Name = "ucCategoryExtend5";
            this.ucCategoryExtend5.Name_VN = "";
            this.ucCategoryExtend5.SetValidate = false;
            this.ucCategoryExtend5.ShowColumnCode = true;
            this.ucCategoryExtend5.ShowColumnName = true;
            this.ucCategoryExtend5.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend5.TabIndex = 5;
            this.ucCategoryExtend5.TagCode = "";
            this.ucCategoryExtend5.TagName = "";
            this.ucCategoryExtend5.WhereCondition = "";
            this.ucCategoryExtend5.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend4
            // 
            this.ucCategoryExtend4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ucCategoryExtend4.Code = "";
            this.ucCategoryExtend4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend4.IsValidate = true;
            this.ucCategoryExtend4.Location = new System.Drawing.Point(394, 128);
            this.ucCategoryExtend4.Name = "ucCategoryExtend4";
            this.ucCategoryExtend4.Name_VN = "";
            this.ucCategoryExtend4.SetValidate = false;
            this.ucCategoryExtend4.ShowColumnCode = true;
            this.ucCategoryExtend4.ShowColumnName = true;
            this.ucCategoryExtend4.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend4.TabIndex = 5;
            this.ucCategoryExtend4.TagCode = "";
            this.ucCategoryExtend4.TagName = "";
            this.ucCategoryExtend4.WhereCondition = "";
            this.ucCategoryExtend4.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend3
            // 
            this.ucCategoryExtend3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E002;
            this.ucCategoryExtend3.Code = "";
            this.ucCategoryExtend3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend3.IsValidate = true;
            this.ucCategoryExtend3.Location = new System.Drawing.Point(394, 96);
            this.ucCategoryExtend3.Name = "ucCategoryExtend3";
            this.ucCategoryExtend3.Name_VN = "";
            this.ucCategoryExtend3.SetValidate = false;
            this.ucCategoryExtend3.ShowColumnCode = true;
            this.ucCategoryExtend3.ShowColumnName = true;
            this.ucCategoryExtend3.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend3.TabIndex = 5;
            this.ucCategoryExtend3.TagCode = "";
            this.ucCategoryExtend3.TagName = "";
            this.ucCategoryExtend3.WhereCondition = "";
            this.ucCategoryExtend3.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend2
            // 
            this.ucCategoryExtend2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E003;
            this.ucCategoryExtend2.Code = "";
            this.ucCategoryExtend2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend2.IsValidate = true;
            this.ucCategoryExtend2.Location = new System.Drawing.Point(394, 55);
            this.ucCategoryExtend2.Name = "ucCategoryExtend2";
            this.ucCategoryExtend2.Name_VN = "";
            this.ucCategoryExtend2.SetValidate = false;
            this.ucCategoryExtend2.ShowColumnCode = true;
            this.ucCategoryExtend2.ShowColumnName = true;
            this.ucCategoryExtend2.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend2.TabIndex = 5;
            this.ucCategoryExtend2.TagCode = "";
            this.ucCategoryExtend2.TagName = "";
            this.ucCategoryExtend2.WhereCondition = "";
            this.ucCategoryExtend2.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend1
            // 
            this.ucCategoryExtend1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E001;
            this.ucCategoryExtend1.Code = "";
            this.ucCategoryExtend1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend1.IsValidate = true;
            this.ucCategoryExtend1.Location = new System.Drawing.Point(118, 55);
            this.ucCategoryExtend1.Name = "ucCategoryExtend1";
            this.ucCategoryExtend1.Name_VN = "";
            this.ucCategoryExtend1.SetValidate = false;
            this.ucCategoryExtend1.ShowColumnCode = true;
            this.ucCategoryExtend1.ShowColumnName = true;
            this.ucCategoryExtend1.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend1.TabIndex = 5;
            this.ucCategoryExtend1.TagCode = "";
            this.ucCategoryExtend1.TagName = "";
            this.ucCategoryExtend1.WhereCondition = "";
            this.ucCategoryExtend1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCuaKhauXuatNhap1
            // 
            this.ucCuaKhauXuatNhap1.Code = "";
            this.ucCuaKhauXuatNhap1.CountryCode = "CN";
            this.ucCuaKhauXuatNhap1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCuaKhauXuatNhap1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCuaKhauXuatNhap1.IsValidate = true;
            this.ucCuaKhauXuatNhap1.Location = new System.Drawing.Point(118, 198);
            this.ucCuaKhauXuatNhap1.Name = "ucCuaKhauXuatNhap1";
            this.ucCuaKhauXuatNhap1.Name_VN = "";
            this.ucCuaKhauXuatNhap1.SetValidate = false;
            this.ucCuaKhauXuatNhap1.ShowColumnCode = true;
            this.ucCuaKhauXuatNhap1.ShowColumnName = true;
            this.ucCuaKhauXuatNhap1.Size = new System.Drawing.Size(250, 26);
            this.ucCuaKhauXuatNhap1.TabIndex = 4;
            this.ucCuaKhauXuatNhap1.TagCode = "";
            this.ucCuaKhauXuatNhap1.TagName = "";
            this.ucCuaKhauXuatNhap1.WhereCondition = "";
            this.ucCuaKhauXuatNhap1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap.EditValueChangedHandle(this.ucCuaKhauXuatNhap1_EditValueChanged);
            // 
            // ucMaLoaiHinhIDA1
            // 
            this.ucMaLoaiHinhIDA1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A601;
            this.ucMaLoaiHinhIDA1.Code = "";
            this.ucMaLoaiHinhIDA1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaLoaiHinhIDA1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaLoaiHinhIDA1.IsValidate = true;
            this.ucMaLoaiHinhIDA1.Location = new System.Drawing.Point(118, 23);
            this.ucMaLoaiHinhIDA1.Name = "ucMaLoaiHinhIDA1";
            this.ucMaLoaiHinhIDA1.Name_VN = "";
            this.ucMaLoaiHinhIDA1.SetValidate = false;
            this.ucMaLoaiHinhIDA1.ShowColumnCode = true;
            this.ucMaLoaiHinhIDA1.ShowColumnName = false;
            this.ucMaLoaiHinhIDA1.Size = new System.Drawing.Size(202, 26);
            this.ucMaLoaiHinhIDA1.TabIndex = 0;
            this.ucMaLoaiHinhIDA1.TagName = "";
            this.ucMaLoaiHinhIDA1.WhereCondition = "";
            this.ucMaLoaiHinhIDA1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(this.ucMaLoaiHinhIDA1_EditValueChanged);
            // 
            // ucCategoryExtend14
            // 
            this.ucCategoryExtend14.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A014;
            this.ucCategoryExtend14.Code = "";
            this.ucCategoryExtend14.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend14.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend14.IsValidate = true;
            this.ucCategoryExtend14.Location = new System.Drawing.Point(394, 198);
            this.ucCategoryExtend14.Name = "ucCategoryExtend14";
            this.ucCategoryExtend14.Name_VN = "";
            this.ucCategoryExtend14.SetValidate = false;
            this.ucCategoryExtend14.ShowColumnCode = true;
            this.ucCategoryExtend14.ShowColumnName = true;
            this.ucCategoryExtend14.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend14.TabIndex = 5;
            this.ucCategoryExtend14.TagCode = "";
            this.ucCategoryExtend14.TagName = "";
            this.ucCategoryExtend14.WhereCondition = "";
            this.ucCategoryExtend14.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend17
            // 
            this.ucCategoryExtend17.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucCategoryExtend17.Code = "";
            this.ucCategoryExtend17.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend17.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend17.IsValidate = true;
            this.ucCategoryExtend17.Location = new System.Drawing.Point(394, 239);
            this.ucCategoryExtend17.Name = "ucCategoryExtend17";
            this.ucCategoryExtend17.Name_VN = "";
            this.ucCategoryExtend17.SetValidate = false;
            this.ucCategoryExtend17.ShowColumnCode = true;
            this.ucCategoryExtend17.ShowColumnName = true;
            this.ucCategoryExtend17.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend17.TabIndex = 5;
            this.ucCategoryExtend17.TagCode = "";
            this.ucCategoryExtend17.TagName = "";
            this.ucCategoryExtend17.WhereCondition = "";
            this.ucCategoryExtend17.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend20
            // 
            this.ucCategoryExtend20.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ucCategoryExtend20.Code = "";
            this.ucCategoryExtend20.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend20.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend20.IsValidate = true;
            this.ucCategoryExtend20.Location = new System.Drawing.Point(394, 271);
            this.ucCategoryExtend20.Name = "ucCategoryExtend20";
            this.ucCategoryExtend20.Name_VN = "";
            this.ucCategoryExtend20.SetValidate = false;
            this.ucCategoryExtend20.ShowColumnCode = true;
            this.ucCategoryExtend20.ShowColumnName = true;
            this.ucCategoryExtend20.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend20.TabIndex = 5;
            this.ucCategoryExtend20.TagCode = "";
            this.ucCategoryExtend20.TagName = "";
            this.ucCategoryExtend20.WhereCondition = "";
            this.ucCategoryExtend20.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ucCategoryExtend23
            // 
            this.ucCategoryExtend23.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ucCategoryExtend23.Code = "";
            this.ucCategoryExtend23.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategoryExtend23.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategoryExtend23.IsValidate = true;
            this.ucCategoryExtend23.Location = new System.Drawing.Point(394, 303);
            this.ucCategoryExtend23.Name = "ucCategoryExtend23";
            this.ucCategoryExtend23.Name_VN = "";
            this.ucCategoryExtend23.SetValidate = false;
            this.ucCategoryExtend23.ShowColumnCode = true;
            this.ucCategoryExtend23.ShowColumnName = true;
            this.ucCategoryExtend23.Size = new System.Drawing.Size(250, 26);
            this.ucCategoryExtend23.TabIndex = 5;
            this.ucCategoryExtend23.TagCode = "";
            this.ucCategoryExtend23.TagName = "";
            this.ucCategoryExtend23.WhereCondition = "";
            this.ucCategoryExtend23.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EditValueChangedHandle(this.ucCategoryExtend1_EditValueChanged);
            // 
            // ctrDiaDiemXepHang
            // 
            this.ctrDiaDiemXepHang.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A601;
            this.ctrDiaDiemXepHang.Code = "";
            this.ctrDiaDiemXepHang.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrDiaDiemXepHang.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrDiaDiemXepHang.IsValidate = true;
            this.ctrDiaDiemXepHang.Location = new System.Drawing.Point(691, 55);
            this.ctrDiaDiemXepHang.Name = "ctrDiaDiemXepHang";
            this.ctrDiaDiemXepHang.Name_VN = "";
            this.ctrDiaDiemXepHang.SetValidate = false;
            this.ctrDiaDiemXepHang.ShowColumnCode = true;
            this.ctrDiaDiemXepHang.ShowColumnName = true;
            this.ctrDiaDiemXepHang.Size = new System.Drawing.Size(202, 26);
            this.ctrDiaDiemXepHang.TabIndex = 0;
            this.ctrDiaDiemXepHang.TagName = "";
            this.ctrDiaDiemXepHang.WhereCondition = "";
            // 
            // ucCategory1
            // 
            this.ucCategory1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ucCategory1.Code = "";
            this.ucCategory1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory1.IsValidate = true;
            this.ucCategory1.Location = new System.Drawing.Point(691, 96);
            this.ucCategory1.Name = "ucCategory1";
            this.ucCategory1.Name_VN = "";
            this.ucCategory1.SetValidate = false;
            this.ucCategory1.ShowColumnCode = true;
            this.ucCategory1.ShowColumnName = true;
            this.ucCategory1.Size = new System.Drawing.Size(202, 26);
            this.ucCategory1.TabIndex = 0;
            this.ucCategory1.TagName = "";
            this.ucCategory1.WhereCondition = "";
            // 
            // ucCategory2
            // 
            this.ucCategory2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ucCategory2.Code = "";
            this.ucCategory2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory2.IsValidate = true;
            this.ucCategory2.Location = new System.Drawing.Point(691, 128);
            this.ucCategory2.Name = "ucCategory2";
            this.ucCategory2.Name_VN = "";
            this.ucCategory2.SetValidate = false;
            this.ucCategory2.ShowColumnCode = true;
            this.ucCategory2.ShowColumnName = true;
            this.ucCategory2.Size = new System.Drawing.Size(202, 26);
            this.ucCategory2.TabIndex = 0;
            this.ucCategory2.TagName = "";
            this.ucCategory2.WhereCondition = "";
            // 
            // ucCategory3
            // 
            this.ucCategory3.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ucCategory3.Code = "";
            this.ucCategory3.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory3.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory3.IsValidate = true;
            this.ucCategory3.Location = new System.Drawing.Point(691, 160);
            this.ucCategory3.Name = "ucCategory3";
            this.ucCategory3.Name_VN = "";
            this.ucCategory3.SetValidate = false;
            this.ucCategory3.ShowColumnCode = true;
            this.ucCategory3.ShowColumnName = true;
            this.ucCategory3.Size = new System.Drawing.Size(202, 26);
            this.ucCategory3.TabIndex = 0;
            this.ucCategory3.TagName = "";
            this.ucCategory3.WhereCondition = "";
            // 
            // ucCategory4
            // 
            this.ucCategory4.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ucCategory4.Code = "";
            this.ucCategory4.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory4.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory4.IsValidate = true;
            this.ucCategory4.Location = new System.Drawing.Point(691, 198);
            this.ucCategory4.Name = "ucCategory4";
            this.ucCategory4.Name_VN = "";
            this.ucCategory4.SetValidate = false;
            this.ucCategory4.ShowColumnCode = true;
            this.ucCategory4.ShowColumnName = true;
            this.ucCategory4.Size = new System.Drawing.Size(202, 26);
            this.ucCategory4.TabIndex = 0;
            this.ucCategory4.TagName = "";
            this.ucCategory4.WhereCondition = "";
            // 
            // ucCategory5
            // 
            this.ucCategory5.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A506;
            this.ucCategory5.Code = "";
            this.ucCategory5.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory5.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory5.IsValidate = true;
            this.ucCategory5.Location = new System.Drawing.Point(691, 239);
            this.ucCategory5.Name = "ucCategory5";
            this.ucCategory5.Name_VN = "";
            this.ucCategory5.SetValidate = false;
            this.ucCategory5.ShowColumnCode = true;
            this.ucCategory5.ShowColumnName = true;
            this.ucCategory5.Size = new System.Drawing.Size(202, 26);
            this.ucCategory5.TabIndex = 0;
            this.ucCategory5.TagName = "";
            this.ucCategory5.WhereCondition = "";
            // 
            // FormTestControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1284, 571);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ucCategoryAllowEmpty5);
            this.Controls.Add(this.ucCategoryAllowEmpty4);
            this.Controls.Add(this.ucCategoryAllowEmpty3);
            this.Controls.Add(this.ucCategoryAllowEmpty2);
            this.Controls.Add(this.ucCategoryAllowEmpty1);
            this.Controls.Add(this.lookUpEdit1);
            this.Controls.Add(this.cboName);
            this.Controls.Add(this.ucCategoryExtend23);
            this.Controls.Add(this.ucCategoryExtend5);
            this.Controls.Add(this.ucCategoryExtend20);
            this.Controls.Add(this.ucCategoryExtend4);
            this.Controls.Add(this.ucCategoryExtend17);
            this.Controls.Add(this.ucCategoryExtend3);
            this.Controls.Add(this.ucCategoryExtend14);
            this.Controls.Add(this.ucCategoryExtend2);
            this.Controls.Add(this.ucCategoryExtend1);
            this.Controls.Add(this.ucCuaKhauXuatNhap1);
            this.Controls.Add(this.txtMa);
            this.Controls.Add(this.textEdit2);
            this.Controls.Add(this.txtNameInput);
            this.Controls.Add(this.txtNameNull);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtREFNull);
            this.Controls.Add(this.txtRefDB);
            this.Controls.Add(this.txtCodeInput);
            this.Controls.Add(this.txtCodeNull);
            this.Controls.Add(this.textEdit1);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.labelControl9);
            this.Controls.Add(this.labelControl6);
            this.Controls.Add(this.labelControl11);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.labelControl8);
            this.Controls.Add(this.labelControl10);
            this.Controls.Add(this.labelControl5);
            this.Controls.Add(this.labelControl7);
            this.Controls.Add(this.labelControl3);
            this.Controls.Add(this.labelControl2);
            this.Controls.Add(this.labelControl1);
            this.Controls.Add(this.ucCategory5);
            this.Controls.Add(this.ucCategory4);
            this.Controls.Add(this.ucCategory3);
            this.Controls.Add(this.ucCategory2);
            this.Controls.Add(this.ucCategory1);
            this.Controls.Add(this.ctrDiaDiemXepHang);
            this.Controls.Add(this.ucMaLoaiHinhIDA1);
            this.Name = "FormTestControl";
            this.Text = "FormTestControl";
            this.Load += new System.EventHandler(this.FormTestControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtCode.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRefDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cboName.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMa.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeNull.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtREFNull.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameNull.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeInput.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNameInput.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaLoaiHinhIDA1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.TextEdit txtCode;
        private DevExpress.XtraEditors.TextEdit txtName;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.TextEdit txtRefDB;
        private Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap ucCuaKhauXuatNhap1;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend1;
        private DevExpress.XtraEditors.ComboBoxEdit cboName;
        private DevExpress.XtraEditors.TextEdit txtMa;
        private DevExpress.XtraEditors.LookUpEdit lookUpEdit1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl9;
        private DevExpress.XtraEditors.TextEdit txtCodeNull;
        private DevExpress.XtraEditors.TextEdit txtREFNull;
        private DevExpress.XtraEditors.TextEdit txtNameNull;
        private DevExpress.XtraEditors.LabelControl labelControl10;
        private DevExpress.XtraEditors.LabelControl labelControl11;
        private DevExpress.XtraEditors.TextEdit txtCodeInput;
        private DevExpress.XtraEditors.TextEdit txtNameInput;
        private System.Windows.Forms.Button button1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucCategoryAllowEmpty5;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend14;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend17;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend20;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucCategoryExtend23;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrDiaDiemXepHang;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory2;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory3;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory5;
    }
}