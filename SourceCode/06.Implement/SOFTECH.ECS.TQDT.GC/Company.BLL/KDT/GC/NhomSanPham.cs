using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
    public partial class NhomSanPham
    {
        public double TriGia { set; get; }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction_KTX(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_InsertUpdate_ByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction_V3(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@GiaGiaCong", SqlDbType.Money, this._GiaGiaCong);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TenSanPham", SqlDbType.NVarChar, this._TenSanPham);
            db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, TriGia);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public NhomSanPhamCollection SelectCollectionBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 28/06/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_NhomSanPham_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            NhomSanPhamCollection collection = new NhomSanPhamCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                NhomSanPham entity = new NhomSanPham();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("GiaGiaCong"))) entity.GiaGiaCong = reader.GetDecimal(reader.GetOrdinal("GiaGiaCong"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSanPham"))) entity.TenSanPham = reader.GetString(reader.GetOrdinal("TenSanPham"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }


    }
}
