using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
using System.IO;
using Infragistics.Excel;

namespace Company.Interface.GC
{
    public partial class SelectNPLTaiXuatTKCTForm : BaseForm
    {
        public PhanBoToKhaiXuat pbToKhaiXuat = new PhanBoToKhaiXuat();
        public IList<PhanBoToKhaiXuat> pbToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
        public ToKhaiChuyenTiep TKCT;

        private int SoToKhai;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        public bool isPhanBo = false;
        private decimal doChenhLech = 0;
        public SelectNPLTaiXuatTKCTForm()
        {
            InitializeComponent();
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Value);
                this.MaHaiQuan = TKCT.MaHaiQuanTiepNhan;
                txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
                txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
                txtLuong.Text = this.SoLuong.ToString();
                btnAdd.Enabled = true;
            }
        }
        private void TinhTongNPL()
        {
            decimal TongLuongCungUng = 0;
            foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                if (pbTKN.MaNPL.Trim().ToUpper() == cbhang.SelectedValue.ToString())
                {
                    TongLuongCungUng += (decimal)pbTKN.LuongPhanBo;
                }
            }
            lblTong.Text = "Tổng lượng NPL : " + TongLuongCungUng.ToString() +"  Chênh lệch : "+(TongLuongCungUng-pbToKhaiXuat.SoLuongXuat)+"";
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan, txtMaNPL.Text))
                {
                    //ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác.", false);
                    showMsg("MSG_240207");
                    txtSoToKhai.Text = "";
                    txtLuong.Value = 0;
                    txtMaNPL.Text = "";
                    return;
                }
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                    showMsg("MSG_WRN23");
                    //ShowMessage("Lượng tái xuất phải nhỏ hơn hoặc bằng lượng nguyên phụ liệu tồn của tờ khai.", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                PhanBoToKhaiNhap pbTKNhap = new PhanBoToKhaiNhap();
                pbTKNhap.LuongTonDau = Convert.ToDouble(this.SoLuong);
                pbTKNhap.LuongPhanBo = Convert.ToDouble(txtLuong.Text);
                pbTKNhap.LuongTonCuoi = pbTKNhap.LuongTonDau - pbTKNhap.LuongPhanBo;
                pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                pbTKNhap.MaHaiQuanNhap = TKCT.MaHaiQuanTiepNhan;
                pbTKNhap.MaNPL = txtMaNPL.Text.Trim();
                pbTKNhap.NamDangKyNhap =(short) this.NgayDangKy.Year;
                pbTKNhap.SoToKhaiNhap = this.SoToKhai;
                pbTKNhap.MaLoaiHinhNhap = this.MaLoaiHinh;
                decimal TongLuongCungUng=0;
                foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    if (pbTKN.MaNPL.Trim().ToUpper() == cbhang.SelectedValue.ToString())
                    {
                        TongLuongCungUng += (decimal)pbTKN.LuongPhanBo;
                    }
                }
                doChenhLech = (TongLuongCungUng + (decimal)pbTKNhap.LuongPhanBo) - Convert.ToDecimal(txtLuongTaiXuat.Text);
                if ((TongLuongCungUng + (decimal) pbTKNhap.LuongPhanBo) > Convert.ToDecimal(txtLuongTaiXuat.Text))
                {
                    string st = ShowMessage("Lượng tái xuất được chọn trong các tờ khai nhập của sản phẩm : " + pbToKhaiXuat.MaSP + " chưa bằng lượng tái xuất.Độ chênh lệch là : " + doChenhLech.ToString() , false);
                        return;
                }
                pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                dgLispbTKX.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                TinhTongNPL();
                txtLuong.Value = 0;
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (PhanBoToKhaiNhap nplTT in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                if (nplTT.SoToKhaiNhap == soToKhai && nplTT.MaLoaiHinhNhap == maLoaiHinh && nplTT.NamDangKyNhap == namDangKy && nplTT.MaHaiQuanNhap == maHaiQuan && nplTT.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void SelectPhanBoToKhaiXuatOfMaNPL(string MaNPL)
        {
            foreach (PhanBoToKhaiXuat pbTKX in TKCT.PhanBoToKhaiXuatCollection)
            {
                if (pbTKX.MaSP.Trim().ToUpper() == MaNPL.Trim().ToUpper())
                {
                    pbToKhaiXuat = pbTKX;
                    txtLuongTaiXuat.Text = pbToKhaiXuat.SoLuongXuat.ToString();
                    return;
                }
            }
        }
        private void BK07Form_Load(object sender, EventArgs e)
        {
            
            txtLuongTaiXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgListTon.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            
            if (TKCT.HCTCollection.Count == 0)
            {
                TKCT.LoadHCTCollection();
            }
            cbhang.Items.Clear();
            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
            {
                cbhang.Items.Add(HCT.TenHang + " / " + HCT.MaHang, HCT.MaHang);
            }
            cbhang.SelectedIndex = 0;
            this.Text = "Phân bổ cho tờ khai tái xuất số : "+TKCT.SoToKhai+"/"+TKCT.MaLoaiHinh+"/"+TKCT.NgayDangKy.Year;
        }
        private DataRow GetRowTon(DataTable dt, int soToKhai, int namDangKy, string maLoaiHinh, string maNPL)
        {
            foreach (DataRow dr in dt.Rows)
            {
                if (Convert.ToInt32(dr["SoToKhai"]) == soToKhai && Convert.ToDateTime(dr["NgayDangKy"]).Year == namDangKy && Convert.ToString(dr["MaLoaiHinh"]) == maLoaiHinh && Convert.ToString(dr["MaNPL"]).Trim() == maNPL.Trim())
                    return dr;
            }
            return null;
        }
        private void BindData()
        {
            DataTable NPLNhapTonThucTeCollection = NPLNhapTonThucTe.SelectBy_HopDongAndMaNPLAndTonHonO(TKCT.IDHopDong, cbhang.SelectedValue.ToString(), TKCT.NgayDangKy).Tables[0];
            foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                //DataRow rowTon = NPLNhapTonThucTeCollection.Select("SoToKhai=" + pbTKN.SoToKhaiNhap + " and MaLoaiHinh='" + pbTKN.MaLoaiHinhNhap + "' and year(NgayDangKy)=" + pbTKN.NamDangKyNhap + " and MaHaiQuan='" + pbTKN.MaHaiQuanNhap + "' and MaNPL='" + pbTKN.MaNPL.Trim()+ "'")[0];
                DataRow rowTon = GetRowTon(NPLNhapTonThucTeCollection, pbTKN.SoToKhaiNhap, pbTKN.NamDangKyNhap, pbTKN.MaLoaiHinhNhap, pbTKN.MaNPL);
                rowTon["Ton"] = (Convert.ToDouble(rowTon["Ton"]) - pbTKN.LuongPhanBo);                
            }
            dgListTon.DataSource = NPLNhapTonThucTeCollection;
            dgLispbTKX.DataSource = pbToKhaiXuat.PhanBoToKhaiNhapCollection;
            TinhTongNPL();
        }
        private bool KiemTraLuongTaiXuat(string maNPL, IList<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection,double LuongTaiXuat)
        {
            decimal TongTaixuat = 0;
            foreach (PhanBoToKhaiNhap pbTKN in PhanBoToKhaiNhapCollection)
            {
                TongTaixuat += (decimal)pbTKN.LuongPhanBo;
            }
            doChenhLech = TongTaixuat - (decimal)LuongTaiXuat;
            if (TongTaixuat != (decimal)LuongTaiXuat)
                return false;
            return true; 
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            string malech = "";
            try
            {
                foreach(PhanBoToKhaiXuat pbToKhaiX in TKCT.PhanBoToKhaiXuatCollection)
                {

                    if (pbToKhaiX.PhanBoToKhaiNhapCollection == null || pbToKhaiX.PhanBoToKhaiNhapCollection.Count == 0)
                    {
                        showMsg("MSG_WRN25", pbToKhaiX.MaSP);
                        //ShowMessage("Bạn chưa chọn phân bổ tờ khai nhập cho mặt hàng " + pbToKhaiX.MaSP + ".", false);
                        return;
                    }
                    if(!KiemTraLuongTaiXuat(pbToKhaiX.MaSP,pbToKhaiX.PhanBoToKhaiNhapCollection,Convert.ToDouble(pbToKhaiX.SoLuongXuat)))
                    {
                        //showMsg("MSG_WRN26");
                        malech += pbToKhaiX.MaSP + ";";
                        string st=ShowMessage("Lượng tái xuất được chọn trong các tờ khai nhập của sản phẩm : "+pbToKhaiX.MaSP+" chưa bằng lượng tái xuất.Độ chênh lệch là : "+doChenhLech.ToString(),false);
                        continue;
                    }
                }
                if (malech != "")
                {
                    string st = ShowMessage("Danh sách NPL có lượng phân bổ chưa bằng lượng xuất : " + malech, false);
                    return;
                }
                this.Cursor = Cursors.WaitCursor;
                PhanBoToKhaiXuat.PhanBoChoToKhaiTaiXuat(TKCT, GlobalSettings.SoThapPhan.LuongNPL);
                isPhanBo = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //showMsg("MSG_2702004", ex.Message);
                ShowMessage("Lỗi: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
                NPL.Ma = e.Row.Cells["MaNPL"].Text.Trim();
                NPL.HopDong_ID = TKCT.IDHopDong;
                NPL.Load();
                e.Row.Cells["TenHang"].Text = NPL.Ten;
            }
        }

        private void dgList2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["SoToKhaiNhap"].Text = e.Row.Cells["SoToKhaiNhap"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhNhap"].Value) + "/" + e.Row.Cells["NamDangKyNhap"].Text;
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //txtSoToKhai.Text = e.Row.Cells["SoToKhaiNhap"].Text;
            //this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhaiNhap"].Value);
            //this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinhNhap"].Value);
            //int namdkNhap = Convert.ToInt32(e.Row.Cells["NamDangKyNhap"].Value);            
            //txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
            //txtLuong.Value = Convert.ToDecimal(e.Row.Cells["LuongPhanBo"].Value);
            //this.SoLuong = Convert.ToDecimal(e.Row.Cells["LuongPhanBo"].Text);
            //NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load((int)SoToKhai,MaLoaiHinh,(short)namdkNhap, Glo1balSettings.MA_HAI_QUAN, txtMaNPL.Text);
            //this.NgayDangKy = nplTon.NgayDangKy;
        }

      
        private void cbhang_SelectedIndexChanged(object sender, EventArgs e)
        {            
            SelectPhanBoToKhaiXuatOfMaNPL(cbhang.SelectedValue.ToString());
            if (this.pbToKhaiXuat.PhanBoToKhaiNhapCollection == null)
                this.pbToKhaiXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
            BindData();

        }

      
        private void uiButton2_Click(object sender, EventArgs e)
        {
            if (TKCT.PhanBoToKhaiXuatCollection.Count > 0)
            {
                string st = ShowMessage("Nếu nhập từ excel sẽ xóa hết các thông tin đã nhập trước đó. Bạn có muốn tiếp tục không ?", true);
                if (st != "Yes")
                    return;
            }
            ReadExcelFormNPLPhanBo f = new ReadExcelFormNPLPhanBo();
            f.ShowDialog();

            if (f.table.Rows.Count == 0)
                return;

            TKCT.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
            foreach (HangChuyenTiep hct in TKCT.HCTCollection)
            {
                string manpl = hct.MaHang;
                pbToKhaiXuat = new PhanBoToKhaiXuat();
                pbToKhaiXuat.ID_TKMD = TKCT.ID;
                pbToKhaiXuat.MaDoanhNghiep = TKCT.MaLoaiHinh;
                pbToKhaiXuat.MaSP = manpl;
                pbToKhaiXuat.SoLuongXuat = hct.SoLuong;
                pbToKhaiXuat.PhanBoToKhaiNhapCollection = new List<PhanBoToKhaiNhap>();
                TKCT.PhanBoToKhaiXuatCollection.Add(pbToKhaiXuat);
                DataRow[] rows = f.table.Select("MaNPL='"+manpl+"'");
                if (rows.Length == 0)
                {
                    string st=ShowMessage("Không có dữ liệu phân bổ cho nguyên phụ liệu : "+manpl+" Bạn có muốn tiếp tục không ?",true);
                    if (st == "Yes")
                        continue;
                    else
                        return;
                }
                foreach (DataRow row in rows)
                {
                    NPLNhapTonThucTe nplTon = NPLNhapTonThucTe.Load(Convert.ToInt32(row["SOTK"]), row["MALH"].ToString().Trim(), Convert.ToInt16(row["NamDK"]), TKCT.MaHaiQuanTiepNhan, manpl);
                    if (nplTon == null)
                    {
                        string st = ShowMessage("Không có dữ liệu tồn của nguyên phụ liệu : " + manpl + " trên tờ khai : " + row["SOTK"] + "/" + row["MALH"] + "/" + row["NamDK"] + " Bạn có muốn tiếp tục không ?", true);
                        if (st == "Yes")
                            continue;
                        else
                            return;
                    }
                    PhanBoToKhaiNhap pbTKNhap = new PhanBoToKhaiNhap();
                    pbTKNhap.LuongTonDau =Math.Round(Convert.ToDouble(nplTon.Ton),GlobalSettings.SoThapPhan.LuongNPL);
                    pbTKNhap.LuongPhanBo = Math.Round(Convert.ToDouble(row["Luong"]), GlobalSettings.SoThapPhan.LuongNPL);
                    if (pbTKNhap.LuongPhanBo > pbTKNhap.LuongTonDau)
                    {
                        string st = ShowMessage("Lượng phân bổ của  nguyên phụ liệu : " + manpl + " trên tờ khai : " + row["SOTK"] + "/" + row["MALH"] + "/" + row["NamDK"] + "là : "+pbTKNhap.LuongPhanBo+" lớn hơn lượng tồn đầu của tờ khai là : "+nplTon.Ton+" Bạn có muốn tiếp tục không ?", true);
                        if (st != "Yes")                        
                            return;
                    }
                    pbTKNhap.LuongTonCuoi = pbTKNhap.LuongTonDau - pbTKNhap.LuongPhanBo;
                    pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    pbTKNhap.MaHaiQuanNhap = TKCT.MaHaiQuanTiepNhan;
                    pbTKNhap.MaNPL = manpl;
                    pbTKNhap.NamDangKyNhap = Convert.ToInt16(row["NamDK"]);
                    pbTKNhap.SoToKhaiNhap = nplTon.SoToKhai;
                    pbTKNhap.MaLoaiHinhNhap = nplTon.MaLoaiHinh;
                    pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                }
                
            }            
            cbhang_SelectedIndexChanged(null, null);
        }

    }
}