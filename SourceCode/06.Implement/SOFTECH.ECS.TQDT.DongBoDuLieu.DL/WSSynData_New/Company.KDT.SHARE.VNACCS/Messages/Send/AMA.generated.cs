using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class AMA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SYN { get; set; }
public PropertiesAttribute CHS { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute YNK { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute ICB { get; set; }
public PropertiesAttribute IDD { get; set; }
public PropertiesAttribute IPD { get; set; }
public PropertiesAttribute RED { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IMY { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute IMT { get; set; }
public PropertiesAttribute REC { get; set; }
public PropertiesAttribute TTC { get; set; }
public PropertiesAttribute BRC { get; set; }
public PropertiesAttribute BYA { get; set; }
public PropertiesAttribute BCM { get; set; }
public PropertiesAttribute BCN { get; set; }
public PropertiesAttribute ENC { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute RYA { get; set; }
public PropertiesAttribute SCM { get; set; }
public PropertiesAttribute SCN { get; set; }
public PropertiesAttribute CDB { get; set; }
public PropertiesAttribute RTB { get; set; }
public PropertiesAttribute CDA { get; set; }
public PropertiesAttribute RTA { get; set; }
public PropertiesAttribute REF { get; set; }
public PropertiesAttribute NTB { get; set; }
public PropertiesAttribute NTA { get; set; }

public AMA()
        {
SYN = new PropertiesAttribute(12, typeof(int));
CHS = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
YNK = new PropertiesAttribute(1, typeof(string));
ICN = new PropertiesAttribute(12, typeof(int));
ICB = new PropertiesAttribute(3, typeof(string));
IDD = new PropertiesAttribute(8, typeof(DateTime));
IPD = new PropertiesAttribute(8, typeof(DateTime));
RED = new PropertiesAttribute(8, typeof(DateTime));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IMY = new PropertiesAttribute(7, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
IMT = new PropertiesAttribute(20, typeof(string));
REC = new PropertiesAttribute(1, typeof(string));
TTC = new PropertiesAttribute(3, typeof(string));
BRC = new PropertiesAttribute(11, typeof(string));
BYA = new PropertiesAttribute(4, typeof(int));
BCM = new PropertiesAttribute(10, typeof(string));
BCN = new PropertiesAttribute(10, typeof(string));
ENC = new PropertiesAttribute(1, typeof(string));
SBC = new PropertiesAttribute(11, typeof(string));
RYA = new PropertiesAttribute(4, typeof(int));
SCM = new PropertiesAttribute(10, typeof(string));
SCN = new PropertiesAttribute(10, typeof(string));
CDB = new PropertiesAttribute(3, typeof(string));
RTB = new PropertiesAttribute(9, typeof(int));
CDA = new PropertiesAttribute(3, typeof(string));
RTA = new PropertiesAttribute(9, typeof(int));
REF = new PropertiesAttribute(20, typeof(string));
NTB = new PropertiesAttribute(300, typeof(string));
NTA = new PropertiesAttribute(300, typeof(string));
TongSoByte = 1483;
}

}public partial class EnumGroupID
    {
}

}
