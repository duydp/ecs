﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.Windows.Forms;
using Customs.Component.Utility;
using Janus.Windows.GridEX;
using Microsoft.ApplicationBlocks.Data;
using System.Configuration;
using System.Text;

namespace HaiQuan.HS
{
    public class FavouriteForm : CommonForm
	{
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
		private Janus.Windows.GridEX.EditControls.EditBox txtDescription02;
		private Janus.Windows.GridEX.GridEX gridEX1;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private IContainer components = null;
		private System.Windows.Forms.MenuItem mnuClearItemFavourite;
		private System.Windows.Forms.MenuItem mnuClearAll;
		private System.Windows.Forms.MenuItem mnuClearGroupFavourite;

		public FavouriteForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Janus.Windows.GridEX.GridEXLayout gridEXLayout1 = new Janus.Windows.GridEX.GridEXLayout();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FavouriteForm));
			this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
			this.gridEX1 = new Janus.Windows.GridEX.GridEX();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.mnuClearGroupFavourite = new System.Windows.Forms.MenuItem();
			this.mnuClearItemFavourite = new System.Windows.Forms.MenuItem();
			this.mnuClearAll = new System.Windows.Forms.MenuItem();
			this.txtDescription02 = new Janus.Windows.GridEX.EditControls.EditBox();
			((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
			this.uiGroupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
			this.SuspendLayout();
			// 
			// uiGroupBox1
			// 
			this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
			this.uiGroupBox1.Controls.Add(this.gridEX1);
			this.uiGroupBox1.Controls.Add(this.txtDescription02);
			this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
			this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
			this.uiGroupBox1.Name = "uiGroupBox1";
			this.uiGroupBox1.Size = new System.Drawing.Size(536, 318);
			this.uiGroupBox1.TabIndex = 4;
			this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
			// 
			// gridEX1
			// 
			this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
			this.gridEX1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
				| System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.gridEX1.ColumnAutoResize = true;
			this.gridEX1.ContextMenu = this.contextMenu1;
			gridEXLayout1.LayoutString = "<GridEXLayoutData><RootTable><Columns Collection=\"true\"><Column0 ID=\"ID\"><Caption" +
				">ID</Caption><DataMember>ID</DataMember><Key>ID</Key><Position>0</Position><Visi" +
				"ble>False</Visible><Width>51</Width></Column0><Column1 ID=\"ID01\"><DataMember>ID0" +
				"1</DataMember><Key>ID01</Key><Position>1</Position><Width>56</Width></Column1><C" +
				"olumn2 ID=\"ID02\"><DataMember>ID02</DataMember><Key>ID02</Key><Position>2</Positi" +
				"on><Width>28</Width></Column2><Column3 ID=\"ID03\"><DataMember>ID03</DataMember><K" +
				"ey>ID03</Key><Position>3</Position><Width>25</Width></Column3><Column4 ID=\"ID04\"" +
				"><DataMember>ID04</DataMember><Key>ID04</Key><Position>4</Position><Width>25</Wi" +
				"dth></Column4><Column5 ID=\"Description\"><Caption>Mô tả hàng hóa</Caption><DataMe" +
				"mber>Description</DataMember><Key>Description</Key><Position>5</Position><Width>" +
				"308</Width></Column5><Column6 ID=\"Percent\"><Caption>TS (%)</Caption><DataMember>" +
				"Percent</DataMember><Key>Percent</Key><Position>6</Position><TextAlignment>Cente" +
				"r</TextAlignment><Width>58</Width></Column6></Columns><FormatConditions Collecti" +
				"on=\"true\"><Condition0 ID=\"FormatCondition1\"><ColIndex>1</ColIndex><ConditionOper" +
				"ator>IsEmpty</ConditionOperator><FormatStyleIndex>0</FormatStyleIndex><PreviewRo" +
				"wFormatStyleIndex>0</PreviewRowFormatStyleIndex><TargetColIndex>1</TargetColInde" +
				"x><Value1 /><Key>FormatCondition1</Key><FilterCondition><ColumnIndex>4</ColumnIn" +
				"dex><ConditionOperator>IsEmpty</ConditionOperator><Conditions Collection=\"true\">" +
				"<FilterCondition0><ColumnIndex>3</ColumnIndex><ConditionOperator>IsEmpty</Condit" +
				"ionOperator><LogicalOperator>And</LogicalOperator></FilterCondition0><FilterCond" +
				"ition1><ColumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator" +
				"><LogicalOperator>And</LogicalOperator></FilterCondition1><FilterCondition2><Col" +
				"umnIndex>1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOperator><Logica" +
				"lOperator>And</LogicalOperator></FilterCondition2></Conditions></FilterCondition" +
				"></Condition0><Condition1 ID=\"FormatCondition2\"><ColIndex>2</ColIndex><FormatSty" +
				"leIndex>0</FormatStyleIndex><PreviewRowFormatStyleIndex>0</PreviewRowFormatStyle" +
				"Index><TargetColIndex>2</TargetColIndex><Key>FormatCondition2</Key><FilterCondit" +
				"ion><ColumnIndex>1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOperator" +
				"><Conditions Collection=\"true\"><FilterCondition0><ColumnIndex>2</ColumnIndex><Co" +
				"nditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator" +
				"></FilterCondition0><FilterCondition1><ColumnIndex>3</ColumnIndex><ConditionOper" +
				"ator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCo" +
				"ndition1><FilterCondition2><ColumnIndex>4</ColumnIndex><ConditionOperator>IsEmpt" +
				"y</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition2></" +
				"Conditions></FilterCondition></Condition1><Condition2 ID=\"FormatCondition3\"><Col" +
				"Index>3</ColIndex><FormatStyleIndex>0</FormatStyleIndex><PreviewRowFormatStyleIn" +
				"dex>0</PreviewRowFormatStyleIndex><TargetColIndex>3</TargetColIndex><Key>FormatC" +
				"ondition3</Key><FilterCondition><ColumnIndex>1</ColumnIndex><ConditionOperator>N" +
				"otIsEmpty</ConditionOperator><Conditions Collection=\"true\"><FilterCondition0><Co" +
				"lumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalO" +
				"perator>And</LogicalOperator></FilterCondition0><FilterCondition1><ColumnIndex>3" +
				"</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And" +
				"</LogicalOperator></FilterCondition1><FilterCondition2><ColumnIndex>4</ColumnInd" +
				"ex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOp" +
				"erator></FilterCondition2></Conditions></FilterCondition></Condition2><Condition" +
				"3 ID=\"FormatCondition4\"><ColIndex>4</ColIndex><FormatStyleIndex>0</FormatStyleIn" +
				"dex><PreviewRowFormatStyleIndex>0</PreviewRowFormatStyleIndex><TargetColIndex>4<" +
				"/TargetColIndex><Key>FormatCondition4</Key><FilterCondition><ColumnIndex>1</Colu" +
				"mnIndex><ConditionOperator>NotIsEmpty</ConditionOperator><Conditions Collection=" +
				"\"true\"><FilterCondition0><ColumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty<" +
				"/ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition0><Fil" +
				"terCondition1><ColumnIndex>3</ColumnIndex><ConditionOperator>IsEmpty</ConditionO" +
				"perator><LogicalOperator>And</LogicalOperator></FilterCondition1><FilterConditio" +
				"n2><ColumnIndex>4</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><Lo" +
				"gicalOperator>And</LogicalOperator></FilterCondition2></Conditions></FilterCondi" +
				"tion></Condition3><Condition4 ID=\"FormatCondition5\"><ColIndex>5</ColIndex><Forma" +
				"tStyleIndex>0</FormatStyleIndex><PreviewRowFormatStyleIndex>0</PreviewRowFormatS" +
				"tyleIndex><TargetColIndex>5</TargetColIndex><Key>FormatCondition5</Key><FilterCo" +
				"ndition><ColumnIndex>1</ColumnIndex><ConditionOperator>NotIsEmpty</ConditionOper" +
				"ator><Conditions Collection=\"true\"><FilterCondition0><ColumnIndex>2</ColumnIndex" +
				"><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOper" +
				"ator></FilterCondition0><FilterCondition1><ColumnIndex>3</ColumnIndex><Condition" +
				"Operator>IsEmpty</ConditionOperator><LogicalOperator>And</LogicalOperator></Filt" +
				"erCondition1><FilterCondition2><ColumnIndex>4</ColumnIndex><ConditionOperator>Is" +
				"Empty</ConditionOperator><LogicalOperator>And</LogicalOperator></FilterCondition" +
				"2></Conditions></FilterCondition></Condition4><Condition5 ID=\"FormatCondition6\">" +
				"<ColIndex>6</ColIndex><FormatStyleIndex>0</FormatStyleIndex><PreviewRowFormatSty" +
				"leIndex>0</PreviewRowFormatStyleIndex><TargetColIndex>6</TargetColIndex><Key>For" +
				"matCondition6</Key><FilterCondition><ColumnIndex>1</ColumnIndex><ConditionOperat" +
				"or>NotIsEmpty</ConditionOperator><Conditions Collection=\"true\"><FilterCondition0" +
				"><ColumnIndex>2</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><Logi" +
				"calOperator>And</LogicalOperator></FilterCondition0><FilterCondition1><ColumnInd" +
				"ex>3</ColumnIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator" +
				">And</LogicalOperator></FilterCondition1><FilterCondition2><ColumnIndex>4</Colum" +
				"nIndex><ConditionOperator>IsEmpty</ConditionOperator><LogicalOperator>And</Logic" +
				"alOperator></FilterCondition2></Conditions></FilterCondition></Condition5></Form" +
				"atConditions><GroupCondition ID=\"\" /></RootTable><FormatStyles Collection=\"true\"" +
				"><Style0 ID=\"FormatStyle1\"><BackColor>255, 255, 192</BackColor><FontBold>True</F" +
				"ontBold><Key>FormatStyle1</Key></Style0></FormatStyles></GridEXLayoutData>";
			this.gridEX1.DesignTimeLayout = gridEXLayout1;
			this.gridEX1.GroupByBoxVisible = false;
			this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
			this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
			this.gridEX1.Location = new System.Drawing.Point(8, 8);
			this.gridEX1.Name = "gridEX1";
			this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
			this.gridEX1.Size = new System.Drawing.Size(520, 240);
			this.gridEX1.TabIndex = 12;
			this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			this.gridEX1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridEX1_RowDoubleClick);
			this.gridEX1.SelectionChanged += new System.EventHandler(this.gridEX1_SelectionChanged);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.mnuClearGroupFavourite,
																						 this.mnuClearItemFavourite,
																						 this.mnuClearAll});
			// 
			// mnuClearGroupFavourite
			// 
			this.mnuClearGroupFavourite.Index = 0;
			this.mnuClearGroupFavourite.Text = "<< Xóa khỏi danh sách thường dùng (Cả nhóm)";
			this.mnuClearGroupFavourite.Click += new System.EventHandler(this.mnuClearGroupFavourite_Click);
			// 
			// mnuClearItemFavourite
			// 
			this.mnuClearItemFavourite.Index = 1;
			this.mnuClearItemFavourite.Text = "<< Xóa khỏi danh sách thường dùng (Dòng này)";
			this.mnuClearItemFavourite.Click += new System.EventHandler(this.mnuClearItemFavourite_Click);
			// 
			// mnuClearAll
			// 
			this.mnuClearAll.Index = 2;
			this.mnuClearAll.Text = "<< Xóa tất cả danh sách thường dùng";
			this.mnuClearAll.Click += new System.EventHandler(this.mnuClearAll_Click);
			// 
			// txtDescription02
			// 
			this.txtDescription02.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
				| System.Windows.Forms.AnchorStyles.Right)));
			this.txtDescription02.AutoScrollMargin = new System.Drawing.Size(0, 0);
			this.txtDescription02.AutoScrollMinSize = new System.Drawing.Size(0, 0);
			this.txtDescription02.Location = new System.Drawing.Point(8, 256);
			this.txtDescription02.Multiline = true;
			this.txtDescription02.Name = "txtDescription02";
			this.txtDescription02.ReadOnly = true;
			this.txtDescription02.Size = new System.Drawing.Size(520, 56);
			this.txtDescription02.TabIndex = 11;
			this.txtDescription02.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
			this.txtDescription02.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
			// 
			// FavouriteForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
			this.ClientSize = new System.Drawing.Size(536, 318);
			this.Controls.Add(this.uiGroupBox1);
			this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FavouriteForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Tag = "SearchForm";
			this.Text = "Danh sách mã số thường dùng";
			this.Load += new System.EventHandler(this.FavouriteForm_Load);
			this.Activated += new System.EventHandler(this.FavouriteForm_Activated);
			((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
			this.uiGroupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		public void SearchByFavourite()
		{
			Item item = new Item();
			gridEX1.DataSource = item.SearchByFavourite().Tables[0];			
		}

		private void FavouriteForm_Load(object sender, EventArgs e)
		{
			this.SearchByFavourite();
		}

		private void gridEX1_SelectionChanged(object sender, System.EventArgs e)
		{
			try
			{
				txtDescription02.Text = gridEX1.GetRow().Cells["Description"].Text;
			}
			catch
			{
			}
		}

		private void gridEX1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
		{
			string ID = e.Row.Cells["ID"].Text.ToString().Substring(0,4);
			SearchForm f = new SearchForm();
			f.SetSearchObjectVisible(false, "Chương " + ID.Substring(0, 2));
			f.Search(ID, "", "AND", HSSearchWhat.AllWord);			
			f.FormBorderStyle = FormBorderStyle.FixedDialog;
			f.MinimizeBox = false;
			f.MaximizeBox = false;
			f.ShowDialog();
		}

		private void mnuClearGroupFavourite_Click(object sender, System.EventArgs e)
		{
			try
			{
				DialogResult ret = MessageBox.Show("Bạn có muốn xóa nhóm này khỏi danh sách thường dùng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ret == DialogResult.Yes)
				{
					Item item = new Item();
					item.ClearFavourite(gridEX1.GetRow().Cells["ID"].Text, true);
					MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.SearchByFavourite();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		
		}

		private void mnuClearItemFavourite_Click(object sender, System.EventArgs e)
		{
			try
			{
				DialogResult ret = MessageBox.Show("Bạn có muốn xóa dòng này khỏi danh sách thường dùng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ret == DialogResult.Yes)
				{
					Item item = new Item();
					item.ClearFavourite(gridEX1.GetRow().Cells["ID"].Text, false);
					MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.SearchByFavourite();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}		
		}

		private void mnuClearAll_Click(object sender, System.EventArgs e)
		{
			try
			{
				DialogResult ret = MessageBox.Show("Bạn có muốn xóa tất cả khỏi danh sách thường dùng không?", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ret == DialogResult.Yes)
				{
					Item item = new Item();
					item.ClearAllFavourite();
					MessageBox.Show("Thao tác thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.SearchByFavourite();
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Thao tác không thành công thành công! Lý do: " + ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}

		}

		private void FavouriteForm_Activated(object sender, System.EventArgs e)
		{
			this.SearchByFavourite();
		}
	}
}