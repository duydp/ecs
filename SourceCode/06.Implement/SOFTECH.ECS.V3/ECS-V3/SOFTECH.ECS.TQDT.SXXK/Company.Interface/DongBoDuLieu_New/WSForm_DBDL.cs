﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using System.Text;
using System.Security.Cryptography;

namespace Company.Interface.DongBoDuLieu_New
{
    public partial class WSForm_DBDL : BaseForm
    {
        public bool IsReady = false;
        public WSForm_DBDL()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.Text = string.IsNullOrEmpty(Config.User) ? GlobalSettings.MA_DON_VI.Trim() : Config.User;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            error.Clear();
            bool isValid = Globals.ValidateNull(txtMaDoanhNghiep, error, "Mã doanh nghiệp");
            isValid &= Globals.ValidateNull(txtMatKhau, error, "Mật khẩu");
            if (!isValid)
                return;
            IsReady = true;
            Config.IsRemember = ckLuu.Checked;
            Config.User = txtMaDoanhNghiep.Text.Trim();
            txtMatKhau.Text = GetMD5Value(txtMatKhau.Text.Trim());
            if (Config.IsRemember)
                Config.Pass = txtMatKhau.Text;
//             if (GlobalSettings.IsRemember)
//             {
//                 GlobalSettings.PassWordDT = txtMatKhau.Text;
//             }
            this.Close();

        }
        public string GetMD5Value(string data)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            //return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper(); //TQDT Version
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper().ToLower(); //TNTT Version
        }

    }
}