﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class EditInformationHopDongForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public HopDong HD;
        public KDT_GC_PhuKien_TTHopDong TTHD;
        public PhuKienDangKy PKDK;
        public bool isAdd = false;
        public string NoiDung = String.Empty;
        public String Caption;
        public bool IsChange;

        public EditInformationHopDongForm()
        {
            InitializeComponent();
            cbPTTT.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = "GhiChu";
            cbPTTT.ValueMember = "ID";

            cbPTTT.TextChanged -= new EventHandler(txt_TextChanged);
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            cbPTTT.TextChanged += new EventHandler(txt_TextChanged);

            nguyenTeControl1.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
            nuocHControl1.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
            cbPTTT.TextChanged -= new EventHandler(txt_TextChanged);
        }

        private void EditInformationHopDongForm_Load(object sender, EventArgs e)
        {
            try
            {
                Caption = this.Text;
                if (this.OpenType == OpenFormType.View)
                {
                    btnUpdate.Enabled = false;
                }
                else
                {
                    btnUpdate.Enabled = true;
                }
                SetInformationContract();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (PKDK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || PKDK.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || PKDK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetInformationContract()
        {
            try
            {
                if (TTHD==null)
                {

                    nguyenTeControl1.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                    nguyenTeControl1.Ma = HD.NguyenTe_ID;
                    nguyenTeControl1.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);

                    nuocHControl1.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
                    nuocHControl1.Ma = HD.NuocThue_ID;
                    nuocHControl1.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

                    cbPTTT.TextChanged -= new EventHandler(txt_TextChanged);
                    cbPTTT.SelectedValue = HD.PTTT_ID == null ? "" : HD.PTTT_ID;
                    cbPTTT.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenDoiTac.Text = HD.TenDonViDoiTac == null ? HD.DonViDoiTac.Trim() : HD.TenDonViDoiTac.Trim();
                    txtTenDoiTac.TextChanged += new EventHandler(txt_TextChanged);

                    txtDCDT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDCDT.Text = HD.DiaChiDoiTac == null ? String.Empty : HD.DiaChiDoiTac.Trim();
                    txtDCDT.TextChanged += new EventHandler(txt_TextChanged);

                    txtDVDT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDVDT.Text = HD.DonViDoiTac == null ? String.Empty : HD.DonViDoiTac.Trim();
                    txtDVDT.TextChanged += new EventHandler(txt_TextChanged);

                    txtMaBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaBenNhan.Text = HD.MaDoanhNghiep;
                    txtMaBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenBenNhan.Text = HD.TenDoanhNghiep;
                    txtTenBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiBenNhan.Text = HD.DiaChiDoanhNghiep;
                    txtDiaChiBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongGiaTriSP.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongGiaTriSP.Value = HD.TongTriGiaSP;
                    txtTongGiaTriSP.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongTriGiaTienCong.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongTriGiaTienCong.Value = HD.TongTriGiaTienCong;
                    txtTongTriGiaTienCong.TextChanged += new EventHandler(txt_TextChanged);

                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = HD.GhiChu == null ? String.Empty : HD.GhiChu.Trim();
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);
                }
                else
                {
                    nguyenTeControl1.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                    nguyenTeControl1.Ma = TTHD.NguyenTe_ID;
                    nguyenTeControl1.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);

                    nuocHControl1.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
                    nuocHControl1.Ma = TTHD.NuocThue_ID;
                    nuocHControl1.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

                    cbPTTT.TextChanged -= new EventHandler(txt_TextChanged);
                    cbPTTT.SelectedValue = TTHD.PTTT_ID == null ? "" : TTHD.PTTT_ID;
                    cbPTTT.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenDoiTac.Text = TTHD.TenDonViDoiTac == null ? TTHD.DonViDoiTac.Trim() : TTHD.TenDonViDoiTac.Trim();
                    txtTenDoiTac.TextChanged += new EventHandler(txt_TextChanged);

                    txtDCDT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDCDT.Text = TTHD.DiaChiDoiTac == null ? String.Empty : TTHD.DiaChiDoiTac.Trim();
                    txtDCDT.TextChanged += new EventHandler(txt_TextChanged);

                    txtDVDT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDVDT.Text = TTHD.DonViDoiTac == null ? String.Empty : TTHD.DonViDoiTac.Trim();
                    txtDVDT.TextChanged += new EventHandler(txt_TextChanged);

                    txtMaBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaBenNhan.Text = TTHD.MaDoanhNghiep;
                    txtMaBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenBenNhan.Text = TTHD.TenDoanhNghiep;
                    txtTenBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiBenNhan.Text = TTHD.DiaChiDoanhNghiep;
                    txtDiaChiBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongGiaTriSP.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongGiaTriSP.Value = TTHD.TongTriGiaSP;
                    txtTongGiaTriSP.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongTriGiaTienCong.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongTriGiaTienCong.Value = TTHD.TongTriGiaTienCong;
                    txtTongTriGiaTienCong.TextChanged += new EventHandler(txt_TextChanged);

                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = TTHD.GhiChu == null ? String.Empty : TTHD.GhiChu.Trim();
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetInformationContract()
        {
            try
            {
                if (TTHD==null)
                {
                    TTHD = new KDT_GC_PhuKien_TTHopDong();
                }
                //TTHD.LoaiPhuKien_ID = PKDK.ID;
                TTHD.PTTT_ID = cbPTTT.SelectedValue.ToString();
                TTHD.NguyenTe_ID = nguyenTeControl1.Ma;
                TTHD.TongTriGiaSP = Convert.ToDouble(txtTongGiaTriSP.Value);
                TTHD.TongTriGiaTienCong = Convert.ToDouble(txtTongTriGiaTienCong.Value);
                TTHD.NuocThue_ID = nuocHControl1.Ma;

                TTHD.MaDoanhNghiep = txtMaBenNhan.Text.Trim();
                TTHD.DiaChiDoanhNghiep = txtDiaChiBenNhan.Text;
                TTHD.TenDoanhNghiep = txtTenBenNhan.Text;

                TTHD.DonViDoiTac = txtDVDT.Text.Trim();
                TTHD.DiaChiDoiTac = txtDCDT.Text.Trim();
                TTHD.TenDonViDoiTac = txtTenDoiTac.Text;

                TTHD.GhiChu = txtGhiChu.Text;               
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbPTTT, errorProvider, "Mã phương thức thanh toán", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenBenNhan, errorProvider, "Tên người nhận gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaBenNhan, errorProvider, "Mã người nhận gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiBenNhan, errorProvider, "Địa chỉ người nhận gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDoiTac, errorProvider, "Tên người thuê gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDVDT, errorProvider, "Mã người thuê gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDCDT, errorProvider, "Địa chỉ người thuê gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTongGiaTriSP, errorProvider, "Tổng trị giá sản phẩm");
                isValid &= ValidateControl.ValidateZero(txtTongTriGiaTienCong, errorProvider, "Tổng trị giá tiền công");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetInformationContract();
                String Notes = String.Empty;
                Notes = "#&" + HD.SoHopDong + "#&" + HD.NgayKy.ToString("ddMMyyyy") + "#&" + HD.NgayHetHan.ToString("ddMMyyyy") + "#&" + txtTenDoiTac.Text.ToString().Trim() + "#&";
                if (Notes.Length >= 100)
                {
                    errorProvider.SetError(txtTenDoiTac, "Tên đối tác đã nhập vượt quá ký tự cho phép .Đối với HĐGC khi Khai báo Tờ khai , phần Ghi chú trên Tờ khai sẽ bao gồm : #& Số HĐGC #& Ngày HĐGC #& Ngày hết hạn #& Tên đối tác #& Nội dung ghi chú .Nội dung Ghi chú trên tờ khai của doanh nghiệp đang nhập đang vượt quá độ dài cho phép là 100 ký tự.");
                    return ;
                }
                if (isAdd)
                {
                    Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
                    LoaiPK.MaPhuKien = "501";
                    LoaiPK.NoiDung = NoiDung;
                    LoaiPK.Master_ID = PKDK.ID;
                    long ID;
                    if (LoaiPK.ID == 0)
                    {
                       ID = LoaiPK.Insert();
                    }
                    else
                    {
                      ID = LoaiPK.Update();
                    }
                    if (TTHD.ID == 0)
                    {
                        TTHD.LoaiPhuKien_ID = ID;
                        TTHD.Insert();
                    }
                    else
                    {
                        TTHD.Update();
                    }
                    PKDK.PKCollection.Add(LoaiPK);
                }
                else
                {
                    TTHD.Update();
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "Lưu thành công", false);
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void EditInformationHopDongForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Sửa Thông tin chung của Hợp đồng có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    btnUpdate_Click(null,null);
                }
            }
        }
    }
}
