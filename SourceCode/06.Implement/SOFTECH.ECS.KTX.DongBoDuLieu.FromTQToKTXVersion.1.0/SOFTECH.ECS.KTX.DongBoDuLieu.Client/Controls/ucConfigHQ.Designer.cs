namespace DongBo.Controls
{
    partial class ucConfigHQ
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtDiaChiHQDT = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenTruyCap = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMatKhau = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenHQ = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cboxMaHQ = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.btnChangePass = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.radSynFromHQ = new System.Windows.Forms.RadioButton();
            this.radSynLocal = new System.Windows.Forms.RadioButton();
            this.cboType = new System.Windows.Forms.ComboBox();
            this.pnlBottom.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlContent.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlBottom
            // 
            this.pnlBottom.Controls.Add(this.label5);
            this.pnlBottom.Size = new System.Drawing.Size(600, 49);
            this.pnlBottom.TabIndex = 2;
            // 
            // pnlTop
            // 
            this.pnlTop.Size = new System.Drawing.Size(600, 28);
            this.pnlTop.TabIndex = 0;
            // 
            // pnlContent
            // 
            this.pnlContent.Controls.Add(this.cboType);
            this.pnlContent.Controls.Add(this.radSynLocal);
            this.pnlContent.Controls.Add(this.radSynFromHQ);
            this.pnlContent.Controls.Add(this.btnSave);
            this.pnlContent.Controls.Add(this.btnChangePass);
            this.pnlContent.Controls.Add(this.cboxMaHQ);
            this.pnlContent.Controls.Add(this.txtMatKhau);
            this.pnlContent.Controls.Add(this.label3);
            this.pnlContent.Controls.Add(this.txtTenHQ);
            this.pnlContent.Controls.Add(this.label7);
            this.pnlContent.Controls.Add(this.label6);
            this.pnlContent.Controls.Add(this.txtTenTruyCap);
            this.pnlContent.Controls.Add(this.label2);
            this.pnlContent.Controls.Add(this.txtDiaChiHQDT);
            this.pnlContent.Controls.Add(this.label8);
            this.pnlContent.Controls.Add(this.label4);
            this.pnlContent.Controls.Add(this.label1);
            this.pnlContent.Size = new System.Drawing.Size(600, 275);
            this.pnlContent.TabIndex = 1;
            // 
            // lblCaption
            // 
            this.lblCaption.Size = new System.Drawing.Size(298, 22);
            this.lblCaption.Text = "Cấu hình thông số kết nối hải quan";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(38, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Địa chỉ Hải quan điện tử:";
            // 
            // txtDiaChiHQDT
            // 
            this.txtDiaChiHQDT.Location = new System.Drawing.Point(187, 28);
            this.txtDiaChiHQDT.Name = "txtDiaChiHQDT";
            this.txtDiaChiHQDT.Size = new System.Drawing.Size(197, 21);
            this.txtDiaChiHQDT.TabIndex = 0;
            this.txtDiaChiHQDT.TextChanged += new System.EventHandler(this.txtDiaChiHQDT_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(100, 174);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên truy cập:";
            // 
            // txtTenTruyCap
            // 
            this.txtTenTruyCap.Location = new System.Drawing.Point(187, 171);
            this.txtTenTruyCap.Name = "txtTenTruyCap";
            this.txtTenTruyCap.Size = new System.Drawing.Size(120, 21);
            this.txtTenTruyCap.TabIndex = 5;
            this.txtTenTruyCap.TextChanged += new System.EventHandler(this.txtTenTruyCap_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(118, 201);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mật khẩu:";
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(187, 198);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(120, 21);
            this.txtMatKhau.TabIndex = 6;
            this.txtMatKhau.TextChanged += new System.EventHandler(this.txtMatKhau_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label4.Location = new System.Drawing.Point(184, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(275, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "(Địa chỉ Hải quan của doanh nghiệp đã khai báo điện tử)";
            // 
            // label5
            // 
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(5, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(590, 39);
            this.label5.TabIndex = 0;
            this.label5.Text = "Tên truy cập và mật khẩu  trên đây được cung cấp bởi Hải quan mà doanh nghiệp đã " +
                "tham gia khai báo điện tử";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(98, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Hải quan:";
            // 
            // txtTenHQ
            // 
            this.txtTenHQ.BackColor = System.Drawing.Color.White;
            this.txtTenHQ.Enabled = false;
            this.txtTenHQ.Location = new System.Drawing.Point(187, 113);
            this.txtTenHQ.Name = "txtTenHQ";
            this.txtTenHQ.Size = new System.Drawing.Size(197, 21);
            this.txtTenHQ.TabIndex = 3;
            this.txtTenHQ.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(102, 89);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Mã Hải quan:";
            // 
            // cboxMaHQ
            // 
            this.cboxMaHQ.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cboxMaHQ.FormattingEnabled = true;
            this.cboxMaHQ.Location = new System.Drawing.Point(187, 86);
            this.cboxMaHQ.Name = "cboxMaHQ";
            this.cboxMaHQ.Size = new System.Drawing.Size(69, 21);
            this.cboxMaHQ.TabIndex = 2;
            this.cboxMaHQ.Leave += new System.EventHandler(this.cboxMaHQ_Leave);
            this.cboxMaHQ.SelectedValueChanged += new System.EventHandler(this.cboxMaHQ_SelectedValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label8.Location = new System.Drawing.Point(184, 138);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(213, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "(Hãy chọn chính xác Mã Hải quan khai báo)";
            // 
            // btnChangePass
            // 
            this.btnChangePass.Location = new System.Drawing.Point(313, 196);
            this.btnChangePass.Name = "btnChangePass";
            this.btnChangePass.Size = new System.Drawing.Size(84, 23);
            this.btnChangePass.TabIndex = 8;
            this.btnChangePass.Text = "Đổi mật khẩu";
            this.btnChangePass.UseVisualStyleBackColor = true;
            this.btnChangePass.Click += new System.EventHandler(this.btnChangePass_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(313, 169);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(84, 23);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Lưu cấu hình";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // radSynFromHQ
            // 
            this.radSynFromHQ.AutoSize = true;
            this.radSynFromHQ.Checked = true;
            this.radSynFromHQ.Location = new System.Drawing.Point(187, 6);
            this.radSynFromHQ.Name = "radSynFromHQ";
            this.radSynFromHQ.Size = new System.Drawing.Size(125, 17);
            this.radSynFromHQ.TabIndex = 10;
            this.radSynFromHQ.TabStop = true;
            this.radSynFromHQ.Text = "Đồng bộ từ Hải quan";
            this.radSynFromHQ.UseVisualStyleBackColor = true;
            this.radSynFromHQ.CheckedChanged += new System.EventHandler(this.radSynFromHQ_CheckedChanged);
            // 
            // radSynLocal
            // 
            this.radSynLocal.AutoSize = true;
            this.radSynLocal.Location = new System.Drawing.Point(318, 6);
            this.radSynLocal.Name = "radSynLocal";
            this.radSynLocal.Size = new System.Drawing.Size(124, 17);
            this.radSynLocal.TabIndex = 10;
            this.radSynLocal.Text = "Đồng bộ trong mạng";
            this.radSynLocal.UseVisualStyleBackColor = true;
            this.radSynLocal.CheckedChanged += new System.EventHandler(this.radSynLocal_CheckedChanged);
            // 
            // cboType
            // 
            this.cboType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboType.Enabled = false;
            this.cboType.FormattingEnabled = true;
            this.cboType.Items.AddRange(new object[] {
            "KTX",
            "TQDT"});
            this.cboType.Location = new System.Drawing.Point(445, 2);
            this.cboType.Name = "cboType";
            this.cboType.Size = new System.Drawing.Size(54, 21);
            this.cboType.TabIndex = 11;
            // 
            // ucConfigHQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Name = "ucConfigHQ";
            this.Size = new System.Drawing.Size(610, 352);
            this.Load += new System.EventHandler(this.ucConfigHQ_Load);
            this.pnlBottom.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTop.PerformLayout();
            this.pnlContent.ResumeLayout(false);
            this.pnlContent.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtMatKhau;
        public System.Windows.Forms.TextBox txtTenTruyCap;
        public System.Windows.Forms.TextBox txtDiaChiHQDT;
        public System.Windows.Forms.ComboBox cboxMaHQ;
        public System.Windows.Forms.TextBox txtTenHQ;
        private System.Windows.Forms.Button btnChangePass;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RadioButton radSynLocal;
        private System.Windows.Forms.RadioButton radSynFromHQ;
        private System.Windows.Forms.ComboBox cboType;
    }
}
