
-------------------------------------------- STORE

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SAA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]
	@MaNguoiKhai varchar(13),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@SoHopDong nvarchar(35),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@NuocXuatKhau varchar(2),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@SoDangKyKinhDoanh varchar(35),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@NuocQuaCanh varchar(2),
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauNhap varchar(6),
	@TenCuaKhauNhap nvarchar(35),
	@SoGiayChungNhan nvarchar(35),
	@DiaDiemKiemDich nvarchar(150),
	@BenhMienDich nvarchar(300),
	@NgayTiemPhong datetime,
	@KhuTrung nvarchar(300),
	@NongDo nvarchar(300),
	@DiaDiemNuoiTrong nvarchar(70),
	@NgayKiemDich datetime,
	@ThoiGianKiemDich datetime,
	@DiaDiemGiamSat nvarchar(150),
	@NgayGiamSat datetime,
	@ThoiGianGiamSat datetime,
	@SoBanCanCap numeric(2, 0),
	@VatDungKhac nvarchar(150),
	@HoSoLienQuan nvarchar(750),
	@NoiChuyenDen nvarchar(250),
	@NguoiKiemTra nvarchar(50),
	@KetQuaKiemTra nvarchar(996),
	@TenTau nvarchar(35),
	@QuocTich varchar(2),
	@TenThuyenTruong nvarchar(50),
	@TenBacSi nvarchar(50),
	@SoThuyenVien numeric(4, 0),
	@SoHanhKhach numeric(4, 0),
	@CangRoiCuoiCung nvarchar(300),
	@CangDenTiepTheo nvarchar(300),
	@CangBocHangDauTien nvarchar(300),
	@NgayRoiCang datetime,
	@TenHangCangDauTien nvarchar(300),
	@SoLuongHangCangDauTien numeric(8, 0),
	@DonViTinhSoLuongHangCangDauTien varchar(30),
	@KhoiLuongHangCangDauTien numeric(10, 3),
	@DonViTinhKhoiLuongHangCangDauTien varchar(3),
	@TenHangCanBoc nvarchar(300),
	@SoLuongHangCanBoc numeric(8, 0),
	@DonViTinhSoLuongHangCanBoc varchar(30),
	@KhoiLuongHangCanBoc numeric(10, 3),
	@DonViTinhKhoiLuongHangCanBoc varchar(3),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@LyDoKhongDat nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(300),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TenLoaiPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int,
	@NgayKhaiBao datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SAA]
(
	[MaNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[SoHopDong],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[NuocXuatKhau],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[SoDangKyKinhDoanh],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[NuocQuaCanh],
	[PhuongTienVanChuyen],
	[MaCuaKhauNhap],
	[TenCuaKhauNhap],
	[SoGiayChungNhan],
	[DiaDiemKiemDich],
	[BenhMienDich],
	[NgayTiemPhong],
	[KhuTrung],
	[NongDo],
	[DiaDiemNuoiTrong],
	[NgayKiemDich],
	[ThoiGianKiemDich],
	[DiaDiemGiamSat],
	[NgayGiamSat],
	[ThoiGianGiamSat],
	[SoBanCanCap],
	[VatDungKhac],
	[HoSoLienQuan],
	[NoiChuyenDen],
	[NguoiKiemTra],
	[KetQuaKiemTra],
	[TenTau],
	[QuocTich],
	[TenThuyenTruong],
	[TenBacSi],
	[SoThuyenVien],
	[SoHanhKhach],
	[CangRoiCuoiCung],
	[CangDenTiepTheo],
	[CangBocHangDauTien],
	[NgayRoiCang],
	[TenHangCangDauTien],
	[SoLuongHangCangDauTien],
	[DonViTinhSoLuongHangCangDauTien],
	[KhoiLuongHangCangDauTien],
	[DonViTinhKhoiLuongHangCangDauTien],
	[TenHangCanBoc],
	[SoLuongHangCanBoc],
	[DonViTinhSoLuongHangCanBoc],
	[KhoiLuongHangCanBoc],
	[DonViTinhKhoiLuongHangCanBoc],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[LyDoKhongDat],
	[DaiDienDonViCapPhep],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TenLoaiPhuongTienVanChuyen],
	[TrangThaiXuLy],
	[NgayKhaiBao]
)
VALUES 
(
	@MaNguoiKhai,
	@MaBuuChinhNguoiKhai,
	@DiaChiNguoiKhai,
	@MaNuocNguoiKhai,
	@SoDienThoaiNguoiKhai,
	@SoFaxNguoiKhai,
	@EmailNguoiKhai,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@MaDonViCapPhep,
	@SoHopDong,
	@MaThuongNhanXuatKhau,
	@TenThuongNhanXuatKhau,
	@MaBuuChinhXuatKhau,
	@SoNhaTenDuongXuatKhau,
	@PhuongXaXuatKhau,
	@QuanHuyenXuatKhau,
	@TinhThanhPhoXuatKhau,
	@MaQuocGiaXuatKhau,
	@SoDienThoaiXuatKhau,
	@SoFaxXuatKhau,
	@EmailXuatKhau,
	@NuocXuatKhau,
	@MaThuongNhanNhapKhau,
	@TenThuongNhanNhapKhau,
	@SoDangKyKinhDoanh,
	@MaBuuChinhNhapKhau,
	@DiaChiThuongNhanNhapKhau,
	@MaQuocGiaNhapKhau,
	@SoDienThoaiNhapKhau,
	@SoFaxNhapKhau,
	@EmailNhapKhau,
	@NuocQuaCanh,
	@PhuongTienVanChuyen,
	@MaCuaKhauNhap,
	@TenCuaKhauNhap,
	@SoGiayChungNhan,
	@DiaDiemKiemDich,
	@BenhMienDich,
	@NgayTiemPhong,
	@KhuTrung,
	@NongDo,
	@DiaDiemNuoiTrong,
	@NgayKiemDich,
	@ThoiGianKiemDich,
	@DiaDiemGiamSat,
	@NgayGiamSat,
	@ThoiGianGiamSat,
	@SoBanCanCap,
	@VatDungKhac,
	@HoSoLienQuan,
	@NoiChuyenDen,
	@NguoiKiemTra,
	@KetQuaKiemTra,
	@TenTau,
	@QuocTich,
	@TenThuyenTruong,
	@TenBacSi,
	@SoThuyenVien,
	@SoHanhKhach,
	@CangRoiCuoiCung,
	@CangDenTiepTheo,
	@CangBocHangDauTien,
	@NgayRoiCang,
	@TenHangCangDauTien,
	@SoLuongHangCangDauTien,
	@DonViTinhSoLuongHangCangDauTien,
	@KhoiLuongHangCangDauTien,
	@DonViTinhKhoiLuongHangCangDauTien,
	@TenHangCanBoc,
	@SoLuongHangCanBoc,
	@DonViTinhSoLuongHangCanBoc,
	@KhoiLuongHangCanBoc,
	@DonViTinhKhoiLuongHangCanBoc,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@LyDoKhongDat,
	@DaiDienDonViCapPhep,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@TenLoaiPhuongTienVanChuyen,
	@TrangThaiXuLy,
	@NgayKhaiBao
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_InsertUpdate]    Script Date: 11/11/2013 17:34:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@SoHopDong nvarchar(35),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@NuocXuatKhau varchar(2),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@SoDangKyKinhDoanh varchar(35),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@NuocQuaCanh varchar(2),
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauNhap varchar(6),
	@TenCuaKhauNhap nvarchar(35),
	@SoGiayChungNhan nvarchar(35),
	@DiaDiemKiemDich nvarchar(150),
	@BenhMienDich nvarchar(300),
	@NgayTiemPhong datetime,
	@KhuTrung nvarchar(300),
	@NongDo nvarchar(300),
	@DiaDiemNuoiTrong nvarchar(70),
	@NgayKiemDich datetime,
	@ThoiGianKiemDich datetime,
	@DiaDiemGiamSat nvarchar(150),
	@NgayGiamSat datetime,
	@ThoiGianGiamSat datetime,
	@SoBanCanCap numeric(2, 0),
	@VatDungKhac nvarchar(150),
	@HoSoLienQuan nvarchar(750),
	@NoiChuyenDen nvarchar(250),
	@NguoiKiemTra nvarchar(50),
	@KetQuaKiemTra nvarchar(996),
	@TenTau nvarchar(35),
	@QuocTich varchar(2),
	@TenThuyenTruong nvarchar(50),
	@TenBacSi nvarchar(50),
	@SoThuyenVien numeric(4, 0),
	@SoHanhKhach numeric(4, 0),
	@CangRoiCuoiCung nvarchar(300),
	@CangDenTiepTheo nvarchar(300),
	@CangBocHangDauTien nvarchar(300),
	@NgayRoiCang datetime,
	@TenHangCangDauTien nvarchar(300),
	@SoLuongHangCangDauTien numeric(8, 0),
	@DonViTinhSoLuongHangCangDauTien varchar(30),
	@KhoiLuongHangCangDauTien numeric(10, 3),
	@DonViTinhKhoiLuongHangCangDauTien varchar(3),
	@TenHangCanBoc nvarchar(300),
	@SoLuongHangCanBoc numeric(8, 0),
	@DonViTinhSoLuongHangCanBoc varchar(30),
	@KhoiLuongHangCanBoc numeric(10, 3),
	@DonViTinhKhoiLuongHangCanBoc varchar(3),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@LyDoKhongDat nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(300),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TenLoaiPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int,
	@NgayKhaiBao datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SAA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[MaBuuChinhNguoiKhai] = @MaBuuChinhNguoiKhai,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[MaNuocNguoiKhai] = @MaNuocNguoiKhai,
			[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
			[SoFaxNguoiKhai] = @SoFaxNguoiKhai,
			[EmailNguoiKhai] = @EmailNguoiKhai,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[MaDonViCapPhep] = @MaDonViCapPhep,
			[SoHopDong] = @SoHopDong,
			[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
			[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
			[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
			[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
			[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
			[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
			[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
			[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
			[SoDienThoaiXuatKhau] = @SoDienThoaiXuatKhau,
			[SoFaxXuatKhau] = @SoFaxXuatKhau,
			[EmailXuatKhau] = @EmailXuatKhau,
			[NuocXuatKhau] = @NuocXuatKhau,
			[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
			[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
			[SoDangKyKinhDoanh] = @SoDangKyKinhDoanh,
			[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
			[DiaChiThuongNhanNhapKhau] = @DiaChiThuongNhanNhapKhau,
			[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
			[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
			[SoFaxNhapKhau] = @SoFaxNhapKhau,
			[EmailNhapKhau] = @EmailNhapKhau,
			[NuocQuaCanh] = @NuocQuaCanh,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[MaCuaKhauNhap] = @MaCuaKhauNhap,
			[TenCuaKhauNhap] = @TenCuaKhauNhap,
			[SoGiayChungNhan] = @SoGiayChungNhan,
			[DiaDiemKiemDich] = @DiaDiemKiemDich,
			[BenhMienDich] = @BenhMienDich,
			[NgayTiemPhong] = @NgayTiemPhong,
			[KhuTrung] = @KhuTrung,
			[NongDo] = @NongDo,
			[DiaDiemNuoiTrong] = @DiaDiemNuoiTrong,
			[NgayKiemDich] = @NgayKiemDich,
			[ThoiGianKiemDich] = @ThoiGianKiemDich,
			[DiaDiemGiamSat] = @DiaDiemGiamSat,
			[NgayGiamSat] = @NgayGiamSat,
			[ThoiGianGiamSat] = @ThoiGianGiamSat,
			[SoBanCanCap] = @SoBanCanCap,
			[VatDungKhac] = @VatDungKhac,
			[HoSoLienQuan] = @HoSoLienQuan,
			[NoiChuyenDen] = @NoiChuyenDen,
			[NguoiKiemTra] = @NguoiKiemTra,
			[KetQuaKiemTra] = @KetQuaKiemTra,
			[TenTau] = @TenTau,
			[QuocTich] = @QuocTich,
			[TenThuyenTruong] = @TenThuyenTruong,
			[TenBacSi] = @TenBacSi,
			[SoThuyenVien] = @SoThuyenVien,
			[SoHanhKhach] = @SoHanhKhach,
			[CangRoiCuoiCung] = @CangRoiCuoiCung,
			[CangDenTiepTheo] = @CangDenTiepTheo,
			[CangBocHangDauTien] = @CangBocHangDauTien,
			[NgayRoiCang] = @NgayRoiCang,
			[TenHangCangDauTien] = @TenHangCangDauTien,
			[SoLuongHangCangDauTien] = @SoLuongHangCangDauTien,
			[DonViTinhSoLuongHangCangDauTien] = @DonViTinhSoLuongHangCangDauTien,
			[KhoiLuongHangCangDauTien] = @KhoiLuongHangCangDauTien,
			[DonViTinhKhoiLuongHangCangDauTien] = @DonViTinhKhoiLuongHangCangDauTien,
			[TenHangCanBoc] = @TenHangCanBoc,
			[SoLuongHangCanBoc] = @SoLuongHangCanBoc,
			[DonViTinhSoLuongHangCanBoc] = @DonViTinhSoLuongHangCanBoc,
			[KhoiLuongHangCanBoc] = @KhoiLuongHangCanBoc,
			[DonViTinhKhoiLuongHangCanBoc] = @DonViTinhKhoiLuongHangCanBoc,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[LyDoKhongDat] = @LyDoKhongDat,
			[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[TenLoaiPhuongTienVanChuyen] = @TenLoaiPhuongTienVanChuyen,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[NgayKhaiBao] = @NgayKhaiBao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SAA]
		(
			[MaNguoiKhai],
			[MaBuuChinhNguoiKhai],
			[DiaChiNguoiKhai],
			[MaNuocNguoiKhai],
			[SoDienThoaiNguoiKhai],
			[SoFaxNguoiKhai],
			[EmailNguoiKhai],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[MaDonViCapPhep],
			[SoHopDong],
			[MaThuongNhanXuatKhau],
			[TenThuongNhanXuatKhau],
			[MaBuuChinhXuatKhau],
			[SoNhaTenDuongXuatKhau],
			[PhuongXaXuatKhau],
			[QuanHuyenXuatKhau],
			[TinhThanhPhoXuatKhau],
			[MaQuocGiaXuatKhau],
			[SoDienThoaiXuatKhau],
			[SoFaxXuatKhau],
			[EmailXuatKhau],
			[NuocXuatKhau],
			[MaThuongNhanNhapKhau],
			[TenThuongNhanNhapKhau],
			[SoDangKyKinhDoanh],
			[MaBuuChinhNhapKhau],
			[DiaChiThuongNhanNhapKhau],
			[MaQuocGiaNhapKhau],
			[SoDienThoaiNhapKhau],
			[SoFaxNhapKhau],
			[EmailNhapKhau],
			[NuocQuaCanh],
			[PhuongTienVanChuyen],
			[MaCuaKhauNhap],
			[TenCuaKhauNhap],
			[SoGiayChungNhan],
			[DiaDiemKiemDich],
			[BenhMienDich],
			[NgayTiemPhong],
			[KhuTrung],
			[NongDo],
			[DiaDiemNuoiTrong],
			[NgayKiemDich],
			[ThoiGianKiemDich],
			[DiaDiemGiamSat],
			[NgayGiamSat],
			[ThoiGianGiamSat],
			[SoBanCanCap],
			[VatDungKhac],
			[HoSoLienQuan],
			[NoiChuyenDen],
			[NguoiKiemTra],
			[KetQuaKiemTra],
			[TenTau],
			[QuocTich],
			[TenThuyenTruong],
			[TenBacSi],
			[SoThuyenVien],
			[SoHanhKhach],
			[CangRoiCuoiCung],
			[CangDenTiepTheo],
			[CangBocHangDauTien],
			[NgayRoiCang],
			[TenHangCangDauTien],
			[SoLuongHangCangDauTien],
			[DonViTinhSoLuongHangCangDauTien],
			[KhoiLuongHangCangDauTien],
			[DonViTinhKhoiLuongHangCangDauTien],
			[TenHangCanBoc],
			[SoLuongHangCanBoc],
			[DonViTinhSoLuongHangCanBoc],
			[KhoiLuongHangCanBoc],
			[DonViTinhKhoiLuongHangCanBoc],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[LyDoKhongDat],
			[DaiDienDonViCapPhep],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[TenLoaiPhuongTienVanChuyen],
			[TrangThaiXuLy],
			[NgayKhaiBao]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@MaBuuChinhNguoiKhai,
			@DiaChiNguoiKhai,
			@MaNuocNguoiKhai,
			@SoDienThoaiNguoiKhai,
			@SoFaxNguoiKhai,
			@EmailNguoiKhai,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@MaDonViCapPhep,
			@SoHopDong,
			@MaThuongNhanXuatKhau,
			@TenThuongNhanXuatKhau,
			@MaBuuChinhXuatKhau,
			@SoNhaTenDuongXuatKhau,
			@PhuongXaXuatKhau,
			@QuanHuyenXuatKhau,
			@TinhThanhPhoXuatKhau,
			@MaQuocGiaXuatKhau,
			@SoDienThoaiXuatKhau,
			@SoFaxXuatKhau,
			@EmailXuatKhau,
			@NuocXuatKhau,
			@MaThuongNhanNhapKhau,
			@TenThuongNhanNhapKhau,
			@SoDangKyKinhDoanh,
			@MaBuuChinhNhapKhau,
			@DiaChiThuongNhanNhapKhau,
			@MaQuocGiaNhapKhau,
			@SoDienThoaiNhapKhau,
			@SoFaxNhapKhau,
			@EmailNhapKhau,
			@NuocQuaCanh,
			@PhuongTienVanChuyen,
			@MaCuaKhauNhap,
			@TenCuaKhauNhap,
			@SoGiayChungNhan,
			@DiaDiemKiemDich,
			@BenhMienDich,
			@NgayTiemPhong,
			@KhuTrung,
			@NongDo,
			@DiaDiemNuoiTrong,
			@NgayKiemDich,
			@ThoiGianKiemDich,
			@DiaDiemGiamSat,
			@NgayGiamSat,
			@ThoiGianGiamSat,
			@SoBanCanCap,
			@VatDungKhac,
			@HoSoLienQuan,
			@NoiChuyenDen,
			@NguoiKiemTra,
			@KetQuaKiemTra,
			@TenTau,
			@QuocTich,
			@TenThuyenTruong,
			@TenBacSi,
			@SoThuyenVien,
			@SoHanhKhach,
			@CangRoiCuoiCung,
			@CangDenTiepTheo,
			@CangBocHangDauTien,
			@NgayRoiCang,
			@TenHangCangDauTien,
			@SoLuongHangCangDauTien,
			@DonViTinhSoLuongHangCangDauTien,
			@KhoiLuongHangCangDauTien,
			@DonViTinhKhoiLuongHangCangDauTien,
			@TenHangCanBoc,
			@SoLuongHangCanBoc,
			@DonViTinhSoLuongHangCanBoc,
			@KhoiLuongHangCanBoc,
			@DonViTinhKhoiLuongHangCanBoc,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@LyDoKhongDat,
			@DaiDienDonViCapPhep,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@TenLoaiPhuongTienVanChuyen,
			@TrangThaiXuLy,
			@NgayKhaiBao
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[SoHopDong],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[NuocXuatKhau],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[SoDangKyKinhDoanh],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[NuocQuaCanh],
	[PhuongTienVanChuyen],
	[MaCuaKhauNhap],
	[TenCuaKhauNhap],
	[SoGiayChungNhan],
	[DiaDiemKiemDich],
	[BenhMienDich],
	[NgayTiemPhong],
	[KhuTrung],
	[NongDo],
	[DiaDiemNuoiTrong],
	[NgayKiemDich],
	[ThoiGianKiemDich],
	[DiaDiemGiamSat],
	[NgayGiamSat],
	[ThoiGianGiamSat],
	[SoBanCanCap],
	[VatDungKhac],
	[HoSoLienQuan],
	[NoiChuyenDen],
	[NguoiKiemTra],
	[KetQuaKiemTra],
	[TenTau],
	[QuocTich],
	[TenThuyenTruong],
	[TenBacSi],
	[SoThuyenVien],
	[SoHanhKhach],
	[CangRoiCuoiCung],
	[CangDenTiepTheo],
	[CangBocHangDauTien],
	[NgayRoiCang],
	[TenHangCangDauTien],
	[SoLuongHangCangDauTien],
	[DonViTinhSoLuongHangCangDauTien],
	[KhoiLuongHangCangDauTien],
	[DonViTinhKhoiLuongHangCangDauTien],
	[TenHangCanBoc],
	[SoLuongHangCanBoc],
	[DonViTinhSoLuongHangCanBoc],
	[KhoiLuongHangCanBoc],
	[DonViTinhKhoiLuongHangCanBoc],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[LyDoKhongDat],
	[DaiDienDonViCapPhep],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TenLoaiPhuongTienVanChuyen],
	[TrangThaiXuLy],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[SoHopDong],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[NuocXuatKhau],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[SoDangKyKinhDoanh],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[NuocQuaCanh],
	[PhuongTienVanChuyen],
	[MaCuaKhauNhap],
	[TenCuaKhauNhap],
	[SoGiayChungNhan],
	[DiaDiemKiemDich],
	[BenhMienDich],
	[NgayTiemPhong],
	[KhuTrung],
	[NongDo],
	[DiaDiemNuoiTrong],
	[NgayKiemDich],
	[ThoiGianKiemDich],
	[DiaDiemGiamSat],
	[NgayGiamSat],
	[ThoiGianGiamSat],
	[SoBanCanCap],
	[VatDungKhac],
	[HoSoLienQuan],
	[NoiChuyenDen],
	[NguoiKiemTra],
	[KetQuaKiemTra],
	[TenTau],
	[QuocTich],
	[TenThuyenTruong],
	[TenBacSi],
	[SoThuyenVien],
	[SoHanhKhach],
	[CangRoiCuoiCung],
	[CangDenTiepTheo],
	[CangBocHangDauTien],
	[NgayRoiCang],
	[TenHangCangDauTien],
	[SoLuongHangCangDauTien],
	[DonViTinhSoLuongHangCangDauTien],
	[KhoiLuongHangCangDauTien],
	[DonViTinhKhoiLuongHangCangDauTien],
	[TenHangCanBoc],
	[SoLuongHangCanBoc],
	[DonViTinhSoLuongHangCanBoc],
	[KhoiLuongHangCanBoc],
	[DonViTinhKhoiLuongHangCanBoc],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[LyDoKhongDat],
	[DaiDienDonViCapPhep],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TenLoaiPhuongTienVanChuyen],
	[TrangThaiXuLy],
	[NgayKhaiBao]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[SoHopDong],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[NuocXuatKhau],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[SoDangKyKinhDoanh],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[NuocQuaCanh],
	[PhuongTienVanChuyen],
	[MaCuaKhauNhap],
	[TenCuaKhauNhap],
	[SoGiayChungNhan],
	[DiaDiemKiemDich],
	[BenhMienDich],
	[NgayTiemPhong],
	[KhuTrung],
	[NongDo],
	[DiaDiemNuoiTrong],
	[NgayKiemDich],
	[ThoiGianKiemDich],
	[DiaDiemGiamSat],
	[NgayGiamSat],
	[ThoiGianGiamSat],
	[SoBanCanCap],
	[VatDungKhac],
	[HoSoLienQuan],
	[NoiChuyenDen],
	[NguoiKiemTra],
	[KetQuaKiemTra],
	[TenTau],
	[QuocTich],
	[TenThuyenTruong],
	[TenBacSi],
	[SoThuyenVien],
	[SoHanhKhach],
	[CangRoiCuoiCung],
	[CangDenTiepTheo],
	[CangBocHangDauTien],
	[NgayRoiCang],
	[TenHangCangDauTien],
	[SoLuongHangCangDauTien],
	[DonViTinhSoLuongHangCangDauTien],
	[KhoiLuongHangCangDauTien],
	[DonViTinhKhoiLuongHangCangDauTien],
	[TenHangCanBoc],
	[SoLuongHangCanBoc],
	[DonViTinhSoLuongHangCanBoc],
	[KhoiLuongHangCanBoc],
	[DonViTinhKhoiLuongHangCanBoc],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[LyDoKhongDat],
	[DaiDienDonViCapPhep],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TenLoaiPhuongTienVanChuyen],
	[TrangThaiXuLy],
	[NgayKhaiBao]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]
	@GiayPhep_ID bigint,
	@TongSoToKhai numeric(6, 0),
	@TinhTrangThamTra varchar(2),
	@PhuongThucXuLyCuoiCung numeric(1, 0),
	@SoDonXinCapPhep numeric(12, 0),
	@MaNhanVienKiemTra varchar(8),
	@NgayKhaiBao datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@NgayGiaoViec datetime,
	@GioGiaoViec datetime,
	@NguoiGiaoViec varchar(8),
	@NoiDungGiaoViec nvarchar(768),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
(
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhep_ID,
	@TongSoToKhai,
	@TinhTrangThamTra,
	@PhuongThucXuLyCuoiCung,
	@SoDonXinCapPhep,
	@MaNhanVienKiemTra,
	@NgayKhaiBao,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@NgayGiaoViec,
	@GioGiaoViec,
	@NguoiGiaoViec,
	@NoiDungGiaoViec,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TongSoToKhai numeric(6, 0),
	@TinhTrangThamTra varchar(2),
	@PhuongThucXuLyCuoiCung numeric(1, 0),
	@SoDonXinCapPhep numeric(12, 0),
	@MaNhanVienKiemTra varchar(8),
	@NgayKhaiBao datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@NgayGiaoViec datetime,
	@GioGiaoViec datetime,
	@NguoiGiaoViec varchar(8),
	@NoiDungGiaoViec nvarchar(768),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[TongSoToKhai] = @TongSoToKhai,
			[TinhTrangThamTra] = @TinhTrangThamTra,
			[PhuongThucXuLyCuoiCung] = @PhuongThucXuLyCuoiCung,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[MaNhanVienKiemTra] = @MaNhanVienKiemTra,
			[NgayKhaiBao] = @NgayKhaiBao,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[NgayGiaoViec] = @NgayGiaoViec,
			[GioGiaoViec] = @GioGiaoViec,
			[NguoiGiaoViec] = @NguoiGiaoViec,
			[NoiDungGiaoViec] = @NoiDungGiaoViec,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
		(
			[GiayPhep_ID],
			[TongSoToKhai],
			[TinhTrangThamTra],
			[PhuongThucXuLyCuoiCung],
			[SoDonXinCapPhep],
			[MaNhanVienKiemTra],
			[NgayKhaiBao],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[NgayGiaoViec],
			[GioGiaoViec],
			[NguoiGiaoViec],
			[NoiDungGiaoViec],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@TongSoToKhai,
			@TinhTrangThamTra,
			@PhuongThucXuLyCuoiCung,
			@SoDonXinCapPhep,
			@MaNhanVienKiemTra,
			@NgayKhaiBao,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@NgayGiaoViec,
			@GioGiaoViec,
			@NguoiGiaoViec,
			@NoiDungGiaoViec,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TongSoToKhai numeric(6, 0),
	@TinhTrangThamTra varchar(2),
	@PhuongThucXuLyCuoiCung numeric(1, 0),
	@SoDonXinCapPhep numeric(12, 0),
	@MaNhanVienKiemTra varchar(8),
	@NgayKhaiBao datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@NgayGiaoViec datetime,
	@GioGiaoViec datetime,
	@NguoiGiaoViec varchar(8),
	@NoiDungGiaoViec nvarchar(768),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[TongSoToKhai] = @TongSoToKhai,
	[TinhTrangThamTra] = @TinhTrangThamTra,
	[PhuongThucXuLyCuoiCung] = @PhuongThucXuLyCuoiCung,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[MaNhanVienKiemTra] = @MaNhanVienKiemTra,
	[NgayKhaiBao] = @NgayKhaiBao,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[NgayGiaoViec] = @NgayGiaoViec,
	[GioGiaoViec] = @GioGiaoViec,
	[NguoiGiaoViec] = @NguoiGiaoViec,
	[NoiDungGiaoViec] = @NoiDungGiaoViec,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@SoHopDong nvarchar(35),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@NuocXuatKhau varchar(2),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@SoDangKyKinhDoanh varchar(35),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@NuocQuaCanh varchar(2),
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauNhap varchar(6),
	@TenCuaKhauNhap nvarchar(35),
	@SoGiayChungNhan nvarchar(35),
	@DiaDiemKiemDich nvarchar(150),
	@BenhMienDich nvarchar(300),
	@NgayTiemPhong datetime,
	@KhuTrung nvarchar(300),
	@NongDo nvarchar(300),
	@DiaDiemNuoiTrong nvarchar(70),
	@NgayKiemDich datetime,
	@ThoiGianKiemDich datetime,
	@DiaDiemGiamSat nvarchar(150),
	@NgayGiamSat datetime,
	@ThoiGianGiamSat datetime,
	@SoBanCanCap numeric(2, 0),
	@VatDungKhac nvarchar(150),
	@HoSoLienQuan nvarchar(750),
	@NoiChuyenDen nvarchar(250),
	@NguoiKiemTra nvarchar(50),
	@KetQuaKiemTra nvarchar(996),
	@TenTau nvarchar(35),
	@QuocTich varchar(2),
	@TenThuyenTruong nvarchar(50),
	@TenBacSi nvarchar(50),
	@SoThuyenVien numeric(4, 0),
	@SoHanhKhach numeric(4, 0),
	@CangRoiCuoiCung nvarchar(300),
	@CangDenTiepTheo nvarchar(300),
	@CangBocHangDauTien nvarchar(300),
	@NgayRoiCang datetime,
	@TenHangCangDauTien nvarchar(300),
	@SoLuongHangCangDauTien numeric(8, 0),
	@DonViTinhSoLuongHangCangDauTien varchar(30),
	@KhoiLuongHangCangDauTien numeric(10, 3),
	@DonViTinhKhoiLuongHangCangDauTien varchar(3),
	@TenHangCanBoc nvarchar(300),
	@SoLuongHangCanBoc numeric(8, 0),
	@DonViTinhSoLuongHangCanBoc varchar(30),
	@KhoiLuongHangCanBoc numeric(10, 3),
	@DonViTinhKhoiLuongHangCanBoc varchar(3),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@LyDoKhongDat nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(300),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TenLoaiPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int,
	@NgayKhaiBao datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SAA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[MaBuuChinhNguoiKhai] = @MaBuuChinhNguoiKhai,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[MaNuocNguoiKhai] = @MaNuocNguoiKhai,
	[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
	[SoFaxNguoiKhai] = @SoFaxNguoiKhai,
	[EmailNguoiKhai] = @EmailNguoiKhai,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[MaDonViCapPhep] = @MaDonViCapPhep,
	[SoHopDong] = @SoHopDong,
	[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
	[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
	[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
	[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
	[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
	[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
	[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
	[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
	[SoDienThoaiXuatKhau] = @SoDienThoaiXuatKhau,
	[SoFaxXuatKhau] = @SoFaxXuatKhau,
	[EmailXuatKhau] = @EmailXuatKhau,
	[NuocXuatKhau] = @NuocXuatKhau,
	[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
	[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
	[SoDangKyKinhDoanh] = @SoDangKyKinhDoanh,
	[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
	[DiaChiThuongNhanNhapKhau] = @DiaChiThuongNhanNhapKhau,
	[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
	[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
	[SoFaxNhapKhau] = @SoFaxNhapKhau,
	[EmailNhapKhau] = @EmailNhapKhau,
	[NuocQuaCanh] = @NuocQuaCanh,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[MaCuaKhauNhap] = @MaCuaKhauNhap,
	[TenCuaKhauNhap] = @TenCuaKhauNhap,
	[SoGiayChungNhan] = @SoGiayChungNhan,
	[DiaDiemKiemDich] = @DiaDiemKiemDich,
	[BenhMienDich] = @BenhMienDich,
	[NgayTiemPhong] = @NgayTiemPhong,
	[KhuTrung] = @KhuTrung,
	[NongDo] = @NongDo,
	[DiaDiemNuoiTrong] = @DiaDiemNuoiTrong,
	[NgayKiemDich] = @NgayKiemDich,
	[ThoiGianKiemDich] = @ThoiGianKiemDich,
	[DiaDiemGiamSat] = @DiaDiemGiamSat,
	[NgayGiamSat] = @NgayGiamSat,
	[ThoiGianGiamSat] = @ThoiGianGiamSat,
	[SoBanCanCap] = @SoBanCanCap,
	[VatDungKhac] = @VatDungKhac,
	[HoSoLienQuan] = @HoSoLienQuan,
	[NoiChuyenDen] = @NoiChuyenDen,
	[NguoiKiemTra] = @NguoiKiemTra,
	[KetQuaKiemTra] = @KetQuaKiemTra,
	[TenTau] = @TenTau,
	[QuocTich] = @QuocTich,
	[TenThuyenTruong] = @TenThuyenTruong,
	[TenBacSi] = @TenBacSi,
	[SoThuyenVien] = @SoThuyenVien,
	[SoHanhKhach] = @SoHanhKhach,
	[CangRoiCuoiCung] = @CangRoiCuoiCung,
	[CangDenTiepTheo] = @CangDenTiepTheo,
	[CangBocHangDauTien] = @CangBocHangDauTien,
	[NgayRoiCang] = @NgayRoiCang,
	[TenHangCangDauTien] = @TenHangCangDauTien,
	[SoLuongHangCangDauTien] = @SoLuongHangCangDauTien,
	[DonViTinhSoLuongHangCangDauTien] = @DonViTinhSoLuongHangCangDauTien,
	[KhoiLuongHangCangDauTien] = @KhoiLuongHangCangDauTien,
	[DonViTinhKhoiLuongHangCangDauTien] = @DonViTinhKhoiLuongHangCangDauTien,
	[TenHangCanBoc] = @TenHangCanBoc,
	[SoLuongHangCanBoc] = @SoLuongHangCanBoc,
	[DonViTinhSoLuongHangCanBoc] = @DonViTinhSoLuongHangCanBoc,
	[KhoiLuongHangCanBoc] = @KhoiLuongHangCanBoc,
	[DonViTinhKhoiLuongHangCanBoc] = @DonViTinhKhoiLuongHangCanBoc,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[LyDoKhongDat] = @LyDoKhongDat,
	[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[TenLoaiPhuongTienVanChuyen] = @TenLoaiPhuongTienVanChuyen,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[NgayKhaiBao] = @NgayKhaiBao
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SEA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]    Script Date: 11/11/2013 17:34:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Insert]
	@MaNguoiKhai varchar(13),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@LoaiHinhXNK varchar(1),
	@MaDV_CapPhep varchar(6),
	@MaDoanhNghiep varchar(13),
	@TenDoanhNghiep nvarchar(300),
	@QuyetDinhSo varchar(17),
	@SoGiayChungNhan varchar(17),
	@NoiCap nvarchar(35),
	@NgayCap datetime,
	@MaBuuChinh varchar(7),
	@DiaChi varchar(300),
	@MaQuocGia varchar(2),
	@SoDienThoai varchar(20),
	@SoFax varchar(20),
	@Email varchar(210),
	@SoHopDongMua varchar(17),
	@NgayHopDongMua datetime,
	@SoHopDongBan varchar(17),
	@NgayHopDongBan datetime,
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauXuatNhap varchar(6),
	@TenCuaKhauXuatNhap varchar(35),
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TenGiamDoc nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(50),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(996),
	@NgayKhaiBao datetime,
	@TenNguoiKhai nvarchar(300),
	@TenDonViCapPhep nvarchar(300),
	@TenPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SEA]
(
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
)
VALUES 
(
	@MaNguoiKhai,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@LoaiHinhXNK,
	@MaDV_CapPhep,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@QuyetDinhSo,
	@SoGiayChungNhan,
	@NoiCap,
	@NgayCap,
	@MaBuuChinh,
	@DiaChi,
	@MaQuocGia,
	@SoDienThoai,
	@SoFax,
	@Email,
	@SoHopDongMua,
	@NgayHopDongMua,
	@SoHopDongBan,
	@NgayHopDongBan,
	@PhuongTienVanChuyen,
	@MaCuaKhauXuatNhap,
	@TenCuaKhauXuatNhap,
	@NgayBatDau,
	@NgayKetThuc,
	@HoSoLienQuan,
	@GhiChu,
	@TenGiamDoc,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@GhiChuDanhChoCoQuanCapPhep,
	@DaiDienDonViCapPhep,
	@NgayKhaiBao,
	@TenNguoiKhai,
	@TenDonViCapPhep,
	@TenPhuongTienVanChuyen,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@LoaiHinhXNK varchar(1),
	@MaDV_CapPhep varchar(6),
	@MaDoanhNghiep varchar(13),
	@TenDoanhNghiep nvarchar(300),
	@QuyetDinhSo varchar(17),
	@SoGiayChungNhan varchar(17),
	@NoiCap nvarchar(35),
	@NgayCap datetime,
	@MaBuuChinh varchar(7),
	@DiaChi varchar(300),
	@MaQuocGia varchar(2),
	@SoDienThoai varchar(20),
	@SoFax varchar(20),
	@Email varchar(210),
	@SoHopDongMua varchar(17),
	@NgayHopDongMua datetime,
	@SoHopDongBan varchar(17),
	@NgayHopDongBan datetime,
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauXuatNhap varchar(6),
	@TenCuaKhauXuatNhap varchar(35),
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TenGiamDoc nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(50),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(996),
	@NgayKhaiBao datetime,
	@TenNguoiKhai nvarchar(300),
	@TenDonViCapPhep nvarchar(300),
	@TenPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SEA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SEA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[LoaiHinhXNK] = @LoaiHinhXNK,
			[MaDV_CapPhep] = @MaDV_CapPhep,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[QuyetDinhSo] = @QuyetDinhSo,
			[SoGiayChungNhan] = @SoGiayChungNhan,
			[NoiCap] = @NoiCap,
			[NgayCap] = @NgayCap,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChi] = @DiaChi,
			[MaQuocGia] = @MaQuocGia,
			[SoDienThoai] = @SoDienThoai,
			[SoFax] = @SoFax,
			[Email] = @Email,
			[SoHopDongMua] = @SoHopDongMua,
			[NgayHopDongMua] = @NgayHopDongMua,
			[SoHopDongBan] = @SoHopDongBan,
			[NgayHopDongBan] = @NgayHopDongBan,
			[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
			[MaCuaKhauXuatNhap] = @MaCuaKhauXuatNhap,
			[TenCuaKhauXuatNhap] = @TenCuaKhauXuatNhap,
			[NgayBatDau] = @NgayBatDau,
			[NgayKetThuc] = @NgayKetThuc,
			[HoSoLienQuan] = @HoSoLienQuan,
			[GhiChu] = @GhiChu,
			[TenGiamDoc] = @TenGiamDoc,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
			[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
			[NgayKhaiBao] = @NgayKhaiBao,
			[TenNguoiKhai] = @TenNguoiKhai,
			[TenDonViCapPhep] = @TenDonViCapPhep,
			[TenPhuongTienVanChuyen] = @TenPhuongTienVanChuyen,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SEA]
		(
			[MaNguoiKhai],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[LoaiHinhXNK],
			[MaDV_CapPhep],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[QuyetDinhSo],
			[SoGiayChungNhan],
			[NoiCap],
			[NgayCap],
			[MaBuuChinh],
			[DiaChi],
			[MaQuocGia],
			[SoDienThoai],
			[SoFax],
			[Email],
			[SoHopDongMua],
			[NgayHopDongMua],
			[SoHopDongBan],
			[NgayHopDongBan],
			[PhuongTienVanChuyen],
			[MaCuaKhauXuatNhap],
			[TenCuaKhauXuatNhap],
			[NgayBatDau],
			[NgayKetThuc],
			[HoSoLienQuan],
			[GhiChu],
			[TenGiamDoc],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[GhiChuDanhChoCoQuanCapPhep],
			[DaiDienDonViCapPhep],
			[NgayKhaiBao],
			[TenNguoiKhai],
			[TenDonViCapPhep],
			[TenPhuongTienVanChuyen],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@LoaiHinhXNK,
			@MaDV_CapPhep,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@QuyetDinhSo,
			@SoGiayChungNhan,
			@NoiCap,
			@NgayCap,
			@MaBuuChinh,
			@DiaChi,
			@MaQuocGia,
			@SoDienThoai,
			@SoFax,
			@Email,
			@SoHopDongMua,
			@NgayHopDongMua,
			@SoHopDongBan,
			@NgayHopDongBan,
			@PhuongTienVanChuyen,
			@MaCuaKhauXuatNhap,
			@TenCuaKhauXuatNhap,
			@NgayBatDau,
			@NgayKetThuc,
			@HoSoLienQuan,
			@GhiChu,
			@TenGiamDoc,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@GhiChuDanhChoCoQuanCapPhep,
			@DaiDienDonViCapPhep,
			@NgayKhaiBao,
			@TenNguoiKhai,
			@TenDonViCapPhep,
			@TenPhuongTienVanChuyen,
			@TrangThaiXuLy
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[LoaiHinhXNK],
	[MaDV_CapPhep],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[QuyetDinhSo],
	[SoGiayChungNhan],
	[NoiCap],
	[NgayCap],
	[MaBuuChinh],
	[DiaChi],
	[MaQuocGia],
	[SoDienThoai],
	[SoFax],
	[Email],
	[SoHopDongMua],
	[NgayHopDongMua],
	[SoHopDongBan],
	[NgayHopDongBan],
	[PhuongTienVanChuyen],
	[MaCuaKhauXuatNhap],
	[TenCuaKhauXuatNhap],
	[NgayBatDau],
	[NgayKetThuc],
	[HoSoLienQuan],
	[GhiChu],
	[TenGiamDoc],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienDonViCapPhep],
	[NgayKhaiBao],
	[TenNguoiKhai],
	[TenDonViCapPhep],
	[TenPhuongTienVanChuyen],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SEA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SEA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@LoaiHinhXNK varchar(1),
	@MaDV_CapPhep varchar(6),
	@MaDoanhNghiep varchar(13),
	@TenDoanhNghiep nvarchar(300),
	@QuyetDinhSo varchar(17),
	@SoGiayChungNhan varchar(17),
	@NoiCap nvarchar(35),
	@NgayCap datetime,
	@MaBuuChinh varchar(7),
	@DiaChi varchar(300),
	@MaQuocGia varchar(2),
	@SoDienThoai varchar(20),
	@SoFax varchar(20),
	@Email varchar(210),
	@SoHopDongMua varchar(17),
	@NgayHopDongMua datetime,
	@SoHopDongBan varchar(17),
	@NgayHopDongBan datetime,
	@PhuongTienVanChuyen varchar(2),
	@MaCuaKhauXuatNhap varchar(6),
	@TenCuaKhauXuatNhap varchar(35),
	@NgayBatDau datetime,
	@NgayKetThuc datetime,
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TenGiamDoc nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(50),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(996),
	@DaiDienDonViCapPhep nvarchar(996),
	@NgayKhaiBao datetime,
	@TenNguoiKhai nvarchar(300),
	@TenDonViCapPhep nvarchar(300),
	@TenPhuongTienVanChuyen nvarchar(75),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SEA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[LoaiHinhXNK] = @LoaiHinhXNK,
	[MaDV_CapPhep] = @MaDV_CapPhep,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[QuyetDinhSo] = @QuyetDinhSo,
	[SoGiayChungNhan] = @SoGiayChungNhan,
	[NoiCap] = @NoiCap,
	[NgayCap] = @NgayCap,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChi] = @DiaChi,
	[MaQuocGia] = @MaQuocGia,
	[SoDienThoai] = @SoDienThoai,
	[SoFax] = @SoFax,
	[Email] = @Email,
	[SoHopDongMua] = @SoHopDongMua,
	[NgayHopDongMua] = @NgayHopDongMua,
	[SoHopDongBan] = @SoHopDongBan,
	[NgayHopDongBan] = @NgayHopDongBan,
	[PhuongTienVanChuyen] = @PhuongTienVanChuyen,
	[MaCuaKhauXuatNhap] = @MaCuaKhauXuatNhap,
	[TenCuaKhauXuatNhap] = @TenCuaKhauXuatNhap,
	[NgayBatDau] = @NgayBatDau,
	[NgayKetThuc] = @NgayKetThuc,
	[HoSoLienQuan] = @HoSoLienQuan,
	[GhiChu] = @GhiChu,
	[TenGiamDoc] = @TenGiamDoc,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
	[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
	[NgayKhaiBao] = @NgayKhaiBao,
	[TenNguoiKhai] = @TenNguoiKhai,
	[TenDonViCapPhep] = @TenDonViCapPhep,
	[TenPhuongTienVanChuyen] = @TenPhuongTienVanChuyen,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SFA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]    Script Date: 11/11/2013 17:34:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(210),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@SoHopDong nvarchar(35),
	@SoVanDon varchar(35),
	@MaBenDi varchar(6),
	@TenBenDi varchar(35),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@SoNhaTenDuongNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(70),
	@MaBenDen varchar(6),
	@TenBenDen varchar(35),
	@ThoiGianNhapKhauDuKien datetime,
	@GiaTriHangHoa numeric(19, 2),
	@DonViTienTe varchar(3),
	@DiaDiemTapKetHangHoa nvarchar(70),
	@ThoiGianKiemTra datetime,
	@DiaDiemKiemTra nvarchar(70),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@DaiDienThuongNhanNhapKhau nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@DaiDienCoQuanKiemTra nvarchar(500),
	@NgayKhaiBao datetime,
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SFA]
(
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
)
VALUES 
(
	@MaNguoiKhai,
	@TenNguoiKhai,
	@MaDonViCapPhep,
	@TenDonViCapPhep,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@MaThuongNhanXuatKhau,
	@TenThuongNhanXuatKhau,
	@MaBuuChinhXuatKhau,
	@SoNhaTenDuongXuatKhau,
	@PhuongXaXuatKhau,
	@QuanHuyenXuatKhau,
	@TinhThanhPhoXuatKhau,
	@MaQuocGiaXuatKhau,
	@SoDienThoaiXuatKhau,
	@SoFaxXuatKhau,
	@EmailXuatKhau,
	@SoHopDong,
	@SoVanDon,
	@MaBenDi,
	@TenBenDi,
	@MaThuongNhanNhapKhau,
	@TenThuongNhanNhapKhau,
	@MaBuuChinhNhapKhau,
	@SoNhaTenDuongNhapKhau,
	@MaQuocGiaNhapKhau,
	@SoDienThoaiNhapKhau,
	@SoFaxNhapKhau,
	@EmailNhapKhau,
	@MaBenDen,
	@TenBenDen,
	@ThoiGianNhapKhauDuKien,
	@GiaTriHangHoa,
	@DonViTienTe,
	@DiaDiemTapKetHangHoa,
	@ThoiGianKiemTra,
	@DiaDiemKiemTra,
	@HoSoLienQuan,
	@GhiChu,
	@DaiDienThuongNhanNhapKhau,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@GhiChuDanhChoCoQuanCapPhep,
	@DaiDienCoQuanKiemTra,
	@NgayKhaiBao,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(210),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@SoHopDong nvarchar(35),
	@SoVanDon varchar(35),
	@MaBenDi varchar(6),
	@TenBenDi varchar(35),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@SoNhaTenDuongNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(70),
	@MaBenDen varchar(6),
	@TenBenDen varchar(35),
	@ThoiGianNhapKhauDuKien datetime,
	@GiaTriHangHoa numeric(19, 2),
	@DonViTienTe varchar(3),
	@DiaDiemTapKetHangHoa nvarchar(70),
	@ThoiGianKiemTra datetime,
	@DiaDiemKiemTra nvarchar(70),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@DaiDienThuongNhanNhapKhau nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@DaiDienCoQuanKiemTra nvarchar(500),
	@NgayKhaiBao datetime,
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SFA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SFA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[MaDonViCapPhep] = @MaDonViCapPhep,
			[TenDonViCapPhep] = @TenDonViCapPhep,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
			[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
			[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
			[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
			[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
			[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
			[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
			[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
			[SoDienThoaiXuatKhau] = @SoDienThoaiXuatKhau,
			[SoFaxXuatKhau] = @SoFaxXuatKhau,
			[EmailXuatKhau] = @EmailXuatKhau,
			[SoHopDong] = @SoHopDong,
			[SoVanDon] = @SoVanDon,
			[MaBenDi] = @MaBenDi,
			[TenBenDi] = @TenBenDi,
			[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
			[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
			[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
			[SoNhaTenDuongNhapKhau] = @SoNhaTenDuongNhapKhau,
			[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
			[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
			[SoFaxNhapKhau] = @SoFaxNhapKhau,
			[EmailNhapKhau] = @EmailNhapKhau,
			[MaBenDen] = @MaBenDen,
			[TenBenDen] = @TenBenDen,
			[ThoiGianNhapKhauDuKien] = @ThoiGianNhapKhauDuKien,
			[GiaTriHangHoa] = @GiaTriHangHoa,
			[DonViTienTe] = @DonViTienTe,
			[DiaDiemTapKetHangHoa] = @DiaDiemTapKetHangHoa,
			[ThoiGianKiemTra] = @ThoiGianKiemTra,
			[DiaDiemKiemTra] = @DiaDiemKiemTra,
			[HoSoLienQuan] = @HoSoLienQuan,
			[GhiChu] = @GhiChu,
			[DaiDienThuongNhanNhapKhau] = @DaiDienThuongNhanNhapKhau,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
			[DaiDienCoQuanKiemTra] = @DaiDienCoQuanKiemTra,
			[NgayKhaiBao] = @NgayKhaiBao,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SFA]
		(
			[MaNguoiKhai],
			[TenNguoiKhai],
			[MaDonViCapPhep],
			[TenDonViCapPhep],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[MaThuongNhanXuatKhau],
			[TenThuongNhanXuatKhau],
			[MaBuuChinhXuatKhau],
			[SoNhaTenDuongXuatKhau],
			[PhuongXaXuatKhau],
			[QuanHuyenXuatKhau],
			[TinhThanhPhoXuatKhau],
			[MaQuocGiaXuatKhau],
			[SoDienThoaiXuatKhau],
			[SoFaxXuatKhau],
			[EmailXuatKhau],
			[SoHopDong],
			[SoVanDon],
			[MaBenDi],
			[TenBenDi],
			[MaThuongNhanNhapKhau],
			[TenThuongNhanNhapKhau],
			[MaBuuChinhNhapKhau],
			[SoNhaTenDuongNhapKhau],
			[MaQuocGiaNhapKhau],
			[SoDienThoaiNhapKhau],
			[SoFaxNhapKhau],
			[EmailNhapKhau],
			[MaBenDen],
			[TenBenDen],
			[ThoiGianNhapKhauDuKien],
			[GiaTriHangHoa],
			[DonViTienTe],
			[DiaDiemTapKetHangHoa],
			[ThoiGianKiemTra],
			[DiaDiemKiemTra],
			[HoSoLienQuan],
			[GhiChu],
			[DaiDienThuongNhanNhapKhau],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[GhiChuDanhChoCoQuanCapPhep],
			[DaiDienCoQuanKiemTra],
			[NgayKhaiBao],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@TenNguoiKhai,
			@MaDonViCapPhep,
			@TenDonViCapPhep,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@MaThuongNhanXuatKhau,
			@TenThuongNhanXuatKhau,
			@MaBuuChinhXuatKhau,
			@SoNhaTenDuongXuatKhau,
			@PhuongXaXuatKhau,
			@QuanHuyenXuatKhau,
			@TinhThanhPhoXuatKhau,
			@MaQuocGiaXuatKhau,
			@SoDienThoaiXuatKhau,
			@SoFaxXuatKhau,
			@EmailXuatKhau,
			@SoHopDong,
			@SoVanDon,
			@MaBenDi,
			@TenBenDi,
			@MaThuongNhanNhapKhau,
			@TenThuongNhanNhapKhau,
			@MaBuuChinhNhapKhau,
			@SoNhaTenDuongNhapKhau,
			@MaQuocGiaNhapKhau,
			@SoDienThoaiNhapKhau,
			@SoFaxNhapKhau,
			@EmailNhapKhau,
			@MaBenDen,
			@TenBenDen,
			@ThoiGianNhapKhauDuKien,
			@GiaTriHangHoa,
			@DonViTienTe,
			@DiaDiemTapKetHangHoa,
			@ThoiGianKiemTra,
			@DiaDiemKiemTra,
			@HoSoLienQuan,
			@GhiChu,
			@DaiDienThuongNhanNhapKhau,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@GhiChuDanhChoCoQuanCapPhep,
			@DaiDienCoQuanKiemTra,
			@NgayKhaiBao,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@TrangThaiXuLy
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[SoDienThoaiXuatKhau],
	[SoFaxXuatKhau],
	[EmailXuatKhau],
	[SoHopDong],
	[SoVanDon],
	[MaBenDi],
	[TenBenDi],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[SoNhaTenDuongNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[MaBenDen],
	[TenBenDen],
	[ThoiGianNhapKhauDuKien],
	[GiaTriHangHoa],
	[DonViTienTe],
	[DiaDiemTapKetHangHoa],
	[ThoiGianKiemTra],
	[DiaDiemKiemTra],
	[HoSoLienQuan],
	[GhiChu],
	[DaiDienThuongNhanNhapKhau],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[GhiChuDanhChoCoQuanCapPhep],
	[DaiDienCoQuanKiemTra],
	[NgayKhaiBao],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SFA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(210),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@SoDienThoaiXuatKhau varchar(20),
	@SoFaxXuatKhau varchar(20),
	@EmailXuatKhau varchar(210),
	@SoHopDong nvarchar(35),
	@SoVanDon varchar(35),
	@MaBenDi varchar(6),
	@TenBenDi varchar(35),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@SoNhaTenDuongNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(70),
	@MaBenDen varchar(6),
	@TenBenDen varchar(35),
	@ThoiGianNhapKhauDuKien datetime,
	@GiaTriHangHoa numeric(19, 2),
	@DonViTienTe varchar(3),
	@DiaDiemTapKetHangHoa nvarchar(70),
	@ThoiGianKiemTra datetime,
	@DiaDiemKiemTra nvarchar(70),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@DaiDienThuongNhanNhapKhau nvarchar(300),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@DaiDienCoQuanKiemTra nvarchar(500),
	@NgayKhaiBao datetime,
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SFA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[MaDonViCapPhep] = @MaDonViCapPhep,
	[TenDonViCapPhep] = @TenDonViCapPhep,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
	[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
	[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
	[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
	[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
	[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
	[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
	[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
	[SoDienThoaiXuatKhau] = @SoDienThoaiXuatKhau,
	[SoFaxXuatKhau] = @SoFaxXuatKhau,
	[EmailXuatKhau] = @EmailXuatKhau,
	[SoHopDong] = @SoHopDong,
	[SoVanDon] = @SoVanDon,
	[MaBenDi] = @MaBenDi,
	[TenBenDi] = @TenBenDi,
	[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
	[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
	[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
	[SoNhaTenDuongNhapKhau] = @SoNhaTenDuongNhapKhau,
	[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
	[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
	[SoFaxNhapKhau] = @SoFaxNhapKhau,
	[EmailNhapKhau] = @EmailNhapKhau,
	[MaBenDen] = @MaBenDen,
	[TenBenDen] = @TenBenDen,
	[ThoiGianNhapKhauDuKien] = @ThoiGianNhapKhauDuKien,
	[GiaTriHangHoa] = @GiaTriHangHoa,
	[DonViTienTe] = @DonViTienTe,
	[DiaDiemTapKetHangHoa] = @DiaDiemTapKetHangHoa,
	[ThoiGianKiemTra] = @ThoiGianKiemTra,
	[DiaDiemKiemTra] = @DiaDiemKiemTra,
	[HoSoLienQuan] = @HoSoLienQuan,
	[GhiChu] = @GhiChu,
	[DaiDienThuongNhanNhapKhau] = @DaiDienThuongNhanNhapKhau,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
	[DaiDienCoQuanKiemTra] = @DaiDienCoQuanKiemTra,
	[NgayKhaiBao] = @NgayKhaiBao,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SMA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Insert]
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@KetQuaXuLy numeric(1, 0),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@SoCuaGiayPhepLuuHanh varchar(17),
	@SoGiayPhepThucHanh varchar(17),
	@MaCuaKhauNhapDuKien varchar(6),
	@TenCuaKhauNhapDuKien nvarchar(35),
	@TonKhoDenNgay datetime,
	@TenGiamDocDoanhNghiep nvarchar(300),
	@DaiDienDonViCapPhep nvarchar(300),
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@NgayKhaiBao datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TrangThaiXuLy int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SMA]
(
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
)
VALUES 
(
	@MaNguoiKhai,
	@TenNguoiKhai,
	@MaBuuChinhNguoiKhai,
	@DiaChiNguoiKhai,
	@MaNuocNguoiKhai,
	@SoDienThoaiNguoiKhai,
	@SoFaxNguoiKhai,
	@EmailNguoiKhai,
	@SoDonXinCapPhep,
	@ChucNangChungTu,
	@LoaiGiayPhep,
	@MaDonViCapPhep,
	@TenDonViCapPhep,
	@MaThuongNhanNhapKhau,
	@TenThuongNhanNhapKhau,
	@MaBuuChinhNhapKhau,
	@DiaChiThuongNhanNhapKhau,
	@MaQuocGiaNhapKhau,
	@SoDienThoaiNhapKhau,
	@SoFaxNhapKhau,
	@EmailNhapKhau,
	@KetQuaXuLy,
	@MaKetQuaXuLy,
	@KetQuaXuLySoGiayPhep,
	@KetQuaXuLyNgayCap,
	@KetQuaXuLyHieuLucTuNgay,
	@KetQuaXuLyHieuLucDenNgay,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag,
	@HoSoKemTheo_1,
	@HoSoKemTheo_2,
	@HoSoKemTheo_3,
	@HoSoKemTheo_4,
	@HoSoKemTheo_5,
	@HoSoKemTheo_6,
	@HoSoKemTheo_7,
	@HoSoKemTheo_8,
	@HoSoKemTheo_9,
	@HoSoKemTheo_10,
	@SoCuaGiayPhepLuuHanh,
	@SoGiayPhepThucHanh,
	@MaCuaKhauNhapDuKien,
	@TenCuaKhauNhapDuKien,
	@TonKhoDenNgay,
	@TenGiamDocDoanhNghiep,
	@DaiDienDonViCapPhep,
	@GhiChuDanhChoCoQuanCapPhep,
	@NgayKhaiBao,
	@SoTiepNhan,
	@NgayTiepNhan,
	@PhanLuong,
	@HuongDan,
	@HoSoLienQuan,
	@GhiChu,
	@TrangThaiXuLy
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]    Script Date: 11/11/2013 17:34:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_InsertUpdate]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@KetQuaXuLy numeric(1, 0),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@SoCuaGiayPhepLuuHanh varchar(17),
	@SoGiayPhepThucHanh varchar(17),
	@MaCuaKhauNhapDuKien varchar(6),
	@TenCuaKhauNhapDuKien nvarchar(35),
	@TonKhoDenNgay datetime,
	@TenGiamDocDoanhNghiep nvarchar(300),
	@DaiDienDonViCapPhep nvarchar(300),
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@NgayKhaiBao datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TrangThaiXuLy int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SMA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SMA] 
		SET
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[MaBuuChinhNguoiKhai] = @MaBuuChinhNguoiKhai,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[MaNuocNguoiKhai] = @MaNuocNguoiKhai,
			[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
			[SoFaxNguoiKhai] = @SoFaxNguoiKhai,
			[EmailNguoiKhai] = @EmailNguoiKhai,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[ChucNangChungTu] = @ChucNangChungTu,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[MaDonViCapPhep] = @MaDonViCapPhep,
			[TenDonViCapPhep] = @TenDonViCapPhep,
			[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
			[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
			[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
			[DiaChiThuongNhanNhapKhau] = @DiaChiThuongNhanNhapKhau,
			[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
			[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
			[SoFaxNhapKhau] = @SoFaxNhapKhau,
			[EmailNhapKhau] = @EmailNhapKhau,
			[KetQuaXuLy] = @KetQuaXuLy,
			[MaKetQuaXuLy] = @MaKetQuaXuLy,
			[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
			[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
			[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
			[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag,
			[HoSoKemTheo_1] = @HoSoKemTheo_1,
			[HoSoKemTheo_2] = @HoSoKemTheo_2,
			[HoSoKemTheo_3] = @HoSoKemTheo_3,
			[HoSoKemTheo_4] = @HoSoKemTheo_4,
			[HoSoKemTheo_5] = @HoSoKemTheo_5,
			[HoSoKemTheo_6] = @HoSoKemTheo_6,
			[HoSoKemTheo_7] = @HoSoKemTheo_7,
			[HoSoKemTheo_8] = @HoSoKemTheo_8,
			[HoSoKemTheo_9] = @HoSoKemTheo_9,
			[HoSoKemTheo_10] = @HoSoKemTheo_10,
			[SoCuaGiayPhepLuuHanh] = @SoCuaGiayPhepLuuHanh,
			[SoGiayPhepThucHanh] = @SoGiayPhepThucHanh,
			[MaCuaKhauNhapDuKien] = @MaCuaKhauNhapDuKien,
			[TenCuaKhauNhapDuKien] = @TenCuaKhauNhapDuKien,
			[TonKhoDenNgay] = @TonKhoDenNgay,
			[TenGiamDocDoanhNghiep] = @TenGiamDocDoanhNghiep,
			[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
			[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
			[NgayKhaiBao] = @NgayKhaiBao,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[PhanLuong] = @PhanLuong,
			[HuongDan] = @HuongDan,
			[HoSoLienQuan] = @HoSoLienQuan,
			[GhiChu] = @GhiChu,
			[TrangThaiXuLy] = @TrangThaiXuLy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SMA]
		(
			[MaNguoiKhai],
			[TenNguoiKhai],
			[MaBuuChinhNguoiKhai],
			[DiaChiNguoiKhai],
			[MaNuocNguoiKhai],
			[SoDienThoaiNguoiKhai],
			[SoFaxNguoiKhai],
			[EmailNguoiKhai],
			[SoDonXinCapPhep],
			[ChucNangChungTu],
			[LoaiGiayPhep],
			[MaDonViCapPhep],
			[TenDonViCapPhep],
			[MaThuongNhanNhapKhau],
			[TenThuongNhanNhapKhau],
			[MaBuuChinhNhapKhau],
			[DiaChiThuongNhanNhapKhau],
			[MaQuocGiaNhapKhau],
			[SoDienThoaiNhapKhau],
			[SoFaxNhapKhau],
			[EmailNhapKhau],
			[KetQuaXuLy],
			[MaKetQuaXuLy],
			[KetQuaXuLySoGiayPhep],
			[KetQuaXuLyNgayCap],
			[KetQuaXuLyHieuLucTuNgay],
			[KetQuaXuLyHieuLucDenNgay],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag],
			[HoSoKemTheo_1],
			[HoSoKemTheo_2],
			[HoSoKemTheo_3],
			[HoSoKemTheo_4],
			[HoSoKemTheo_5],
			[HoSoKemTheo_6],
			[HoSoKemTheo_7],
			[HoSoKemTheo_8],
			[HoSoKemTheo_9],
			[HoSoKemTheo_10],
			[SoCuaGiayPhepLuuHanh],
			[SoGiayPhepThucHanh],
			[MaCuaKhauNhapDuKien],
			[TenCuaKhauNhapDuKien],
			[TonKhoDenNgay],
			[TenGiamDocDoanhNghiep],
			[DaiDienDonViCapPhep],
			[GhiChuDanhChoCoQuanCapPhep],
			[NgayKhaiBao],
			[SoTiepNhan],
			[NgayTiepNhan],
			[PhanLuong],
			[HuongDan],
			[HoSoLienQuan],
			[GhiChu],
			[TrangThaiXuLy]
		)
		VALUES 
		(
			@MaNguoiKhai,
			@TenNguoiKhai,
			@MaBuuChinhNguoiKhai,
			@DiaChiNguoiKhai,
			@MaNuocNguoiKhai,
			@SoDienThoaiNguoiKhai,
			@SoFaxNguoiKhai,
			@EmailNguoiKhai,
			@SoDonXinCapPhep,
			@ChucNangChungTu,
			@LoaiGiayPhep,
			@MaDonViCapPhep,
			@TenDonViCapPhep,
			@MaThuongNhanNhapKhau,
			@TenThuongNhanNhapKhau,
			@MaBuuChinhNhapKhau,
			@DiaChiThuongNhanNhapKhau,
			@MaQuocGiaNhapKhau,
			@SoDienThoaiNhapKhau,
			@SoFaxNhapKhau,
			@EmailNhapKhau,
			@KetQuaXuLy,
			@MaKetQuaXuLy,
			@KetQuaXuLySoGiayPhep,
			@KetQuaXuLyNgayCap,
			@KetQuaXuLyHieuLucTuNgay,
			@KetQuaXuLyHieuLucDenNgay,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag,
			@HoSoKemTheo_1,
			@HoSoKemTheo_2,
			@HoSoKemTheo_3,
			@HoSoKemTheo_4,
			@HoSoKemTheo_5,
			@HoSoKemTheo_6,
			@HoSoKemTheo_7,
			@HoSoKemTheo_8,
			@HoSoKemTheo_9,
			@HoSoKemTheo_10,
			@SoCuaGiayPhepLuuHanh,
			@SoGiayPhepThucHanh,
			@MaCuaKhauNhapDuKien,
			@TenCuaKhauNhapDuKien,
			@TonKhoDenNgay,
			@TenGiamDocDoanhNghiep,
			@DaiDienDonViCapPhep,
			@GhiChuDanhChoCoQuanCapPhep,
			@NgayKhaiBao,
			@SoTiepNhan,
			@NgayTiepNhan,
			@PhanLuong,
			@HuongDan,
			@HoSoLienQuan,
			@GhiChu,
			@TrangThaiXuLy
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[MaBuuChinhNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNuocNguoiKhai],
	[SoDienThoaiNguoiKhai],
	[SoFaxNguoiKhai],
	[EmailNguoiKhai],
	[SoDonXinCapPhep],
	[ChucNangChungTu],
	[LoaiGiayPhep],
	[MaDonViCapPhep],
	[TenDonViCapPhep],
	[MaThuongNhanNhapKhau],
	[TenThuongNhanNhapKhau],
	[MaBuuChinhNhapKhau],
	[DiaChiThuongNhanNhapKhau],
	[MaQuocGiaNhapKhau],
	[SoDienThoaiNhapKhau],
	[SoFaxNhapKhau],
	[EmailNhapKhau],
	[KetQuaXuLy],
	[MaKetQuaXuLy],
	[KetQuaXuLySoGiayPhep],
	[KetQuaXuLyNgayCap],
	[KetQuaXuLyHieuLucTuNgay],
	[KetQuaXuLyHieuLucDenNgay],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag],
	[HoSoKemTheo_1],
	[HoSoKemTheo_2],
	[HoSoKemTheo_3],
	[HoSoKemTheo_4],
	[HoSoKemTheo_5],
	[HoSoKemTheo_6],
	[HoSoKemTheo_7],
	[HoSoKemTheo_8],
	[HoSoKemTheo_9],
	[HoSoKemTheo_10],
	[SoCuaGiayPhepLuuHanh],
	[SoGiayPhepThucHanh],
	[MaCuaKhauNhapDuKien],
	[TenCuaKhauNhapDuKien],
	[TonKhoDenNgay],
	[TenGiamDocDoanhNghiep],
	[DaiDienDonViCapPhep],
	[GhiChuDanhChoCoQuanCapPhep],
	[NgayKhaiBao],
	[SoTiepNhan],
	[NgayTiepNhan],
	[PhanLuong],
	[HuongDan],
	[HoSoLienQuan],
	[GhiChu],
	[TrangThaiXuLy]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SMA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 25, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SMA_Update]
	@ID bigint,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@MaBuuChinhNguoiKhai varchar(7),
	@DiaChiNguoiKhai nvarchar(300),
	@MaNuocNguoiKhai varchar(2),
	@SoDienThoaiNguoiKhai varchar(20),
	@SoFaxNguoiKhai varchar(20),
	@EmailNguoiKhai varchar(210),
	@SoDonXinCapPhep numeric(12, 0),
	@ChucNangChungTu int,
	@LoaiGiayPhep varchar(4),
	@MaDonViCapPhep varchar(6),
	@TenDonViCapPhep nvarchar(300),
	@MaThuongNhanNhapKhau varchar(13),
	@TenThuongNhanNhapKhau nvarchar(300),
	@MaBuuChinhNhapKhau varchar(7),
	@DiaChiThuongNhanNhapKhau nvarchar(300),
	@MaQuocGiaNhapKhau varchar(2),
	@SoDienThoaiNhapKhau varchar(20),
	@SoFaxNhapKhau varchar(20),
	@EmailNhapKhau varchar(210),
	@KetQuaXuLy numeric(1, 0),
	@MaKetQuaXuLy varchar(75),
	@KetQuaXuLySoGiayPhep varchar(70),
	@KetQuaXuLyNgayCap datetime,
	@KetQuaXuLyHieuLucTuNgay datetime,
	@KetQuaXuLyHieuLucDenNgay datetime,
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@HoSoKemTheo_1 varchar(10),
	@HoSoKemTheo_2 varchar(10),
	@HoSoKemTheo_3 varchar(10),
	@HoSoKemTheo_4 varchar(10),
	@HoSoKemTheo_5 varchar(10),
	@HoSoKemTheo_6 varchar(10),
	@HoSoKemTheo_7 varchar(10),
	@HoSoKemTheo_8 varchar(10),
	@HoSoKemTheo_9 varchar(10),
	@HoSoKemTheo_10 varchar(10),
	@SoCuaGiayPhepLuuHanh varchar(17),
	@SoGiayPhepThucHanh varchar(17),
	@MaCuaKhauNhapDuKien varchar(6),
	@TenCuaKhauNhapDuKien nvarchar(35),
	@TonKhoDenNgay datetime,
	@TenGiamDocDoanhNghiep nvarchar(300),
	@DaiDienDonViCapPhep nvarchar(300),
	@GhiChuDanhChoCoQuanCapPhep nvarchar(500),
	@NgayKhaiBao datetime,
	@SoTiepNhan numeric(12, 0),
	@NgayTiepNhan datetime,
	@PhanLuong nvarchar(300),
	@HuongDan nvarchar(1000),
	@HoSoLienQuan nvarchar(750),
	@GhiChu nvarchar(996),
	@TrangThaiXuLy int
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SMA]
SET
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[MaBuuChinhNguoiKhai] = @MaBuuChinhNguoiKhai,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[MaNuocNguoiKhai] = @MaNuocNguoiKhai,
	[SoDienThoaiNguoiKhai] = @SoDienThoaiNguoiKhai,
	[SoFaxNguoiKhai] = @SoFaxNguoiKhai,
	[EmailNguoiKhai] = @EmailNguoiKhai,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[ChucNangChungTu] = @ChucNangChungTu,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[MaDonViCapPhep] = @MaDonViCapPhep,
	[TenDonViCapPhep] = @TenDonViCapPhep,
	[MaThuongNhanNhapKhau] = @MaThuongNhanNhapKhau,
	[TenThuongNhanNhapKhau] = @TenThuongNhanNhapKhau,
	[MaBuuChinhNhapKhau] = @MaBuuChinhNhapKhau,
	[DiaChiThuongNhanNhapKhau] = @DiaChiThuongNhanNhapKhau,
	[MaQuocGiaNhapKhau] = @MaQuocGiaNhapKhau,
	[SoDienThoaiNhapKhau] = @SoDienThoaiNhapKhau,
	[SoFaxNhapKhau] = @SoFaxNhapKhau,
	[EmailNhapKhau] = @EmailNhapKhau,
	[KetQuaXuLy] = @KetQuaXuLy,
	[MaKetQuaXuLy] = @MaKetQuaXuLy,
	[KetQuaXuLySoGiayPhep] = @KetQuaXuLySoGiayPhep,
	[KetQuaXuLyNgayCap] = @KetQuaXuLyNgayCap,
	[KetQuaXuLyHieuLucTuNgay] = @KetQuaXuLyHieuLucTuNgay,
	[KetQuaXuLyHieuLucDenNgay] = @KetQuaXuLyHieuLucDenNgay,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag,
	[HoSoKemTheo_1] = @HoSoKemTheo_1,
	[HoSoKemTheo_2] = @HoSoKemTheo_2,
	[HoSoKemTheo_3] = @HoSoKemTheo_3,
	[HoSoKemTheo_4] = @HoSoKemTheo_4,
	[HoSoKemTheo_5] = @HoSoKemTheo_5,
	[HoSoKemTheo_6] = @HoSoKemTheo_6,
	[HoSoKemTheo_7] = @HoSoKemTheo_7,
	[HoSoKemTheo_8] = @HoSoKemTheo_8,
	[HoSoKemTheo_9] = @HoSoKemTheo_9,
	[HoSoKemTheo_10] = @HoSoKemTheo_10,
	[SoCuaGiayPhepLuuHanh] = @SoCuaGiayPhepLuuHanh,
	[SoGiayPhepThucHanh] = @SoGiayPhepThucHanh,
	[MaCuaKhauNhapDuKien] = @MaCuaKhauNhapDuKien,
	[TenCuaKhauNhapDuKien] = @TenCuaKhauNhapDuKien,
	[TonKhoDenNgay] = @TonKhoDenNgay,
	[TenGiamDocDoanhNghiep] = @TenGiamDocDoanhNghiep,
	[DaiDienDonViCapPhep] = @DaiDienDonViCapPhep,
	[GhiChuDanhChoCoQuanCapPhep] = @GhiChuDanhChoCoQuanCapPhep,
	[NgayKhaiBao] = @NgayKhaiBao,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[PhanLuong] = @PhanLuong,
	[HuongDan] = @HuongDan,
	[HoSoLienQuan] = @HoSoLienQuan,
	[GhiChu] = @GhiChu,
	[TrangThaiXuLy] = @TrangThaiXuLy
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Insert]
	@GiayPhep_ID bigint,
	@TenHangCangTrungGian nvarchar(300),
	@TenCangTrungGian nvarchar(300),
	@SoLuongHangCangTrungGian numeric(8, 0),
	@DonViTinhSoLuongHangCangTrungGian varchar(3),
	@KhoiLuongHangCangTrungGian numeric(10, 3),
	@DonViTinhKhoiLuongHangCangTrungGian varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
(
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhep_ID,
	@TenHangCangTrungGian,
	@TenCangTrungGian,
	@SoLuongHangCangTrungGian,
	@DonViTinhSoLuongHangCangTrungGian,
	@KhoiLuongHangCangTrungGian,
	@DonViTinhKhoiLuongHangCangTrungGian,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenHangCangTrungGian nvarchar(300),
	@TenCangTrungGian nvarchar(300),
	@SoLuongHangCangTrungGian numeric(8, 0),
	@DonViTinhSoLuongHangCangTrungGian varchar(3),
	@KhoiLuongHangCangTrungGian numeric(10, 3),
	@DonViTinhKhoiLuongHangCangTrungGian varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[TenHangCangTrungGian] = @TenHangCangTrungGian,
			[TenCangTrungGian] = @TenCangTrungGian,
			[SoLuongHangCangTrungGian] = @SoLuongHangCangTrungGian,
			[DonViTinhSoLuongHangCangTrungGian] = @DonViTinhSoLuongHangCangTrungGian,
			[KhoiLuongHangCangTrungGian] = @KhoiLuongHangCangTrungGian,
			[DonViTinhKhoiLuongHangCangTrungGian] = @DonViTinhKhoiLuongHangCangTrungGian,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
		(
			[GiayPhep_ID],
			[TenHangCangTrungGian],
			[TenCangTrungGian],
			[SoLuongHangCangTrungGian],
			[DonViTinhSoLuongHangCangTrungGian],
			[KhoiLuongHangCangTrungGian],
			[DonViTinhKhoiLuongHangCangTrungGian],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@TenHangCangTrungGian,
			@TenCangTrungGian,
			@SoLuongHangCangTrungGian,
			@DonViTinhSoLuongHangCangTrungGian,
			@KhoiLuongHangCangTrungGian,
			@DonViTinhKhoiLuongHangCangTrungGian,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[TenHangCangTrungGian],
	[TenCangTrungGian],
	[SoLuongHangCangTrungGian],
	[DonViTinhSoLuongHangCangTrungGian],
	[KhoiLuongHangCangTrungGian],
	[DonViTinhKhoiLuongHangCangTrungGian],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_CangTrungGian_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenHangCangTrungGian nvarchar(300),
	@TenCangTrungGian nvarchar(300),
	@SoLuongHangCangTrungGian numeric(8, 0),
	@DonViTinhSoLuongHangCangTrungGian varchar(3),
	@KhoiLuongHangCangTrungGian numeric(10, 3),
	@DonViTinhKhoiLuongHangCangTrungGian varchar(3),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangGiayPhep_CangTrungGian]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[TenHangCangTrungGian] = @TenHangCangTrungGian,
	[TenCangTrungGian] = @TenCangTrungGian,
	[SoLuongHangCangTrungGian] = @SoLuongHangCangTrungGian,
	[DonViTinhSoLuongHangCangTrungGian] = @DonViTinhSoLuongHangCangTrungGian,
	[KhoiLuongHangCangTrungGian] = @KhoiLuongHangCangTrungGian,
	[DonViTinhKhoiLuongHangCangTrungGian] = @DonViTinhKhoiLuongHangCangTrungGian,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangGiayPhep]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Insert]
	@GiayPhepType varchar(3),
	@GiayPhep_ID bigint,
	@TenHangHoa nvarchar(768),
	@MaSoHangHoa varchar(12),
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@KhoiLuong numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@XuatXu varchar(2),
	@TinhBiet varchar(1),
	@Tuoi numeric(3, 0),
	@NoiSanXuat nvarchar(150),
	@QuyCachDongGoi nvarchar(50),
	@TongSoLuongNhapKhau numeric(8, 0),
	@DonViTinhTongSoLuongNhapKhau varchar(3),
	@KichCoCaThe nvarchar(10),
	@TrongLuongTinh numeric(10, 3),
	@DonViTinhTrongLuong varchar(3),
	@TrongLuongCaBi numeric(10, 3),
	@DonViTinhTrongLuongCaBi varchar(3),
	@SoLuongKiemDich numeric(8, 0),
	@DonViTinhSoLuongKiemDich varchar(3),
	@LoaiBaoBi nvarchar(100),
	@MucDichSuDung nvarchar(100),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep]
(
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhepType,
	@GiayPhep_ID,
	@TenHangHoa,
	@MaSoHangHoa,
	@SoLuong,
	@DonVitinhSoLuong,
	@KhoiLuong,
	@DonVitinhKhoiLuong,
	@XuatXu,
	@TinhBiet,
	@Tuoi,
	@NoiSanXuat,
	@QuyCachDongGoi,
	@TongSoLuongNhapKhau,
	@DonViTinhTongSoLuongNhapKhau,
	@KichCoCaThe,
	@TrongLuongTinh,
	@DonViTinhTrongLuong,
	@TrongLuongCaBi,
	@DonViTinhTrongLuongCaBi,
	@SoLuongKiemDich,
	@DonViTinhSoLuongKiemDich,
	@LoaiBaoBi,
	@MucDichSuDung,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]    Script Date: 11/11/2013 17:34:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_InsertUpdate]
	@ID bigint,
	@GiayPhepType varchar(3),
	@GiayPhep_ID bigint,
	@TenHangHoa nvarchar(768),
	@MaSoHangHoa varchar(12),
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@KhoiLuong numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@XuatXu varchar(2),
	@TinhBiet varchar(1),
	@Tuoi numeric(3, 0),
	@NoiSanXuat nvarchar(150),
	@QuyCachDongGoi nvarchar(50),
	@TongSoLuongNhapKhau numeric(8, 0),
	@DonViTinhTongSoLuongNhapKhau varchar(3),
	@KichCoCaThe nvarchar(10),
	@TrongLuongTinh numeric(10, 3),
	@DonViTinhTrongLuong varchar(3),
	@TrongLuongCaBi numeric(10, 3),
	@DonViTinhTrongLuongCaBi varchar(3),
	@SoLuongKiemDich numeric(8, 0),
	@DonViTinhSoLuongKiemDich varchar(3),
	@LoaiBaoBi nvarchar(100),
	@MucDichSuDung nvarchar(100),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangGiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangGiayPhep] 
		SET
			[GiayPhepType] = @GiayPhepType,
			[GiayPhep_ID] = @GiayPhep_ID,
			[TenHangHoa] = @TenHangHoa,
			[MaSoHangHoa] = @MaSoHangHoa,
			[SoLuong] = @SoLuong,
			[DonVitinhSoLuong] = @DonVitinhSoLuong,
			[KhoiLuong] = @KhoiLuong,
			[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
			[XuatXu] = @XuatXu,
			[TinhBiet] = @TinhBiet,
			[Tuoi] = @Tuoi,
			[NoiSanXuat] = @NoiSanXuat,
			[QuyCachDongGoi] = @QuyCachDongGoi,
			[TongSoLuongNhapKhau] = @TongSoLuongNhapKhau,
			[DonViTinhTongSoLuongNhapKhau] = @DonViTinhTongSoLuongNhapKhau,
			[KichCoCaThe] = @KichCoCaThe,
			[TrongLuongTinh] = @TrongLuongTinh,
			[DonViTinhTrongLuong] = @DonViTinhTrongLuong,
			[TrongLuongCaBi] = @TrongLuongCaBi,
			[DonViTinhTrongLuongCaBi] = @DonViTinhTrongLuongCaBi,
			[SoLuongKiemDich] = @SoLuongKiemDich,
			[DonViTinhSoLuongKiemDich] = @DonViTinhSoLuongKiemDich,
			[LoaiBaoBi] = @LoaiBaoBi,
			[MucDichSuDung] = @MucDichSuDung,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep]
		(
			[GiayPhepType],
			[GiayPhep_ID],
			[TenHangHoa],
			[MaSoHangHoa],
			[SoLuong],
			[DonVitinhSoLuong],
			[KhoiLuong],
			[DonVitinhKhoiLuong],
			[XuatXu],
			[TinhBiet],
			[Tuoi],
			[NoiSanXuat],
			[QuyCachDongGoi],
			[TongSoLuongNhapKhau],
			[DonViTinhTongSoLuongNhapKhau],
			[KichCoCaThe],
			[TrongLuongTinh],
			[DonViTinhTrongLuong],
			[TrongLuongCaBi],
			[DonViTinhTrongLuongCaBi],
			[SoLuongKiemDich],
			[DonViTinhSoLuongKiemDich],
			[LoaiBaoBi],
			[MucDichSuDung],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhepType,
			@GiayPhep_ID,
			@TenHangHoa,
			@MaSoHangHoa,
			@SoLuong,
			@DonVitinhSoLuong,
			@KhoiLuong,
			@DonVitinhKhoiLuong,
			@XuatXu,
			@TinhBiet,
			@Tuoi,
			@NoiSanXuat,
			@QuyCachDongGoi,
			@TongSoLuongNhapKhau,
			@DonViTinhTongSoLuongNhapKhau,
			@KichCoCaThe,
			@TrongLuongTinh,
			@DonViTinhTrongLuong,
			@TrongLuongCaBi,
			@DonViTinhTrongLuongCaBi,
			@SoLuongKiemDich,
			@DonViTinhSoLuongKiemDich,
			@LoaiBaoBi,
			@MucDichSuDung,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Load]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhepType],
	[GiayPhep_ID],
	[TenHangHoa],
	[MaSoHangHoa],
	[SoLuong],
	[DonVitinhSoLuong],
	[KhoiLuong],
	[DonVitinhKhoiLuong],
	[XuatXu],
	[TinhBiet],
	[Tuoi],
	[NoiSanXuat],
	[QuyCachDongGoi],
	[TongSoLuongNhapKhau],
	[DonViTinhTongSoLuongNhapKhau],
	[KichCoCaThe],
	[TrongLuongTinh],
	[DonViTinhTrongLuong],
	[TrongLuongCaBi],
	[DonViTinhTrongLuongCaBi],
	[SoLuongKiemDich],
	[DonViTinhSoLuongKiemDich],
	[LoaiBaoBi],
	[MucDichSuDung],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangGiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Insert]
	@GiayPhep_ID bigint,
	@TenThuocQuyCachDongGoi nvarchar(768),
	@MaSoHangHoa varchar(12),
	@TenHoatChatGayNghien nvarchar(70),
	@HoatChat varchar(35),
	@TieuChuanChatLuong varchar(35),
	@SoDangKy varchar(17),
	@HanDung datetime,
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@CongDung varchar(50),
	@TongSoKhoiLuongHoatChatGayNghien numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@GiaNhapKhauVND numeric(19, 3),
	@GiaBanBuonVND numeric(19, 3),
	@GiaBanLeVND numeric(19, 3),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@MaCongTySanXuat varchar(13),
	@TenCongTySanXuat nvarchar(300),
	@MaBuuChinhCongTySanXuat varchar(9),
	@SoNhaTenDuongCongTySanXuat varchar(35),
	@PhuongXaCongTySanXuat nvarchar(35),
	@QuanHuyenCongTySanXuat nvarchar(35),
	@TinhThanhPhoCongTySanXuat nvarchar(35),
	@MaQuocGiaCongTySanXuat varchar(2),
	@MaCongTyCungCap varchar(13),
	@TenCongTyCungCap nvarchar(300),
	@MaBuuChinhCongTyCungCap varchar(9),
	@SoNhaTenDuongCongTyCungCap varchar(35),
	@PhuongXaCongTyCungCap nvarchar(35),
	@QuanHuyenCongTyCungCap nvarchar(35),
	@TinhThanhPhoCongTyCungCap nvarchar(35),
	@MaQuocGiaCongTyCungCap varchar(2),
	@MaDonViUyThac varchar(13),
	@TenDonViUyThac nvarchar(300),
	@MaBuuChinhDonViUyThac varchar(9),
	@SoNhaTenDuongDonViUyThac varchar(35),
	@PhuongXaDonViUyThac nvarchar(35),
	@QuanHuyenDonViUyThac nvarchar(35),
	@TinhThanhPhoDonViUyThac nvarchar(35),
	@MaQuocGiaDonViUyThac varchar(2),
	@LuongTonKhoKyTruoc numeric(8, 0),
	@DonViTinhLuongTonKyTruoc varchar(3),
	@LuongNhapTrongKy numeric(8, 0),
	@TongSo numeric(9, 0),
	@TongSoXuatTrongKy numeric(9, 0),
	@SoLuongTonKhoDenNgay numeric(8, 0),
	@HuHao numeric(8, 0),
	@GhiChuChiTiet nvarchar(105),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
(
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhep_ID,
	@TenThuocQuyCachDongGoi,
	@MaSoHangHoa,
	@TenHoatChatGayNghien,
	@HoatChat,
	@TieuChuanChatLuong,
	@SoDangKy,
	@HanDung,
	@SoLuong,
	@DonVitinhSoLuong,
	@CongDung,
	@TongSoKhoiLuongHoatChatGayNghien,
	@DonVitinhKhoiLuong,
	@GiaNhapKhauVND,
	@GiaBanBuonVND,
	@GiaBanLeVND,
	@MaThuongNhanXuatKhau,
	@TenThuongNhanXuatKhau,
	@MaBuuChinhXuatKhau,
	@SoNhaTenDuongXuatKhau,
	@PhuongXaXuatKhau,
	@QuanHuyenXuatKhau,
	@TinhThanhPhoXuatKhau,
	@MaQuocGiaXuatKhau,
	@MaCongTySanXuat,
	@TenCongTySanXuat,
	@MaBuuChinhCongTySanXuat,
	@SoNhaTenDuongCongTySanXuat,
	@PhuongXaCongTySanXuat,
	@QuanHuyenCongTySanXuat,
	@TinhThanhPhoCongTySanXuat,
	@MaQuocGiaCongTySanXuat,
	@MaCongTyCungCap,
	@TenCongTyCungCap,
	@MaBuuChinhCongTyCungCap,
	@SoNhaTenDuongCongTyCungCap,
	@PhuongXaCongTyCungCap,
	@QuanHuyenCongTyCungCap,
	@TinhThanhPhoCongTyCungCap,
	@MaQuocGiaCongTyCungCap,
	@MaDonViUyThac,
	@TenDonViUyThac,
	@MaBuuChinhDonViUyThac,
	@SoNhaTenDuongDonViUyThac,
	@PhuongXaDonViUyThac,
	@QuanHuyenDonViUyThac,
	@TinhThanhPhoDonViUyThac,
	@MaQuocGiaDonViUyThac,
	@LuongTonKhoKyTruoc,
	@DonViTinhLuongTonKyTruoc,
	@LuongNhapTrongKy,
	@TongSo,
	@TongSoXuatTrongKy,
	@SoLuongTonKhoDenNgay,
	@HuHao,
	@GhiChuChiTiet,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenThuocQuyCachDongGoi nvarchar(768),
	@MaSoHangHoa varchar(12),
	@TenHoatChatGayNghien nvarchar(70),
	@HoatChat varchar(35),
	@TieuChuanChatLuong varchar(35),
	@SoDangKy varchar(17),
	@HanDung datetime,
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@CongDung varchar(50),
	@TongSoKhoiLuongHoatChatGayNghien numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@GiaNhapKhauVND numeric(19, 3),
	@GiaBanBuonVND numeric(19, 3),
	@GiaBanLeVND numeric(19, 3),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@MaCongTySanXuat varchar(13),
	@TenCongTySanXuat nvarchar(300),
	@MaBuuChinhCongTySanXuat varchar(9),
	@SoNhaTenDuongCongTySanXuat varchar(35),
	@PhuongXaCongTySanXuat nvarchar(35),
	@QuanHuyenCongTySanXuat nvarchar(35),
	@TinhThanhPhoCongTySanXuat nvarchar(35),
	@MaQuocGiaCongTySanXuat varchar(2),
	@MaCongTyCungCap varchar(13),
	@TenCongTyCungCap nvarchar(300),
	@MaBuuChinhCongTyCungCap varchar(9),
	@SoNhaTenDuongCongTyCungCap varchar(35),
	@PhuongXaCongTyCungCap nvarchar(35),
	@QuanHuyenCongTyCungCap nvarchar(35),
	@TinhThanhPhoCongTyCungCap nvarchar(35),
	@MaQuocGiaCongTyCungCap varchar(2),
	@MaDonViUyThac varchar(13),
	@TenDonViUyThac nvarchar(300),
	@MaBuuChinhDonViUyThac varchar(9),
	@SoNhaTenDuongDonViUyThac varchar(35),
	@PhuongXaDonViUyThac nvarchar(35),
	@QuanHuyenDonViUyThac nvarchar(35),
	@TinhThanhPhoDonViUyThac nvarchar(35),
	@MaQuocGiaDonViUyThac varchar(2),
	@LuongTonKhoKyTruoc numeric(8, 0),
	@DonViTinhLuongTonKyTruoc varchar(3),
	@LuongNhapTrongKy numeric(8, 0),
	@TongSo numeric(9, 0),
	@TongSoXuatTrongKy numeric(9, 0),
	@SoLuongTonKhoDenNgay numeric(8, 0),
	@HuHao numeric(8, 0),
	@GhiChuChiTiet nvarchar(105),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangGiayPhep_SMA] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[TenThuocQuyCachDongGoi] = @TenThuocQuyCachDongGoi,
			[MaSoHangHoa] = @MaSoHangHoa,
			[TenHoatChatGayNghien] = @TenHoatChatGayNghien,
			[HoatChat] = @HoatChat,
			[TieuChuanChatLuong] = @TieuChuanChatLuong,
			[SoDangKy] = @SoDangKy,
			[HanDung] = @HanDung,
			[SoLuong] = @SoLuong,
			[DonVitinhSoLuong] = @DonVitinhSoLuong,
			[CongDung] = @CongDung,
			[TongSoKhoiLuongHoatChatGayNghien] = @TongSoKhoiLuongHoatChatGayNghien,
			[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
			[GiaNhapKhauVND] = @GiaNhapKhauVND,
			[GiaBanBuonVND] = @GiaBanBuonVND,
			[GiaBanLeVND] = @GiaBanLeVND,
			[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
			[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
			[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
			[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
			[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
			[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
			[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
			[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
			[MaCongTySanXuat] = @MaCongTySanXuat,
			[TenCongTySanXuat] = @TenCongTySanXuat,
			[MaBuuChinhCongTySanXuat] = @MaBuuChinhCongTySanXuat,
			[SoNhaTenDuongCongTySanXuat] = @SoNhaTenDuongCongTySanXuat,
			[PhuongXaCongTySanXuat] = @PhuongXaCongTySanXuat,
			[QuanHuyenCongTySanXuat] = @QuanHuyenCongTySanXuat,
			[TinhThanhPhoCongTySanXuat] = @TinhThanhPhoCongTySanXuat,
			[MaQuocGiaCongTySanXuat] = @MaQuocGiaCongTySanXuat,
			[MaCongTyCungCap] = @MaCongTyCungCap,
			[TenCongTyCungCap] = @TenCongTyCungCap,
			[MaBuuChinhCongTyCungCap] = @MaBuuChinhCongTyCungCap,
			[SoNhaTenDuongCongTyCungCap] = @SoNhaTenDuongCongTyCungCap,
			[PhuongXaCongTyCungCap] = @PhuongXaCongTyCungCap,
			[QuanHuyenCongTyCungCap] = @QuanHuyenCongTyCungCap,
			[TinhThanhPhoCongTyCungCap] = @TinhThanhPhoCongTyCungCap,
			[MaQuocGiaCongTyCungCap] = @MaQuocGiaCongTyCungCap,
			[MaDonViUyThac] = @MaDonViUyThac,
			[TenDonViUyThac] = @TenDonViUyThac,
			[MaBuuChinhDonViUyThac] = @MaBuuChinhDonViUyThac,
			[SoNhaTenDuongDonViUyThac] = @SoNhaTenDuongDonViUyThac,
			[PhuongXaDonViUyThac] = @PhuongXaDonViUyThac,
			[QuanHuyenDonViUyThac] = @QuanHuyenDonViUyThac,
			[TinhThanhPhoDonViUyThac] = @TinhThanhPhoDonViUyThac,
			[MaQuocGiaDonViUyThac] = @MaQuocGiaDonViUyThac,
			[LuongTonKhoKyTruoc] = @LuongTonKhoKyTruoc,
			[DonViTinhLuongTonKyTruoc] = @DonViTinhLuongTonKyTruoc,
			[LuongNhapTrongKy] = @LuongNhapTrongKy,
			[TongSo] = @TongSo,
			[TongSoXuatTrongKy] = @TongSoXuatTrongKy,
			[SoLuongTonKhoDenNgay] = @SoLuongTonKhoDenNgay,
			[HuHao] = @HuHao,
			[GhiChuChiTiet] = @GhiChuChiTiet,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
		(
			[GiayPhep_ID],
			[TenThuocQuyCachDongGoi],
			[MaSoHangHoa],
			[TenHoatChatGayNghien],
			[HoatChat],
			[TieuChuanChatLuong],
			[SoDangKy],
			[HanDung],
			[SoLuong],
			[DonVitinhSoLuong],
			[CongDung],
			[TongSoKhoiLuongHoatChatGayNghien],
			[DonVitinhKhoiLuong],
			[GiaNhapKhauVND],
			[GiaBanBuonVND],
			[GiaBanLeVND],
			[MaThuongNhanXuatKhau],
			[TenThuongNhanXuatKhau],
			[MaBuuChinhXuatKhau],
			[SoNhaTenDuongXuatKhau],
			[PhuongXaXuatKhau],
			[QuanHuyenXuatKhau],
			[TinhThanhPhoXuatKhau],
			[MaQuocGiaXuatKhau],
			[MaCongTySanXuat],
			[TenCongTySanXuat],
			[MaBuuChinhCongTySanXuat],
			[SoNhaTenDuongCongTySanXuat],
			[PhuongXaCongTySanXuat],
			[QuanHuyenCongTySanXuat],
			[TinhThanhPhoCongTySanXuat],
			[MaQuocGiaCongTySanXuat],
			[MaCongTyCungCap],
			[TenCongTyCungCap],
			[MaBuuChinhCongTyCungCap],
			[SoNhaTenDuongCongTyCungCap],
			[PhuongXaCongTyCungCap],
			[QuanHuyenCongTyCungCap],
			[TinhThanhPhoCongTyCungCap],
			[MaQuocGiaCongTyCungCap],
			[MaDonViUyThac],
			[TenDonViUyThac],
			[MaBuuChinhDonViUyThac],
			[SoNhaTenDuongDonViUyThac],
			[PhuongXaDonViUyThac],
			[QuanHuyenDonViUyThac],
			[TinhThanhPhoDonViUyThac],
			[MaQuocGiaDonViUyThac],
			[LuongTonKhoKyTruoc],
			[DonViTinhLuongTonKyTruoc],
			[LuongNhapTrongKy],
			[TongSo],
			[TongSoXuatTrongKy],
			[SoLuongTonKhoDenNgay],
			[HuHao],
			[GhiChuChiTiet],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@TenThuocQuyCachDongGoi,
			@MaSoHangHoa,
			@TenHoatChatGayNghien,
			@HoatChat,
			@TieuChuanChatLuong,
			@SoDangKy,
			@HanDung,
			@SoLuong,
			@DonVitinhSoLuong,
			@CongDung,
			@TongSoKhoiLuongHoatChatGayNghien,
			@DonVitinhKhoiLuong,
			@GiaNhapKhauVND,
			@GiaBanBuonVND,
			@GiaBanLeVND,
			@MaThuongNhanXuatKhau,
			@TenThuongNhanXuatKhau,
			@MaBuuChinhXuatKhau,
			@SoNhaTenDuongXuatKhau,
			@PhuongXaXuatKhau,
			@QuanHuyenXuatKhau,
			@TinhThanhPhoXuatKhau,
			@MaQuocGiaXuatKhau,
			@MaCongTySanXuat,
			@TenCongTySanXuat,
			@MaBuuChinhCongTySanXuat,
			@SoNhaTenDuongCongTySanXuat,
			@PhuongXaCongTySanXuat,
			@QuanHuyenCongTySanXuat,
			@TinhThanhPhoCongTySanXuat,
			@MaQuocGiaCongTySanXuat,
			@MaCongTyCungCap,
			@TenCongTyCungCap,
			@MaBuuChinhCongTyCungCap,
			@SoNhaTenDuongCongTyCungCap,
			@PhuongXaCongTyCungCap,
			@QuanHuyenCongTyCungCap,
			@TinhThanhPhoCongTyCungCap,
			@MaQuocGiaCongTyCungCap,
			@MaDonViUyThac,
			@TenDonViUyThac,
			@MaBuuChinhDonViUyThac,
			@SoNhaTenDuongDonViUyThac,
			@PhuongXaDonViUyThac,
			@QuanHuyenDonViUyThac,
			@TinhThanhPhoDonViUyThac,
			@MaQuocGiaDonViUyThac,
			@LuongTonKhoKyTruoc,
			@DonViTinhLuongTonKyTruoc,
			@LuongNhapTrongKy,
			@TongSo,
			@TongSoXuatTrongKy,
			@SoLuongTonKhoDenNgay,
			@HuHao,
			@GhiChuChiTiet,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[TenThuocQuyCachDongGoi],
	[MaSoHangHoa],
	[TenHoatChatGayNghien],
	[HoatChat],
	[TieuChuanChatLuong],
	[SoDangKy],
	[HanDung],
	[SoLuong],
	[DonVitinhSoLuong],
	[CongDung],
	[TongSoKhoiLuongHoatChatGayNghien],
	[DonVitinhKhoiLuong],
	[GiaNhapKhauVND],
	[GiaBanBuonVND],
	[GiaBanLeVND],
	[MaThuongNhanXuatKhau],
	[TenThuongNhanXuatKhau],
	[MaBuuChinhXuatKhau],
	[SoNhaTenDuongXuatKhau],
	[PhuongXaXuatKhau],
	[QuanHuyenXuatKhau],
	[TinhThanhPhoXuatKhau],
	[MaQuocGiaXuatKhau],
	[MaCongTySanXuat],
	[TenCongTySanXuat],
	[MaBuuChinhCongTySanXuat],
	[SoNhaTenDuongCongTySanXuat],
	[PhuongXaCongTySanXuat],
	[QuanHuyenCongTySanXuat],
	[TinhThanhPhoCongTySanXuat],
	[MaQuocGiaCongTySanXuat],
	[MaCongTyCungCap],
	[TenCongTyCungCap],
	[MaBuuChinhCongTyCungCap],
	[SoNhaTenDuongCongTyCungCap],
	[PhuongXaCongTyCungCap],
	[QuanHuyenCongTyCungCap],
	[TinhThanhPhoCongTyCungCap],
	[MaQuocGiaCongTyCungCap],
	[MaDonViUyThac],
	[TenDonViUyThac],
	[MaBuuChinhDonViUyThac],
	[SoNhaTenDuongDonViUyThac],
	[PhuongXaDonViUyThac],
	[QuanHuyenDonViUyThac],
	[TinhThanhPhoDonViUyThac],
	[MaQuocGiaDonViUyThac],
	[LuongTonKhoKyTruoc],
	[DonViTinhLuongTonKyTruoc],
	[LuongNhapTrongKy],
	[TongSo],
	[TongSoXuatTrongKy],
	[SoLuongTonKhoDenNgay],
	[HuHao],
	[GhiChuChiTiet],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangGiayPhep_SMA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 04, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_SMA_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TenThuocQuyCachDongGoi nvarchar(768),
	@MaSoHangHoa varchar(12),
	@TenHoatChatGayNghien nvarchar(70),
	@HoatChat varchar(35),
	@TieuChuanChatLuong varchar(35),
	@SoDangKy varchar(17),
	@HanDung datetime,
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@CongDung varchar(50),
	@TongSoKhoiLuongHoatChatGayNghien numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@GiaNhapKhauVND numeric(19, 3),
	@GiaBanBuonVND numeric(19, 3),
	@GiaBanLeVND numeric(19, 3),
	@MaThuongNhanXuatKhau varchar(13),
	@TenThuongNhanXuatKhau nvarchar(300),
	@MaBuuChinhXuatKhau varchar(9),
	@SoNhaTenDuongXuatKhau varchar(35),
	@PhuongXaXuatKhau nvarchar(35),
	@QuanHuyenXuatKhau nvarchar(35),
	@TinhThanhPhoXuatKhau nvarchar(35),
	@MaQuocGiaXuatKhau varchar(2),
	@MaCongTySanXuat varchar(13),
	@TenCongTySanXuat nvarchar(300),
	@MaBuuChinhCongTySanXuat varchar(9),
	@SoNhaTenDuongCongTySanXuat varchar(35),
	@PhuongXaCongTySanXuat nvarchar(35),
	@QuanHuyenCongTySanXuat nvarchar(35),
	@TinhThanhPhoCongTySanXuat nvarchar(35),
	@MaQuocGiaCongTySanXuat varchar(2),
	@MaCongTyCungCap varchar(13),
	@TenCongTyCungCap nvarchar(300),
	@MaBuuChinhCongTyCungCap varchar(9),
	@SoNhaTenDuongCongTyCungCap varchar(35),
	@PhuongXaCongTyCungCap nvarchar(35),
	@QuanHuyenCongTyCungCap nvarchar(35),
	@TinhThanhPhoCongTyCungCap nvarchar(35),
	@MaQuocGiaCongTyCungCap varchar(2),
	@MaDonViUyThac varchar(13),
	@TenDonViUyThac nvarchar(300),
	@MaBuuChinhDonViUyThac varchar(9),
	@SoNhaTenDuongDonViUyThac varchar(35),
	@PhuongXaDonViUyThac nvarchar(35),
	@QuanHuyenDonViUyThac nvarchar(35),
	@TinhThanhPhoDonViUyThac nvarchar(35),
	@MaQuocGiaDonViUyThac varchar(2),
	@LuongTonKhoKyTruoc numeric(8, 0),
	@DonViTinhLuongTonKyTruoc varchar(3),
	@LuongNhapTrongKy numeric(8, 0),
	@TongSo numeric(9, 0),
	@TongSoXuatTrongKy numeric(9, 0),
	@SoLuongTonKhoDenNgay numeric(8, 0),
	@HuHao numeric(8, 0),
	@GhiChuChiTiet nvarchar(105),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangGiayPhep_SMA]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[TenThuocQuyCachDongGoi] = @TenThuocQuyCachDongGoi,
	[MaSoHangHoa] = @MaSoHangHoa,
	[TenHoatChatGayNghien] = @TenHoatChatGayNghien,
	[HoatChat] = @HoatChat,
	[TieuChuanChatLuong] = @TieuChuanChatLuong,
	[SoDangKy] = @SoDangKy,
	[HanDung] = @HanDung,
	[SoLuong] = @SoLuong,
	[DonVitinhSoLuong] = @DonVitinhSoLuong,
	[CongDung] = @CongDung,
	[TongSoKhoiLuongHoatChatGayNghien] = @TongSoKhoiLuongHoatChatGayNghien,
	[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
	[GiaNhapKhauVND] = @GiaNhapKhauVND,
	[GiaBanBuonVND] = @GiaBanBuonVND,
	[GiaBanLeVND] = @GiaBanLeVND,
	[MaThuongNhanXuatKhau] = @MaThuongNhanXuatKhau,
	[TenThuongNhanXuatKhau] = @TenThuongNhanXuatKhau,
	[MaBuuChinhXuatKhau] = @MaBuuChinhXuatKhau,
	[SoNhaTenDuongXuatKhau] = @SoNhaTenDuongXuatKhau,
	[PhuongXaXuatKhau] = @PhuongXaXuatKhau,
	[QuanHuyenXuatKhau] = @QuanHuyenXuatKhau,
	[TinhThanhPhoXuatKhau] = @TinhThanhPhoXuatKhau,
	[MaQuocGiaXuatKhau] = @MaQuocGiaXuatKhau,
	[MaCongTySanXuat] = @MaCongTySanXuat,
	[TenCongTySanXuat] = @TenCongTySanXuat,
	[MaBuuChinhCongTySanXuat] = @MaBuuChinhCongTySanXuat,
	[SoNhaTenDuongCongTySanXuat] = @SoNhaTenDuongCongTySanXuat,
	[PhuongXaCongTySanXuat] = @PhuongXaCongTySanXuat,
	[QuanHuyenCongTySanXuat] = @QuanHuyenCongTySanXuat,
	[TinhThanhPhoCongTySanXuat] = @TinhThanhPhoCongTySanXuat,
	[MaQuocGiaCongTySanXuat] = @MaQuocGiaCongTySanXuat,
	[MaCongTyCungCap] = @MaCongTyCungCap,
	[TenCongTyCungCap] = @TenCongTyCungCap,
	[MaBuuChinhCongTyCungCap] = @MaBuuChinhCongTyCungCap,
	[SoNhaTenDuongCongTyCungCap] = @SoNhaTenDuongCongTyCungCap,
	[PhuongXaCongTyCungCap] = @PhuongXaCongTyCungCap,
	[QuanHuyenCongTyCungCap] = @QuanHuyenCongTyCungCap,
	[TinhThanhPhoCongTyCungCap] = @TinhThanhPhoCongTyCungCap,
	[MaQuocGiaCongTyCungCap] = @MaQuocGiaCongTyCungCap,
	[MaDonViUyThac] = @MaDonViUyThac,
	[TenDonViUyThac] = @TenDonViUyThac,
	[MaBuuChinhDonViUyThac] = @MaBuuChinhDonViUyThac,
	[SoNhaTenDuongDonViUyThac] = @SoNhaTenDuongDonViUyThac,
	[PhuongXaDonViUyThac] = @PhuongXaDonViUyThac,
	[QuanHuyenDonViUyThac] = @QuanHuyenDonViUyThac,
	[TinhThanhPhoDonViUyThac] = @TinhThanhPhoDonViUyThac,
	[MaQuocGiaDonViUyThac] = @MaQuocGiaDonViUyThac,
	[LuongTonKhoKyTruoc] = @LuongTonKhoKyTruoc,
	[DonViTinhLuongTonKyTruoc] = @DonViTinhLuongTonKyTruoc,
	[LuongNhapTrongKy] = @LuongNhapTrongKy,
	[TongSo] = @TongSo,
	[TongSoXuatTrongKy] = @TongSoXuatTrongKy,
	[SoLuongTonKhoDenNgay] = @SoLuongTonKhoDenNgay,
	[HuHao] = @HuHao,
	[GhiChuChiTiet] = @GhiChuChiTiet,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangGiayPhep_Update]    Script Date: 11/11/2013 17:34:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangGiayPhep_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangGiayPhep_Update]
	@ID bigint,
	@GiayPhepType varchar(3),
	@GiayPhep_ID bigint,
	@TenHangHoa nvarchar(768),
	@MaSoHangHoa varchar(12),
	@SoLuong numeric(10, 3),
	@DonVitinhSoLuong varchar(3),
	@KhoiLuong numeric(10, 3),
	@DonVitinhKhoiLuong varchar(3),
	@XuatXu varchar(2),
	@TinhBiet varchar(1),
	@Tuoi numeric(3, 0),
	@NoiSanXuat nvarchar(150),
	@QuyCachDongGoi nvarchar(50),
	@TongSoLuongNhapKhau numeric(8, 0),
	@DonViTinhTongSoLuongNhapKhau varchar(3),
	@KichCoCaThe nvarchar(10),
	@TrongLuongTinh numeric(10, 3),
	@DonViTinhTrongLuong varchar(3),
	@TrongLuongCaBi numeric(10, 3),
	@DonViTinhTrongLuongCaBi varchar(3),
	@SoLuongKiemDich numeric(8, 0),
	@DonViTinhSoLuongKiemDich varchar(3),
	@LoaiBaoBi nvarchar(100),
	@MucDichSuDung nvarchar(100),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangGiayPhep]
SET
	[GiayPhepType] = @GiayPhepType,
	[GiayPhep_ID] = @GiayPhep_ID,
	[TenHangHoa] = @TenHangHoa,
	[MaSoHangHoa] = @MaSoHangHoa,
	[SoLuong] = @SoLuong,
	[DonVitinhSoLuong] = @DonVitinhSoLuong,
	[KhoiLuong] = @KhoiLuong,
	[DonVitinhKhoiLuong] = @DonVitinhKhoiLuong,
	[XuatXu] = @XuatXu,
	[TinhBiet] = @TinhBiet,
	[Tuoi] = @Tuoi,
	[NoiSanXuat] = @NoiSanXuat,
	[QuyCachDongGoi] = @QuyCachDongGoi,
	[TongSoLuongNhapKhau] = @TongSoLuongNhapKhau,
	[DonViTinhTongSoLuongNhapKhau] = @DonViTinhTongSoLuongNhapKhau,
	[KichCoCaThe] = @KichCoCaThe,
	[TrongLuongTinh] = @TrongLuongTinh,
	[DonViTinhTrongLuong] = @DonViTinhTrongLuong,
	[TrongLuongCaBi] = @TrongLuongCaBi,
	[DonViTinhTrongLuongCaBi] = @DonViTinhTrongLuongCaBi,
	[SoLuongKiemDich] = @SoLuongKiemDich,
	[DonViTinhSoLuongKiemDich] = @DonViTinhSoLuongKiemDich,
	[LoaiBaoBi] = @LoaiBaoBi,
	[MucDichSuDung] = @MucDichSuDung,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Delete]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangHoaDon]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangHoaDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Insert]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Insert]
	@HoaDon_ID bigint,
	@MaHang varchar(40),
	@MaSoHang varchar(12),
	@TenHang nvarchar(200),
	@MaNuocXX varchar(2),
	@TenNuocXX varchar(30),
	@SoKienHang varchar(35),
	@SoLuong1 numeric(12, 0),
	@MaDVT_SoLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@MaDVT_SoLuong2 varchar(4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTT_DonGia varchar(3),
	@MaDVT_DonGia varchar(3),
	@TriGiaHoaDon numeric(24, 4),
	@MaTT_GiaTien varchar(3),
	@LoaiKhauTru varchar(20),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(50),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangHoaDon]
(
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@HoaDon_ID,
	@MaHang,
	@MaSoHang,
	@TenHang,
	@MaNuocXX,
	@TenNuocXX,
	@SoKienHang,
	@SoLuong1,
	@MaDVT_SoLuong1,
	@SoLuong2,
	@MaDVT_SoLuong2,
	@DonGiaHoaDon,
	@MaTT_DonGia,
	@MaDVT_DonGia,
	@TriGiaHoaDon,
	@MaTT_GiaTien,
	@LoaiKhauTru,
	@SoTienKhauTru,
	@MaTT_TienKhauTru,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_InsertUpdate]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaHang varchar(40),
	@MaSoHang varchar(12),
	@TenHang nvarchar(200),
	@MaNuocXX varchar(2),
	@TenNuocXX varchar(30),
	@SoKienHang varchar(35),
	@SoLuong1 numeric(12, 0),
	@MaDVT_SoLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@MaDVT_SoLuong2 varchar(4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTT_DonGia varchar(3),
	@MaDVT_DonGia varchar(3),
	@TriGiaHoaDon numeric(24, 4),
	@MaTT_GiaTien varchar(3),
	@LoaiKhauTru varchar(20),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(50),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangHoaDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangHoaDon] 
		SET
			[HoaDon_ID] = @HoaDon_ID,
			[MaHang] = @MaHang,
			[MaSoHang] = @MaSoHang,
			[TenHang] = @TenHang,
			[MaNuocXX] = @MaNuocXX,
			[TenNuocXX] = @TenNuocXX,
			[SoKienHang] = @SoKienHang,
			[SoLuong1] = @SoLuong1,
			[MaDVT_SoLuong1] = @MaDVT_SoLuong1,
			[SoLuong2] = @SoLuong2,
			[MaDVT_SoLuong2] = @MaDVT_SoLuong2,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[MaTT_DonGia] = @MaTT_DonGia,
			[MaDVT_DonGia] = @MaDVT_DonGia,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[MaTT_GiaTien] = @MaTT_GiaTien,
			[LoaiKhauTru] = @LoaiKhauTru,
			[SoTienKhauTru] = @SoTienKhauTru,
			[MaTT_TienKhauTru] = @MaTT_TienKhauTru,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangHoaDon]
		(
			[HoaDon_ID],
			[MaHang],
			[MaSoHang],
			[TenHang],
			[MaNuocXX],
			[TenNuocXX],
			[SoKienHang],
			[SoLuong1],
			[MaDVT_SoLuong1],
			[SoLuong2],
			[MaDVT_SoLuong2],
			[DonGiaHoaDon],
			[MaTT_DonGia],
			[MaDVT_DonGia],
			[TriGiaHoaDon],
			[MaTT_GiaTien],
			[LoaiKhauTru],
			[SoTienKhauTru],
			[MaTT_TienKhauTru],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@HoaDon_ID,
			@MaHang,
			@MaSoHang,
			@TenHang,
			@MaNuocXX,
			@TenNuocXX,
			@SoKienHang,
			@SoLuong1,
			@MaDVT_SoLuong1,
			@SoLuong2,
			@MaDVT_SoLuong2,
			@DonGiaHoaDon,
			@MaTT_DonGia,
			@MaDVT_DonGia,
			@TriGiaHoaDon,
			@MaTT_GiaTien,
			@LoaiKhauTru,
			@SoTienKhauTru,
			@MaTT_TienKhauTru,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Load]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangHoaDon]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_HangHoaDon]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HoaDon_ID],
	[MaHang],
	[MaSoHang],
	[TenHang],
	[MaNuocXX],
	[TenNuocXX],
	[SoKienHang],
	[SoLuong1],
	[MaDVT_SoLuong1],
	[SoLuong2],
	[MaDVT_SoLuong2],
	[DonGiaHoaDon],
	[MaTT_DonGia],
	[MaDVT_DonGia],
	[TriGiaHoaDon],
	[MaTT_GiaTien],
	[LoaiKhauTru],
	[SoTienKhauTru],
	[MaTT_TienKhauTru],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_HangHoaDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangHoaDon_Update]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangHoaDon_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangHoaDon_Update]
	@ID bigint,
	@HoaDon_ID bigint,
	@MaHang varchar(40),
	@MaSoHang varchar(12),
	@TenHang nvarchar(200),
	@MaNuocXX varchar(2),
	@TenNuocXX varchar(30),
	@SoKienHang varchar(35),
	@SoLuong1 numeric(12, 0),
	@MaDVT_SoLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@MaDVT_SoLuong2 varchar(4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTT_DonGia varchar(3),
	@MaDVT_DonGia varchar(3),
	@TriGiaHoaDon numeric(24, 4),
	@MaTT_GiaTien varchar(3),
	@LoaiKhauTru varchar(20),
	@SoTienKhauTru numeric(13, 4),
	@MaTT_TienKhauTru varchar(50),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangHoaDon]
SET
	[HoaDon_ID] = @HoaDon_ID,
	[MaHang] = @MaHang,
	[MaSoHang] = @MaSoHang,
	[TenHang] = @TenHang,
	[MaNuocXX] = @MaNuocXX,
	[TenNuocXX] = @TenNuocXX,
	[SoKienHang] = @SoKienHang,
	[SoLuong1] = @SoLuong1,
	[MaDVT_SoLuong1] = @MaDVT_SoLuong1,
	[SoLuong2] = @SoLuong2,
	[MaDVT_SoLuong2] = @MaDVT_SoLuong2,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[MaTT_DonGia] = @MaTT_DonGia,
	[MaDVT_DonGia] = @MaDVT_DonGia,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[MaTT_GiaTien] = @MaTT_GiaTien,
	[LoaiKhauTru] = @LoaiKhauTru,
	[SoTienKhauTru] = @SoTienKhauTru,
	[MaTT_TienKhauTru] = @MaTT_TienKhauTru,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Delete]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Insert]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 0),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue numeric(12, 0),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
(
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@MaQuanLy,
	@TenHang,
	@ThueSuat,
	@ThueSuatTuyetDoi,
	@MaDVTTuyetDoi,
	@MaTTTuyetDoi,
	@NuocXuatXu,
	@SoLuong1,
	@DVTLuong1,
	@SoLuong2,
	@DVTLuong2,
	@TriGiaHoaDon,
	@DonGiaHoaDon,
	@MaTTDonGia,
	@DVTDonGia,
	@MaBieuThueNK,
	@MaHanNgach,
	@MaThueNKTheoLuong,
	@MaMienGiamThue,
	@SoTienGiamThue,
	@TriGiaTinhThue,
	@MaTTTriGiaTinhThue,
	@SoMucKhaiKhoanDC,
	@SoTTDongHangTKTNTX,
	@SoDMMienThue,
	@SoDongDMMienThue,
	@MaMienGiam,
	@SoTienMienGiam,
	@MaTTSoTienMienGiam,
	@MaVanBanPhapQuyKhac1,
	@MaVanBanPhapQuyKhac2,
	@MaVanBanPhapQuyKhac3,
	@MaVanBanPhapQuyKhac4,
	@MaVanBanPhapQuyKhac5,
	@SoDong,
	@MaPhanLoaiTaiXacNhanGia,
	@TenNoiXuatXu,
	@SoLuongTinhThue,
	@MaDVTDanhThue,
	@DonGiaTinhThue,
	@DV_SL_TrongDonGiaTinhThue,
	@MaTTDonGiaTinhThue,
	@TriGiaTinhThueS,
	@MaTTTriGiaTinhThueS,
	@MaTTSoTienMienGiam1,
	@MaPhanLoaiThueSuatThue,
	@ThueSuatThue,
	@PhanLoaiThueSuatThue,
	@SoTienThue,
	@MaTTSoTienThueXuatKhau,
	@TienLePhi_DonGia,
	@TienBaoHiem_DonGia,
	@TienLePhi_SoLuong,
	@TienLePhi_MaDVSoLuong,
	@TienBaoHiem_SoLuong,
	@TienBaoHiem_MaDVSoLuong,
	@TienLePhi_KhoanTien,
	@TienBaoHiem_KhoanTien,
	@DieuKhoanMienGiam
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]    Script Date: 11/11/2013 17:34:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 0),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 0),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 0),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(13, 4),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue numeric(12, 0),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[MaQuanLy] = @MaQuanLy,
			[TenHang] = @TenHang,
			[ThueSuat] = @ThueSuat,
			[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
			[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
			[MaTTTuyetDoi] = @MaTTTuyetDoi,
			[NuocXuatXu] = @NuocXuatXu,
			[SoLuong1] = @SoLuong1,
			[DVTLuong1] = @DVTLuong1,
			[SoLuong2] = @SoLuong2,
			[DVTLuong2] = @DVTLuong2,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[MaTTDonGia] = @MaTTDonGia,
			[DVTDonGia] = @DVTDonGia,
			[MaBieuThueNK] = @MaBieuThueNK,
			[MaHanNgach] = @MaHanNgach,
			[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
			[MaMienGiamThue] = @MaMienGiamThue,
			[SoTienGiamThue] = @SoTienGiamThue,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
			[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
			[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
			[SoDMMienThue] = @SoDMMienThue,
			[SoDongDMMienThue] = @SoDongDMMienThue,
			[MaMienGiam] = @MaMienGiam,
			[SoTienMienGiam] = @SoTienMienGiam,
			[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
			[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
			[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
			[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
			[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
			[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
			[SoDong] = @SoDong,
			[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
			[TenNoiXuatXu] = @TenNoiXuatXu,
			[SoLuongTinhThue] = @SoLuongTinhThue,
			[MaDVTDanhThue] = @MaDVTDanhThue,
			[DonGiaTinhThue] = @DonGiaTinhThue,
			[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
			[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
			[TriGiaTinhThueS] = @TriGiaTinhThueS,
			[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
			[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
			[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
			[ThueSuatThue] = @ThueSuatThue,
			[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
			[SoTienThue] = @SoTienThue,
			[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
			[TienLePhi_DonGia] = @TienLePhi_DonGia,
			[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
			[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
			[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
			[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
			[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
			[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
			[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
			[DieuKhoanMienGiam] = @DieuKhoanMienGiam
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
		(
			[TKMD_ID],
			[MaSoHang],
			[MaQuanLy],
			[TenHang],
			[ThueSuat],
			[ThueSuatTuyetDoi],
			[MaDVTTuyetDoi],
			[MaTTTuyetDoi],
			[NuocXuatXu],
			[SoLuong1],
			[DVTLuong1],
			[SoLuong2],
			[DVTLuong2],
			[TriGiaHoaDon],
			[DonGiaHoaDon],
			[MaTTDonGia],
			[DVTDonGia],
			[MaBieuThueNK],
			[MaHanNgach],
			[MaThueNKTheoLuong],
			[MaMienGiamThue],
			[SoTienGiamThue],
			[TriGiaTinhThue],
			[MaTTTriGiaTinhThue],
			[SoMucKhaiKhoanDC],
			[SoTTDongHangTKTNTX],
			[SoDMMienThue],
			[SoDongDMMienThue],
			[MaMienGiam],
			[SoTienMienGiam],
			[MaTTSoTienMienGiam],
			[MaVanBanPhapQuyKhac1],
			[MaVanBanPhapQuyKhac2],
			[MaVanBanPhapQuyKhac3],
			[MaVanBanPhapQuyKhac4],
			[MaVanBanPhapQuyKhac5],
			[SoDong],
			[MaPhanLoaiTaiXacNhanGia],
			[TenNoiXuatXu],
			[SoLuongTinhThue],
			[MaDVTDanhThue],
			[DonGiaTinhThue],
			[DV_SL_TrongDonGiaTinhThue],
			[MaTTDonGiaTinhThue],
			[TriGiaTinhThueS],
			[MaTTTriGiaTinhThueS],
			[MaTTSoTienMienGiam1],
			[MaPhanLoaiThueSuatThue],
			[ThueSuatThue],
			[PhanLoaiThueSuatThue],
			[SoTienThue],
			[MaTTSoTienThueXuatKhau],
			[TienLePhi_DonGia],
			[TienBaoHiem_DonGia],
			[TienLePhi_SoLuong],
			[TienLePhi_MaDVSoLuong],
			[TienBaoHiem_SoLuong],
			[TienBaoHiem_MaDVSoLuong],
			[TienLePhi_KhoanTien],
			[TienBaoHiem_KhoanTien],
			[DieuKhoanMienGiam]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@MaQuanLy,
			@TenHang,
			@ThueSuat,
			@ThueSuatTuyetDoi,
			@MaDVTTuyetDoi,
			@MaTTTuyetDoi,
			@NuocXuatXu,
			@SoLuong1,
			@DVTLuong1,
			@SoLuong2,
			@DVTLuong2,
			@TriGiaHoaDon,
			@DonGiaHoaDon,
			@MaTTDonGia,
			@DVTDonGia,
			@MaBieuThueNK,
			@MaHanNgach,
			@MaThueNKTheoLuong,
			@MaMienGiamThue,
			@SoTienGiamThue,
			@TriGiaTinhThue,
			@MaTTTriGiaTinhThue,
			@SoMucKhaiKhoanDC,
			@SoTTDongHangTKTNTX,
			@SoDMMienThue,
			@SoDongDMMienThue,
			@MaMienGiam,
			@SoTienMienGiam,
			@MaTTSoTienMienGiam,
			@MaVanBanPhapQuyKhac1,
			@MaVanBanPhapQuyKhac2,
			@MaVanBanPhapQuyKhac3,
			@MaVanBanPhapQuyKhac4,
			@MaVanBanPhapQuyKhac5,
			@SoDong,
			@MaPhanLoaiTaiXacNhanGia,
			@TenNoiXuatXu,
			@SoLuongTinhThue,
			@MaDVTDanhThue,
			@DonGiaTinhThue,
			@DV_SL_TrongDonGiaTinhThue,
			@MaTTDonGiaTinhThue,
			@TriGiaTinhThueS,
			@MaTTTriGiaTinhThueS,
			@MaTTSoTienMienGiam1,
			@MaPhanLoaiThueSuatThue,
			@ThueSuatThue,
			@PhanLoaiThueSuatThue,
			@SoTienThue,
			@MaTTSoTienThueXuatKhau,
			@TienLePhi_DonGia,
			@TienBaoHiem_DonGia,
			@TienLePhi_SoLuong,
			@TienLePhi_MaDVSoLuong,
			@TienBaoHiem_SoLuong,
			@TienBaoHiem_MaDVSoLuong,
			@TienLePhi_KhoanTien,
			@TienBaoHiem_KhoanTien,
			@DieuKhoanMienGiam
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteBy_TKMDBoSung_ID]
	@TKMDBoSung_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[TKMDBoSung_ID] = @TKMDBoSung_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Insert]
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(17, 0),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(17, 0),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(12, 0),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(50),
	@ThueSuatTruocKhiKhaiBoSung varchar(13),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung numeric(16, 0),
	@SoTienThueSauKhiKhaiBoSung numeric(16, 0),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
(
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
)
VALUES 
(
	@TKMDBoSung_ID,
	@SoDong,
	@SoThuTuDongHangTrenToKhaiGoc,
	@MoTaHangHoaTruocKhiKhaiBoSung,
	@MoTaHangHoaSauKhiKhaiBoSung,
	@MaSoHangHoaTruocKhiKhaiBoSung,
	@MaSoHangHoaSauKhiKhaiBoSung,
	@MaNuocXuatXuTruocKhiKhaiBoSung,
	@MaNuocXuatXuSauKhiKhaiBoSung,
	@TriGiaTinhThueTruocKhiKhaiBoSung,
	@TriGiaTinhThueSauKhiKhaiBoSung,
	@SoLuongTinhThueTruocKhiKhaiBoSung,
	@SoLuongTinhThueSauKhiKhaiBoSung,
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
	@ThueSuatTruocKhiKhaiBoSung,
	@ThueSuatSauKhiKhaiBoSung,
	@SoTienThueTruocKhiKhaiBoSung,
	@SoTienThueSauKhiKhaiBoSung,
	@HienThiMienThueTruocKhiKhaiBoSung,
	@HienThiMienThueSauKhiKhaiBoSung,
	@HienThiSoTienTangGiamThueXuatNhapKhau,
	@SoTienTangGiamThue
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_InsertUpdate]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(17, 0),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(17, 0),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(12, 0),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(50),
	@ThueSuatTruocKhiKhaiBoSung varchar(13),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung numeric(16, 0),
	@SoTienThueSauKhiKhaiBoSung numeric(16, 0),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] 
		SET
			[TKMDBoSung_ID] = @TKMDBoSung_ID,
			[SoDong] = @SoDong,
			[SoThuTuDongHangTrenToKhaiGoc] = @SoThuTuDongHangTrenToKhaiGoc,
			[MoTaHangHoaTruocKhiKhaiBoSung] = @MoTaHangHoaTruocKhiKhaiBoSung,
			[MoTaHangHoaSauKhiKhaiBoSung] = @MoTaHangHoaSauKhiKhaiBoSung,
			[MaSoHangHoaTruocKhiKhaiBoSung] = @MaSoHangHoaTruocKhiKhaiBoSung,
			[MaSoHangHoaSauKhiKhaiBoSung] = @MaSoHangHoaSauKhiKhaiBoSung,
			[MaNuocXuatXuTruocKhiKhaiBoSung] = @MaNuocXuatXuTruocKhiKhaiBoSung,
			[MaNuocXuatXuSauKhiKhaiBoSung] = @MaNuocXuatXuSauKhiKhaiBoSung,
			[TriGiaTinhThueTruocKhiKhaiBoSung] = @TriGiaTinhThueTruocKhiKhaiBoSung,
			[TriGiaTinhThueSauKhiKhaiBoSung] = @TriGiaTinhThueSauKhiKhaiBoSung,
			[SoLuongTinhThueTruocKhiKhaiBoSung] = @SoLuongTinhThueTruocKhiKhaiBoSung,
			[SoLuongTinhThueSauKhiKhaiBoSung] = @SoLuongTinhThueSauKhiKhaiBoSung,
			[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
			[ThueSuatTruocKhiKhaiBoSung] = @ThueSuatTruocKhiKhaiBoSung,
			[ThueSuatSauKhiKhaiBoSung] = @ThueSuatSauKhiKhaiBoSung,
			[SoTienThueTruocKhiKhaiBoSung] = @SoTienThueTruocKhiKhaiBoSung,
			[SoTienThueSauKhiKhaiBoSung] = @SoTienThueSauKhiKhaiBoSung,
			[HienThiMienThueTruocKhiKhaiBoSung] = @HienThiMienThueTruocKhiKhaiBoSung,
			[HienThiMienThueSauKhiKhaiBoSung] = @HienThiMienThueSauKhiKhaiBoSung,
			[HienThiSoTienTangGiamThueXuatNhapKhau] = @HienThiSoTienTangGiamThueXuatNhapKhau,
			[SoTienTangGiamThue] = @SoTienTangGiamThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
		(
			[TKMDBoSung_ID],
			[SoDong],
			[SoThuTuDongHangTrenToKhaiGoc],
			[MoTaHangHoaTruocKhiKhaiBoSung],
			[MoTaHangHoaSauKhiKhaiBoSung],
			[MaSoHangHoaTruocKhiKhaiBoSung],
			[MaSoHangHoaSauKhiKhaiBoSung],
			[MaNuocXuatXuTruocKhiKhaiBoSung],
			[MaNuocXuatXuSauKhiKhaiBoSung],
			[TriGiaTinhThueTruocKhiKhaiBoSung],
			[TriGiaTinhThueSauKhiKhaiBoSung],
			[SoLuongTinhThueTruocKhiKhaiBoSung],
			[SoLuongTinhThueSauKhiKhaiBoSung],
			[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
			[ThueSuatTruocKhiKhaiBoSung],
			[ThueSuatSauKhiKhaiBoSung],
			[SoTienThueTruocKhiKhaiBoSung],
			[SoTienThueSauKhiKhaiBoSung],
			[HienThiMienThueTruocKhiKhaiBoSung],
			[HienThiMienThueSauKhiKhaiBoSung],
			[HienThiSoTienTangGiamThueXuatNhapKhau],
			[SoTienTangGiamThue]
		)
		VALUES 
		(
			@TKMDBoSung_ID,
			@SoDong,
			@SoThuTuDongHangTrenToKhaiGoc,
			@MoTaHangHoaTruocKhiKhaiBoSung,
			@MoTaHangHoaSauKhiKhaiBoSung,
			@MaSoHangHoaTruocKhiKhaiBoSung,
			@MaSoHangHoaSauKhiKhaiBoSung,
			@MaNuocXuatXuTruocKhiKhaiBoSung,
			@MaNuocXuatXuSauKhiKhaiBoSung,
			@TriGiaTinhThueTruocKhiKhaiBoSung,
			@TriGiaTinhThueSauKhiKhaiBoSung,
			@SoLuongTinhThueTruocKhiKhaiBoSung,
			@SoLuongTinhThueSauKhiKhaiBoSung,
			@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
			@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
			@ThueSuatTruocKhiKhaiBoSung,
			@ThueSuatSauKhiKhaiBoSung,
			@SoTienThueTruocKhiKhaiBoSung,
			@SoTienThueSauKhiKhaiBoSung,
			@HienThiMienThueTruocKhiKhaiBoSung,
			@HienThiMienThueSauKhiKhaiBoSung,
			@HienThiSoTienTangGiamThueXuatNhapKhau,
			@SoTienTangGiamThue
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectBy_TKMDBoSung_ID]
	@TKMDBoSung_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
WHERE
	[TKMDBoSung_ID] = @TKMDBoSung_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMDBoSung_ID],
	[SoDong],
	[SoThuTuDongHangTrenToKhaiGoc],
	[MoTaHangHoaTruocKhiKhaiBoSung],
	[MoTaHangHoaSauKhiKhaiBoSung],
	[MaSoHangHoaTruocKhiKhaiBoSung],
	[MaSoHangHoaSauKhiKhaiBoSung],
	[MaNuocXuatXuTruocKhiKhaiBoSung],
	[MaNuocXuatXuSauKhiKhaiBoSung],
	[TriGiaTinhThueTruocKhiKhaiBoSung],
	[TriGiaTinhThueSauKhiKhaiBoSung],
	[SoLuongTinhThueTruocKhiKhaiBoSung],
	[SoLuongTinhThueSauKhiKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung],
	[ThueSuatTruocKhiKhaiBoSung],
	[ThueSuatSauKhiKhaiBoSung],
	[SoTienThueTruocKhiKhaiBoSung],
	[SoTienThueSauKhiKhaiBoSung],
	[HienThiMienThueTruocKhiKhaiBoSung],
	[HienThiMienThueSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueXuatNhapKhau],
	[SoTienTangGiamThue]
FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_Thue_Update]
	@ID bigint,
	@TKMDBoSung_ID bigint,
	@SoDong varchar(2),
	@SoThuTuDongHangTrenToKhaiGoc varchar(2),
	@MoTaHangHoaTruocKhiKhaiBoSung nvarchar(600),
	@MoTaHangHoaSauKhiKhaiBoSung nvarchar(600),
	@MaSoHangHoaTruocKhiKhaiBoSung varchar(12),
	@MaSoHangHoaSauKhiKhaiBoSung varchar(12),
	@MaNuocXuatXuTruocKhiKhaiBoSung varchar(2),
	@MaNuocXuatXuSauKhiKhaiBoSung varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSung numeric(17, 0),
	@TriGiaTinhThueSauKhiKhaiBoSung numeric(17, 0),
	@SoLuongTinhThueTruocKhiKhaiBoSung numeric(12, 0),
	@SoLuongTinhThueSauKhiKhaiBoSung numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueTruocKhaiBoSung varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung varchar(50),
	@ThueSuatTruocKhiKhaiBoSung varchar(13),
	@ThueSuatSauKhiKhaiBoSung varchar(30),
	@SoTienThueTruocKhiKhaiBoSung numeric(16, 0),
	@SoTienThueSauKhiKhaiBoSung numeric(16, 0),
	@HienThiMienThueTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueXuatNhapKhau varchar(1),
	@SoTienTangGiamThue numeric(16, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_Thue]
SET
	[TKMDBoSung_ID] = @TKMDBoSung_ID,
	[SoDong] = @SoDong,
	[SoThuTuDongHangTrenToKhaiGoc] = @SoThuTuDongHangTrenToKhaiGoc,
	[MoTaHangHoaTruocKhiKhaiBoSung] = @MoTaHangHoaTruocKhiKhaiBoSung,
	[MoTaHangHoaSauKhiKhaiBoSung] = @MoTaHangHoaSauKhiKhaiBoSung,
	[MaSoHangHoaTruocKhiKhaiBoSung] = @MaSoHangHoaTruocKhiKhaiBoSung,
	[MaSoHangHoaSauKhiKhaiBoSung] = @MaSoHangHoaSauKhiKhaiBoSung,
	[MaNuocXuatXuTruocKhiKhaiBoSung] = @MaNuocXuatXuTruocKhiKhaiBoSung,
	[MaNuocXuatXuSauKhiKhaiBoSung] = @MaNuocXuatXuSauKhiKhaiBoSung,
	[TriGiaTinhThueTruocKhiKhaiBoSung] = @TriGiaTinhThueTruocKhiKhaiBoSung,
	[TriGiaTinhThueSauKhiKhaiBoSung] = @TriGiaTinhThueSauKhiKhaiBoSung,
	[SoLuongTinhThueTruocKhiKhaiBoSung] = @SoLuongTinhThueTruocKhiKhaiBoSung,
	[SoLuongTinhThueSauKhiKhaiBoSung] = @SoLuongTinhThueSauKhiKhaiBoSung,
	[MaDonViTinhSoLuongTinhThueTruocKhaiBoSung] = @MaDonViTinhSoLuongTinhThueTruocKhaiBoSung,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSung,
	[ThueSuatTruocKhiKhaiBoSung] = @ThueSuatTruocKhiKhaiBoSung,
	[ThueSuatSauKhiKhaiBoSung] = @ThueSuatSauKhiKhaiBoSung,
	[SoTienThueTruocKhiKhaiBoSung] = @SoTienThueTruocKhiKhaiBoSung,
	[SoTienThueSauKhiKhaiBoSung] = @SoTienThueSauKhiKhaiBoSung,
	[HienThiMienThueTruocKhiKhaiBoSung] = @HienThiMienThueTruocKhiKhaiBoSung,
	[HienThiMienThueSauKhiKhaiBoSung] = @HienThiMienThueSauKhiKhaiBoSung,
	[HienThiSoTienTangGiamThueXuatNhapKhau] = @HienThiSoTienTangGiamThueXuatNhapKhau,
	[SoTienTangGiamThue] = @SoTienTangGiamThue
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteBy_HMDBoSung_ID]
	@HMDBoSung_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[HMDBoSung_ID] = @HMDBoSung_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Insert]
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(17, 0),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(12, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac numeric(16, 0),
	@SoTienThueSauKhiKhaiBoSungThuKhac numeric(16, 0),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
(
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
)
VALUES 
(
	@HMDBoSung_ID,
	@SoDong,
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
	@ThueSuatTruocKhiKhaiBoSungThuKhac,
	@ThueSuatSauKhiKhaiBoSungThuKhac,
	@SoTienThueTruocKhiKhaiBoSungThuKhac,
	@SoTienThueSauKhiKhaiBoSungThuKhac,
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
	@HienThiSoTienTangGiamThueVaThuKhac,
	@SoTienTangGiamThuKhac
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_InsertUpdate]
	@ID bigint,
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(17, 0),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(12, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac numeric(16, 0),
	@SoTienThueSauKhiKhaiBoSungThuKhac numeric(16, 0),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] 
		SET
			[HMDBoSung_ID] = @HMDBoSung_ID,
			[SoDong] = @SoDong,
			[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
			[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
			[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
			[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
			[ThueSuatTruocKhiKhaiBoSungThuKhac] = @ThueSuatTruocKhiKhaiBoSungThuKhac,
			[ThueSuatSauKhiKhaiBoSungThuKhac] = @ThueSuatSauKhiKhaiBoSungThuKhac,
			[SoTienThueTruocKhiKhaiBoSungThuKhac] = @SoTienThueTruocKhiKhaiBoSungThuKhac,
			[SoTienThueSauKhiKhaiBoSungThuKhac] = @SoTienThueSauKhiKhaiBoSungThuKhac,
			[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
			[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
			[HienThiSoTienTangGiamThueVaThuKhac] = @HienThiSoTienTangGiamThueVaThuKhac,
			[SoTienTangGiamThuKhac] = @SoTienTangGiamThuKhac
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
		(
			[HMDBoSung_ID],
			[SoDong],
			[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
			[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
			[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
			[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
			[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
			[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
			[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
			[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
			[ThueSuatTruocKhiKhaiBoSungThuKhac],
			[ThueSuatSauKhiKhaiBoSungThuKhac],
			[SoTienThueTruocKhiKhaiBoSungThuKhac],
			[SoTienThueSauKhiKhaiBoSungThuKhac],
			[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
			[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
			[HienThiSoTienTangGiamThueVaThuKhac],
			[SoTienTangGiamThuKhac]
		)
		VALUES 
		(
			@HMDBoSung_ID,
			@SoDong,
			@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
			@TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
			@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			@SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
			@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
			@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
			@MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
			@ThueSuatTruocKhiKhaiBoSungThuKhac,
			@ThueSuatSauKhiKhaiBoSungThuKhac,
			@SoTienThueTruocKhiKhaiBoSungThuKhac,
			@SoTienThueSauKhiKhaiBoSungThuKhac,
			@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
			@HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
			@HienThiSoTienTangGiamThueVaThuKhac,
			@SoTienTangGiamThuKhac
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectBy_HMDBoSung_ID]
	@HMDBoSung_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
WHERE
	[HMDBoSung_ID] = @HMDBoSung_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HMDBoSung_ID],
	[SoDong],
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac],
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac],
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac],
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac],
	[ThueSuatTruocKhiKhaiBoSungThuKhac],
	[ThueSuatSauKhiKhaiBoSungThuKhac],
	[SoTienThueTruocKhiKhaiBoSungThuKhac],
	[SoTienThueSauKhiKhaiBoSungThuKhac],
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung],
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung],
	[HienThiSoTienTangGiamThueVaThuKhac],
	[SoTienTangGiamThuKhac]
FROM [dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 17, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac_Update]
	@ID bigint,
	@HMDBoSung_ID bigint,
	@SoDong varchar(2),
	@TriGiaTinhThueTruocKhiKhaiBoSungThuKhac numeric(17, 0),
	@TriGiaTinhThueSauKhiKhaiBoSungThuKhac numeric(17, 0),
	@SoLuongTinhThueTruocKhiKhaiBoSungThuKhac numeric(12, 0),
	@SoLuongTinhThueSauKhiKhaiBoSungThuKhac numeric(12, 0),
	@MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac varchar(4),
	@MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac varchar(4),
	@MaApDungThueSuatTruocKhiKhaiBoSungThuKhac varchar(10),
	@MaApDungThueSuatSauKhiKhaiBoSungThuKhac varchar(10),
	@ThueSuatTruocKhiKhaiBoSungThuKhac varchar(25),
	@ThueSuatSauKhiKhaiBoSungThuKhac varchar(25),
	@SoTienThueTruocKhiKhaiBoSungThuKhac numeric(16, 0),
	@SoTienThueSauKhiKhaiBoSungThuKhac numeric(16, 0),
	@HienThiMienThueVaThuKhacTruocKhiKhaiBoSung varchar(1),
	@HienThiMienThueVaThuKhacSauKhiKhaiBoSung varchar(1),
	@HienThiSoTienTangGiamThueVaThuKhac varchar(1),
	@SoTienTangGiamThuKhac numeric(16, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_KhaiBoSung_ThueThuKhac]
SET
	[HMDBoSung_ID] = @HMDBoSung_ID,
	[SoDong] = @SoDong,
	[TriGiaTinhThueTruocKhiKhaiBoSungThuKhac] = @TriGiaTinhThueTruocKhiKhaiBoSungThuKhac,
	[TriGiaTinhThueSauKhiKhaiBoSungThuKhac] = @TriGiaTinhThueSauKhiKhaiBoSungThuKhac,
	[SoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @SoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	[SoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @SoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	[MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueTruocKhiKhaiBoSungThuKhac,
	[MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac] = @MaDonViTinhSoLuongTinhThueSauKhiKhaiBoSungThuKhac,
	[MaApDungThueSuatTruocKhiKhaiBoSungThuKhac] = @MaApDungThueSuatTruocKhiKhaiBoSungThuKhac,
	[MaApDungThueSuatSauKhiKhaiBoSungThuKhac] = @MaApDungThueSuatSauKhiKhaiBoSungThuKhac,
	[ThueSuatTruocKhiKhaiBoSungThuKhac] = @ThueSuatTruocKhiKhaiBoSungThuKhac,
	[ThueSuatSauKhiKhaiBoSungThuKhac] = @ThueSuatSauKhiKhaiBoSungThuKhac,
	[SoTienThueTruocKhiKhaiBoSungThuKhac] = @SoTienThueTruocKhiKhaiBoSungThuKhac,
	[SoTienThueSauKhiKhaiBoSungThuKhac] = @SoTienThueSauKhiKhaiBoSungThuKhac,
	[HienThiMienThueVaThuKhacTruocKhiKhaiBoSung] = @HienThiMienThueVaThuKhacTruocKhiKhaiBoSung,
	[HienThiMienThueVaThuKhacSauKhiKhaiBoSung] = @HienThiMienThueVaThuKhacSauKhiKhaiBoSung,
	[HienThiSoTienTangGiamThueVaThuKhac] = @HienThiSoTienTangGiamThueVaThuKhac,
	[SoTienTangGiamThuKhac] = @SoTienTangGiamThuKhac
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_Load]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]    Script Date: 11/11/2013 17:34:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]
	@Master_ID bigint,
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(12, 0),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(18, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(17, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(16, 0),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(12, 0),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(12, 0),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(16, 4),
	@TienBaoHiem_KhoanTien numeric(16, 4),
	@DieuKhoanMienGiam varchar(60),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
(
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
)
VALUES 
(
	@Master_ID,
	@SoDong,
	@MaPhanLoaiTaiXacNhanGia,
	@TenNoiXuatXu,
	@SoLuongTinhThue,
	@MaDVTDanhThue,
	@DonGiaTinhThue,
	@DV_SL_TrongDonGiaTinhThue,
	@MaTTDonGiaTinhThue,
	@TriGiaTinhThueS,
	@MaTTTriGiaTinhThueS,
	@MaTTSoTienMienGiam,
	@MaPhanLoaiThueSuatThue,
	@ThueSuatThue,
	@PhanLoaiThueSuatThue,
	@SoTienThue,
	@MaTTSoTienThueXuatKhau,
	@TienLePhi_DonGia,
	@TienBaoHiem_DonGia,
	@TienLePhi_SoLuong,
	@TienLePhi_MaDVSoLuong,
	@TienBaoHiem_SoLuong,
	@TienBaoHiem_MaDVSoLuong,
	@TienLePhi_KhoanTien,
	@TienBaoHiem_KhoanTien,
	@DieuKhoanMienGiam
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(12, 0),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(18, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(17, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(16, 0),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(12, 0),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(12, 0),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(16, 4),
	@TienBaoHiem_KhoanTien numeric(16, 4),
	@DieuKhoanMienGiam varchar(60)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] 
		SET
			[Master_ID] = @Master_ID,
			[SoDong] = @SoDong,
			[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
			[TenNoiXuatXu] = @TenNoiXuatXu,
			[SoLuongTinhThue] = @SoLuongTinhThue,
			[MaDVTDanhThue] = @MaDVTDanhThue,
			[DonGiaTinhThue] = @DonGiaTinhThue,
			[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
			[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
			[TriGiaTinhThueS] = @TriGiaTinhThueS,
			[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
			[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
			[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
			[ThueSuatThue] = @ThueSuatThue,
			[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
			[SoTienThue] = @SoTienThue,
			[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
			[TienLePhi_DonGia] = @TienLePhi_DonGia,
			[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
			[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
			[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
			[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
			[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
			[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
			[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
			[DieuKhoanMienGiam] = @DieuKhoanMienGiam
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
		(
			[Master_ID],
			[SoDong],
			[MaPhanLoaiTaiXacNhanGia],
			[TenNoiXuatXu],
			[SoLuongTinhThue],
			[MaDVTDanhThue],
			[DonGiaTinhThue],
			[DV_SL_TrongDonGiaTinhThue],
			[MaTTDonGiaTinhThue],
			[TriGiaTinhThueS],
			[MaTTTriGiaTinhThueS],
			[MaTTSoTienMienGiam],
			[MaPhanLoaiThueSuatThue],
			[ThueSuatThue],
			[PhanLoaiThueSuatThue],
			[SoTienThue],
			[MaTTSoTienThueXuatKhau],
			[TienLePhi_DonGia],
			[TienBaoHiem_DonGia],
			[TienLePhi_SoLuong],
			[TienLePhi_MaDVSoLuong],
			[TienBaoHiem_SoLuong],
			[TienBaoHiem_MaDVSoLuong],
			[TienLePhi_KhoanTien],
			[TienBaoHiem_KhoanTien],
			[DieuKhoanMienGiam]
		)
		VALUES 
		(
			@Master_ID,
			@SoDong,
			@MaPhanLoaiTaiXacNhanGia,
			@TenNoiXuatXu,
			@SoLuongTinhThue,
			@MaDVTDanhThue,
			@DonGiaTinhThue,
			@DV_SL_TrongDonGiaTinhThue,
			@MaTTDonGiaTinhThue,
			@TriGiaTinhThueS,
			@MaTTTriGiaTinhThueS,
			@MaTTSoTienMienGiam,
			@MaPhanLoaiThueSuatThue,
			@ThueSuatThue,
			@PhanLoaiThueSuatThue,
			@SoTienThue,
			@MaTTSoTienThueXuatKhau,
			@TienLePhi_DonGia,
			@TienBaoHiem_DonGia,
			@TienLePhi_SoLuong,
			@TienLePhi_MaDVSoLuong,
			@TienBaoHiem_SoLuong,
			@TienBaoHiem_MaDVSoLuong,
			@TienLePhi_KhoanTien,
			@TienBaoHiem_KhoanTien,
			@DieuKhoanMienGiam
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]    Script Date: 11/11/2013 17:34:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(12, 0),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(18, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(17, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(16, 0),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(12, 0),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(12, 0),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(16, 4),
	@TienBaoHiem_KhoanTien numeric(16, 4),
	@DieuKhoanMienGiam varchar(60)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
SET
	[Master_ID] = @Master_ID,
	[SoDong] = @SoDong,
	[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
	[TenNoiXuatXu] = @TenNoiXuatXu,
	[SoLuongTinhThue] = @SoLuongTinhThue,
	[MaDVTDanhThue] = @MaDVTDanhThue,
	[DonGiaTinhThue] = @DonGiaTinhThue,
	[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
	[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
	[TriGiaTinhThueS] = @TriGiaTinhThueS,
	[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
	[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
	[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
	[ThueSuatThue] = @ThueSuatThue,
	[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
	[SoTienThue] = @SoTienThue,
	[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
	[TienLePhi_DonGia] = @TienLePhi_DonGia,
	[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
	[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
	[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
	[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
	[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
	[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
	[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
	[DieuKhoanMienGiam] = @DieuKhoanMienGiam
WHERE
	[ID] = @ID


GO


IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '12.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('12.1', GETDATE(), N'Cap nhat store (VNACCS)')
END	

