using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long LePhi_ID { set; get; }
		public string TenSacPhi { set; get; }
		public decimal SoPhiPhaiNop { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection = new List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet>();
			while (reader.Read())
			{
				KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet entity = new KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhi_ID"))) entity.LePhi_ID = reader.GetInt64(reader.GetOrdinal("LePhi_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenSacPhi"))) entity.TenSacPhi = reader.GetString(reader.GetOrdinal("TenSacPhi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPhiPhaiNop"))) entity.SoPhiPhaiNop = reader.GetDecimal(reader.GetOrdinal("SoPhiPhaiNop"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection, long id)
        {
            foreach (KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet VALUES(@LePhi_ID, @TenSacPhi, @SoPhiPhaiNop)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet SET LePhi_ID = @LePhi_ID, TenSacPhi = @TenSacPhi, SoPhiPhaiNop = @SoPhiPhaiNop WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LePhi_ID", SqlDbType.BigInt, "LePhi_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacPhi", SqlDbType.NVarChar, "TenSacPhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, "SoPhiPhaiNop", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LePhi_ID", SqlDbType.BigInt, "LePhi_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacPhi", SqlDbType.NVarChar, "TenSacPhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, "SoPhiPhaiNop", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet VALUES(@LePhi_ID, @TenSacPhi, @SoPhiPhaiNop)";
            string update = "UPDATE t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet SET LePhi_ID = @LePhi_ID, TenSacPhi = @TenSacPhi, SoPhiPhaiNop = @SoPhiPhaiNop WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LePhi_ID", SqlDbType.BigInt, "LePhi_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenSacPhi", SqlDbType.NVarChar, "TenSacPhi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, "SoPhiPhaiNop", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LePhi_ID", SqlDbType.BigInt, "LePhi_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenSacPhi", SqlDbType.NVarChar, "TenSacPhi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, "SoPhiPhaiNop", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> SelectCollectionBy_LePhi_ID(long lePhi_ID)
		{
            IDataReader reader = SelectReaderBy_LePhi_ID(lePhi_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_LePhi_ID(long lePhi_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LePhi_ID", SqlDbType.BigInt, lePhi_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_LePhi_ID(long lePhi_ID)
		{
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_SelectBy_LePhi_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LePhi_ID", SqlDbType.BigInt, lePhi_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet(long lePhi_ID, string tenSacPhi, decimal soPhiPhaiNop)
		{
			KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet entity = new KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet();	
			entity.LePhi_ID = lePhi_ID;
			entity.TenSacPhi = tenSacPhi;
			entity.SoPhiPhaiNop = soPhiPhaiNop;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@LePhi_ID", SqlDbType.BigInt, LePhi_ID);
			db.AddInParameter(dbCommand, "@TenSacPhi", SqlDbType.NVarChar, TenSacPhi);
			db.AddInParameter(dbCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, SoPhiPhaiNop);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet(long id, long lePhi_ID, string tenSacPhi, decimal soPhiPhaiNop)
		{
			KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet entity = new KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet();			
			entity.ID = id;
			entity.LePhi_ID = lePhi_ID;
			entity.TenSacPhi = tenSacPhi;
			entity.SoPhiPhaiNop = soPhiPhaiNop;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LePhi_ID", SqlDbType.BigInt, LePhi_ID);
			db.AddInParameter(dbCommand, "@TenSacPhi", SqlDbType.NVarChar, TenSacPhi);
			db.AddInParameter(dbCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, SoPhiPhaiNop);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet(long id, long lePhi_ID, string tenSacPhi, decimal soPhiPhaiNop)
		{
			KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet entity = new KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet();			
			entity.ID = id;
			entity.LePhi_ID = lePhi_ID;
			entity.TenSacPhi = tenSacPhi;
			entity.SoPhiPhaiNop = soPhiPhaiNop;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@LePhi_ID", SqlDbType.BigInt, LePhi_ID);
			db.AddInParameter(dbCommand, "@TenSacPhi", SqlDbType.NVarChar, TenSacPhi);
			db.AddInParameter(dbCommand, "@SoPhiPhaiNop", SqlDbType.Decimal, SoPhiPhaiNop);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet(long id)
		{
			KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet entity = new KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_LePhi_ID(long lePhi_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteBy_LePhi_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@LePhi_ID", SqlDbType.BigInt, lePhi_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_ToKhaiMauDich_LePhiPhaiThuChiTiet item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}