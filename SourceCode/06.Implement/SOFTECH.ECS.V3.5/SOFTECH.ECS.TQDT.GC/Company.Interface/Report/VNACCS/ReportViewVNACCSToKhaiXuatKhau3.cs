using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using BarcodeLib;
namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewVNACCSToKhaiXuatKhau3 : DevExpress.XtraReports.UI.XtraReport
    {
        public int count;
        public ReportViewVNACCSToKhaiXuatKhau3()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindReport(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                pictureBox1.Image = GenerateBarCode(tkmd.SoToKhai.ToString());
                lblTenThongTinXuat.Text = "Tờ khai hàng hóa xuất khẩu (thông quan)";
                int HangHoa = tkmd.HangCollection.Count;
                if (HangHoa <= 2)
                {
                    lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
                }
                else
                {
                    lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
                }
                lblSotokhai.Text = tkmd.SoToKhai.ToString();
                lblSotokhaidautien.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.SoToKhaiDauTien.ToString();
                lblSonhanhtokhaichianho.Text = tkmd.SoNhanhToKhai.ToString();
                lblTongsotokhaichianho.Text = tkmd.TongSoTKChiaNho.ToString();
                lblSotokhaitamnhaptaixuat.Text = tkmd.SoToKhaiTNTX.ToString();
                lblMaphanloaikiemtra.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiKiemTra) ? "" : tkmd.MaPhanLoaiKiemTra.ToString();
                lblMaloaihinh.Text = String.IsNullOrEmpty(tkmd.MaLoaiHinh) ? "" : tkmd.MaLoaiHinh.ToString();
                lblMaphanloaihanghoa.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiHH) ? "" : tkmd.MaPhanLoaiHH.ToString();
                lblMahieuphuongthucvanchuyen.Text = String.IsNullOrEmpty(tkmd.MaPhuongThucVT) ? "" : tkmd.MaPhuongThucVT.ToString();
                lblMasothuedaidien.Text = String.IsNullOrEmpty(tkmd.MaSoThueDaiDien) ? "" : tkmd.MaSoThueDaiDien.ToString();
                lblTencoquanhaiquantiepnhantokhai.Text = String.IsNullOrEmpty(tkmd.TenCoQuanHaiQuan) ? "" : tkmd.TenCoQuanHaiQuan.ToString();
                lblNhomxulyhoso.Text = String.IsNullOrEmpty(tkmd.NhomXuLyHS) ? "" : tkmd.NhomXuLyHS.ToString();
                if
                 (tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaydangky.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgaydangky.Text = "";
                }

                if (tkmd.NgayDangKy.Hour.ToString() != "" && tkmd.NgayDangKy.Hour.ToString() != "0")
                {
                    lblGiodangky.Text = tkmd.NgayDangKy.Hour.ToString() + ":" + tkmd.NgayDangKy.Minute.ToString() + ":" + tkmd.NgayDangKy.Second.ToString();
                }
                else
                {
                    lblGiodangky.Text = "";
                }
                if
                    (tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaythaydoidangky.Text = tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy");

                }
                else
                {
                    lblNgaythaydoidangky.Text = "";
                }

                if (tkmd.NgayThayDoiDangKy.Hour.ToString() != "" && tkmd.NgayThayDoiDangKy.Hour.ToString() != "0")
                {
                    lblGiothaydoidangky.Text = tkmd.NgayThayDoiDangKy.Hour.ToString() + ":" + tkmd.NgayThayDoiDangKy.Minute.ToString() + ":" + tkmd.NgayThayDoiDangKy.Second.ToString();
                }
                else
                {
                    lblGiothaydoidangky.Text = "";
                }
                if
                    (tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblThoihantainhapxuat.Text = tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoihantainhapxuat.Text = "";
                }

                lblBieuthitruonghophethan.Text = String.IsNullOrEmpty(tkmd.BieuThiTruongHopHetHan) ? "" : tkmd.BieuThiTruongHopHetHan.ToString();

                #region bind hang
                DataTable dt = new DataTable();
                dt.Columns.Add("Sodong1", typeof(string));
                dt.Columns.Add("Masohanghoa", typeof(string));
                dt.Columns.Add("Maquanlyrieng", typeof(string));
                dt.Columns.Add("Mataixacnhangia", typeof(string));
                dt.Columns.Add("Motahanghoa", typeof(string));
                dt.Columns.Add("Soluong1", typeof(decimal));
                dt.Columns.Add("Madvtinh1", typeof(string));
                dt.Columns.Add("Soluong2", typeof(decimal));
                dt.Columns.Add("Madvtinh2", typeof(string));
                dt.Columns.Add("Trigia", typeof(decimal));
                dt.Columns.Add("Dongia", typeof(decimal));
                dt.Columns.Add("Madongtien", typeof(string));
                dt.Columns.Add("Donvidongia", typeof(string));
                dt.Columns.Add("TrigiathueS", typeof(decimal));
                dt.Columns.Add("MadongtienS", typeof(string));
                dt.Columns.Add("MadongtienM", typeof(string));
                dt.Columns.Add("TrigiathueM", typeof(decimal));
                dt.Columns.Add("Soluongthue", typeof(decimal));
                dt.Columns.Add("Madvtinhchuanthue", typeof(string));
                dt.Columns.Add("Dongiathue", typeof(decimal));
                dt.Columns.Add("Matientedongiathue", typeof(string));
                dt.Columns.Add("Donvisoluongthue", typeof(string));
                dt.Columns.Add("Thuesuat", typeof(string));
                dt.Columns.Add("Phanloainhapthue", typeof(string));
                dt.Columns.Add("Sotienthue", typeof(decimal));
                dt.Columns.Add("Matientesotienthue", typeof(string));
                dt.Columns.Add("Sotienmiengiam", typeof(decimal));
                dt.Columns.Add("Matientesotiengiamthue", typeof(string));
                dt.Columns.Add("Sodonghangtaixuat", typeof(string));
                dt.Columns.Add("Danhmucmienthue", typeof(string));
                dt.Columns.Add("Sodongmienthue", typeof(string));
                dt.Columns.Add("Tienlephi", typeof(string));
                dt.Columns.Add("Tienbaohiem", typeof(string));
                dt.Columns.Add("Soluongtienlephi", typeof(decimal));
                dt.Columns.Add("Madvtienlephi", typeof(string));
                dt.Columns.Add("Soluongtienbaohiem", typeof(decimal));
                dt.Columns.Add("Madvtienbaohiem", typeof(string));
                dt.Columns.Add("Khoantienlephi", typeof(decimal));
                dt.Columns.Add("Khoantienbaohiem", typeof(decimal));
                dt.Columns.Add("Mavanvanphapquy1", typeof(string));
                dt.Columns.Add("Mavanvanphapquy2", typeof(string));
                dt.Columns.Add("Mavanvanphapquy3", typeof(string));
                dt.Columns.Add("Mavanvanphapquy4", typeof(string));
                dt.Columns.Add("Mavanvanphapquy5", typeof(string));
                dt.Columns.Add("Mamiengiam", typeof(string));
                dt.Columns.Add("Dieukhoanmien", typeof(string));

                //int begin = spl * 2;
                //int end = spl * 2 + 2;
                //for (int i = begin; i < end; i++)
                for (int i = 0; i < tkmd.HangCollection.Count; i++)
                {
                    //if (i < Vae.HangMD.Count)
                    //{
                    DataRow dr = dt.NewRow();

                    dr["Sodong1"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDong) ? "" : tkmd.HangCollection[i].SoDong.ToString();
                    dr["Masohanghoa"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaSoHang) ? "" : tkmd.HangCollection[i].MaSoHang.ToString();
                    dr["Maquanlyrieng"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaQuanLy) ? "" : tkmd.HangCollection[i].MaQuanLy.ToString();
                    dr["Mataixacnhangia"] = "";//tkmd.HangCollection[i]..GetValue().ToString();
                    dr["Motahanghoa"] = String.IsNullOrEmpty(tkmd.HangCollection[i].TenHang) ? "" : tkmd.HangCollection[i].TenHang.ToString();
                    dr["Soluong1"] = tkmd.HangCollection[i].SoLuong1.ToString().Replace(".", ",");
                    dr["Madvtinh1"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTLuong1) ? "" : tkmd.HangCollection[i].DVTLuong1.ToString();
                    dr["Soluong2"] = tkmd.HangCollection[i].SoLuong2.ToString().Replace(".", ",");
                    dr["Madvtinh2"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTLuong2) ? "" : tkmd.HangCollection[i].DVTLuong2.ToString();
                    dr["Trigia"] = tkmd.HangCollection[i].TriGiaHoaDon.ToString().Replace(".", ",");
                    dr["Dongia"] = tkmd.HangCollection[i].DonGiaHoaDon.ToString().Replace(".", ",");
                    dr["Madongtien"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaTTDonGia) ? "" : tkmd.HangCollection[i].MaTTDonGia.ToString();
                    dr["Donvidongia"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DVTDonGia) ? "" : tkmd.HangCollection[i].DVTDonGia.ToString();
                    dr["TrigiathueS"] = tkmd.HangCollection[i].TriGiaTinhThueS.ToString().Replace(".", ",");
                    dr["MadongtienS"] = string.IsNullOrEmpty(tkmd.HangCollection[i].MaTTTriGiaTinhThueS) ? String.Empty : tkmd.HangCollection[i].MaTTTriGiaTinhThueS.ToString();
                    dr["MadongtienM"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaTTDonGia) ? "" : tkmd.HangCollection[i].MaTTDonGia.ToString();
                    dr["TrigiathueM"] = tkmd.HangCollection[i].TriGiaTinhThue.ToString().Replace(".", ",");
                    dr["Soluongthue"] = tkmd.HangCollection[i].SoLuongTinhThue.ToString().Replace(".", ",");
                    dr["Madvtinhchuanthue"] = string.IsNullOrEmpty(tkmd.HangCollection[i].MaTTTriGiaTinhThue) ? String.Empty : tkmd.HangCollection[i].MaTTTriGiaTinhThue.ToString();
                    dr["Dongiathue"] = tkmd.HangCollection[i].DonGiaTinhThue.ToString().Replace(".", ",");
                    dr["Matientedongiathue"] = string.IsNullOrEmpty(tkmd.HangCollection[i].MaTTDonGiaTinhThue) ? String.Empty : tkmd.HangCollection[i].MaTTDonGiaTinhThue.ToString();
                    dr["Donvisoluongthue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].DV_SL_TrongDonGiaTinhThue) ? "" : tkmd.HangCollection[i].DV_SL_TrongDonGiaTinhThue.ToString();
                    dr["Thuesuat"] = tkmd.HangCollection[i].ThueSuat.ToString();
                    dr["Phanloainhapthue"] = tkmd.HangCollection[i].ThueSuatTuyetDoi.ToString();
                    dr["Sotienthue"] = tkmd.HangCollection[i].SoTienThue.ToString().Replace(".", ",");
                    dr["Matientesotienthue"] = string.IsNullOrEmpty(tkmd.HangCollection[i].MaTTSoTienMienGiam) ? String.Empty : tkmd.HangCollection[i].MaTTSoTienMienGiam.ToString();
                    dr["Sotienmiengiam"] = tkmd.HangCollection[i].SoTienMienGiam.ToString().Replace(".", ",");
                    dr["Matientesotiengiamthue"] = string.IsNullOrEmpty(tkmd.HangCollection[i].MaTTSoTienMienGiam) ? String.Empty : tkmd.HangCollection[i].MaTTSoTienMienGiam.ToString();
                    dr["Sodonghangtaixuat"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoTTDongHangTKTNTX) ? "" : tkmd.HangCollection[i].SoTTDongHangTKTNTX.ToString();
                    dr["Danhmucmienthue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDMMienThue) ? "" : tkmd.HangCollection[i].SoDMMienThue.ToString();
                    dr["Sodongmienthue"] = String.IsNullOrEmpty(tkmd.HangCollection[i].SoDongDMMienThue) ? "" : tkmd.HangCollection[i].SoDongDMMienThue.ToString();
                    dr["Tienlephi"] = string.IsNullOrEmpty(tkmd.HangCollection[i].TienLePhi_DonGia) ? String.Empty : tkmd.HangCollection[i].TienLePhi_DonGia.ToString();
                    dr["Tienbaohiem"] = string.IsNullOrEmpty(tkmd.HangCollection[i].TienBaoHiem_DonGia) ? String.Empty : tkmd.HangCollection[i].TienBaoHiem_DonGia.ToString();
                    dr["Soluongtienlephi"] = tkmd.HangCollection[i].TienLePhi_SoLuong.ToString().Replace(".", ",");
                    dr["Madvtienlephi"] = string.IsNullOrEmpty(tkmd.HangCollection[i].TienLePhi_MaDVSoLuong) ? String.Empty : tkmd.HangCollection[i].TienLePhi_MaDVSoLuong.ToString();
                    dr["Soluongtienbaohiem"] = tkmd.HangCollection[i].TienBaoHiem_SoLuong.ToString().Replace(".", ",");
                    dr["Madvtienbaohiem"] = string.IsNullOrEmpty(tkmd.HangCollection[i].TienBaoHiem_MaDVSoLuong) ? String.Empty : tkmd.HangCollection[i].TienBaoHiem_MaDVSoLuong.ToString();
                    dr["Khoantienlephi"] = tkmd.HangCollection[i].TienLePhi_KhoanTien.ToString().Replace(".", ",");
                    dr["Khoantienbaohiem"] = tkmd.HangCollection[i].TienBaoHiem_KhoanTien.ToString().Replace(".", ",");
                    dr["Mavanvanphapquy1"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaVanBanPhapQuyKhac1) ? "" : tkmd.HangCollection[i].MaVanBanPhapQuyKhac1.ToString();
                    dr["Mavanvanphapquy2"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaVanBanPhapQuyKhac2) ? "" : tkmd.HangCollection[i].MaVanBanPhapQuyKhac2.ToString();
                    dr["Mavanvanphapquy3"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaVanBanPhapQuyKhac3) ? "" : tkmd.HangCollection[i].MaVanBanPhapQuyKhac3.ToString();
                    dr["Mavanvanphapquy4"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaVanBanPhapQuyKhac4) ? "" : tkmd.HangCollection[i].MaVanBanPhapQuyKhac4.ToString();
                    dr["Mavanvanphapquy5"] = String.IsNullOrEmpty(tkmd.HangCollection[i].MaVanBanPhapQuyKhac5) ? "" : tkmd.HangCollection[i].MaVanBanPhapQuyKhac5.ToString();
                    dr["Mamiengiam"] = string.IsNullOrEmpty(tkmd.HangCollection[i].MaMienGiam) ? String.Empty : tkmd.HangCollection[i].MaMienGiam.ToString();
                    dr["Dieukhoanmien"] = string.IsNullOrEmpty(tkmd.HangCollection[i].DieuKhoanMienGiam) ? String.Empty : tkmd.HangCollection[i].DieuKhoanMienGiam.ToString();
                    dt.Rows.Add(dr);
                    //}


                }
                BindReportHang(dt);
                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        public void BindReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSodong1.DataBindings.Add("Text", DetailReport.DataSource, "Sodong1");
            lblMasohanghoa.DataBindings.Add("Text", DetailReport.DataSource, "Masohanghoa");
            lblMaquanlyrieng.DataBindings.Add("Text", DetailReport.DataSource, "Maquanlyrieng");
            lblMataixacnhangia.DataBindings.Add("Text", DetailReport.DataSource, "Mataixacnhangia");
            lblMotahanghoa.DataBindings.Add("Text", DetailReport.DataSource, "Motahanghoa");
            lblSoluong1.DataBindings.Add("Text", DetailReport.DataSource, "Soluong1", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblMadvtinh1.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinh1");
            lblSoluong2.DataBindings.Add("Text", DetailReport.DataSource, "Soluong2", Company.KDT.SHARE.Components.Globals.FormatNumber(2, false));
            lblMadvtinh2.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinh2");
            lblTrigia.DataBindings.Add("Text", DetailReport.DataSource, "Trigia", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblDongia.DataBindings.Add("Text", DetailReport.DataSource, "Dongia", Company.KDT.SHARE.Components.Globals.FormatNumber(6, false));
            lblMadongtien.DataBindings.Add("Text", DetailReport.DataSource, "Madongtien");
            lblDonvidongia.DataBindings.Add("Text", DetailReport.DataSource, "Donvidongia");
            lblTrigiathueS.DataBindings.Add("Text", DetailReport.DataSource, "TrigiathueS", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadongtienS.DataBindings.Add("Text", DetailReport.DataSource, "MadongtienS");
            lblMadongtienM.DataBindings.Add("Text", DetailReport.DataSource, "MadongtienM");
            lblTrigiathueM.DataBindings.Add("Text", DetailReport.DataSource, "TrigiathueM", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblSoluongthue.DataBindings.Add("Text", DetailReport.DataSource, "Soluongthue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadvtinhchuanthue.DataBindings.Add("Text", DetailReport.DataSource, "Madvtinhchuanthue");
            lblDongiathue.DataBindings.Add("Text", DetailReport.DataSource, "Dongiathue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMatientedongiathue.DataBindings.Add("Text", DetailReport.DataSource, "Matientedongiathue");
            lblDonvisoluongthue.DataBindings.Add("Text", DetailReport.DataSource, "Donvisoluongthue");
            lblThuesuat.DataBindings.Add("Text", DetailReport.DataSource, "Thuesuat");
            lblPhanloainhapthue.DataBindings.Add("Text", DetailReport.DataSource, "Phanloainhapthue");
            lblSotienthue.DataBindings.Add("Text", DetailReport.DataSource, "Sotienthue", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMatientesotienthue.DataBindings.Add("Text", DetailReport.DataSource, "Matientesotienthue");
            lblSotienmiengiam.DataBindings.Add("Text", DetailReport.DataSource, "Sotienmiengiam", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMatientesotiengiamthue.DataBindings.Add("Text", DetailReport.DataSource, "Matientesotiengiamthue");
            lblSodonghangtaixuat.DataBindings.Add("Text", DetailReport.DataSource, "Sodonghangtaixuat");
            lblDanhmucmienthue.DataBindings.Add("Text", DetailReport.DataSource, "Danhmucmienthue");
            lblSodongmienthue.DataBindings.Add("Text", DetailReport.DataSource, "Sodongmienthue");
            lblTienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Tienlephi");
            lblTienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Tienbaohiem");
            lblSoluongtienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Soluongtienlephi", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadvtienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Madvtienlephi");
            lblSoluongtienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Soluongtienbaohiem", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblMadvtienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Madvtienbaohiem", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblKhoantienlephi.DataBindings.Add("Text", DetailReport.DataSource, "Khoantienlephi", Company.KDT.SHARE.Components.Globals.FormatNumber(4, false));
            lblKhoantienbaohiem.DataBindings.Add("Text", DetailReport.DataSource, "Khoantienbaohiem");
            lblMavanvanphapquy1.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy1");
            lblMavanvanphapquy2.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy2");
            lblMavanvanphapquy3.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy3");
            lblMavanvanphapquy4.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy4");
            lblMavanvanphapquy5.DataBindings.Add("Text", DetailReport.DataSource, "Mavanvanphapquy5");
            lblMamiengiam.DataBindings.Add("Text", DetailReport.DataSource, "Mamiengiam");
            lblDieukhoanmien.DataBindings.Add("Text", DetailReport.DataSource, "Dieukhoanmien");

        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }

        private void lblTongSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            //XRControl cell = (XRControl)sender;
            //cell.Text = (e.PageIndex + 3).ToString();
        }

        private void lblTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            int totalPages = PrintingSystem.Document.PageCount + 3;
            lblTrang.Text = totalPages.ToString();
        }

        private void lblTongSoTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //int totalPages = count + 2;
            //lblTongSoTrang.Text = totalPages.ToString();
        }
    }
}
