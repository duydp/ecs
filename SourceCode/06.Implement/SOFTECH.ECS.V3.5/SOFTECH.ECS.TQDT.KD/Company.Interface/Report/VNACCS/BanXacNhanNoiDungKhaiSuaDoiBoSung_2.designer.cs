namespace Company.Interface.Report.VNACCS
{
    partial class BanXacNhanNoiDungKhaiSuaDoiBoSung_2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDKKhaiBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDKKhaiBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNhomXuLyHoSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayToKhaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDauHieuBaoQua = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayCapPhep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiHanTaiNhapXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTiepNhanTangGiamTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTiepNhanTangGiamTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTruocBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaSauBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTruocBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTruocBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongSauBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLSauBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatTruocBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatSauBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTruocBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatSauBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueTruocBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKTruocBoSung5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueSauBoSungTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKSauBoSung5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTangGiamTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienTangGiamTTK5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTiepNhanTangGiamTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTiepNhanTangGiamTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTruocBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaSauBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTruocBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTruocBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongSauBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLSauBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatTruocBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatSauBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTruocBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatSauBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueTruocBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKTruocBoSung4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueSauBoSungTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKSauBoSung4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTangGiamTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienTangGiamTTK4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTiepNhanTangGiamTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTiepNhanTangGiamTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTruocBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaSauBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell57 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTruocBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTruocBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongSauBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLSauBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatTruocBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatSauBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTruocBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatSauBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueTruocBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKTruocBoSung3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueSauBoSungTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKSauBoSung3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTangGiamTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienTangGiamTTK3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTiepNhanTangGiamTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTiepNhanTangGiamTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTruocBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaSauBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTruocBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTruocBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongSauBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLSauBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatTruocBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatSauBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTruocBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatSauBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueTruocBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKTruocBoSung2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueSauBoSungTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKSauBoSung2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTangGiamTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienTangGiamTTK2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThuTuHangTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHangTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuocXXTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHangSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuocXXSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHSSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTienThueSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTangGiamThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienTangGiamThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTiepNhanTangGiamTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenTiepNhanTangGiamTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTruocBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaSauBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTruocBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTTruocBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongSauBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDVTSLSauBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatTruocBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaThueSuatSauBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatTruocBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatSauBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueTruocBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKTruocBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueSauBoSungTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMienThueTTKSauBoSung = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTangGiamTTK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienTangGiamTTK = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable4,
            this.xrLabel1,
            this.xrTable3});
            this.TopMargin.HeightF = 144.125F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(656.1667F, 29.16667F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow23});
            this.xrTable4.SizeF = new System.Drawing.SizeF(69.83327F, 18F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang,
            this.xrTableCell3,
            this.lblSoPL});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "XX";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTrang.Weight = 1.3485199481430721;
            this.lblSoTrang.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.lblSoTrang_PrintOnPage);
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell3.StylePriority.UsePadding = false;
            this.xrTableCell3.Text = "/";
            this.xrTableCell3.Weight = 0.52269253924298331;
            // 
            // lblSoPL
            // 
            this.lblSoPL.Name = "lblSoPL";
            this.lblSoPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.lblSoPL.StylePriority.UsePadding = false;
            this.lblSoPL.Text = "XX";
            this.lblSoPL.Weight = 1.5148779820294527;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(142.7082F, 24.16666F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(432.2023F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Tờ khai bổ sung sau thông quan (đã đăng ký)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 70.08331F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22});
            this.xrTable3.SizeF = new System.Drawing.SizeF(726F, 63F);
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UsePadding = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell103,
            this.lblSoToKhaiBoSung,
            this.xrTableCell92,
            this.xrTableCell107,
            this.lblNgayDKKhaiBoSung,
            this.lblGioDKKhaiBoSung});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.75;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Text = "Số tờ khai bổ sung";
            this.xrTableCell103.Weight = 0.4951788216583;
            // 
            // lblSoToKhaiBoSung
            // 
            this.lblSoToKhaiBoSung.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiBoSung.Name = "lblSoToKhaiBoSung";
            this.lblSoToKhaiBoSung.StylePriority.UseFont = false;
            this.lblSoToKhaiBoSung.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiBoSung.Weight = 0.85778256881335557;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.10571629547875776;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Text = "Ngày đăng ký";
            this.xrTableCell107.Weight = 0.70230733067536155;
            // 
            // lblNgayDKKhaiBoSung
            // 
            this.lblNgayDKKhaiBoSung.Name = "lblNgayDKKhaiBoSung";
            this.lblNgayDKKhaiBoSung.Text = "dd/MM/yyyy";
            this.lblNgayDKKhaiBoSung.Weight = 0.41950749168711254;
            // 
            // lblGioDKKhaiBoSung
            // 
            this.lblGioDKKhaiBoSung.Name = "lblGioDKKhaiBoSung";
            this.lblGioDKKhaiBoSung.Text = "hh:mm:ss";
            this.lblGioDKKhaiBoSung.Weight = 0.41950749168711254;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell104,
            this.lblCoQuanNhan,
            this.xrTableCell95,
            this.xrTableCell108,
            this.lblNhomXuLyHoSo});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.75;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Text = "Cơ quan nhận";
            this.xrTableCell104.Weight = 0.4951788216583;
            // 
            // lblCoQuanNhan
            // 
            this.lblCoQuanNhan.Name = "lblCoQuanNhan";
            this.lblCoQuanNhan.Text = "XXXXXXXXXE";
            this.lblCoQuanNhan.Weight = 0.85778256881335557;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.10571629547875776;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "Nhóm xử lý hồ sơ";
            this.xrTableCell108.Weight = 0.70230733067536155;
            // 
            // lblNhomXuLyHoSo
            // 
            this.lblNhomXuLyHoSo.Name = "lblNhomXuLyHoSo";
            this.lblNhomXuLyHoSo.Text = "XE";
            this.lblNhomXuLyHoSo.Weight = 0.83901498337422509;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.lblPhanLoaiXNK,
            this.xrTableCell111,
            this.lblSoToKhaiXNK,
            this.xrTableCell114,
            this.lblMaLoaiHinh,
            this.xrTableCell98,
            this.xrTableCell109,
            this.lblNgayToKhaiXNK,
            this.xrTableCell117,
            this.lblDauHieuBaoQua});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.75;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Text = "Số tờ khai";
            this.xrTableCell105.Weight = 0.4951788216583;
            // 
            // lblPhanLoaiXNK
            // 
            this.lblPhanLoaiXNK.Name = "lblPhanLoaiXNK";
            this.lblPhanLoaiXNK.Text = "X";
            this.lblPhanLoaiXNK.Weight = 0.090392648681136223;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "-";
            this.xrTableCell111.Weight = 0.064566044768026076;
            // 
            // lblSoToKhaiXNK
            // 
            this.lblSoToKhaiXNK.Name = "lblSoToKhaiXNK";
            this.lblSoToKhaiXNK.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiXNK.Weight = 0.48640499430254475;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "-";
            this.xrTableCell114.Weight = 0.0645661117616764;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.Weight = 0.15185276929997219;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 0.10571629547875776;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Text = "Ngày tờ khai xuất nhập khẩu";
            this.xrTableCell109.Weight = 0.70230733067536144;
            // 
            // lblNgayToKhaiXNK
            // 
            this.lblNgayToKhaiXNK.Name = "lblNgayToKhaiXNK";
            this.lblNgayToKhaiXNK.Text = "dd/MM/yyyy";
            this.lblNgayToKhaiXNK.Weight = 0.35296782186208675;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "-";
            this.xrTableCell117.Weight = 0.06456611570247929;
            // 
            // lblDauHieuBaoQua
            // 
            this.lblDauHieuBaoQua.Name = "lblDauHieuBaoQua";
            this.lblDauHieuBaoQua.Text = "X";
            this.lblDauHieuBaoQua.Weight = 0.42148104580965917;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell106,
            this.lblNgayCapPhep,
            this.xrTableCell101,
            this.xrTableCell110,
            this.lblThoiHanTaiNhapXuat});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.StylePriority.UseBorders = false;
            this.xrTableRow22.Weight = 0.9;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Ngày cấp phép";
            this.xrTableCell106.Weight = 0.4951788216583;
            // 
            // lblNgayCapPhep
            // 
            this.lblNgayCapPhep.Name = "lblNgayCapPhep";
            this.lblNgayCapPhep.Text = "dd/MM/yyyy";
            this.lblNgayCapPhep.Weight = 0.85778256881335557;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.10571629547875776;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell110.Weight = 0.70230733067536155;
            // 
            // lblThoiHanTaiNhapXuat
            // 
            this.lblThoiHanTaiNhapXuat.Name = "lblThoiHanTaiNhapXuat";
            this.lblThoiHanTaiNhapXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapXuat.Weight = 0.83901498337422509;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 23F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable8,
            this.xrTable7,
            this.xrTable6,
            this.xrTable5,
            this.xrTable1,
            this.xrTable2});
            this.Detail1.HeightF = 899.7916F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 769.9999F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47});
            this.xrTable8.SizeF = new System.Drawing.SizeF(726F, 120F);
            this.xrTable8.StylePriority.UseFont = false;
            this.xrTable8.StylePriority.UsePadding = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell140,
            this.lblMaTiepNhanTangGiamTTK5,
            this.lblTenTiepNhanTangGiamTTK5,
            this.xrTableCell143,
            this.xrTableCell144});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.StylePriority.UseBorders = false;
            this.xrTableRow42.Weight = 1;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.StylePriority.UseBorders = false;
            this.xrTableCell140.Weight = 0.54424929027715008;
            // 
            // lblMaTiepNhanTangGiamTTK5
            // 
            this.lblMaTiepNhanTangGiamTTK5.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaTiepNhanTangGiamTTK5.Name = "lblMaTiepNhanTangGiamTTK5";
            this.lblMaTiepNhanTangGiamTTK5.StylePriority.UseBorders = false;
            this.lblMaTiepNhanTangGiamTTK5.Text = "X";
            this.lblMaTiepNhanTangGiamTTK5.Weight = 0.10175600918856535;
            // 
            // lblTenTiepNhanTangGiamTTK5
            // 
            this.lblTenTiepNhanTangGiamTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenTiepNhanTangGiamTTK5.Name = "lblTenTiepNhanTangGiamTTK5";
            this.lblTenTiepNhanTangGiamTTK5.StylePriority.UseBorders = false;
            this.lblTenTiepNhanTangGiamTTK5.Text = "lblTenTiepNhanTangGiamTTK";
            this.lblTenTiepNhanTangGiamTTK5.Weight = 0.8085401550797392;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.StylePriority.UseBorders = false;
            this.xrTableCell143.Weight = 0.916985630003874;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.StylePriority.UseBorders = false;
            this.xrTableCell144.Weight = 0.62846891545067152;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.lblTriGiaTruocBoSungTTK5,
            this.lblTriGiaSauBoSungTTK5,
            this.xrTableCell148});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 1;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell145.StylePriority.UsePadding = false;
            this.xrTableCell145.Text = "Trị giá tính thuế";
            this.xrTableCell145.Weight = 0.54424929027715008;
            // 
            // lblTriGiaTruocBoSungTTK5
            // 
            this.lblTriGiaTruocBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaTruocBoSungTTK5.Name = "lblTriGiaTruocBoSungTTK5";
            this.lblTriGiaTruocBoSungTTK5.StylePriority.UseBorders = false;
            this.lblTriGiaTruocBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTruocBoSungTTK5.Text = "12,345,678,901,234,567";
            this.lblTriGiaTruocBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaTruocBoSungTTK5.Weight = 0.91029616426830451;
            this.lblTriGiaTruocBoSungTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaSauBoSungTTK5
            // 
            this.lblTriGiaSauBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaSauBoSungTTK5.Name = "lblTriGiaSauBoSungTTK5";
            this.lblTriGiaSauBoSungTTK5.StylePriority.UseBorders = false;
            this.lblTriGiaSauBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblTriGiaSauBoSungTTK5.Text = "12,345,678,901,234,567";
            this.lblTriGiaSauBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaSauBoSungTTK5.Weight = 0.916985630003874;
            this.lblTriGiaSauBoSungTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 0.62846891545067152;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.lblSoLuongTruocBoSungTTK5,
            this.lblMaDVTTruocBoSungTTK5,
            this.lblSoLuongSauBoSungTTK5,
            this.lblMaDVTSLSauBoSungTTK5,
            this.xrTableCell154});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 1;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell149.StylePriority.UsePadding = false;
            this.xrTableCell149.Text = "Số lượng tính thuế";
            this.xrTableCell149.Weight = 0.54424929027715008;
            // 
            // lblSoLuongTruocBoSungTTK5
            // 
            this.lblSoLuongTruocBoSungTTK5.Name = "lblSoLuongTruocBoSungTTK5";
            this.lblSoLuongTruocBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTruocBoSungTTK5.Text = "lblSoLuongTruocBoSungTTK";
            this.lblSoLuongTruocBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongTruocBoSungTTK5.Weight = 0.71831953821103434;
            this.lblSoLuongTruocBoSungTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTTruocBoSungTTK5
            // 
            this.lblMaDVTTruocBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTTruocBoSungTTK5.Name = "lblMaDVTTruocBoSungTTK5";
            this.lblMaDVTTruocBoSungTTK5.StylePriority.UseBorders = false;
            this.lblMaDVTTruocBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblMaDVTTruocBoSungTTK5.Text = "XXXE";
            this.lblMaDVTTruocBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTTruocBoSungTTK5.Weight = 0.19197662605727014;
            // 
            // lblSoLuongSauBoSungTTK5
            // 
            this.lblSoLuongSauBoSungTTK5.Name = "lblSoLuongSauBoSungTTK5";
            this.lblSoLuongSauBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblSoLuongSauBoSungTTK5.Text = "lblSoLuongSauBoSungTTK";
            this.lblSoLuongSauBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongSauBoSungTTK5.Weight = 0.718305855743156;
            this.lblSoLuongSauBoSungTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLSauBoSungTTK5
            // 
            this.lblMaDVTSLSauBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLSauBoSungTTK5.Name = "lblMaDVTSLSauBoSungTTK5";
            this.lblMaDVTSLSauBoSungTTK5.StylePriority.UseBorders = false;
            this.lblMaDVTSLSauBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLSauBoSungTTK5.Text = "XXXE";
            this.lblMaDVTSLSauBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLSauBoSungTTK5.Weight = 0.19867977426071798;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Weight = 0.62846891545067152;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.lblMaThueSuatTruocBoSungTTK5,
            this.lblMaThueSuatSauBoSungTTK5,
            this.xrTableCell158});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.Weight = 1;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell155.StylePriority.UsePadding = false;
            this.xrTableCell155.Text = "Mã xác định thuế suất";
            this.xrTableCell155.Weight = 0.54424929027715008;
            // 
            // lblMaThueSuatTruocBoSungTTK5
            // 
            this.lblMaThueSuatTruocBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatTruocBoSungTTK5.Name = "lblMaThueSuatTruocBoSungTTK5";
            this.lblMaThueSuatTruocBoSungTTK5.StylePriority.UseBorders = false;
            this.lblMaThueSuatTruocBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatTruocBoSungTTK5.Text = "lblMaThueSuatTruocBoSungTTK";
            this.lblMaThueSuatTruocBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatTruocBoSungTTK5.Weight = 0.91029616426830451;
            // 
            // lblMaThueSuatSauBoSungTTK5
            // 
            this.lblMaThueSuatSauBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatSauBoSungTTK5.Name = "lblMaThueSuatSauBoSungTTK5";
            this.lblMaThueSuatSauBoSungTTK5.StylePriority.UseBorders = false;
            this.lblMaThueSuatSauBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatSauBoSungTTK5.Text = "lblMaThueSuatSauBoSungTTK";
            this.lblMaThueSuatSauBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatSauBoSungTTK5.Weight = 0.916985630003874;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Weight = 0.62846891545067152;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.lblThueSuatTruocBoSungTTK5,
            this.lblThueSuatSauBoSungTTK5,
            this.xrTableCell162});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 1;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell159.StylePriority.UsePadding = false;
            this.xrTableCell159.Text = "Thuế suất";
            this.xrTableCell159.Weight = 0.54424929027715008;
            // 
            // lblThueSuatTruocBoSungTTK5
            // 
            this.lblThueSuatTruocBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatTruocBoSungTTK5.Name = "lblThueSuatTruocBoSungTTK5";
            this.lblThueSuatTruocBoSungTTK5.StylePriority.UseBorders = false;
            this.lblThueSuatTruocBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTruocBoSungTTK5.Text = "lblThueSuatTruocBoSungTTK";
            this.lblThueSuatTruocBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTruocBoSungTTK5.Weight = 0.91029616426830451;
            // 
            // lblThueSuatSauBoSungTTK5
            // 
            this.lblThueSuatSauBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatSauBoSungTTK5.Name = "lblThueSuatSauBoSungTTK5";
            this.lblThueSuatSauBoSungTTK5.StylePriority.UseBorders = false;
            this.lblThueSuatSauBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblThueSuatSauBoSungTTK5.Text = "lblThueSuatSauBoSungTTK";
            this.lblThueSuatSauBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatSauBoSungTTK5.Weight = 0.916985630003874;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.62846891545067152;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.lblSoTienThueTruocBoSungTTK5,
            this.lblMienThueTTKTruocBoSung5,
            this.lblSoTienThueSauBoSungTTK5,
            this.lblMienThueTTKSauBoSung5,
            this.lblTangGiamTTK5,
            this.lblSoTienTangGiamTTK5});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.StylePriority.UseBorders = false;
            this.xrTableRow47.Weight = 1;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell163.StylePriority.UseBorders = false;
            this.xrTableCell163.StylePriority.UsePadding = false;
            this.xrTableCell163.Text = "Sô tiền thuế";
            this.xrTableCell163.Weight = 0.54424929027715008;
            // 
            // lblSoTienThueTruocBoSungTTK5
            // 
            this.lblSoTienThueTruocBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueTruocBoSungTTK5.Name = "lblSoTienThueTruocBoSungTTK5";
            this.lblSoTienThueTruocBoSungTTK5.StylePriority.UseBorders = false;
            this.lblSoTienThueTruocBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueTruocBoSungTTK5.Text = "lblSoTienThueTruocBoSungTTK";
            this.lblSoTienThueTruocBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueTruocBoSungTTK5.Weight = 0.80457986878954668;
            this.lblSoTienThueTruocBoSungTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKTruocBoSung5
            // 
            this.lblMienThueTTKTruocBoSung5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKTruocBoSung5.Name = "lblMienThueTTKTruocBoSung5";
            this.lblMienThueTTKTruocBoSung5.StylePriority.UseBorders = false;
            this.lblMienThueTTKTruocBoSung5.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKTruocBoSung5.Text = "X";
            this.lblMienThueTTKTruocBoSung5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKTruocBoSung5.Weight = 0.1057162954787578;
            // 
            // lblSoTienThueSauBoSungTTK5
            // 
            this.lblSoTienThueSauBoSungTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueSauBoSungTTK5.Name = "lblSoTienThueSauBoSungTTK5";
            this.lblSoTienThueSauBoSungTTK5.StylePriority.UseBorders = false;
            this.lblSoTienThueSauBoSungTTK5.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueSauBoSungTTK5.Text = "lblSoTienThueSauBoSungTTK";
            this.lblSoTienThueSauBoSungTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueSauBoSungTTK5.Weight = 0.8099173553719009;
            this.lblSoTienThueSauBoSungTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKSauBoSung5
            // 
            this.lblMienThueTTKSauBoSung5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKSauBoSung5.Name = "lblMienThueTTKSauBoSung5";
            this.lblMienThueTTKSauBoSung5.StylePriority.UseBorders = false;
            this.lblMienThueTTKSauBoSung5.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKSauBoSung5.Text = "X";
            this.lblMienThueTTKSauBoSung5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKSauBoSung5.Weight = 0.10706827463197322;
            // 
            // lblTangGiamTTK5
            // 
            this.lblTangGiamTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTangGiamTTK5.Name = "lblTangGiamTTK5";
            this.lblTangGiamTTK5.StylePriority.UseBorders = false;
            this.lblTangGiamTTK5.StylePriority.UseTextAlignment = false;
            this.lblTangGiamTTK5.Text = "X";
            this.lblTangGiamTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTangGiamTTK5.Weight = 0.081796441196410136;
            // 
            // lblSoTienTangGiamTTK5
            // 
            this.lblSoTienTangGiamTTK5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienTangGiamTTK5.Name = "lblSoTienTangGiamTTK5";
            this.lblSoTienTangGiamTTK5.StylePriority.UseBorders = false;
            this.lblSoTienTangGiamTTK5.StylePriority.UseTextAlignment = false;
            this.lblSoTienTangGiamTTK5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienTangGiamTTK5.Weight = 0.54667247425426135;
            this.lblSoTienTangGiamTTK5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 650F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36,
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41});
            this.xrTable7.SizeF = new System.Drawing.SizeF(726F, 120F);
            this.xrTable7.StylePriority.UseFont = false;
            this.xrTable7.StylePriority.UsePadding = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.lblMaTiepNhanTangGiamTTK4,
            this.lblTenTiepNhanTangGiamTTK4,
            this.xrTableCell102,
            this.xrTableCell112});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.StylePriority.UseBorders = false;
            this.xrTableRow36.Weight = 1;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.StylePriority.UseBorders = false;
            this.xrTableCell97.Weight = 0.54424929027715008;
            // 
            // lblMaTiepNhanTangGiamTTK4
            // 
            this.lblMaTiepNhanTangGiamTTK4.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaTiepNhanTangGiamTTK4.Name = "lblMaTiepNhanTangGiamTTK4";
            this.lblMaTiepNhanTangGiamTTK4.StylePriority.UseBorders = false;
            this.lblMaTiepNhanTangGiamTTK4.Text = "X";
            this.lblMaTiepNhanTangGiamTTK4.Weight = 0.10175600918856535;
            // 
            // lblTenTiepNhanTangGiamTTK4
            // 
            this.lblTenTiepNhanTangGiamTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenTiepNhanTangGiamTTK4.Name = "lblTenTiepNhanTangGiamTTK4";
            this.lblTenTiepNhanTangGiamTTK4.StylePriority.UseBorders = false;
            this.lblTenTiepNhanTangGiamTTK4.Text = "lblTenTiepNhanTangGiamTTK";
            this.lblTenTiepNhanTangGiamTTK4.Weight = 0.8085401550797392;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.StylePriority.UseBorders = false;
            this.xrTableCell102.Weight = 0.916985630003874;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.StylePriority.UseBorders = false;
            this.xrTableCell112.Weight = 0.62846891545067152;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.lblTriGiaTruocBoSungTTK4,
            this.lblTriGiaSauBoSungTTK4,
            this.xrTableCell118});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 1;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell113.StylePriority.UsePadding = false;
            this.xrTableCell113.Text = "Trị giá tính thuế";
            this.xrTableCell113.Weight = 0.54424929027715008;
            // 
            // lblTriGiaTruocBoSungTTK4
            // 
            this.lblTriGiaTruocBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaTruocBoSungTTK4.Name = "lblTriGiaTruocBoSungTTK4";
            this.lblTriGiaTruocBoSungTTK4.StylePriority.UseBorders = false;
            this.lblTriGiaTruocBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTruocBoSungTTK4.Text = "12,345,678,901,234,567";
            this.lblTriGiaTruocBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaTruocBoSungTTK4.Weight = 0.91029616426830451;
            this.lblTriGiaTruocBoSungTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaSauBoSungTTK4
            // 
            this.lblTriGiaSauBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaSauBoSungTTK4.Name = "lblTriGiaSauBoSungTTK4";
            this.lblTriGiaSauBoSungTTK4.StylePriority.UseBorders = false;
            this.lblTriGiaSauBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblTriGiaSauBoSungTTK4.Text = "12,345,678,901,234,567";
            this.lblTriGiaSauBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaSauBoSungTTK4.Weight = 0.916985630003874;
            this.lblTriGiaSauBoSungTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.62846891545067152;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell119,
            this.lblSoLuongTruocBoSungTTK4,
            this.lblMaDVTTruocBoSungTTK4,
            this.lblSoLuongSauBoSungTTK4,
            this.lblMaDVTSLSauBoSungTTK4,
            this.xrTableCell124});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 1;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell119.StylePriority.UsePadding = false;
            this.xrTableCell119.Text = "Số lượng tính thuế";
            this.xrTableCell119.Weight = 0.54424929027715008;
            // 
            // lblSoLuongTruocBoSungTTK4
            // 
            this.lblSoLuongTruocBoSungTTK4.Name = "lblSoLuongTruocBoSungTTK4";
            this.lblSoLuongTruocBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTruocBoSungTTK4.Text = "lblSoLuongTruocBoSungTTK";
            this.lblSoLuongTruocBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongTruocBoSungTTK4.Weight = 0.71831953821103434;
            this.lblSoLuongTruocBoSungTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTTruocBoSungTTK4
            // 
            this.lblMaDVTTruocBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTTruocBoSungTTK4.Name = "lblMaDVTTruocBoSungTTK4";
            this.lblMaDVTTruocBoSungTTK4.StylePriority.UseBorders = false;
            this.lblMaDVTTruocBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblMaDVTTruocBoSungTTK4.Text = "XXXE";
            this.lblMaDVTTruocBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTTruocBoSungTTK4.Weight = 0.19197662605727014;
            // 
            // lblSoLuongSauBoSungTTK4
            // 
            this.lblSoLuongSauBoSungTTK4.Name = "lblSoLuongSauBoSungTTK4";
            this.lblSoLuongSauBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblSoLuongSauBoSungTTK4.Text = "lblSoLuongSauBoSungTTK";
            this.lblSoLuongSauBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongSauBoSungTTK4.Weight = 0.718305855743156;
            this.lblSoLuongSauBoSungTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLSauBoSungTTK4
            // 
            this.lblMaDVTSLSauBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLSauBoSungTTK4.Name = "lblMaDVTSLSauBoSungTTK4";
            this.lblMaDVTSLSauBoSungTTK4.StylePriority.UseBorders = false;
            this.lblMaDVTSLSauBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLSauBoSungTTK4.Text = "XXXE";
            this.lblMaDVTSLSauBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLSauBoSungTTK4.Weight = 0.19867977426071798;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Weight = 0.62846891545067152;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.lblMaThueSuatTruocBoSungTTK4,
            this.lblMaThueSuatSauBoSungTTK4,
            this.xrTableCell128});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 1;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell125.StylePriority.UsePadding = false;
            this.xrTableCell125.Text = "Mã xác định thuế suất";
            this.xrTableCell125.Weight = 0.54424929027715008;
            // 
            // lblMaThueSuatTruocBoSungTTK4
            // 
            this.lblMaThueSuatTruocBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatTruocBoSungTTK4.Name = "lblMaThueSuatTruocBoSungTTK4";
            this.lblMaThueSuatTruocBoSungTTK4.StylePriority.UseBorders = false;
            this.lblMaThueSuatTruocBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatTruocBoSungTTK4.Text = "lblMaThueSuatTruocBoSungTTK";
            this.lblMaThueSuatTruocBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatTruocBoSungTTK4.Weight = 0.91029616426830451;
            // 
            // lblMaThueSuatSauBoSungTTK4
            // 
            this.lblMaThueSuatSauBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatSauBoSungTTK4.Name = "lblMaThueSuatSauBoSungTTK4";
            this.lblMaThueSuatSauBoSungTTK4.StylePriority.UseBorders = false;
            this.lblMaThueSuatSauBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatSauBoSungTTK4.Text = "lblMaThueSuatSauBoSungTTK";
            this.lblMaThueSuatSauBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatSauBoSungTTK4.Weight = 0.916985630003874;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.62846891545067152;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell129,
            this.lblThueSuatTruocBoSungTTK4,
            this.lblThueSuatSauBoSungTTK4,
            this.xrTableCell132});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 1;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell129.StylePriority.UsePadding = false;
            this.xrTableCell129.Text = "Thuế suất";
            this.xrTableCell129.Weight = 0.54424929027715008;
            // 
            // lblThueSuatTruocBoSungTTK4
            // 
            this.lblThueSuatTruocBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatTruocBoSungTTK4.Name = "lblThueSuatTruocBoSungTTK4";
            this.lblThueSuatTruocBoSungTTK4.StylePriority.UseBorders = false;
            this.lblThueSuatTruocBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTruocBoSungTTK4.Text = "lblThueSuatTruocBoSungTTK";
            this.lblThueSuatTruocBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTruocBoSungTTK4.Weight = 0.91029616426830451;
            // 
            // lblThueSuatSauBoSungTTK4
            // 
            this.lblThueSuatSauBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatSauBoSungTTK4.Name = "lblThueSuatSauBoSungTTK4";
            this.lblThueSuatSauBoSungTTK4.StylePriority.UseBorders = false;
            this.lblThueSuatSauBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblThueSuatSauBoSungTTK4.Text = "lblThueSuatSauBoSungTTK";
            this.lblThueSuatSauBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatSauBoSungTTK4.Weight = 0.916985630003874;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.62846891545067152;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell133,
            this.lblSoTienThueTruocBoSungTTK4,
            this.lblMienThueTTKTruocBoSung4,
            this.lblSoTienThueSauBoSungTTK4,
            this.lblMienThueTTKSauBoSung4,
            this.lblTangGiamTTK4,
            this.lblSoTienTangGiamTTK4});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 1;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell133.StylePriority.UseBorders = false;
            this.xrTableCell133.StylePriority.UsePadding = false;
            this.xrTableCell133.Text = "Sô tiền thuế";
            this.xrTableCell133.Weight = 0.54424929027715008;
            // 
            // lblSoTienThueTruocBoSungTTK4
            // 
            this.lblSoTienThueTruocBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueTruocBoSungTTK4.Name = "lblSoTienThueTruocBoSungTTK4";
            this.lblSoTienThueTruocBoSungTTK4.StylePriority.UseBorders = false;
            this.lblSoTienThueTruocBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueTruocBoSungTTK4.Text = "lblSoTienThueTruocBoSungTTK";
            this.lblSoTienThueTruocBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueTruocBoSungTTK4.Weight = 0.80457986878954668;
            this.lblSoTienThueTruocBoSungTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKTruocBoSung4
            // 
            this.lblMienThueTTKTruocBoSung4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKTruocBoSung4.Name = "lblMienThueTTKTruocBoSung4";
            this.lblMienThueTTKTruocBoSung4.StylePriority.UseBorders = false;
            this.lblMienThueTTKTruocBoSung4.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKTruocBoSung4.Text = "X";
            this.lblMienThueTTKTruocBoSung4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKTruocBoSung4.Weight = 0.1057162954787578;
            // 
            // lblSoTienThueSauBoSungTTK4
            // 
            this.lblSoTienThueSauBoSungTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueSauBoSungTTK4.Name = "lblSoTienThueSauBoSungTTK4";
            this.lblSoTienThueSauBoSungTTK4.StylePriority.UseBorders = false;
            this.lblSoTienThueSauBoSungTTK4.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueSauBoSungTTK4.Text = "lblSoTienThueSauBoSungTTK";
            this.lblSoTienThueSauBoSungTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueSauBoSungTTK4.Weight = 0.8099173553719009;
            this.lblSoTienThueSauBoSungTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKSauBoSung4
            // 
            this.lblMienThueTTKSauBoSung4.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKSauBoSung4.Name = "lblMienThueTTKSauBoSung4";
            this.lblMienThueTTKSauBoSung4.StylePriority.UseBorders = false;
            this.lblMienThueTTKSauBoSung4.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKSauBoSung4.Text = "X";
            this.lblMienThueTTKSauBoSung4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKSauBoSung4.Weight = 0.10706827463197322;
            // 
            // lblTangGiamTTK4
            // 
            this.lblTangGiamTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTangGiamTTK4.Name = "lblTangGiamTTK4";
            this.lblTangGiamTTK4.StylePriority.UseBorders = false;
            this.lblTangGiamTTK4.StylePriority.UseTextAlignment = false;
            this.lblTangGiamTTK4.Text = "X";
            this.lblTangGiamTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTangGiamTTK4.Weight = 0.081796441196410136;
            // 
            // lblSoTienTangGiamTTK4
            // 
            this.lblSoTienTangGiamTTK4.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienTangGiamTTK4.Name = "lblSoTienTangGiamTTK4";
            this.lblSoTienTangGiamTTK4.StylePriority.UseBorders = false;
            this.lblSoTienTangGiamTTK4.StylePriority.UseTextAlignment = false;
            this.lblSoTienTangGiamTTK4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienTangGiamTTK4.Weight = 0.54667247425426135;
            this.lblSoTienTangGiamTTK4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 530F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30,
            this.xrTableRow31,
            this.xrTableRow32,
            this.xrTableRow33,
            this.xrTableRow34,
            this.xrTableRow35});
            this.xrTable6.SizeF = new System.Drawing.SizeF(726F, 120F);
            this.xrTable6.StylePriority.UseFont = false;
            this.xrTable6.StylePriority.UsePadding = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.lblMaTiepNhanTangGiamTTK3,
            this.lblTenTiepNhanTangGiamTTK3,
            this.xrTableCell50,
            this.xrTableCell51});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.StylePriority.UseBorders = false;
            this.xrTableRow30.Weight = 1;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.StylePriority.UseBorders = false;
            this.xrTableCell45.Weight = 0.54424929027715008;
            // 
            // lblMaTiepNhanTangGiamTTK3
            // 
            this.lblMaTiepNhanTangGiamTTK3.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaTiepNhanTangGiamTTK3.Name = "lblMaTiepNhanTangGiamTTK3";
            this.lblMaTiepNhanTangGiamTTK3.StylePriority.UseBorders = false;
            this.lblMaTiepNhanTangGiamTTK3.Text = "X";
            this.lblMaTiepNhanTangGiamTTK3.Weight = 0.10175600918856535;
            // 
            // lblTenTiepNhanTangGiamTTK3
            // 
            this.lblTenTiepNhanTangGiamTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenTiepNhanTangGiamTTK3.Name = "lblTenTiepNhanTangGiamTTK3";
            this.lblTenTiepNhanTangGiamTTK3.StylePriority.UseBorders = false;
            this.lblTenTiepNhanTangGiamTTK3.Text = "lblTenTiepNhanTangGiamTTK";
            this.lblTenTiepNhanTangGiamTTK3.Weight = 0.8085401550797392;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.StylePriority.UseBorders = false;
            this.xrTableCell50.Weight = 0.916985630003874;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.StylePriority.UseBorders = false;
            this.xrTableCell51.Weight = 0.62846891545067152;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell52,
            this.lblTriGiaTruocBoSungTTK3,
            this.lblTriGiaSauBoSungTTK3,
            this.xrTableCell57});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell52.StylePriority.UsePadding = false;
            this.xrTableCell52.Text = "Trị giá tính thuế";
            this.xrTableCell52.Weight = 0.54424929027715008;
            // 
            // lblTriGiaTruocBoSungTTK3
            // 
            this.lblTriGiaTruocBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaTruocBoSungTTK3.Name = "lblTriGiaTruocBoSungTTK3";
            this.lblTriGiaTruocBoSungTTK3.StylePriority.UseBorders = false;
            this.lblTriGiaTruocBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTruocBoSungTTK3.Text = "12,345,678,901,234,567";
            this.lblTriGiaTruocBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaTruocBoSungTTK3.Weight = 0.91029616426830451;
            this.lblTriGiaTruocBoSungTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaSauBoSungTTK3
            // 
            this.lblTriGiaSauBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaSauBoSungTTK3.Name = "lblTriGiaSauBoSungTTK3";
            this.lblTriGiaSauBoSungTTK3.StylePriority.UseBorders = false;
            this.lblTriGiaSauBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblTriGiaSauBoSungTTK3.Text = "12,345,678,901,234,567";
            this.lblTriGiaSauBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaSauBoSungTTK3.Weight = 0.916985630003874;
            this.lblTriGiaSauBoSungTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell57
            // 
            this.xrTableCell57.Name = "xrTableCell57";
            this.xrTableCell57.Weight = 0.62846891545067152;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.lblSoLuongTruocBoSungTTK3,
            this.lblMaDVTTruocBoSungTTK3,
            this.lblSoLuongSauBoSungTTK3,
            this.lblMaDVTSLSauBoSungTTK3,
            this.xrTableCell65});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 1;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.Text = "Số lượng tính thuế";
            this.xrTableCell58.Weight = 0.54424929027715008;
            // 
            // lblSoLuongTruocBoSungTTK3
            // 
            this.lblSoLuongTruocBoSungTTK3.Name = "lblSoLuongTruocBoSungTTK3";
            this.lblSoLuongTruocBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTruocBoSungTTK3.Text = "lblSoLuongTruocBoSungTTK";
            this.lblSoLuongTruocBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongTruocBoSungTTK3.Weight = 0.71831953821103434;
            this.lblSoLuongTruocBoSungTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTTruocBoSungTTK3
            // 
            this.lblMaDVTTruocBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTTruocBoSungTTK3.Name = "lblMaDVTTruocBoSungTTK3";
            this.lblMaDVTTruocBoSungTTK3.StylePriority.UseBorders = false;
            this.lblMaDVTTruocBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblMaDVTTruocBoSungTTK3.Text = "XXXE";
            this.lblMaDVTTruocBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTTruocBoSungTTK3.Weight = 0.19197662605727014;
            // 
            // lblSoLuongSauBoSungTTK3
            // 
            this.lblSoLuongSauBoSungTTK3.Name = "lblSoLuongSauBoSungTTK3";
            this.lblSoLuongSauBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongSauBoSungTTK3.Text = "lblSoLuongSauBoSungTTK";
            this.lblSoLuongSauBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongSauBoSungTTK3.Weight = 0.718305855743156;
            this.lblSoLuongSauBoSungTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLSauBoSungTTK3
            // 
            this.lblMaDVTSLSauBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLSauBoSungTTK3.Name = "lblMaDVTSLSauBoSungTTK3";
            this.lblMaDVTSLSauBoSungTTK3.StylePriority.UseBorders = false;
            this.lblMaDVTSLSauBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLSauBoSungTTK3.Text = "XXXE";
            this.lblMaDVTSLSauBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLSauBoSungTTK3.Weight = 0.19867977426071798;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.62846891545067152;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.lblMaThueSuatTruocBoSungTTK3,
            this.lblMaThueSuatSauBoSungTTK3,
            this.xrTableCell71});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 1;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell68.StylePriority.UsePadding = false;
            this.xrTableCell68.Text = "Mã xác định thuế suất";
            this.xrTableCell68.Weight = 0.54424929027715008;
            // 
            // lblMaThueSuatTruocBoSungTTK3
            // 
            this.lblMaThueSuatTruocBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatTruocBoSungTTK3.Name = "lblMaThueSuatTruocBoSungTTK3";
            this.lblMaThueSuatTruocBoSungTTK3.StylePriority.UseBorders = false;
            this.lblMaThueSuatTruocBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatTruocBoSungTTK3.Text = "lblMaThueSuatTruocBoSungTTK";
            this.lblMaThueSuatTruocBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatTruocBoSungTTK3.Weight = 0.91029616426830451;
            // 
            // lblMaThueSuatSauBoSungTTK3
            // 
            this.lblMaThueSuatSauBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatSauBoSungTTK3.Name = "lblMaThueSuatSauBoSungTTK3";
            this.lblMaThueSuatSauBoSungTTK3.StylePriority.UseBorders = false;
            this.lblMaThueSuatSauBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatSauBoSungTTK3.Text = "lblMaThueSuatSauBoSungTTK";
            this.lblMaThueSuatSauBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatSauBoSungTTK3.Weight = 0.916985630003874;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.62846891545067152;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.lblThueSuatTruocBoSungTTK3,
            this.lblThueSuatSauBoSungTTK3,
            this.xrTableCell83});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 1;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell72.StylePriority.UsePadding = false;
            this.xrTableCell72.Text = "Thuế suất";
            this.xrTableCell72.Weight = 0.54424929027715008;
            // 
            // lblThueSuatTruocBoSungTTK3
            // 
            this.lblThueSuatTruocBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatTruocBoSungTTK3.Name = "lblThueSuatTruocBoSungTTK3";
            this.lblThueSuatTruocBoSungTTK3.StylePriority.UseBorders = false;
            this.lblThueSuatTruocBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTruocBoSungTTK3.Text = "lblThueSuatTruocBoSungTTK";
            this.lblThueSuatTruocBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTruocBoSungTTK3.Weight = 0.91029616426830451;
            // 
            // lblThueSuatSauBoSungTTK3
            // 
            this.lblThueSuatSauBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatSauBoSungTTK3.Name = "lblThueSuatSauBoSungTTK3";
            this.lblThueSuatSauBoSungTTK3.StylePriority.UseBorders = false;
            this.lblThueSuatSauBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatSauBoSungTTK3.Text = "lblThueSuatSauBoSungTTK";
            this.lblThueSuatSauBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatSauBoSungTTK3.Weight = 0.916985630003874;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.62846891545067152;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.lblSoTienThueTruocBoSungTTK3,
            this.lblMienThueTTKTruocBoSung3,
            this.lblSoTienThueSauBoSungTTK3,
            this.lblMienThueTTKSauBoSung3,
            this.lblTangGiamTTK3,
            this.lblSoTienTangGiamTTK3});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 1;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell86.StylePriority.UseBorders = false;
            this.xrTableCell86.StylePriority.UsePadding = false;
            this.xrTableCell86.Text = "Sô tiền thuế";
            this.xrTableCell86.Weight = 0.54424929027715008;
            // 
            // lblSoTienThueTruocBoSungTTK3
            // 
            this.lblSoTienThueTruocBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueTruocBoSungTTK3.Name = "lblSoTienThueTruocBoSungTTK3";
            this.lblSoTienThueTruocBoSungTTK3.StylePriority.UseBorders = false;
            this.lblSoTienThueTruocBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueTruocBoSungTTK3.Text = "lblSoTienThueTruocBoSungTTK";
            this.lblSoTienThueTruocBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueTruocBoSungTTK3.Weight = 0.80457986878954668;
            this.lblSoTienThueTruocBoSungTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKTruocBoSung3
            // 
            this.lblMienThueTTKTruocBoSung3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKTruocBoSung3.Name = "lblMienThueTTKTruocBoSung3";
            this.lblMienThueTTKTruocBoSung3.StylePriority.UseBorders = false;
            this.lblMienThueTTKTruocBoSung3.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKTruocBoSung3.Text = "X";
            this.lblMienThueTTKTruocBoSung3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKTruocBoSung3.Weight = 0.1057162954787578;
            // 
            // lblSoTienThueSauBoSungTTK3
            // 
            this.lblSoTienThueSauBoSungTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueSauBoSungTTK3.Name = "lblSoTienThueSauBoSungTTK3";
            this.lblSoTienThueSauBoSungTTK3.StylePriority.UseBorders = false;
            this.lblSoTienThueSauBoSungTTK3.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueSauBoSungTTK3.Text = "lblSoTienThueSauBoSungTTK";
            this.lblSoTienThueSauBoSungTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueSauBoSungTTK3.Weight = 0.8099173553719009;
            this.lblSoTienThueSauBoSungTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKSauBoSung3
            // 
            this.lblMienThueTTKSauBoSung3.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKSauBoSung3.Name = "lblMienThueTTKSauBoSung3";
            this.lblMienThueTTKSauBoSung3.StylePriority.UseBorders = false;
            this.lblMienThueTTKSauBoSung3.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKSauBoSung3.Text = "X";
            this.lblMienThueTTKSauBoSung3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKSauBoSung3.Weight = 0.10706827463197322;
            // 
            // lblTangGiamTTK3
            // 
            this.lblTangGiamTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTangGiamTTK3.Name = "lblTangGiamTTK3";
            this.lblTangGiamTTK3.StylePriority.UseBorders = false;
            this.lblTangGiamTTK3.StylePriority.UseTextAlignment = false;
            this.lblTangGiamTTK3.Text = "X";
            this.lblTangGiamTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTangGiamTTK3.Weight = 0.081796441196410136;
            // 
            // lblSoTienTangGiamTTK3
            // 
            this.lblSoTienTangGiamTTK3.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienTangGiamTTK3.Name = "lblSoTienTangGiamTTK3";
            this.lblSoTienTangGiamTTK3.StylePriority.UseBorders = false;
            this.lblSoTienTangGiamTTK3.StylePriority.UseTextAlignment = false;
            this.lblSoTienTangGiamTTK3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienTangGiamTTK3.Weight = 0.54667247425426135;
            this.lblSoTienTangGiamTTK3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 410F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29});
            this.xrTable5.SizeF = new System.Drawing.SizeF(726F, 120F);
            this.xrTable5.StylePriority.UseFont = false;
            this.xrTable5.StylePriority.UsePadding = false;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.lblMaTiepNhanTangGiamTTK2,
            this.lblTenTiepNhanTangGiamTTK2,
            this.xrTableCell5,
            this.xrTableCell6});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 1;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.StylePriority.UseBorders = false;
            this.xrTableCell2.Weight = 0.54424929027715008;
            // 
            // lblMaTiepNhanTangGiamTTK2
            // 
            this.lblMaTiepNhanTangGiamTTK2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaTiepNhanTangGiamTTK2.Name = "lblMaTiepNhanTangGiamTTK2";
            this.lblMaTiepNhanTangGiamTTK2.StylePriority.UseBorders = false;
            this.lblMaTiepNhanTangGiamTTK2.Text = "X";
            this.lblMaTiepNhanTangGiamTTK2.Weight = 0.10175600918856535;
            // 
            // lblTenTiepNhanTangGiamTTK2
            // 
            this.lblTenTiepNhanTangGiamTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenTiepNhanTangGiamTTK2.Name = "lblTenTiepNhanTangGiamTTK2";
            this.lblTenTiepNhanTangGiamTTK2.StylePriority.UseBorders = false;
            this.lblTenTiepNhanTangGiamTTK2.Text = "lblTenTiepNhanTangGiamTTK";
            this.lblTenTiepNhanTangGiamTTK2.Weight = 0.80854028118543386;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.StylePriority.UseBorders = false;
            this.xrTableCell5.Weight = 0.91698550389817934;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.StylePriority.UseBorders = false;
            this.xrTableCell6.Weight = 0.62846891545067152;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.lblTriGiaTruocBoSungTTK2,
            this.lblTriGiaSauBoSungTTK2,
            this.xrTableCell11});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell7.StylePriority.UsePadding = false;
            this.xrTableCell7.Text = "Trị giá tính thuế";
            this.xrTableCell7.Weight = 0.54424929027715008;
            // 
            // lblTriGiaTruocBoSungTTK2
            // 
            this.lblTriGiaTruocBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaTruocBoSungTTK2.Name = "lblTriGiaTruocBoSungTTK2";
            this.lblTriGiaTruocBoSungTTK2.StylePriority.UseBorders = false;
            this.lblTriGiaTruocBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTruocBoSungTTK2.Text = "12,345,678,901,234,567";
            this.lblTriGiaTruocBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaTruocBoSungTTK2.Weight = 0.91029629037399917;
            this.lblTriGiaTruocBoSungTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaSauBoSungTTK2
            // 
            this.lblTriGiaSauBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaSauBoSungTTK2.Name = "lblTriGiaSauBoSungTTK2";
            this.lblTriGiaSauBoSungTTK2.StylePriority.UseBorders = false;
            this.lblTriGiaSauBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblTriGiaSauBoSungTTK2.Text = "12,345,678,901,234,567";
            this.lblTriGiaSauBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaSauBoSungTTK2.Weight = 0.91698550389817934;
            this.lblTriGiaSauBoSungTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.62846891545067152;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.lblSoLuongTruocBoSungTTK2,
            this.lblMaDVTTruocBoSungTTK2,
            this.lblSoLuongSauBoSungTTK2,
            this.lblMaDVTSLSauBoSungTTK2,
            this.xrTableCell20});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 1;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell12.StylePriority.UsePadding = false;
            this.xrTableCell12.Text = "Số lượng tính thuế";
            this.xrTableCell12.Weight = 0.54424929027715008;
            // 
            // lblSoLuongTruocBoSungTTK2
            // 
            this.lblSoLuongTruocBoSungTTK2.Name = "lblSoLuongTruocBoSungTTK2";
            this.lblSoLuongTruocBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTruocBoSungTTK2.Text = "lblSoLuongTruocBoSungTTK";
            this.lblSoLuongTruocBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongTruocBoSungTTK2.Weight = 0.71831953821103434;
            this.lblSoLuongTruocBoSungTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTTruocBoSungTTK2
            // 
            this.lblMaDVTTruocBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTTruocBoSungTTK2.Name = "lblMaDVTTruocBoSungTTK2";
            this.lblMaDVTTruocBoSungTTK2.StylePriority.UseBorders = false;
            this.lblMaDVTTruocBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblMaDVTTruocBoSungTTK2.Text = "XXXE";
            this.lblMaDVTTruocBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTTruocBoSungTTK2.Weight = 0.19197675216296489;
            // 
            // lblSoLuongSauBoSungTTK2
            // 
            this.lblSoLuongSauBoSungTTK2.Name = "lblSoLuongSauBoSungTTK2";
            this.lblSoLuongSauBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongSauBoSungTTK2.Text = "lblSoLuongSauBoSungTTK";
            this.lblSoLuongSauBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongSauBoSungTTK2.Weight = 0.71830572963746131;
            this.lblSoLuongSauBoSungTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLSauBoSungTTK2
            // 
            this.lblMaDVTSLSauBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLSauBoSungTTK2.Name = "lblMaDVTSLSauBoSungTTK2";
            this.lblMaDVTSLSauBoSungTTK2.StylePriority.UseBorders = false;
            this.lblMaDVTSLSauBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLSauBoSungTTK2.Text = "XXXE";
            this.lblMaDVTSLSauBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLSauBoSungTTK2.Weight = 0.19867977426071798;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.62846891545067152;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell21,
            this.lblMaThueSuatTruocBoSungTTK2,
            this.lblMaThueSuatSauBoSungTTK2,
            this.xrTableCell24});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell21.StylePriority.UsePadding = false;
            this.xrTableCell21.Text = "Mã xác định thuế suất";
            this.xrTableCell21.Weight = 0.54424929027715008;
            // 
            // lblMaThueSuatTruocBoSungTTK2
            // 
            this.lblMaThueSuatTruocBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatTruocBoSungTTK2.Name = "lblMaThueSuatTruocBoSungTTK2";
            this.lblMaThueSuatTruocBoSungTTK2.StylePriority.UseBorders = false;
            this.lblMaThueSuatTruocBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatTruocBoSungTTK2.Text = "lblMaThueSuatTruocBoSungTTK";
            this.lblMaThueSuatTruocBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatTruocBoSungTTK2.Weight = 0.91029629037399917;
            // 
            // lblMaThueSuatSauBoSungTTK2
            // 
            this.lblMaThueSuatSauBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatSauBoSungTTK2.Name = "lblMaThueSuatSauBoSungTTK2";
            this.lblMaThueSuatSauBoSungTTK2.StylePriority.UseBorders = false;
            this.lblMaThueSuatSauBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatSauBoSungTTK2.Text = "lblMaThueSuatSauBoSungTTK";
            this.lblMaThueSuatSauBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatSauBoSungTTK2.Weight = 0.91698550389817934;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.62846891545067152;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.lblThueSuatTruocBoSungTTK2,
            this.lblThueSuatSauBoSungTTK2,
            this.xrTableCell29});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 1;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell26.StylePriority.UsePadding = false;
            this.xrTableCell26.Text = "Thuế suất";
            this.xrTableCell26.Weight = 0.54424929027715008;
            // 
            // lblThueSuatTruocBoSungTTK2
            // 
            this.lblThueSuatTruocBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatTruocBoSungTTK2.Name = "lblThueSuatTruocBoSungTTK2";
            this.lblThueSuatTruocBoSungTTK2.StylePriority.UseBorders = false;
            this.lblThueSuatTruocBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTruocBoSungTTK2.Text = "lblThueSuatTruocBoSungTTK";
            this.lblThueSuatTruocBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTruocBoSungTTK2.Weight = 0.91029629037399917;
            // 
            // lblThueSuatSauBoSungTTK2
            // 
            this.lblThueSuatSauBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatSauBoSungTTK2.Name = "lblThueSuatSauBoSungTTK2";
            this.lblThueSuatSauBoSungTTK2.StylePriority.UseBorders = false;
            this.lblThueSuatSauBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatSauBoSungTTK2.Text = "lblThueSuatSauBoSungTTK";
            this.lblThueSuatSauBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatSauBoSungTTK2.Weight = 0.91698550389817934;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.62846891545067152;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell30,
            this.lblSoTienThueTruocBoSungTTK2,
            this.lblMienThueTTKTruocBoSung2,
            this.lblSoTienThueSauBoSungTTK2,
            this.lblMienThueTTKSauBoSung2,
            this.lblTangGiamTTK2,
            this.lblSoTienTangGiamTTK2});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 1;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell30.StylePriority.UseBorders = false;
            this.xrTableCell30.StylePriority.UsePadding = false;
            this.xrTableCell30.Text = "Sô tiền thuế";
            this.xrTableCell30.Weight = 0.54424929027715008;
            // 
            // lblSoTienThueTruocBoSungTTK2
            // 
            this.lblSoTienThueTruocBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueTruocBoSungTTK2.Name = "lblSoTienThueTruocBoSungTTK2";
            this.lblSoTienThueTruocBoSungTTK2.StylePriority.UseBorders = false;
            this.lblSoTienThueTruocBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueTruocBoSungTTK2.Text = "lblSoTienThueTruocBoSungTTK";
            this.lblSoTienThueTruocBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueTruocBoSungTTK2.Weight = 0.80457986878954668;
            this.lblSoTienThueTruocBoSungTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKTruocBoSung2
            // 
            this.lblMienThueTTKTruocBoSung2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKTruocBoSung2.Name = "lblMienThueTTKTruocBoSung2";
            this.lblMienThueTTKTruocBoSung2.StylePriority.UseBorders = false;
            this.lblMienThueTTKTruocBoSung2.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKTruocBoSung2.Text = "X";
            this.lblMienThueTTKTruocBoSung2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKTruocBoSung2.Weight = 0.10571642158445253;
            // 
            // lblSoTienThueSauBoSungTTK2
            // 
            this.lblSoTienThueSauBoSungTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueSauBoSungTTK2.Name = "lblSoTienThueSauBoSungTTK2";
            this.lblSoTienThueSauBoSungTTK2.StylePriority.UseBorders = false;
            this.lblSoTienThueSauBoSungTTK2.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueSauBoSungTTK2.Text = "lblSoTienThueSauBoSungTTK";
            this.lblSoTienThueSauBoSungTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueSauBoSungTTK2.Weight = 0.80991722926620624;
            this.lblSoTienThueSauBoSungTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKSauBoSung2
            // 
            this.lblMienThueTTKSauBoSung2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKSauBoSung2.Name = "lblMienThueTTKSauBoSung2";
            this.lblMienThueTTKSauBoSung2.StylePriority.UseBorders = false;
            this.lblMienThueTTKSauBoSung2.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKSauBoSung2.Text = "X";
            this.lblMienThueTTKSauBoSung2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKSauBoSung2.Weight = 0.10706827463197322;
            // 
            // lblTangGiamTTK2
            // 
            this.lblTangGiamTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTangGiamTTK2.Name = "lblTangGiamTTK2";
            this.lblTangGiamTTK2.StylePriority.UseBorders = false;
            this.lblTangGiamTTK2.StylePriority.UseTextAlignment = false;
            this.lblTangGiamTTK2.Text = "X";
            this.lblTangGiamTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTangGiamTTK2.Weight = 0.081796441196410136;
            // 
            // lblSoTienTangGiamTTK2
            // 
            this.lblSoTienTangGiamTTK2.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienTangGiamTTK2.Name = "lblSoTienTangGiamTTK2";
            this.lblSoTienTangGiamTTK2.StylePriority.UseBorders = false;
            this.lblSoTienTangGiamTTK2.StylePriority.UseTextAlignment = false;
            this.lblSoTienTangGiamTTK2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienTangGiamTTK2.Weight = 0.54667247425426135;
            this.lblSoTienTangGiamTTK2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12});
            this.xrTable1.SizeF = new System.Drawing.SizeF(726F, 290F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoDong});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.Weight = 1;
            // 
            // lblSoDong
            // 
            this.lblSoDong.Name = "lblSoDong";
            this.lblSoDong.Text = "< NE >";
            this.lblSoDong.Weight = 3;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell8,
            this.lblSoThuTuHangTK});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell8.StylePriority.UsePadding = false;
            this.xrTableCell8.Text = "Số thứ tự dòng/hàng trên tờ khai gốc";
            this.xrTableCell8.Weight = 1;
            // 
            // lblSoThuTuHangTK
            // 
            this.lblSoThuTuHangTK.Name = "lblSoThuTuHangTK";
            this.lblSoThuTuHangTK.Text = "XE";
            this.lblSoThuTuHangTK.Weight = 2;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell1,
            this.xrTableCell18});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 1.4009985805543002;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Mô tả hàng hóa";
            this.xrTableCell1.Weight = 1.1775203736360407;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Mã nước xuất xứ";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell18.Weight = 0.42148104580965906;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblMoTaHangTruocBoSung,
            this.lblMaNuocXXTruocBoSung});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 2.25;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell19.StylePriority.UsePadding = false;
            this.xrTableCell19.Text = "Trước khi khai báo";
            this.xrTableCell19.Weight = 0.54424935332999747;
            // 
            // lblMoTaHangTruocBoSung
            // 
            this.lblMoTaHangTruocBoSung.Name = "lblMoTaHangTruocBoSung";
            this.lblMoTaHangTruocBoSung.Text = "lblMoTaHangTruocBoSung";
            this.lblMoTaHangTruocBoSung.Weight = 2.0342696008603434;
            // 
            // lblMaNuocXXTruocBoSung
            // 
            this.lblMaNuocXXTruocBoSung.Name = "lblMaNuocXXTruocBoSung";
            this.lblMaNuocXXTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblMaNuocXXTruocBoSung.Text = "XE";
            this.lblMaNuocXXTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaNuocXXTruocBoSung.Weight = 0.42148104580965906;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.lblMoTaHangSauBoSung,
            this.lblMaNuocXXSauBoSung});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 2.25;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell25.StylePriority.UsePadding = false;
            this.xrTableCell25.Text = "Sau khi khai báo";
            this.xrTableCell25.Weight = 0.54424935332999747;
            // 
            // lblMoTaHangSauBoSung
            // 
            this.lblMoTaHangSauBoSung.Name = "lblMoTaHangSauBoSung";
            this.lblMoTaHangSauBoSung.Text = "lblMoTaHangSauBoSung";
            this.lblMoTaHangSauBoSung.Weight = 2.0342696008603434;
            // 
            // lblMaNuocXXSauBoSung
            // 
            this.lblMaNuocXXSauBoSung.Name = "lblMaNuocXXSauBoSung";
            this.lblMaNuocXXSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblMaNuocXXSauBoSung.Text = "XE";
            this.lblMaNuocXXSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaNuocXXSauBoSung.Weight = 0.42148104580965906;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell34,
            this.xrTableCell36});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 1;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell32.StylePriority.UseBorders = false;
            this.xrTableCell32.StylePriority.UsePadding = false;
            this.xrTableCell32.StylePriority.UseTextAlignment = false;
            this.xrTableCell32.Text = "Trước khi khai báo";
            this.xrTableCell32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell32.Weight = 1.4545454545454546;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.StylePriority.UseBorders = false;
            this.xrTableCell34.StylePriority.UseTextAlignment = false;
            this.xrTableCell34.Text = "Sau khi khai báo";
            this.xrTableCell34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell34.Weight = 0.9169856300038739;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.StylePriority.UseTextAlignment = false;
            this.xrTableCell36.Text = "Tăng/giảm thuế";
            this.xrTableCell36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell36.Weight = 0.62846891545067152;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell40,
            this.xrTableCell42});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.54424935332999747;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.StylePriority.UseBorders = false;
            this.xrTableCell38.Text = "Thuế xuất nhập khẩu";
            this.xrTableCell38.Weight = 0.91029616426830462;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.StylePriority.UseBorders = false;
            this.xrTableCell40.Weight = 0.91698556695102651;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Weight = 0.62846891545067152;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.lblTriGiaTruocBoSung,
            this.lblTriGiaSauBoSung,
            this.xrTableCell48});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell43.StylePriority.UsePadding = false;
            this.xrTableCell43.Text = "Trị giá tính thuế";
            this.xrTableCell43.Weight = 0.54424935332999747;
            // 
            // lblTriGiaTruocBoSung
            // 
            this.lblTriGiaTruocBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaTruocBoSung.Name = "lblTriGiaTruocBoSung";
            this.lblTriGiaTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTriGiaTruocBoSung.StylePriority.UseBorders = false;
            this.lblTriGiaTruocBoSung.StylePriority.UsePadding = false;
            this.lblTriGiaTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTruocBoSung.Text = "12,345,678,901,234,567";
            this.lblTriGiaTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaTruocBoSung.Weight = 0.91029616426830462;
            this.lblTriGiaTruocBoSung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaSauBoSung
            // 
            this.lblTriGiaSauBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaSauBoSung.Name = "lblTriGiaSauBoSung";
            this.lblTriGiaSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTriGiaSauBoSung.StylePriority.UseBorders = false;
            this.lblTriGiaSauBoSung.StylePriority.UsePadding = false;
            this.lblTriGiaSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblTriGiaSauBoSung.Text = "12,345,678,901,234,567";
            this.lblTriGiaSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaSauBoSung.Weight = 0.91698556695102651;
            this.lblTriGiaSauBoSung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.62846891545067152;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.lblSoLuongTruocBoSung,
            this.lblMaDVTSLTruocBoSung,
            this.lblSoLuongSauBoSung,
            this.lblMaDVTSLSauBoSung,
            this.xrTableCell54});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell49.StylePriority.UsePadding = false;
            this.xrTableCell49.Text = "Số lượng tính thuế";
            this.xrTableCell49.Weight = 0.54424935332999747;
            // 
            // lblSoLuongTruocBoSung
            // 
            this.lblSoLuongTruocBoSung.Name = "lblSoLuongTruocBoSung";
            this.lblSoLuongTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblSoLuongTruocBoSung.StylePriority.UsePadding = false;
            this.lblSoLuongTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTruocBoSung.Text = "123,456,789,012";
            this.lblSoLuongTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongTruocBoSung.Weight = 0.71831953821103434;
            this.lblSoLuongTruocBoSung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLTruocBoSung
            // 
            this.lblMaDVTSLTruocBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLTruocBoSung.Name = "lblMaDVTSLTruocBoSung";
            this.lblMaDVTSLTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMaDVTSLTruocBoSung.StylePriority.UseBorders = false;
            this.lblMaDVTSLTruocBoSung.StylePriority.UsePadding = false;
            this.lblMaDVTSLTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLTruocBoSung.Text = "XXXE";
            this.lblMaDVTSLTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLTruocBoSung.Weight = 0.19197656300442284;
            // 
            // lblSoLuongSauBoSung
            // 
            this.lblSoLuongSauBoSung.Name = "lblSoLuongSauBoSung";
            this.lblSoLuongSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblSoLuongSauBoSung.StylePriority.UsePadding = false;
            this.lblSoLuongSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblSoLuongSauBoSung.Text = "lblSoLuongSauBoSung";
            this.lblSoLuongSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongSauBoSung.Weight = 0.71830579269030859;
            this.lblSoLuongSauBoSung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLSauBoSung
            // 
            this.lblMaDVTSLSauBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLSauBoSung.Name = "lblMaDVTSLSauBoSung";
            this.lblMaDVTSLSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMaDVTSLSauBoSung.StylePriority.UseBorders = false;
            this.lblMaDVTSLSauBoSung.StylePriority.UsePadding = false;
            this.lblMaDVTSLSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLSauBoSung.Text = "XXXE";
            this.lblMaDVTSLSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLSauBoSung.Weight = 0.19867983731356534;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 0.62846891545067152;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.lblMaHSTruocBoSung,
            this.lblMaHSSauBoSung,
            this.xrTableCell60});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell55.StylePriority.UsePadding = false;
            this.xrTableCell55.Text = "Mã số hàng hóa";
            this.xrTableCell55.Weight = 0.54424935332999747;
            // 
            // lblMaHSTruocBoSung
            // 
            this.lblMaHSTruocBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaHSTruocBoSung.Name = "lblMaHSTruocBoSung";
            this.lblMaHSTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMaHSTruocBoSung.StylePriority.UseBorders = false;
            this.lblMaHSTruocBoSung.StylePriority.UsePadding = false;
            this.lblMaHSTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblMaHSTruocBoSung.Text = "lblMaHSTruocBoSung";
            this.lblMaHSTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaHSTruocBoSung.Weight = 0.91029616426830462;
            // 
            // lblMaHSSauBoSung
            // 
            this.lblMaHSSauBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaHSSauBoSung.Name = "lblMaHSSauBoSung";
            this.lblMaHSSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMaHSSauBoSung.StylePriority.UseBorders = false;
            this.lblMaHSSauBoSung.StylePriority.UsePadding = false;
            this.lblMaHSSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblMaHSSauBoSung.Text = "lblMaHSSauBoSung";
            this.lblMaHSSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaHSSauBoSung.Weight = 0.91698556695102651;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Weight = 0.62846891545067152;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61,
            this.lblThueSuatTruocBoSung,
            this.lblThueSuatSauBoSung,
            this.xrTableCell66});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell61.StylePriority.UsePadding = false;
            this.xrTableCell61.Text = "Thuế suất";
            this.xrTableCell61.Weight = 0.54424935332999747;
            // 
            // lblThueSuatTruocBoSung
            // 
            this.lblThueSuatTruocBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatTruocBoSung.Name = "lblThueSuatTruocBoSung";
            this.lblThueSuatTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblThueSuatTruocBoSung.StylePriority.UseBorders = false;
            this.lblThueSuatTruocBoSung.StylePriority.UsePadding = false;
            this.lblThueSuatTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTruocBoSung.Text = "lblThueSuatTruocBoSung";
            this.lblThueSuatTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTruocBoSung.Weight = 0.91029616426830462;
            // 
            // lblThueSuatSauBoSung
            // 
            this.lblThueSuatSauBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatSauBoSung.Name = "lblThueSuatSauBoSung";
            this.lblThueSuatSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblThueSuatSauBoSung.StylePriority.UseBorders = false;
            this.lblThueSuatSauBoSung.StylePriority.UsePadding = false;
            this.lblThueSuatSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblThueSuatSauBoSung.Text = "lblThueSuatSauBoSung";
            this.lblThueSuatSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatSauBoSung.Weight = 0.91698556695102651;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 0.62846891545067152;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.lblTienThueTruocBoSung,
            this.lblMienThueTruocBoSung,
            this.lblTienThueSauBoSung,
            this.lblMienThueSauBoSung,
            this.lblTangGiamThueXNK,
            this.lblSoTienTangGiamThue});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell67.StylePriority.UseBorders = false;
            this.xrTableCell67.StylePriority.UsePadding = false;
            this.xrTableCell67.Text = "Sô tiền thuế";
            this.xrTableCell67.Weight = 0.54424935332999747;
            // 
            // lblTienThueTruocBoSung
            // 
            this.lblTienThueTruocBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTienThueTruocBoSung.Name = "lblTienThueTruocBoSung";
            this.lblTienThueTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTienThueTruocBoSung.StylePriority.UseBorders = false;
            this.lblTienThueTruocBoSung.StylePriority.UsePadding = false;
            this.lblTienThueTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblTienThueTruocBoSung.Text = "lblTienThueTruocBoSung";
            this.lblTienThueTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueTruocBoSung.Weight = 0.80457974268385213;
            this.lblTienThueTruocBoSung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTruocBoSung
            // 
            this.lblMienThueTruocBoSung.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTruocBoSung.Name = "lblMienThueTruocBoSung";
            this.lblMienThueTruocBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMienThueTruocBoSung.StylePriority.UseBorders = false;
            this.lblMienThueTruocBoSung.StylePriority.UsePadding = false;
            this.lblMienThueTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblMienThueTruocBoSung.Text = "X";
            this.lblMienThueTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTruocBoSung.Weight = 0.10571635853160519;
            // 
            // lblTienThueSauBoSung
            // 
            this.lblTienThueSauBoSung.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTienThueSauBoSung.Name = "lblTienThueSauBoSung";
            this.lblTienThueSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTienThueSauBoSung.StylePriority.UseBorders = false;
            this.lblTienThueSauBoSung.StylePriority.UsePadding = false;
            this.lblTienThueSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblTienThueSauBoSung.Text = "lblTienThueSauBoSung";
            this.lblTienThueSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTienThueSauBoSung.Weight = 0.80991760758329023;
            this.lblTienThueSauBoSung.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueSauBoSung
            // 
            this.lblMienThueSauBoSung.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueSauBoSung.Name = "lblMienThueSauBoSung";
            this.lblMienThueSauBoSung.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblMienThueSauBoSung.StylePriority.UseBorders = false;
            this.lblMienThueSauBoSung.StylePriority.UsePadding = false;
            this.lblMienThueSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblMienThueSauBoSung.Text = "X";
            this.lblMienThueSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueSauBoSung.Weight = 0.10706802242058369;
            // 
            // lblTangGiamThueXNK
            // 
            this.lblTangGiamThueXNK.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTangGiamThueXNK.Name = "lblTangGiamThueXNK";
            this.lblTangGiamThueXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblTangGiamThueXNK.StylePriority.UseBorders = false;
            this.lblTangGiamThueXNK.StylePriority.UsePadding = false;
            this.lblTangGiamThueXNK.StylePriority.UseTextAlignment = false;
            this.lblTangGiamThueXNK.Text = "X";
            this.lblTangGiamThueXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTangGiamThueXNK.Weight = 0.0817966934077996;
            // 
            // lblSoTienTangGiamThue
            // 
            this.lblSoTienTangGiamThue.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienTangGiamThue.Name = "lblSoTienTangGiamThue";
            this.lblSoTienTangGiamThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 3, 0, 0, 100F);
            this.lblSoTienTangGiamThue.StylePriority.UseBorders = false;
            this.lblSoTienTangGiamThue.StylePriority.UsePadding = false;
            this.lblSoTienTangGiamThue.StylePriority.UseTextAlignment = false;
            this.lblSoTienTangGiamThue.Text = "1,234,567,890,123,456";
            this.lblSoTienTangGiamThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienTangGiamThue.Weight = 0.54667222204287191;
            this.lblSoTienTangGiamThue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(1.000007F, 290F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow13,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow14,
            this.xrTableRow17,
            this.xrTableRow18});
            this.xrTable2.SizeF = new System.Drawing.SizeF(726F, 120F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell73,
            this.lblMaTiepNhanTangGiamTTK,
            this.lblTenTiepNhanTangGiamTTK,
            this.xrTableCell74,
            this.xrTableCell75});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 1;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.StylePriority.UseBorders = false;
            this.xrTableCell73.Weight = 0.54424929027715008;
            // 
            // lblMaTiepNhanTangGiamTTK
            // 
            this.lblMaTiepNhanTangGiamTTK.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.lblMaTiepNhanTangGiamTTK.Name = "lblMaTiepNhanTangGiamTTK";
            this.lblMaTiepNhanTangGiamTTK.StylePriority.UseBorders = false;
            this.lblMaTiepNhanTangGiamTTK.Text = "X";
            this.lblMaTiepNhanTangGiamTTK.Weight = 0.10175600918856535;
            // 
            // lblTenTiepNhanTangGiamTTK
            // 
            this.lblTenTiepNhanTangGiamTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTenTiepNhanTangGiamTTK.Name = "lblTenTiepNhanTangGiamTTK";
            this.lblTenTiepNhanTangGiamTTK.StylePriority.UseBorders = false;
            this.lblTenTiepNhanTangGiamTTK.Text = "lblTenTiepNhanTangGiamTTK";
            this.lblTenTiepNhanTangGiamTTK.Weight = 0.80854028118543386;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.StylePriority.UseBorders = false;
            this.xrTableCell74.Weight = 0.91698550389817934;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.StylePriority.UseBorders = false;
            this.xrTableCell75.Weight = 0.62846891545067152;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.lblTriGiaTruocBoSungTTK,
            this.lblTriGiaSauBoSungTTK,
            this.xrTableCell81});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 1;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell79.StylePriority.UsePadding = false;
            this.xrTableCell79.Text = "Trị giá tính thuế";
            this.xrTableCell79.Weight = 0.54424929027715008;
            // 
            // lblTriGiaTruocBoSungTTK
            // 
            this.lblTriGiaTruocBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaTruocBoSungTTK.Name = "lblTriGiaTruocBoSungTTK";
            this.lblTriGiaTruocBoSungTTK.StylePriority.UseBorders = false;
            this.lblTriGiaTruocBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTruocBoSungTTK.Text = "12,345,678,901,234,567";
            this.lblTriGiaTruocBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaTruocBoSungTTK.Weight = 0.91029629037399917;
            this.lblTriGiaTruocBoSungTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaSauBoSungTTK
            // 
            this.lblTriGiaSauBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblTriGiaSauBoSungTTK.Name = "lblTriGiaSauBoSungTTK";
            this.lblTriGiaSauBoSungTTK.StylePriority.UseBorders = false;
            this.lblTriGiaSauBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblTriGiaSauBoSungTTK.Text = "12,345,678,901,234,567";
            this.lblTriGiaSauBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTriGiaSauBoSungTTK.Weight = 0.91698550389817934;
            this.lblTriGiaSauBoSungTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.62846891545067152;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell82,
            this.lblSoLuongTruocBoSungTTK,
            this.lblMaDVTTruocBoSungTTK,
            this.lblSoLuongSauBoSungTTK,
            this.lblMaDVTSLSauBoSungTTK,
            this.xrTableCell84});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell82.StylePriority.UsePadding = false;
            this.xrTableCell82.Text = "Số lượng tính thuế";
            this.xrTableCell82.Weight = 0.54424929027715008;
            // 
            // lblSoLuongTruocBoSungTTK
            // 
            this.lblSoLuongTruocBoSungTTK.Name = "lblSoLuongTruocBoSungTTK";
            this.lblSoLuongTruocBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTruocBoSungTTK.Text = "lblSoLuongTruocBoSungTTK";
            this.lblSoLuongTruocBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongTruocBoSungTTK.Weight = 0.71831953821103434;
            this.lblSoLuongTruocBoSungTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTTruocBoSungTTK
            // 
            this.lblMaDVTTruocBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTTruocBoSungTTK.Name = "lblMaDVTTruocBoSungTTK";
            this.lblMaDVTTruocBoSungTTK.StylePriority.UseBorders = false;
            this.lblMaDVTTruocBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblMaDVTTruocBoSungTTK.Text = "XXXE";
            this.lblMaDVTTruocBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTTruocBoSungTTK.Weight = 0.19197675216296489;
            // 
            // lblSoLuongSauBoSungTTK
            // 
            this.lblSoLuongSauBoSungTTK.Name = "lblSoLuongSauBoSungTTK";
            this.lblSoLuongSauBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblSoLuongSauBoSungTTK.Text = "lblSoLuongSauBoSungTTK";
            this.lblSoLuongSauBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoLuongSauBoSungTTK.Weight = 0.71830572963746131;
            this.lblSoLuongSauBoSungTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDVTSLSauBoSungTTK
            // 
            this.lblMaDVTSLSauBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaDVTSLSauBoSungTTK.Name = "lblMaDVTSLSauBoSungTTK";
            this.lblMaDVTSLSauBoSungTTK.StylePriority.UseBorders = false;
            this.lblMaDVTSLSauBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblMaDVTSLSauBoSungTTK.Text = "XXXE";
            this.lblMaDVTSLSauBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaDVTSLSauBoSungTTK.Weight = 0.19867977426071798;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Weight = 0.62846891545067152;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell76,
            this.lblMaThueSuatTruocBoSungTTK,
            this.lblMaThueSuatSauBoSungTTK,
            this.xrTableCell78});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell76.StylePriority.UsePadding = false;
            this.xrTableCell76.Text = "Mã xác định thuế suất";
            this.xrTableCell76.Weight = 0.54424929027715008;
            // 
            // lblMaThueSuatTruocBoSungTTK
            // 
            this.lblMaThueSuatTruocBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatTruocBoSungTTK.Name = "lblMaThueSuatTruocBoSungTTK";
            this.lblMaThueSuatTruocBoSungTTK.StylePriority.UseBorders = false;
            this.lblMaThueSuatTruocBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatTruocBoSungTTK.Text = "lblMaThueSuatTruocBoSungTTK";
            this.lblMaThueSuatTruocBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatTruocBoSungTTK.Weight = 0.91029629037399917;
            // 
            // lblMaThueSuatSauBoSungTTK
            // 
            this.lblMaThueSuatSauBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblMaThueSuatSauBoSungTTK.Name = "lblMaThueSuatSauBoSungTTK";
            this.lblMaThueSuatSauBoSungTTK.StylePriority.UseBorders = false;
            this.lblMaThueSuatSauBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblMaThueSuatSauBoSungTTK.Text = "lblMaThueSuatSauBoSungTTK";
            this.lblMaThueSuatSauBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMaThueSuatSauBoSungTTK.Weight = 0.91698550389817934;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.62846891545067152;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell85,
            this.lblThueSuatTruocBoSungTTK,
            this.lblThueSuatSauBoSungTTK,
            this.xrTableCell87});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell85.StylePriority.UsePadding = false;
            this.xrTableCell85.Text = "Thuế suất";
            this.xrTableCell85.Weight = 0.54424929027715008;
            // 
            // lblThueSuatTruocBoSungTTK
            // 
            this.lblThueSuatTruocBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatTruocBoSungTTK.Name = "lblThueSuatTruocBoSungTTK";
            this.lblThueSuatTruocBoSungTTK.StylePriority.UseBorders = false;
            this.lblThueSuatTruocBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblThueSuatTruocBoSungTTK.Text = "lblThueSuatTruocBoSungTTK";
            this.lblThueSuatTruocBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatTruocBoSungTTK.Weight = 0.91029629037399917;
            // 
            // lblThueSuatSauBoSungTTK
            // 
            this.lblThueSuatSauBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Right;
            this.lblThueSuatSauBoSungTTK.Name = "lblThueSuatSauBoSungTTK";
            this.lblThueSuatSauBoSungTTK.StylePriority.UseBorders = false;
            this.lblThueSuatSauBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblThueSuatSauBoSungTTK.Text = "lblThueSuatSauBoSungTTK";
            this.lblThueSuatSauBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblThueSuatSauBoSungTTK.Weight = 0.91698550389817934;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.62846891545067152;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell88,
            this.lblSoTienThueTruocBoSungTTK,
            this.lblMienThueTTKTruocBoSung,
            this.lblSoTienThueSauBoSungTTK,
            this.lblMienThueTTKSauBoSung,
            this.lblTangGiamTTK,
            this.lblSoTienTangGiamTTK});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTableCell88.StylePriority.UseBorders = false;
            this.xrTableCell88.StylePriority.UsePadding = false;
            this.xrTableCell88.Text = "Sô tiền thuế";
            this.xrTableCell88.Weight = 0.54424929027715008;
            // 
            // lblSoTienThueTruocBoSungTTK
            // 
            this.lblSoTienThueTruocBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueTruocBoSungTTK.Name = "lblSoTienThueTruocBoSungTTK";
            this.lblSoTienThueTruocBoSungTTK.StylePriority.UseBorders = false;
            this.lblSoTienThueTruocBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueTruocBoSungTTK.Text = "lblSoTienThueTruocBoSungTTK";
            this.lblSoTienThueTruocBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueTruocBoSungTTK.Weight = 0.80457986878954668;
            this.lblSoTienThueTruocBoSungTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKTruocBoSung
            // 
            this.lblMienThueTTKTruocBoSung.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKTruocBoSung.Name = "lblMienThueTTKTruocBoSung";
            this.lblMienThueTTKTruocBoSung.StylePriority.UseBorders = false;
            this.lblMienThueTTKTruocBoSung.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKTruocBoSung.Text = "X";
            this.lblMienThueTTKTruocBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKTruocBoSung.Weight = 0.10571642158445253;
            // 
            // lblSoTienThueSauBoSungTTK
            // 
            this.lblSoTienThueSauBoSungTTK.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienThueSauBoSungTTK.Name = "lblSoTienThueSauBoSungTTK";
            this.lblSoTienThueSauBoSungTTK.StylePriority.UseBorders = false;
            this.lblSoTienThueSauBoSungTTK.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueSauBoSungTTK.Text = "lblSoTienThueSauBoSungTTK";
            this.lblSoTienThueSauBoSungTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienThueSauBoSungTTK.Weight = 0.80991722926620624;
            this.lblSoTienThueSauBoSungTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMienThueTTKSauBoSung
            // 
            this.lblMienThueTTKSauBoSung.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMienThueTTKSauBoSung.Name = "lblMienThueTTKSauBoSung";
            this.lblMienThueTTKSauBoSung.StylePriority.UseBorders = false;
            this.lblMienThueTTKSauBoSung.StylePriority.UseTextAlignment = false;
            this.lblMienThueTTKSauBoSung.Text = "X";
            this.lblMienThueTTKSauBoSung.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblMienThueTTKSauBoSung.Weight = 0.10706827463197322;
            // 
            // lblTangGiamTTK
            // 
            this.lblTangGiamTTK.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTangGiamTTK.Name = "lblTangGiamTTK";
            this.lblTangGiamTTK.StylePriority.UseBorders = false;
            this.lblTangGiamTTK.StylePriority.UseTextAlignment = false;
            this.lblTangGiamTTK.Text = "X";
            this.lblTangGiamTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblTangGiamTTK.Weight = 0.081796441196410136;
            // 
            // lblSoTienTangGiamTTK
            // 
            this.lblSoTienTangGiamTTK.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblSoTienTangGiamTTK.Name = "lblSoTienTangGiamTTK";
            this.lblSoTienTangGiamTTK.StylePriority.UseBorders = false;
            this.lblSoTienTangGiamTTK.StylePriority.UseTextAlignment = false;
            this.lblSoTienTangGiamTTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTienTangGiamTTK.Weight = 0.54667247425426135;
            this.lblSoTienTangGiamTTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // BanXacNhanNoiDungKhaiSuaDoiBoSung_2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(68, 55, 144, 23);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThuTuHangTK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHangTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocXXTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHangSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocXXSauBoSung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHSSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienTangGiamThue;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiBoSung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDKKhaiBoSung;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanNhan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell lblNhomXuLyHoSo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblDauHieuBaoQua;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayCapPhep;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiHanTaiNhapXuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaSauBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLSauBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatSauBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatSauBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienTangGiamTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDKKhaiBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayToKhaiXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTiepNhanTangGiamTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTruocBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTruocBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatTruocBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTruocBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueTruocBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblTienThueSauBoSung;
        private DevExpress.XtraReports.UI.XRTableCell lblTangGiamThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell lblTangGiamTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTiepNhanTangGiamTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTruocBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongSauBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueTruocBoSungTTK;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueSauBoSungTTK;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell lblSoPL;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTiepNhanTangGiamTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTiepNhanTangGiamTTK5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTruocBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaSauBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTruocBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTruocBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongSauBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLSauBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatTruocBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatSauBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTruocBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatSauBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueTruocBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKTruocBoSung5;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueSauBoSungTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKSauBoSung5;
        private DevExpress.XtraReports.UI.XRTableCell lblTangGiamTTK5;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienTangGiamTTK5;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTiepNhanTangGiamTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTiepNhanTangGiamTTK4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTruocBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaSauBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTruocBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTruocBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongSauBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLSauBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatTruocBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatSauBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTruocBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatSauBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueTruocBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKTruocBoSung4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueSauBoSungTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKSauBoSung4;
        private DevExpress.XtraReports.UI.XRTableCell lblTangGiamTTK4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienTangGiamTTK4;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTiepNhanTangGiamTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTiepNhanTangGiamTTK3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTruocBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaSauBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell57;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTruocBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTruocBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongSauBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLSauBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatTruocBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatSauBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTruocBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatSauBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueTruocBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKTruocBoSung3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueSauBoSungTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKSauBoSung3;
        private DevExpress.XtraReports.UI.XRTableCell lblTangGiamTTK3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienTangGiamTTK3;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTiepNhanTangGiamTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblTenTiepNhanTangGiamTTK2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTruocBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaSauBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTruocBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTTruocBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongSauBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDVTSLSauBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatTruocBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaThueSuatSauBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatTruocBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatSauBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueTruocBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKTruocBoSung2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueSauBoSungTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblMienThueTTKSauBoSung2;
        private DevExpress.XtraReports.UI.XRTableCell lblTangGiamTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienTangGiamTTK2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
    }
}
