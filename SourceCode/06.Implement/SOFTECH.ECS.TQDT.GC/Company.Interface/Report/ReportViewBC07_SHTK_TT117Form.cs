﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC07_SHTK_TT117Form : BaseForm
    {
        public DataSet ds = new DataSet();
        private ThanhKhoan07_ReiKer_TT117 BC07 = new ThanhKhoan07_ReiKer_TT117();
        public HopDong HD = new HopDong();
        public DataSet dsBK = new DataSet();
        public ReportViewBC07_SHTK_TT117Form()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            
                this.BC07.HD = this.HD;
                this.BC07.BindReport(dsBK.Tables[0]);
                printControl1.PrintingSystem = this.BC07.PrintingSystem;
                this.BC07.CreateDocument();
            
        }

        

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }

   
    }
}