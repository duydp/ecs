﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Logger;

namespace Company.Interface.VNACCS
{
    public partial class VNACCS_ChuyenThanhKhoan_New : BaseForm
    {
        public VNACCS_ChuyenThanhKhoan_New()
        {
            InitializeComponent();
        }

        private void VNACCS_ChuyenThanhKhoan_New_Load(object sender, EventArgs e)
        {
            SetError(string.Empty);
            loadData();

        }
        private void loadData()
        {
            try
            {
                //
                List<KDT_VNACC_ToKhaiMauDich_KhaiBoSung> listTK = KDT_VNACC_ToKhaiMauDich_KhaiBoSung.SelectCollectionAll();
                grList.DataSource = listTK;
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnThucHien_Click(object sender, EventArgs e)
        {
            btnThucHien.Enabled = false;
            btnDong.Enabled = false;
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }
        private void SetError(string error)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = error; }));
            }
            else
                lblError.Text = error;
        }
        private void SetProcessBar(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { uiProgressBar1.Value = value; SetError("Đang thực hiện ... " + value + " %"); }));
            }
            else
            {
                uiProgressBar1.Value = value;
                SetError("Đang thực hiện ... " + value + " %");
            }
        }
        private void DoWork(object obj)
        {
            try
            {

                SetError(string.Empty);
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                for (int i = 0; i < listChecked.Length;i++ )
                {
                    KDT_VNACC_HangMauDich_KhaiBoSung_Thue hmdKhaiBoSung = new KDT_VNACC_HangMauDich_KhaiBoSung_Thue();
                    hmdKhaiBoSung.LoadHangThuKhacCollection();
                    Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                    KDT_VNACC_ToKhaiMauDich_KhaiBoSung tkmdVNACCS = (KDT_VNACC_ToKhaiMauDich_KhaiBoSung)item.DataRow;
                    try
                    {
                        Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCSKhaiBoSung(tkmdVNACCS);
                    }
                    catch (System.Exception ex)
                    {
                        LocalLogger.Instance().WriteMessage(ex);
                        LocalLogger.Instance().WriteMessage(new Exception("Loi tai to khai " + tkmdVNACCS.SoToKhai));
                    }
                    SetProcessBar((i * 100 / listChecked.Length));
                }
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate 
                        { 
                            loadData(); 
                            btnThucHien.Enabled = true; btnDong.Enabled = true;
                            
                        }));
                }
                else
                {
                    loadData();
                    btnThucHien.Enabled = true; btnDong.Enabled = true;
                }
                SetProcessBar(0);
                SetError("Hoàn thành");
                
            }

            catch (System.Exception ex)
            {
                SetError(string.Empty);
                LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            Janus.Windows.GridEX.Export.GridEXExporter gridEXExport = new Janus.Windows.GridEX.Export.GridEXExporter();
            gridEXExport.GridEX = grList;
            
            //if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
            //{
            //    switch (e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString())
            //    {
            //        case "1":
            //            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng xanh";
            //            break;
            //        case "2":
            //            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng vàng";
            //            break;
            //        case "3":
            //            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng đỏ";
            //            break;
            //    }
            //}
        }

        private void ckbKhongHienThi_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ckbKhongHienThi.Checked)
                {
                    List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionDaVaChuaThanhKhoan(chkIsChuaThongQuan.Checked);
                    grList.DataSource = listTK;
                }
                else
                {
                    List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionChuaThanhKhoan(chkIsChuaThongQuan.Checked,true);
                    grList.DataSource = listTK;
                }
                grList.Refresh();
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void chkIsChuaThongQuan_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ckbKhongHienThi.Checked)
                {
                    List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionDaVaChuaThanhKhoan(chkIsChuaThongQuan.Checked);
                    grList.DataSource = listTK;
                }
                else
                {
                    List<KDT_VNACC_ToKhaiMauDich> listTK = KDT_VNACC_ToKhaiMauDich.SelectCollectionChuaThanhKhoan(chkIsChuaThongQuan.Checked,true);
                    grList.DataSource = listTK;
                }
                grList.Refresh();
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
