﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif

namespace Company.Interface
{
    public partial class ChungTuKemForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public ChungTuKem CTK = new ChungTuKem();
        public List<ChungTuKemChiTiet> ListCTDK = new List<ChungTuKemChiTiet>();
        public bool isKhaiBoSung = false;
        public bool isAddNew = true;
        string versionSend = Company.KDT.SHARE.Components.Globals.VersionSend;
        private string msgInfor = string.Empty;
        string filebase64 = "";
        long filesize = 0;
        public ChungTuKemForm()
        {
            InitializeComponent();
            CTK.TrangThaiXuLy = "-1";
        }

        /// <summary>
        /// Tinh tong dung luong
        /// </summary>
        /// <param name="FileInBytes"></param>
        /// <returns></returns>
        private string CalculateFileSize(decimal FileInBytes)
        {
            string strSize = "00";
            if (FileInBytes < 1024)
                strSize = FileInBytes + " B";//Byte
            else if (FileInBytes > 1024 & FileInBytes < 1048576)
                strSize = Math.Round((FileInBytes / 1024), 2) + " KB";//Kilobyte
            else if (FileInBytes > 1048576 & FileInBytes < 107341824)
                strSize = Math.Round((FileInBytes / 1024) / 1024, 2) + " MB";//Megabyte
            else if (FileInBytes > 107341824 & FileInBytes < 1099511627776)
                strSize = Math.Round(((FileInBytes / 1024) / 1024) / 1024, 2) + " GB";//Gigabyte
            else
                strSize = Math.Round((((FileInBytes / 1024) / 1024) / 1024) / 1024, 2) + " TB";//Terabyte
            return strSize;
        }
        /// <summary>
        /// Lay tong dung luong cac file dinh kem
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private void HienThiTongDungLuong(List<ChungTuKemChiTiet> list)
        {
            long size = 0;

            for (int i = 0; i < list.Count; i++)
            {
                size += Convert.ToInt64(list[i].FileSize);
            }


            //hien thi tong dung luong file
            //lblTongDungLuong.Text = Convert.ToString(size / (1024 * 1024)) + " MB (" + size.ToString() + " Bytes)";
            lblTongDungLuong.Text = CalculateFileSize(size);
            lblLuuY.Text = CalculateFileSize(GlobalSettings.FileSize);//Convert.ToString(GlobalSettings.FileSize / (1024 * 1024)) + " MB";
        }
        private void BindData()
        {
            dgList.DataSource = ListCTDK;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

            //Cap nhat thong tin tong dung luong file.
            HienThiTongDungLuong(ListCTDK);

        }

        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            //Ngay chung tu mac dinh
            ccNgayChungTu.Text = DateTime.Today.ToShortDateString();

            //Thiet lap nut Khai bao, Phan hoi mac dinh
            btnKhaiBao.Enabled = true;
            btnLayPhanHoi.Enabled = false;

            //Load danh sach Loai chung tu
            DataView dvLCT = LoaiChungTu.SelectAll().Tables[0].DefaultView;
            dvLCT.Sort = "ID ASC";
            cbLoaiCT.Items.Clear();
            for (int i = 0; i < dvLCT.Count; i++)
            {
                cbLoaiCT.Items.Add(dvLCT[i]["Ten"].ToString(), (int)dvLCT[i]["ID"]);
            }
            cbLoaiCT.SelectedIndex = cbLoaiCT.Items.Count > 0 ? 0 : -1;

            if (CTK.ID > 0)
            {
                ccNgayChungTu.Text = CTK.NGAY_CT.ToShortDateString();
                txtThongTinKhac.Text = CTK.DIENGIAI;
                txtSoChungTu.Text = CTK.SO_CT;
                cbLoaiCT.SelectedValue = CTK.MA_LOAI_CT;

                ListCTDK = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(CTK.ID);

                BindData();
            }
            else
            {
                //Cap nhat thong tin tong dung luong file.
                HienThiTongDungLuong(ListCTDK);
            }

            //if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO && !isKhaiBoSung)
            //{
            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (CTK.SOTN > 0 && CTK.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                txtSoTiepNhan.Text = CTK.SOTN.ToString("N0");
                ccNgayTiepNhan.Text = CTK.NGAYTN.ToShortDateString();
            }
            else if (CTK.SOTN > 0 && CTK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
                txtSoTiepNhan.Text = CTK.SOTN.ToString("N0");
                ccNgayTiepNhan.Text = CTK.NGAYTN.ToShortDateString();
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //}

            txtSoChungTu.Focus();

            // Thiết lập trạng thái các nút trên form.
            // HUNGTQ, Update 07/06/2010.
            SetButtonStateCHUNGTUKEM(TKMD, CTK);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuKemChiTiet> HangGiayPhepDetailCollection = new List<ChungTuKemChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKemChiTiet hgpDetail = new ChungTuKemChiTiet();
                        hgpDetail = (ChungTuKemChiTiet)i.GetRow().DataRow;

                        if (hgpDetail == null) continue;

                        //Detele from DB.
                        if (hgpDetail.ID > 0)
                            hgpDetail.Delete();

                        //Remove out Collction
                        ListCTDK.Remove(hgpDetail);
                    }
                }
            }

            BindData();
        }

        private ChungTuKem checkSoCTK(string soCT)
        {


            foreach (ChungTuKem ctk in TKMD.ChungTuKemCollection)
            {
                if (ctk.SO_CT.Trim().ToUpper() == soCT.Trim().ToUpper())
                    return ctk;
            }
            return null;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            bool isValidate = ValidateControl.ValidateDate(ccNgayChungTu, epError, "Ngày chứng từ không hợp lệ");
            if (!isValidate) return;
            if (TKMD.ID <= 0)
            {
                ShowMessage("Vui lòng lưu tờ khai trước", false);
                return;
            }
            ChungTuKem ctkExist = checkSoCTK(txtSoChungTu.Text);
            if (ctkExist != null)
            {
                //ShowMessage("Chứng từ đã tồn tại", false);
                //return;
                CTK = ctkExist;
            }
            if (ListCTDK == null || ListCTDK.Count == 0)
            {
                ShowMessage("Bạn chưa thêm file chứng từ dạng ảnh", false);
                return;
            }
            //Cap nhat lai danh sach file dinh kem chi tiet
            CTK.listCTChiTiet = ListCTDK;

            //TKMD.ChungTuKemCollection.Remove(chungTuKem);


            CTK.SO_CT = txtSoChungTu.Text.Trim();
            CTK.DIENGIAI = txtThongTinKhac.Text;
            CTK.MA_LOAI_CT = cbLoaiCT.SelectedValue.ToString();
            CTK.NGAY_CT = ccNgayChungTu.Value;
            CTK.TKMDID = TKMD.ID;

            //Khai bo sung
            if (isKhaiBoSung)
                CTK.LoaiKB = "1";
            else
                CTK.LoaiKB = "0";

            try
            {
                if (string.IsNullOrEmpty(CTK.GUIDSTR))
                    CTK.GUIDSTR = Guid.NewGuid().ToString();
                isAddNew = CTK.ID == 0;
                CTK.InsertUpdateFull(ListCTDK);

                if (isAddNew)
                    TKMD.ChungTuKemCollection.Add(CTK);

                BindData();

                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
                return;
            }

        }

        static public string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }

        static public string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes

                = System.Convert.FromBase64String(encodedData);

            string returnValue =

               System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "Image Files(*.BMP;*.JPG;*.GIF;*.tif;*.tiff;*.jpeg;*png)|*.BMP;*.JPG;*.GIF;*.tif;*.tiff;*.jpeg;*png";
            //"(*pnj)|*.pnj| (*.bmp) |TIFF (*.tif; *.tiff)|*.tif;*.tiff|(*.gif)|*.gif";
            openFileDialog1.Multiselect = true;

            try
            {
                if (openFileDialog1.ShowDialog(this) != DialogResult.Cancel)
                {
                    for (int k = 0; k < openFileDialog1.FileNames.Length; k++)
                    {
                        System.IO.FileInfo fin = new System.IO.FileInfo(openFileDialog1.FileNames[k]);
                        if (fin.Extension.ToUpper() != ".BMP" && fin.Extension.ToUpper() != ".JPG" && fin.Extension.ToUpper() != ".GIF" && fin.Extension.ToUpper() != ".TIF" &&
                            fin.Extension.ToUpper() != ".TIFF" && fin.Extension.ToUpper() != ".JPEG" && fin.Extension.ToUpper() != ".PNG")
                        {
                            ShowMessage("File " + fin.Name + "Không đúng định dạng tiếp nhận của Hải Quan", false);
                        }
                        else
                        {
                            System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileNames[k], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                            //Cap nhat tong dung luong file.
                            long size = 0;

                            for (int i = 0; i < ListCTDK.Count; i++)
                            {
                                size += Convert.ToInt64(ListCTDK[i].FileSize);
                                lblTongDungLuong.Text = CalculateFileSize(size);
                                
                            }

                            //+ them dung luong file moi chuan bi them vao danh sach
                            size += fs.Length;

                            //Kiem tra dung luong file
                            if (size > GlobalSettings.FileSize)
                            {
                                this.ShowMessage(string.Format("Tổng dung lượng cho phép {0}\r\nDung lượng bạn nhập {1} đã vượt mức cho phép.", CalculateFileSize(GlobalSettings.FileSize), CalculateFileSize(size)), false);
                                return;
                            }

                            byte[] data = new byte[fs.Length];
                            fs.Read(data, 0, data.Length);
                            filebase64 = System.Convert.ToBase64String(data);
                            filesize = fs.Length;

                            /*
                             * truoc khi them moi file dinh kem vao danh sach, phai kiem tra tong dung luong co hop len khong?.
                             * Neu > dung luong choh phep -> Hien thi thong bao va khong them vao danh sach.
                             */
                            ChungTuKemChiTiet ctctiet = new ChungTuKemChiTiet();
                            ctctiet.ChungTuKemID = CTK.ID;
                            ctctiet.FileName = fin.Name;
                            ctctiet.FileSize = filesize;
                            ctctiet.NoiDung = data;

                            ListCTDK.Add(ctctiet);
                            dgList.DataSource = ListCTDK;
                            try
                            {
                                dgList.Refetch();
                            }
                            catch { dgList.Refresh(); }
                        }
                    }

                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            //kiem tra thu da co muc temp?. Neu chua co -> tao 1 thu muc temp de chua cac file tam thoi.
            string path = Application.StartupPath + "\\Temp";

            if (System.IO.Directory.Exists(path) == false)
            {
                System.IO.Directory.CreateDirectory(path);
            }

            //Giai ma basecode64 -> file & luu tam vao thu muc Temp moi tao.
            if (dgList.GetRow() == null)
            {
                ShowMessage("Chưa chọn file để xem", false);
                return;
            }
            ChungTuKemChiTiet fileData = (ChungTuKemChiTiet)dgList.GetRow().DataRow;
            string fileName = path + "\\" + fileData.FileName;

            //Ghi file
            if (System.IO.File.Exists(fileName))
            {
                System.IO.File.Delete(fileName);
            }

            System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
            fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
            fs.Close();

            System.Diagnostics.Process.Start(fileName);
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {

            try
            {
                if (CTK.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO.ToString() || CTK.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET.ToString())
                    CTK.GUIDSTR = Guid.NewGuid().ToString();
                else if (CTK.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI.ToString())
                {
                    ShowMessage("Bạn đã gửi yêu cầu khai báo bổ đến hải quan\r\nVui lòng nhận phản hồi thông tin", false);
                    return;
                }
                #region V3

                ObjectSend msgSend = SingleMessage.BoSungChungTuKem(TKMD, CTK);

                CTK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO.ToString();
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                bool isSend = sendMessageForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendMessageForm.Message.XmlSaveMessage(CTK.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKem);
                    CTK.TrangThaiXuLy = TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI.ToString();
                    CTK.Update();
                }
                else if (isSend && feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    ShowMessageTQDT(msgInfor, false);
                }
                SetButtonStateCHUNGTUKEM(TKMD, CTK);

                #endregion
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
// 
//         private void KhaiBaoChungTuKem()
//         {
//             string password = "";
//             WSForm wsForm = new WSForm();
//             try
//             {
//                 if (GlobalSettings.PassWordDT == "")
//                 {
//                     wsForm.ShowDialog(this);
//                     if (!wsForm.IsReady) return;
//                 }
//                 password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
// 
//                 bool thanhcong = CTK.WSKhaiBaoChungTuDinhKem(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, ListCTDK, MessageTypes.DanhMucNguyenPhuLieu, MessageFunctions.KhaiBao);
//                 if (thanhcong)
//                 {
//                     this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);
// 
//                     btnKhaiBao.Enabled = false;
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     btnKhaiBao.Enabled = true;
//                     btnLayPhanHoi.Enabled = true;
//                 }
//             }
//             catch (Exception ex)
//             {
//                 ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
//         }

        /// <summary>
        /// Lấy phản hồi sau khi khai báo. Mục đích là lấy số tiếp nhận điện tử.
        /// </summary>
        private void LayPhanHoiKhaiBao(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = CTK.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnKhaiBao.Enabled = true;
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.CTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong);
//                     this.ShowMessage(message, false);
// 
//                     btnKhaiBao.Enabled = false;
//                     txtSoTiepNhan.Text = this.CTK.SOTN.ToString("N0");
//                     ccNgayTiepNhan.Value = this.CTK.NGAYTN;
//                     ccNgayTiepNhan.Text = this.CTK.NGAYTN.ToShortDateString();
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        /// <summary>
        /// Lấy thông tin phản hồi sau khi đã khai báo có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        private void LayPhanHoiDuyet(string password)
        {
//         StartInvoke:
//             try
//             {
//                 bool thanhcong = CTK.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
// 
//                 // Thực hiện kiểm tra.  
//                 if (thanhcong == false)
//                 {
//                     if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
//                     {
//                         this.Refresh();
//                         goto StartInvoke;
//                     }
// 
//                     btnLayPhanHoi.Enabled = true;
//                 }
//                 else
//                 {
//                     string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.CTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan);
//                     if (message.Length == 0)
//                         message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.CTK.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
// 
//                     this.ShowMessage(message, false);
//                     btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
//                 }
//             }
//             catch (Exception ex)
//             {
//                 this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
//             }
        }

        /// <summary>
        /// Lấy các thông tin phản hồi.
        /// </summary>
        private void LayThongTinPhanHoi()
        {
//             string password = string.Empty;
//             WSForm form = new WSForm();
//             if (GlobalSettings.PassWordDT == "")
//             {
//                 form.ShowDialog(this);
//                 if (!form.IsReady) return;
//             }
//             password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : form.txtMatKhau.Text.Trim();
// 
//             if (this.CTK.SOTN == 0)
//             {
//                 this.LayPhanHoiKhaiBao(password);
//             }
//             else if (this.CTK.SOTN > 0)
//             {
//                 this.LayPhanHoiDuyet(password);
//             }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBack(TKMD, CTK.GUIDSTR);
                SendMessageForm sendMessageForm = new SendMessageForm();
                sendMessageForm.Send += SendMessage;
                isFeedBack = sendMessageForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                        feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                        feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                        )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY) this.SetButtonStateCHUNGTUKEM(TKMD, CTK);
                }


            }
//             ObjectSend msgSend = SingleMessage.FeedBack(TKMD, CTK.GUIDSTR);
//             SendMessageForm sendMessageForm = new SendMessageForm();
//             sendMessageForm.Send += SendMessage;
//             sendMessageForm.DoSend(msgSend);
//             SetButtonStateCHUNGTUKEM(TKMD, CTK);
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (CTK.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
                {

                    //Xoa chung tu chi tiet
                    CTK.LoadListCTChiTiet();

                    for (int i = 0; i < CTK.listCTChiTiet.Count; i++)
                    {
                        CTK.listCTChiTiet[i].Delete();
                    }

                    //Xoa chung tu trong DB
                    CTK.Delete();

                    //Xoa chung tu trong Collection
                    TKMD.ChungTuKemCollection.Remove(CTK);

                    //Load lai danh sach chung tu kem cho TK
                    TKMD.LoadChungTuKem();
                }
            }

            //Dong form sau khi xoa chung tu.
            this.Close();
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUNGTUKEM.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private void SetButtonStateCHUNGTUKEM(ToKhaiMauDich tkmd, ChungTuKem chungTuKem)
        {
            if (chungTuKem == null)
                return;
            if (CTK.LoaiKB == "1")
            {
                switch (CTK.TrangThaiXuLy)
                {
                    case "-1":
                        lbTrangThai.Text = "Chưa Khai Báo";
                        break;
                    case "-3":
                        lbTrangThai.Text = "Đã Khai báo nhưng chưa có phản hồi từ Hải quan";
                        break;
                    case "0":
                        lbTrangThai.Text = "Chờ Duyệt";
                        break;
                    case "1":
                        lbTrangThai.Text = "Đã Duyệt";
                        break;
                    case "2":
                        lbTrangThai.Text = "Hải quan không phê duyệt";
                        break;
                }
            }
            else
                lbTrangThai.Text = string.Empty;
            bool status = false;
            btnXemFile.Enabled = true;
            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY);

                btnXoa.Enabled = status;
                btnXoaChungTu.Enabled = status;
                btnAddNew.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                if (chungTuKem.SOTN > 0)
                {
                    txtSoTiepNhan.Text = chungTuKem.SOTN.ToString();
                    ccNgayTiepNhan.Value = chungTuKem.NGAYTN;
                }
                btnKetQuaXuLy.Enabled = true;
                bool khaiBaoEnable = (int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.CHUA_KHAI_BAO ||
                                     int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.KHONG_PHE_DUYET) && chungTuKem.SOTN == 0;

                btnKhaiBao.Enabled = btnXoa.Enabled = btnXoaChungTu.Enabled = btnAddNew.Enabled = btnGhi.Enabled = khaiBaoEnable;

                bool layPhanHoi = int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ||
                                   int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.DA_DUYET ||
                                   int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.CHO_DUYET || int.Parse(chungTuKem.TrangThaiXuLy) == TrangThaiXuLy.KHONG_PHE_DUYET;
                btnLayPhanHoi.Enabled = layPhanHoi;
            }
        }

        #endregion

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (CTK.GUIDSTR != null && CTK.GUIDSTR != "")
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = CTK.ID;
                form.DeclarationIssuer = AdditionalDocumentType.CHUNG_TU_DANG_ANH;
                form.ShowDialog(this);
            }
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
        #region  V3

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public FeedBackContent feedbackContent;
        void SendHandler(object sender, SendEventArgs e)
        {
            try
            {
                if (e.Error == null)
                {

                    feedbackContent = Helpers.GetFeedBackContent(e.FeedBackMessage);
                    string noidung = feedbackContent.AdditionalInformations[0].Content.Text;
                    bool isUpdate = true;
                    if (noidung == "Sai mật khẩu, hãy kiểm tra lại ...")
                    {
                        GlobalSettings.IsRemember = false;
                    }
                    switch (feedbackContent.Function.Trim())
                    {
                        case DeclarationFunction.KHONG_CHAP_NHAN:
                            {

                                noidung = SingleMessage.GetErrorContent(feedbackContent);
                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.TuChoiTiepNhan, noidung);
                                CTK.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET.ToString();
                                noidung = "Hải quan từ chối tiếp nhận.\r\nLý do: " + noidung;
                                break;
                            }
                        case DeclarationFunction.CHUA_XU_LY:
                            //noidung = noidung.Replace(string.Format("Message [{0}]", CTK.GUIDSTR), string.Empty);
                            //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                            isUpdate = false;
                            break;
                        case DeclarationFunction.CAP_SO_TIEP_NHAN:
                            {
                                string[] vals = noidung.Split('/');

                                CTK.SOTN = long.Parse(vals[0].Trim());
                                if (feedbackContent.Acceptance.Length == 10)
                                    CTK.NGAYTN = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd", null);
                                else if (feedbackContent.Acceptance.Length == 19)
                                    CTK.NGAYTN = DateTime.ParseExact(feedbackContent.Acceptance, "yyyy-MM-dd HH:mm:ss", null);
                                else
                                    CTK.NGAYTN = DateTime.ParseExact(feedbackContent.Issue, "yyyy-MM-dd HH:mm:ss", null);

                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemCoSoTN, noidung);
                                CTK.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET.ToString();
                                noidung = string.Format("Hải quan cấp số tiếp nhận\r\nSố tiếp nhận: {0} \r\nNgày tiếp nhận: {1}\r\nLoại hình: Chứng từ đính kèm dạng ảnh\r\nHải quan: {2}", new object[] {
                                    CTK.SOTN.ToString(), CTK.NGAYTN.ToString("dd-MM-yyyy"), GlobalSettings.MA_DON_VI });
                                //this.ShowMessage(string.Format("Thông báo từ hệ thống hải quan : {0}", noidung), false);
                                break;
                            }
                        case DeclarationFunction.THONG_QUAN:
                            {
                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan, noidung);
                                CTK.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET.ToString();
                                noidung = "Chứng từ đã được duyệt.\n Thông tin trả về từ hải quan: " + noidung;
                            }
                            break;
                        default:
                            {
                                e.FeedBackMessage.XmlSaveMessage(CTK.ID, MessageTitle.Error, noidung);
                                break;
                            }
                    }
                    if (isUpdate)
                    {
                        CTK.Update();
                        SetButtonStateCHUNGTUKEM(TKMD, CTK);
                    }
                    msgInfor = noidung;
                }
                else
                {

                    this.ShowMessageTQDT("Lỗi kết nối hoặc hệ thống hải quan không xử lý được", e.Error.Message, false);
                }
            }
            catch (Exception ex)
            {
                if (!string.IsNullOrEmpty(e.FeedBackMessage))
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.FeedBackMessage, ex);
                    this.ShowMessageTQDT("Không hiểu thông tin trả về từ hải quan", e.FeedBackMessage, false);
                }
                else
                {
                    Logger.LocalLogger.Instance().WriteMessage(e.Error);
                    this.ShowMessageTQDT("Hệ thống không thể xử lý", e.Error.Message, false);
                }
            }
        }

        #endregion
    }
}

