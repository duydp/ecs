﻿using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.SXXK;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface.KDT.GC
{
    public partial class ToKhaiGCCTManagerForm : BaseForm
    {
        private ToKhaiChuyenTiepCollection tkctCollection = new ToKhaiChuyenTiepCollection();
        private string xmlCurrent = "";
        public ToKhaiGCCTManagerForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            //string where = string.Format("[ID] LIKE '{0}'", GlobalSettings.MA_CUC_HAI_QUAN);
            this._DonViHaiQuan = DonViHaiQuan.SelectAll();
            this._DonViTinh = DonViTinh.SelectAll();
        }
        private void SetComboTrangthai(Janus.Windows.EditControls.UIComboBox cboTrangThai)
        {
            cboTrangThai.Items.Add("Sửa tờ khai", Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET);
            //cboTrangThai.Items.Add("Chờ hủy", Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY);
            cboTrangThai.Items.Add("Đã hủy", Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY);
            cboTrangThai.Items.Add("Không phê duyệt", Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);
        }

        private void ToKhaiGCCTManagerForm_Load(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            //An nut xac nhan
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;
            //An nut in to khai (cu)
            ToKhaiViet.Visible = Janus.Windows.UI.InheritableBoolean.False;

            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            this.khoitao_DuLieuChuan();
            cbStatus.SelectedIndex = 0;
            //this.search();
            SetComboTrangthai(cbStatus);

            dgList.RootTable.Columns["NgayDangKy"].Width = 120;

            this.Cursor = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private ToKhaiChuyenTiep getTKCTByID(long id)
        {
            foreach (ToKhaiChuyenTiep tk in this.tkctCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                f.OpenType = OpenFormType.Edit;
                f.TKCT = (ToKhaiChuyenTiep)e.Row.DataRow;
                f.TKCT.Load();
                f.TKCT.LoadHCTCollection();
                f.TKCT.LoadChungTuKem();
                
                if (f.TKCT.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                    f.OpenType = OpenFormType.View;
                if (f.TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                    f.OpenType = OpenFormType.Edit;
                f.ShowDialog();
                search();
            }
        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            where += string.Format(" AND MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);

            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoTiepNhan = " + txtSoTiepNhan.Value;
            }

            if (Convert.ToInt32(cbStatus.SelectedValue) != TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                if (txtNamTiepNhan.TextLength > 0)
                {
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Value;
                }
            }
            if (cbStatus.SelectedValue != null)
                where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;

            // Thực hiện tìm kiếm.            
            this.tkctCollection = new ToKhaiChuyenTiep().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkctCollection;
            this.setCommandStatus();

            //this.currentNPLDangKy.TrangThaiXuLy = Convert.ToInt32(cbStatus.SelectedValue);
        }

        private void setCommandStatus()
        {
            switch (Convert.ToInt32(cbStatus.SelectedValue))
            {
                case -1:
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    cmdXuatToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    btnXoa.Enabled = true;
                    cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    break;
                case 0:
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                    cmdXuatToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnXoa.Enabled = false;
                    cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    break;
                case 1:
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCancel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSingleDownload.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    cmdXuatToKhai.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    btnXoa.Enabled = false;
                    cmdCSDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    InPhieuTN.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    break;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }
        private void Cancel()
        {
            MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            try
            {
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    this.Cursor = Cursors.WaitCursor;
                    tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkct.Load();

                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    if (sendXML.Load())
                    {
                        showMsg("MSG_STN01");
                        //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.","MSG_WRN05","", false);
                        return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();


                    long soTiepNhan = tkct.SoTiepNhan;

                    xmlCurrent = tkct.WSCancel(password);
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", ex.Message, false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi hủy Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private ToKhaiChuyenTiep search(object id)
        {
            foreach (ToKhaiChuyenTiep tkct in this.tkctCollection)
            {
                if (tkct.ID == Convert.ToInt64(id))
                {
                    return tkct;
                }
            }
            return null;
        }
        private void Download()
        {
            MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                        tkct.Load();

                        sendXML.LoaiHS = "TKCT";
                        sendXML.master_id = tkct.ID;
                        //if (!sendXML.Load())
                        //{
                        //    showMsg("MSG_STN01");
                        //    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_WRN05", "", false);
                        //    return;
                        //}


                        int ttxl = tkct.TrangThaiXuLy;

                        if (GlobalSettings.PassWordDT == "")
                        {
                            wsForm.ShowDialog(this);
                            if (!wsForm.IsReady) return;
                        }
                        if (GlobalSettings.PassWordDT != "")
                            password = GlobalSettings.PassWordDT;
                        else
                            password = wsForm.txtMatKhau.Text.Trim();
                        this.Cursor = Cursors.WaitCursor;
                        LayPhanHoi(password);

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage(ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void Send()
        {
            ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
            MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            WSForm wsForm = new WSForm();
            string password = "";
            try
            {
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    this.Cursor = Cursors.WaitCursor;
                    tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkct.Load();
                    tkct.LoadHCTCollection();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkct.ID;
                    if (sendXML.Load())
                    {
                        //showMsg("MSG_STN01");
                        ShowMessage("Tờ khai đã được gửi đến hải quan. Nhấn nút [Lấy phản hồi] để nhận thông tin từ hải quan", false);
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        // ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                        //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_WRN05", "", false);
                        return;
                    }

                    try
                    {
                        if (GlobalSettings.PassWordDT == "")
                        {
                            wsForm.ShowDialog(this);
                            if (!wsForm.IsReady) return;
                        }
                        if (GlobalSettings.PassWordDT != "")
                            password = GlobalSettings.PassWordDT;
                        else
                            password = wsForm.txtMatKhau.Text.Trim();
                        xmlCurrent = SOFTECH.ECS.V3.Components.GC.SHARE.ProcessXML.SendTKNhapGCCT(password, tkct.ID);
                        sendXML = new MsgSend();
                        sendXML.LoaiHS = "TKCT";
                        sendXML.master_id = tkct.ID;
                        sendXML.msg = xmlCurrent;
                        sendXML.func = 1;
                        xmlCurrent = "";
                        sendXML.InsertUpdate();
                        LayPhanHoi(password);

                    }
                    catch (Exception ex)
                    {
                        this.Cursor = Cursors.Default;
                        {
                            string[] msg = ex.Message.Split('|');
                            if (msg.Length == 2)
                            {
                                if (msg[1] == "DOTNET_LEVEL")
                                {
                                    //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                    //{
                                    //    HangDoi hd = new HangDoi();
                                    //    hd.ID = TKMD.ID;
                                    //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                    //    hd.TrangThai = TKMD.TrangThaiXuLy;
                                    //    hd.ChucNang = ChucNang.KHAI_BAO;
                                    //    hd.PassWord = password;
                                    //    MainForm.AddToQueueForm(hd);
                                    //    MainForm.ShowQueueForm();
                                    //}
                                    showMsg("MSG_WRN12");
                                    //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN15", "", false);
                                    return;
                                }
                                else
                                {
                                    showMsg("MSG_2702016", msg[0]);
                                    //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN12", msg[0], false);
                                }
                            }
                            else
                            {
                                if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                    showMsg("MSG_WRN13", ex.Message);
                                //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN16", ex.Message, false);
                                else
                                {
                                    GlobalSettings.PassWordDT = "";
                                    showMsg("MSG_2702004", ex.Message);
                                    //ShowMessage(ex.Message, false);
                                }
                                setCommandStatus();
                            }
                        }

                        StreamWriter write = File.AppendText("Error.txt");
                        write.WriteLine("--------------------------------");
                        write.WriteLine("Lỗi khi khai báo thông tin tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                        write.WriteLine(ex.StackTrace);
                        write.WriteLine("Lỗi là : ");
                        write.WriteLine(ex.Message);
                        write.WriteLine("--------------------------------");
                        write.Flush();
                        write.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                ;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            ToKhaiChuyenTiep tkmd = new ToKhaiChuyenTiep();
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            this.Cursor = Cursors.WaitCursor;
            try
            {
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    tkmd = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkmd.Load();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkmd.ID;
                    if (!sendXML.Load())
                    {
                        showMsg("MSG_STN01");
                        //MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01", "", false);
                        return;
                    }
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    if (GlobalSettings.PassWordDT != "")
                        password = GlobalSettings.PassWordDT;
                    else
                        password = wsForm.txtMatKhau.Text.Trim();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        xmlCurrent = tkmd.LayPhanHoi(password, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        doc.LoadXml(xmlCurrent);
                        node = doc.SelectSingleNode("Megs");
                        if (node == null)
                        {
                            if (showMsg("MSG_STN02", true) == "Yes")
                            //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", "MSG_STN02", "", true) == "Yes")
                            {
                                this.Refresh();
                                LayPhanHoi(password);
                            }
                            return;
                        }
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", tkmd.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, "MSG_SEN02", tkmd.SoTiepNhan.ToString(), false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702029");
                        //MLMessages("Đã hủy tờ khai này", "MSG_CAN01", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN11", new string[] { tkmd.SoToKhai.ToString(), mess });
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.", "MSG_SEN03", "", false);                            
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN04", mess);
                            //MLMessages("Hải quan chưa xử lý tờ khai này!", "MSG_STN06", "", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN05", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0],"","", false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LayPhanHoi(string pass)
        {
            ToKhaiChuyenTiep tkmd = new ToKhaiChuyenTiep();
            MsgSend sendXML = new MsgSend();
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record)
                {
                    tkmd = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                    tkmd.Load();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = tkmd.ID;
                    sendXML.Load();
                    this.Cursor = Cursors.WaitCursor;
                    {
                        //xmlCurrent = tkmd.LayPhanHoi(pass, sendXML.msg);
                        xmlCurrent = SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep.LayPhanHoi(pass, sendXML.msg);
                    }
                    this.Cursor = Cursors.Default;
                    // Thực hiện kiểm tra.  
                    if (xmlCurrent != "")
                    {
                        doc.LoadXml(xmlCurrent);
                        node = doc.SelectSingleNode("Megs");
                        if (node == null)
                        {
                            if (showMsg("MSG_STN02", true) == "Yes")
                            //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không", "MSG_STN02", "", true) == "Yes")
                            {
                                this.Refresh();
                                LayPhanHoi(pass);
                            }
                            return;
                        }
                    }

                    if (sendXML.func == 1)
                    {
                        showMsg("MSG_SEN02", tkmd.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + tkmd.SoTiepNhan, "MSG_SEN02", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 3)
                    {
                        showMsg("MSG_2702029");
                        //MLMessages("Đã hủy tờ khai này", "MSG_CAN01", "", false);
                        this.search();
                    }
                    else if (sendXML.func == 2)
                    {
                        if (tkmd.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            showMsg("MSG_SEN11", new string[] { tkmd.SoToKhai.ToString(), mess });
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.Số tờ khai :" + tkmd.SoToKhai.ToString(), "MSG_SEN03", tkmd.SoToKhai.ToString(), false);                            
                            this.search();
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            string mess = "";
                            if (node != null)
                            {
                                if (node.HasChildNodes)
                                    mess += "\nCó phản hồi từ hải quan : \r\n";
                                foreach (XmlNode nodeCon in node.ChildNodes)
                                {
                                    mess += FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                                }
                            }
                            //showMsg("MSG_SEN04", mess);
                            //MLMessages("Hải quan chưa xử lý tờ khai này!", "MSG_STN06", "", false);
                            
                            if (mess != "")
                                ShowMessage(mess, false);
                            else
                                ShowMessage("Hải quan chưa xử lý tờ khai chuyển tiếp này!", false);
                        }
                        else if (tkmd.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                        {
                            showMsg("MSG_SEN05");
                            //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.\nHãy kiểm tra lại dữ liệu", "MSG_SEN05", "", false);
                            this.search();
                        }
                    }
                    setCommandStatus();
                    //xoa thông tin msg nay trong database
                    sendXML.Delete();
                    this.Cursor = Cursors.Default;
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //MLMessages("Không kết nối được với hệ thống hải quan.", "MSG_WRN12", "", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //MLMessages("Có lỗi trong khai báo : " + msg[0], "MSG_SEN09", msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //MLMessages("Xảy ra lỗi không xác định." + ex.Message, "MSG_WRN13", "", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            showMsg("MSG_2702004", ex.Message);
                            //GlobalSettings.PassWordDT = "";
                            ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận  Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void cmMain_CommandClick_1(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSend":
                    Send();
                    break;
                case "cmdSingleDownload":
                    Download();
                    break;
                case "cmdCancel":
                    Cancel();
                    break;
                case "XacNhan":
                    LaySoTiepNhanDT();
                    break;
                case "XacNhan1":
                    LaySoTiepNhanDT();
                    break;
                case "cmdXuatToKhai":
                    XuatToKhaiGCCTChoPhongKhai();
                    break;
                case "ToKhaiViet":
                    try
                    {
                        Print();
                    }
                    catch { ShowMessage(" Lỗi dữ liệu, vui lòng kiểm tra lại ", false); }
                    break;
                case "cmdInTKGCCT":
                    try
                    {
                        this.PrintGCCTA4();
                    }
                    catch { ShowMessage(" Lỗi dữ liệu, vui lòng kiểm tra lại ", false); }
                    break;
                //case "cmdTKGCCTA4":
                //    try
                //    {
                //    this.PrintGCCTA4();
                //    }
                //    catch { ShowMessage(" Không có dữ liệu ", false); }
                //    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdINGCCTTQ":
                    this.inGCCTTQ();
                    break;

                case "cmdCSDaDuyet":
                    ChuyenTrangThai();
                    break;
                case "Copy":
                    SaoChepToKhai();
                    break;
                case "CopyHang":
                    SaoChepHangHoa();
                    break;
                case "CopyTK":
                    SaoChepHangHoa();
                    break;
            }
        }

        private void SaoChepToKhai()
        {
            if (dgList.GetRow().DataRow != null && dgList.GetRow().RowType == RowType.Record)
            {
                ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                //ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                f.TKCT.ID = TKCT.ID;
                f.TKCT.Load();
                f.TKCT.ID = 0;
                f.TKCT.SoTiepNhan = 0;
                f.TKCT.TrangThaiXuLy = -1;
                f.TKCT.SoToKhai = 0;
                f.TKCT.PhanLuong = "";
                f.OpenType = OpenFormType.Edit;
                f.WindowState = FormWindowState.Maximized;
                f.ShowDialog();
            }
        }
        private void SaoChepHangHoa()
        {

            if (dgList.GetRow().DataRow != null && dgList.GetRow().RowType == RowType.Record)
            {
                ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                ToKhaiChuyenTiep TKCT = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
                f.TKCT.ID = TKCT.ID;
                f.TKCT.Load();
                f.TKCT.LoadHCTCollection();
                f.TKCT.ID = 0;
                f.TKCT.SoTiepNhan = 0;
                f.TKCT.TrangThaiXuLy = -1;
                f.TKCT.SoToKhai = 0;
                f.TKCT.PhanLuong = "";
                f.TKCT.GUIDSTR = Guid.NewGuid().ToString();
                foreach (HangChuyenTiep HCT in f.TKCT.HCTCollection)
                    HCT.ID = 0;

                f.OpenType = OpenFormType.Edit;
                f.WindowState = FormWindowState.Maximized;
                f.ShowDialog();
            }
        }
        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                ToKhaiChuyenTiepCollection tkctColl = new ToKhaiChuyenTiepCollection();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        tkctColl.Add((ToKhaiChuyenTiep)grItem.GetRow().DataRow);
                    }
                }

                for (int i = 0; i < tkctColl.Count; i++)
                {

                    tkctColl[i].LoadHCTCollection();

                    //string msg = "Bạn có muốn chuyển trạng thái của tờ khai được chọn sang đã duyệt không?";
                    //msg += "\n\nSố thứ tự tờ khai: " + tkctColl[i].ID.ToString();
                    //msg += "\n----------------------";
                    //msg += "\nCó " + tkctColl[i].HCTCollection.Count.ToString() + " sản phẩm đăng ký";

                    string[] args = new string[2];
                    args[0] = tkctColl[i].ID.ToString();
                    args[1] = tkctColl[i].HCTCollection.Count.ToString();

                    if (showMsg("MSG_0203073", args, true) == "Yes")
                    {
                        ChuyenTrangThaiTKCT obj = new ChuyenTrangThaiTKCT(tkctColl[i]);
                        obj.ShowDialog();
                    }
                }
                this.search();
            }
            else
            {
                showMsg("MSG_240233");
                //MLMessages("Không có dữ liệu được chọn!", "MSG_WRN14", "", false);
            }
        }

        private void XuatToKhaiGCCTChoPhongKhai()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                showMsg("MSG_0203007");
                //MLMessages("Chưa chọn danh sách tờ khai cần xuất ra file.","MSG_WRN14","", false);
                return;
            }
            try
            {
                ToKhaiChuyenTiepCollection col = new ToKhaiChuyenTiepCollection();
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiChuyenTiepCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    int sotokhai = 0;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiChuyenTiep tkmdSelected = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                            tkmdSelected.LoadHCTCollection();
                            col.Add(tkmdSelected);
                            sotokhai++;
                        }
                    }
                    serializer.Serialize(fs, col);
                    showMsg("MSG_EXC05", sotokhai);
                    //MLMessages("Xuất ra file thành công " + sotokhai + " tờ khai GCCT.", "MSG_EXC05", sotokhai.ToString(), false);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }

        }
        private void inPhieuTN()
        {
            if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
            phieuTNForm.TenPhieu = "HỢP ĐỒNG";
            Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
            string[,] arrPhieuTN = new string[items.Count, 2];
            int j = 0;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    ToKhaiChuyenTiep tkctDangKySelected = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                    arrPhieuTN[j, 0] = tkctDangKySelected.SoTiepNhan.ToString();
                    arrPhieuTN[j, 1] = tkctDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                    j++;
                    break;
                }
            }
            phieuTNForm.phieuTN = arrPhieuTN;
            phieuTNForm.Show();

        }
       
        private void Print()
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewTKCTForm f = new Company.Interface.Report.ReportViewTKCTForm();
                f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }
        private void PrintGCCTA4()
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewGCCTA4Form f = new Report.ReportViewGCCTA4Form();
                f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }
        private void inGCCTTQ()
        {
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewGCCTTQForm f = new Report.ReportViewGCCTTQForm();
                f.TKCT = (ToKhaiChuyenTiep)row.DataRow;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string maLH = e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim();
                e.Row.Cells["MaLoaiHinh"].Text = maLH + " - " + this.LoaiHinhMauDich_GetName(maLH);

                if (e.Row.Cells["NgayTiepNhan"].Text != "")
                {
                    DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                    if (dt.Year <= 1900)
                        e.Row.Cells["NgayTiepNhan"].Text = "";
                }

                DateTime dtNgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Text);
                if (dtNgayDangKy.Year <= 1900)
                    e.Row.Cells["NgayDangKy"].Text = "";

                #region Begin TrangThaiXuLy
                switch (Convert.ToInt32(e.Row.Cells["TrangThaiXuLy"].Value))
                {
                    case -1:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Chưa khai báo", "Not declared yet");
                        break;
                    case 0:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ duyệt", "Wait for approval");
                        break;
                    case 1:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã duyệt", "Approved");
                        break;
                    case 2:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Không phê duyệt", "Not approved");
                        break;
                    case 5:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Sửa tờ khai", "Edit");
                        break;
                    case 10:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Đã hủy", "Deleted");
                        break;
                    case 11:
                        e.Row.Cells["TrangThaiXuLy"].Text = setText("Chờ hủy", "Wait for delete");
                        break;
                }
                #endregion End TrangThaiXuLy

                #region Begin PhanLuong
                if (e.Row.Cells["PhanLuong"].Value != null && e.Row.Cells["PhanLuong"].Value != "")
                {
                    switch (Convert.ToInt32(e.Row.Cells["PhanLuong"].Value))
                    {
                        case 1:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng xanh";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Green";
                            break;
                        case 2:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng vàng";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Yellow";
                            break;
                        case 3:
                            if (GlobalSettings.NGON_NGU == "0")
                                e.Row.Cells["PhanLuong"].Text = "Luồng đỏ";
                            else
                                e.Row.Cells["PhanLuong"].Text = "Red";
                            break;
                    }

                    e.Row.Cells["HuongDan"].ToolTipText = e.Row.Cells["HuongDan"].Text;
                }
                #endregion End PhanLuong
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "TKCT";
                        sendXML.master_id = tkct.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Không được xóa, sửa?", "MSG_STN11",j.ToString(), false);
                            //if (st == "Yes")
                            //{
                            //    if (tkct.ID > 0)
                            //    {
                            //        tkct.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (tkct.ID > 0)
                            {
                                if (tkct.MaLoaiHinh.Contains("X"))
                                {
                                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                                    {
                                        ShowMessage("Tờ khai chuyển tiếp có id = " + tkct.ID + " này đã được phân bổ nên không thể xóa được.", false);
                                        continue;
                                    }
                                }
                                tkct.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cbStatus.SelectedValue) == Company.GC.BLL.TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                txtNamTiepNhan.Text = string.Empty;
                txtNamTiepNhan.Value = 0;
                txtNamTiepNhan.Enabled = false;
                txtSoTiepNhan.Value = 0;
                txtSoTiepNhan.Text = string.Empty;
                txtSoTiepNhan.Enabled = false;
            }
            else
            {
                txtNamTiepNhan.Value = DateTime.Today.Year;
                txtNamTiepNhan.Enabled = true;
                txtSoTiepNhan.Enabled = true;
            }
            this.search();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (tkctCollection.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "TKCT";
                        sendXML.master_id = tkct.ID;
                        if (sendXML.Load())
                        {
                            //j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn không được xóa?", "MSG_STN09", j.ToString(),false);
                            //if (st == "Yes")
                            //{
                            //    if (tkct.ID > 0)
                            //    {
                            //        tkct.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (tkct.ID > 0)
                            {
                                if (tkct.MaLoaiHinh.Contains("X"))
                                {
                                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(tkct.ID))
                                    {
                                        ShowMessage("Tờ khai chuyển tiếp có id = " + tkct.ID + " này đã được phân bổ nên không thể xóa được.", false);
                                        continue;
                                    }
                                }
                                tkct.Delete();
                            }
                        }
                    }
                }
                this.search();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() == null) return;
            ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)dgList.GetRow().DataRow;
            Globals.ShowKetQuaXuLyBoSung(tkct.GUIDSTR);

        }

      


    }
}