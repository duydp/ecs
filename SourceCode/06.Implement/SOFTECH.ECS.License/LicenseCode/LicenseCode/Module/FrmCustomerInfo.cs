﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using KeyServer;

namespace LicenseCode.Module
{
    public partial class FrmCustomerInfo : DevExpress.XtraEditors.XtraForm
    {
        private Customer Cust;
        public Boolean isEdit = false;
        //public FrmInvoiceInfo objInfo = null;

        public FrmCustomerInfo(string ID)
        {
            InitializeComponent();
            if (ID == null)
            {
                txtCustCode.Text = Math.Abs(DateTime.Now.GetHashCode()).ToString();
                this.Text = "Thêm mới khách hàng: " + txtCustCode.Text;
                lblCaption.Text = "THÊM MỚI KHÁCH HÀNG: " + txtCustCode.Text;
                Cust = new Customer();
                this.Name = "Cust_New";
                this.isEdit = true;
            }
            else
            {
                txtCustCode.Text = ID;
                this.Text = "Khách hàng: " + txtCustCode.Text;
                lblCaption.Text = "Khách hàng: " + txtCustCode.Text;
                //Cust = Customer.Load(ID);
                this.Name = "Cust_" + ID;
                this.FillData();
            }
        }

        #region Add Methods.
        private void GetData()
        {
            Cust.CustomerCode = txtCustCode.Text;
            Cust.CustomerName = txtCustName.Text;
            Cust.ContactFirstName = txtCustFirstName.Text;
            Cust.ContactLastName = txtCustLastName.Text;
            Cust.CompanyOrDepartment = txtCustDepartment.Text;
            Cust.ContactTitle = txtCustTitle.Text;
            Cust.BillingAddress = txtCustAddress.Text;
            Cust.City = txtCustCity.Text;
            Cust.StateOrProvince = txtCustProvince.Text;
            Cust.PostalCode = txtCustPostalCode.Text;
            Cust.CustomerPhone = txtCustPhone.Text;
            Cust.FaxNumber = txtCustFax.Text;
            Cust.CustomerEmail = txtCustEmail.Text;
            Cust.CustomerDescription = txtCustNotes.Text;
            Cust.TaxCode = txtMaDN.Text;
        }

        private void FillData()
        {
            txtCustCode.Text = Cust.CustomerCode;
            txtCustName.Text = Cust.CustomerName;
            txtCustFirstName.Text = Cust.ContactFirstName;
            txtCustLastName.Text = Cust.ContactLastName;
            txtCustDepartment.Text = Cust.CompanyOrDepartment;
            txtCustTitle.Text = Cust.ContactTitle;
            txtCustAddress.Text = Cust.BillingAddress;
            txtCustCity.Text = Cust.City;
            txtCustProvince.Text = Cust.StateOrProvince;
            txtCustPostalCode.Text = Cust.PostalCode;
            txtCustPhone.Text = Cust.CustomerPhone;
            txtCustFax.Text = Cust.FaxNumber;
            txtCustEmail.Text = Cust.CustomerEmail;
            txtCustNotes.Text = Cust.CustomerDescription;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            groupCustInfo.Enabled = val;
            groupCustAddress.Enabled = val;
            btnEdit.Enabled = !val;
        }
        #endregion

        private void FrmCustomerInfo_Load(object sender, EventArgs e)
        {
            this.DisableButton(isEdit);
        }

        private bool ValidateControl(DevExpress.XtraEditors.TextEdit textControl, string message)
        {
            errorProvider1.SetError(textControl, "");

            if (textControl.Text.Length == 0)
            {
                errorProvider1.SetError(textControl, message);
                textControl.Focus();
                return false;
            }

            return true;
        }

        private bool ValidateForm()
        {
            bool valid = true;

            valid &= ValidateControl(txtMaDN, "Chưa nhập Mã Doanh nghiệp.");

            if (valid)
                valid &= ValidateControl(txtCustFirstName, "Chưa nhập Họ và tên đại diện.");

            if (valid)
                valid &= ValidateControl(txtCustLastName, "Chưa nhập Họ và tên đại diện.");

            if (valid)
                valid &= ValidateControl(txtCustAddress, "Chưa nhập Địa chỉ.");

            if (valid)
                valid &= ValidateControl(txtCustCity, "Chưa nhập Thành phố.");

            if (valid)
                valid &= ValidateControl(txtCustPhone, "Chưa nhập Điện thoại.");

            if (valid)
                valid &= ValidateControl(txtCustEmail, "Chưa nhập Thư điện tử.");

            return valid;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!ValidateForm())
                return;

            this.GetData();
            if (this.Name == "Cust_New")
            {
               // Cust.Insert();
                this.Text = "Khách hàng: " + Cust.CustomerCode;
                lblCaption.Text = "Khách hàng: " + Cust.CustomerCode;
                this.Name = "Cust_" + Cust.CustomerCode;
            }
            else
            {
                //Cust.Update();
            }
            this.DisableButton(false);

            //if (objInfo != null)
            //{
            //    objInfo.SelectCustomer(Cust.ID);
            //}
            //if (ClassLib.IsFormShowed("FrmCustomerManager"))
            //{
            //    FrmCustomerManager obj = (FrmCustomerManager)Application.OpenForms["FrmCustomerManager"];
            //    obj.RefreshData();
            //}
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Cust_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}