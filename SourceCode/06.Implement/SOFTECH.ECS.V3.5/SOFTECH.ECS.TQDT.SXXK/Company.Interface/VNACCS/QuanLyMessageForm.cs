using System;
using System.Data;
using System.Windows.Forms;
//using Company.BLL;
//using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Collections;
//using Company.Interface.KDT.SXXK;
using System.Drawing;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.LogMessages;
using System.Collections.Generic;
using Company.KDT.SHARE.VNACCS.Controls;
namespace Company.Interface.VNACCS
{
    public partial class QuanLyMessageForm : BaseForm
    {
        public List<MsgPhanBo> listPhanBo = new List<MsgPhanBo>();
        //public List<MsgPhanBo> msgList = (List<MsgPhanBo>)MsgPhanBo.SelectCollectionAll();
       
        public MsgPhanBo msg = new MsgPhanBo();
        public QuanLyMessageForm()
        {
            InitializeComponent();
        }

        private void LoadData(string where)
        {
            try
            {
                listPhanBo = (List<MsgPhanBo>)MsgPhanBo.SelectCollectionDynamic(where, "");
                dgList.DataSource = listPhanBo;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void QuanLyMsgForm_Load(object sender, EventArgs e)
        {
            try
            {
                cbUserKB.SelectedIndex = 0;
                this.setDataToComboUserKB();
                cbUserKB.SelectedIndex = 0;
                ccTuNgay.Value = DateTime.Now.AddDays(-30);
                ccDenNgay.Value = DateTime.Now;
                if (ckbTimKiem.Checked)
                    grbtimkiem.Enabled = true;
                else
                    grbtimkiem.Enabled = false;
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                //string userKB = (cbUserKB.SelectedValue == null) ? "" : cbUserKB.SelectedValue.ToString().Trim();
                string sotk = txtSoTK.Text.Trim();
                string tungay = Convert.ToDateTime(ccTuNgay.Value).ToString("dd/MM/yyyy hh:mm:ss");
                string denngay = Convert.ToDateTime(ccDenNgay.Value).ToString("dd/MM/yyyy hh:mm:ss");
                string where = "(1=1) ";
                //if (sotk != "")
                //    where = where + "AND  SoTiepNhan like '%" + sotk + "%' ";
                if (ckbTimKiem.Checked)
                {
                    if (tungay != null && denngay != null)
                        where = where + " and CreatedTime between '" + Convert.ToDateTime(ccTuNgay.Value).ToString("MM/dd/yyyy 00:00:00") + "'" + " and " + "'" + Convert.ToDateTime(ccDenNgay.Value).ToString("MM/dd/yyyy 23:59:59") + "'";
                }
                else
                {
                    if (sotk != "")
                        where = where + "AND  SoTiepNhan like '%" + sotk + "%' ";
                }
                //if (tungay != null && denngay != null)
                //    where = where + " and CreatedTime between '" + Convert.ToDateTime(ccTuNgay.Value).ToString("MM/dd/yyyy 00:00:00") + "'" + " and " + "'" + Convert.ToDateTime(ccDenNgay.Value).ToString("MM/dd/yyyy 23:59:59") + "'";
                if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != (cbUserKB.DataSource as List<Company.QuanTri.User>).Count - 1 )
                    where = where + " AND  TerminalID  = '" + cbUserKB.SelectedValue.ToString() + "'";

                listPhanBo.Clear();
                LoadData(where);
                dgList.RootTable.FilterCondition = null;
                dgList.DataSource = listPhanBo;
                dgList.Refresh();


                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
                this.Cursor = Cursors.Default;
            }
        }

        private void setDataToComboUserKB()
        {
            try
            {
                //DataTable dt = Company.QuanTri.User.SelectAll().Tables[0];
                //DataRow dr = dt.NewRow();
                List<Company.QuanTri.User> listUser = Company.QuanTri.User.SelectCollectionDynamic_VNACC(" VNACC_TerminalID is not null", null);
                //dr["USER_NAME"] = -1;
                //dr["HO_TEN"] = "--Tất cả--";
                //dt.Rows.InsertAt(dr, 0);
                listUser.Add(new Company.QuanTri.User() { USER_NAME = "-1", HO_TEN = "--Tất cả--" });
                cbUserKB.DataSource = listUser;
                cbUserKB.DisplayMember = "HO_TEN";// dt.Columns["HO_TEN"].ToString();
                cbUserKB.ValueMember = "VNACC_TerminalID";// dt.Columns["TerminalID"].ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void chkGroup_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!chkGroup.Checked)
                {
                    dgList.RootTable.Groups.Clear();
                    dgList.RootTable.Columns["MaNgiepVu"].Visible = true;
                }
                else
                {
                    GridEXGroup group = new GridEXGroup();
                    GridEXColumn column = new GridEXColumn();
                    dgList.RootTable.Groups.Clear();
                    column = dgList.RootTable.Columns["MaNgiepVu"];
                    group = new GridEXGroup(column, Janus.Windows.GridEX.SortOrder.Ascending);
                    dgList.RootTable.Groups.Add(group);
                }
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (!string.IsNullOrEmpty(e.Row.Cells["MaNghiepVu"].Value.ToString()))
            //{
            //    e.Row.Cells["MaNghiepVu"].Value = ReturnMessages.GetThongBaoPhanHoi(e.Row.Cells["MaNghiepVu"].Value.ToString());

            //}
            try
            {
                if (e.Row.Cells["MaNghiepVu"].Value != null)
                    e.Row.Cells["Ten"].Text = ReturnMessages.GetThongBaoPhanHoi(e.Row.Cells["MaNghiepVu"].Value.ToString());
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void messagesGốcToolStripMenuItem_Click(object sender, EventArgs ev)
        {
            if (dgList.SelectedItems[0].RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(dgList.SelectedItems[0].GetRow().Cells["Master_ID"].Value);
                    MsgLog msg = MsgLog.Load(id);
                    if (msg != null)
                    {
                        ShowMessagesEDI f = new ShowMessagesEDI(msg.Log_Messages);
                        f.ShowDialog(this);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    ShowMessage(ex.Message, false);
                }
            }
        }


        //private void thêmMessageToolStripMenuItem_Click(object sender, EventArgs e)
        //{
        //    if (dgList.SelectedItems[0].RowType == RowType.Record)
        //    {
        //        try
        //        {
        //            int id = Convert.ToInt32(dgList.SelectedItems[0].GetRow().Cells["ID"].Value);
        //            MsgLog msg = MsgLog.Load(id);
        //            if (msg != null)
        //            {
        //                //AddMessageForm f = new AddMessageForm(msg.Log_Messages);
        //                //f.ShowDialog(this);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            ShowMessage(ex.Message, false);
        //        }
        //    }
        //}

        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                AddMessageForm f = new AddMessageForm();
                f.ShowDialog();
                if (f.tkmd != null)
                {
#if SXXK_V4
                Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(f.tkmd);
#elif GC_V4
                    Company.GC.BLL.VNACC.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(f.tkmd, GlobalSettings.MA_DON_VI != "4000395355");
#endif

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //private void dgList_DoubleClick(object sender, EventArgs e)
        //{
        //     if (e.Row.RowType == RowType.Record)
        //    {
        //        try
        //        {
        //            int id = Convert.ToInt32(dgList.SelectedItems[0].GetRow().Cells["Master_ID"].Value);
        //            string maNV = dgList.SelectedItems[0].GetRow().Cells["MaNghiepVu"].Value.ToString();
        //            ProcessReport.ShowReport(id, maNV);
        //        }
        //        catch (Exception ex)
        //        {
        //            Logger.LocalLogger.Instance().WriteMessage(ex);
        //            Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
        //        }
        //        finally { Cursor = Cursors.Default; }
        //    }
        //}

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    int id = Convert.ToInt32(dgList.SelectedItems[0].GetRow().Cells["Master_ID"].Value);
                    string maNV = dgList.SelectedItems[0].GetRow().Cells["MaNghiepVu"].Value.ToString();
                    ProcessReport.ShowReport(id, maNV);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (ckbTimKiem.Checked)
                {
                    grbtimkiem.Enabled = true;
                    txtSoTK.Enabled = false;
                }
                else
                {
                    grbtimkiem.Enabled = false;
                    txtSoTK.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        } 


    }
}